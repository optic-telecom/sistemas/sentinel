#!/bin/bash
source /home/sentinel/sentinel_env/bin/activate
cd /home/sentinel/sentinel_backend/sentinel/
exec celery worker -A config.celery -B --loglevel=info --scheduler django_celery_beat.schedulers:DatabaseScheduler
