#!/bin/bash

NAME="Sentinel_server" # Name of the application
DJANGODIR=/home/sentinel/sentinel_backend/sentinel # Django project directory
LOGFILE=/home/sentinel/sentinel_backend/logs/gunicorn_sentinel.log
LOGDIR=$(dirname $LOGFILE)
SOCKFILE=/home/sentinel/sentinel_backend/run/gunicorn.sock # we will communicate using this unix socket
LOGDIR_PROJECT=/home/sentinel/sentinel_backend/logs # we will communicate using this unix socket
USER=sentinel # the user to run as
GROUP=sentinel # the group to run as
NUM_WORKERS=3 # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=config.settings.$1  # which settings file should Django use
DJANGO_WSGI_MODULE=config.wsgi
TIMEOUT=600

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
source /home/sentinel/sentinel_env/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Create the run directory if it doesn't exist for logs sentinel
test -d $LOGDIR_PROJECT || mkdir -p $LOGDIR_PROJECT

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec /home/sentinel/sentinel_env/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
--name $NAME \
--workers $NUM_WORKERS \
--user=$USER --group=$GROUP \
--bind=unix:$SOCKFILE \
--log-level=debug \
--timeout $TIMEOUT \
--capture-output \
--log-file=$LOGFILE 2>>$LOGFILE