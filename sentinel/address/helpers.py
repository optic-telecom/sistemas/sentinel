import requests

from utp.permissions import IsSystemInternalPermission, TOKEN

from dynamic_preferences.registries import global_preferences_registry


def request_address_pulso(id, timeout=30):
    preferences = global_preferences_registry.manager()
    # r = requests.get(f'http://127.0.0.1:9001/api/v1/complete-location/{id}/' )
    headers = {'Authorization': TOKEN}
    r = requests.get(f'{preferences["url_pulso"]}api/v1/complete-location/{id}/', headers=headers, timeout=timeout, verify=False)
    status_code = r.status_code
    return status_code, r


def get_complete_address(id):

    status_code, r = request_address_pulso(id)
    
    if status_code == 200:
        address = r.json()
        res = { 
            'ok': True,
            'data': {
                'id': address['id'],
                'number': address['number'],
                'country': address['country'],
                'country_id': address['country_id'],
                'region': address['region'],
                'region_id': address['region_id'],
                'commune': address['commune'],
                'commune_id': address['commune_id'],
                'street': address['street'],
                'street_id': address['street_id'],
                'tower': address['tower'],
                'floor': address['floor_num'],
                'department': address['departament'],
                'street_location': address['street_location']
                
            }
            
        }

        return res

    else :
        return {
            'ok': True, 
            'detail': f'error {status_code}', 
            'data': {
                'id': '-',
                'number': '-',
                'country': '-',
                'country_id': '-',
                'region': '-',
                'region_id': '-',
                'commune': '-',
                'commune_id': '-',
                'street': '-',
                'street_id': '-',
                'tower': '-',
                'floor': '-',
                'department': '-',
                'street_location': '-'
            }
        }
    