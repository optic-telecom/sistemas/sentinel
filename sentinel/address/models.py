import requests
from django.db import models
from .helpers import request_address_pulso, get_complete_address

import pprint

# Create your models here.
class Address(models.Model):
    id_pulso = models.PositiveIntegerField(blank = True)
    
    _address = None

    def _get_address(self):
        if(not self._address):
            res = get_complete_address(self.id_pulso)
            # print('---------------')
            # pprint.pprint(res)
            if res['ok']:
                self._address = res['data']
                return True
            
            return False

        return True

    @property
    def country(self):
        if self._get_address():
            return self._address['country']
        else:
            return ''

    @property
    def region(self):
        if self._get_address():
            return self._address['region']
        else:
            return ''

    @property
    def commune(self):
        if self._get_address():
            return self._address['commune']
        else:
            return ''

    @property
    def country_id(self):
        if self._get_address():
            return self._address['country_id']
        else:
            return ''

    @property
    def region_id(self):
        if self._get_address():
            return self._address['region_id']
        else:
            return ''

    @property
    def commune_id(self):
        if self._get_address():
            return self._address['commune_id']
        else:
            return ''

    @property
    def tower(self):
        if self._get_address():
            return self._address['tower']
        else:
            return ''

    @property
    def street(self):
        if self._get_address():
            return self._address['street']
        else:
            return ''

    @property
    def street_id(self):
        if self._get_address():
            return self._address['street_id']
        else:
            return ''

    @property
    def floor(self):
        if self._get_address():
            return self._address['floor']
        else:
            return ''
    
    @property
    def department(self):
        if self._get_address():
            return self._address['department']
        else:
            return ''

    @property
    def streetlocation(self):
        if self._get_address():
            return self._address['street_location']
        else:
            return ''

    
    @property
    def number(self):
        if self._get_address():
            return self._address['number']
        else:
            return ''

    @property
    def complete_location(self):
        if self._get_address():
            return self._address['street_location']
        else:
            return ''


    def save(self, *args, **kwargs):
        if not self.pk:
            # This code only happens if the objects is
            # not in the database yet. Otherwise it would
            # have pk

            if not self._get_address():
                raise Address.DoesNotExist

        super(Address, self).save(*args, **kwargs)