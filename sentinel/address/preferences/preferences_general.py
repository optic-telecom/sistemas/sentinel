from dynamic_preferences.types import StringPreference
from dynamic_preferences.registries import global_preferences_registry
from dynamic_preferences.users.registries import user_preferences_registry


@global_preferences_registry.register
class URLPulso(StringPreference):
    name = 'url_pulso'
    default = 'https://pulso.multifiber.cl/'
    required = True