#By JovanPacheco

class Attrs(object):
    """ Clase de atributos"""
    def __init__(self, **kwargs):
        for n,v in kwargs.items():
            self[n] = v

    def __getattr__(self, name):
        return self.__getitem__(name)

    def __getitem__(self, name):
        v = self.__dict__.get(name)
        if not v:
            raise AttributeError('')
        return v
        
    def __setitem__(self, name, value):
        self.__dict__[name] = value

    def __contains__(self, name):
        return name in self.__dict__