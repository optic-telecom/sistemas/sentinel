import os
import time
import uuid

from django.core.management.base import OutputWrapper,\
    BaseCommand as DjangoCommand
from django.core.management.color import no_style
from django.utils.termcolors import colorize

from dynamic_preferences.registries import global_preferences_registry
from .utils import cyan



class BaseCommand(DjangoCommand):

    
    def __init__(self, *args, **kwargs):
        global_preferences = global_preferences_registry.manager()
        if not global_preferences['execute_commands']:
        	cyan('Ejecucion de comandos no esta habilitado')
        	exit()

        # un identificador de lotes para el comando
        self.command_uuid = uuid.uuid4()
        self.is_command_wrappers = True 
        # is_command_wrappers para saber si estoy dentro de este 
        super(BaseCommand, self).__init__(*args, **kwargs)


    def out(self, text, fg='green', bg='black'):

        if os.name == 'nt':
            print (str(text))
        else:
            print (colorize(str(text), fg=fg, bg=bg))

    def get_by(self, **options):
        if 'user' in options:
            return options['user']

        if 'by' in options:
            return options['by']

        if 'request' in options:
            if options['request'].user:
                return options['request'].user.username
                
        return 'system'

    def execute(self, *args, **options):
        """
        Antes de llamar al metodo handler
        se pasan parametos por defectos y se mide el tiempo
        """
        inicio = time.time()
        if options['no_color']:
            self.style = no_style()
            self.stderr.style_func = None
        if options.get('stdout'):
            self.stdout = OutputWrapper(options['stdout'])
        if options.get('stderr'):
            self.stderr = OutputWrapper(options['stderr'], self.stderr.style_func)

        if self.requires_system_checks and not options.get('skip_checks'):
            self.check()
        if self.requires_migrations_checks:
            self.check_migrations()
        
        options['close_ssh'] = True
        self.stdout.write('Inicio')

        try:
            output = self.handle(*args, **options)
        except Exception as err_cmd:
            print('\a')
            print('\a')
            print('\a\a\a\a')
            print('\a')
            raise err_cmd

        tiempo_diferencia = time.time() - inicio
        self.stdout.write('Transcurrido: ')
        self.stdout.write(str(tiempo_diferencia))

        if output:
            self.stdout.write(output)

        if hasattr(self, 'sender'):
            import importlib
            imp = self.sender.split('.')
            imp_module = ".".join(imp[:-1])
            sender = importlib.import_module(imp_module)
            function = getattr(sender, imp[-1])
            if callable(function):
                try:
                    function()
                except Exception as e:
                    print (e)

        return output