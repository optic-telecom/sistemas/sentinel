from django.core.management.base import BaseCommand
from django.db import (connection,connections)

class Command(BaseCommand):
    help = 'Realiza truncate sobre todas las tablas de la base de dato'
    intocables = [
                'devices_olt',
                # DINAMIC PREFERENCES
                'dynamic_preferences_globalpreferencemodel',
                'dynamic_preferences_users_userpreferencemodel',
                # TABLAS DJANGO
                'auth_user_user_permissions',
                'auth_group',
                'auth_user_groups',
                'auth_user',
                'auth_permission',
                'auth_group_permissions',
                'django_migrations',
                'django_session',
                'django_content_type',
                'django_admin_log',
                # VISTAS
                'change_status_node',
                'mikrotik_list',
                'onu_tx_rx_view',
                'target_smokeping',
                ]

    def handle(self, *args, **kwargs):
        truncado = False

        with connection.cursor() as cursor:
            cursor.execute("SELECT table_name FROM information_schema.tables WHERE table_schema=%s",['public'])
            rows = cursor.fetchall()

        listado = list(map(lambda x: x[0],rows))
        listado = list(filter(lambda x: x not in self.intocables, listado))

        for TABLE in listado:
            if not TABLE in self.intocables:
                try:
                    sql = f'TRUNCATE TABLE {TABLE} RESTART IDENTITY CASCADE;'
                    truncado = True
                except Exception as e:
                    error = str(e)
                    print(error)

                with connection.cursor() as cursor:
                    cursor.execute(sql)
                
                print(f'STATUS DE LA TABLA: {TABLE}, TRUNCADO:{truncado}, INDICE REINICIALIZADO:{truncado}')
