from django.db import models
from itertools import tee, islice, chain
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from simple_history.models import HistoricalRecords
from address.models import Address


class BaseModelWithoutHistory(models.Model):
    created = models.DateTimeField(auto_now_add = True, editable=True)
    modified = models.DateTimeField(auto_now = True)
    status_field = models.BooleanField(default = True)
    id_data = models.UUIDField(db_index = True,
                               null = True, blank = True,
                               editable = settings.DEBUG)

    class Meta:
        abstract = True


# BIG CRUNCH!!!1
def previous_and_next(some_iterable):
    prevs, items, nexts = tee(some_iterable, 3)
    prevs = chain([None], prevs)
    nexts = chain(islice(nexts, 1, None), [None])
    return zip(prevs, items, nexts)
    
class BaseModel(models.Model):
    created = models.DateTimeField(auto_now_add = True, editable=True)
    modified = models.DateTimeField(auto_now = True)
    status_field = models.BooleanField(default = True)
    id_data = models.UUIDField(db_index = True,
                               null = True, blank = True,
                               editable = settings.DEBUG)
    history = HistoricalRecords(inherit = True)

    def action_log(self, just=[]):
        fields = self.__class__._meta.fields
        filtered_fields = list(filter(lambda f: f.get_attname() in just, fields) if just else fields)
        field_names = [f.get_attname() for f in filtered_fields]
        historic_field_names = (just or field_names) + ['history_user_id', 'history_date']
        users = {None: 'matrix'}
        users.update({u.pk: u.username for u in get_user_model().objects.filter(id__in=self.history.values_list('history_user_id', flat=True).distinct())})
        rx = self.history.values(*historic_field_names)
        changes = []

        
        for prev, record, nxt in previous_and_next(rx):
            if nxt:
                changed_fields = [f for f in filtered_fields if record[f.get_attname()] != nxt[f.get_attname()]]
                verbose_changed_fields = [f.verbose_name for f in changed_fields]
                if changed_fields:
                    changes.append({'user': users[record['history_user_id']],
                                    'action': _('changed'),
                                    'changes': verbose_changed_fields,
                                    'current_values': {field.attname: record[field.attname] for field in changed_fields},
                                    'previous_values': {field.attname: nxt[field.attname] for field in changed_fields},
                                    'dt': record['history_date']})
            else:
                if not just:
                    changes.append({'user': users[record['history_user_id']],
                                    'action': _('created'),
                                    'dt': record['history_date']})
        return changes

    class Meta:
        abstract = True





class Technology(BaseModel):
    name = models.CharField(max_length=20, db_index=True)

class SupportedLevel(BaseModel):
    name = models.CharField(max_length=40, db_index=True)

    class Meta:
        verbose_name = _('Supported level')
        verbose_name_plural = _('Supported levels')


class AddressNode(BaseModel):
    address = models.OneToOneField(Address, on_delete=models.CASCADE)
    primary = models.BooleanField(blank= True, default=False)

class Node(BaseModel):
    ENABLED = 1
    ENABLED_PARTIALLY = 2
    INCIDENCE = 3
    TO_ENABLE = 4
    DISCARDED = 5
    OTHER = 6
    STATUS_CHOICES = ((ENABLED, _('habilitado')),
                      (ENABLED_PARTIALLY, _('habilitado parcialmente')),
                      (INCIDENCE, _('incidencia mayor')),
                      (TO_ENABLE, _('por abilitar')),
                      (DISCARDED, _('descartado')),
                      (OTHER, _('otro'))
                    )
    
    BUILDING = 1
    CONDOMINIUM = 2
    ENTITY = 3
    ANTENNA = 4
    BUILDING_OSPF = 5
    ONLY_OSPF = 6
    OTHER = 7
    KIND_CHOICES = ((BUILDING, _('Edificio')),
                      (CONDOMINIUM, _('condominio')),
                      (ENTITY, _('empresa')),
                      (ANTENNA, _('torre antenas')),
                      (BUILDING_OSPF, _('edificio ospf')),
                      (ONLY_OSPF, _('ospf')),
                      (OTHER, _('otro'))
                    )

    status = models.PositiveIntegerField(_('status'), default=ENABLED, null=True, choices=STATUS_CHOICES)
    kind = models.PositiveIntegerField(_('kind'), default=ENABLED, null=True, choices=KIND_CHOICES)
    code = models.CharField(max_length=20)
    ip = models.ForeignKey('devices.IPTable',
                            on_delete=models.PROTECT)
    description = models.TextField(max_length=1000)
    address = models.ForeignKey(AddressNode, on_delete=models.CASCADE)
    technologies = models.ManyToManyField(Technology)
    supported_levels = models.ManyToManyField(SupportedLevel)

    # mac = models.ForeignKey('MACTable', null=True, blank=True,
                            # on_delete=models.PROTECT)
    # model = models.ForeignKey(ModelDevice,
    #                           on_delete=models.CASCADE)
    # alias = models.CharField(max_length=80, db_index=True)
    # firmware = models.CharField(max_length=100, null=True, blank=True)
    # ram = models.CharField(max_length=10, null=True, blank=True)
    # cpu = models.CharField(max_length=10, null=True, blank=True)
    # temperature = models.CharField(max_length=10, null=True, blank=True)
    # time_uptime = models.DateTimeField(null=True, blank=True)
    # uptime = models.DateTimeField(null=True, blank=True)
    



