import json

from dynamic_preferences.registries import global_preferences_registry
from django.http import HttpResponse
from django.views.generic import TemplateView
from django.conf import settings
from users.utils import JWTMixin
from django.urls import reverse
from devices.models import Operator, ONUAutoFind

from django.http import JsonResponse
from devices.postgres_views import SendTargetFile
class T(TemplateView):

    def get(self,request,*args,**kwargs):
        send = SendTargetFile()
        return JsonResponse({'id':send()})
    

class HomeView(JWTMixin, TemplateView):
    template_name = 'base.html'

class Dashboard(JWTMixin, TemplateView):
    template_name = 'index.html'

class LoginView(TemplateView):
    template_name = 'users/login.html'

class SearchView(JWTMixin, TemplateView):
    template_name = 'search.html'

class OLTList(JWTMixin, TemplateView):
    template_name = 'olt/list.html'

class UTPList(JWTMixin, TemplateView):
    template_name = 'utp/list.html'

class UTPRadiusList(JWTMixin, TemplateView):
    template_name = 'utp/radius_list.html'

class OLTDetail(JWTMixin, TemplateView):
    template_name = 'olt/detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'newtab' in self.request.GET:
            context['template_base'] = 'base.html'
        else:
            context['template_base'] = 'base_ajax.html'
        return context

class UTPDetail(JWTMixin, TemplateView):
    template_name = 'utp/detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'newtab' in self.request.GET:
            context['template_base'] = 'base.html'
        else:
            context['template_base'] = 'base_ajax.html'
        return context

class UTPRadiusDetail(JWTMixin, TemplateView):
    template_name = 'utp/radius_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'newtab' in self.request.GET:
            context['template_base'] = 'base.html'
        else:
            context['template_base'] = 'base_ajax.html'
        return context

class ProvisioningONU(JWTMixin, TemplateView):
    template_name = 'olt/provisioning_step1.html'

class ProvisioningCPEUTP(JWTMixin, TemplateView):
    template_name = 'utp/provisioning_step1.html'

class ProvisioningONUS_Step2(JWTMixin, TemplateView):
    template_name = 'olt/provisioning_step2.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['operator'] = Operator.objects.filter(status_field=True)
        context['onu'] = ONUAutoFind.objects.get(uuid=kwargs['uuid'])
        return context


class ShowImgView(JWTMixin, TemplateView):
    template_name = 'show_img.html'


class Template_vacio(JWTMixin, TemplateView):
    template_name = 'template_vacio.html'

class IconsView(JWTMixin, TemplateView):
    template_name = 'icons.html'    

class ONUList(JWTMixin, TemplateView):
    template_name = 'onu/list.html'

class CPEUTPList(JWTMixin, TemplateView):
    template_name = 'utpcpe/list.html'
    
class Planes(JWTMixin, TemplateView):
    template_name = 'plan/list.html'

class Operador(JWTMixin, TemplateView):
    template_name = 'operator/list.html'

class VLANs(JWTMixin, TemplateView):
    template_name = 'vlans/list.html'

class Velocidades(JWTMixin, TemplateView):
    template_name = 'speeds/list.html'

class VelocidadesUTP(JWTMixin, TemplateView):
    template_name = 'speeds/utplist.html'

class NodeList(JWTMixin, TemplateView):
    template_name = 'node/list.html'

class SSHOLTs(JWTMixin, TemplateView):
    template_name = 'olt/ssh.html'

class PhysicalExtremeList(JWTMixin, TemplateView):
    template_name = 'fmm/physical_extreme_list.html'

class ManufacturerList(JWTMixin, TemplateView):
    template_name = 'fmm/manufacturer_list.html'

class ManufacturerDetail(JWTMixin, TemplateView):
    template_name = 'fmm/manufacturer_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'newtab' in self.request.GET:
            context['template_base'] = 'base.html'
        else:
            context['template_base'] = 'base_ajax.html'
        return context

class MufaList(JWTMixin, TemplateView):
    template_name = 'fmm/mufa_list.html'

class CabeceraList(JWTMixin, TemplateView):
    template_name = 'fmm/cabecera_list.html'

class CabeceraDetail(JWTMixin, TemplateView):
    template_name = 'fmm/cabecera_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'newtab' in self.request.GET:
            context['template_base'] = 'base.html'
        else:
            context['template_base'] = 'base_ajax.html'
        return context

class CableList(JWTMixin, TemplateView):
    template_name = 'fmm/cable_list.html'

class CableDetail(JWTMixin, TemplateView):
    template_name = 'fmm/cable_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'newtab' in self.request.GET:
            context['template_base'] = 'base.html'
        else:
            context['template_base'] = 'base_ajax.html'
        return context


# UTP VIEWS

# class OLTList(JWTMixin, TemplateView):
#     template_name = 'olt/list.html'
class GearList(JWTMixin, TemplateView):
    template_name = 'gear/list.html'

class GearDetail(JWTMixin, TemplateView):
    template_name = 'gear/detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'newtab' in self.request.GET:
            context['template_base'] = 'base.html'
        else:
            context['template_base'] = 'base_ajax.html'
        return context
        
#====================================================================#
#======================= SMOKEPING VIEW =============================#
#====================================================================#

class SmokepingView(JWTMixin, TemplateView):
    template_name="smokeping/list.html"

#====================================================================#
#========================= IPTABLE VIEW =============================#
#====================================================================#

class IPTableView(JWTMixin, TemplateView):
    template_name="iptable/list.html"

#====================================================================#
#======================= FACTIBILITY VIEW ===========================#
#====================================================================#

class FactibilityView(JWTMixin, TemplateView):
    template_name="factibilidad/factibilidad_remota.html"


class ReportsView(JWTMixin, TemplateView):
    pass



def vars_javascript(request):

    uuid = '00000000-0000-0000-0000-000000000000'

    ## datatables urls
   
    
    datatable_unu_olt = reverse('datatable_unu_olt', args=[uuid])\
                       .replace(uuid,':val:')
    datatable_olt = reverse('datatable_olt')
    # datatable_node = reverse('datatable_node')
    datatable_node = ''
    datatable_vlans = reverse('datatable_vlans')
    datatable_speeds = reverse('datatable_speeds')
    datatable_plan = reverse('datatable_plan')
    datatable_operators = reverse('datatable_operators')
    datatable_olt_vlans = reverse('datatable_olt_vlans', args=[uuid])\
                          .replace(uuid,':val:')
    datatable_olt_speeds = reverse('datatable_olt_speeds', args=[uuid])\
                          .replace(uuid,':val:')
    datatable_olt_ports = reverse('datatable_olt_ports', args=[uuid])\
                          .replace(uuid,':val:')
    datatable_olt_interfaces = reverse('datatable_olt_interfaces', args=[uuid])\
                          .replace(uuid,':val:')
    datatable_olt_users  = reverse('datatable_olt_users', args=[uuid])\
                          .replace(uuid,':val:')
    datatable_olt_logs = reverse('datatable_olt_logs', args=[uuid])\
                          .replace(uuid,':val:')
                          
    datatable_syslog = reverse('datatable_syslog',args=[uuid])\
                            .replace(uuid,':val:')
                                                  
    datatable_utp_logs = reverse('datatable_utp_logs', args=[uuid])\
                          .replace(uuid,':val:')  

    datatable_utpsyslog = reverse('datatable_syslog_mikrotik',args=[uuid])\
                            .replace(uuid,':val:')                
    datatable_unu_list = reverse('datatable_unu_list')

    datatable_SSH_OLT = reverse('datatable_ssh_olt', args=['0'])\
                       .replace('0',':val:')
    datatable_SSH_list = reverse('datatable_ssh_list')

    ## views urls
    olt_onus = reverse('olt_onus')

    ## apis urls
    api_onu_detail = reverse('onu_api-detail', kwargs={'version':'v1','uuid':uuid}).replace(uuid,':val:')
    api_ssh_onu_ping = reverse('ssh_onu_ping_api-detail', kwargs={'version':'v1','uuid':uuid}).replace(uuid,':val:')
    api_ssh_onu_signal = reverse('ssh_onu_signal_api-detail', kwargs={'version':'v1','uuid':uuid}).replace(uuid,':val:')
    api_ssh_command_onu_signal = reverse('ssh_command_onu_signal_api-list', kwargs={'version':'v1'})
    api_ssh_command_onu_ping = reverse('ssh_command_onu_ping_api-list', kwargs={'version':'v1'})
    #OLTS
    api_olt_detail = reverse('olt_api-detail', kwargs={'version':'v1','uuid':uuid}).replace(uuid,':val:')
    api_olt_list = reverse('olt_api-list', kwargs={'version':'v1'})


    api_ssh_olt_list = reverse('ssh_olt_api-list', kwargs={'version':'v1'}) 
    api_plan_list = reverse('plan_api-list', kwargs={'version':'v1'})
    api_plan_detail = reverse('plan_api-detail', kwargs={'version':'v1','pk':0}).replace('0',':val:')
    # api_node_list = reverse('node_api-list', kwargs={'version':'v1'})
    api_node_list = reverse('utp_api-list', kwargs={'version':'v1'})
    api_entity_list = reverse('entity_log_api-list', kwargs={'version':'v1'}) 
    api_actions_log_list = reverse('actions_log_api-list', kwargs={'version':'v1'}) 
    api_operator_list = reverse('operator_api-list', kwargs={'version':'v1'})
    api_type_vlan_list = reverse('type_vlan_api-list', kwargs={'version':'v1'})
    api_type_plan_list = reverse('type_plan_api-list', kwargs={'version':'v1'})
    api_matrix_plan_list = reverse('matrix_plan_api-list', kwargs={'version':'v1'})
    api_matrix_technician_list = reverse('matrix_technician_api-list', kwargs={'version':'v1'})
    api_technician_list = reverse('technician_api-list', kwargs={'version':'v1'})

    api_matrix_service_number_detail = reverse('matrix_service_number_api-detail',
                                               kwargs={'version':'v1','service':'0'}).replace('0',':val:')

    api_provisioning_onu_list = reverse('provisioning_onu_api-list', kwargs={'version':'v1'})
    api_provisioning_perform_onu = reverse('provisioning_perform_onu_api-list', kwargs={'version':'v1'})
    api_log_onu = reverse('log_onu_api-detail', kwargs={'version':'v1','uuid':uuid}).replace(uuid,':val:')
    api_log_olt = reverse('log_olt_api-detail', kwargs={'version':'v1','uuid':uuid}).replace(uuid,':val:')
    api_anormalitie_onu = reverse('anormalities_onu_api-detail', kwargs={'version':'v1','uuid':uuid}).replace(uuid,':val:')
    api_config_onu = reverse('configuration_onu_api-detail', kwargs={'version':'v1','uuid':uuid}).replace(uuid,':val:')
    api_interface_olt = reverse('interface_olt_api-detail', kwargs={'version':'v1','uuid':uuid}).replace(uuid,':val:')
    api_interface_onu = reverse('interface_onu_api-detail', kwargs={'version':'v1','uuid':uuid}).replace(uuid,':val:')
    api_update_remote_olt = reverse('update_remote_olt_api-detail', kwargs={'version':'v1','uuid':uuid}).replace(uuid,':val:')
    api_update_uptime_olts = reverse('update_uptime_olts_api-list', kwargs={'version':'v1'})
    api_update_remote_wifi = reverse('update_remote_wifi_api-detail', kwargs={'version':'v1','uuid':uuid}).replace(uuid,':val:')
    api_update_remote_lan = reverse('update_remote_lan_api-detail', kwargs={'version':'v1','uuid':uuid}).replace(uuid,':val:')
    api_update_remote_statistics_fsp = reverse('update_remote_statistics_fsp_api-detail', kwargs={'version':'v1','pk':0}).replace('0',':val:')
    api_user = reverse('user_view')
    api_task = reverse('task_api-list', kwargs={'version':'v1'})
    api_search = reverse('search_api-list', kwargs={'version':'v1'}) 

    api_system_speeds = reverse('system_speeds_api-list', kwargs={'version':'v1'})
    api_system_vlan = reverse('system_vlan_api-list', kwargs={'version':'v1'})
    api_olt_speed_detail = reverse('olt_speeds_api-detail', kwargs={'version':'v1','pk':uuid}).replace(uuid,':val:')
    api_olt_speed_list = reverse('olt_speeds_api-list', kwargs={'version':'v1'})
    api_olt_vlan = reverse('olt_vlan_api-detail', kwargs={'version':'v1','pk':uuid}).replace(uuid,':val:')


    form_fsp_api = reverse('form_fsp_api-list', kwargs={'version':'v1'})
    fsp_for_olt_api = reverse('fsp_for_olt_api-detail', kwargs={'version':'v1','uuid':uuid}).replace(uuid,':val:')
    fsp_api = reverse('fsp_api-detail', kwargs={'version':'v1','pk':0}).replace('0',':val:')

    feedback_api = reverse('feedback_api-list', kwargs={'version':'v1'})

    api_model_device_list = reverse('model_device_api-list', kwargs={'version':'v1'})
    api_model_for_olt = reverse('model_for_olt_api-detail', kwargs={'version':'v1','uuid':uuid}).replace(uuid,':val:')


    # Select2 para syslog General
    select2_syslog = reverse('select2', kwargs={'selector':'selector',
                                                'dispositivo':'dispositivo',
                                                'uuid':uuid}).replace(uuid,':val:')

    ### BANDA ANCHA TV                                                
    datatable_voucher_list = reverse('tv:datatables:dt-vouchers')
    
    datatable_setupbox_list = reverse('tv:datatables:dt-setupbox')
    datatable_setupbox_detail = reverse('tv:datatables:dt-setupbox-detail',kwargs={'voucher':uuid}).replace(uuid,':val:')
    
    api_voucher_list = reverse('voucher-list',kwargs={'version':'v1'})
    api_voucher_detail = reverse('voucher-detail',kwargs={'version':'v1','pk':uuid}).replace(uuid,':val:')

    api_setupbox_list = reverse('setupbox-list',kwargs={'version':'v1'})
    api_setupbox_detail = reverse('setupbox-detail',kwargs={'version':'v1','pk':uuid}).replace(uuid,':val:')
    #========================= GRÁFICO ===========================#
    # api_grafico = reverse('onu_pg_api-list')
    #========================= GRÁFICO ===========================#

    global_preferences = global_preferences_registry.manager()

    MIKROTIK_URL = ''
    FMM_URL = settings.SENTINEL_FMM_URL

    data = {
        'datatable_unu_olt' : datatable_unu_olt,
        'datatable_unu_list' : datatable_unu_list,
        'datatable_olt' : datatable_olt,
        'datatable_node':datatable_node,
        'datatable_vlans':datatable_vlans,
        'datatable_olt_vlans':datatable_olt_vlans,
        'datatable_operators':datatable_operators,
        'datatable_olt_speeds':datatable_olt_speeds,
        'datatable_olt_ports':datatable_olt_ports,
        'datatable_olt_interfaces':datatable_olt_interfaces,
        'datatable_olt_users':datatable_olt_users,
        'datatable_olt_logs':datatable_olt_logs,
        'datatable_syslog':datatable_syslog,
        'datatable_utpsyslog':datatable_utpsyslog,
        'datatable_speeds':datatable_speeds,
        'datatable_plan':datatable_plan,
        'datatable_SSH_OLT':datatable_SSH_OLT,
        'datatable_SSH_list':datatable_SSH_list,

        # BANDA ANCHA TV
        'datatable_voucher_list':datatable_voucher_list,
        'datatable_setupbox_list':datatable_setupbox_list,
        'datatable_setupbox_detail':datatable_setupbox_detail,
        
        'api_voucher_list':api_voucher_list,
        'api_voucher_detail':api_voucher_detail,

        'api_setupbox_list':api_setupbox_list,
        'api_setupbox_detail':api_setupbox_detail,
        

        #utps
        'datatable_utp_list':MIKROTIK_URL + '/datatables/utp',
        'datatable_cpes_utp':MIKROTIK_URL + '/datatables/utp_cpe/:val:/',
        'datatable_cpes_utp_list':MIKROTIK_URL + '/datatables/utp_cpe',
        'datatable_logs_utp':MIKROTIK_URL + '/datatables/utp_logs/:val:/',
        'datatable_utp_plan':MIKROTIK_URL + '/datatables/planes',
        'datatable_utp_speeds':MIKROTIK_URL + '/datatables/profiles/:val:/',
        'datatable_utp_system_speeds':MIKROTIK_URL + '/datatables/systemprofiles',
        'datatable_utp_radius_list':MIKROTIK_URL + '/datatables/radius',
        'api_utp':MIKROTIK_URL + '/api/v1/utp/',
        'api_utp_detail':MIKROTIK_URL + '/api/v1/utp/:val:/',
        'api_utp_radius':MIKROTIK_URL + '/api/v1/radius/',
        'api_utp_radius_detail':MIKROTIK_URL + '/api/v1/radius/:val:/',
        'api_utp_profile':MIKROTIK_URL + '/api/v1/profileutp/',
        'api_utp_profile_detail':MIKROTIK_URL + '/api/v1/profileutp/:val:/',
        'api_utp_system_profile':MIKROTIK_URL + '/api/v1/system_profileutp/',
        'api_utp_system_profile_detail':MIKROTIK_URL + '/api/v1/system_profileutp/:val:/',
        'api_utp_plan':MIKROTIK_URL + '/api/v1/plan/',
        'api_utp_typeplan':MIKROTIK_URL + '/api/v1/type_plan/',
        'api_utp_operator':MIKROTIK_URL + '/api/v1/operator/',
        'api_utpcpe_detail' : MIKROTIK_URL + '/api/v1/cpeutp/:val:/',
        'api_utpcpe' : MIKROTIK_URL + '/api/v1/cpeutp/',
        'api_update_list_utpcpe' : MIKROTIK_URL + '/api/v1/update_list_cpe/:val:/',
        'api_update_list_radiusprofile' : MIKROTIK_URL + '/api/v1/update_list_radiusprofile/:val:',


        'api_utp_detail_pics':MIKROTIK_URL + '/api/v1/utp/:val:/list_gallery/',
        'api_utp_add_pic':MIKROTIK_URL + '/api/v1/utp/:val:/add_gallery/',
        'api_utp_add_doc':MIKROTIK_URL + '/api/v1/utp/:val:/add_document/',
        'api_utp_detail_docs':MIKROTIK_URL + '/api/v1/utp/:val:/list_document/',
        'api_utp_detail_gears':MIKROTIK_URL + '/api/v1/utp/:val:/get_gears/',
        'api_utp_detail_services_report':MIKROTIK_URL + '/api/v1/utp/:val:/services_report/',
        'api_utp_detail_get_neighbors':MIKROTIK_URL + '/api/v1/utp/:val:/get_neighbors/',
        
        #end utps

        #gears
        'datatable_gear':MIKROTIK_URL + '/datatables/gear',
        'api_gear_list':MIKROTIK_URL + '/api/v1/gear',
        'api_gear_model_list':MIKROTIK_URL + '/api/v1/gear/models',
        'api_gear_brand_list':MIKROTIK_URL + '/api/v1/gear/brands',
        'api_gear_vlans_list':MIKROTIK_URL + '/api/v1/gear/vlans',
        'api_gear_nodes_list':MIKROTIK_URL + '/api/v1/gear/nodes',
        'api_gear_detail':MIKROTIK_URL + '/api/v1/gear/:val:',
        'api_gear_detail_pics':MIKROTIK_URL + '/api/v1/gear/:val:/list_gallery',
        'api_gear_add_pic':MIKROTIK_URL + '/api/v1/gear/:val:/add_gallery',
        'api_gear_add_doc':MIKROTIK_URL + '/api/v1/gear/:val:/add_document/',
        'api_gear_detail_docs':MIKROTIK_URL + '/api/v1/gear/:val:/list_document/',
        #end gears
        #pics
        'pic_api_detail':MIKROTIK_URL + '/api/v1/pic/:val:/',
        #end pics
        #docs
        'doc_api_detail':MIKROTIK_URL + '/api/v1/doc/:val:/',
        #end docs
        
        #availability
        'api_availability_detail':MIKROTIK_URL + '/api/v1/availability/:val:/',
        'api_availability_list':MIKROTIK_URL + '/api/v1/availability/',
        #end docs
        
        #pulso url
        'PULSO_URL':'https://pulso.multifiber.cl',

        #durations
        'api_duration_list': MIKROTIK_URL + '/api/v1/durations/',
        'api_duration_detail': MIKROTIK_URL + '/api/v1/durations/:val:/',
        'api_duration_activities': MIKROTIK_URL + '/api/v1/activities/',


        ## Leasee
        'datatable_lease_utp':MIKROTIK_URL + '/datatables/lease/:val:/',
        'datatable_neighbor_utp':MIKROTIK_URL + '/datatables/neighbor/:val:/',


        # services
        'change_plan':MIKROTIK_URL + '/api/v1/change_plan/:val:/',
        'refresh_network_status':MIKROTIK_URL + '/api/v1/refresh_network_status/',
        'set_primary_equipment': MIKROTIK_URL + '/api/v1/service/:val:/primary_equipment/',


        #FMM
        'datatable_physicalextreme_list':FMM_URL + '/datatables/physicalextreme', 
        'api_physicalextreme_list': FMM_URL + '/api/v1/physicalextreme',
        'api_physicalextreme_detail':FMM_URL + '/api/v1/physicalextreme/:val:/',
        'datatable_manufacturer_list':FMM_URL + '/datatables/manufacturer', 
        'api_manufacturer_list': FMM_URL + '/api/v1/manufacturer',
        'api_manufacturer_detail':FMM_URL + '/api/v1/manufacturer/:val:/',
        'datatable_model':FMM_URL + '/datatables/model/:val:/',
        'api_model_detail':FMM_URL + '/api/v1/model/:val:/',
        'api_manufacturer_models':FMM_URL + '/api/v1/manufacturer/:val:/models/',

        'datatable_mufa_list':FMM_URL + '/datatables/mufa', 
        'api_mufa_detail':FMM_URL + '/api/v1/mufa/:val:/',
        'datatable_cabecera_list':FMM_URL + '/datatables/cabecera',
        'api_cabecera_list': FMM_URL + '/api/v1/cabecera',
        'api_cabecera_detail':FMM_URL + '/api/v1/cabecera/:val:/',
        'datatable_port':FMM_URL + '/datatables/port/:val:/',
        'api_port_detail':FMM_URL + '/api/v1/port/:val:/',
        'datatable_cable_list':FMM_URL + '/datatables/cable',
        'api_cable_list': FMM_URL + '/api/v1/cable',
        'api_cable_detail':FMM_URL + '/api/v1/cable/:val:/',
        'api_termination_list': FMM_URL + '/api/v1/cabletermination',
        'datatable_filament':FMM_URL + '/datatables/filament/:val:/',
        'api_filament_detail':FMM_URL + '/api/v1/filament/:val:/',
        #END FMM

        'api_model_device_list':api_model_device_list,
        'api_model_for_olt':api_model_for_olt,

        'api_onu_detail':api_onu_detail,
        'api_ssh_onu_ping':api_ssh_onu_ping,
        'api_ssh_onu_signal':api_ssh_onu_signal,
        'api_ssh_command_onu_signal':api_ssh_command_onu_signal,
        'api_ssh_command_onu_ping':api_ssh_command_onu_ping,
        'api_olt_detail':api_olt_detail,
        'api_olt_list':api_olt_list,
        'api_ssh_olt_list':api_ssh_olt_list,
        'api_plan_list':api_plan_list,
        'api_plan_detail':api_plan_detail,
        'api_node_list':api_node_list,
        'api_entity_list':api_entity_list,
        'api_actions_log_list':api_actions_log_list,
        'api_operator_list':api_operator_list,
        'api_type_vlan_list':api_type_vlan_list,
        'api_type_plan_list':api_type_plan_list,
        'api_matrix_plan_list':api_matrix_plan_list,
        'api_matrix_technician_list':api_matrix_technician_list,
        'api_technician_list':api_technician_list,
        'api_matrix_service_number_detail':api_matrix_service_number_detail,


        'api_provisioning_onu_list':api_provisioning_onu_list,
        'api_provisioning_perform_onu':api_provisioning_perform_onu,
        'api_log_onu':api_log_onu,
        'api_log_olt':api_log_olt,
        'api_config_onu':api_config_onu,
        'api_anormalitie_onu':api_anormalitie_onu,
        'api_interface_olt':api_interface_olt,
        'api_interface_onu':api_interface_onu,
        'api_system_vlan':api_system_vlan,
        'api_olt_vlan':api_olt_vlan,
        'api_olt_speed_detail':api_olt_speed_detail,
        'api_olt_speed_list':api_olt_speed_list,

        'api_update_remote_olt':api_update_remote_olt,
        'api_update_uptime_olts':api_update_uptime_olts,
        'api_update_remote_wifi':api_update_remote_wifi,
        'api_update_remote_lan':api_update_remote_lan,
        'api_update_remote_statistics_fsp':api_update_remote_statistics_fsp,

        'api_system_speeds':api_system_speeds,
        'api_task':api_task,
        'api_search':api_search,

        'select2_syslog':select2_syslog,

        #========================= GRÁFICO ===========================#
        # 'api_grafico':api_grafico,
        #========================= GRÁFICO ===========================#

        
        'feedback_api':feedback_api,

        'fsp_for_olt_api':fsp_for_olt_api,
        'form_fsp_api':form_fsp_api,
        'fsp_api':fsp_api,
        'api_smokeping': MIKROTIK_URL + '/api/v1/smokeping-menus',

        'olt_onus': olt_onus,

        'name_jwt': settings.NAME_JWT,
        'color_success':global_preferences['color_success'],
        'color_alert':global_preferences['color_alert'],
        'color_error':global_preferences['color_error'],
        'STATIC_URL':settings.STATIC_URL,
        'api_user':api_user,
        'time_reload_dashboard': 10000,
        'MIKROTIK_URL':MIKROTIK_URL,
        'FMM_URL':FMM_URL
    }

    ## configuracione para mikrotik

    


    http_res = 'var Django = ' + json.dumps(data, indent=1)
    return HttpResponse(http_res, content_type="application/javascript")

