from django.urls import path, re_path, include
from rest_framework import routers
from common.templates_views import *
from devices.datatables import DatatableOLT, DatatableUNU, DatatableVLANs,\
     DatatableOperator, DatatableOLTVLAN, DatatableOLTSpeeds, \
     DatatableSystemSpeeds, DatatablePlan, DatatablePortOLT,\
     DatatableInterfacesOLT, DatatableSSH, DatatableOLTUsers,DatatableIPTable,\
     DatatableMenuSmokeping

from utp.datatables import DatatableGear, DatatableLease, DatatableNeighbor

from dashboard.datatables import DatatableLOG
from syslog_app.datatables import SysLogDatatable
from utp.datatables import SysLogMikrotikDatatable
from tv.datatables import VoucherDatatable,SetupBoxDatatable
from utp.datatables import DatatableUTP, DatatableCPEUTP, DatatablePlan, DatatableProfile, \
							DatatableRadius, DatatableSystemProfile, ServiceStatusLogTable

from customers.datatables import ServiceReportTable

#vista de Smokeping
from .views import *

def trigger_error(request):
    division_by_zero = 1 / 0

router = routers.DefaultRouter()



urlpatterns = [
    path('test',T.as_view(),name="paramiko"),
    path('', HomeView.as_view(), name="home"),
    path('sentry-debug/', trigger_error, name='sentry-debug'),
    path('dashboard', Dashboard.as_view(), name="dashboard"),
    path('configuraciones/planes', Planes.as_view(), name="planes"),
    path('configuraciones/operador', Operador.as_view(), name="operador"),
    path('configuraciones/VLANs', VLANs.as_view(), name="VLANs"),
    path('configuraciones/SSH', SSHOLTs.as_view(), name="SSHOLTs"),
    path('configuraciones/velocidades', Velocidades.as_view(), name="velocidades"),
    path('configuraciones/velocidades/utp', VelocidadesUTP.as_view(), name="velocidades"),

    path('login/', LoginView.as_view(), name="login"),
    path('icons/', IconsView.as_view(), name="icons"), 
    re_path(r'^show_img/(?P<src>[-\w]+)/$', ShowImgView.as_view(), name="show_img"), 
    path('search/', SearchView.as_view(), name="search"),

    path('fmm/physicalextreme/list', PhysicalExtremeList.as_view(), name="fmm_physicalextreme_list"),
    path('fmm/manufacturer/list', ManufacturerList.as_view(), name="fmm_manufacturer_list"),
    re_path(r'^fmm/manufacturer/(?P<id>[-\w]+)/$',  ManufacturerDetail.as_view(), name="fmm_manufacturer_detail"),
    path('fmm/mufa/list', MufaList.as_view(), name="fmm_mufa_list"),
    path('fmm/cabecera/list', CabeceraList.as_view(), name="fmm_cabecera_list"),
    re_path(r'^fmm/cabecera/(?P<id>[-\w]+)/$',  CabeceraDetail.as_view(), name="fmm_cabecera_detail"),
    path('fmm/cable/list', CableList.as_view(), name="fmm_cable_list"),
    re_path(r'^fmm/cable/(?P<id>[-\w]+)/$',  CableDetail.as_view(), name="fmm_cable_detail"),

    path('devices/olt/list', OLTList.as_view(), name="olt_list"),
    path('devices/utp/list', UTPList.as_view(), name="utp_list"),
    path('devices/utp/radiuslist', UTPRadiusList.as_view(), name="utp_list"),
    
    path('devices/provisioning/olt/', ProvisioningONU.as_view(), name="provisioning_onu"),
    path('devices/provisioning/utp/', ProvisioningCPEUTP.as_view(), name="provisioning_cpeutp"),
    re_path(r'^devices/provisioning/olt/(?P<uuid>[-\w]+)/$',
            ProvisioningONUS_Step2.as_view(), name="provisioning_onu_step2"),
 	path('devices/olt/onus/',  ONUList.as_view(), name="olt_onus"),
    path('dashboard/nodes/',  NodeList.as_view(), name="node_list"),
    
	re_path(r'^devices/olt/(?P<uuid>[-\w]+)/$',  OLTDetail.as_view(), name="olt_detail"), 	
    re_path(r'^devices/utp/(?P<uuid>[-\w]+)/$',  UTPDetail.as_view(), name="utp_detail"),   
    re_path(r'^devices/utp/radius/(?P<uuid>[-\w]+)/$',  UTPRadiusDetail.as_view(), name="utp_detail"), 
    path('devices/onu/list', ONUList.as_view(), name="onu_list"),
    path('devices/cpeutp/list', CPEUTPList.as_view(), name="cpeutp_list"),

    re_path(r'^datatables/onu/(?P<uuid>[-\w]+)/$', DatatableUNU.as_view() , name="datatable_unu_olt"),
    path('datatables/olt', DatatableOLT.as_view() , name="datatable_olt"),

    #logs
    path('datatables/logs/', DatatableLOG.as_view() , name="datatable_logs"),
    re_path(r'^datatables/olt_logs/(?P<uuid>[-\w]+)/$', DatatableLOG.as_view() , name="datatable_olt_logs"),
    re_path(r'^datatables/utp_logs/(?P<uuid>[-\w]+)/$', DatatableLOG.as_view() , name="datatable_utp_logs"),
    re_path(r'^datatables/olt_users/(?P<uuid>[-\w]+)/$', DatatableOLTUsers.as_view() , name="datatable_olt_users"),

    #syslogs
    path('datatable/syslogs/<uuid>',SysLogDatatable.as_view(),name="datatable_syslog"),
    path('datatable/syslogs-mikrotik/<uuid>',SysLogMikrotikDatatable.as_view(),name="datatable_syslog_mikrotik"),
#     re_path(r'^datatables/syslogs/(?P<uuid>[-\w]+)/$', SysLogDatatable.as_view() , name="datatable_syslog"),
    
    path('datatables/onu', DatatableUNU.as_view() , name="datatable_unu_list"),
    path('datatables/vlans', DatatableVLANs.as_view() , name="datatable_vlans"),
    path('datatables/speeds', DatatableSystemSpeeds.as_view() , name="datatable_speeds"),
    path('datatables/planes', DatatablePlan.as_view() , name="datatable_plan"),

    path('datatables/ssh', DatatableSSH.as_view() , name="datatable_ssh_list"),

    re_path(r'^datatables/ssh_olt/(?P<uuid>[-\w]+)/$',DatatableSSH.as_view() ,name="datatable_ssh_olt"),


    path('datatables/operators', DatatableOperator.as_view() , name="datatable_operators"),
    re_path(r'^datatables/olt_vlans/(?P<uuid>[-\w]+)/$',
            DatatableOLTVLAN.as_view() , name="datatable_olt_vlans"),
    re_path(r'^datatables/olt_speeds/(?P<uuid>[-\w]+)/$',
            DatatableOLTSpeeds.as_view() , name="datatable_olt_speeds"),
    re_path(r'^datatables/olt_ports/(?P<uuid>[-\w]+)/$',
            DatatablePortOLT.as_view() , name="datatable_olt_ports"),    
    re_path(r'^datatables/olt_interfaces/(?P<uuid>[-\w]+)/$',
            DatatableInterfacesOLT.as_view() , name="datatable_olt_interfaces"),   

	path('config_django.js', vars_javascript, name="vars_javascript"),


    path("ipt-settings-list",DatatableIPTable.as_view(),name="ipt-settings-list"),
    path("ipt-settings-detail/<int:menu>",DatatableIPTable.as_view(),name="ipt-settings-detail"),
    path("ms-settings-list",DatatableMenuSmokeping.as_view(),name="ms-settings-list"),

    path('smokeping-settings',
        SmokepingView.as_view(template_name="smokeping/nuevo_smokeping_list.html"),
        name="smokeping-settings"),

    path('smokeping-detail/<int:pk>',
        SmokepingView.as_view(template_name="smokeping/nuevo_smokeping_detail.html"),
        name="smokeping-detail"),

    path('ip-settings',IPTableView.as_view(),name="ip-settings"),
    path('factibility',FactibilityView.as_view(),name="factibility-view"),

    # BANDA ANCHA TV
    ## datatable
    # path('datatables/voucher',
    #     VoucherDatatable.as_view(),
    #     name="dt-vouchers"),#Listado general
    # path('datatables/voucher-detail/<slug:voucher>',
    #     VoucherDatatable.as_view(),
    #     name="dt-vouchers-detail"),
    # path('datatables/setupbox',
    #     SetupBoxDatatable.as_view(),
    #     name="dt-setupbox"),#Listado general
    # path('datatables/setupbox-detail/<slug:voucher>',
    #     SetupBoxDatatable.as_view(),
    #     name="dt-setupbox-detail"),#detalle del setupbox segun voucher
    ##vistas
    # path('tv/vouchers',
    #     BandaAnchaTvView.as_view(template_name='tv_voucher/list.html'),
    #     name="list-vouchers"),
    # path('tv/vouchers-detail/<slug:pk>',
    #     BandaAnchaTvView.as_view(template_name="tv_voucher/detail_index.html"),
    #     name="detail-vouchers"),
    # path('tv/setupbox',
    #     BandaAnchaTvView.as_view(template_name="tv_setupbox/list.html"),
    #     name="list-setupbox"),
    
    # REPORTS
    path('reports/status-nodos',ReportsView.as_view(template_name="reports_tmp/status_node_change_list.html"),name="reports-nodos"),
    path('datatables/status-node-change-list',ServiceStatusLogTable.as_view(),name="dt-status-node-change-list"),
    
    path('datatables/status-service-list',ServiceReportTable.as_view(),name="dt-status-service-list"),
    path('reports/status-servicios',ReportsView.as_view(template_name="reports_tmp/status_service_list.html"),name="reports-servicios"),


    # UPT urls
    path('datatables/utp', DatatableUTP.as_view() , name="datatable_utp"),
    path('datatables/logs/', DatatableLOG.as_view() , name="datatable_logs"),
    re_path(r'^datatables/utp_logs/(?P<uuid>[-\w]+)/$', DatatableLOG.as_view() , name="datatable_utp_logs"),
    re_path(r'^datatables/utp_cpe/(?P<uuid>[-\w]+)/$', DatatableCPEUTP.as_view() , name="datatable_cpe_utp"),
    path('datatables/utp_cpe', DatatableCPEUTP.as_view() , name="datatable_cpeutp_list"),
    path('datatables/planes', DatatablePlan.as_view() , name="datatable_plan"),
    re_path(r'^datatables/profiles/(?P<uuid>[-\w]+)/$', DatatableProfile.as_view() , name="datatable_profile"),
    path('datatables/systemprofiles', DatatableSystemProfile.as_view() , name="datatable_profile"),
    path('datatables/radius', DatatableRadius.as_view() , name="datatable_radius"),


    # urls  
    path('devices/gear/list', GearList.as_view(), name="gear_list"),
    path('datatables/gear', DatatableGear.as_view() , name="datatable_gear_list"),
    re_path(r'^devices/gear/(?P<uuid>[-\w]+)/$',  GearDetail.as_view(), name="gear_detail"),   
    
    re_path(r'^datatables/lease/(?P<uuid>[-\w]+)/$', DatatableLease.as_view() , name="datatable_lease_list"),
    re_path(r'^datatables/neighbor/(?P<uuid>[-\w]+)/$', DatatableNeighbor.as_view() , name="datatable_neighbor_list"),

] + router.urls

