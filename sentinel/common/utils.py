from colorama import Fore, Back, Style, init
init(autoreset=True)
def timedelta_format(td):
    if td.days > 365:
        year = td.days//365
        days = td.days - (year * 365)
        result = year, days , td.seconds//3600, (td.seconds//60)%60, td.seconds%60
        return "{}a {}d {}h {}m {}s".format(*result)
    else:
        result = td.days, td.seconds//3600, (td.seconds//60)%60, td.seconds%60
        return "{}d {}h {}m {}s".format(*result)


def green(*args):
    print(Style.BRIGHT + Fore.GREEN + ' '.join(str(e) for e in args))
    
def red(*args):
    print(Style.BRIGHT + Fore.RED + ' '.join(str(e) for e in args))
    
def cyan(*args):
    print(Style.BRIGHT + Fore.CYAN + ' '.join(str(e) for e in args))

def blue(*args):
    print(Style.BRIGHT + Fore.BLUE + ' '.join(str(e) for e in args))

# Fore: BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE, RESET.
# Back: BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE, RESET.
# Style: DIM, NORMAL, BRIGHT, RESET_ALL 

# print(Fore.RED + 'some red text') 
# print(Style.DIM + 'and in dim text')
# print(Style.RESET_ALL)