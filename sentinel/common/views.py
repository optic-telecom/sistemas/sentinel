from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.views import APIView
from rest_framework.decorators import action
#from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.renderers import CoreJSONRenderer
from rest_framework.schemas import SchemaGenerator
from rest_framework_swagger import renderers
from rest_framework.response import Response
from django.contrib.sites.shortcuts import get_current_site



class MySchemaGenerator(SchemaGenerator):
    title = 'REST API Index'

    def get_link(self, path, method, view):
        link = super(MySchemaGenerator, self).get_link(path, method, view)
        link._fields += self.get_core_fields(view)
        return link

    def get_core_fields(self, view):
        if hasattr(view, "get_core_fields"):
            return getattr(view, 'get_core_fields')(coreapi, coreschema)
        return ()


class SwaggerSchemaView(APIView):
    _ignore_model_permissions = True
    exclude_from_schema = True
    permission_classes = [AllowAny]
    renderer_classes = [
        CoreJSONRenderer,
        renderers.OpenAPIRenderer,
        renderers.SwaggerUIRenderer
    ]

    def get(self, request):
        current_site = get_current_site(request)
        domain = current_site.domain
        proto = 'https' if request.is_secure() else 'http'
        url = proto + '://' + domain
        generator = MySchemaGenerator(
            title="Sentinel API",
            #url=url,
            # patterns=patterns,
            # urlconf=urlconf
        )
        schema = generator.get_schema(request=request)

        if not schema:
            raise exceptions.ValidationError(
                'The schema generator did not return a schema Document'
            )

        return Response(schema)   




        

