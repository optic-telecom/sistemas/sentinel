"""sentinel URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include
from django.conf import settings

from common.views import SwaggerSchemaView
from common.urls import urlpatterns as urlpatter_common
from common.urls import router as router_common
from django.conf.urls.static import static

from users.urls import urlpatterns as urlpatterns_user
from devices.urls import router as router_devices
from dashboard.urls import router as router_dashboard
from users.urls import router as router_user
from utp.urls import urlpatterns as urlpatterns_utp
from devices.urls import urlpatterns as urlpatterns_devices
from utp.urls import router as router_utp
from customers.urls import urlpatterns as urlpatterns_customer
from customers.urls import router as router_customer
from documents.urls import urlpatterns as urlpatterns_documents 

from syslog_app.urls import router as router_syslog
from syslog_app.urls import urlpatterns as urlpatterns_syslog
from tv.urls.tv_api import router as router_tv
from tv.urls import urlpatterns as urlpatterns_tv

urlpatterns = [
    path('admin/', admin.site.urls),   
    # re_path(r'^api/(?P<version>[-\w]+)/', include(router_common)),
    re_path(r'^api/(?P<version>[-\w]+)/', include(router_tv.urls)),
    re_path(r'^api/(?P<version>[-\w]+)/', include(router_devices.urls)),
    re_path(r'^api/(?P<version>[-\w]+)/', include(router_syslog.urls)),
    re_path(r'^api/', include(router_user.urls)),  
    re_path(r'^api/(?P<version>[-\w]+)/', include(router_dashboard.urls)),
    re_path(r'^api/(?P<version>[-\w]+)/', include(router_utp.urls)),
    re_path(r'^api/(?P<version>[-\w]+)/', include(urlpatterns_utp)),
    # re_path(r'^api/(?P<version>[-\w]+)/', include(urlpatterns_tv)),  
    re_path(r'^api/(?P<version>[-\w]+)/', include(router_customer.urls)),
    re_path(r'^api/(?P<version>[-\w]+)/', include(urlpatterns_customer)),  
    re_path(r'^api/(?P<version>[-\w]+)/', include(urlpatterns_devices)),  
    re_path(r'^api/(?P<version>[-\w]+)/', include(urlpatterns_documents)),  
    path('api/docs', SwaggerSchemaView.as_view()),
    path('', include(urlpatter_common)),
    path("tv/", include("tv.urls")),
] + urlpatterns_user + urlpatterns_syslog + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + \
    static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import sys
    if "debug_toolbar" in sys.modules:
        import debug_toolbar

        urlpatterns = [
                          path('__debug__/', include(debug_toolbar.urls)),
                      ] + urlpatterns
