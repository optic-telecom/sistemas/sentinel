import pytest
from config.settings import get_env_variable
from devices.models import Operator, IPTable, IPRange, MACTable, \
                            ModelDevice, ONU, OLT
from customers.models import TypePLAN, Plan, Service
from utp.models import PlanOrderKind, Group, Mikrotik
from address.models import Address
import requests


# Parámetro para la opción de fallar el test
def pytest_addoption(parser):
    parser.addoption("--fail", action="store_true")


# fixture para verificar el valor de "fail"
@pytest.fixture(scope='session')
def fail(request):
    if request.config.getoption('--fail'):
        return 1

    return None


# adquiere la url local que se usará para el testeo
@pytest.fixture
def url():
    test_url = get_env_variable('TEST_URL')
    return test_url

# adquiere la url de pulso que se usará para el testeo
@pytest.fixture
def pulso_url():
    test_pulso_url = get_env_variable('TEST_PULSO_URL')
    return test_pulso_url


# Headers necesarios para poser hacer los posts request a sentinel
@pytest.fixture
def headers():
    test_headers = {'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'}
    return test_headers


##### Modelos creados en fixtures ########

@pytest.fixture
def test_operator():
    test_operator = Operator(name='test_operator',
                             code='ts',
                             id="1")
    test_operator.save()
    return test_operator


@pytest.fixture
def test_type_plan():
    test_type_plan = TypePLAN(name='test', id='1')
    test_type_plan.save()
    return test_type_plan


@pytest.fixture
def test_planorderkind():
    test_planorderkind = PlanOrderKind(name='test', id='1')
    test_planorderkind.save()
    return test_planorderkind


@pytest.fixture
def test_plan(test_operator, test_type_plan):
    test_plan = Plan(id='1', operator=test_operator, name='test',
                     matrix_plan='1', type_plan=test_type_plan)
    test_plan.save()
    return test_plan


@pytest.fixture
def test_group():
    test_group = Group(id='1', name='test')
    test_group.save()
    return test_group


@pytest.fixture
def test_IPTable():
    test_table = IPTable(ip='1.0.0.1', host='test',
                         menu='test', title='test')
    test_table.save()
    return test_table


@pytest.fixture
def test_IPRange(test_IPTable):
    test_range = IPRange(ip=test_IPTable, mask='2')
    test_range.save()
    return test_range


@pytest.fixture
def test_Address(pulso_url):

    pulso_payload = {
            'commune': 'algo',
            'street': 'algo'
        }

    pulso_request = requests.get(pulso_url, pulso_payload)
    dire_id = pulso_request.json()['results'][0]['id']
    test_Add = Address(id_pulso=dire_id)
    test_Add.save()
    return test_Add


@pytest.fixture
def test_model():
    test_model = ModelDevice(model='test', brand='test', kind='1')
    test_model.save()
    return test_model


@pytest.fixture
def test_node(test_planorderkind, test_plan, test_group, test_IPRange,
              pulso_url, test_IPTable, test_Address, test_model):

    test_node = Mikrotik(id='1', group=test_group, alias='test', status='1',
                         address_pulso=test_Address, towers='1',
                         apartments='1', phone='111', ip=test_IPTable,
                         comment='test', no_address_binding=True,
                         matrix_seller='1', description='None',
                         type='1', model=test_model)

    test_node.save()
    return test_node


@pytest.fixture
def test_MACTable():
    test_mac = MACTable(mac='testtest')
    test_mac.save()
    return test_mac


@pytest.fixture
def test_service(test_node):
    test_service = Service(id='1', number='1', seen_connected=True,
                           network_mismatch=False,
                           node_mismatch=test_node, node=test_node)
    test_service.save()
    return test_service


@pytest.fixture
def test_olt(test_IPTable, test_MACTable, test_model):
    test_olt = OLT(ip=test_IPTable, mac=test_MACTable,
                   id='1', alias='test', model=test_model,
                   description='test')
    test_olt.save()
    return test_olt


@pytest.fixture
def test_onu(test_IPTable, test_service, test_olt):
    test_onu = ONU(onu_id='1', alias='test', serial='test',
                   frame=1, slot=1, port=1, olt_id=test_olt.id,
                   ip=test_IPTable, service=test_service)
    test_onu.save()
    return test_onu