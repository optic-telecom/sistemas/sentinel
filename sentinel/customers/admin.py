from django.contrib import admin
from common.admin import BaseAdmin
from .models import TypePLAN, Plan, Service, NetworkStatusChange, \
                    Technician, TechnicianToClient, Enterprise, Slack, \
                    UserProfile


@admin.register(UserProfile)
class UserProfileAdmin(BaseAdmin):
    list_display = ('user',)


@admin.register(Enterprise)
class EnterpriseAdmin(BaseAdmin):
    list_display = ('country_id', 'name')


@admin.register(Slack)
class SlackAdmin(BaseAdmin):
    list_display = ('enterprise',)


@admin.register(TypePLAN)
class TypePLANAdmin(BaseAdmin):
    list_display = ('name', )


@admin.register(Service)
class ServiceAdmin(BaseAdmin):
    list_display = ('number', 'matrix_id', 'seen_connected',
                    'network_mismatch', 'node_mismatch', 'node')


@admin.register(Technician)
class TechnicianAdmin(BaseAdmin):
    list_display = ('username', 'first_name', 'last_name', 'contact_number',
                    'active')
    date_hierarchy = None


@admin.register(TechnicianToClient)
class TechnicianToClientAdmin(BaseAdmin):
    list_display = ('id', 'service', 'technician', 'onu', 'cpeutp', 'date')
    date_hierarchy = None


@admin.register(NetworkStatusChange)
class NetworkStatusChangeAdmin(BaseAdmin):
    list_display = (
        'service',
        'new_state',
        'agent',
    )
    date_hierarchy = 'created_at'


@admin.register(Plan)
class PlanAdmin(BaseAdmin):
    list_display = ('operator', 'name', 'type_plan', 'aggregation_rate',
                    'matrix_plan', 'download_speed', 'upload_speed',
                    'speeds_system_download', 'speeds_system_upload',
                    'profile_name')
    list_filter = ('type_plan', )
