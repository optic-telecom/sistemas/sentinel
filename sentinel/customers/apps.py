from django.apps import AppConfig


class CustomersAppConfig(AppConfig):
    name = 'customers'
    verbose_name = "Customers"
