import datetime 
from dateutil.relativedelta import relativedelta
from django.utils import timezone
from django.db.models import Q#, Value, Sum, F
#from django.db.models.functions import Concat
#from django.db.models.fields import IntegerField

from common.utils import timedelta_format
from common.mixins import DataTable, ButtonDataTable

from customers.models import Service
from random import randint as r



class ServiceReportTable(DataTable):
    """
    Muestra un reporte de los servicios indicando el
    id,nro de servicio, tecnología y status e tiempo real
    este último campo se consigue realizando una conexion 
    al dispositivo físico.

    """
    # model = Service
    columns = ['id','number','get_technology','get_status']
    order_columns = ['id','number','get_technology','get_status']
    max_display_length = 100000 

    def get_initial_queryset(self):
        queryset = (Service.objects.all())
        return queryset


    def render_column(self, row, column):

        if column == 'get_status':
            if row.get_status == 'ACTIVO':
                bg = 'success'
            elif row.get_status == 'CORTADO':
                bg = 'danger'
            else:
                bg = 'dark'
            btn = f'<center><i class="fa fa-circle text-{bg}" title="{row.get_status}"></i></center>'    
            return btn
        else:
           return super().render_column(row, column)


    def filter_queryset(self,qs):
        
        search = self.request.POST.get(u'search[value]', None)

        if search:
            qs = qs.filter(Q(number__istartswith=search)|Q(id__istartswith = search))

        return qs

