import routeros_api
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MaxValueValidator
from django.contrib.auth.models import User
from common.models import BaseModel
from utp.mikrotik import get_routeros_api, _get_client_status, get_mikrotik_info
from devices.models import OLT, ONU, Operator
from django.core.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework import status
from rest_framework.renderers import JSONRenderer


class Service(BaseModel):
    number = models.PositiveIntegerField(_('number'), default=0)
    seen_connected = models.NullBooleanField(_('seen connected'), default=False)
    matrix_id = models.PositiveIntegerField(_('matrix_id'), 
                                            unique=True,
                                            blank=True,
                                            null=True)
    network_mismatch = models.BooleanField(_('network mismatch'),
                                           default=False)
    node_mismatch = models.ForeignKey('utp.Mikrotik',
                                      verbose_name=_('node mismatch'),
                                      null=True,
                                      blank=True,
                                      related_name='+',
                                      on_delete=models.CASCADE)
    node = models.ForeignKey('utp.Mikrotik',
                             verbose_name=_('connection node'),
                             on_delete=models.CASCADE,
                             null=True,
                             blank=True)

    @property
    def seen_connected_live(self):
        try:
            # Buscamos el nodo al que nos conectaremos
            node = self.node
            connection_creds = node.mikrotikconnection_set.all().first()
            username = connection_creds.username
            password = connection_creds.password

            # Nos conectamos al nodo y verificamos si está en el firewall de cortados
            try:
                connection = routeros_api.RouterOsApiPool(
                    node.ip.ip, username=username, 
                    password=password, plaintext_login=True
                )
                api = connection.get_api()
            except Exception as e:
                print(e)
                return None
            
            # Obtenemos la lista de firewall
            firewall_info = api.get_resource("/ip/firewall/address-list")

            # Verificamos si el servicio actual está en la lista
            client_info = firewall_info.get(comment=f"servicio #{self.number}")

            # Si la lista está vacía quiere decir que NO está cortado
            if len(client_info) != 0:
                seen_connected = False
            else:
                seen_connected = True
            connection.disconnect()
            
            # asociamos a la propiedad estática del modelo
            self.seen_connected = seen_connected
            self.save()
            return seen_connected
        except Exception as e:
            print(e)
            return None
    
    @property
    def get_status(self):

        if not self.get_client_ip and not self.get_mikrotik_ip:
            x = self.get_client_ip
            y = 'Conexión no establecida, faltan ip de cliente e ip de mikrotik'

        elif not self.get_client_ip:
            x = self.get_client_ip
            y = 'Conexión no establecida, falta ip del cliente'

        elif not self.get_mikrotik_ip:
            x = self.get_mikrotik_ip
            y = 'Conexión no establecida, falta ip del mikrotik'

        elif self.get_mikrotik_ip and self.get_client_ip:
            try:
                x = _get_client_status(self.get_mikrotik_ip, self.get_username,
                                       self.get_password, self.get_client_ip)
                if x in [True, False]:
                    y = 'Ok'
                else:
                    y = "No se logró establecer una conexión efectiva."

            except Exception as e:
                # print(str(e))
                x = f'Error ...'
                y = f'Error en conexión, {str(e)}'

        status = {False: 'CORTADO', True: 'ACTIVO'}
        return status.get(x, y)

    @property
    def get_mikrotik_ip(self):
        mikrotik_ip = None
        technology = self.get_technology

        if technology == 'FIBRA':
            try:
                mikrotik_ip = self.onu_set.order_by('id').last().olt.node.ip.ip
            except Exception as e:
                str(e)
        elif technology == 'UTP':
            mikrotik_ip = self.cpeutp_set.order_by('id').last().nodo_utp.ip.ip

        return (mikrotik_ip)

    @property
    def get_client_ip(self):
        technology = self.get_technology

        if technology == 'FIBRA':
            try:
                client_ip = self.onu_set.order_by('id').last().ip.ip
            except Exception as e:
                client_ip = None
        elif technology == 'UTP':
            try:
                client_ip = self.cpeutp_set.order_by('id').last().ip.ip
            except Exception as e:
                client_ip = None
        else:
            client_ip = None

        return client_ip

    @property
    def get_technology(self):
        technology = 'SIN TECNOLOGÍA'

        if self.cpeutp_set.filter(service=self).exists():
            technology = 'UTP'
        elif self.onu_set.filter(service__pk=self.pk).exists():
            technology = 'FIBRA'

        return technology

    @property
    def get_username(self):
        username = None

        technology = self.get_technology

        if technology == 'FIBRA':
            if self.onu_set.last().olt.node:
                username = (
                    self.onu_set.order_by('id').last().olt.node.
                    mikrotikconnection_set.order_by('id').last().username)

        elif technology == 'UTP':
            if self.cpeutp_set.last().nodo_utp:
                username = (
                    self.cpeutp_set.order_by('id').last().nodo_utp.
                    mikrotikconnection_set.order_by('id').last().username)

        return username

    @property
    def get_password(self):
        password = None
        technology = self.get_technology

        if technology == 'FIBRA':
            password = (self.onu_set.order_by('id').last().olt.node.
                        mikrotikconnection_set.order_by('id').last().password)

        elif technology == 'UTP':
            password = (self.cpeutp_set.order_by('id').last().nodo_utp.
                        mikrotikconnection_set.order_by('id').last().password)

        return password

    # @property
    def get_services_by_technology(self, technology):
        from devices.models import ONU
        from utp.models import CpeUtp

        if technology == 'utp':
            queryset = CpeUtp.objects.exclude(service=None)
        elif technology == 'onu':
            queryset = ONU.objects.exclude(service=None)

        return queryset

    @property
    def latest_network_changes(self):
        return self.networkstatuschange_set.order_by('-created_at')[:10]

    @property
    def get_original_node(self):
        try:
            original_node = self.history.order_by('-created').last().node
            return original_node
        except Exception as e:
            print(e)
            return None

    def network_action_log(self):
        return self.action_log(just=['node_id'])

    @property
    def latest_network_changes(self):
        return self.networkstatuschange_set.order_by('-created_at')[:10]

    def __str__(self):
        return str(self.number)


class Technician(User):
    """Clase para los técnicos de Nodo"""

    contact_number = models.CharField(max_length=16, blank=True, null=True)
    active = models.BooleanField(default=False)

    def __str__(self):
        return str(self.first_name.capitalize() + '' +
                   self.last_name.capitalize())

    @property
    def get_full_name(self):
        full_name = self.first_name.capitalize(
        ) + '' + self.last_name.capitalize()
        return full_name

    class Meta:
        verbose_name = _('Technician')
        verbose_name_plural = _('Technicians')


class TechnicianToClient(models.Model):

    technician = models.ForeignKey(Technician,
                                   verbose_name=_("technician"),
                                   on_delete=models.CASCADE)
    service = models.ForeignKey(Service,
                                verbose_name=_('service'),
                                on_delete=models.CASCADE)
    date = models.DateField(auto_now_add=True)

    onu = models.ForeignKey(ONU,
                            verbose_name=_("ONU"),
                            on_delete=models.CASCADE,
                            null=True,
                            blank=True)

    cpeutp = models.ForeignKey("utp.CpeUtp",
                               verbose_name=_("CpeUtp"),
                               on_delete=models.CASCADE,
                               null=True,
                               blank=True)

    class Meta:
        verbose_name = _('TechnicianToClient')


class NetworkStatusChange(models.Model):
    service = models.ForeignKey(Service,
                                verbose_name=_('service'),
                                on_delete=models.CASCADE)
    new_state = models.CharField(_('new state'), max_length=10)
    agent = models.CharField(verbose_name=_('agent'), max_length=50)
    created_at = models.DateTimeField(_('created at'), auto_now_add=True)

    class Meta:
        verbose_name = _('network status change')
        verbose_name_plural = _('network status changes')


class TypePLAN(BaseModel):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-name"]


class Plan(BaseModel):

    operator = models.ForeignKey("devices.Operator", on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    type_plan = models.ForeignKey(TypePLAN, on_delete=models.CASCADE)
    aggregation_rate = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(4096)], null=True, blank=True)
    matrix_plan = models.PositiveSmallIntegerField()

    #OLT
    download_speed = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(4096)], null=True, blank=True)
    upload_speed = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(4096)], null=True, blank=True)
    speeds_system_download = models.ForeignKey(
        "devices.SystemSpeeds",
        on_delete=models.CASCADE,
        related_name='speeds_system_plan_download',
        null=True,
        blank=True)
    speeds_system_upload = models.ForeignKey(
        "devices.SystemSpeeds",
        on_delete=models.CASCADE,
        related_name='speeds_system_plan_updoad',
        null=True,
        blank=True)

    # UTP
    profile_name = models.ForeignKey('utp.SystemProfile',
                                     on_delete=models.CASCADE,
                                     null=True,
                                     blank=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    class Meta:
        ordering = ["-matrix_plan"]


class PlanOLT(Plan):
    def clean_fields(self, exclude=None):
        super().clean_fields(exclude=exclude)
        if self.speeds_system_download is None:

            raise ValidationError(_('speeds_system_download es obligatorio'))
        elif self.speeds_system_upload is None:
            raise ValidationError(_('speeds_system_upload es obligatorio'))
        elif self.profile_name is not None:
            raise ValidationError(_('profile_name debe ser null'))

    class Meta:
        proxy = True
        ordering = ["-matrix_plan"]


class PlanUTP(Plan):
    def clean_fields(self, exclude=None):
        super().clean_fields(exclude=exclude)

        if self.speeds_system_download is not None:
            raise ValidationError(_('speeds_system_download debe ser null'))
        elif self.speeds_system_upload is not None:
            raise ValidationError(_('speeds_system_upload debe ser null'))
        elif self.profile_name is None:
            raise ValidationError(_('profile_name es obligatorio'))

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    class Meta:
        proxy = True
        ordering = ["-matrix_plan"]


class Enterprise(BaseModel):
    """
    Clase para las empresas provenientes de Pulso
    """
    country_id = models.IntegerField()
    operators = models.ManyToManyField(
        Operator,
        blank=True,
        verbose_name="Operadores",
        help_text="M2M relacion al modelo models.Operator")
    name = models.CharField(max_length=120)


class Slack(BaseModel):
    """
    clase que almacena la información de Slack con su empresa y Operador
    """
    enterprise = models.ForeignKey(
        Enterprise,
        blank=True,
        on_delete=models.CASCADE,
        verbose_name="Slack de Empresa *",
        help_text="FK, Enterprise"
    )
    operator = models.ManyToManyField(
        Operator,
        blank=True,
        verbose_name="Operadores",
        help_text="M2M, devices.Operator"
    )

    def __str__(self):
        return f"{self.name}"

    class Meta:
        ordering = ["-id"]
        verbose_name = "Slacks"
        verbose_name_plural = "Información de los Slack"


class UserProfile(BaseModel):
    """
    Clase que almacena las relaciones entre usuarios, empresas, 
    operadores y Slack
    """
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name="Usuario*",
        help_text="FK, relación AUTH.User"
    )
    enterprises = models.ManyToManyField(
        Enterprise,
        blank=True,
        verbose_name="Empresa *",
        help_text="M2M, Enterprise"
    )
    operators = models.ManyToManyField(
        Operator,
        blank=True,
        verbose_name="Operadores",
        help_text="M2M, devices.Operator"
    )
