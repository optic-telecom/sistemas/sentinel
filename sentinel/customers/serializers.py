from rest_framework import serializers
from rest_framework.serializers import HyperlinkedModelSerializer
from common.serializers import QueryFieldsMixin

from .models import TypePLAN, Plan, Service, NetworkStatusChange, Technician, TechnicianToClient, Enterprise

class SysLogSelect2Sz(serializers.Serializer):
    id = serializers.CharField(max_length=30)
    text = serializers.CharField(max_length=255)


    

class TypePLANSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    id = serializers.IntegerField(required=False, min_value=1)
    
    class Meta:
        model = TypePLAN
        fields = ('id', 'name',)


    
class PlanSerializer(QueryFieldsMixin, serializers.ModelSerializer):
    id = serializers.IntegerField(min_value=1)
    
    class Meta:
        model = Plan
        fields = ('id','operator','name','type_plan','download_speed',
                  'upload_speed','aggregation_rate','speeds_system_download',
                  'speeds_system_upload','matrix_plan', 'profile_name')  


from utp.models import Mikrotik
from utp.serializers import MikrotikSerializer
class ServiceSerializer(QueryFieldsMixin, serializers.ModelSerializer):
    # from utp.serializers import MikrotikBaseSerializer
    
    id = serializers.IntegerField(required=False, min_value=1)
    # node = MikrotikBaseSerializer(required=False)
    # node_mismatch = MikrotikBaseSerializer(required=False)
    node = MikrotikSerializer(many=False)
    # node_mismatch = serializers.IntegerField(required=False, min_value=1)

    class Meta:
        model = Service
        fields = ('id', 'number', 'node', 'seen_connected', 'network_mismatch', 'node_mismatch')

class ServiceMigrateSerializer(QueryFieldsMixin, serializers.ModelSerializer):
    # from utp.serializers import MikrotikBaseSerializer
    id = serializers.IntegerField(required=False, min_value=1)
    class Meta:
        model = Service
        fields = ('id', 'number', 'node', 'seen_connected', 'network_mismatch', 'node_mismatch')


class NetworkStatusChangeSerializer(QueryFieldsMixin, serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, min_value=1)
    created_at = serializers.DateTimeField(required=False)

    def create(self, validated_data):
        obj = NetworkStatusChange.objects.create(**validated_data)
    
        if validated_data['created_at']:
            obj.created_at = validated_data['created_at'] 
            obj.save()
    
        return obj

    class Meta:
        model = NetworkStatusChange
        fields = ('id', 'service', 'new_state', 'agent', 'created_at')

class TechnicianSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(min_value=1)

    class Meta:
        model = Technician
        fields = ('id','first_name', 'last_name', 'username', 'contact_number', 'active')


class TechnicianToClientSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(min_value=1)

    class Meta:
        model = TechnicianToClient
        fields = ('id', 'technician', 'service', 'date', 'onu', 'cpeutp')


class ServiceFilterSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, min_value=1)

    class Meta:
        model = Service
        fields = ('id',)


class EnterpriseSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(min_value=1)
    country_id = serializers.IntegerField(min_value=1, required=True)
    name = serializers.CharField(required=True)

    class Meta:
        model = Enterprise
        fields = ('id', 'country_id', 'operators', 'name')
