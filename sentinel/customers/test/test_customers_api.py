import pytest
from datetime import datetime


@pytest.mark.django_db
class TestCustomersApis():
    def test_create_type_plan(self, url, client, headers, fail):

        pass_payload = {
            'id': "1",
            'name': 'test_plan'
        }

        fail_payload = {
            'id': "aaaa",
            'name': 'test_plan'
        }

        request = client.post("{}/type_plan/".format(url),
                              data=pass_payload if not fail else fail_payload,
                              headers=headers, verify=False)
        print(request.json())
        assert request.status_code == 201

    def test_create_plan(self, url, client, headers, test_operator,
                         test_type_plan, fail):

        pass_payload = {
                "id": "1",
                "operator": test_operator.id,
                "name": 'testplan',
                "matrix_plan": '1',
                "type_plan": test_type_plan.id,
            }

        fail_payload = {
                "id": "aaaa",
                "operator": 23,
                "name": 'testplan',
                "matrix_plan": '1',
                "type_plan": 23,
            }

        request = client.post("{}/plan/".format(url),
                              data=pass_payload if not fail else fail_payload,
                              headers=headers, verify=False)
        print(request.json())
        assert request.status_code == 201

    def test_create_service(self, url, client, headers, fail, test_node):
        pass_payload = {
            'id': '1',
            'number': '1',
            'seen_connected': True,
            'network_mismatch': True,
            'node_mismatch': test_node.id,
            'node': test_node.id
            }

        fail_payload = {
            'id': 'aaaaa',
            'number': '1',
            'seen_connected': None,
            'network_mismatch': True,
            'node_mismatch': None,
            'node': test_node.id
            }

        request = client.post("{}/service-migrate/".format(url),
                              data=pass_payload if not fail else fail_payload,
                              headers=headers, verify=False)
        print(request.json())
        assert request.status_code == 201

    def test_network_status_change(self, url, client, headers, fail,
                                   test_service):
        pass_payload = {
                'service': test_service.id,
                'new_state': 'test',
                'agent': 'test',
                'created_at': datetime.today()
            }

        fail_payload = {
                'service': 'aaaa',
                'new_state': 'test',
                'agent': 'test',
                'created_at': datetime.today()
            }


        request = client.post("{}/networkstatuschange/".format(url),
                              data=pass_payload if not fail else fail_payload,
                              headers=headers, verify=False)
        print(request.json())
        assert request.status_code == 201