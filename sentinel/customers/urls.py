from rest_framework.routers import DefaultRouter
from .views import *
from django.urls import path
# from .logfile.syslog_search import SearchSysLog

from django.urls import re_path

router = DefaultRouter()

router.register(r'type_plan', TypePLANViewSet, 'type_plan_api')
router.register(r'plan', PlanViewSet, 'plan_api')
router.register(r'service', ServiceViewSet, 'service_api')
#router.register(r'service-filter', ServiceFilter, 'service_api')
router.register(r'service-migrate', ServiceMigrateViewSet, 'service_migrate_api')
router.register(r'enterprise', EnterpriseViewSet, 'enterprise_api')

router.register(r'networkstatuschange', NetworkStatusChangeViewSet, 'network_change_api')
router.register(r'technician', TechnicianViewset, 'technician_api')

urlpatterns = [
    path('api/v1/s2-syslog/lt',SelectSyslog.as_view(selector='log_type'),name="select_log_type"),
    path('api/v1/s2-syslog/user',SelectSyslog.as_view(selector='username'),name="select_username"),
    path('api/v1/s2-syslog/ip',SelectSyslog.as_view(selector='user_ip'),name="select_user_ip"),
    path('api/v1/s2-syslog/origin',SelectSyslog.as_view(selector='origin'),name="select_origin"),
    path('api/v1/s2-syslog/identificador',SelectSyslog.as_view(selector='identificador'),name="select_identificador"),
    path('api/v1/s2-syslog/created',SelectSyslog.as_view(selector='created'),name="select_created"),
        
    # path('api/v1/syslog-search', SearchSysLog.as_view(),name="search")

    re_path(r'^services', ServiceList.as_view()),
    re_path(r'^service-filter', ServiceFilter.as_view()),
    
    re_path(r'^services/(?P<number>[0-9]+)', ServiceRetrieve.as_view()),

]

