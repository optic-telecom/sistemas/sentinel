from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet 
from rest_framework.views import APIView
from rest_framework import viewsets, status
from rest_framework.generics import RetrieveAPIView, ListAPIView
from rest_framework.decorators import action
# Create your views here.
from .serializers import SysLogSelect2Sz, TypePLANSerializer, PlanSerializer,\
                         ServiceSerializer, NetworkStatusChangeSerializer, \
                         ServiceMigrateSerializer, ServiceFilterSerializer, \
                         TechnicianSerializer, EnterpriseSerializer
from .models import  Plan, TypePLAN, Service, NetworkStatusChange, Technician, \
                     Enterprise
from syslog_app.models import SysLog
from .views_helper import *
from rest_framework.decorators import api_view, permission_classes


class SelectSyslog(APIView):
    """
    {} Esta api retorna un objeto para ser utilizado por los
    selectores del datatable de la sección syslog en los detalles 
    de las OLT
    """
    selector = None
    
    def get(self,request,format=None):
        
        syslog = SysLog.objects.all()
        
        search = list()
        try:
            for x in syslog:
                search.append(getattr(x,self.selector))
            
            listado = set(search)
            
            ORIGEN_LIST = ((0,'NO ENCONTRADO'),
               (1,'ONU'),
               (2,'OLT'),
               (3,'MIKROTIK'),
               (4,'CPE-UTP'),
               (5,'RADIUS'),)
            
            
            if self.selector == 'origin':
                output = [{'id':x,'text':ORIGEN_LIST[x][1]} for x in listado]
            elif self.selector != 'user_ip' and self.selector != 'created':
                output = [{'id':x,'text':x.lower()} for x in listado]
            else:
                if self.selector == 'created':
                    output = [{'id':x.strftime("%d-%m-%Y"),'text':x.strftime("%d-%m-%Y")} for x in listado]
                else:
                    output = [{'id':x,'text':x} for x in listado]
            
            serializer = SysLogSelect2Sz(output,many=True).data
            status = 200
        except Exception as e:
            serializer = list()
            status = 404
        

        return Response(serializer,status=status)


class LoopForModels(APIView):
    """
    
    """
    MODEL_LIST = ['ONU','Mikrotik','Radius']



class TypePLANViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for
    retrieve:
        End point for
    update:
        End point for
    delete:
        End point for
    """
    queryset = TypePLAN.objects.all()
    serializer_class = TypePLANSerializer   
    

class PlanViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Plan
    retrieve:
        End point for Plan
    update:
        End point for Plan
    delete:
        End point for Plan
    """
    queryset = Plan.objects.all()
    serializer_class = PlanSerializer

    def get_queryset(self):
        qs = super().get_queryset()
        with_system_speed = self.request.GET.get('with_system_speed', False)
        if with_system_speed:
            qs = qs.filter(speeds_system_download__isnull=False, speeds_system_upload__isnull=False)
        return qs

    @action(detail=True, methods=['post'])
    def add_history(self, request, version=None, pk=None):

        try:
            plan = Plan.objects.get(pk = pk)
        except ValueError:
            return error_response(f"El plan con id = {pk} no fue encontrado")
        
        return add_plan_history(plan, request.data)
    
class ServiceViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Service
    retrieve:
        End point for Service
    update:
        End point for Service
    delete:
        End point for Service
    """
    queryset = Service.objects.all()
    lookup_field = 'number'
    serializer_class = ServiceSerializer

    @action(detail=True, methods=['post'])
    def add_history(self, request, version=None, number=None):

        try:
            service = Service.objects.get(pk = number)
        except ValueError:
            return error_response(f"El servicio con id = {number} no fue encontrado")
        
        return add_service_history(service, request.data)

    @action(detail=True, methods=['get'])
    def network(self, request, version=None, number=None):
        """ Obtiene la informacion de red de un servicio.

            Devuelve un Response con un diccionario con la informacion de la red:
            
                {
                    service_number: integer
                    node_uuid: uuid
                    primary_equipment: cpeutp  
                    network_equipments: [cpeutp] 
                    cutting_node_name: name
                    cutting_node_url: url
                    technology_type
                    instalation_notes
                }

            Parámetros:
            pk -- service number en Matrix

            Excepciones:
            ValueError -- Si (number != pk)
            
        """

        try:
            service = Service.objects.get(number = number)
        except Exception as e:
            return error_response(f"El servicio con numero = {number} no fue encontrado.", status=status.HTTP_404_NOT_FOUND)
        
        return getNetworkInfo(service, request.data)


    @action(detail=True, methods=['post'])
    def primary_equipment(self, request, version=None, number=None):
        if number is None:
            return error_response(
                "El número de servicio no está definido", 
                status=status.HTTP_400_BAD_REQUEST
            )

        try:
            service = Service.objects.get(number=number)
        except Exception as e:
            return error_response(
                "El servicio no fue encontrado.", 
                status=status.HTTP_404_NOT_FOUND
            )
        
        # Asociamos el equipo primario
        # Tipo de equipo a buscar
        device_type = request.data.get('device_type')

        # IP del equipo a buscar
        device_ip = request.data.get('device_ip')

        if device_ip is None or device_ip == "":
            return error_response(
                "La ip del equipo no puede estar vacía",
                status=status.HTTP_400_BAD_REQUEST
            )

        if device_type == "UTP":
            cpe = service.cpeutp_set.get(ip__ip=device_ip)

            # Quitamos el primary de todos los demás equipos
            for device in service.cpeutp_set.all():
                device.primary = False
                device.save()

            # Ponemos el primary en el equipo actual
            cpe.primary = True
            cpe.save()
            return Response(
                {'success': 'El equipo primario fue actualizado correctamente'}
            )

        elif device_type == "GPON":
            onu = service.onu_set.get(ip__ip=device_ip)

            # Quitamos el primary de todos los demás equipos
            for device in service.onu_set(manager='all_objects').all():
                device.primary = False
                device.save()

            # Ponemos el primary en el equipo actual
            onu.primary = True
            onu.save()

            return Response(
                {'success': 'El equipo primario fue actualizado correctamente'}
            )
        else:
            return error_response(
                "El tipo de tecnología debe ser UTP o GPON", 
                status=status.HTTP_400_BAD_REQUEST
            )




class ServiceMigrateViewSet(viewsets.ModelViewSet):
    queryset = Service.objects.all()
    serializer_class = ServiceMigrateSerializer


class NetworkStatusChangeViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for NetworkStatusChange
    retrieve:
        End point for NetworkStatusChange
    update:
        End point for NetworkStatusChange
    delete:
        End point for NetworkStatusChange
    """
    queryset = NetworkStatusChange.objects.all()
    serializer_class = NetworkStatusChangeSerializer


class ServiceList(ListAPIView):
    queryset = Service.objects.all()
    serializer_class = ServiceSerializer
    # permission_classes = [IsAdminUser]

    def list(self, request, version=None):
        # Note the use of `get_queryset()` instead of `self.queryset`
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = ServiceSerilizer(queryset, many=True)
        return Response(serializer.data)

class ServiceRetrieve(RetrieveAPIView):
    queryset = None
    serializer_class = ServiceSerializer

    
    def retrieve(self, request, version=None, number=None):
        print('*****  ', version, number, '*****')
        try:
            service = Service.objects.values('seen_connected', 'network_mismatch', 'number').get(number=int(number))
        except ValueError:
            response = Response(
                {"detail": "Numero de servicio inválido"},
                content_type="application/json",
                status=status.HTTP_400_BAD_REQUEST,
            )
            response.accepted_renderer = JSONRenderer()
            response.accepted_media_type = "application/json"
            response.renderer_context = {}
            return response

        serializer = ServiceSerializer(service, many=False)

        return Response(serializer.data)        


class TechnicianViewset(viewsets.ModelViewSet):
    """
    create:
        End point for Technician
    retrieve:
        End point for Technician
    update:
        End point for Technician
    delete:
        End point for Technician
    """
    serializer_class = TechnicianSerializer
    #queryset = Technician.objects.all()

    def get_queryset(self):
        queryset = Technician.objects.all()

        # obtiene los datos del get request
        data = self.request.query_params

        # chequea si hay filtros para así modificar el queryset
        if data.get('filter'):
            flt = data.get('filter')
            # filtra por estatus activo o no activo
            if flt == 'active':
                queryset = Technician.objects.filter(active=True)
            elif flt == 'not_active':
                queryset = Technician.objects.filter(active=False)

            # filtra por el id del técnico, formato "id_numeroid"
            elif 'id_' in flt:
                flt = flt.split('_')
                queryset = Technician.objects.filter(id=flt[1])

            # filtra por el nombre del técnico, formato "name_Nombre_Apellido"
            elif 'name_' in flt:
                flt = flt.split('_')
                queryset = Technician.objects.filter(first_name=flt[1], last_name=flt[2])

        return queryset


class ServiceFilter(ListAPIView):
    """
    API para devolver una lista de servicios dependiendo del filtro
    utilizado.
    """
    queryset = Service.objects.all()
    serializer = ServiceFilterSerializer(queryset, many=True)
    # permission_classes = [IsAdminUser]

    def list(self, request, version=None):
        # Note the use of `get_qSeueryset()` instead of `self.queryset`
        queryset = self.get_queryset()
        
        
        #recibimos el diccionario con la información
        data = request.data
        print('===========')
        print('Esta es la data,', data)
        print('===========')

        # Buscamos la lista de filtros
        filter_list = data['filters']

        # Creamos las listas
        tk_list = []
        nc_list = []

        for _filter in filter_list:
            filter_name = _filter[0]
            filter_comparison = _filter[1]
            value = _filter[2]

            # hacemos el filtro por techgnology_kind
            if 'technology_kind' in filter_name:

                if filter_comparison == "equal":
                    tk_list = [service.id for service in queryset if
                           service.get_technology == value]
                if filter_comparison == "different":
                    tk_list = [service.id for service in queryset if
                           service.get_technology == value]

                #return Response({'result': res})

            # hacemos el filtro por node__code (mikrotik.alias)
            if 'node__code' in filter_name:
                if filter_comparison == "different":
                    # lista res para almacenar los service.id
                    nc_list = []
                    for service in queryset:
                        # agregamos try-except porque hay servicios sin nodo
                        try:
                            if service.node.alias != value and \
                               service.node.alias not in nc_list:
                                nc_list.append(service.id)
                        except Exception as e:
                            pass

                if filter_comparison == "equal":
                    # lista nc_list para almacenar los service.id
                    nc_list = []
                    for service in queryset:
                        # agregamos try-except porque hay servicios sin nodo
                        try:
                            if service.node.alias == value and \
                               service.node.alias not in nc_list:
                                nc_list.append(service.id)
                        except Exception as e:
                            pass

                if filter_comparison == "iexact":
                    # lista nc_list para almacenar los service.id
                    nc_list = []
                    for service in queryset:
                        # agregamos try-except porque hay servicios sin nodo
                        try:
                            if service.node.alias == value and \
                               service.node.alias not in nc_list:
                                nc_list.append(service.id)
                        except Exception as e:
                            pass

                if filter_comparison == "icontains":
                    # lista nc_list para almacenar los service.id
                    nc_list = []
                    for service in queryset:
                        # agregamos try-except porque hay servicios sin nodo
                        try:
                            if value in service.node.alias and \
                               service.node.alias not in nc_list:
                                nc_list.append(service.id)
                        except Exception as e:
                            pass

                if filter_comparison == "istartswith":
                    # lista nc_list para almacenar los service.id
                    nc_list = []
                    for service in queryset:
                        # agregamos try-except porque hay servicios sin nodo
                        try:
                            if service.node.alias.startswith(value) and \
                               service.node.alias not in nc_list:
                                nc_list.append(service.id)
                        except Exception as e:
                            pass

                if filter_comparison == "iendswith":
                    # lista nc_list para almacenar los service.id
                    nc_list = []
                    for service in queryset:
                        # agregamos try-except porque hay servicios sin nodo
                        try:
                            if service.node.alias.endswith(value) and \
                               service.node.alias not in nc_list:
                                nc_list.append(service.id)
                        except Exception as e:
                            pass

                #return Response({'result': res})

        criterion = data['criterion']

        if criterion == 'C':
            unique = list(set(tk_list + nc_list))
            return Response({'result': unique})
        else:
            concat = tk_list + nc_list
            return Response({'result': concat})


class EnterpriseViewSet(viewsets.ModelViewSet):

    serializer_class = EnterpriseSerializer
    queryset = Enterprise.objects.all()
