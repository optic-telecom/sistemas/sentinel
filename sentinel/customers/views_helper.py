



from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework import status
from .serializers import NetworkStatusChangeSerializer

def error_response(error, status=status.HTTP_400_BAD_REQUEST):
    
    response = Response(
        {"detail": f"{str(error)}"},
        content_type="application/json",
        status=status,
    )
    response.accepted_renderer = JSONRenderer()
    response.accepted_media_type = "application/json"
    response.renderer_context = {}
    return response


def add_plan_history(plan, data):
    try:
        plan.history.create(
            id= data['id'],
            created= data['created'],
            modified= data['created'],
            name= data['name'],
            history_date= data['history_date'],
            history_type= data['history_type'],
            operator_id= data['operator_id'] if data.get('operator_id', None) else plan.operator.id,
            matrix_plan= plan.matrix_plan,
            type_plan_id= data['type_plan_id'],
        )
    except Exception as e :
        print(e)
        return error_response(e)

    return Response({'ok': True}, status=status.HTTP_200_OK)


def add_service_history(service, data):
    try:
        service.history.create(
            id= data['id'],
            created= data['created'],
            modified= data['created'],
            history_date= data['history_date'],
            history_type= data['history_type'],
            # history_id= data['history_id'],
            number= data['number'],
            seen_connected = data.get('seen_connected', None)  ,
            network_mismatch = data.get('network_mismatch', None),
            node_id = data.get('node_id', None),
            node_mismatch_id = data.get('node_mismatch_id', None)
        )
    except Exception as e :
        print(e)
        return error_response(e)

    return Response({'ok': True}, status=status.HTTP_200_OK)


def getNetworkInfo(service, data):
    cutting_node = f'{service.node.address} ({service.node.alias})' if service.node is not None else None
    network_mismatch = service.network_mismatch
    node_mismatch =  f'{service.node_mismatch.address} ({service.node_mismatch.alias})' if service.node_mismatch else None
    
    if service.get_original_node is not None:
        instalation_node = f'{service.get_original_node.address} ({service.get_original_node.alias})'
    elif service.node:
        instalation_node = f'{service.node.address} ({service.node.alias})'
    else:
        instalation_node = f'Sin nodo'
    
    primary_cpe = service.cpeutp_set.filter(primary=True)\
        .values('ip__ip', 'model__model', 'model__brand', 'mac__mac',
                'description', 'ssid', 'ssid_5g', 'password')\
        .first()

    # try/except to avoid NoneType error
    try:
        #adding last_history_ip property
        primary_cpe['last_history_ip'] = service.cpeutp_set.filter(primary=True)\
            .first().last_history_ip
        print('done_cpe')
    except Exception as e:
        pass


    primary_onu = service.onu_set(manager='all_objects').filter(primary=True)\
        .values('ip__ip', 'model__model', 'model__brand', 'mac__mac',
                'description', 'ssid', 'ssid_5g', 'password')\
        .first()
    
    # try/except to avoid NoneType error
    try:
        #adding last_history_ip property
        primary_onu['last_history_ip'] = service.onu_set(manager='all_objects').filter(primary=True)\
            .first().last_history_ip
        print('done_onu')
    except Exception as e:
        pass

    # primary_cpe = cpes.filter(primary=True).first()
    
    kind_technology = ''

    if primary_cpe:
        primary_equipment = {
            'ip': primary_cpe['ip__ip'] if primary_cpe['ip__ip'] else 'Desconocida',
            'brand': primary_cpe['model__brand'] if primary_cpe['model__brand'] else 'Desconocida',
            'model': primary_cpe['model__model'] if primary_cpe['model__model'] else 'Desconocida',
            'mac': primary_cpe['mac__mac'] if primary_cpe['mac__mac'] else 'Desconocida',
            'notes': primary_cpe['description'] if primary_cpe['description'] else 'Ninguna',
            'ssid': primary_cpe['ssid'] if primary_cpe['ssid'] else 'Sin Asignar',
            'ssid_5g': primary_cpe['ssid_5g'] if primary_cpe['ssid_5g'] else 'Sin Asignar',
            'password': primary_cpe['password'] if primary_cpe['password'] else 'Sin Asignar',
            'last_history_ip': str(primary_cpe['last_history_ip'].ip) if 'last_history_ip' in primary_cpe else 'Desconocida',
        }
        
        kind_technology = 'UTP'
    elif primary_onu:
        primary_equipment = {
            'ip': primary_onu['ip__ip'] if primary_onu['ip__ip'] else 'Desconocida',
            'brand': primary_onu['model__brand'] if primary_onu['model__brand'] else 'Desconocida',
            'model': primary_onu['model__model'] if primary_onu['model__model'] else 'Desconocida',
            'mac': primary_onu['mac__mac'] if primary_onu['mac__mac'] else 'Desconocida',
            'notes': primary_onu['description'] if primary_onu['description'] else 'Ninguna',
            'ssid': primary_onu['ssid'] if primary_onu['ssid'] else 'Sin Asignar',
            'ssid_5g': primary_onu['ssid_5g'] if primary_onu['ssid_5g'] else 'Sin Asignar',
            'password': primary_onu['password'] if primary_onu['password'] else 'Sin Asignar',
            'last_history_ip': str(primary_onu['last_history_ip'].ip) if 'last_history_ip' in primary_onu else 'Desconocida',
        }
        
        #kind_technology = 'GPON | GEPON'
        kind_technology = 'GPON'
    else:
        primary_equipment = None



    cpes = service.cpeutp_set.all()\
           .values('ip__ip', 'model__model', 'model__brand', 'mac__mac',
                   'description', 'ssid', 'ssid_5g', 'password', 'serial', 'id')

    onus = service.onu_set(manager='all_objects').all()\
           .values('ip__ip', 'model__model', 'model__brand', 'mac__mac',
                   'description', 'ssid', 'ssid_5g', 'password', 'serial', 'id')

    n_e = list(cpes) + list(onus)    

    network_equipments = []
    for cpe in n_e:
        # try/except to avoid NoneType error
        try:
            #adding last_history_ip property
            cpe['last_history_ip'] = service.cpeutp_set\
                                            .filter(id=cpe['id'])\
                                            .last_history_ip
        except Exception as e:
            pass
        network_equipments.append({
            'ip': cpe['ip__ip'] if cpe['ip__ip'] else 'Desconocida',
            'brand': cpe['model__brand'] if cpe['model__brand'] else 'Desconocida',
            'model': cpe['model__model'] if cpe['model__model'] else 'Desconocida',
            'mac': cpe['mac__mac'] if cpe['mac__mac'] else 'Desconocida',
            'notes': cpe['description'] if cpe['description'] else 'Ninguna',
            'ssid': cpe['ssid'] if cpe['ssid'] else 'Sin Asignar',
            'ssid_5g': cpe['ssid_5g'] if cpe['ssid_5g'] else 'Sin Asignar',
            'password': cpe['password'] if cpe['password'] else 'Sin Asignar',
            'serial_number': cpe['serial'] if cpe['serial'] else 'Sin Serial',
            'last_history_ip': str(cpe['last_history_ip'].ip) if 'last_history_ip' in cpe else 'Desconocida'
        })

    network_action_log = service.network_action_log()
    
    networkStatusChangeSerializer = NetworkStatusChangeSerializer(service.latest_network_changes, many=True)
    # print('**************')
    # print()
    latest_network_changes = networkStatusChangeSerializer.data
    
    obj = {
        'cutting_node': cutting_node,
        'network_mismatch': network_mismatch,
        'node_mismatch': node_mismatch,
        'instalation_node': instalation_node,
        'primary_equipment': primary_equipment,
        'network_equipments': network_equipments,
        'network_action_log': network_action_log,
        'latest_network_changes': latest_network_changes,
        'kind_technology': kind_technology
    }

    return Response(obj, status=status.HTTP_200_OK)
    