from django.contrib import admin
from common.admin import BaseAdmin
from .models import Feedback, TaskModel, LogSentinel, TypeAction, Entity,\
    Variable


@admin.register(Feedback)
class FeedbackAdmin(BaseAdmin):
    list_display = ('platform', 'user_agent', 'commentary', )
    list_filter = ('platform',) 
    search_fields = ['platform', 'user_agent', 'commentary']


@admin.register(LogSentinel)
class LogSentineladmin(BaseAdmin):
    list_display = ('created', 'action', 'by_representation',
                    'entity_representation','id_value','description','level')

    list_filter = ('action','entity')
    search_fields = ['id_value']

@admin.register(TaskModel)
class TaskModelAdmin(BaseAdmin):
    list_display = ('name', 'id_obj', 'model_obj','status','time','transcurrido')
    list_filter = ('model_obj','status')
    
    def time(self, obj):
        try:
            return obj.date_done - obj.created
        except Exception as e:
            return None

    def transcurrido(self, obj):
        from django.utils import timezone
        return timezone.now() - obj.created 


@admin.register(TypeAction)
class TypeActionAdmin(BaseAdmin):
    list_display = ('created', 'type_action',)


@admin.register(Entity)
class EntityAdmin(BaseAdmin):
    list_display = ('created', 'entity',)


@admin.register(Variable)
class VariableAdmin(BaseAdmin):
    list_display = ('nombre', 'valor', 'namespace', 'active')
    list_filter = ('namespace',)
    list_editable = ('active', 'valor')    