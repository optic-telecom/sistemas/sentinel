from django.db.models import Q
from common.mixins import DataTable
from .models import LogSentinel


class DatatableLOG(DataTable):
    model = LogSentinel
    columns = ['created', 'by_representation', 'entity', 'entity_representation',
               'id_value', 'action', 'description']
    order_columns = columns
    max_display_length = 1000

    def display_created(self, obj):
        return obj.created.strftime("%d-%m-%Y %H:%M:%S")

    def get_initial_queryset(self):
        try:
            #qs = self.model.objects.none()
           
            get_onu = False
            get_olt = False
            get_port = False
            get_vlans = False
            get_interfaces = False

            filter_type_log = self._querydict.get('filter_type_log', None)
            filter_type_action = self._querydict.get('filter_type_action', None)            
            if filter_type_action:
                try:
                    int(filter_type_action)
                except Exception as e:
                    filter_type_action = None

            if filter_type_log:
                if filter_type_log == 'ONU':
                    get_onu = True
                elif filter_type_log == 'INTERFACE':
                    get_interfaces = True
                elif filter_type_log == 'PORTOLT':
                    get_port = True
                elif filter_type_log == 'OLTVLAN':
                    get_vlans = True
                elif filter_type_log == 'OLT':
                    get_olt = True                    
                elif filter_type_log == 'Tipo':
                    get_onu = True
                    get_port = True
                    get_vlans = True
                    get_interfaces = True
                    get_olt = True
            else:
                get_onu = True
                get_port = True
                get_vlans = True
                get_interfaces = True
                get_olt = True

            qs = self.model.objects.filter(id_value='none_field')


            if get_olt and filter_type_action:
                qs =  self.model.objects.filter(id_value=self.uuid,action__type_action=filter_type_action)
            elif get_olt:
                qs = self.model.objects.filter(id_value=self.uuid)

            nqs = None

            # if filter_type_action is None and filter_type_log == 'Tipo':
            #     print ('adfgfdd')
            #     return self.model.objects.filter()

            # if get_olt:
            #     nqs = self.model.objects.filter(
            #             Q(id_value=self.pk, id_field='id')
            #     )

            if get_onu:
                from devices.models import ONU
                pks = ONU.objects.filter(status_field=True,olt__uuid=self.uuid).values_list('id', flat=True)
                nqs = self.model.objects.filter(
                        Q(id_value__in=list(pks), entity__entity='ONU')#id_field='id', 
                     )

            if get_port:
                from devices.models import PortOLT
                pks = PortOLT.objects.filter(status_field=True, interface__olt__uuid=self.uuid).values_list('id', flat=True)
                nqs = self.model.objects.filter(
                        Q(id_value__in=list(pks), entity__entity='PORTOLT')#id_field='id', 
                     )

            if get_vlans:
                from devices.models import OLTVLAN
                pks = OLTVLAN.objects.filter(status_field=True, olt__uuid=self.uuid).values_list('id', flat=True)
                nqs = self.model.objects.filter(
                        Q(id_value__in=list(pks), entity__entity='OLTVLAN')#id_field='id', 
                     )

            if get_interfaces:
                from devices.models import InterfaceOLT
                pks = InterfaceOLT.objects.filter(status_field=True, olt__uuid=self.uuid).values_list('id', flat=True)
                nqs = self.model.objects.filter(
                        Q(id_value__in=list(pks), entity__entity='INTERFACE')#id_field='id', 
                     )

            if nqs and filter_type_action:
                nqs = nqs.filter(action__type_action=filter_type_action)
                #

            try:
                qs = qs | nqs
            except Exception as e:
                pass
            return qs

        except Exception as e:
            print (e)
            qs = self.model.objects.filter(status_field=True)
            return qs 

    def filter_queryset(self, qs):

        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = qs.filter(
                Q(description__icontains=search) |
                Q(entity_representation__icontains=search)
            )

        #qs = self.get_initial_queryset()
        return qs



