# Generated by Django 2.1.3 on 2020-04-27 10:01

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entity',
            name='id_data',
            field=models.UUIDField(blank=True, db_index=True, editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='feedback',
            name='id_data',
            field=models.UUIDField(blank=True, db_index=True, editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='historicalfeedback',
            name='id_data',
            field=models.UUIDField(blank=True, db_index=True, editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='historicaltaskmodel',
            name='id_data',
            field=models.UUIDField(blank=True, db_index=True, editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='historicaltaskmodel',
            name='uuid',
            field=models.UUIDField(db_index=True, default=uuid.uuid4, editable=False),
        ),
        migrations.AlterField(
            model_name='logsentinel',
            name='id_data',
            field=models.UUIDField(blank=True, db_index=True, editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='taskmodel',
            name='id_data',
            field=models.UUIDField(blank=True, db_index=True, editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='taskmodel',
            name='uuid',
            field=models.UUIDField(db_index=True, default=uuid.uuid4, editable=False),
        ),
        migrations.AlterField(
            model_name='typeaction',
            name='id_data',
            field=models.UUIDField(blank=True, db_index=True, editable=False, null=True),
        ),
    ]
