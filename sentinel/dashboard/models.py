# -*- coding: utf-8 -*-
import uuid
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.contrib.postgres.fields import JSONField
from common.models import BaseModel, BaseModelWithoutHistory
from celery import states
from .actions import ACTION_CHOICES, LEVEL_CHOICES


class Feedback(BaseModel):
    platform = models.CharField(max_length=10)
    user_agent = models.CharField(max_length=200)
    cookie_enabled = models.BooleanField(default=True)
    version_sentinel = models.CharField(max_length=5)
    width = models.CharField(max_length=5)
    height = models.CharField(max_length=5)
    url = models.CharField(max_length=200)
    commentary = models.CharField(max_length=200) 
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL,
                                   on_delete=models.DO_NOTHING)

    def __str__(self):
        return ('%s - %s') % (self.commentary, self.user_agent)

    class Meta:
        ordering = ["id"]



ALL_STATES = sorted(states.ALL_STATES)
TASK_STATE_CHOICES = sorted(zip(ALL_STATES, ALL_STATES))

class TaskModel(BaseModel):
    uuid = models.UUIDField(db_index=True,
                            default=uuid.uuid4,
                            editable=settings.DEBUG)    
    name = models.CharField(max_length=180)
    id_obj = models.PositiveIntegerField(default=0)
    model_obj = models.CharField(max_length=180)
    status = models.CharField(_('state'), max_length=50,
                              default=states.PENDING, choices=TASK_STATE_CHOICES)
    #result = JSONField(null=True, default=None, editable=False)
    result = models.BooleanField(default=False)
    date_done = models.DateTimeField(_('done at'), blank=True, null=True,
                                     auto_now=True)
    traceback = models.TextField(_('traceback'), blank=True, null=True)


    def __str__(self):
        return self.name

    class Meta:
        ordering = ["id"]



class TypeAction(BaseModelWithoutHistory):

    type_action = models.PositiveIntegerField(default=0, choices=ACTION_CHOICES)

    def __str__(self):
        return ACTION_CHOICES[self.type_action-1][1]
        return str(self.type_action)
        

class Entity(BaseModelWithoutHistory):

    entity = models.CharField(max_length=35) 

    def __str__(self):
        return self.entity


class LogSentinel(BaseModelWithoutHistory):
    created = models.DateTimeField(auto_now_add = True, editable=True, db_index=True)
    action = models.ForeignKey(TypeAction, on_delete=models.DO_NOTHING)
    by = models.ForeignKey(settings.AUTH_USER_MODEL,
                            on_delete=models.DO_NOTHING,
                            null=True, blank=True)
    by_representation = models.CharField(max_length=135) 
    entity = models.ForeignKey(Entity,on_delete=models.DO_NOTHING)
    entity_representation = models.CharField(max_length=135)
    id_field = models.CharField(max_length=135, default='id')
    id_value = models.CharField(max_length=80, db_index=True)
    description = models.TextField()
    level = models.PositiveIntegerField(default=20, choices=LEVEL_CHOICES)

    def __str__(self):
        return ('%s - %s') % (self.entity_representation, self.description)

    class Meta:
        ordering = ["id"]



class Variable(models.Model):

    """ Para utilizar en simulaciones y algunas configuraciones"""
    created = models.DateTimeField(auto_now_add = True)
    nombre = models.CharField(max_length=135)
    valor = models.TextField()
    namespace = models.CharField(max_length=135)
    active = models.BooleanField(default = True)