from django.core import exceptions
from django.conf import settings

from rest_framework import serializers
from rest_framework.serializers import HyperlinkedModelSerializer
from rest_framework.utils import model_meta

from devices.models import OLT, ONU
from .models import Feedback, TaskModel, Entity
from utp.models import Mikrotik
from .utils import slack_feedback


class DasboardSerializer(HyperlinkedModelSerializer):

    devices_online = serializers.SerializerMethodField()

    def get_devices_online(self, obj):
        total = 0
        total_devices = 0
        olts = OLT.objects.filter(node=obj, status=True, status_field=True)
        total += olts.count()

        for olt in olts:
            total_devices += 1
            total += 1
            onus = ONU.objects.filter(status_field=True, olt=olt)
            total_devices += onus.count()
            for ont in onus:
                try:
                    if ont.status:
                        total += 1
                except:
                    pass
                    
        #obj.devices = total_devices
        #obj.save()

        return total

    class Meta:
        model = Mikrotik
        # model = Node
        fields = ('id', 'alias', 'devices_online')
        # fields = ('id', 'status', 'kind', 'code', 'ip', 'address', 'technologies', 'get_devices_online')


class FeedbackSerializer(HyperlinkedModelSerializer):

    created_by = serializers.HiddenField(
                 default=serializers.CurrentUserDefault()
                 )

    def create(self, validated_data):

        feedback = Feedback(version_sentinel=settings.SENTINEL_VERSION,
                            **validated_data)
        feedback.save()

        try:
            slack_feedback(feedback)
        except Exception as e:
            print("FeedbackSerializer error ", e)

        return feedback

    class Meta:
        model = Feedback
        fields = ('platform', 'user_agent', 'cookie_enabled', 'width',
                  'height', 'url', 'commentary', 'created_by')



class EntitySerializer(HyperlinkedModelSerializer):
    
    class Meta:
        model = Entity
        fields = ('entity',)    
    

class TaskCreateSerializer(serializers.Serializer):

    task = serializers.CharField()
    olt = serializers.UUIDField(required=False)

    def create(self, validated_data):
        from devices.tasks import generate_csv, task_uptime_onus, \
                                task_ip_onus, task_speeds_olt, \
                                task_new_olt, task_get_onus, \
                                task_vlans_olt

        rec = validated_data.pop('task')
        if rec == 'update_utp':
            
            from utp.tasks import task_new_mikrotik

            utp = Mikrotik.objects.get(uuid=validated_data.get('olt'))
            task = task_new_mikrotik.delay(utp.uuid, 'task_update_mikrotik')
            TaskModel.objects.get_or_create(id_obj=utp.id,
                                            model_obj='mikrotik',
                                            name='task_update_mikrotik',
                                            status='PENDING')
        if rec == 'uptime_onus':
            task = task_uptime_onus.delay(validated_data.get('olt'))
        elif rec == 'get_onus':
            task = task_get_onus.delay(validated_data.get('olt'))
        elif rec == 'ip_onus':
            task = task_ip_onus.delay(validated_data.get('olt'))
        elif rec == "get_vlans":
            task = task_vlans_olt.delay(validated_data.get('olt'))
        elif rec == 'speeds_olt':
            task = task_speeds_olt.delay(validated_data.get('olt'))
        elif rec == 'update_olt':
            olt = OLT.objects.get(uuid=validated_data.get('olt'))
            task = task_new_olt.delay(validated_data.get('olt'), None, None)
            TaskModel.objects.get_or_create(id_obj=olt.id,
                                            model_obj='olt',
                                            name='task_update_olt',
                                            status='PENDING')
        else:
            task = generate_csv.delay()
        return {'task': task}
