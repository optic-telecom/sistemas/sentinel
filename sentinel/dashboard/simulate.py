import json
from common import Attrs
from dashboard.models import Variable


class Simulate(object):

	def __init__(self, namespace, nombre, ajax=False):
		self.namespace = namespace
		self.nombre = nombre
		self.response = False
		self.ajax = ajax

	def __enter__(self):
		v = Variable.objects.filter(nombre=self.nombre,
									namespace=self.namespace,
									active=True)
		if v.exists():
			try:
				values = json.loads(v[0].valor)
				if self.ajax:
					self.response = values
				else:
					self.response = Attrs(**values)
			except Exception as e:
				print (e)

		return self

	def __exit__(self, *args):
		del self.response