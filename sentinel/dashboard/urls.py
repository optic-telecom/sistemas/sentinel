from django.conf.urls import url
from rest_framework import routers
from dynamic_preferences.api.viewsets import GlobalPreferencesViewSet
from .views import  DasboardViewSet, FeedbackViewSet,\
	TaskViewSet, SearchViewSet, SlackONUViewSet, EntityLogViewSet,\
	ActionsChoicesList


router = routers.DefaultRouter()
router.register(r'dashboard', DasboardViewSet)
router.register(r'feedback', FeedbackViewSet, 'feedback_api')
router.register(r'task', TaskViewSet, 'task_api')
router.register(r'search', SearchViewSet, 'search_api')
router.register(r'entity_log', EntityLogViewSet, 'entity_log_api')
router.register(r'actions_log', ActionsChoicesList, 'actions_log_api')
router.register(r'preferences/global', GlobalPreferencesViewSet, base_name='global')

router.register(r'slack_onu', SlackONUViewSet)