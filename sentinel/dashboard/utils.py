import os
import requests
from django.conf import settings
from ua_parser.user_agent_parser import Parse
from dynamic_preferences.registries import global_preferences_registry
from .models import LogSentinel, Entity, TypeAction
from dashboard.simulate import Simulate
from dashboard.actions import *


def slack_feedback(obj):
    preferences = global_preferences_registry.manager()
    user_agent = Parse(obj.user_agent)
    navigator = user_agent['user_agent']['family']
    version = user_agent['user_agent']['major'] + '.' + user_agent[
        'user_agent']['minor']
    system = user_agent['os']['family'] + ' ' + user_agent['os']['major']
    data = {
        "text":
        "Nuevo comentario para sentinel.",
        "attachments": [{
            "fallback":
            obj.commentary,
            "color":
            "#36a64f",
            "author_name":
            str(obj.created_by),
            "text":
            obj.commentary,
            "fields": [{
                "title": "Versión sentinel",
                "value": obj.version_sentinel,
                "short": True
            }, {
                "title": "Url",
                "value": obj.url,
                "short": False
            }, {
                "title": "Resolución",
                "value": "%sx%s" % (obj.width, obj.height),
                "short": True
            }, {
                "title": "Sistema operativo",
                "value": system,
                "short": True
            }, {
                "title": "Navegador",
                "value": navigator,
                "short": True
            }, {
                "title": "Versión navegador",
                "value": version,
                "short": True
            }]
        }]
    }
    return slack_send_message(preferences['url_slack_feedback'], data)


def slack_error(title, error, fields=[]):
    preferences = global_preferences_registry.manager()
    fields_ini = [{
        "title": "Versión sentinel",
        "value": settings.SENTINEL_VERSION,
        "short": True
    }]

    if fields != []:
        fields_end = fields_ini + fields
    else:
        fields_end = fields_ini

    data = {
        "text": title,
        "attachments": [{
            "color": "#ec2622",
            "text": error,
            "fields": fields_end
        }]
    }

    slack_sms = slack_send_message(preferences['url_slack_error'], data)
    return slack_sms


def slack_exito_caso2(onu, plan, client, service_number, service_address,
                      user):
    preferences = global_preferences_registry.manager()
    fields = [{
        "title": "Versión sentinel",
        "value": settings.SENTINEL_VERSION,
        "short": True
    }, {
        "title": "Serial",
        "value": onu.serial,
        "short": True
    }, {
        "title": "F/S/P",
        "value": onu.fsp,
        "short": True
    }, {
        "title": "F/S/P Descripcion",
        "value": onu.description,
        "short": True
    }, {
        "title": "OLT",
        "value": onu.olt.alias,
        "short": True
    }, {
        "title": "Cliente",
        "value": client,
        "short": True
    }, {
        "title": "Dirección del servicio",
        "value": service_address,
        "short": False
    }, {
        "title": "Plan",
        "value": str(plan.name),
        "short": True
    }, {
        "title": "Usuario",
        "value": str(user),
        "short": True
    }]

    fields.append({
        "title":
        'Nro de servicio',
        "short":
        False,
        "value":
        f"{service_number} https://optic.matrix2.cl/contracts/{service_number}/"
    })

    if onu.model:
        fields.append({
            "title": 'Modelo',
            "short": True,
            "value": str(onu.model.model)
        })

    data = {
        "text":
        'Onu aún por terminar de provisionar. Falta ip.',
        "attachments": [{
            "color": "#ffd429",
            "text": 'Se provisiono la ONU, pero no se pudo optener la ip',
            "fields": fields
        }]
    }

    url = preferences['url_slack_exito']
    slack_sms = slack_send_message(url, data)
    return slack_sms


def slack_exito_caso3(onu, plan, client, service_number, service_address,
                      signal, user):
    preferences = global_preferences_registry.manager()
    fields = [{
        "title": "Versión sentinel",
        "value": settings.SENTINEL_VERSION,
        "short": True
    }, {
        "title": "Serial",
        "value": onu.serial,
        "short": True
    }, {
        "title": "F/S/P",
        "value": onu.fsp,
        "short": True
    }, {
        "title": "F/S/P Descripcion",
        "value": onu.description,
        "short": True
    }, {
        "title": "OLT",
        "value": onu.olt.alias,
        "short": True
    }, {
        "title": "Cliente",
        "value": client,
        "short": True
    }, {
        "title": "Dirección del servicio",
        "value": service_address,
        "short": False
    }, {
        "title": "Plan",
        "value": str(plan.name),
        "short": True
    }, {
        "title": "Señal",
        "value": str(signal),
        "short": True
    }, {
        "title": "Usuario",
        "value": str(user),
        "short": True
    }]

    fields.append({
        "title":
        'Nro de servicio',
        "short":
        False,
        "value":
        f"{service_number} https://optic.matrix2.cl/contracts/{service_number}/"
    })

    if onu.model:
        fields.append({
            "title": 'Modelo',
            "short": True,
            "value": str(onu.model.model)
        })

    data = {
        "text":
        'Onu aún por terminar de provisionar. Señal inadecuada.',
        "attachments": [{
            "color": "#ffd429",
            "text":
            'Se provisiono la ONU, pero el nivel de señal no fue adecuado',
            "fields": fields
        }]
    }

    url = preferences['url_slack_exito']
    slack_sms = slack_send_message(url, data)
    return slack_sms


def slack_exito_caso6(onu, plan, client, service_number, service_address,
                      signal, user):

    preferences = global_preferences_registry.manager()
    fields = [{
        "title": "Versión sentinel",
        "value": settings.SENTINEL_VERSION,
        "short": True
    }, {
        "title": "Serial",
        "value": onu.serial,
        "short": True
    }, {
        "title": "F/S/P",
        "value": onu.fsp,
        "short": True
    }, {
        "title": "F/S/P Descripcion",
        "value": onu.description,
        "short": True
    }, {
        "title": "OLT",
        "value": onu.olt.alias,
        "short": True
    }, {
        "title": "Cliente",
        "value": client,
        "short": True
    }, {
        "title": "Dirección del servicio",
        "value": service_address,
        "short": False
    }, {
        "title": "Plan",
        "value": str(plan.name),
        "short": True
    }, {
        "title": "Señal",
        "value": str(signal),
        "short": True
    }, {
        "title": "Usuario",
        "value": str(user),
        "short": True
    }]

    fields.append({
        "title":
        'Nro de servicio',
        "short":
        False,
        "value":
        f"{service_number} https://optic.matrix2.cl/contracts/{service_number}/"
    })

    if onu.model:
        fields.append({
            "title": 'Modelo',
            "short": True,
            "value": str(onu.model.model)
        })

    data = {
        "text":
        'Onu provisionada',
        "attachments": [{
            "color": "#2aaf1e",
            "text": 'Se provisiono la ONU, falta configurar el WiFI.',
            "fields": fields
        }]
    }

    slack_sms = slack_send_message(preferences['url_slack_exito'], data)
    return slack_sms


def slack_erp_error(response, text, request=None):
    preferences = global_preferences_registry.manager()
    fields = [
        {
            "title": 'url',
            "short": False,
            "value": response.url
        },
        {
            "title": 'text',
            "short": False,
            "value": response.text
        },
    ]

    if request:
        fields.append({
            "title": 'Usuario',
            "short": True,
            "value": str(request.user.username)
        })

    data = {
        "text": "Error en respuesta del api de matrix.",
        "attachments": [{
            "color": "#ec2622",
            "text": text,
            "fields": fields
        }]
    }

    slack_sms = slack_send_message(preferences['url_slack_erp_error'], data)
    return slack_sms


def slack_escalar_caso2(onu, service_number, user):
    preferences = global_preferences_registry.manager()
    fields = [{
        "title":
        'Nro de servicio',
        "short":
        False,
        "value":
        f"{service_number} https://optic.matrix2.cl/contracts/{service_number}/"
    }, {
        "title": "Serial",
        "value": onu.serial,
        "short": True
    }, {
        "title": "F/S/P",
        "value": onu.fsp,
        "short": True
    }, {
        "title": "F/S/P Descripcion",
        "value": onu.description,
        "short": True
    }, {
        "title": "Usuario",
        "value": str(user),
        "short": True
    }]

    data = {
        "text":
        "Error en provisionamiento, posible caso de pool de IPs del nodo lleno",
        "attachments": [{
            "color": "#ec2622",
            "text": "<@UHBU6RDMZ> <@UBMH110TA> <@UB74477CL> <@UB6K9G0SC>",
            "fields": fields
        }]
    }

    slack_sms = slack_send_message(preferences['url_slack_escalar_caso2'],
                                   data)
    return slack_sms


def slack_debug(title, sms, fields=[]):
    preferences = global_preferences_registry.manager()

    fields_ini = [{
        "title": "Versión sentinel",
        "value": settings.SENTINEL_VERSION,
        "short": True
    }]

    if fields != []:
        fields_end = fields_ini + fields
    else:
        fields_end = fields_ini

    data = {
        "text": title,
        "attachments": [{
            "color": "#2aaf1e",
            "text": sms,
            "fields": fields_end
        }]
    }

    slack_sms = slack_send_message(preferences['url_slack_debug'], data)
    return slack_sms


def send_data_erp(request, data, text_error_slack=' '):
    preferences = global_preferences_registry.manager()
    h = {
        'Authorization':
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'
    }
    # with Simulate('ERP','send_data_erp') as s:
    #     if s.response:
    #         import time
    #         time.sleep(2)
    #         return s.response

    url = preferences['url_endpoint_erp'] + preferences[
        'url_endpoint_erp_provisioning_onu']
    response = requests.post(url, json=data, headers=h, verify=False)
    print('status matrix', response.status_code)
    print('data a matrix', data)
    print("respuesta matrix send_data_erp", response.text)
    if response.status_code == 201 or response.status_code == 200:

        print('888888')
        return True
    else:
        print('77777')
        slack_erp_error(response, text_error_slack, request)

        print('99999')
        return False


def send_status_change_matrix(request, data, text_error_slack=' '):

    preferences = global_preferences_registry.manager()
    h = {
        'Authorization':
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'
    }

    change = {'status' : 1}

    url = preferences['url_endpoint_erp'] + preferences[
        'url_endpoint_erp_provisioning_onu']

    response = requests.patch(url, json=change, headers=h, verify=False)

    print('status de matrix', response.status_code)
    print('data enviada a matrix', data)
    print("respuesta matrix send_status_change_matrix", response.text)

    if response.status_code == 201 or response.status_code == 200:

        print('Status cambiado')
        return True
    else:
        print('Error al cambiar estatus')
        # Utiliza el texto de error
        slack_erp_error(response, text_error_slack, request)

        print('Enviando a Slack')
        return False


def slack_send_message(url, data):
    preferences = global_preferences_registry.manager()
    if preferences['message_slack']:
        r = requests.post(url, json=data)
        return r.text
    return False


class LOG(object):

    model = LogSentinel
    _ent = None
    _method = None

    def __getattr__(self, attr):
        name = attr.upper()
        actions = [
            'ADD', 'CHANGE', 'FIND_AUTOFIND', 'DELETE', 'DEL', 'ERASE', 'SUP',
            'COUNTERS_UPDATE', 'SHELL', 'GET'
        ]
        if name in actions:
            self._method = name
            return self
        self._ent = name
        return self

    def get_action_method(self):
        r = None
        if self._method == 'ADD':
            r = ADD_ACTION
        if self._method == 'CHANGE':
            r = CHANGE_ACTION
        if self._method == 'FIND_AUTOFIND':
            r = FIND_AUTOFIND_ACTION
        if self._method in ['DELETE', 'DEL', 'ERASE', 'SUP']:
            r = DELETE_ACTION
        if self._method == 'COUNTERS_UPDATE':
            r = COUNTERS_UPDATE_ACTION
        if self._method == 'SHELL':
            r = SHELL_ACTION
        if self._method == 'GET':
            r = GET_ACTION
        return r

    def __call__(self, *args, **values):

        if len(args) == 1:
            return self.save_args(*args, **values)

        action = self.get_action_method()
        if action:
            return self.save_log(action, **values)

        print('me estan llamando sin funcion', values, self._method, self._ent)

    def save_args(self, *args, **values):
        data_local = dict(args[0])
        data_send = {}

        action = self.get_action_method()
        if action:

            data_send['id_data'] = data_local.get('_id_data', None)
            if 'obj_to_log' in data_local:
                data_send['by_representation'] = data_local['obj_to_log'][
                    'by_representation']

            if SHELL_ACTION == action:

                if 'e' in data_local:
                    sms = f'No se pudo conectar -> {data_local["e"]}'
                    data_send['level'] = 30
                else:
                    sms = 'Nueva conexión'

                data_send['description'] = sms
                data_send['id_value'] = data_local.get('olt').uuid
                data_send['entity_representation'] = repr(
                    data_local.get('olt'))
                return self.save_log(SHELL_ACTION, **data_send)

            if ADD_ACTION == action:
                data_send['description'] = 'Añadido con exito'

                if self._ent == 'INTERFACE':
                    data_send['entity_representation'] = repr(
                        data_local.get('interface'))
                    data_send['id_value'] = data_local.get('interface').id

                return self.save_log(SHELL_ACTION, **data_send)

    def save_log(self, action_method, **values):
        #print ('save_log',action_method,values)
        if self._ent != 'OLT':
            #exit()
            pass
        entity, c = Entity.objects.get_or_create(entity=self._ent)
        action, c = TypeAction.objects.get_or_create(
            type_action=values.get('action', action_method))
        print('entity', entity)
        entity_representation = values.get('entity_representation',
                                           str(entity))

        by = values.get('by', None)
        by_representation = values.get('by_representation')
        id_field = values.get('id_field', None)
        id_value = values.get('id_value', None)
        description = values.get('description')
        id_data = values.get('id_data', None)
        level = values.get('level', None)

        data = {
            'by_representation': by_representation,
            'action': action,
            'entity_representation': entity_representation,
            'entity': entity,
            'description': description,
            'id_data': id_data
        }

        if id_value:
            data['id_value'] = id_value

        if id_field:
            data['id_field'] = id_field

        if level:
            data['level'] = level

        if by:
            data['by'] = by

        self.model.objects.create(**data)
        return True


def slack_fields_data(onu, service_number, user):

    fields = [{
        "title": "Versión sentinel",
        "value": settings.SENTINEL_VERSION,
        "short": True
    }, {
        "title":
        'Nro de servicio',
        "short":
        False,
        "value":
        f"{service_number} https://optic.matrix2.cl/contracts/{service_number}/"
    }, {
        "title": "Serial",
        "value": onu.serial,
        "short": True
    }, {
        "title": "F/S/P",
        "value": '{}/{}/{}'.format(onu.frame, onu.slot, onu.port),
        "short": True
    }, {
        "title": "F/S/P Descripcion",
        "value": onu.description,
        "short": True
    }, {
        "title": "Usuario",
        "value": str(user),
        "short": True
    }]

    return fields


URL = 'https://hooks.slack.com/services/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'


def slack_notification_exito(onu, service_number, user):
    preferences = global_preferences_registry.manager()
    fields = slack_fields_data(onu, service_number, user)

    data = {
        "text": "Onu privisionada con éxito",
        "attachments": [{
            "color": "#43e024",
            "text": "Texto",
            "fields": fields
        }]
    }

    slack_sms = slack_send_message(preferences['url_slack_exito'], data)
    # print('exito')
    # slack_sms = slack_send_message(URL, data)
    return slack_sms


def slack_alert_escalamiento(message, onu, service_number, user):
    preferences = global_preferences_registry.manager()
    fields = slack_fields_data(onu, service_number, user)

    fields.append({"title": "IP de la ONU", "value": onu.ip, "short": True})
    fields.append({
        "title": "IP de la OLT",
        "value": onu.olt.ip,
        "short": True
    })

    if onu.olt.ip_node:
        fields.append({
            "title": "IP del nodo",
            "value": onu.olt.ip_node,
            "short": True
        })

    data = {
        "text": message,
        "attachments": [{
            "color": "#ec2622",
            "text": "",
            "fields": fields
        }]
    }

    # canal de #escalamiento-tecnico
    slack_sms = slack_send_message(preferences['url_slack_escalar_caso2'],
                                   data)
    # print(message)
    # slack_sms = slack_send_message(URL, data)
    return slack_sms


def slack_alert_incidencia(message, onu, service_number, user):
    preferences = global_preferences_registry.manager()
    fields = slack_fields_data(onu, service_number, user)

    fields.append({"title": "IP de la ONU", "value": onu.ip, "short": True})
    data = {
        "text": message,
        "attachments": [{
            "color": "#ec2622",
            "text": "",
            "fields": fields
        }]
    }

    # canal de #provisionar-clientes
    slack_sms = slack_send_message(preferences['url_slack_exito'], data)
    # print(message)
    # slack_sms = slack_send_message(URL, data)
    return slack_sms


def slack_alert_signal(message, onu, service_number, user, signal='-'):
    preferences = global_preferences_registry.manager()
    fields = slack_fields_data(onu, service_number, user)
    fields.append({"title": "IP de la ONU", "value": onu.ip, "short": True})
    fields.append({"title": "Señal", "value": signal, "short": True})

    data = {
        "text": message,
        "attachments": [{
            "color": "#ec2622",
            "text": "",
            "fields": fields
        }]
    }

    # canal de #provisionar-clientes
    slack_sms = slack_send_message(preferences['url_slack_exito'], data)
    # print(message)
    # slack_sms = slack_send_message(URL, data)
    return slack_sms


def slack_alert_node(message, onu, service_number, user):
    preferences = global_preferences_registry.manager()
    fields = slack_fields_data(onu, service_number, user)
    fields.append({"title": "IP de la ONU", "value": onu.ip, "short": True})

    fields.append({
        "title": "IP de la OLT",
        "value": onu.olt.ip,
        "short": True
    })

    if onu.olt.ip_node:
        fields.append({
            "title": "IP del nodo",
            "value": onu.olt.ip_node,
            "short": True
        })

    data = {
        "text": message,
        "attachments": [{
            "color": "#ec2622",
            "text": "",
            "fields": fields
        }]
    }

    # canal de #provisionar-clientes
    slack_sms = slack_send_message(preferences['url_slack_escalar_caso2'],
                                   data)
    # print(message)
    # slack_sms = slack_send_message(URL, data)
    return slack_sms


def slack_alert_error(message,
                      onu,
                      service_number,
                      user,
                      signal='-',
                      potencia_optica='-'):
    preferences = global_preferences_registry.manager()
    fields = slack_fields_data(onu, service_number, user)
    fields.append({"title": "IP de la ONU", "value": onu.ip, "short": True})
    fields.append({
        "title": "IP de la OLT",
        "value": onu.olt.ip,
        "short": True
    })

    fields.append({
        "title": "Modelo de la OLT",
        "value": str(onu.olt.model.model) + str(onu.olt.model.brand),
        "short": True
    })

    fields.append({
        "title": "Descripcion de la OLT",
        "value": str(onu.olt.description),
        "short": False
    })

    fields.append({
        "title": "Modelo de la ONU",
        "value": str(onu.model.model) + str(onu.model.brand),
        "short": True
    })

    fields.append({
        "title": "Potencia óptica",
        "value": potencia_optica,
        "short": True
    })

    fields.append({"title": "Señal", "value": signal, "short": True})

    if onu.olt.ip_node:
        fields.append({
            "title": "IP del nodo",
            "value": onu.olt.ip_node,
            "short": True
        })

    data = {
        "text": message,
        "attachments": [{
            "color": "#ec2622",
            "text": "",
            "fields": fields
        }]
    }

    # canal de #errores-sentinel
    slack_sms = slack_send_message(preferences['url_slack_error'], data)
    # print(message)
    # slack_sms = slack_send_message(URL, data)
    return slack_sms


# def slack_notification(onu_serial, onu_fsp, onu_descripcion, service_number, user):
#     fields = [{
#         "title":'Nro de servicio',
#         "short": False,
#         "value": f"{service_number} https://optic.matrix2.cl/contracts/{service_number}/"
#         },
#         {
#         "title": "Serial",
#         "value": onu_serial,
#         "short": True
#         },
#         {
#         "title": "F/S/P",
#         "value": onu_fsp,
#         "short": True
#         },
#         {
#         "title": "F/S/P Descripcion",
#         "value": onu_descripcion,
#         "short": True
#         },
#         {
#         "title": "Usuario",
#         "value": str(user),
#         "short": True
#         }
#     ]

#     data = {
#         "text": "Onu privisionada con éxito",
#         "attachments": [
#           {
#             "color": "#43e024",
#             "text": "Texto",
#             "fields": fields
#           }
#         ]
#     }

#     r = requests.post('https://hooks.slack.com/services/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', json = data)
# slack_sms = slack_send_message(preferences['url_slack_exito'], data)
# return slack_sms

# return fields