from django_filters.rest_framework import DjangoFilterBackend
from django.contrib.auth import get_user_model
from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.views import APIView
from rest_framework.response import Response
from celery.result import AsyncResult

from .serializers import FeedbackSerializer, DasboardSerializer,\
    TaskCreateSerializer, EntitySerializer

from utp.models import Mikrotik, CpeUtp
from .models import Feedback, Entity
from devices.models import OLT, ONU
from .actions import ACTION_CHOICES

class SlackONUViewSet(mixins.CreateModelMixin,
                      viewsets.GenericViewSet):
    
    queryset = Mikrotik.objects.filter(status_field=True)
    serializer_class = DasboardSerializer

    permission_classes = (AllowAny,)
    
    def create(self, request, *args, **kwargs):
        try:
            s = request.POST.get('text', None)
            onus = ONU.objects.filter(serial__icontains=s)
            attachments = []

            for onu in onus:

                fields = [
                    {
                    "title": "Serial",
                    "value": onu.serial,
                    "short": True
                    },
                    {
                    "title": "F/S/P",
                    "value": "{}/{}/{}".format(onu.frame,onu.slot,onu.port),
                    "short": True
                    },
                    {
                    "title": "OLT",
                    "value": onu.olt.alias,
                    "short": True
                    }
                ]
                attachments.append({
                    "color": "#2aaf1e",
                    "text": "ONT ID: {}".format(onu.onu_id),
                    "fields": fields
                })

            if len(attachments) == 0:
                raise Exception('no encontrada')
            data = {
                "attachments": attachments
            }

        except Exception as e:
            data = {
                "attachments": [
                    {
                        "color": "#ec2622",
                        "text": "La ONU no pudo ser encontrada"
                    }
                ]
            }

        return Response(data)



class DasboardViewSet(viewsets.GenericViewSet):
    
    queryset = Mikrotik.objects.filter(status_field=True)
    serializer_class = DasboardSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)



class FeedbackViewSet(mixins.CreateModelMixin,
                      viewsets.GenericViewSet):

    queryset = Feedback.objects.all()
    serializer_class = FeedbackSerializer




class TaskViewSet(mixins.CreateModelMixin,
                  viewsets.GenericViewSet):

    queryset = OLT.objects.all()
    serializer_class = TaskCreateSerializer


    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=201, headers=headers)
        except Exception as e:
            return Response({'error':'No se pudo conectar al servidor de tareas','code':'redis'}, status=500)




class SearchViewSet(viewsets.GenericViewSet):

    # solo por requerimiento se colocan
    queryset = OLT.objects.filter(status_field=True)
    serializer_class = TaskCreateSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('ip',)

    def list(self, request, *args, **kwargs):
        from django.db.models import Q
        from django.contrib.humanize.templatetags.humanize import naturaltime
        import ipaddress

        q = request.GET.get('q', None)
        ip = request.GET.get('ip', None)
        onus, cpes, olts, results = [],[],[],[]

        if ip and ip != 'null':
            onus = ONU.objects.filter(ip__ip=ip)
            olts = OLT.objects.filter(ip__ip=ip)

        elif q and q != 'null':

            try:
                ipaddress.ip_address(q)
                onus = ONU.objects.filter(ip__ip=q)
                olts = OLT.objects.filter(ip__ip=q)
                cpes = CpeUtp.objects.filter(ip__ip=q)
            except ValueError as e:

                try:
                    int_q = int(q)
                    onus = ONU.objects.filter(
                        Q(serial__icontains=q) |
                        Q(model__model__icontains=q) |
                        Q(service__number=int_q) |
                        Q(description__icontains=q)
                    )                
                except Exception as e:
                    onus = ONU.objects.filter(
                        Q(serial__icontains=q) |
                        Q(model__model__icontains=q) |
                        Q(description__icontains=q)
                    ) 

                olts = OLT.objects.filter(
                    Q(alias__icontains=q) |
                    Q(model__model__icontains=q) |
                    Q(description__icontains=q)
                )



        try:
            for onu in onus:
                results.append({'title':'ONU Serial: %s' % onu.serial,
                                'model':onu.model.model if onu.model else '',
                                'id':onu.uuid,
                                'uptime':naturaltime(onu.uptime),
                                'type':'ONU',
                                'color':'blue',
                                'serial':onu.serial,
                                'ip':onu.ip.ip if onu.ip else '',
                                'uuid':onu.uuid})            
        except Exception as e:
            print (e)

        try:
            for olt in olts:
                newMac = ':'.join(olt.mac.mac[i:i+2] for i in range(0,12,2))
                results.append({'title':'OLT mac: %s' % newMac.upper() ,
                                'model':olt.model.model,
                                'id':olt.uuid,
                                'uptime':naturaltime(olt.uptime),
                                'type':'OLT',
                                'color':'orange',
                                'ip':olt.ip.ip if olt.ip else '',
                                'uuid':olt.uuid,
                                'alias':olt.alias})            
        except Exception as e:
            print (e)


        try:
            for cpe in cpes:
                newMac = ':'.join(cpe.mac.mac[i:i+2] for i in range(0,12,2))
                results.append({'title':'CPE mac: %s' % newMac.upper() ,
                            'model':cpe.model.model,
                            'id':cpe.uuid,
                            'uptime':naturaltime(cpe.uptime),
                            'type':'CPE',
                            'color':'green',
                            'ip':cpe.ip.ip if cpe.ip else '',
                            'uuid':cpe.uuid,
                            'alias':cpe.alias})            
        except Exception as e:
            print (e)




        return Response(results)


class EntityLogViewSet(viewsets.ModelViewSet):

    queryset = Entity.objects.all()
    serializer_class = EntitySerializer



class MultipleChoicesMixins():
    choices_response = {}

    def get_choices_response(self):
        return self.choices_response

    def reformat_object(self, array):
        return [{"id": obj[0], "name": obj[1]} for obj in array]

    def get_data(self):
        return {key: self.reformat_object(value) for key, value in self.get_choices_response().items()}


class MultipleChoicesViewSet(MultipleChoicesMixins, mixins.ListModelMixin,
                             viewsets.GenericViewSet):
    def list(self, request, version, format=None):
        return Response(self.get_data())


class ActionsChoicesList(MultipleChoicesViewSet):
    """
    List all choices for actions logs
    """
    queryset = Entity.objects.all()
    serializer_class = EntitySerializer
    choices_response = {"actions": ACTION_CHOICES}



def task_result(request, task):
    task = AsyncResult(task)
    if task.ready():
        try:
            return Response({"finished": True, "result": task.get(timeout=20, propagate=False)})
        except TypeError:
            return Response(
                {"finished": True, "result": str(task.get(timeout=20, propagate=False))})
    else:

        return Response({"finished": False})    