from django.contrib import admin, messages
from common.admin import BaseAdmin
from .models import OLT, ONU, ModelDevice, Ethernet,\
    OLTConnection, LogDevice, Abnormalitie, AbnormalitieDevice,\
    InterfaceOLT, WirelessONUS,\
    PortOLT, SystemVLAN, Operator, TypeVLAN, OLTVLAN,\
    OLTSpeeds, ServicePortONUS, \
    FirmwareONUS, ONUAutoFind, StatisticsPortOLT,\
    SystemSpeeds, ServicePortHistorical, OLTUser, IPTable, MenuSmokeping,\
    MACTable, Device, SSHConnection

from .forms import MenuSmokepingForm
from dynamic_preferences.registries import global_preferences_registry


@admin.register(MACTable)
class MACTableAdmin(BaseAdmin):
    search_fields = ["mac"]

admin.site.register(SSHConnection)


@admin.register(OLT)
class OLTAdmin(BaseAdmin):
    list_display = ('ip','alias','model', 'mac', 'status','uptime')
    list_editable = ('mac',)
    exclude = ('address',)
    raw_id_fields = ('mac', 'node', 'mac_secondary', 'ip')


@admin.register(ONU)
class ONUAdmin(BaseAdmin):
    list_display = ('serial', 'mac', 'model', 'ip', 'card', 'service', 'onu_id', 'uptime')
    # list_editable = ('service',)
    list_filter = ('model', 'olt', 'slot', 'port', 'status') 
    search_fields = ['mac__mac','serial', 'ip__ip','service__number']
    actions = ['change_activation', 'clear_ips', 'clear_status']
    raw_id_fields = ('olt','service','model','mac','ip')

    def clear_ips(self, request, queryset):
        n = queryset.count()
        self.message_user(request,"%(count)d ips borradas." % {"count": n }, messages.SUCCESS)   
        for item in queryset:
            item.ip = None
            item.save()
        return queryset
    clear_ips.short_description = 'Borra las ips'

    def clear_status(self, request, queryset):
        n = queryset.count()
        self.message_user(request,"%(count)d status borrados." % {"count": n }, messages.SUCCESS)   
        for item in queryset:
            item.status = None
            item.save()
        return queryset
    clear_status.short_description = 'Borra los status'

    def card(self, obj):
        return "%s/%s/%s" % (obj.frame, obj.slot, obj.port)

    def mac(self, obj):
        return obj.get_mac()

    def ip(self, obj):
        return obj.get_ip()


# @admin.register(ONUHistorical)
# class ONUHistoricalAdmin(ONUAdmin):
#     search_fields = ['serial', 'onu_id']


@admin.register(ModelDevice)
class ModelDeviceAdmin(BaseAdmin):
    list_display = ('model','brand','kind',)


@admin.register(Ethernet)
class EthernetAdmin(BaseAdmin):
    list_display = ('status','port_type','port_id','speeds','duplex','serial')
    list_filter = ['onu__olt',] 
    search_fields = ['id_data','onu__serial']      

    def serial(self,obj):
        return obj.onu.serial

    def port_id(self, obj):
        return obj.port_id if obj.port_id else '-'

    def port_type(self, obj):
        return obj.port_type if obj.port_type else '-'

    def speeds(self, obj):
        return obj.speeds if obj.speeds else '-'

    def duplex(self, obj):
        return obj.duplex if obj.duplex else '-'


@admin.register(OLTConnection)
class OLTConnectionAdmin(BaseAdmin):
    list_display = ('host','port','username','connections')
    list_filter = ['olt']  
    actions = ['change_activation', 'liberar']

    def host(self, obj):
        return obj.olt.ip.ip + " " + obj.olt.alias

    def liberar(self, request, queryset):
        model = self.model._meta.verbose_name
        n = queryset.count()
        message = "%(count)d %(model)s cambiadas." % {"count": n ,"model":model}
        self.message_user(request, message, messages.SUCCESS)   
        for item in queryset:
            item.connections = 0
            item.save()
        return queryset
    liberar.short_description = 'Resetea nro de conexiones'

@admin.register(LogDevice)
class LogDeviceAdmin(BaseAdmin):
    list_display = ('entry','type','onu','olt','created')


@admin.register(Abnormalitie)
class AbnormalitieAdmin(BaseAdmin):
    list_display = ('reason','link','created')


@admin.register(AbnormalitieDevice)
class AbnormalitieDeviceAdmin(BaseAdmin):
    list_display = ('olt','onu','abnormalitie')


@admin.register(InterfaceOLT)
class InterfaceOLTAdmin(BaseAdmin):
    list_display = ('interface','slot','onus','ports',
                    'onus_active','ports_active','status')  
    list_filter = ['olt']
    search_fields = ['id_data','serial']    


# @admin.register(StatusONUS)
# class StatusONUSAdmin(BaseAdmin):
#     list_display = ('onu','status','downcause','uptime')  
#     search_fields = ['onu__serial',]
#     list_filter = ['onu__olt']


# @admin.register(MoreDetailONUS)
# class MoreDetailONUSAdmin(BaseAdmin):
#     list_display = ('onu','ram','cpu','temperature',)  
#     list_filter = ['onu__olt']
#     search_fields = ['onu__serial',]




@admin.register(WirelessONUS)
class WirelessONUSAdmin(BaseAdmin):
    list_display = ('onu', 'wifi_old_band_ssid','wifi_old_band_status',
                    'wifi_new_band_ssid','wifi_new_band_status',)
    search_fields = ['onu__serial','onu__uuid']
    list_filter = ['onu__olt']    


@admin.register(PortOLT)
class PortOLTAdmin(BaseAdmin):
    list_display = ('frame','slot','port','description','onus_active')  
    list_filter = ['interface__olt']

    def frame(self, obj):
        return obj.interface.frame

    def slot(self, obj):
        return obj.interface.slot


@admin.register(Operator)
class OperatorAdmin(BaseAdmin):
    list_display = ('name', 'code')  


@admin.register(TypeVLAN)
class TypeVLANAdmin(BaseAdmin):
    list_display = ('name',)  


@admin.register(SystemVLAN)
class SystemVLANAdmin(BaseAdmin):
    list_display = ('vlan_type','vlan_id','operator','description',)


@admin.register(OLTVLAN)
class OLTVLANAdmin(BaseAdmin):
    list_display = ('vlan_id','vlan_type','description',
                    'stdn_port','associated_ports','created')
    list_filter = ('olt', 'vlan_type', 'vlan_system',) 

    
@admin.register(OLTSpeeds)
class OLTSpeedsAdmin(BaseAdmin):

    list_display = ('olt','tid','name',
                    'cir','cbs','pir','pbs','pri',
                    'tidsis')
    
    def tidsis(self, obj):
        if obj.speeds_system:
            return obj.speeds_system.tidsis
        return ''

    def get_list_filter(self, request):
        global_preferences = global_preferences_registry.manager()

        try:
            list_filter = global_preferences['olt_speeds_list_filter'].split(',')
            return list_filter
        except Exception as e:
            return ('olt','speeds_system','id_data')


@admin.register(ServicePortONUS)
class ServicePortONUSAdmin(BaseAdmin):
    list_display = ('service_port','vlan_id','vlan_attr',
                    'port_type','status',)
    list_filter = ('onu__olt',)
    date_hierarchy = 'modified'





@admin.register(FirmwareONUS)
class FirmwareONUSAdmin(BaseAdmin):
    list_display = ('main_software','standby_software','serial')
    search_fields = ['onu__serial']

    def serial(self, obj):
        return obj.onu.serial


@admin.register(ONUAutoFind)
class ONUAutoFindAdmin(BaseAdmin):
    list_display = ('serial','description','service',
                    'autofind_time','software_version','fsp','created',
                    'id_data','in_previus_olt')
    search_fields = ['id_data','uuid','serial']


@admin.register(StatisticsPortOLT)
class StatisticsPortOLTAdmin(BaseAdmin):
    list_display = ('unicast_rec','multicast_rec','broadcast_rec',
                    'discarded_rec','error_rec','unicast_send','multicast_send',
                    'id_data','broadcast_send')
    search_fields = ['port','port__port',]
    list_filter = ('port__interface__olt',)


@admin.register(ServicePortHistorical)
class ServicePortHistoricalAdmin(BaseAdmin):
    list_display = ('serial','frame','slot',
                    'port','description','model','onu_id',
                    'service_port','vlan_id','status')
    search_fields = ['port','model',]
    list_filter = ('port','model')


@admin.register(SystemSpeeds)
class SystemSpeedsAdmin(BaseAdmin):
    list_display = ('tidsis','name','cir',
                    'cbs','pir','pbs','pri',)
    list_editable = ('name','cir',
                    'cbs','pir','pbs','pri',)


@admin.register(OLTUser)
class OLTUserAdmin(BaseAdmin):
    list_display = ('name','level','online',
                    'reenter_num','profile','append_info',)
    list_editable = ('level','online',)



@admin.register(IPTable)
class IPTableAdmin(BaseAdmin):
    list_display = ('ip','host','menu', 'title',)
    list_editable = ('menu',)
    readonly_fields = ('created',)
    search_fields = ('ip',)
    
    fieldsets = (
        ('IP', {
            'fields': (
                'ip',
            ),
        }),
        ('Información Gral', {
            'classes':('collapse',),
            'fields': (
                'host',
                'menu',
                'title'
            ),
        }),
        ('Auditoría',{
            'classes':('collapse',),
            'fields':(
                'created',
                'status_field'

            )
        }),

    )


@admin.register(MenuSmokeping)
class MenuSmokepingAdmin(BaseAdmin):
    form = MenuSmokepingForm
    list_display = ('id','title','order_field')
    list_editable = ('title','order_field')
