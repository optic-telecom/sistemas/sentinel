from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet, ViewSet, ReadOnlyModelViewSet

from devices.models import *
from devices.serializers import ONUSerializer,LogDeviceSerializer,OLTVLANSerializer,OLTPortSerializer,OLTSpeedsSerializer,InterfaceOLTSerializer
from users.serializers import UserSerializer


from rest_framework import serializers as sz 

from dynamic_preferences.registries import global_preferences_registry


import requests


def get_peticion(url):
    headers = {'Authorization' : 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'
    }

    response = requests.get(url, headers=headers, verify=False)
    # results = response.json()['results']

    return response.json()


class OLTUserSerializer(sz.ModelSerializer):

    class Meta:
        model = OLTUser
        fields = ['name','level','online','reenter_num','profile','append_info']


class ONUSAltern(ReadOnlyModelViewSet):
    queryset = OLT.objects.all()
    serializer_class = ONUSerializer

    def get_query(self):
        qs = self.queryset.get(uuid = self.kwargs['uuid'], status_field = True)
        qs = qs.onu_set.all()
        return qs

    def list(self,request,*args,**kwargs):
        qs = self.get_query()
        page = self.paginate_queryset(qs)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(qs, many=True)
        return Response(serializer.data)


class UserAltern(ONUSAltern):
    serializer_class = OLTUserSerializer

    def get_query(self):
        qs = self.queryset.get(uuid = self.kwargs['uuid'], status_field = True)
        qs = qs.oltuser_set.all()
        return qs


class LogDevicesAltern(ONUSAltern):
    serializer_class = LogDeviceSerializer

    def get_query(self):
        qs = self.queryset.get(uuid = self.kwargs['uuid'], status_field = True)
        qs = qs.logdevice_set.all()
        return qs


class VLANAltern(ONUSAltern):
    serializer_class = OLTVLANSerializer

    def get_query(self):
        qs = self.queryset.get(uuid = self.kwargs['uuid'], status_field = True)
        qs = qs.oltvlan_set.all()
        return qs

class SpeedAltern(ONUSAltern):
    serializer_class = OLTSpeedsSerializer

    def get_query(self):
        qs = self.queryset.get(uuid = self.kwargs['uuid'], status_field = True)
        qs = qs.oltspeeds_set.all()
        return qs

class InterfacesAltern(ONUSAltern):
    serializer_class = InterfaceOLTSerializer

    def get_query(self):
        qs = self.queryset.get(uuid = self.kwargs['uuid'], status_field = True)
        qs = qs.interfaceolt_set.all()
        return qs

# ================================================= #
class OnuUTP(ReadOnlyModelViewSet):
    """
    APi de respuesta para Iris...
    """
    queryset = ONU.objects.all()
    serializer_class = ONUSerializer
    
    def request_mikrotik(self,sn):
        global_preferences = global_preferences_registry.manager()
        url = '{url_mikrotik}api/v1/cpeutp/{service_number}/retrieve_by_service_number/'.format(url_mikrotik=global_preferences['url_mikrotik'],service_number = sn)
        print = url
        if global_preferences['url_mikrotik']:
            try:
                # url = 'http://localhost:8080/api/v1/cpeutp/{service_number}/retrieve_by_service_number/'.format(service_number = sn)
                url = '{url_mikrotik}api/v1/cpeutp/{service_number}/retrieve_by_service_number/'.format(url_mikrotik=global_preferences['url_mikrotik'],service_number = sn)
                print = url
                response = get_peticion(url)
                return response[0]
            except Exception as e:
                return dict()
                
    def request_sentinel_base(self,sn):
        qs = self.queryset.filter(service_number=sn, status_field = True)
        qs = qs.last()
        print(qs)
        if qs:
            serializer = self.get_serializer(qs)
            return serializer.data
        else:
            return {}

    def list(self, request, *args, **kwargs):
        global_preferences = global_preferences_registry.manager()
        respuesta = dict(
            # sentinel_base = self.request_sentinel_base(int(kwargs['service_number'])),
            sentinel_mikrotik = self.request_mikrotik(int(kwargs['service_number']))
        )
        url = '{url_mikrotik}api/v1/cpeutp/{service_number}/retrieve_by_service_number/'.format(url_mikrotik=global_preferences['url_mikrotik'],service_number = int(kwargs['service_number']))
        print = url
        print(respuesta)
        return Response(respuesta)


