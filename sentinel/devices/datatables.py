import datetime
from dateutil.relativedelta import relativedelta
from django.utils import timezone
from django.db.models import Q  #, Value, Sum, F
#from django.db.models.functions import Concat
#from django.db.models.fields import IntegerField

from common.utils import timedelta_format
from common.mixins import DataTable, ButtonDataTable
from dashboard.models import TaskModel
from devices.models import OLT, ONU, SystemVLAN,\
    Operator, OLTVLAN, OLTSpeeds, SystemSpeeds,\
    PortOLT, InterfaceOLT, OLTConnection, OLTUser,MenuSmokeping,IPTable

from customers.models import Plan

from dynamic_preferences.registries import global_preferences_registry

from .serializers import IPTableSerializers, MenuSmokepingSerializers


class DatatableOLT(DataTable):
    model = OLT
    id_current = 0
    columns = [
        'contador', 'id', 'alias', 'ip', 'onus', 'uptime', 'model.model', 'description',
        'botones'
    ]
    order_columns = [
        'id', 'alias', 'ip', 'onus', 'uptime', 'model.model', 'description'
    ]
    max_display_length = 1000

    def display_id(self, obj):
        return '<a href="#/devices/olt/%s/" data-toggle="ajax">%s</a>' % (
            obj.uuid, obj.id)

    def display_alias(self, obj):
        return '<a href="#/devices/olt/%s/" data-toggle="ajax">%s</a>' % (
            obj.uuid, obj.alias)

    def btn_delete(self, row):
        t = """<a type="button" class="btn btn-danger" onclick="{fn}">
        <img style="width:18px;" src="/static/img/papelera.png"></a>"""
        return t.format(fn="delete_olt('%s')" % str(row.uuid))

    def btn_update_olt(self, btn, row):
        btn.click("taskUpdateOLT('%s')" % str(row.uuid), 'info', 'play',
                  'Actualizar TODA la informacion de OLT y ONUs')

    def btn_task_new_olt(self, row):
        try:
            TaskModel.objects.get(id_obj=row.id,
                                  model_obj='olt',
                                  name='task_new_olt',
                                  status_field=True,
                                  status='PENDING')
            inf = """<a type="button" title="Creando OLT" class="btn btn-white">
            <i class="fas fa-cog fa-spin"></i></a>"""
            return inf
        except TaskModel.DoesNotExist:
            return None

    def btn_task_update_olt(self, row):
        try:
            TaskModel.objects.get(id_obj=row.id,
                                  model_obj='olt',
                                  name='task_update_olt',
                                  status_field=True,
                                  status='PENDING')
            inf = """<div type="button" title="Actualizando OLT" class="btn btn-white"">
            <i class="fas fa-cog fa-spin fa-lg"></i></div>"""
            return inf
        except TaskModel.DoesNotExist:
            return None

    def btn_get_all_info_olt(self, row):
        try:
            TaskModel.objects.get(id_obj=row.id,
                                  model_obj='olt',
                                  name='get_all_info_olt',
                                  status_field=True,
                                  status='PENDING')
            inf = """<a type="button" title="Cron all info OLT" class="btn btn-white">
            <i class="fas fa-cog fa-spin fa-lg"></i></a>"""
            return inf
        except TaskModel.DoesNotExist:
            return None

    def render_column(self, row, column):
        global_preferences = global_preferences_registry.manager()
        if column == 'botones':
            btn = ButtonDataTable()

            if global_preferences['actions_olt_datatable']:
                btn_task1 = self.btn_task_new_olt(row)
                if btn_task1:
                    btn.list_btn.append(btn_task1)
                btn_task2 = self.btn_task_update_olt(row)

                if btn_task2:
                    btn.list_btn.append(btn_task2)

                btn_cron1 = self.btn_get_all_info_olt(row)
                if btn_cron1:
                    btn.list_btn.append(btn_cron1)

                if not btn_task2 and not btn_cron1 and not btn_task1:
                    # Se puede actualizar
                    self.btn_update_olt(btn, row)

            btn.list_btn.append(self.btn_delete(row))
            return btn()
        elif column == 'contador':
            self.id_current += 1
            return '<p>%s</p>' % (self.id_current)
        else:
            return super().render_column(row, column)

    def display_uptime(self, obj):
        if obj.status:
            display = '<i class="fas fa-circle" style="color: green;"></i> '
            if obj.uptime:
                #display += str(obj.uptime.strftime("%dd, %Hh, %Mm, %Ss"))
                u = timezone.now() - obj.uptime
                display += timedelta_format(u)
            elif obj.get_uptime():
                u = timezone.now() - obj.get_uptime()
                display += timedelta_format(u)
            return display

        diff = relativedelta(obj.time_uptime, timezone.now())
        diff = '%sd, %sh, %sm, %ss' % (diff.days, diff.hours, diff.minutes,
                                       diff.seconds)
        diff = diff.replace('-', '')
        return '<i class="fas fa-circle" style="color: red;"></i> conexión pérdida hace ' + str(
            diff)

    def display_onus(self, obj):
        return '%s/%d' % (obj.onus_active, obj.onus)

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = self.model.objects.filter(
                Q(alias__icontains=search) | Q(ip__ip__icontains=search)
                | Q(description__icontains=search)
                | Q(model__model__icontains=search))
        return qs


class DatatableSSH(DataTable):
    model = OLTConnection
    id_current = 0
    columns = [
        'id',
        'olt',
        'username',
        'port',
    ]
    order_columns = columns

    def get_initial_queryset(self):
        try:
            qs = self.model.objects.filter(status_field=True,
                                           olt__uuid=self.uuid)
        except:
            qs = self.model.objects.filter(status_field=True)
        return qs

    def display_id(self, obj):
        self.id_current += 1
        return self.id_current

    def display_password(self, obj):
        return '*' * len(obj.password)

    def display_port(self, obj):
        return obj.port if obj.port else '22'

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = self.model.objects.filter(
                Q(username__icontains=search) | Q(olt__alias__icontains=search)
                | Q(olt__ip__ip__icontains=search))
        return qs


class DatatableUNU(DataTable):
    model = ONU
    id_current = 0
    columns = [
        'contador', 'id', 'service', 'status', 'network_status', 'uptime', 'serial', 'model', 
        'description', 'distance', 'RX', 'mac', 'ip', 'activate_service_button',
        'corte', 'reposicion', 'primary_equipment', 'olt', 'created'
    ]
    order_columns = [
        'id', 'service', 'status', 'uptime', 'serial', 'model', 
        'description', 'distance', 'RX', 'mac', 'ip', 'olt', 'created'
    ]
    max_display_length = 1000
    html_panel = '<a data-click="panel-lateral" data-id="%s" data-panel="panel_onu_content">%s</a>'
    html_panel_serial = '<a data-click="panel-lateral" data-id="%s" data-serial="%s" data-ip="%s" data-panel="panel_onu_content">%s</a>'

    def render_column(self, row, column):
        if column == 'contador':
            self.id_current += 1
            return self.html_panel % (row.uuid, self.id_current)
        elif column == 'activate_service_button':
            if row.service is not None:
                return f'<button class="btn btn-success" \
                onclick="activateMatrixService({row.service.matrix_id}) \
                ">Activar Servicio {row.service.number} en Matrix</button>'
            else:
                return '<button class="btn btn-fail">\
                No cuenta con Número de Servicio</button>'
        elif column == 'corte':
            if row.service is not None:
                return f'<button class="btn btn-danger" \
                    onclick="corteDeServicio({row.service.number}) \
                    ">Cortar Servicio {row.service.number}</button>'
            else:
                return '<button class="btn btn-fail">\
                No cuenta con Número de Servicio</button>'
        elif column == 'reposicion':
            if row.service is not None:
                return f'<button class="btn btn-success" \
                    onclick="reposicionDeServicio({row.service.number}) \
                    ">Conectar Servicio {row.service.number}</button>'
            else:
                return '<button class="btn btn-fail">\
                No cuenta con Número de Servicio</button>'
        
        elif column == 'network_status':
            if row.service is not None:
                if row.service.seen_connected is True:
                    return '<button class="btn btn-success">En línea</button>'
                else:
                    return '<button class="btn btn-danger">Cortado</button>'
            else:
                return '<button class="btn btn-fail">\
                No cuenta con Número de Servicio</button>'

        elif column == 'primary_equipment':
            if row.service is not None:
                if row.primary:
                    # Si ya es primario lo indicamos
                    return '<button class="btn btn-info">Ya es el equipo primario</button>'
                else:
                    if row.ip is not None:
                        return f'<button class="btn btn-primary"\
                                 onclick="setPrimaryEquipment({row.service.number}, \'{row.ip.ip}\')">Asociar como equipo primario <br>\
                                 del servicio #{row.service.number}</button>'
                    else:
                        '<button class="btn btn-fail"> El equipo no cuenta con Dirección IP</button>'
            else:
                return '<button class="btn btn-fail">\
                No cuenta con Número de Servicio</button>'

        else:
            return super().render_column(row, column)

    def display_created(self, obj):
        try:
            update_time = obj.time_uptime.strftime("%d/%m/%Y %H:%M:%S")
        except:
            update_time = 'No disponible'
        return update_time

    def display_id(self, obj):
        return obj.id

    def display_olt(self, obj):
        return '<a href="#/devices/olt/%s/" data-toggle="ajax">%s</a>' % (
            obj.olt.uuid, obj.olt.alias)

    def get_initial_queryset(self):
        try:
            qs = self.model.objects.filter(status_field=True,
                                           olt__uuid=self.uuid)
        except:
            qs = self.model.objects.filter(status_field=True)

        filter_status = self._querydict.get('filter_status', None)
        filter_model = self._querydict.get('filter_model', None)
        filter_fsp = self._querydict.get('filter_fsp', None)
        filter_olt = self._querydict.get('filter_olt', None)
        filter_desprovisionadas = self._querydict.get('filter_desprovisionadas', None)

        if filter_desprovisionadas and filter_desprovisionadas != 'Provisionadas':
            qs = self.model.all_objects.filter(deleted_sentinel=True, olt__uuid=self.uuid)

        if filter_status and filter_status == 'ONLINE':
            qs = qs.filter(status=True)

        if filter_status and filter_status == 'OFFLINE':
            qs = qs.filter(status=False)


        if filter_model and filter_model != 'Modelo':
            qs = qs.filter(model__pk=filter_model)

        if filter_fsp and filter_fsp != 'F/S/P':
            try:
                fsp = filter_fsp.split('/')
                qs = qs.filter(frame=fsp[0], slot=fsp[1], port=fsp[2])
            except Exception as e:
                pass

        if filter_olt and filter_olt != '0':
            try:
                qs = qs.filter(olt__uuid=filter_olt)
            except Exception as e:
                pass

        return qs

    def display_serial(self, obj):
        return self.html_panel_serial % (obj.uuid, obj.serial, str(
            obj.ip), obj.serial)

    def display_uptime(self, obj):
        try:
            if obj.status:
                u = timezone.now() - obj.uptime
                return timedelta_format(u)
            return None
        except Exception as e:
            return None

    def display_RX(self, obj):
        click_template = 'data-click="panel-lateral" data-id="{id}" data-panel="panel_onu_content"'
        bar_template = """<div {click} class="progress" style="height: 10px; width:51%;">
        <div onclick="openPanel('{id}');panel_onu_content('#panel_right_content','{id}');" class="progress-bar {color}" style="width: {percent}%;"></div>
        </div><div {click} style="float: right; position:relative; top: -13px;">
        &nbsp;{signal}</div>"""
        signal = float(obj.RX) if obj.RX else ''
        percent = 0
        color_bar = ''

        if signal != '':
            if (signal > -10):
                html = """<div style="color:red;" data-click="panel-lateral" data-id="{}" 
                data-panel="panel_onu_content">Sobrecarga {}</div>"""
                return html.format(obj.uuid, str(signal) + ' dBm')
            elif signal == -10:
                color_bar = 'bg-lime'
                percent = 100
            elif (signal < -10) & (signal >= -18.9):
                percent = 75
                color_bar = 'bg-green-transparent-8'
            elif (signal < -18.9) & (signal >= -22):
                color_bar = 'bg-green-transparent-8 '
                percent = 55
            elif (signal < -22) & (signal >= -24.9):
                color_bar = 'bg-red-transparent-8 '
                percent = 35
            else:
                color_bar = 'bg-red'
                percent = 5

        return bar_template.format(click=click_template.format(id=obj.uuid),
                                   id=obj.uuid,
                                   color=color_bar,
                                   percent=percent,
                                   signal=str(signal) + ' dBm')

    def display_description(self, obj):
        return self.html_panel % (obj.uuid, obj.description)

    def display_model(self, obj):
        return self.html_panel % (obj.uuid, obj.model)

    def display_ip(self, obj):
        return self.html_panel % (obj.uuid, str(obj.ip.ip if obj.ip else None))

    def display_service_number(self, obj):
        return self.html_panel % (obj.uuid, obj.service.number)

    def display_mac(self, obj):
        try:
            mac = obj.mac.mac
        except:
            mac = None
        return mac

    def display_distance(self, obj):
        if obj.distance:
            return self.html_panel % (obj.uuid, '%d m' % obj.distance)
        return None

    def display_status(self, obj):
        if obj.status:
            return '<i class="fas fa-circle" style="color: green;"></i>'
        return '<i class="fas fa-circle" style="color: red;"></i>'

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = self.model.objects.filter(
                Q(serial__icontains=search) | Q(ip__ip__icontains=search)
                | Q(description__icontains=search.strip())
                | Q(model__model__icontains=search.strip())
                | Q(mac__mac__icontains=search.strip())
                | Q(service__number__exact=search.strip())
            )
            try:
                qs = qs.filter(status_field=True, olt__uuid=self.uuid)
            except:
                qs = qs.filter(status_field=True)

        filter_status = self._querydict.get('filter_status', None)
        filter_model = self._querydict.get('filter_model', None)
        filter_fsp = self._querydict.get('filter_fsp', None)
        filter_desprovisionadas = self._querydict.get('filter_desprovisionadas', None)
        
        if filter_desprovisionadas and filter_desprovisionadas != 'Provisionadas':
            qs = self.model.all_objects.filter(deleted_sentinel=True, olt__uuid=self.uuid)

        if filter_status and filter_status == 'ONLINE':
            qs = qs.filter(status=True)

        if filter_status and filter_status == 'OFFLINE':
            qs = qs.filter(status=False)
       
        if filter_model and filter_model != 'Modelo':
            qs = qs.filter(model__pk=filter_model)

        if filter_fsp and filter_fsp != 'F/S/P':
            try:
                fsp = filter_fsp.split('/')
                qs = qs.filter(frame=fsp[0], slot=fsp[1], port=fsp[2])
            except Exception as e:
                pass

        return qs


class DatatableVLANs(DataTable):
    model = SystemVLAN
    columns = [
        'vlan_id', 'vlan_type', 'description', 'operator', 'olts', 'botones'
    ]
    order_columns = [
        'vlan_id',
        'vlan_type',
        'description',
        'operator',
    ]
    max_display_length = 1000

    def render_column(self, row, column):
        if column == 'botones':
            btn = ButtonDataTable()
            btn.click("delete_vlans('%s')" % str(row.id), 'danger', 'user')
            return btn()
        else:
            return super().render_column(row, column)

    def display_olts(self, obj):
        try:
            return OLTVLAN.objects.filter(vlan_system=obj).count()
        except OLTVLAN.DoesNotExist:
            return 0

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = self.model.objects.filter(
                Q(vlan_type__name__icontains=search)
                | Q(description__icontains=search)
                | Q(operator__name__icontains=search))
        return qs


class DatatableOperator(DataTable):
    model = Operator
    columns = [
        'name',
        'code',
        'vlan1',
        'vlan2',
        'vlan3',
    ]
    order_columns = columns
    max_display_length = 1000

    def display_vlan1(self, o):
        return ''

    def display_vlan2(self, o):
        return ''

    def display_vlan3(self, o):
        return ''

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = self.model.objects.filter(
                Q(code__icontains=search) | Q(name__icontains=search))
        return qs


class DatatableOLTVLAN(DataTable):
    model = OLTVLAN
    columns = [
        'vlan_id', 'vlan_type', 'vlan_system', 'description', 'service_port',
        'associated_ports', 'botones'
    ]
    order_columns = [
        'vlan_id', 'vlan_type', 'vlan_system', 'description', 'service_port',
        'associated_ports'
    ]
    max_display_length = 1000

    def display_vlan_system(self, obj):
        if obj.vlan_system:
            return obj.vlan_system.vlan_id
        return 'Sin VLAN asociada'

    def render_column(self, row, column):
        if column == 'botones':
            btn = ButtonDataTable()
            t = '<a type="button" class="btn btn-info" onclick="{fn}"><i class="fas fa-exchange-alt"></i></a>'
            #<img style="width:18px;" src="/static/img/papelera.png">
            vlan_id = row.vlan_system.id if row.vlan_system else 0
            btn.list_btn.append(
                t.format(fn="vlanOltSystem(%d,%d)" % (row.id, vlan_id)))
            return btn()
        else:
            return super().render_column(row, column)

    def get_initial_queryset(self):
        try:
            id_d = self.model.objects.filter(status_field=True,
                                             olt__uuid=self.uuid)\
                   .latest('created').id_data
            qs = self.model.objects.filter(status_field=True,
                                           olt__uuid=self.uuid,
                                           id_data=id_d)
        except:
            qs = self.model.objects.none()
        return qs

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = self.model.objects.filter(
                Q(description__icontains=search)
                | Q(vlan_type__name__icontains=search))
        return qs


class DatatableOLTSpeeds(DataTable):
    model = OLTSpeeds
    columns = [
        'tid', 'name', 'cir', 'cbs', 'pir', 'pbs', 'pri', 'speeds_system',
        'botones'
    ]
    order_columns = [
        'tid', 'name', 'cir', 'cbs', 'pir', 'pbs', 'speeds_system', 'pri'
    ]
    max_display_length = 1000

    def render_column(self, row, column):
        if column == 'botones':
            btn = ButtonDataTable()
            t = '<a type="button" class="btn btn-info" onclick="{fn}"><i class="fas fa-exchange-alt"></i></a>'
            #<img style="width:18px;" src="/static/img/papelera.png">
            speeds_system = row.speeds_system.id if row.speeds_system else 0
            if row.name:
                name = str(row.tid) + " - " + row.name
            else:
                name = str(row.tid)

            btn.list_btn.append(
                t.format(fn="speedOltSystem(%d,%d,'%s')" %
                         (row.id, speeds_system, name)))
            btn.click("deleteSpeed('%s')" % str(row.id), 'danger', 'recycle')

            return btn()
        else:
            return super().render_column(row, column)

    def get_initial_queryset(self):
        try:
            id_d = self.model.objects.filter(status_field=True,
                                             olt__uuid=self.uuid)\
                   .exclude(id_data__isnull=True).latest('created')
            qs = self.model.objects.filter(status_field=True,
                                           olt__uuid=self.uuid,
                                           id_data=id_d.id_data)
            qs = qs | self.model.objects.filter(status_field=True,
                                                olt__uuid=self.uuid,
                                                created__gt=id_d.created)
        except:
            qs = self.model.objects.none()
        return qs

    def display_name(self, obj):
        if obj.name:
            return obj.name
        return ''

    def display_speeds_system(self, obj):
        if obj.speeds_system:
            return obj.speeds_system.tidsis
        return ''

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = self.model.objects.filter(olt__uuid=self.uuid).filter(
                Q(name__icontains=search) | Q(olt__ip__ip__icontains=search))
        return qs


class DatatableSystemSpeeds(DataTable):
    model = SystemSpeeds
    columns = ['name', 'tidsis', 'cir', 'cbs', 'pir', 'pbs', 'pri']
    order_columns = columns
    max_display_length = 1000

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = self.model.objects.filter(
                Q(name__icontains=search) | Q(cir__icontains=search))
        return qs


class DatatablePlan(DataTable):
    model = Plan
    columns = [
        'id', 'operator', 'name', 'type_plan', 'download_speed',
        'upload_speed', 'aggregation_rate', 'matrix_plan', 'botones'
    ]
    order_columns = columns
    max_display_length = 1000

    def display_aggregation_rate(self, obj):
        if obj.aggregation_rate:
            return "1:%d" % obj.aggregation_rate
        return ''

    def display_upload_speed(self, obj):
        if obj.upload_speed:
            return "%d Mbps" % obj.upload_speed
        return ''

    def display_download_speed(self, obj):
        if obj.download_speed:
            return "%d Mbps" % obj.download_speed
        return ''

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = self.model.objects.filter(
                Q(name__icontains=search)
                | Q(type_plan__name__icontains=search))
        return qs

    def render_column(self, row, column):
        if column == 'botones':
            btn = ButtonDataTable()
            btn.click("editPlan('%s')" % str(row.id), 'info', 'edit')
            return btn()
        else:
            return super().render_column(row, column)


class DatatablePortOLT(DataTable):
    model = PortOLT
    columns = ['id', 'fsp_field', 'description', 'onus_active', 'onus']
    order_columns = columns
    max_display_length = 1000
    html_panel = '<a data-click="panel-lateral" data-id="%s" data-panel="panel_fsp_content">%s</a>'

    def get_initial_queryset(self):
        try:
            id_d = self.model.objects.filter(status_field=True,
                                             interface__olt__uuid=self.uuid)\
                   .latest('created').id_data
            qs = self.model.objects.filter(status_field=True,
                                           interface__olt__uuid=self.uuid,
                                           id_data=id_d)
        except:
            qs = self.model.objects.none()
        return qs

    def display_id(self, obj):
        return self.html_panel % (obj.id, obj.id)

    def display_description(self, obj):
        return self.html_panel % (obj.id, obj.description)

    def display_fsp_field(self, obj):
        fsp = "{0.interface.frame}/{0.interface.slot}/{0.port}".format(obj)
        return self.html_panel % (obj.id, fsp)

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = self.model.objects.filter(Q(description__icontains=search))
        return qs


class DatatableInterfacesOLT(DataTable):
    model = InterfaceOLT
    columns = [
        'slot', 'interface', 'onus', 'onus_active', 'ports', 'ports_active',
        'serial', 'status', 'in_used'
    ]
    order_columns = columns
    max_display_length = 1000

    def get_initial_queryset(self):
        try:
            id_d = self.model.objects.filter(status_field=True,
                                             olt__uuid=self.uuid)\
                   .latest('created').id_data
            qs = self.model.objects.filter(status_field=True,
                                           olt__uuid=self.uuid,
                                           id_data=id_d)
        except:
            qs = self.model.objects.none()
        return qs

    def filter_queryset(self, qs):
        search = self.request.POST.get('search[value]', None)
        if search:
            qs = self.model.objects.filter(Q(interface__icontains=search))
        return qs

    def display_status(self, obj):

        if obj.status in ['Normal', 'Active_normal']:
            status = '<span style="color: green">%s</span>' % obj.status
        elif obj.status == 'Active_standby':
            status = '<span style="color: #c6c41e">%s</span>' % obj.status
        else:
            status = '<span style="color: red">%s</span>' % obj.status

        alert = ''
        alert_error = """
        <i title="Modelo incorrecto para funcion del slot"
        class="fas fa-info-circle fa-fw"  
        style="color: red;"></i>
        """
        if obj.slot in [1, 2, 3, 4, 5, 6, 7, 10, 11, 12, 13, 14, 15, 16, 17]:
            if obj.interface != '' and obj.interface != 'H901GPHF':
                alert = alert_error
        elif obj.slot in [8, 9]:
            if obj.interface != '' and obj.interface not in [
                    'H901MPLA', 'H902MPLA'
            ]:
                alert = alert_error
        elif obj.slot == 0:
            if obj.interface != '' and obj.interface != 'H901CIUA':
                alert = alert_error
        elif obj.slot in [18, 19]:
            if obj.interface != '' and obj.interface != 'H901PILA':
                alert = alert_error
        return status + " " + alert

    def display_in_used(self, obj):
        if obj.in_used:
            return '<i class="fas fa-arrow-right fa-rotate-270" style="color: green;"></i>'
        return '<i class="fas fa-arrow-right fa-rotate-90" style="color: red;"></i>'


class DatatableOLTUsers(DataTable):
    model = OLTUser
    columns = [
        'name', 'level', 'online', 'reenter_num', 'profile', 'append_info'
    ]
    order_columns = columns
    max_display_length = 1000

    def display_online(self, obj):
        return 'si' if obj.online else 'no'

    def display_level(self, obj):
        if obj.level == 1:
            return 'Super'
        elif obj.level == 2:
            return 'Admin'
        else:
            return 'Operator'

    def get_initial_queryset(self):
        try:
            id_d = self.model.objects.filter(status_field=True,
                                             olt__uuid=self.uuid)\
                   .latest('created').id_data
            qs = self.model.objects.filter(status_field=True,
                                           olt__uuid=self.uuid,
                                           id_data=id_d)
        except:
            qs = self.model.objects.none()
        return qs

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = qs.filter(Q(nombre__icontains=search))
        return qs


class DatatableIPTable(DataTable):
    model = IPTable
    columns = ['ip', 'host', 'menu', 'title', 'botones']
    order_columns = columns
    max_display_length = 5000

    def get_initial_queryset(self):
        qs = super().get_initial_queryset()
        if 'menu' in self.kwargs:
            qs = (MenuSmokeping.objects.get(
                pk=self.kwargs['menu']).iptable.all())
        return qs

    def btn_detail(self, row):
        t = """<a type="button" onclick="return {fn}" class="btn btn-default btn-xs" >
        <i class="fa fa-eye" title="detalle"></i></a>"""

        obj = {
            'ip': row.ip,
            'host': row.host,
            'menu': row.menu,
            'title': row.title,
        }

        return t.format(fn="modalDetail(%s)" % str(obj))

    def btn_update(self, row):
        t = """<a type="button" onclick="return {fn}" class="btn btn-warning btn-xs" >
        <i class="fa fa-edit text-inverse" title="Edición"></i></a>"""
        obj = {
            'ip': row.ip,
            'host': row.host,
            'menu': row.menu,
            'title': row.title,
            'id': row.id,
        }
        return t.format(fn="loadModalEdit(%s)" % str(obj))

    def btn_delete(self, row):
        t = """<a type="button" onclick="return {fn}" class="btn btn-danger btn-xs" >
        <i class="fa fa-trash text-inverse" title="Eliminación"></i></a>"""
        obj = {
            'pk': row.id,
            'ip': row.ip,
        }
        return t.format(fn="deleteIP('%s')" % str(row.id))

    def render_column(self, row, column):
        if column == 'botones':
            btn = ButtonDataTable()

            btn.list_btn.append(self.btn_detail(row))
            btn.list_btn.append(self.btn_update(row))
            btn.list_btn.append(self.btn_delete(row))

            return btn()

        if column in ['ip', 'host', 'menu', 'title']:
            return getattr(row, column)

        else:
            return super().render_column(row, column)


class DatatableMenuSmokeping(DataTable):
    model = MenuSmokeping
    columns = ['id', 'title', 'order_field', 'counterIps', 'botones']
    order_columns = columns
    max_display_length = 500

    def btn_add(self, row):
        t = """<a type="button" onclick="return {fn}" class="btn btn-default btn-xs" >
        <i class="fa fa-eye text-inverse" title="Detalle del menú"></i></a>"""
        iptable = list(
            map(
                lambda obj: {
                    'ip': obj.ip,
                    'host': obj.host,
                    'menu': obj.menu,
                    'title': obj.title
                }, row.iptable.all()))
        obj = {
            'pk': row.id,
            'title': row.title,
            'order_field': row.order_field,
            'iptable': iptable,
        }
        return t.format(fn='detailMenu(%s)' % str(obj))

    def btn_update(self, row):
        t = """<a type="button" onclick="return {fn}" class="btn btn-warning btn-xs" >
        <i class="fa fa-edit text-inverse" title="Edición"></i></a>"""
        obj = {
            'id':
            row.id,
            'title':
            row.title,
            'order_field':
            row.order_field,
            'iptable': [{
                'id': obj.id,
                'text': obj.ip
            } for obj in row.iptable.all()],
        }
        return t.format(fn="loadModalEdit(%s)" % str(obj))

    def btn_delete(self, row):
        t = """<a type="button" onclick="return {fn}" class="btn btn-danger btn-xs" >
        <i class="fa fa-trash text-inverse" title="Eliminación"></i></a>"""
        json = dict(id=row.id, title=row.title)
        return t.format(fn="deleteMenu(%s)" % str(json))

    def render_column(self, row, column):
        if column == 'botones':
            btn = ButtonDataTable()
            btn.list_btn.append(self.btn_add(row))
            btn.list_btn.append(self.btn_update(row))
            btn.list_btn.append(self.btn_delete(row))

            return btn()
        if column == 'title':
            return f'<a href="#/smokeping-detail/{row.id}">{row.title}</a>'

        if column in [
                'id',
                'order_field',
        ]:
            return getattr(row, column)
        elif column == 'counterIps':
            return str(row.counterIps)

        if row.order_field == None:
            row.order_field = 0
            return super().render_column(row, column)
        else:
            return super().render_column(row, column)
