from django import forms
from dal import autocomplete
from .models import IPTable,MenuSmokeping
from django.urls import reverse


class MenuSmokepingForm(forms.ModelForm):
    class Meta:
        model = MenuSmokeping 
        fields = ['id','title','order_field','iptable']

        widgets = {
            # 'title':forms.CharField(max_length=150),
            # 'order_field':forms.IntegerField(),
            'iptable':(autocomplete.
                        ModelSelect2Multiple(url='/api/v1/iptable-ac/',
                                            attrs={'class':'form-control',
                                                    'style':'width:100%',
                                                    'data-placeholder': 'IPs',
                                                    'data-minimum-input-length': 3,
                                                    'data-dropdownParent': '#modal_create',}))
        }