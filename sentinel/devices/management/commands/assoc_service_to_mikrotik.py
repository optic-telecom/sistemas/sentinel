import time
import re
from django.utils import timezone

from common.command import BaseCommand
from devices.models import ONU
from utp.models import CpeUtp
from customers.models import Service


class Command(BaseCommand):
    help = "*** Comando encargado de asociar ONU's y CpeUtp's a servicios dentro de sentinel ***"

    def find_service_number(self,instance,description):

        coincidences = re.findall('\d{5}', description)
        
        if len(coincidences) == 1:
            instance.service, created = Service.objects.get_or_create(number=coincidences[0])
        else:
            new_coincidences = re.findall('\d{4}',description)
            if len(new_coincidences) > 0:
                instance.service, created = Service.objects.get_or_create(number=new_coincidences[0])
            else:
                instance.service = None

        return instance.service

    def assocOnus(self):
        onus = ONU.objects.all()
        try:
            for onu in onus:
                print(f'Asociando la onu: {onu} al servicio {onu.service}')
                description = onu.service.number if onu.service else onu.description
                onu.service = self.find_service_number(onu,str(description))
                if onu.service and onu.olt:
                    onu.service.node = onu.olt.node
                    onu.service.save()
                onu.save()
        except Exception as e:
            print(str(e))

    def assocCpes(self):
        cpes = CpeUtp.objects.all()
        try:
            for cpe in cpes:
                print(f'Asociando el cpe: {cpe} al servicio {cpe.service}')
                description = cpe.service.number if cpe.service else cpe.description
                cpe.service = self.find_service_number(cpe,str(description))
                if cpe.service and cpe.nodo_utp:
                    cpe.service.node = cpe.nodo_utp
                    cpe.service.save()
                cpe.save()
        except Exception as e:
            print(str(e))

    def handle(self, *args, **options):
        print(self.help)
        print("=== Asociando ONU's ===")
        self.assocOnus()
        print("=== Asociando CPE's ===")
        self.assocCpes()
            