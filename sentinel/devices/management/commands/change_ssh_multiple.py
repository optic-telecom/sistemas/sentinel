import ipaddress  
from time import sleep
from django.utils import timezone
from datetime import timedelta
from django.db.models import Max

from common.command import BaseCommand
from devices.models import OLTConnection, SSHConnection

class Command(BaseCommand):

    help = 'Para cambiar de SSHConnection a OLTConnection que permite multiples conexiones ssh'
    
    def handle(self, *args, **options):
        
        all_ssh = SSHConnection.objects.all()
        for conn in all_ssh:
            OLTConnection.objects.create(
                olt=conn.olt,
                username=conn.username,
                password=conn.password,
            )
            