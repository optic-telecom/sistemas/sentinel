from common.command import BaseCommand
import subprocess as subp
import datetime
import os


class Command(BaseCommand):

    help = 'Comprimir los logs SSH de sentinel en un tarball'

    def handle(self, *args, **options):

        """
        El archivo final tiene el formato semana_XX-XX_mes_año.tar.gz
        """

        # Obtenemos la fecha actual (un viernes, que es cuando se ejecuta)
        friday = datetime.date.today()

        # Obtenemos la fecha del lunes
        monday = friday - datetime.timedelta(days=4)

        # Obtenemos el mes
        month = friday.month

        # Obtenemos el año
        year = friday.year

        os.chdir('/home/sentinel/sentinel_backend/logs')

        subp.run([f'tar cvzf semana_{monday.day}-{friday.day}_{month}_{year}.tar.gz ssh_2021*.log --remove-files'], shell=True)
