from django.apps import apps
from django.db.models import GenericIPAddressField
from devices.models import IPTable

from common.command import BaseCommand

class Command(BaseCommand):
    help = "llena la tabla IPTable con todas las ip's ya \
        guardadas en los otros modelos que no esten dentro de esta tabla"
 

    def add_arguments(self, parser):
        parser.add_argument('apps', metavar='a',nargs='+', type=str, help='Define las apps a revisar',)
        parser.add_argument('-m', '--models', nargs='+', type=str, help='Define los modelos a revisar',)
        parser.add_argument('-f', '--fields', nargs='+', type=str, help='Define los campos a revisar',)
        
    def handle(self, *args, **options):
        appss = options['apps']
        modelss = options['models']
        fields = options['fields']

        fill_iptable(appss, modelss, fields)    
        
        self.stdout.write("Se ha completado el llenado")


def fill_iptable(appss, modelss=None, fields=None):  
    """ Funcion que llena la tabla IPTable con todas las ip's ya 
        guardadas en los otros modelos que no esten dentro de esta tabla

        Devuelve void

        Parametros:
            appss: Lista de apps que se desea revisar.
            modelss: Lista de modelos que se desea revisar. Por defecto busca en todos los modelos
                        El nombre del los modelos debe ser todo en minuscula
            fields: lista de campos que desea. 
    """
    for app in appss:
     
        models = list(apps.all_models[app].items())
        
        for model, model_class in models:
            
            if ( (modelss is None) or (modelss is not None and model in modelss) ):
                print(" ----- " , model)
                if (not 'historical' in model and not 'iptable' in model):    
                    
                    for field in model_class._meta.get_fields():
                        # print(type(field), end=', ')
                        if type(field) == GenericIPAddressField or field.name == 'ip' and ((fields is None) or (fields is not None and field.name in fields)):
                            # print(model, field.name)
                            
                            for i in model_class.objects.values(field.name):
                                print(i['ip'], end=', ')

                                try:
                                    ip_exists = IPTable.objects.get(ip=i['ip'])
                                    print('{} existe en IPTable'.format(i['ip']))    
                                except IPTable.DoesNotExist:
                                    ip_exists = IPTable(ip=i['ip']).save()
                                    print('{} NO existe en IPTable'.format(i['ip']))
                        