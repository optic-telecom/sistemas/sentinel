import ipaddress  
from time import sleep
from django.utils import timezone
from datetime import timedelta
from django.db.models import Max

from common.command import BaseCommand
from devices.wrappers import OLTWrapperSSH
from devices.models import PortOLT, OLT

class Command(BaseCommand):

    def set_field(self, qs):
        for puerto in qs:
            if puerto.port == 0:
                factor = 0.9
            else:
                factor = puerto.port
            if puerto.interface.slot == 1:
                c = factor * 1
            if puerto.interface.slot == 2:
                c = factor * 20
            if puerto.interface.slot == 3:
                c = factor * 350
            if puerto.interface.slot == 4:
                c = factor * 6000
            if puerto.interface.slot == 5:
                c = factor * 101000
            if puerto.interface.slot == 6:
                c = factor * 1712000
            if puerto.interface.slot == 7:
                c = factor * 29000000
            if puerto.interface.slot == 8: 
                c = factor * 495000000
            print (puerto.interface.slot, puerto.port, c)
            puerto.fsp_field = c
            puerto.save()

    def handle(self, *args, **options):
        
        olts = OLT.objects.all()
        for olt in olts:
            print ("_______OLT_____")
            _all = PortOLT.objects.filter(interface__olt=olt)
            self.set_field(_all)