import time

from django.utils import timezone

from common.command import BaseCommand
from devices.wrappers import OLTWrapperSSH
from devices.models import OLT, ONU
from dashboard.models import TaskModel
from dashboard.utils import LOG


class Command(BaseCommand):
    help = 'Obtiene información de las OLTs y la almacena en la base de datos'

    def getdata(self, olt, pre_olt_ssh=None, id_data=None, **options):

        _l = LOG()
        _id_data = id_data if id_data is not None else self.command_uuid
        obj_to_log = {'by_representation': self.get_by(**options)}

        if not pre_olt_ssh:
            _ssh = OLTWrapperSSH(olt, **{'_l': _l, **options})
            olt_ssh = _ssh.get_me()
            try:
                olt_ssh.shell(olt, self.get_by(**options))
                olt.set_status_online()
                _l.OLT.SHELL(locals())
            except Exception as e:
                olt.set_status_offline()
                _l.OLT.SHELL(locals())
                print("Error getdata get_all_info", str(e))
                return False
        else:
            olt_ssh = pre_olt_ssh

        TaskModel.objects.get_or_create(id_obj=olt.id,
                                        model_obj='olt',
                                        name='get_all_info_olt',
                                        status='PENDING')

        from devices.management.commands.getolts import Command as NewOLT
        command = NewOLT()
        command.getdata(olt, olt_ssh, **options)
        time.sleep(1)

        from devices.management.commands.get_vlans_olts import Command as VLANS
        command = VLANS()
        command.getdata(olt, olt_ssh, **options)
        time.sleep(2)

        from devices.management.commands.getonus import Command as Getonus
        command = Getonus()
        command.getdata(olt, olt_ssh, **options)
        time.sleep(1)

        from devices.management.commands.get_info_onus import Command as GetInfoOnus
        command = GetInfoOnus()
        command.getdata(olt, olt_ssh, **options)
        time.sleep(1)

        from devices.management.commands.get_wifi_onus import Command as GetWifiOnus
        command = GetWifiOnus()
        command.getdata(olt, olt_ssh, **options)
        time.sleep(1)

        onus = ONU.objects.filter(olt=olt)
        from devices.management.commands.get_ip_onus import Command as GetIpsOnus
        command = GetIpsOnus()
        command.getdata(onus, olt_ssh, **{'pre_status': True})
        time.sleep(0.85)

        # interfaces and ports
        from devices.management.commands.get_fsp_olts import Command as GetFSP
        cmd = GetFSP()
        cmd.getdata(olt, olt_ssh, **options)
        # end interfaces

        # salud olt
        from devices.management.commands.get_health_olts import Command as HealthOLTs
        cmd = HealthOLTs()
        cmd.getdata(olt, olt_ssh, **options)
        # end

        try:
            tasks = TaskModel.objects.filter(id_obj=olt.id,
                                             model_obj='olt',
                                             name='get_all_info_olt',
                                             status='PENDING')
            for t in tasks:
                t.date_done = timezone.now()
                t.result = True
                t.status = 'SUCCESS'
                t.save()

        except Exception as e:
            pass

        # Eliminamos la conexión ssh luego de terminar el proceso
        del olt_ssh

    def handle(self, *args, **options):
        all_olt = OLT.objects.filter(status_field=True)
        for olt in reversed(all_olt):
            try:
                self.getdata(olt, **options)
            except Exception as e:
                print(e)
                pass
