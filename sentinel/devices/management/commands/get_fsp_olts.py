import re
import time
import os
import sys

from django.utils import timezone

from common.command import BaseCommand
from devices.wrappers import OLTWrapperSSH
from devices.models import OLT, InterfaceOLT, PortOLT,\
    TYPE_ENERGY, TYPE_CLOCK, TYPE_SERVICE, TYPE_CONTROLLER, ONU
from devices.management.commands.fsp_field import Command as SetFSPField
from dashboard.utils import LOG


class Command(BaseCommand):
    help = 'Obtiene y almacena las interfaces y puertos para las olts'

    def getdata(self, olt, pre_olt_ssh=None, id_data=None, **options):

        _l = LOG()
        _id_data = id_data if id_data is not None else self.command_uuid
        obj_to_log = {'by_representation': self.get_by(**options)}

        if not pre_olt_ssh:
            options['_l'] = _l
            _ssh = OLTWrapperSSH(olt, **options)
            olt_ssh = _ssh.get_me()
            try:
                olt_ssh.shell(olt, self.get_by(**options))
                olt.set_status_online()
                _l.OLT.SHELL(locals())
            except Exception as e:
                olt.set_status_offline()
                _l.OLT.SHELL(locals())
                print("Error getdata get_fsp_olts", str(e))
                return False
        else:
            olt_ssh = pre_olt_ssh

        interfaces_olt = olt_ssh.get_all_cards() if True else []
        interfaces_serial = olt_ssh.get_serials_all_cards() if True else []

        for i, line in enumerate(interfaces_olt, 0):
            if i < 8:
                continue

            reg = line.split()
            if len(reg) > 0 and reg[0].startswith('-'):
                continue

            try:
                int(reg[0])
            except:
                continue

            #print (reg)

            try:
                iface = InterfaceOLT.objects.filter(slot=reg[0],
                                                    olt=olt).last()
            except InterfaceOLT.DoesNotExist:
                iface = None

            if iface:

                try:
                    interface = InterfaceOLT()

                    if reg[1] == '':
                        interface.in_used = False
                    else:
                        iface_ram = olt_ssh.get_ram(reg[0])
                        iface_cpu = olt_ssh.get_cpu(reg[0])

                        if reg[1] == 'H901CIUA':
                            iface_type = TYPE_CLOCK
                        elif reg[1] == 'H901GPHF':
                            iface_type = TYPE_SERVICE
                        elif reg[1] in ['H901MPLA', 'H902MPLA']:
                            iface_type = TYPE_CONTROLLER
                        elif reg[1] == 'H901PILA':
                            iface_type = TYPE_ENERGY

                        interface.interface_type = iface_type
                        interface.interface = reg[1]
                        interface.ram = iface_ram
                        interface.cpu = iface_cpu

                        interface.in_used = True
                        interface.status = reg[2]
                        for serial in interfaces_serial:
                            if serial[0] == reg[0]:
                                interface.serial = serial[1]

                    interface.slot = reg[0]
                    interface.olt = olt
                    interface.id_data = _id_data
                    interface.save()

                except IndexError as e:
                    interface = InterfaceOLT()
                    interface.slot = reg[0]
                    interface.olt = olt
                    interface.in_used = False
                    interface.id_data = _id_data
                    interface.save()
                    continue

            else:
                interface = InterfaceOLT()
                interface.slot = reg[0]
                interface.olt = olt
                interface.id_data = _id_data
                try:

                    print("Aquiiiiiiiiiiii", reg[1])

                    if reg[1] == 'H901CIUA':
                        iface_type = TYPE_CLOCK
                    elif reg[1] == 'H901GPHF':
                        iface_type = TYPE_SERVICE
                    elif reg[1] in ['H901MPLA', 'H902MPLA']:
                        iface_type = TYPE_CONTROLLER
                    elif reg[1] == 'H901PILA':
                        iface_type = TYPE_ENERGY

                    iface_ram = olt_ssh.get_ram(reg[0])
                    iface_cpu = olt_ssh.get_cpu(reg[0])
                    interface.interface_type = iface_type
                    interface.interface = reg[1]
                    interface.ram = iface_ram
                    interface.cpu = iface_cpu
                    interface.in_used = True
                    interface.status = reg[2]
                    for serial in interfaces_serial:
                        if serial[0] == reg[0]:
                            interface.serial = serial[1]

                except IndexError as e:
                    interface.in_used = False

                interface.save()
                _l.interface.add(locals())

        ## end interfaces
        ifaces_services = InterfaceOLT.objects.filter(
            interface_type=TYPE_SERVICE, olt=olt, id_data=_id_data)
        for iface_slot in ifaces_services:

            ports = olt_ssh.get_all_ports(iface_slot.slot)

            _l.OLT.SHELL(by_representation='system',
                         id_value=olt.uuid,
                         description='display port desc 0/%d' %
                         iface_slot.slot,
                         id_data=_id_data,
                         entity_representation=repr(OLT))

            for port in ports:
                new_port = PortOLT()
                new_port.frame = 0
                new_port.slot = iface_slot.slot
                new_port.port = port[0]
                new_port.description = port[1]
                new_port.id_data = _id_data
                new_port.interface = iface_slot

                onus_total = ONU.objects.filter(olt=olt,
                                                slot=iface_slot.slot,
                                                port=port[0])
                onus_total_active = onus_total.filter(status=True)
                new_port.onus = onus_total.count()
                new_port.onus_active = onus_total_active.count()
                new_port.save()

                _l.PortOLT.ADD(by_representation='system',
                               id_value=new_port.pk,
                               description='Nuevo puerto OLT',
                               id_data=_id_data,
                               entity_representation=repr(new_port))

                _l.interface.COUNTERS_UPDATE(
                    by_representation='system',
                    id_value=iface_slot.pk,
                    entity_representation=
                    f'INTERFACE 0/{iface_slot.slot}/{port[0]}',
                    description=
                    f'OLD: onus({iface_slot.onus}) onus_active({iface_slot.onus_active})',
                    id_data=_id_data)

                iface_slot.onus += onus_total.count()
                iface_slot.onus_active += onus_total_active.count()

                _l.interface.COUNTERS_UPDATE(
                    by_representation='system',
                    id_value=iface_slot.pk,
                    entity_representation=
                    f'INTERFACE 0/{iface_slot.slot}/{port[0]}',
                    description=
                    f'NEWS: onus({iface_slot.onus}) onus_active({iface_slot.onus_active})',
                    id_data=_id_data)

                iface_slot.save()

            count_ports = PortOLT.objects.filter(
                interface__slot=iface_slot.slot,
                interface__olt=olt,
                id_data=_id_data)
            iface_slot.ports = count_ports.count()
            iface_slot.ports_active = count_ports.filter(
                onus_active__gt=0).count()
            iface_slot.save()

        cmd = SetFSPField()
        cmd.set_field(
            PortOLT.objects.filter(interface__olt=olt, id_data=_id_data))

        count_onus = ONU.objects.filter(olt=olt)
        count_ifases = InterfaceOLT.objects.filter(olt=olt, id_data=_id_data)

        _l.olt.COUNTERS_UPDATE(by_representation='system',
                               id_value=olt.uuid,
                               id_data=_id_data,
                               description='OLD: onus(%d) onus_active(%d)' %
                               (olt.onus, olt.onus_active),
                               entity_representation=repr(OLT))

        # olt.onus = count_onus.count()
        # olt.onus_active = count_onus.filter(status=True).count()
        olt.cards = count_ifases.count()
        olt.cards_active = count_ifases.filter(in_used=True).count()
        olt.save()
        _l.olt.COUNTERS_UPDATE(by_representation='system',
                               id_value=olt.uuid,
                               id_data=_id_data,
                               description='NEWS: onus(%d) onus_active(%d)' %
                               (olt.onus, olt.onus_active),
                               entity_representation=repr(OLT))

        # Eliminamos la conexión ssh una vez terminado el proceso
        del olt_ssh

    def add_arguments(self, parser):
        parser.add_argument(
            '-olt',
            '--oltpk',
            type=str,
            help='Define la olt a obtener',
        )
        parser.add_argument(
            '--cron',
            action='store_true',
            dest='is_in_cron',
            help='Establece si se esta ejecutando o no en modo cron',
        )

    def handle(self, *args, **options):
        from uuid import UUID
        options['close_ssh'] = True

        if options['oltpk']:
            try:
                UUID(options['oltpk'], version=4)
                filter_by = {'uuid': options['oltpk']}
            except ValueError:
                filter_by = {'pk': options['oltpk']}
            try:
                olt = OLT.objects.get(**filter_by)
                return self.getdata(olt, **options)
            except OLT.DoesNotExist:
                pass

        all_olt = OLT.objects.filter(status_field=True)
        for olt in all_olt:
            self.getdata(olt, **options)