import time
import os

from django.utils import timezone

from common.command import BaseCommand
from devices.wrappers import OLTWrapperSSH
from devices.models import OLT
from dashboard.utils import LOG


class Command(BaseCommand):
    help = 'Obtiene informacion de las OLTs y la almacena en la base de datos'

    def getdata(self, olt, pre_olt_ssh=None, id_data=None, **options):

        _l = LOG()
        _id_data = id_data if id_data is not None else self.command_uuid
        obj_to_log = {'by_representation': self.get_by(**options)}

        if not pre_olt_ssh:
            _ssh = OLTWrapperSSH(olt, **{'_l': _l, **options})
            olt_ssh = _ssh.get_me()
            try:
                olt_ssh.shell(olt, self.get_by(**options))
                olt.set_status_online()
                _l.OLT.SHELL(locals())
            except Exception as e:
                olt.set_status_offline()
                _l.OLT.SHELL(locals())
                print("Error getdata get_health_olts", str(e))
                return False
        else:
            olt_ssh = pre_olt_ssh

        ram = olt_ssh.get_ram() if True else ''
        ram_secondary = olt_ssh.get_ram(9) if True else ''
        watts = olt_ssh.get_power()
        cpu = olt_ssh.get_cpu()
        cpu_secondary = olt_ssh.get_cpu(9)
        temperature = olt_ssh.get_temperature()
        temperature_secondary = olt_ssh.get_temperature(9)

        now = timezone.now()

        olt.ram = ram
        olt.cpu = cpu
        olt.temperature = temperature
        olt.status = True
        olt.time_uptime = now
        olt.ram_secondary = ram_secondary
        olt.cpu_secondary = cpu_secondary
        olt.temperature_secondary = temperature_secondary
        olt.watts = watts
        olt.save()

        # MoreDetailOLTS.objects.create(
        #     olt = olt,
        #     ram = ram,
        #     ram_secondary = ram_secondary,
        #     cpu_secondary = cpu_secondary,
        #     temperature_secondary = temperature_secondary,
        #     cpu = cpu,
        #     temperature = temperature,
        #     watts = watts,
        #     time = now
        # )

        if options.get('close_ssh', False):
            del olt_ssh

    def add_arguments(self, parser):
        parser.add_argument(
            '-olt',
            '--oltpk',
            type=str,
            help='Define la olt a obtener',
        )
        parser.add_argument(
            '-olts',
            type=str,
            action='append',
            default=[],
            help='Define las olt a obtener',
        )
        parser.add_argument(
            '--cron',
            action='store_true',
            dest='is_in_cron',
            help='Establece si se esta ejecutando o no en modo cron',
        )

    def handle(self, *args, **options):
        options['close_ssh'] = True

        if 'oltpk' in options:
            olt = OLT.objects.get(pk=options['oltpk'])
            self.getdata(olt, **options)
        elif 'olts' in options and options['olts'] != []:
            all_olt = OLT.objects.filter(pk__in=options['olts'])
            for olt in all_olt:
                self.getdata(olt, **options)
        else:
            all_olt = OLT.objects.filter(status_field=True)
            for olt in reversed(all_olt):
                try:
                    self.getdata(olt, **options)
                except Exception as e:
                    print(e)
                    pass
