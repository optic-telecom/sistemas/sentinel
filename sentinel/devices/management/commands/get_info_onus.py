import re
from django.utils import timezone
from dateutil.relativedelta import relativedelta

from common.command import BaseCommand
from customers.models import Service
from devices.wrappers import OLTWrapperSSH
from devices.models import OLT, ONU, FirmwareONUS
from dashboard.utils import LOG


def end_command():
    try:
        print('\a')
    except Exception as e:
        pass


class Command(BaseCommand):
    help = 'Obtiene y almacena datos ont/onu de la/las OLT'
    sender = 'devices.management.commands.get_info_onus.end_command'

    def getdata(self, olt, pre_olt_ssh=None, id_data=None, **options):

        _l = LOG()
        _id_data = id_data if id_data is not None else self.command_uuid
        obj_to_log = {'by_representation': self.get_by(**options)}

        if not pre_olt_ssh:
            _ssh = OLTWrapperSSH(olt, **{'_l': _l, **options})
            olt_ssh = _ssh.get_me()
            try:
                olt_ssh.shell(olt, self.get_by(**options))
                olt.set_status_online()
                _l.OLT.SHELL(locals())
            except Exception as e:
                olt.set_status_offline()
                _l.OLT.SHELL(locals())
                print("Error getdata get_info_onus", str(e))
                return False
        else:
            olt_ssh = pre_olt_ssh

        all_onus = ONU.objects.filter(olt=olt)

        if options.get('reversed', None):
            all_onus = reversed(all_onus)

        for onu in all_onus:

            onu_version = olt_ssh.get_version_onu(onu.frame, onu.slot,
                                                  onu.port, onu.onu_id)
            if onu_version[0]:
                onu.main_software, onu.standby_software = onu_version

                firm = FirmwareONUS()
                firm.onu = onu
                firm.main_software, firm.standby_software = onu_version
                firm.save()

            data_onu = olt_ssh.get_info_onu(onu.serial)
            uptime = data_onu.get('uptime', None)
            if uptime:
                u = uptime.split(', ')
                dias = int(u[0].replace('d', ''))
                horas = int(u[1].replace('h', ''))
                minutos = int(u[2].replace('m', ''))
                segundos = int(u[3].replace('s', ''))
                now = timezone.now()
                value_uptime = now + relativedelta(days=-dias,
                                                   hours=-horas,
                                                   minutes=-minutos,
                                                   seconds=-segundos)
                onu.uptime = value_uptime
                onu.time_uptime = now
                onu.status = True

                # s = StatusONUS()
                # s.onu = onu
                # s.status = True
                # s.save()

            # details = MoreDetailONUS.objects.create(onu=onu)

            line_profile_name = data_onu.get('line_profile_name', None)
            if line_profile_name:
                onu.line_profile_name = line_profile_name
                # details.line_profile_name = line_profile_name

            line_profile_id = data_onu.get('profile_id', None)
            if line_profile_id:
                onu.line_profile_id = line_profile_id.replace(':', '')
                # details.line_profile_id = line_profile_id.replace(':','')

            TR069_ip_index = data_onu.get('ip_index', None)
            if TR069_ip_index:
                onu.TR069_ip_index = TR069_ip_index.replace(':', '')
                # details.TR069_ip_index = TR069_ip_index.replace(':','')

            TR069_management = data_onu.get('management', None)
            if TR069_management:
                if TR069_management == ':Disable':
                    onu.TR069_management = False
                    # details.TR069_management = False
                else:
                    onu.TR069_management = True
                    # details.TR069_management = True

            mapping_mode = data_onu.get('mapping_mode', None)
            if mapping_mode:
                onu.mapping_mode = mapping_mode.replace(':', '')
                # details.mapping_mode = mapping_mode.replace(':','')

            config_state = data_onu.get('config_state', None)
            if config_state:
                onu.config_state = config_state
                # details.config_state = config_state

            run_state = data_onu.get('run_state', None)
            if run_state:
                onu.run_state = run_state
                # details.run_state = run_state

            control_flag = data_onu.get('control_flag', None)
            if control_flag:
                onu.control_flag = control_flag
                # details.control_flag = control_flag

            ram = data_onu.get('ram', None)
            if ram:
                onu.ram = ram.replace('%', '')
                # details.ram = ram.replace('%','')

            cpu = data_onu.get('cpu', None)
            if cpu:
                onu.cpu = cpu.replace('%', '')
                # details.cpu = cpu.replace('%','')

            temperature = data_onu.get('temperature', None)
            if temperature:
                onu.temperature = temperature.replace('(C)', '')
                # details.temperature = temperature.replace('(C)','')

            description = data_onu.get('description', None)
            if temperature:
                onu.service = self.get_service(description)
                # details.temperature = temperature.replace('(C)','')

            _id_data = id_data if id_data else self.command_uuid

            onu.save()
            # details.id_data = _id_data
            # details.save()

        # olt.onus = all_onus.count()
        # olt.onus_active = ONU.objects.filter(olt=olt, status=True).count()
        olt.save()

        # Eliminamos la conexión ssh una vez terminado el proceso
        del olt_ssh

    def get_service(self, description):
        """
            return a service if service_number is a integer
        """

        coincidences = re.findall(r'^([0-9]+)', description.strip())
        if len(coincidences) == 1:
            service, created = Service.objects.get_or_create(
                number=coincidences[0])
        else:
            new_coincidences = re.findall(r'^([0-9]+)', description.strip())
            if len(new_coincidences) > 0:
                service, created = Service.objects.get_or_create(
                    number=new_coincidences[0])
            else:
                service = None

        return service

    def add_arguments(self, parser):
        parser.add_argument(
            '-olt',
            '--oltpk',
            type=str,
            help='Define la olt a obtener',
        )
        parser.add_argument(
            '--reversed',
            action='store_true',
            dest='reversed',
            help='Si se recorre el qs de forma inversa',
        )

    def handle(self, *args, **options):

        if 'oltpk' in options:
            olt = OLT.objects.get(pk=options['oltpk'])
            self.getdata(olt, **options)
        else:
            all_olt = OLT.objects.filter(status_field=True)
            if 'reversed' in options:
                all_olt = reversed(all_olt)
            for olt in all_olt:
                self.getdata(olt, **options)
