import ipaddress
from time import sleep
from django.utils import timezone
from django.db.models import Max

from common.command import BaseCommand
from devices.wrappers import OLTWrapperSSH
from devices.models import ONU, OLT, IPTable
from dashboard.utils import LOG


class Command(BaseCommand):
    help = 'Trae una lista de onus y busca sus ips'

    def getips(self, onus, olt_ssh, **options):

        for onu in onus:

            if not onu.status:
                if 'pre_status' not in options:
                    print('ONU offline')
                    continue
                else:
                    print('ONU offline, intento ip')

            ip = olt_ssh.get_ip_onu(onu.slot, onu.port, onu.onu_id)
            if ip:
                if ip == 'not_online':
                    onu.status = False
                    onu.save()
                else:
                    try:
                        ipaddress.ip_address(ip)
                        ipt, created = IPTable.objects.get_or_create(ip=ip)
                        # ip_ont = IpONUS()
                        # ip_ont.id_data=self.command_uuid
                        # ip_ont.onu = onu
                        # ip_ont.ip = ipt
                        # ip_ont.save()
                        sleep(0.85)
                        onu.ip = ipt
                        onu.save()
                    except ValueError as e:
                        print("Error getips", e)

    def getdata(self, onus, pre_olt_ssh=None, id_data=None, **options):

        if onus.count() > 0:
            _l = LOG()
            _id_data = id_data if id_data is not None else self.command_uuid
            obj_to_log = {'by_representation': self.get_by(**options)}

            if not pre_olt_ssh:
                olt = onus[0].olt
                _ssh = OLTWrapperSSH(olt, **{'_l': _l, **options})
                olt_ssh = _ssh.get_me()
                try:
                    olt_ssh.shell(olt, self.get_by(**options))
                    olt.set_status_online()
                    _l.OLT.SHELL(locals())
                except Exception as e:
                    olt.set_status_offline()
                    _l.OLT.SHELL(locals())
                    print("Error getdata get_ip_onus", str(e))
                    return False
            else:
                olt_ssh = pre_olt_ssh

            self.getips(reversed(onus), olt_ssh, **options)
        # Eliminamos la conexión ssh una vez terminado el proceso
        del olt_ssh

    def add_arguments(self, parser):
        parser.add_argument(
            '--with-ip',
            action='store_true',
            dest='with_ip',
            help='Si se incluye o no onus con ips',
        )
        parser.add_argument(
            '--pre-status',
            action='store_true',
            dest='pre_status',
            help='Solo onlines por defecto',
        )
        parser.add_argument(
            '-olt',
            '--oltpk',
            type=int,
            help='Define la olt a obtener',
        )

    def handle(self, *args, **options):
        print('2222')
        if 'oltpk' in options:
            olts = OLT.objects.filter(pk=options['oltpk'])
        else:
            olts = OLT.objects.filter(status_field=True)

        for olt in olts:
            print("Modelo %s" % olt.model.model)
            onus = ONU.objects.filter(status_field=True, olt=olt)
            if 'with_ip' not in options:
                onus = onus.filter(ip__isnull=True)

            self.getdata(onus, None, **options)
