import re
import time
import os
import sys
from datetime import timedelta
from django.utils import timezone
from dateutil.relativedelta import relativedelta

from common.command import BaseCommand
from devices.wrappers import OLTWrapperSSH
from devices.models import OLT, ONU, Ethernet
from dashboard.utils import LOG


class Command(BaseCommand):
    help = 'Obtiene y almacena la informacion de wifi de onus'

    def getdata_filter(self, qs, olt_ssh, id_data=None):        
        for onu in qs:
            lans = olt_ssh.get_lan_onu(onu.slot, onu.port, onu.onu_id)
            _id_data = id_data if id_data else self.command_uuid

            for lan in lans:
                eth = Ethernet()
                eth.onu = onu
                eth.port_id = lan[0]
                eth.port_type = lan[1]
                eth.speeds = lan[2] if lan[2] != '-' else None
                eth.duplex = lan[3] if lan[3] != '-' else None
                eth.duplex = lan[3] if lan[3] != '-' else None
                eth.status = True if lan[3] != 'down' else False
                eth.id_data = _id_data
                eth.save()
    
    def getdata_item(self, onu, pre_olt_ssh=None, id_data=None,**options):

        _l = LOG()
        _id_data = id_data if id_data is not None else self.command_uuid
        obj_to_log = {'by_representation':self.get_by(**options)}
        olt = onu[0].olt
        if not pre_olt_ssh:
            _ssh = OLTWrapperSSH(olt, **{'_l':_l, **options})
            olt_ssh = _ssh.get_me()
            try:
                olt_ssh.shell(olt, self.get_by(**options))
                olt.set_status_online()
                _l.OLT.SHELL(locals())  
            except Exception as e:
                olt.set_status_offline()
                _l.OLT.SHELL(locals())  
                print ("Error getdata get_lan_onus", str(e))
                return False
        else:
            olt_ssh = pre_olt_ssh

        self.getdata_filter(onu, olt_ssh, id_data)

    def getdata(self, olt, pre_olt_ssh=None, id_data=None, **options):
        _l = LOG()
        _id_data = id_data if id_data is not None else self.command_uuid
        obj_to_log = {'by_representation':self.get_by(**options)}

        if not pre_olt_ssh:
            _ssh = OLTWrapperSSH(olt, **{'_l':_l, **options})
            olt_ssh = _ssh.get_me()
            try:
                olt_ssh.shell(olt, self.get_by(**options))
                olt.set_status_online()
                _l.OLT.SHELL(locals())  
            except Exception as e:
                olt.set_status_offline()
                _l.OLT.SHELL(locals())  
                print ("Error getdata get_lan_onus", str(e))
                return False
        else:
            olt_ssh = pre_olt_ssh

        all_onus = ONU.objects.filter(olt=olt)
        self.getdata_filter(all_onus, olt_ssh, id_data)

        # Eliminamos la conexión ssh una vez terminado el proceso
        del olt_ssh
    
    def add_arguments(self, parser):
        parser.add_argument('-olt', '--oltpk', type=str, help='Define la olt a obtener',)
        parser.add_argument('-onu', '--onupk', type=str, help='Define la onu a obtener',)

    def handle(self, *args, **options):

        if options['onupk']:
            onu_qs = ONU.objects.filter(pk=options['onupk'])
            return self.getdata_item(onu_qs,**options)

        if options['oltpk']:
            olt = OLT.objects.get(pk=options['oltpk'])
            self.getdata(olt)
        else:
            all_olt = OLT.objects.filter(status_field=True)
            for olt in all_olt:
                self.getdata(olt,**options)