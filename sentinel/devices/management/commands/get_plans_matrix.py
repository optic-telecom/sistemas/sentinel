import re
import time
import os

from common.command import BaseCommand
from customers.models import Plan 

class Command(BaseCommand):
    help = 'Obtiene todos los planes de fibra de matrix y los guarda en sentinel'

    def handle(self, *args, **options):
        import requests
        r = requests.get('https://optic.matrix2.cl/api/v1/services/')
        if r.status_code == 200:
            planes = r.json()

            for plan in planes['results']:
                print (plan)
                if 'FIBRA' in plan['name'].upper():
                    _new = Plan()
                    _new.operator_id = 1
                    _new.type_plan_id = 1
                    _new.speeds_system_download_id = 1
                    _new.speeds_system_upload_id = 1
                    _new.matrix_plan = plan['id']
                    _new.name = plan['name']
                    _new.save()