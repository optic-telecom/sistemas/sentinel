import re
import time
import os
import paramiko

from common.command import BaseCommand
from django.conf import settings
# from devices.postgres_views import TargetsBySentinel,SmokepingTargetList

from devices.models import OLT,MenuSmokeping,IPTable
from devices.postgres_views import Targets


from dynamic_preferences.registries import global_preferences_registry


__all__ = ['OLT','MenuSmokeping','IPTable']

from django.utils import timezone
from paramiko import SSHClient


class Command(BaseCommand):
    help = 'Crea un archivo Targets para el servidor smokeping'

    def TargetsBySentinel(self,**kwargs):
        global_preferences = global_preferences_registry.manager()
        print('*******************************')
        print('** GENERANDO ARCHIVO TARGETS **')
        print('*******************************')
        print('\n')
        """
        Recibe kwargs con dos claves los mp y los ms
        mp = menus principal, son los items que se vemos en el menu lateral del server smokeping
            +title
        ms = menus secundarios, con una structura de ++ip,host,menu,title

        """

        validator = list()
        with open(f'Targets','w') as t:

            t.write("*** Targets ***" + '\n\n')
            t.write("probe = FPing" + '\n\n')

            t.write("menu = Top" + '\n')
            t.write("title = " + global_preferences['general_title_smokeping'] + '\n')
            t.write("remark = " + global_preferences['general_remark_smokeping'] + '\n\n')

            
            for mp in kwargs['mp']:
                t.write('+ ' + mp.upper().replace(' ','-') + '\n\n')
                t.write('menu = ' + mp.upper().replace(' ','-') + '\n')
                t.write('title = ' + mp.upper().replace(' ','-') + '\n\n')
                for ms in kwargs['ms']:
                    # for m in ms:
                    if ms['ip'] not in validator:
                        if mp == ms['menu_title']:
                            host = ms['ip'] if ms['host'] == '' else ms['host'].replace('_','-').lower()

                            t.write('++ ' + str(ms['ip'].replace('.','-'))+ '\n')
                            t.write('menu = '+str(ms['menu'].replace('_','-').replace(' ','-'))+ '\n')
                            t.write('title = ' + str(ms['title'].replace('_','-').replace(' ','-'))+ '\n')
                            t.write('host = ' + str(host)+ '\n\n')
                            validator.append(ms['ip'])
                
                t.write('\n')

    def SmokepingTargetList(self):
        print('*******************************')
        print('*** CREANDO MENÚS SMOKEPING ***')
        print('*******************************')
        print('\n')

        olts = OLT.objects.all()
        menus = MenuSmokeping.objects.all()

        #SE CREAN LOS MENUS EN EL MODELO MENUSMOKEPING
        for olt in olts:
            if not menus.filter(title=olt.alias).first():
                MenuSmokeping.objects.create(title = olt.alias,order_field = 1)

        iptables = IPTable.objects.all()

        # se crear las IP en IPTables si no existen
        for olt in olts:
            onus = olt.onu_set.exclude(ip=None)
            for onu in onus:

                if not iptables.filter(ip=str(onu.ip)).exists():
                    iptable = {
                        'ip':onu.ip, 
                        'title':f'onu-{onu.serial}',
                        'host':str(onu.ip),
                        'menu':f'onu-{onu.serial}',
                    }
                    ipt = IPTable(**iptable).save()
                    print(f'Nueva IP {ipt}')
                else:
                    ipt = IPTable.objects.get(ip=str(onu.ip))
                    ipt.title = f'onu-{onu.serial}'
                    if ipt.host == '' or ipt.host == None:
                        ipt.host = f'{onu.ip}'
                        ipt.save()
                    ipt.menu = f'onu-{onu.serial}'
                    ipt.save()


        for olt in olts:
            onus = olt.onu_set.exclude(ip=None)
            for menu in menus:
                if olt.alias == menu.title:
                    for onu in onus:
                        ipt = IPTable.objects.get(ip=str(onu.ip))
                        menu.iptable.add(ipt)
                        menu.save()

        targets = Targets.objects.all()

        menus = list()
        mp = list()
        ipt = list()
        ms = list()

        for target in targets:
            orden = target.ordenado_por if target.ordenado_por != None else 1
            menus.append({
                'menu':target.title_menu,
                'orden':orden
            })
            ms.append({
                'ip':target.ip,
                'host':target.host,
                'menu':target.menu,
                'title':target.title_ipt,
                'menu_title':target.title_menu,
                })

        #ORDENAMOS POR EL ORDEN OTORGADO EN EL MENU SMOKEPING

        menus = sorted(menus,key=lambda x: [x['orden']])

        for m in menus:
            if not m['menu'] in mp:
                mp.append(m['menu'])

        
        salida = dict(
            mp = mp,
            ms = ms,
            
            )
        
        self.TargetsBySentinel(**salida)


    def send_file_target(self):
        global_preferences = global_preferences_registry.manager()
        print('*******************************')
        print('*****   SEND TARGETS FILE *****')
        print('*******************************')
        print('\n')
        remoteserver = '/etc/smokeping/config.d/'
        remoteserver = os.path.join(remoteserver,'Targets')
        localpath = os.path.join(settings.BASE_DIR,'Targets')

        ssh = paramiko.SSHClient() 
        if global_preferences['password_smokeping_server']:
            password = global_preferences['password_smokeping_server']
        if global_preferences['user_smokeping_server']:
            username = global_preferences['user_smokeping_server']
        if global_preferences['url_smokeping']:
            host = global_preferences['url_smokeping'].replace('http://','')
        
        ssh.load_host_keys(os.path.expanduser(os.path.join("~", ".ssh", "known_hosts")))
        ssh.connect(host, username=username, password=password,port=22,timeout=15)

        # Aqui eliminamos el Archivo Targets Pre-existente
        stdin, stdout, stderr = ssh.exec_command('rm -rf /etc/smokeping/config.d/Targets')
        # inicio de la conexion sftp
        sftp = ssh.open_sftp()
        # Aqui copiamos el archivo recien creado
        sftp.put(localpath, remoteserver)
        # Aqui reiniciamos el servicio Smokeping
        stdin, stdout, stderr = ssh.exec_command('/home/smoke/Targets/./change_targets.sh')
        sftp.close()
        ssh.close()
                        
    def handle(self, *args, **kwargs):
        time = timezone.now().strftime('%X')
        self.SmokepingTargetList()
        self.send_file_target()
        self.stdout.write("Se ha completado la tarea a las %s hora local." % time)
