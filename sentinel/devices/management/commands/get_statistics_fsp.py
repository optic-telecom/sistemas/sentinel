import re
import time
import os
import sys
from datetime import timedelta
from django.utils import timezone
from dateutil.relativedelta import relativedelta

from common.command import BaseCommand
from devices.wrappers import OLTWrapperSSH
from devices.models import OLT, PortOLT, StatisticsPortOLT
from dashboard.utils import LOG

class Command(BaseCommand):
    help = 'Obtiene y almacena la informacion estadistica de puertos'

    def getdata_filter(self, qs, olt_ssh, id_data=None, **options):        
        for port in qs:
            statistics = olt_ssh.get_statistics_fsp(port.interface.frame,
                                                    port.interface.slot, port.port)
            _id_data = id_data if id_data else self.command_uuid

            e_p = StatisticsPortOLT()
            e_p.port = port
            e_p.id_data = _id_data

            if 'unicast_rec' in statistics:
                e_p.unicast_rec = statistics['unicast_rec']
            if 'multicast_rec' in statistics:
                e_p.multicast_rec = statistics['multicast_rec']
            if 'broadcast_rec' in statistics:
                e_p.broadcast_rec = statistics['broadcast_rec']
            if 'discarded_rec' in statistics:
                e_p.discarded_rec = statistics['discarded_rec']
            if 'error_rec' in statistics:
                e_p.error_rec = statistics['error_rec']
            if 'unicast_send' in statistics:
                e_p.unicast_send = statistics['unicast_send']
            if 'multicast_send' in statistics:
                e_p.multicast_send = statistics['multicast_send']
            if 'broadcast_send' in statistics:
                e_p.broadcast_send = statistics['broadcast_send']

            e_p.save()
    
    def getdata_item(self, port, pre_olt_ssh=None, id_data=None, **options):
        _l = LOG()
        _id_data = id_data if id_data is not None else self.command_uuid
        obj_to_log = {'by_representation':self.get_by(**options)}
        olt = port[0].interface.olt
        if not pre_olt_ssh:
            _ssh = OLTWrapperSSH(olt, **{'_l':_l, **options})
            olt_ssh = _ssh.get_me()
            try:
                olt_ssh.shell(olt, self.get_by(**options))
                olt.set_status_online()
                _l.OLT.SHELL(locals())  
            except Exception as e:
                olt.set_status_offline()
                _l.OLT.SHELL(locals())  
                print ("Error getdata get_statistics_fsp", str(e))
                return False
        else:
            olt_ssh = pre_olt_ssh

        self.getdata_filter(port, olt_ssh, id_data, **options)

    def getdata(self, olt, pre_olt_ssh=None, id_data=None, **options):
        _l = LOG()
        _id_data = id_data if id_data is not None else self.command_uuid
        obj_to_log = {'by_representation':self.get_by(**options)}

        if not pre_olt_ssh:
            _ssh = OLTWrapperSSH(olt, **{'_l':_l, **options})
            olt_ssh = _ssh.get_me()
            try:
                olt_ssh.shell(olt, self.get_by(**options))
                olt.set_status_online()
                _l.OLT.SHELL(locals())  
            except Exception as e:
                olt.set_status_offline()
                _l.OLT.SHELL(locals())  
                print ("Error getdata get_statistics_fsp", str(e))
                return False
        else:
            olt_ssh = pre_olt_ssh

        all_data = PortOLT.objects.filter(interface__olt=olt)
        self.getdata_filter(all_data, olt_ssh, id_data, **options)

        # Eliminamos la conexión ssh una vez terminado el proceso
        del olt_ssh
    
    def add_arguments(self, parser):
        parser.add_argument('-olt', '--oltpk', type=str, help='Define la olt a obtener',)
        #parser.add_argument('-onu', '--onupk', type=str, help='Define la onu a obtener',)

    def handle(self, *args, **options):

        if options['oltpk']:
            olt = OLT.objects.get(pk=options['oltpk'])
            self.getdata(olt)
        else:
            all_olt = OLT.objects.filter(status_field=True)
            for olt in all_olt:
                self.getdata(olt)