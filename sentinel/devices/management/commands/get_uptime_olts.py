import re
import time
import os
import sys
from datetime import timedelta
from django.utils import timezone
from dateutil.relativedelta import relativedelta

from common.command import BaseCommand
from devices.wrappers import OLTWrapperSSH
from devices.models import OLT, ONU
from dashboard.utils import LOG

class Command(BaseCommand):
    help = 'Actualiza el uptime de la/las OLT'

    def getdata(self, olt, pre_olt_ssh=None, id_data=None, **options):
        _l = LOG()
        _id_data = id_data if id_data is not None else self.command_uuid
        obj_to_log = {'by_representation':self.get_by(**options)}


        if not pre_olt_ssh:
            _ssh = OLTWrapperSSH(olt, **{'_l':_l, **options})
            olt_ssh = _ssh.get_me()
            try:
                olt_ssh.shell(olt, self.get_by(**options))
                olt.set_status_online()
                _l.OLT.SHELL(locals())  
            except Exception as e:
                olt.set_status_offline()
                _l.OLT.SHELL(locals())  
                print ("Error getdata get_uptime_olts", str(e))
                return False
        else:
            olt_ssh = pre_olt_ssh

        uptime = olt_ssh.get_sysuptime()

        if uptime:

            uptime = uptime.split(', ')
            dias = int(uptime[0].replace('d',''))
            horas = int(uptime[1].replace('h',''))
            minutos = int(uptime[2].replace('m',''))
            segundos = int(uptime[3].replace('s',''))
            now = timezone.now()
            value_uptime = now + timedelta(days=-dias, hours=-horas,
                                           minutes=-minutos, seconds=-segundos)
            olt.uptime = value_uptime
            olt.time_uptime = now
            olt.status = True
            # MoreDetailOLTS.objects.create(uptime=value_uptime, olt=olt, time=now)
        else:
            olt.status = False
        
        olt.save()
        if options.get('close_ssh', False):
            del olt_ssh

    def add_arguments(self, parser):
        parser.add_argument('-olt', '--oltpk', type=str, help='Define la olt a obtener',)
        parser.add_argument('-olts',
                            type=str,
                            action='append',
                            default=[],                            
                            help='Define lista de olts a obtener',)
        parser.add_argument(
                    '--cron',
                    action='store_true',
                    dest='is_in_cron',
                    help='Establece si se esta ejecutando o no en modo cron',
                )
        
    def handle(self, *args, **options):
        options['close_ssh'] = True 
        if 'oltpk' in options:
            olt = OLT.objects.get(pk=options['oltpk'])
            self.getdata(olt, None, **options)
        elif options.get('olts', None) != []:
            all_olt = OLT.objects.filter(pk__in=options.get('olts', []))
            for olt in all_olt:
                self.getdata(olt, None, **options)
        else:
            all_olt = OLT.objects.filter(status_field=True)

            for olt in all_olt:
                try:
                    self.getdata(olt, None, **options)
                except Exception as e:
                    print(e)
                    pass
