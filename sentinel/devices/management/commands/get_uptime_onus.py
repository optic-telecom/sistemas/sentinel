import re
import time
import os
import sys
from datetime import timedelta
from django.utils import timezone
from dateutil.relativedelta import relativedelta

from common.command import BaseCommand
from devices.wrappers import OLTWrapperSSH
from devices.models import OLT, ONU
from dashboard.utils import LOG


class Command(BaseCommand):
    help = 'Obtiene y almacena el uptime de las onus'

    def getdata(self, olt, pre_olt_ssh=None, id_data=None, **options):

        _l = LOG()
        _id_data = id_data if id_data is not None else self.command_uuid
        obj_to_log = {'by_representation': self.get_by(**options)}

        if not pre_olt_ssh:
            _ssh = OLTWrapperSSH(olt, **{'_l': _l, **options})
            olt_ssh = _ssh.get_me()
            try:
                olt_ssh.shell(olt, self.get_by(**options))
                olt.set_status_online()
                _l.OLT.SHELL(locals())
            except Exception as e:
                olt.set_status_offline()
                _l.OLT.SHELL(locals())
                print("Error getdata get_uptime_onus", str(e))
                return False
        else:
            olt_ssh = pre_olt_ssh

        all_onus = ONU.objects.filter(olt=olt)
        for onu in all_onus:

            # try:
            #     s = StatusONUS.objects.filter(onu=onu).latest('created')
            # except StatusONUS.DoesNotExist as e:
            #     s = StatusONUS()
            #     s.onu = onu

            uptime = olt_ssh.get_uptime_onu(onu.serial)
            #print ('resp uptime',uptime)
            if not uptime:
                # s.status = False
                # s.save()
                onu.status = False
                onu.save()
                continue

            # s.status = True

            try:
                u = uptime.split(', ')
                #'%sd, %sh, %sm, %ss'

                dias = int(u[0].replace('d', ''))
                horas = int(u[1].replace('h', ''))
                minutos = int(u[2].replace('m', ''))
                segundos = int(u[3].replace('s', ''))
                now = timezone.now()
                value_uptime = now + relativedelta(days=-dias,
                                                   hours=-horas,
                                                   minutes=-minutos,
                                                   seconds=-segundos)
                onu.uptime = value_uptime
                onu.time_uptime = now
                onu.status = True
                onu.save()

                # s.uptime = value_uptime

            except Exception as e:
                print(e)

            # s.save()

        # olt.onus = all_onus.count()
        # olt.onus_active = ONU.objects.filter(olt=olt, status=True).count()
        olt.save()

        if options.get('close_ssh', False):
            del olt_ssh

    def add_arguments(self, parser):
        parser.add_argument(
            '-olt',
            '--oltpk',
            type=str,
            help='Define la olt a obtener',
        )
        parser.add_argument(
            '--cron',
            action='store_true',
            dest='is_in_cron',
            help='Establece si se esta ejecutando o no en modo cron',
        )

    def handle(self, *args, **options):
        options['close_ssh'] = True
        if 'olt' in options:
            olt = OLT.objects.get(pk=options['olt'])
            self.getdata(olt, **options)
        else:
            all_olt = OLT.objects.filter(status_field=True)
            for olt in all_olt:
                self.getdata(olt, **options)
