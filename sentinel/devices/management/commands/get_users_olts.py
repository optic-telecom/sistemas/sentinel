import re
import time
import os
import sys

from django.utils import timezone

from common.command import BaseCommand
from devices.wrappers import OLTWrapperSSH
from devices.models import OLT, OLTUser
from dashboard.utils import LOG

class Command(BaseCommand):
    help = 'Obtiene y almacena los usuarios de la/las OLT'

    def getdata(self, olt, pre_olt_ssh=None, id_data=None, **options):

        _l = LOG()
        _id_data = id_data if id_data is not None else self.command_uuid
        obj_to_log = {'by_representation':self.get_by(**options)}

        if not pre_olt_ssh:
            options['_l'] = _l
            _ssh = OLTWrapperSSH(olt, **options)
            olt_ssh = _ssh.get_me()
            try:
                olt_ssh.shell(olt, self.get_by(**options))
                _l.OLT.SHELL(locals())    
            except Exception as e:
                olt.set_status_offline()
                _l.OLT.SHELL(locals())
                print ("Error getdata get_users_olts", str(e))
                return False
        else:
            olt_ssh = pre_olt_ssh

        users = olt_ssh.get_all_users()
        for user in users:
            print (user)
            olt_user = OLTUser()
            olt_user.name = user[0]

            if user[1] == 'Super':
                olt_user.level = 1
            elif user[1] == 'Admin':
                olt_user.level = 2
            else:
                olt_user.level = 3

            olt_user.online = False if user[2] =='Offline' else True
            olt_user.reenter_num = int(user[3])
            olt_user.profile = user[4]
            olt_user.append_info = " ".join(user[5:])
            olt_user.id_data = id_data
            olt_user.olt = olt
            olt_user.save()

        if options.get('close_ssh', False):
            del olt_ssh
    
    def add_arguments(self, parser):
        parser.add_argument('-olt', '--oltpk', type=str, help='Define la olt a obtener',)
        parser.add_argument(
                    '--cron',
                    action='store_true',
                    dest='is_in_cron',
                    help='Establece si se esta ejecutando o no en modo cron',
                )
        
    def _pre_handle(self, *args, **options):
        options['close_ssh'] = True 
        
        if options['oltpk']:
            olt = OLT.objects.get(pk=options['oltpk'])
            self.getdata(olt, **options)
        else:
            all_olt = OLT.objects.filter(status_field=True)
            for olt in all_olt:
                self.getdata(olt, **options)


    def handle(self, *args, **options):
        from uuid import UUID
        options['close_ssh'] = True

        if options['oltpk']:
            try:
                UUID(options['oltpk'], version=4)
                filter_by = {'uuid':options['oltpk']}
            except ValueError:
                filter_by = {'pk':options['oltpk']}
            try:
                olt = OLT.objects.get(**filter_by)
                return self.getdata(olt, **options)
            except OLT.DoesNotExist:
                pass
                
        all_olt = OLT.objects.filter(status_field=True)
        for olt in all_olt:
            self.getdata(olt, **options)                