from common.command import BaseCommand
from dashboard.utils import slack_error
from devices.wrappers import OLTWrapperSSH
from devices.models import SSHConnection, OLT, ONU,\
    OLTVLAN, OLTSpeeds
from dashboard.utils import LOG


class Command(BaseCommand):
    help = 'Obtiene y almacena las velocidades de la/las OLT'

    def getdata(self, olt, pre_olt_ssh=None, id_data=None, **options):

        _l = LOG()
        _id_data = id_data if id_data is not None else self.command_uuid
        obj_to_log = {'by_representation': self.get_by(**options)}

        if not pre_olt_ssh:
            options['_l'] = _l
            _ssh = OLTWrapperSSH(olt, **options)
            olt_ssh = _ssh.get_me()
            try:
                olt_ssh.shell(olt, self.get_by(**options))
                _l.OLT.SHELL(locals())
            except Exception as e:
                olt.set_status_offline()
                _l.OLT.SHELL(locals())
                print("Error getdata get_velocidades_olts", str(e))
                return False
        else:
            olt_ssh = pre_olt_ssh

        speeds = olt_ssh.get_all_speeds()
        all_trafic = olt_ssh.get_traffic_table()
        # Eliminamos la conexión ssh una vez terminado el proceso
        del olt_ssh
        try:
            for speed in speeds:

                olt_s = OLTSpeeds()
                olt_s.id_data = self.command_uuid
                olt_s.olt = olt
                olt_s.tid = speed[0]
                olt_s.cir = speed[1]
                olt_s.cbs = speed[2]
                olt_s.pir = speed[3]
                olt_s.pbs = speed[4]
                olt_s.pri = speed[5]

                for trafic in all_trafic:
                    if speed[0] == trafic[0]:
                        olt_s.name = trafic[1].replace('"', '')

                try:
                    old_speed = OLTSpeeds.objects.filter(
                        status_field=True, olt=olt,
                        tid=speed[0]).latest('created')
                    olt_s.speeds_system = old_speed.speeds_system
                except Exception as e:
                    pass

                olt_s.save()
        except Exception as e:
            slack_error(str(e), 'get_velocidades_olts')

    def add_arguments(self, parser):
        parser.add_argument(
            '-olt',
            '--oltpk',
            type=str,
            help='Define la olt a obtener',
        )
        parser.add_argument(
            '--cron',
            action='store_true',
            dest='is_in_cron',
            help='Establece si se esta ejecutando o no en modo cron',
        )

    def handle(self, *args, **options):
        from uuid import UUID
        options['close_ssh'] = True

        if 'oltpk' in options:
            try:
                UUID(options['oltpk'], version=4)
                filter_by = {'uuid': options['oltpk']}
            except ValueError:
                filter_by = {'pk': options['oltpk']}
            try:
                olt = OLT.objects.get(**filter_by)
                return self.getdata(olt, **options)
            except OLT.DoesNotExist:
                pass

        all_olt = OLT.objects.filter(status_field=True)
        for olt in all_olt:
            try:
                self.getdata(olt, **options)
            except Exception as e:
                print(e)
                pass
