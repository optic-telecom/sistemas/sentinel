import re
import time
import os
import sys

from django.utils import timezone

from common.command import BaseCommand
from devices.wrappers import OLTWrapperSSH
from devices.models import OLT, OLTVLAN, TypeVLAN
from dashboard.utils import LOG


class Command(BaseCommand):
    help = 'Obtiene y almacena las vlans de la/las OLT'

    def getdata(self, olt, pre_olt_ssh=None, id_data=None, **options):

        _l = LOG()
        _id_data = id_data if id_data is not None else self.command_uuid
        obj_to_log = {'by_representation': self.get_by(**options)}

        if not pre_olt_ssh:
            options['_l'] = _l
            _ssh = OLTWrapperSSH(olt, **options)
            olt_ssh = _ssh.get_me()
            try:
                olt_ssh.shell(olt, self.get_by(**options))
                _l.OLT.SHELL(locals())
            except Exception as e:
                olt.set_status_offline()
                _l.OLT.SHELL(locals())
                print("Error getdata get_vlans_olts", str(e))
                return False
        else:
            olt_ssh = pre_olt_ssh

        vlans = olt_ssh.get_all_vlans()

        for vlan in vlans:

            ports = olt_ssh.get_ports_vlans(vlan[0])
            ports_save = []
            for port in ports:
                ports_save.append("%s%s %s" % (port[0], port[1], port[2]))

            olt_vlan = OLTVLAN()
            olt_vlan.vlan_id = vlan[0]
            vlan_type, c = TypeVLAN.objects.get_or_create(name=vlan[1])
            olt_vlan.vlan_type_id = vlan_type.id
            olt_vlan.description = vlan[2]
            olt_vlan.stdn_port = vlan[3]
            olt_vlan.service_port = vlan[4]
            olt_vlan.olt = olt
            olt_vlan.associated_ports = ','.join(ports_save)
            olt_vlan.id_data = id_data
            olt_vlan.save()

        if options.get('close_ssh', False):
            del olt_ssh

    def add_arguments(self, parser):
        parser.add_argument(
            '-olt',
            '--oltpk',
            type=str,
            help='Define la olt a obtener',
        )
        parser.add_argument(
            '--cron',
            action='store_true',
            dest='is_in_cron',
            help='Establece si se esta ejecutando o no en modo cron',
        )

    def handle(self, *args, **options):
        options['close_ssh'] = True

        if 'oltpk' in options:
            olt = OLT.objects.get(pk=options['oltpk'])
            self.getdata(olt, **options)
        else:
            all_olt = OLT.objects.filter(status_field=True)
            for olt in all_olt:
                try:
                    self.getdata(olt, **options)
                except Exception as e:
                    print(e)
                    pass
