import re
import time
import os
import sys
from datetime import timedelta
from django.utils import timezone
from dateutil.relativedelta import relativedelta

from common.command import BaseCommand
from devices.wrappers import OLTWrapperSSH
from devices.models import OLT, ONU, WirelessONUS
from dashboard.utils import LOG


class Command(BaseCommand):
    help = 'Obtiene y almacena la informacion de wifi de onus'

    def getdata_filter(self, qs, olt_ssh, id_data=None, **options):
        for onu in qs:
            wifi = olt_ssh.get_wifi_onu(onu.frame, onu.slot,
                                        onu.port, onu.onu_id)

            _id_data = id_data if id_data else self.command_uuid

            if wifi.get('save_data', True):
                wire = WirelessONUS()
                wire.onu = onu
                wire.id_data = _id_data
                wire.wifi_new_band_ssid = wifi.get('wifi_new_band_ssid',None)
                wire.wifi_old_band_ssid = wifi.get('wifi_old_band_ssid',None)
                wire.wifi_new_band_mode = wifi.get('wifi_new_band_mode',None)
                wire.wifi_old_band_mode = wifi.get('wifi_old_band_mode',None)
                wire.wifi_new_band_status = wifi.get('wifi_new_band_status',False)
                wire.wifi_old_band_status = wifi.get('wifi_old_band_status',False)
                wire.wifi_new_band_devices = wifi.get('wifi_new_band_devices',None)
                wire.wifi_old_band_devices = wifi.get('wifi_old_band_devices',None)
                wire.wifi_new_band_administrative_status = wifi.get('wifi_new_band_administrative_status',False)
                wire.wifi_old_band_administrative_status = wifi.get('wifi_old_band_administrative_status',False)
                wire.save()
            else:
                onu.status = False
                onu.save()


        if options.get('close_ssh', False):
            del olt_ssh

    def getdata_item(self, onu, pre_olt_ssh=None, id_data=None, **options):
        if not pre_olt_ssh:
            _ssh = OLTWrapperSSH(onu[0].olt, **options)
            olt_ssh = _ssh.get_me()
            try:
                olt_ssh.shell()
            except Exception as e:
                onu[0].olt.set_status_offline()
                print ("Error getdata get_wifi_onus", str(e))
                return False
        else:
            olt_ssh = pre_olt_ssh
        self.getdata_filter(onu, olt_ssh, id_data, **options)


    def getdata(self, olt, pre_olt_ssh=None, id_data=None, **options):

        _l = LOG()
        _id_data = id_data if id_data is not None else self.command_uuid
        obj_to_log = {'by_representation':self.get_by(**options)}

        if not pre_olt_ssh:
            options['_l'] = _l
            _ssh = OLTWrapperSSH(olt, **options)
            olt_ssh = _ssh.get_me()
            try:
                olt_ssh.shell(olt, self.get_by(**options))
                _l.OLT.SHELL(locals())    
            except Exception as e:
                olt.set_status_offline()
                _l.OLT.SHELL(locals())
                print ("Error getdata get_wifi_onus", str(e))
                return False
        else:
            olt_ssh = pre_olt_ssh

        all_onus = ONU.objects.filter(olt=olt)
        self.getdata_filter(all_onus, olt_ssh, id_data, **options)
    
    def add_arguments(self, parser):
        parser.add_argument('-olt', '--oltpk', type=str, help='Define la olt a obtener',)
        parser.add_argument('-onu', '--onupk', type=str, help='Define la onu a obtener',)

    def handle(self, *args, **options):
        options['close_ssh'] = True
        if options['onupk']:
            onu_qs = ONU.objects.filter(pk=options['onupk'])
            return self.getdata_item(onu_qs,**options)

        if options['oltpk']:
            olt = OLT.objects.get(pk=options['oltpk'])
            self.getdata(olt,**options)
        else:
            all_olt = OLT.objects.filter(status_field=True)
            for olt in all_olt:
                self.getdata(olt,**options)