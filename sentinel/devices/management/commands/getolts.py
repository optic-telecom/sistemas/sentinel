import re
import time
import os

from common.command import BaseCommand
from devices.wrappers import ssh_all_data_olt
from devices.models import OLT

class Command(BaseCommand):
    help = 'Obtiene informacion de las OLTs y la almacena en la base de datos'

    def getdata(self, olt, old_olt_ssh=None, **options):
        opt = {
            **options,
            'id_data':self.command_uuid
        }
        return ssh_all_data_olt(olt, old_olt_ssh, **opt)

    def add_arguments(self, parser):
        parser.add_argument('-olt', '--oltpk', type=str, help='Define la olt a obtener',)

    def handle(self, *args, **options):
        if options['oltpk']:
            olt = OLT.objects.get(pk=options['oltpk'])      	
            self.getdata(olt,**options)
        else:

            all_olt = OLT.objects.filter(status_field=True)
            for olt in reversed(all_olt):
                self.getdata(olt,**options)