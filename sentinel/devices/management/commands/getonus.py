import re
import time
import os
import sys

from django.utils import timezone
from dateutil import parser
from django.db.models import Max

from common.command import BaseCommand
from common.utils import cyan

from customers.models import Service
from devices.ssh import ManagerSSH, MA5800T_SSH
from devices.models import OLT, ONU, ModelDevice,\
    MACTable, PortOLT, ServicePortONUS,\
    InterfaceOLT
from devices.wrappers import OLTWrapperSSH
from dashboard.utils import LOG


class Command(BaseCommand):
    help = 'Obtiene y almacena las ont/onu de la/las OLT'

    def delete_onu(self, onus_groups, olt):

        _onus_actuales = ONU.all_objects.filter(olt=olt).values('serial')
        _onus_actuales = list(_onus_actuales)
        _onus_recibidas = []

        print(len(onus_groups))
        for ont in list(onus_groups):
            serial = ont[1][1]
            print(serial)
            _onus_recibidas.append(serial)

        for _onu in _onus_actuales:

            if _onu['serial'] not in _onus_recibidas:
                print(_onu['serial'])
                onu = ONU.all_objects.get(serial=_onu['serial'], olt=olt)
                onu.deleted_sentinel = True

                onu.save()

    def save_onu(self, ont, onus, olt):

        try:

            print('BUSCANDO ONU')
            onu = ONU.all_objects.get(serial=ont[1][1], olt=olt)
            print('ONU ENCONTRADA')

        except ONU.DoesNotExist:
            print('ONU NO ENCONTRADA')
            onu = ONU()
            onu.id_data = self.command_uuid

        description = " ".join(ont[1][5:len(ont[1])])
        onu.description = description

        coincidences = re.findall('^([0-9]+)', description.strip())
        if len(coincidences) == 1:
            onu.service, creted = Service.objects.get_or_create(
                number=coincidences[0])
        else:
            new_coincidences = re.findall('^([0-9]+)', onu.description.strip())
            if len(new_coincidences) > 0:
                onu.service, creted = Service.objects.get_or_create(
                    number=new_coincidences[0])
            else:
                onu.service = None

        onu.olt = olt
        onu.serial = ont[1][1]
        onu.onu_id = ont[1][0]

        card = onus['card'].split('/')
        onu.frame = card[0]
        onu.slot = card[1]
        onu.port = card[2]

        if ont[1][2] != '-':
            try:
                my_model = ModelDevice.objects.get(model=ont[1][2], kind=1)
                print("MODELO ENCONTRADO", my_model.id, my_model)
            except Exception as e:
                my_model = ModelDevice.objects.create(
                    model=ont[1][2], kind=1, id_data=self.command_uuid)
                print("MODELO CREADO", my_model.id, my_model)
            # try:
            #     print('en el try')
            #     my_model = ModelDevice.objects.filter(model=ont[1][2],
            #                                           kind=1
            #                                           ).last()
            #     if my_model == None:
            #         my_model = ModelDevice.objects.create(
            #             model=ont[1][2],
            #             kind=1,
            #             id_data=self.command_uuid)

            # except ModelDevice.DoesNotExist as e:
            #     print('en el except')
            #     my_model = ModelDevice.objects.create(model=ont[1][2],
            #     #  for_olt=False,
            #                                           kind = 1,
            #                                           id_data=self.command_uuid)
            print(my_model.id)
            onu.model = my_model
            onu.save()

        if ont[1][0] != '-':
            if ont[1][3] != '-':
                onu.distance = ont[1][3]

        signal = ont[1][4].split('/')
        if signal[0] != '-':
            onu.RX = signal[0]
        if signal[1] != '-':
            onu.TX = signal[1]

        if ont[0][3] != '-':
            onu.downtime = parser.parse(ont[0][3])
        onu.save()

        print("=============================")
        print("=============================")
        print(ont[0][1])
        print("=============================")
        print("=============================")
        if ont[0][1] == 'online':
            onu.status = True
            onu.time_uptime = timezone.now()
            onu.save()
        else:
            onu.status = False 
            onu.save()

    def get_onus(self, lines_ont, olt):

        omit_lines = 0
        onu_counter = -1
        onu_card_list = []
        ont_list = []

        ## for onus
        for i, line in enumerate(lines_ont, 0):

            if i < 7:
                continue

            if "In port " in line:

                omit_lines = 4
                data = line.split()
                value_onu = int(data[8].replace(',', ''))

                onu_card_list.append({
                    'nro': value_onu,
                    'card': data[2].replace(',', '')
                })

                onu_counter = value_onu * 2
                continue

            if omit_lines > 0:
                omit_lines -= 1
                continue

            if "Press" in line:
                line = line[90:]

            nreg = line.split()

            if len(nreg) > 1 and onu_counter > 0:

                if nreg[0] == 'ONT':
                    omit_lines = 1
                    continue

                onu_counter -= 1
                ont_list.append(nreg)
        ## end for

        onus_group = []
        start = None
        for nro_onus in onu_card_list:
            if not start:
                start = 0
            next1 = nro_onus['nro']
            ont_1 = ont_list[start:start + next1]
            ont_2 = ont_list[start + next1:(start + next1 * 2)]
            start = (start + next1 * 2)
            group = zip(ont_1, ont_2)
            onus_group.append({'group': group, 'card': nro_onus['card']})

        # Salvar onus nuevas o actualizar actuales
        onus_groups = list(onus_group)
        # onus_groups2 = onus_groups
        onus_delete = []
        for onus in onus_groups:
            for ont in list(onus['group']):
                onus_delete.append(ont)
                self.save_onu(ont, onus, olt)

        self.delete_onu(onus_delete, olt)

    def get_mac(self, lines_mac, olt):

        ## for onus
        for i, line in enumerate(lines_mac, 0):
            if i < 10:
                continue

            nreg = line.split()
            len_nreg = len(nreg)
            if len_nreg != 11:
                continue

            if nreg[2] == 'eth':
                continue

            if nreg[8] == 'the':
                continue
            try:
                slot = nreg[6].replace('/', '')
                port = nreg[7].replace('/', '')

                mi_ont = ONU.all_objects.get(onu_id=nreg[8],
                                         slot=slot,
                                         port=port,
                                         olt=olt)

                # try:
                # MACTable.objects.get(onu=mi_ont,id_data=self.command_uuid)
                # except MACTable.DoesNotExist:
                # MACTable.objects.create(onu=mi_ont,
                #                        mac=nreg[3].replace('-',''),
                #                        id_data=self.command_uuid)
                # mi_ont.mac = nreg[3].replace('-','')
                # mi_ont.save()
                unformatted_mac = nreg[3].replace('-', '')
                try:
                    formatted_mac = MACTable.objects.format_mac(unformatted_mac)
                    mi_ont.mac = MACTable.objects.get(mac=formatted_mac)
                    mi_ont.save()
                except:
                    mi_ont.mac = MACTable.objects.create(mac=unformatted_mac)
                    mi_ont.save()
                #mi_ont.mac, created = MACTable.objects.get_or_create(
                #    mac=nreg[3].replace('-', ''))
                #mi_ont.save()

            except ONU.DoesNotExist:
                print('Onu no existe get_mac')

            except Exception as e:
                print("error get_mac", e)
                print("error nreg", nreg)
        ## end for

    def get_services_numbers(self, lines_services_numbers):

        ## for onus
        cyan("Inicio get_services_numbers")

        for i, line in enumerate(lines_services_numbers, 0):

            if i < 11:
                continue
            try:

                nreg = line.split()
                service_port = int(nreg[0])

                try:

                    slot = nreg[4].split('/')[1]
                    port = nreg[5].replace('/', '')
                    mi_ont = ONU.all_objects.get(onu_id=nreg[6],
                                             slot=slot,
                                             port=port)
                    mi_ont.service_port = service_port
                    mi_ont.upload_speed = nreg[10]
                    mi_ont.download_speed = nreg[11]
                    mi_ont.save()

                    s_port = ServicePortONUS()
                    s_port.id_data = self.command_uuid
                    s_port.onu = mi_ont
                    s_port.service_port = service_port
                    s_port.vlan_id = nreg[1]
                    s_port.vlan_attr = nreg[2]
                    s_port.port_type = nreg[3]
                    s_port.status = True if nreg[12] == 'up' else False
                    s_port.save()

                except ONU.DoesNotExist:
                    pass

                #print (nreg)

            except Exception as e:
                pass

        ## end for

    def getdata(self, olt, pre_olt_ssh=None, id_data=None, **options):

        _l = LOG()
        _id_data = id_data if id_data is not None else self.command_uuid
        obj_to_log = {'by_representation': self.get_by(**options)}

        if not pre_olt_ssh:
            options['_l'] = _l
            _ssh = OLTWrapperSSH(olt, **options)
            olt_ssh = _ssh.get_me()
            try:
                olt_ssh.shell(olt, self.get_by(**options))
                _l.OLT.SHELL(locals())
            except Exception as e:
                olt.set_status_offline()
                _l.OLT.SHELL(locals())
                print("Error getdata get_getonus_olts", str(e))
                return False
        else:
            olt_ssh = pre_olt_ssh

        lines_ont = olt_ssh.get_all_ont()
        time.sleep(2.2)
        lines_mac = olt_ssh.get_all_mac()
        time.sleep(0.4)
        lines_services_numbers = olt_ssh.get_all_services_numbers()
        self.get_onus(lines_ont, olt)
        self.get_mac(lines_mac, olt)
        self.get_services_numbers(lines_services_numbers)

        # Eliminamos la conexión ssh una vez terminado el proceso
        del olt_ssh

    def add_arguments(self, parser):
        parser.add_argument(
            '-olt',
            '--oltpk',
            type=str,
            help='Define la olt a obtener',
        )

    def handle(self, *args, **options):
        from uuid import UUID
        options['close_ssh'] = True

        if options['oltpk']:
            try:
                UUID(options['oltpk'], version=4)
                filter_by = {'uuid': options['oltpk']}
            except ValueError:
                filter_by = {'pk': options['oltpk']}
            try:
                olt = OLT.objects.get(**filter_by)
                return self.getdata(olt, **options)
            except OLT.DoesNotExist:
                pass
        else:
            all_olt = OLT.objects.filter(status_field=True)
            for olt in all_olt:
                self.getdata(olt, **options)
