import json
from common.command import BaseCommand
from customers.models import Service


class Command(BaseCommand):
    help = "*** Asocia el id de los servicios de matrix con los de Sentinel ***"

    def associate_services(self, data):
        count = 0
        for register in data:
            print(register['id'], 'ID')
            print(register['number'], 'NUMBER')

            # Por cada registro buscamos el número en Sentinel
            # Y si no existe lo creamos
            current_service, created = Service.objects.get_or_create(
                number=register['number']
            )
            if created:
                print('se creó un número de servicio nuevo')
                count += 1

            current_service.matrix_id = register['id']
            current_service.save()
        print('servicios no encontrados (y creados):', count)
        print('servicios encontrados:', len(data) - count)

    def readfile(self):
        """
        Abrimos el archivo json con los números y 
        lo parseamos a un diccionario de python
        """
        file = open('numeros_de_servicio.json', 'r')
        data = json.load(file)
        print(data['results'][0].keys())
        self.associate_services(data['results'])

    def handle(self, *args, **options):
        self.readfile()
        pass
