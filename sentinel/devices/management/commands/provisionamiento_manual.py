import requests
import json
from multifiberpy.command import BaseCommand
from dynamic_preferences.registries import global_preferences_registry
#from devices.utils import send_data_erp


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('-s', '--serviceid', type=int, help='ID del Número de servicio',)

    def handle(self, *args, **options):
        if not options['serviceid']:
            print('Por favor, ingrese tanto el caso como el servicio')
            return 1

        preferences = global_preferences_registry.manager()

        h = {
            'content-type': 'application/json',
            'Authorization':
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'
        }

        data = {'status': 1}

        url = preferences['url_endpoint_erp'] + preferences[
            'url_endpoint_erp_provisioning_onu'] + f'{options["serviceid"]}/'

        # url = 'http://127.0.0.1:8080/services/10794/'

        # response = requests.patch(url, data=json.dumps(data), headers=h, verify=False)

        # print('status de matrix', response.status_code)
        # print('data enviada a matrix', data)
        # print("respuesta matrix send_status_change_matrix", response.text)

        # if response.status_code == 201 or response.status_code == 200:

        #     print('Status cambiado')
        # else:
        #     print("Error")
        #     print(response)

        algo = requests.get("https://demo-matrix.devel7.cl/nodes/243/", headers=h, verify=False)
        print(algo.text)
