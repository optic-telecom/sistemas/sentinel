from common.command import BaseCommand
from devices.models import OLT
from utp.models import Mikrotik
from customers.models import Service


class Command(BaseCommand):
    help = 'Actualiza el estado de red de los servicios del equipo seleccionado'

    def getdata(self, device, device_type):

        if device_type == "OLT":
            for onu in device.onu_set(manager='all_objects').all():
                try:
                    onu.service.seen_connected_live
                    onu.service.seen_connected_live
                except Exception as e:
                    print(e)
        else:
            for cpe in device.cpeutp_set.all():
                try:
                    cpe.service.seen_connected_live
                    cpe.service.seen_connected_live
                except Exception as e:
                    print(e)

    def add_arguments(self, parser):
        parser.add_argument(
            '-olt',
            '--oltpk',
            type=str,
            help='Define la olt a usar',
        )
        parser.add_argument(
            '-utp',
            '--utppk',
            type=str,
            help='Define el cpe a usar',
        )

    def handle(self, *args, **options):
        if 'oltpk' in options:
            olt = OLT.objects.get(uuid=options['oltpk'])
            self.getdata(olt, 'OLT')
        elif 'utppk' in options:
            utp = Mikrotik.objects.get(uuid=options['utppk'])
            self.getdata(utp, 'UTP')
        else:
            services = Service.objects.all()
            for service in services:
                service.seen_connected_live
                service.seen_connected_live
