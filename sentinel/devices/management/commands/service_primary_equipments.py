from common.command import BaseCommand
from devices.models import ONU
from utp.models import CpeUtp
from customers.models import Service


class Command(BaseCommand):
    help = "Comando que asocia los equipos primarios de los servicios que solo tengan un equipo asociado"


    def associate(self):
        # Obtenemos todos los servicios:
        all_services = Service.objects.all()
        for service in all_services:

            if service.cpeutp_set.count() == 1:
                cpe = service.cpeutp_set.all().first()
                cpe.primary = True
                cpe.save()
            if service.onu_set(manager='all_objects').count() == 1:
                onu = service.onu_set(manager='all_objects').all().first()
                onu.primary = True
                onu.save()

            if service.cpeutp_set.count() == 2:
                first_device = service.cpeutp_set.all()[0]
                last_device = service.cpeutp_set.all()[1]
                if first_device.status is True and last_device.status is False:
                    first_device.primary = True
                    first_device.save()
                if first_device.status is False and last_device.status is True:
                    last_device.primary = True
                    last_device.save()
            if service.onu_set(manager='all_objects').count() == 2:
                first_device = service.onu_set(manager='all_objects').all()[0]
                last_device = service.onu_set(manager='all_objects').all()[1]
                if first_device.status is True and last_device.status is False:
                    first_device.primary = True
                    first_device.save()
                if first_device.status is False and last_device.status is True:
                    last_device.primary = True
                    last_device.save()

    def handle(self, *args, **options):
        print(self.help)
        print("Comenzando asociación")
        self.associate()
