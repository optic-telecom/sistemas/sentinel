from pysnmp.hlapi import *
from pysmi import debug
from common.command import BaseCommand
from devices.snmp import ManagerSNMP, MA5800_SNMP


class Command(BaseCommand):
    help = 'Obtiene informacion de OLT desde snmp'


    def handle(self, *args, **options):
        proto = MA5800_SNMP(ip='192.168.22.100')
        proto.oid_consumption()
        proto.oid_signal_all_onus()
        errorIndication, errorStatus, errorIndex, varBinds = proto.getdata()

        if errorIndication:
            print(errorIndication)
        elif errorStatus:
            print('%s at %s' % (errorStatus.prettyPrint(),
                                errorIndex and varBinds[int(errorIndex) - 1][0] or '?'))
        else:

            for varBind in varBinds:
                self.out(' = '.join([x.prettyPrint() for x in varBind]))