import re
import time
import os
import sys

from django.utils import timezone

from common.command import BaseCommand
from devices.wrappers import OLTWrapperSSH
from devices.models import OLT, OLTVLAN, TypeVLAN


class Command(BaseCommand):
    help = 'Obtiene shel de olt'

    def getdata(self, olt, pre_olt_ssh=None):

        if not pre_olt_ssh:
            _ssh = OLTWrapperSSH(olt)
            olt_ssh = _ssh.get_me()
            try:
                olt_ssh.shell()
            except Exception as e:
                olt.set_status_offline()
                print ("Error getdata ssh_olt", str(e))
                return False
        else:
            olt_ssh = pre_olt_ssh

        met = input("olt_ssh -> ")
        method = getattr(olt_ssh, met, None)
        
        if method:
            par = input("parametros-> ")
            if par:
                print ('parametr',par)
                parametros = eval(par)
                print (method(parametros))
            else:
                print (method())

        # Eliminamos la conexión ssh una vez terminado el proceso
        del olt_ssh
    
    def add_arguments(self, parser):
        parser.add_argument('-olt', '--oltpk', type=str, help='Define la olt a obtener',)

    def handle(self, *args, **options):
        
        if options['oltpk']:
            olt = OLT.objects.get(pk=options['oltpk'])
            self.getdata(olt)