from common.command import BaseCommand
from devices.models import OLTConnection


class Command(BaseCommand):

    help = 'Para liberar las conexiones ssh que maneja el sistema'

    def handle(self, *args, **options):

        all_ssh = OLTConnection.objects.all()
        for conn in all_ssh:
            conn.connections = 0
            conn.save()
