import ipaddress  
from time import sleep
from django.utils import timezone
from datetime import timedelta
from django.db.models import Max

from common.command import BaseCommand
from dashboard.utils import LOG 

class Command(BaseCommand):

    help = 'Prueba para ver la excepcion'
    
    def handle(self, *args, **options):
        
        _l = LOG()
        _l.OLT.delete(by_representation='Otro',
        			  description='Se elimino esta OLT',
        			  id_value='44fcd4bd-6fa9-4806-ac61-0e81b4f7247b')