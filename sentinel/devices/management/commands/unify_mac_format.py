from common.command import BaseCommand
from devices.models import MACTable


class Command(BaseCommand):
    help = 'Formatea los objetos del formato xxxxxxxxxxx MACTable al formato XX:XX:XX:XX:XX:XX'

    def format(self, mac, **options):
        # obtenemos el string que contiene la mac del objeto
        mac_str = mac.mac
        
        # Formateamos la mac
        mac_str = ':'.join(mac_str[i:i+2] for i in range(0, 12, 2))
        # asociamos la mac de nuevo al objeto
        print(mac.mac)
        print(mac_str)
        mac.mac = mac_str.upper()
        mac.save()

    def add_arguments(self, parser):
        parser.add_argument('-mac', '--macpk', type=str, help='Define la mac a formatear',)

    def handle(self, *args, **options):
        if options['macpk']:
            mac = MACTable.objects.get(pk=options['macpk'])
            if ":" not in mac.mac:
                self.format(mac, **options)
        else:
            all_mac = MACTable.objects.all()
            for mac in all_mac:
                # Verificamos que tenga la forma sin ":"
                print(mac)
                if ":" not in mac.mac:
                    self.format(mac, **options)
