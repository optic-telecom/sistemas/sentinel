import ipaddress  
from time import sleep
from django.utils import timezone
from datetime import timedelta
from django.db.models import Max

from common.command import BaseCommand
from devices.wrappers import OLTWrapperSSH
from devices.models import ONU

class Command(BaseCommand):

    def update_onu(self, onu):

        print (onu.serial)
        
        if onu.get_status():
            onu.latest_status = onu.get_status()

        if onu.get_uptime():
            #onu.latest_uptime = onu.get_uptime()
            u = onu.get_uptime().split(', ')
            #'%sd, %sh, %sm, %ss'
            dias = int(u[0].replace('d',''))
            horas = int(u[1].replace('h',''))
            minutos = int(u[2].replace('m',''))
            segundos = int(u[3].replace('s',''))
            print (dias,horas,minutos,segundos)
            a = timezone.now()
            b = a - timedelta(days=dias, hours=horas, minutes=minutos, seconds=segundos)
            onu.latest_uptime_datetime = b

        if onu.get_downtime():
            onu.latest_downtime = onu.get_downtime()

        if onu.get_downcause():
            onu.latest_downcause = onu.get_downcause()

        if onu.get_ram():
            onu.latest_ram = onu.get_ram()

        if onu.get_cpu():
            onu.latest_cpu = onu.get_cpu()

        if onu.get_temperature():
            onu.latest_temperature = onu.get_temperature()

        if onu.get_mac():
            onu.latest_mac = onu.get_mac_field()

        if onu.get_ip():
            onu.latest_ip = onu.get_ip()

        print (onu.distance,onu.latest_status)
        onu.save()


    def handle(self, *args, **options):
        
        qs = ONU.objects.all()
        for onu in qs:
            self.update_onu(onu)


