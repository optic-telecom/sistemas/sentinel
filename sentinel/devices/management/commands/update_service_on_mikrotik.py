import requests
from time import sleep
from django.utils import timezone
from common.command import BaseCommand
from customers.models import Service
from utp.mikrotik import cutting_replacement
from random import randint

from dynamic_preferences.registries import global_preferences_registry


class Command(BaseCommand):
    token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'

    help = "Aplica corte y reposición en los nodos de acuerdo al status del servicio consultado a Matrix"

    def getRequest(self,url):

        COUNT = requests.get(
            url=url,
            headers = {'Authorization':self.token},
            verify=False
        ).json()['count']


        return requests.get(
            url=url,
            params={'limit': COUNT, 'offset': 0},
            headers = {'Authorization':self.token},
            verify=False
        ).json()['results']

    def get_services_matrix(self):
        preferences = global_preferences_registry.manager()
        # servicios = Service.objects.all()
        if preferences['url_endpoint_erp']:
            
            url = f'{preferences["url_endpoint_erp"]}api/v1/services/'
            try:
                results = self.getRequest(url)
                print('** CARGANDO LISTA DE SERVICIOS **')
                for servicio in results:
                    type_cr = 'reposicion' if servicio['status'] == 1 else 'corte'
                    number = servicio['number']

                    # Creación de servicios inexistentes en sentinel
                    service, created = Service.objects.get_or_create(number=number)
                    if created:
                        print(f'Servicio creado bajo el nro {service.number}')

                    # Estoy aplicando corte o reposición en el nodo.
                    cutting_replacement(service.number,type_cr)
            except Exception as e:
                print(str(e))

    def handle(self, *args, **options):
        self.get_services_matrix()



