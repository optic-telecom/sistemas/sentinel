# Generated by Django 2.1.3 on 2020-06-03 10:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('utp', '0007_historicalnotfoundmatrixequipment_notfoundmatrixequipment'),
        ('devices', '0009_auto_20200528_1159'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalolt',
            name='node',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='utp.Mikrotik'),
        ),
        migrations.AddField(
            model_name='olt',
            name='node',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='utp.Mikrotik'),
        ),
    ]
