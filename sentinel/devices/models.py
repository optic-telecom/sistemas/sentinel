# -*- coding: utf-8 -*-
import uuid
from dateutil.relativedelta import relativedelta

from django.db import models
from django.conf import settings
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator

from common.utils import timedelta_format
from common.models import BaseModel, Node
from address.models import Address
from django.urls import reverse


class Device(BaseModel):
    uuid = models.UUIDField(db_index=True,
                            default=uuid.uuid4,
                            editable=settings.DEBUG)

    class Meta:
        abstract = True


class ModelDeviceManager(models.Manager):
    """
    Permite que la funcíón get del modelo ModelDevice haga un formateo
    a los parámetros de búsqueda "model" y "brand" para que sea
    case insensitive y corresponda al formato en la base de datos.
    """
    def get(self, model="", brand="", kind=None):

        # Hacemos el formato del nombre del modelo y de la marca
        model = model.replace(' ', '').upper().strip()
        brand = brand.replace(' ', '').upper().strip()

        # Si ambos campos están vacíos devolvemos None
        if model == "" and brand == "":
            return None

        # Buscamos por modelo
        elif model != "" and brand == "":
            if kind is not None:
                return super(ModelDeviceManager,
                             self).get_queryset().get(model=model, kind=kind)
            else:
                return super(ModelDeviceManager,
                             self).get_queryset().get(model=model)

        # Buscamos por marca
        elif model == "" and brand != "":
            if kind is not None:
                return super(ModelDeviceManager,
                             self).get_queryset().get(brand=brand, kind=kind)
            else:
                return super(ModelDeviceManager,
                             self).get_queryset().get(brand=brand)

        # Buscamos por ambos parámetros
        elif model != "" and brand != "":
            if kind is not None:
                return super(ModelDeviceManager,
                             self).get_queryset().get(brand=brand, model=model)
            else:
                return super(ModelDeviceManager,
                             self).get_queryset().get(brand=brand, model=model)

    def create(self, model="", brand="", kind=None, id_data=None):
        # Hacemos el formato del nombre del modelo y de la marca
        model = model.replace(' ', '').upper().strip()
        brand = brand.replace(' ', '').upper().strip()

        # Si ambos campos están vacíos devolvemos None
        if model == "" and brand == "":
            return None

        # Creamos con el modelo
        elif model != "" and brand == "":
            if id_data is not None:
                return super().create(model=model, kind=kind, id_data=id_data)
            else:
                return super().create(model=model, kind=kind)

        # Si no hay modelo devolvemos None
        elif model == "" and brand != "":
            return None

        # Creamos con ambos parámetros
        elif model != "" and brand != "":
            if id_data is not None:
                return super().create(model=model,
                                      brand=brand,
                                      kind=kind,
                                      id_data=id_data)
            else:
                return super().create(model=model, brand=brand, kind=kind)


class ModelDevice(BaseModel):
    TYPE_ONU = 1
    TYPE_OLT = 2
    TYPE_NODE = 3
    TYPE_CPE = 4
    TYPE_GEAR = 5
    TYPE_SETUPBOX = 6
    KIND_MODEL_CHOISES = (
        (TYPE_ONU, "ONU"),
        (TYPE_OLT, "OLT"),
        (TYPE_NODE, "Nodo"),
        (TYPE_CPE, "CPE"),
        (TYPE_GEAR, "GEAR"),
        (TYPE_SETUPBOX, "SETUPBOX"),
    )

    model = models.CharField(max_length=80)
    brand = models.CharField(max_length=80, null=True, blank=True)
    kind = models.PositiveSmallIntegerField(choices=KIND_MODEL_CHOISES,
                                            blank=True,
                                            null=True)
    for_iris = models.BooleanField(default=False)
    objects = ModelDeviceManager()

    def __str__(self):
        return self.model

    class Meta:
        ordering = ['-id']


class OLT(Device):
    # t_ip = models.CharField(max_length=15, db_index=True)
    ip = models.ForeignKey('IPTable', on_delete=models.PROTECT)

    mac = models.ForeignKey('MACTable',
                            null=True,
                            blank=True,
                            on_delete=models.PROTECT)
    mac_secondary = models.ForeignKey('MACTable',
                                      null=True,
                                      blank=True,
                                      on_delete=models.PROTECT,
                                      related_name="olt_secondary")
    # mac = models.CharField(max_length=17, db_index=True, null=True, blank=True)
    # mac_secondary = models.CharField(max_length=17, null=True, blank=True)
    alias = models.CharField(max_length=80, db_index=True)
    status = models.BooleanField(default=False)
    model = models.ForeignKey(ModelDevice, on_delete=models.PROTECT)
    description = models.TextField()
    firmware = models.CharField(max_length=100, null=True, blank=True)
    onus = models.PositiveSmallIntegerField(default=0)
    cards = models.PositiveSmallIntegerField(default=0)
    # ip_node = models.GenericIPAddressField(protocol="IPv4", null=True, blank=True)
    #historical
    #onus_active = models.PositiveSmallIntegerField(default=0)
    cards_active = models.PositiveSmallIntegerField(default=0)
    ram = models.CharField(max_length=10, null=True, blank=True)
    ram_secondary = models.CharField(max_length=10, null=True, blank=True)
    cpu = models.CharField(max_length=10, null=True, blank=True)
    cpu_secondary = models.CharField(max_length=10, null=True, blank=True)
    temperature = models.CharField(max_length=10, null=True, blank=True)
    temperature_secondary = models.CharField(max_length=10,
                                             null=True,
                                             blank=True)
    uptime = models.DateTimeField(null=True, blank=True)
    watts = models.PositiveSmallIntegerField(default=0)
    time_uptime = models.DateTimeField(null=True, blank=True)

    node = models.ForeignKey('utp.Mikrotik',
                             on_delete=models.PROTECT,
                             null=True,
                             blank=True)
    logic_node = models.ForeignKey(Node,
                                   on_delete=models.PROTECT,
                                   null=True,
                                   blank=True)
    address = models.ForeignKey(Address,
                                on_delete=models.CASCADE,
                                null=True,
                                blank=True)

    def __str__(self):
        if self.ip:
            return f"{self.alias} {self.ip}"
        return self.alias

    @property
    def onus_active(self):
        onus = ONU.objects.filter(olt__pk=self.pk, status=True).count()
        return onus

    @property
    def onus(self):
        onus = ONU.objects.filter(olt__pk=self.pk).count()
        return onus

    def get_uptime(self):
        # try:
        #     s = MoreDetailOLTS.objects.filter(olt=self).latest('created')
        #     return s.uptime
        # except Exception as e:
        #     return None

        return self.uptime

    def get_cpu(self):
        # try:
        #   return MoreDetailOLTS.objects.filter(olt=self).latest('created').cpu
        # except Exception as e:
        #   return None
        self.cpu

    def get_temperature(self):
        # try:
        #   return MoreDetailOLTS.objects.filter(olt=self).latest('created').temperature
        # except Exception as e:
        #   return None
        return self.temperature

    def get_ram(self):
        # try:
        #   return MoreDetailOLTS.objects.filter(olt=self).latest('created').ram
        # except Exception as e:
        #   return None
        return self.ram

    def get_watts(self):
        # try:
        #   return MoreDetailOLTS.objects.filter(olt=self).latest('created').watts
        # except Exception as e:
        #   return None
        return self.watts

    def set_status_offline(self):
        self.status = False
        self.save()

    def set_status_online(self):
        self.status = True
        self.save()

    def save(self, *args, **kwargs):
        super(OLT, self).save(*args, **kwargs)

    class Meta:
        ordering = ["alias"]
        unique_together = ("alias", "ip")


TYPE_ENERGY = 1
TYPE_CLOCK = 2
TYPE_SERVICE = 3
TYPE_CONTROLLER = 4
INTERFACE_TYPE_CHOISES = (
    (TYPE_ENERGY, "Energía"),
    (TYPE_CLOCK, "Alarma/Clock"),
    (TYPE_SERVICE, "Servicio"),
    (TYPE_CONTROLLER, "Controladora"),
)


class InterfaceOLT(BaseModel):
    """ 
    Slot 
    Las interfaces son las que obtengo en display port
    """

    olt = models.ForeignKey(OLT, on_delete=models.CASCADE)
    interface = models.CharField(max_length=18)
    frame = models.PositiveSmallIntegerField(default=0)
    slot = models.PositiveSmallIntegerField()
    onus = models.PositiveSmallIntegerField(default=0)
    onus_active = models.PositiveSmallIntegerField(default=0)
    ports = models.PositiveSmallIntegerField(default=0)
    ports_active = models.PositiveSmallIntegerField(default=0)
    ram = models.PositiveSmallIntegerField(default=0)
    cpu = models.PositiveSmallIntegerField(default=0)
    interface_type = models.PositiveSmallIntegerField(
        choices=INTERFACE_TYPE_CHOISES, null=True, blank=True)
    in_used = models.BooleanField(default=False)
    status = models.CharField(max_length=18)
    serial = models.CharField(max_length=30, db_index=True)

    def __str__(self):
        return self.interface

    class Meta:
        ordering = ["interface"]


def get_default_interface():
    try:
        return InterfaceOLT.objects.last().pk
    except Exception as e:
        return 0


class PortOLT(BaseModel):
    """
    Port 
    Son todos los puertos con logicos del sistema asociados a un slot fisico
    """

    port = models.PositiveSmallIntegerField()
    #fsp_field es para ordenar en el datatable
    fsp_field = models.IntegerField(null=True, blank=True)
    description = models.TextField()
    interface = models.ForeignKey(InterfaceOLT,
                                  on_delete=models.CASCADE,
                                  default=get_default_interface)
    onus = models.PositiveSmallIntegerField(default=0)
    onus_active = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return "%d/%d %s" % (int(self.interface.slot), int(
            self.port), self.description)

    class Meta:
        ordering = ["port"]


class StatisticsPortOLT(BaseModel):
    """
    Port 
    Estadisticas para el puerto de la olt.
    """
    port = models.ForeignKey(PortOLT, on_delete=models.CASCADE)
    unicast_rec = models.CharField(max_length=10, null=True, blank=True)
    multicast_rec = models.CharField(max_length=10, null=True, blank=True)
    broadcast_rec = models.CharField(max_length=10, null=True, blank=True)
    discarded_rec = models.CharField(max_length=10, null=True, blank=True)
    error_rec = models.CharField(max_length=10, null=True, blank=True)
    unicast_send = models.CharField(max_length=10, null=True, blank=True)
    multicast_send = models.CharField(max_length=10, null=True, blank=True)
    broadcast_send = models.CharField(max_length=10, null=True, blank=True)

    def __str__(self):
        return "%d/%d" % (self.port.interface.slot, self.port.port)

    class Meta:
        ordering = ["port"]


# class MoreDetailOLTS(BaseModel):
#     olt = models.ForeignKey(OLT, on_delete=models.CASCADE)
#     ram = models.CharField(max_length=10, null=True, blank=True)
#     ram_secondary = models.CharField(max_length=10, null=True, blank=True)
#     cpu = models.CharField(max_length=10, null=True, blank=True)
#     cpu_secondary = models.CharField(max_length=10, null=True, blank=True)
#     temperature = models.CharField(max_length=10, null=True, blank=True)
#     temperature_secondary = models.CharField(max_length=10, null=True, blank=True)
#     uptime = models.DateTimeField(null=True, blank=True)
#     watts = models.PositiveSmallIntegerField(default=0)
#     time = models.DateTimeField(null=True, blank=True)

#     def __str__(self):
#         return str(self.temperature)

#     class Meta:
#         ordering = ["-id"]


class OLTConnection(BaseModel):
    username = models.CharField(max_length=25)
    password = models.CharField(max_length=30)
    port = models.CharField(max_length=5, null=True, blank=True)
    key = models.CharField(max_length=120, blank=True, null=True)
    olt = models.ForeignKey(OLT, blank=True, on_delete=models.CASCADE)
    connections = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return self.olt.alias

    def release_connection(self):
        if self.connections > 0:
            self.connections -= 1
            self.save()

    def connection_usage(self):
        self.connections += 1
        self.save()

    def get_ip(self):
        return self.olt.ip.ip

    class Meta:
        ordering = ["username"]


PREVIUS_OLT_CHOISES = (
    (0, "Sin registro en sentinel"),
    (1, "Registro runstate offline"),
    (2, "Registro runstate online"),
)


class ONUAutoFind(Device):

    serial = models.CharField(max_length=30, db_index=True)
    olt = models.ForeignKey(OLT, on_delete=models.CASCADE)
    #model is Ont EquipmentID and vendor
    model = models.ForeignKey(ModelDevice,
                              null=True,
                              blank=True,
                              on_delete=models.CASCADE)
    onu_origin = models.ForeignKey("ONU",
                                   null=True,
                                   blank=True,
                                   on_delete=models.SET_NULL)
    number = models.PositiveSmallIntegerField()
    description = models.TextField(blank=True)
    autofind_time = models.DateTimeField(null=True, blank=True)
    #vendor = models.CharField(max_length=30, db_index=True)
    software_version = models.CharField(max_length=100, null=True, blank=True)
    fsp = models.CharField(max_length=10, null=True, blank=True)
    password = models.CharField(max_length=80,
                                db_index=True,
                                null=True,
                                blank=True)
    # service_number = models.PositiveSmallIntegerField(null=True, blank=True)
    service = models.ForeignKey('customers.Service',
                                null=True,
                                blank=True,
                                on_delete=models.CASCADE)
    in_previus_olt = models.PositiveSmallIntegerField(
        choices=PREVIUS_OLT_CHOISES)

    def __str__(self):
        return self.serial

    class Meta:
        ordering = ["-olt"]


class ONUNotDeleteManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(deleted_sentinel=False)


class ONUAllManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter()


class ONU(Device):

    alias = models.CharField(max_length=80,
                             db_index=True,
                             null=True,
                             blank=True)
    serial = models.CharField(max_length=30, db_index=True)
    olt = models.ForeignKey(OLT, on_delete=models.CASCADE)
    frame = models.PositiveSmallIntegerField()
    slot = models.PositiveSmallIntegerField()
    port = models.PositiveSmallIntegerField()
    description = models.TextField()
    model = models.ForeignKey(ModelDevice,
                              null=True,
                              blank=True,
                              on_delete=models.PROTECT)
    autofind = models.BooleanField(default=False)
    #fields latest
    main_software = models.CharField(max_length=100, null=True, blank=True)
    standby_software = models.CharField(max_length=100, null=True, blank=True)
    onu_id = models.PositiveSmallIntegerField()
    distance = models.PositiveSmallIntegerField(null=True, blank=True)
    TX = models.DecimalField(max_digits=5,
                             decimal_places=2,
                             null=True,
                             blank=True)
    RX = models.DecimalField(max_digits=5,
                             decimal_places=2,
                             null=True,
                             blank=True)
    service_port = models.PositiveSmallIntegerField(null=True, blank=True)
    upload_speed = models.PositiveSmallIntegerField(null=True, blank=True)
    download_speed = models.PositiveSmallIntegerField(null=True, blank=True)
    # service_number = models.PositiveSmallIntegerField(null=True, blank=True)
    service = models.ForeignKey('customers.Service',
                                null=True,
                                blank=True,
                                on_delete=models.CASCADE)
    service_number_verified = models.BooleanField(default=False)

    control_flag = models.CharField(max_length=10, null=True, blank=True)
    run_state = models.CharField(max_length=10, null=True, blank=True)
    config_state = models.CharField(max_length=10, null=True, blank=True)
    mapping_mode = models.CharField(max_length=10, null=True, blank=True)
    TR069_management = models.BooleanField(default=False,
                                           null=True,
                                           blank=True)
    TR069_ip_index = models.PositiveSmallIntegerField(null=True, blank=True)
    line_profile_id = models.PositiveSmallIntegerField(null=True, blank=True)
    line_profile_name = models.CharField(max_length=30, null=True, blank=True)

    status = models.BooleanField(default=False, null=True, blank=True)
    uptime = models.DateTimeField(null=True, blank=True)
    downtime = models.DateTimeField(null=True, blank=True)
    downcause = models.CharField(max_length=20, null=True, blank=True)
    time_uptime = models.DateTimeField(null=True, blank=True)
    ram = models.CharField(max_length=10, null=True, blank=True)
    cpu = models.CharField(max_length=10, null=True, blank=True)
    temperature = models.CharField(max_length=10, null=True, blank=True)
    # mac = models.CharField(max_length=17, db_index=True, null=True, blank=True)
    mac = models.ForeignKey('MACTable',
                            null=True,
                            blank=True,
                            on_delete=models.PROTECT)
    # t_ip = models.GenericIPAddressField(protocol="IPv4", db_index=True, null=True, blank=True)
    ip = models.ForeignKey('IPTable',
                           null=True,
                           blank=True,
                           on_delete=models.PROTECT)

    # from ONUHistorical
    deleted_sentinel = models.BooleanField(default=False)
    deleted_reason = models.TextField(null=True, blank=True)

    #ssid
    ssid = models.CharField(max_length=100, null=True, blank=True)
    ssid_5g = models.CharField(max_length=100, null=True, blank=True)
    password = models.CharField(max_length=100, null=True, blank=True)

    primary = models.BooleanField(default=False)

    objects = ONUNotDeleteManager()
    all_objects = ONUAllManager()

    def delete_this(self):
        self.delete()

    def __str__(self):
        if self.alias:
            return self.alias
        return self.serial

    def set_status_offline(self):
        self.status = False
        self.save()

    def get_downtime(self):
        # try:
        #     s = StatusONUS.objects.filter(onu=self).last()
        #     return s.downtime
        # except Exception as e:
        #     return False
        if (self.downtime):
            return self.downtime
        else:
            return False

    def get_downcause(self):
        # try:
        #     s = StatusONUS.objects.filter(onu=self).last()
        #     return s.downcause
        # except Exception as e:
        #     return False
        if (self.downcause):
            return self.downcause
        else:
            return False

    def get_status(self):
        # try:
        #     s = StatusONUS.objects.filter(onu=self).last()
        #     return s.status
        # except Exception as e:
        #     return False
        return self.status

    def get_uptime(self):
        try:
            if self.status:
                u = timezone.now() - self.uptime
                return timedelta_format(u)
            return None
        except Exception as e:
            print(e)
            return None

    def get_mac(self):
        try:
            # mac = MacONUS.objects.filter(status_field=True, onu=self).latest('created')

            newMac = ':'.join(self.mac.mac[i:i + 2] for i in range(0, 12, 2))
            return newMac.upper()
        # except MACTable.DoesNotExist:
        except Exception:
            return None

    def get_mac_field(self):
        try:
            # mac = MacONUS.objects.filter(status_field=True, onu=self).latest('created')
            return self.mac.mac.upper()
        # except MacONUS.DoesNotExist:
        except Exception:
            return None

    def get_ip(self):
        if (self.ip):
            return self.ip.ip
        else:
            return None

    def get_cpu(self):
        # try:
        #     return MoreDetailONUS.objects.filter(onu=self).latest('created').cpu
        # except Exception as e:
        #     return None

        return self.cpu

    def get_temperature(self):
        # try:
        #     return MoreDetailONUS.objects.filter(onu=self).latest('created').temperature
        # except Exception as e:
        #     return None

        return self.temperature

    def get_ram(self):
        # try:
        #     return MoreDetailONUS.objects.filter(onu=self).latest('created').ram
        # except Exception as e:
        #     return None
        return self.ram

    def save(self, *args, **kwargs):
        flag = True if not self.id else False
        super(ONU, self).save(*args, **kwargs)
        if flag:
            WirelessONUS.objects.create(onu=self)
            # MoreDetailONUS.objects.create(onu=self)
            # StatusONUS.objects.create(onu=self)
            # MacONUS.objects.create(onu=self)
            # IpONUS.objects.create(onu=self)
            # ServicePortONUS.objects.create(onu=self)

    @property
    def last_history_ip(self):
        first, *hx = self.history.order_by('history_date')
        history = [first]
        for hh in hx:
            if history[-1].ip != hh.ip:
                history.append(hh)
        last = history.pop()
        print('=================')
        print(last)
        print('=================')
        print(type(last))
        print('=================')
        #print(dir(last))
        return last

    class Meta:
        ordering = ["serial"]
        unique_together = (("alias", "serial", "olt"), ("serial", "olt"))


# class ONUHistorical(Device):

#     onu_origin = models.ForeignKey(ONU, on_delete=models.SET_NULL, null=True, blank=True)
#     deleted_sentinel = models.BooleanField(default=False)
#     deleted_reason = models.TextField(null=True, blank=True)
#     alias = models.CharField(max_length=80, db_index=True, null=True, blank=True)
#     serial = models.CharField(max_length=30, db_index=True)
#     olt = models.ForeignKey(OLT, on_delete=models.SET_NULL, null=True, blank=True)
#     frame = models.PositiveSmallIntegerField()
#     slot = models.PositiveSmallIntegerField()
#     port = models.PositiveSmallIntegerField()
#     description = models.TextField()
#     model = models.ForeignKey(ModelDevice, null=True, blank=True,
#                               on_delete=models.CASCADE)
#     autofind = models.BooleanField(default=False)
#     #fields latest
#     main_software = models.CharField(max_length=100, null=True, blank=True)
#     standby_software = models.CharField(max_length=100, null=True, blank=True)
#     onu_id = models.PositiveSmallIntegerField(null=True, blank=True)
#     distance = models.PositiveSmallIntegerField(null=True, blank=True)
#     TX = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
#     RX = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
#     service_port = models.PositiveSmallIntegerField(null=True, blank=True)
#     upload_speed = models.PositiveSmallIntegerField(null=True, blank=True)
#     download_speed = models.PositiveSmallIntegerField(null=True, blank=True)
#     # service_number = models.PositiveSmallIntegerField(null=True, blank=True)
#     service = models.ForeignKey('customers.Service', null=True, blank=True,
#                             on_delete=models.CASCADE)
#     service_number_verified = models.BooleanField(default=False)

#     control_flag = models.CharField(max_length=10, null=True, blank=True)
#     run_state = models.CharField(max_length=10, null=True, blank=True)
#     config_state = models.CharField(max_length=10, null=True, blank=True)
#     mapping_mode = models.CharField(max_length=10, null=True, blank=True)
#     TR069_management = models.BooleanField(default=False, null=True, blank=True)
#     TR069_ip_index = models.PositiveSmallIntegerField(null=True, blank=True)
#     line_profile_id = models.PositiveSmallIntegerField(null=True, blank=True)
#     line_profile_name = models.CharField(max_length=30, null=True, blank=True)

#     status = models.BooleanField(default=False, null=True, blank=True)
#     uptime = models.DateTimeField(null=True, blank=True)
#     downtime = models.DateTimeField(null=True, blank=True)
#     downcause = models.CharField(max_length=20, null=True, blank=True)
#     time_uptime = models.DateTimeField(null=True, blank=True)
#     ram = models.CharField(max_length=10, null=True, blank=True)
#     cpu = models.CharField(max_length=10, null=True, blank=True)
#     temperature = models.CharField(max_length=10, null=True, blank=True)
#     # mac = models.CharField(max_length=17, db_index=True, null=True, blank=True)
#     mac = models.ForeignKey('MACTable', null=True, blank=True,
#                             on_delete=models.PROTECT)
#     # t_ip = models.GenericIPAddressField(protocol="IPv4", db_index=True, null=True, blank=True)
#     ip = models.ForeignKey('IPTable', null=True, blank=True,
#                             on_delete=models.PROTECT)
#     # sugerencia
#     # iptable = models.ForeignKey('IPTable', null=True, blank=True,
#     #                           on_delete=models.PROTECT)

#     def __str__(self):
#         if self.alias:
#             return self.alias
#         return self.serial

#     class Meta:
#         ordering = ["serial"]


class BaseOnu(BaseModel):
    onu = models.ForeignKey(ONU, on_delete=models.CASCADE)

    class Meta:
        abstract = True


# class StatusONUS(BaseOnu):
#     status = models.BooleanField(default=False)
#     uptime = models.DateTimeField(null=True, blank=True)
#     downtime = models.DateTimeField(null=True, blank=True)
#     downcause = models.CharField(max_length=20, null=True, blank=True)

#     def __str__(self):
#         return str(self.status)

#     def status_last(self):
#         pass

#     class Meta:
#         unique_together = ("uptime", "created")

# class MoreDetailONUS(BaseOnu):
#     ram = models.CharField(max_length=10, null=True, blank=True)
#     cpu = models.CharField(max_length=10, null=True, blank=True)
#     temperature = models.CharField(max_length=10, null=True, blank=True)
#     control_flag = models.CharField(max_length=10, null=True, blank=True)
#     run_state = models.CharField(max_length=10, null=True, blank=True)
#     config_state = models.CharField(max_length=10, null=True, blank=True)
#     mapping_mode = models.CharField(max_length=10, null=True, blank=True)
#     TR069_management = models.BooleanField(default=False, null=True, blank=True)
#     TR069_ip_index = models.PositiveSmallIntegerField(null=True, blank=True)
#     line_profile_id = models.PositiveSmallIntegerField(null=True, blank=True)
#     line_profile_name = models.CharField(max_length=30, null=True, blank=True)

#     def __str__(self):
#         return str(self.temperature)

#     class Meta:
#         ordering = ["-id"]


class FirmwareONUS(BaseOnu):
    main_software = models.CharField(max_length=100, null=True, blank=True)
    standby_software = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return str(self.main_software)

    class Meta:
        ordering = ["-id"]


class MACTableManager(models.Manager):
    """
    Permite que al momento de crear una mac ésta tenga el formato deseado
    """
    def create(self, mac):
        mac_str = mac
        # procedemos a formatear la mac
        if ":" in mac_str:
            # verificamos que la mac tenga la longitud correcta
            if len(mac_str.split(':')) != 6:
                raise Exception(
                    'La mac ingresada tiene la longitud incorrecta')
            # verificamos que los elementos de la mac sean alfanuméricos
            for slot in mac_str.split(':'):
                if not slot.isalnum():
                    raise Exception('La mac debe ser alfanumerica')
            # Verificamos que esa mac no exista en la base de datos
            try:
                exists = MACTable.objects.get(mac=mac_str.upper())
                if exists:
                    raise Exception('Esa mac ya existe en la base de datos')
            except Exception as e:
                pass
            except Exception as e:
                pass
            # Finalmente, creamos la mac
            return super().create(mac=mac_str.upper())

        else:
            # verificamos que la mac tenga la longitud correcta
            if len(mac_str) != 12:
                raise Exception(
                    'La mac ingresada tiene la longitud incorrecta')
            # verificamos que los elementos de la mac sean alfanuméricos
            if not mac_str.isalnum():
                raise Exception('La mac debe ser alfanumerica')
            # Formateamos la mac
            mac_str = ':'.join(mac_str[i:i + 2] for i in range(0, 12, 2))
            # Verificamos que esa mac no exista en la base de datos
            try:
                exists = MACTable.objects.get(mac=mac_str.upper())
                if exists:
                    raise Exception('Esa mac ya existe en la base de datos')
            except Exception as e:
                pass
            # Creamos la nueva mac
            return super().create(mac=mac_str.upper())

    def format_mac(self, mac):
        """ Formatea la mac de la forma xxxxxxx a la forma XXX-XXX"""
        if ":" in mac:
            return mac.upper()
        else:
            mac = ':'.join(mac[i:i + 2] for i in range(0, 12, 2))
            return mac.upper()


class MACTable(BaseModel):
    mac = models.CharField(max_length=17, db_index=True, unique=True)
    objects = MACTableManager()

    def __str__(self):
        return self.mac

    class Meta:
        ordering = ["-id"]


class IPRange(BaseModel):
    ip = models.ForeignKey('IPTable', on_delete=models.PROTECT)
    mask = models.PositiveSmallIntegerField(validators=[MaxValueValidator(30)])

    def __str__(self):
        return str(self.ip)

    class Meta:
        ordering = ["-ip"]


class ServicePortHistorical(BaseModel):
    #onu
    serial = models.CharField(max_length=30, db_index=True)
    frame = models.PositiveSmallIntegerField()
    slot = models.PositiveSmallIntegerField()
    port = models.PositiveSmallIntegerField()
    description = models.TextField()
    model = models.ForeignKey(ModelDevice,
                              null=True,
                              blank=True,
                              on_delete=models.CASCADE)
    onu_id = models.PositiveSmallIntegerField()

    #service port
    service_port = models.PositiveSmallIntegerField()
    vlan_id = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(4096)])
    vlan_attr = models.CharField(max_length=15, db_index=True)
    port_type = models.CharField(max_length=15, db_index=True)
    status = models.BooleanField(default=False)

    def __str__(self):
        return str(self.service_port)

    class Meta:
        ordering = ["-id"]


class ServicePortONUS(BaseOnu):
    onu = models.ForeignKey(ONU,
                            on_delete=models.CASCADE,
                            null=True,
                            blank=True)
    onu_autofind = models.ForeignKey(ONUAutoFind,
                                     on_delete=models.CASCADE,
                                     null=True,
                                     blank=True)
    service_port = models.PositiveSmallIntegerField()
    vlan_id = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(4096)])
    vlan_attr = models.CharField(max_length=15, db_index=True)
    port_type = models.CharField(max_length=15, db_index=True)
    status = models.BooleanField(default=False)

    def __str__(self):
        return str(self.service_port)

    def save(self, *args, **kwargs):
        if not self.onu and not self.onu_autofind:
            raise ValidationError(_("Must associate an ONU/OnuAutofind"))
        super(ServicePortONUS, self).save(*args, **kwargs)

    class Meta:
        ordering = ["-id"]


class WirelessONUS(BaseOnu):
    DMZ_OR_PF_CHOISES = (
        (True, "DMZ"),
        (False, "PORT FORWARDING"),
    )
    wifi_old_band_ssid = models.CharField(max_length=180,
                                          null=True,
                                          blank=True)
    wifi_old_band_status = models.BooleanField(default=False)
    wifi_old_band_mode = models.CharField(max_length=80, null=True, blank=True)
    wifi_old_band_devices = models.PositiveIntegerField(null=True, blank=True)
    wifi_old_band_password = models.CharField(max_length=64,
                                              null=True,
                                              blank=True)
    wifi_old_band_channel = models.CharField(max_length=2,
                                             null=True,
                                             blank=True)
    wifi_old_band_administrative_status = models.BooleanField(default=False)

    wifi_new_band_ssid = models.CharField(max_length=180,
                                          null=True,
                                          blank=True)
    wifi_new_band_status = models.BooleanField(default=False)
    wifi_new_band_mode = models.CharField(max_length=80, null=True, blank=True)
    wifi_new_band_devices = models.PositiveIntegerField(null=True, blank=True)
    wifi_new_band_password = models.CharField(max_length=64,
                                              null=True,
                                              blank=True)
    wifi_new_band_channel = models.CharField(max_length=2,
                                             null=True,
                                             blank=True)
    wifi_new_band_administrative_status = models.BooleanField(default=False)

    dmz = models.CharField(max_length=15, null=True, blank=True)
    port_forwarding = models.CharField(max_length=10, null=True, blank=True)
    dmz_or_port_forwarding = models.BooleanField(default=True,
                                                 choices=DMZ_OR_PF_CHOISES)

    def __str__(self):
        if self.wifi_new_band_ssid:
            return self.wifi_new_band_ssid
        return self.wifi_old_band_ssid if self.wifi_old_band_ssid else ''

    class Meta:
        ordering = ["-id"]


class Ethernet(BaseModel):
    port_type = models.CharField(max_length=8, null=True, blank=True)
    port_id = models.PositiveSmallIntegerField()
    status = models.BooleanField(default=False)
    speeds = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(4096)], null=True, blank=True)
    duplex = models.CharField(max_length=8, null=True, blank=True)
    onu = models.ForeignKey(ONU, on_delete=models.CASCADE)

    def __str__(self):
        return self.onu.serial

    class Meta:
        ordering = ["port_id"]


class SSHConnection(BaseModel):
    username = models.CharField(max_length=25)
    password = models.CharField(max_length=30)
    port = models.CharField(max_length=5, null=True, blank=True)
    key = models.CharField(max_length=120, blank=True, null=True)
    onu = models.ForeignKey(ONU,
                            blank=True,
                            null=True,
                            on_delete=models.CASCADE)
    olt = models.ForeignKey(OLT,
                            blank=True,
                            null=True,
                            on_delete=models.CASCADE)

    def __str__(self):
        if self.olt:
            return str(self.olt.ip)
        else:
            return str(self.onu.ip)

    def get_ip(self):
        if self.olt:
            return self.olt.ip
        else:
            return self.onu.ip

    def save(self, *args, **kwargs):
        if not self.olt and not self.onu:
            raise ValidationError(_("Must associate an OLT/ONU"))
        if self.olt and self.onu:
            raise ValidationError(
                _("Solo debe asociar un solo tipo de dispositivo"))
        super(SSHConnection, self).save(*args, **kwargs)

    class Meta:
        ordering = ["username"]


class LogDevice(BaseModel):
    """Log generado por el dispositivo
    onu o olt no por el sistema"""
    entry = models.TextField()
    type = models.PositiveIntegerField(default=0)
    onu = models.ForeignKey(ONU,
                            blank=True,
                            null=True,
                            on_delete=models.CASCADE)
    olt = models.ForeignKey(OLT,
                            blank=True,
                            null=True,
                            on_delete=models.CASCADE)

    def __str__(self):
        return self.entry

    class Meta:
        ordering = ["-created"]


class Abnormalitie(BaseModel):
    reason = models.TextField()
    link = models.CharField(max_length=250)

    def __str__(self):
        return self.reason

    class Meta:
        ordering = ["-created"]


class AbnormalitieDevice(BaseModel):
    olt = models.ForeignKey(OLT,
                            blank=True,
                            null=True,
                            on_delete=models.CASCADE)
    onu = models.ForeignKey(ONU,
                            blank=True,
                            null=True,
                            on_delete=models.CASCADE)
    abnormalitie = models.ForeignKey(Abnormalitie,
                                     blank=True,
                                     null=True,
                                     on_delete=models.CASCADE)

    def __str__(self):
        return self.abnormalitie.reason

    class Meta:
        ordering = ["-created"]


class Operator(BaseModel):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=3)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-name"]


class TypeVLAN(BaseModel):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-name"]


class SystemVLAN(BaseModel):
    vlan_id = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(4096)])
    vlan_type = models.ForeignKey(TypeVLAN, on_delete=models.CASCADE)
    description = models.CharField(max_length=100)
    operator = models.ForeignKey(Operator, on_delete=models.CASCADE)

    def __str__(self):
        return self.description

    def olts(self):
        return 0

    class Meta:
        ordering = ["operator"]
        unique_together = (
            "vlan_id",
            "vlan_type",
        )


class OLTVLAN(BaseModel):
    vlan_id = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(4096)])
    vlan_type = models.ForeignKey(TypeVLAN, on_delete=models.CASCADE)
    vlan_system = models.ForeignKey(SystemVLAN,
                                    on_delete=models.CASCADE,
                                    null=True,
                                    blank=True)
    description = models.CharField(max_length=100, null=True, blank=True)
    stdn_port = models.PositiveSmallIntegerField()
    service_port = models.PositiveSmallIntegerField()
    olt = models.ForeignKey(OLT, on_delete=models.CASCADE)
    associated_ports = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.description

    def olts(self):
        return 0

    class Meta:
        ordering = ["vlan_id"]


class SystemSpeeds(BaseModel):
    tidsis = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(4096)])
    name = models.CharField(max_length=100, null=True, blank=True)
    cir = models.CharField(max_length=8)
    cbs = models.CharField(max_length=8)
    pir = models.CharField(max_length=8)
    pbs = models.CharField(max_length=8)
    pri = models.CharField(max_length=8)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        ordering = ["tidsis"]


class OLTSpeeds(BaseModel):
    speeds_system = models.ForeignKey(SystemSpeeds,
                                      on_delete=models.CASCADE,
                                      null=True,
                                      blank=True)
    olt = models.ForeignKey(OLT, on_delete=models.CASCADE)
    tid = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(4096)])
    name = models.CharField(max_length=100, null=True, blank=True)
    cir = models.CharField(max_length=8)
    cbs = models.CharField(max_length=8)
    pir = models.CharField(max_length=8)
    pbs = models.CharField(max_length=8)
    pri = models.CharField(max_length=8)

    def __str__(self):
        if self.name:
            return self.name
        return self.cir

    class Meta:
        ordering = ["tid"]


LEVEL_CHOISES = (
    (1, 'Super'),
    (2, 'Admin'),
    (3, 'Operator'),
)


class OLTUser(BaseModel):

    olt = models.ForeignKey(OLT, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, null=True)
    level = models.PositiveSmallIntegerField(choices=LEVEL_CHOISES, default=2)
    online = models.BooleanField(default=False)  #si esta online o no
    reenter_num = models.PositiveSmallIntegerField()
    profile = models.CharField(max_length=38)
    append_info = models.CharField(max_length=38)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["id"]


# ====================================================================#
# ======================= SMOKEPING MODELS ===========================#
# ====================================================================#
class IPTable(BaseModel):
    """
    Modelo que maneja las ip genéricas del archivo target
    para trabajar con Smokeping y RRDtool
    """
    ip = models.GenericIPAddressField(protocol="IPv4",
                                      db_index=True,
                                      verbose_name='IP genérica')
    host = models.CharField(max_length=100)
    menu = models.CharField(max_length=100)
    title = models.CharField(max_length=255, verbose_name='titulo')

    def __str__(self):
        return '%s' % (self.ip)

    def get_absolute_url(self):
        return reverse('iptable-detail',
                       kwargs={
                           'version': 'v1',
                           'pk': self.id
                       })

    class Meta:
        ordering = ["-id"]


class MenuSmokeping(BaseModel):
    """
    Modelo que maneja el menú que agrupa las IP genéricas
    almacenadas en el modelo IPTable de arriba.
    """
    title = models.CharField(max_length=255,
                             null=False,
                             blank=False,
                             verbose_name='titulo')
    iptable = models.ManyToManyField('IPTable',
                                     blank=True,
                                     verbose_name="IP's asociadas")

    order_field = models.IntegerField(verbose_name="Nro de orden",
                                      blank=True,
                                      null=True)

    def __str__(self):
        return '%s' % (self.title)

    @property
    def counterIps(self):
        counter = self.iptable.all().count()
        conect = 'IPs asociadas' if counter > 1 else 'IP asociada'
        return f'{counter} {conect}'

    def get_absolute_url(self):
        return reverse('menu-smokeping-detail',
                       kwargs={
                           'version': 'v1',
                           'pk': self.id
                       })

    def get_order_field(self):
        if self.order_field == None:
            self.order_field = self.id
        return self.order_field

    class Meta:
        ordering = ["-id"]
