
from django.db import models

from devices.models import OLT,MenuSmokeping,IPTable

from dynamic_preferences.registries import global_preferences_registry


#=====================================================================#
#======== MANEJO DE LAS VISTAS CREADAS EN EL SCHEMA GRAFICO ==========#
#==== PARA MOSTRAR LOS DATOS EN FRONT A TRAVES DE AMCHARTS4 JS =======#
#=====================================================================#


class ONUViewModel(models.Model):
    id = models.IntegerField(primary_key=True)
    serial = models.CharField(max_length=30)
    tx = models.DecimalField(max_digits=5, decimal_places=2)
    rx = models.DecimalField(max_digits=5, decimal_places=2)
    time_tx = models.DateTimeField()
    time_rx = models.DateTimeField()
    time_date = models.DateField()

    class Meta:
        managed = False
        db_table = 'public\".\"onu_tx_rx_view'


#=====================================================================#
#========  VISTAS CREADAS PARA CREAR EL ARCHIVO TARGETS DE  ==========#
#========================== SMOKEPING ================================#
#=====================================================================#

class Targets(models.Model):
    ip = models.IntegerField(primary_key=True)
    title_menu = models.CharField(max_length=50)
    ordenado_por = models.IntegerField(default=0)
    title_ipt = models.CharField(max_length=50)
    ip = models.CharField(max_length=50)
    host = models.CharField(max_length=50)
    menu = models.CharField(max_length=50)


    class Meta:
        managed = False
        db_table = 'public\".\"target_smokeping'


#===============================================================#
#========================= SECCION =============================#
#======================== SMOKEPING ============================#
#===============================================================#

def TargetsBySentinel(**kwargs):
    global_preferences = global_preferences_registry.manager()
    
    #==========================================#
    #===== funcion que genera el TARGETS ======#
    #==========================================#
    """
    Recibe kwargs con dos claves los mp y los ms
    mp = menus principal, son los items que se vemos en el menu lateral del server smokeping
        +title
    ms = menus secundarios, con una structura de ++ip,host,menu,title

    """

    validator = list()
    with open(f'Targets','w') as t:

        t.write("*** Targets ***" + '\n\n')
        t.write("probe = FPing" + '\n\n')

        t.write("menu = Top" + '\n')
        t.write("title = " + global_preferences['general_title_smokeping'] + '\n')
        t.write("remark = " + global_preferences['general_remark_smokeping'] + '\n\n')

        
        for mp in kwargs['mp']:
            t.write('+' + mp.upper().replace(' ','-') + '\n')
            t.write('menu = ' + mp.upper().replace(' ','-') + '\n')
            t.write('title = ' + mp.upper().replace(' ','-') + '\n')
            for ms in kwargs['ms']:
                # for m in ms:
                if ms['ip'] not in validator:
                    if mp == ms['menu_title']:
                        t.write('++' + str(ms['ip'].replace('.','-'))+ '\n')
                        t.write('menu = '+str(ms['menu'].replace('_','-').replace(' ','-'))+ '\n')
                        t.write('title = ' + str(ms['title'].replace('_','-').replace(' ','-'))+ '\n')
                        t.write('host = ' + str(ms['host'].replace('_','-'))+ '\n')
                        validator.append(ms['ip'])
                else:
                    a = None
            
            t.write('\n')


# def SmokepingTargetList():
#     olts = OLT.objects.all()
#     menus = MenuSmokeping.objects.all()

#     #SE CREAN LOS MENUS EN EL MODELO MENUSMOKEPING
#     for olt in olts:
#         if not menus.filter(title=olt.alias).first():
#             MenuSmokeping.objects.create(title = olt.alias,order_field = 0)


#     #SE ASOCIAN LAS IP'S AL MENUSMOKEPING
#     for olt in olts:
#         for menu in menus:
#             if olt.alias == menu.title:
#                 onus = olt.onu_set.exclude(ip=None)
#                 for onu in onus:
#                     if not IPTable.objects.filter(ip=onu.ip).first():
#                         iptable = {
#                             'ip':onu.ip, 
#                             'title':'onu-' + str(onu.serial),
#                             'host':'onu-' + str(onu.serial),
#                             'menu':olt.alias,
#                         }

#                         ipt = IPTable(**iptable)
#                         ipt.save()
#                         menu.iptable.add(ipt)

    
#     targets = Targets.objects.all()

#     menus = list()
#     mp = list()
#     ipt = list()
#     ms = list()

#     for target in targets:
#         orden = target.ordenado_por if target.ordenado_por != None else 0
#         menus.append({
#             'menu':target.title_menu,
#             'orden':orden
#         })
#         ms.append({
#             'ip':target.ip,
#             'host':target.host,
#             'menu':target.menu,
#             'title':target.title_ipt,
#             'menu_title':target.title_menu,

#         })

#     #ORDENAMOS POR EL ORDEN OTORGADO EN EL MENU SMOKEPING

#     menus = sorted(menus,key=lambda x: [x['orden']])

#     for m in menus:
#         if not m['menu'] in mp:
#             mp.append(m['menu'])

    
#     salida = dict(
#         mp = mp,
#         ms = ms,
        
#         )
    
#     TargetsBySentinel(**salida)
                    


#===============================================================#
#========================== CONEXION ===========================#
#============================= SSH =============================#
#===============================================================#
import os
import paramiko
''' 
'''


class SendTargetFile(object):
    __server = '192.168.66.23'
    __username = 'smoke'
    __password = 'smoke123'
    __local_path = '/var/opt/envSentinel/sentinel/sentinel/Targets'
    __remote_path = "Targets/Targets"

    def get_ssh_conection(self):

        try:
            ssh = paramiko.SSHClient()  # Iniciamos un cliente SSH
            # ssh.load_system_host_keys()  # Agregamos el listado de host conocidos
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())  # Si no encuentra el host, lo agrega automáticamente
            ssh.connect(
                    hostname = self.__server, 
                    username=self.__username, 
                    password=self.__password,
                    port=22,
                    )

            sftp = ssh.open_sftp()
            sftp.get(self.__local_path,self.__remote_path)
            sftp.close()
            ssh.close()
            return True 
        except Exception as e:
            return {
                'sError':str(e),
                'msj':'Error'
            }

    
    def __call__(self):
        ssh = self.get_ssh_conection()




def get_connection():
    """
    Conexion SSH
    """
    ssh = paramiko.SSHClient()  # Iniciamos un cliente SSH
    ssh.load_system_host_keys()  # Agregamos el listado de host conocidos
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())  # Si no encuentra el host, lo agrega automáticamente
    ssh.connect('192.168.66.23', username='sentinel', password='sentinel123')  # Iniciamos la conexión.
    return ssh

# def up_target(ssh_obj):
#     stdin, stdout, stderr = ssh_obj.exec_command("scp -rp jovanp@190.113.247.219:/home/devel/sites/sentinel/sentinel/Probes Targets/.")



def up_target_file():
    local_path = '/opt/envSentinel/sentinel/sentinel/Targets'
    remote_file_path = "/Targets"

    print('iniciamos la conexión')
    ssh_obj = get_connection()


    print('Iniciamos la Transferencia por ftp')
    sftp = ssh_obj.open_sftp()
    sftp.put(local_path,remote_file_path)
    sftp.close()
    ssh_obj.close()

