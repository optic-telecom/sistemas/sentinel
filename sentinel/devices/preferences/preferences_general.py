from decimal import Decimal

from dynamic_preferences.types import BooleanPreference, StringPreference,\
    DecimalPreference, IntegerPreference
from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry
from dynamic_preferences.users.registries import user_preferences_registry

# we create some section objects to link related preferences together
general = Section('general')
discussion = Section('discussion')


# We start with a global preference
@global_preferences_registry.register
class SiteTitle(StringPreference):
    section = general
    name = 'title'
    default = 'Sentinel'
    required = False


@global_preferences_registry.register
class MaintenanceMode(BooleanPreference):
    name = 'maintenance_mode'
    default = False


@global_preferences_registry.register
class ExecuteCommands(BooleanPreference):
    name = 'execute_commands'
    default = True


@global_preferences_registry.register
class ExecuteAutofind(BooleanPreference):
    name = 'execute_autofind'
    default = True


@global_preferences_registry.register
class ActionsOLTDatatable(BooleanPreference):
    name = 'actions_olt_datatable'
    default = True


@global_preferences_registry.register
class ColorError(StringPreference):
    name = 'color_error'
    default = 'red'
    required = False


@global_preferences_registry.register
class ColorAlert(StringPreference):
    name = 'color_alert'
    default = '#fa9c05'
    required = False


@global_preferences_registry.register
class ColorSuccess(StringPreference):
    name = 'color_success'
    default = 'green'
    required = False


@user_preferences_registry.register
class CommentNotificationsEnabled(BooleanPreference):
    """Do you want to be notified on comment publication ?"""
    section = discussion
    name = 'comment_notifications_enabled'
    default = True
    

###ssh
ssh = Section('ssh')

@global_preferences_registry.register
class MaxUserConnection(IntegerPreference):
    name = 'ssh_max_user_connection'
    default = 4
    required = False


## Provisiong ONU and OLT
@global_preferences_registry.register
class ExecuteSearchContratcs(BooleanPreference):
    name = 'execute_search_contracts'
    default = True


@global_preferences_registry.register
class ProvisiongONUValidateStatusMatrix(BooleanPreference):
    name = 'provisioning_onu_validate_status'
    default = True





#provisionamientos
@global_preferences_registry.register
class URLEndpointERP(StringPreference):
    name = 'url_endpoint_erp'
    default = 'https://optic.matrix2.cl/'
    required = False


@global_preferences_registry.register
class URLEndpointERPprovisioning(StringPreference):
    name = 'url_endpoint_erp_provisioning_onu'
    default = 'api/v1/onu-provisioning/'
    required = False


@global_preferences_registry.register
class VerifySerial(BooleanPreference):
    name = 'verify_serial'
    default = True


@global_preferences_registry.register
class VerifyFibraInPlan(BooleanPreference):
    name = 'verify_fibra_in_plan'
    default = True

@global_preferences_registry.register
class UserONUSAdmin(StringPreference):
    name = 'user_onus'
    default = 'telecoadmin'
    required = False

@global_preferences_registry.register
class PassONUSAdmin(StringPreference):
    name = 'pass_onus'
    default = 'A2Gy2xpu7BH6'
    required = False        

# ================================================================================ #
# ================================== SMOKEPING =================================== #
# ================================================================================ #

@global_preferences_registry.register
class PasswordSmokepingServer(StringPreference):
    name = 'password_smokeping_server'
    default = 'smoke123'
    required = True

@global_preferences_registry.register
class UserSmokepingServer(StringPreference):
    name = 'user_smokeping_server'
    default = 'smoke'
    required = True


@global_preferences_registry.register
class URLServerSmokeping(StringPreference):
    name = 'url_smokeping'
    default = 'http://192.168.66.23'
    required = True

@global_preferences_registry.register
class GeneralTitleSmokeping(StringPreference):
    name = 'general_title_smokeping'
    default = 'Gráficos de Latencia'
    required = True

@global_preferences_registry.register
class GeneralRemarkSmokeping(StringPreference):
    name = 'general_remark_smokeping'
    default = "Bienvenido al servidor de SmokePing de OPTIC. Aquí encontrarás la latencia de distintas IP"
    required = True


@global_preferences_registry.register
class UserSentinelSmokeServer(StringPreference):
    name = 'sentinel_smoke_user'
    default = 'sentinel'
    required = True

@global_preferences_registry.register
class PassSentinelSmokeServer(StringPreference):
    name = 'sentinel_smoke_pass'
    default = 'sentinel123'
    required = True


# ================================== SYSLOG ====================================== #
# ================================================================================ #

@global_preferences_registry.register
class URLMikrotik(StringPreference):
    name = 'url_mikrotik'
    default = '/'
    required = True
