from decimal import Decimal

from dynamic_preferences.types import BooleanPreference, StringPreference
from dynamic_preferences.registries import global_preferences_registry
from dynamic_preferences.users.registries import user_preferences_registry


@global_preferences_registry.register
class URLError(StringPreference):
    name = 'url_slack_error'
    default = 'https://hooks.slack.com/services/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'
    required = True

@global_preferences_registry.register
class URLDebug(StringPreference):
    name = 'url_slack_debug'
    default = 'https://hooks.slack.com/services/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'
    required = True

@global_preferences_registry.register
class URLFeedback(StringPreference):
    name = 'url_slack_feedback'
    default = 'https://hooks.slack.com/services/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'
    required = False

@global_preferences_registry.register
class URLExito(StringPreference):
    #caso 6 de provisionamiento
    name = 'url_slack_exito'
    default = 'https://hooks.slack.com/services/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'
    required = False


@global_preferences_registry.register
class URLExitoCaso2(StringPreference):
    name = 'url_slack_exito_caso2'
    default = 'https://hooks.slack.com/services/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'
    required = False


@global_preferences_registry.register
class URLExitoCaso3(StringPreference):
    name = 'url_slack_exito_caso3'
    default = 'https://hooks.slack.com/services/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'
    required = False


@global_preferences_registry.register
class URLErrorERP(StringPreference):
    name = 'url_slack_erp_error'
    default = 'https://hooks.slack.com/services/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'
    required = False


@global_preferences_registry.register
class URLEscalarCaso2(StringPreference):
    name = 'url_slack_escalar_caso2'
    default = 'https://hooks.slack.com/services/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'
    required = False



@global_preferences_registry.register
class OSlf(StringPreference):
    name = 'olt_speeds_list_filter'
    default = 'olt,speeds_system,id_data'
    required = False



@global_preferences_registry.register
class MessagesToSlack(BooleanPreference):
    """Indica si se envia o no los mensajes a slack"""
    name = 'message_slack'
    default = True