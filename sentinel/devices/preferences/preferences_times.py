from decimal import Decimal

from dynamic_preferences.types import BooleanPreference, StringPreference,\
    DecimalPreference, IntegerPreference
from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry
from dynamic_preferences.users.registries import user_preferences_registry

# we create some section objects to link related preferences together
general = Section('general')
discussion = Section('discussion')


### Section times
times = Section('times')

@global_preferences_registry.register
class SleepVeryLow(DecimalPreference):
    section = times
    name = 'sleep_very_low'
    default = Decimal(0.1)
    required = False


@global_preferences_registry.register
class SleepLow(DecimalPreference):
    section = times
    name = 'sleep_low'
    default = Decimal(0.5)
    required = False


@global_preferences_registry.register
class SleepLowMedium(DecimalPreference):
    section = times
    name = 'sleep_low_medium'
    default = Decimal(1)
    required = False


@global_preferences_registry.register
class SleepMedium(DecimalPreference):
    section = times
    name = 'sleep_medium'
    default = Decimal(2)
    required = False


@global_preferences_registry.register
class SleepMediumHigh(DecimalPreference):
    section = times
    name = 'sleep_medium_high'
    default = Decimal(3)
    required = False


@global_preferences_registry.register
class SleepHigh(DecimalPreference):
    section = times
    name = 'sleep_high'
    default = Decimal(4)
    required = False


@global_preferences_registry.register
class SleepVeryHigh(DecimalPreference):
    section = times
    name = 'sleep_very_high'
    default = Decimal(5)
    required = False
