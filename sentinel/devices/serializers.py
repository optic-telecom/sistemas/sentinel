import uuid
import re
from datetime import timedelta

#from django.contrib.auth import get_user_model
from django.utils import timezone
from django.http import JsonResponse
from django.core.management import call_command

from rest_framework import serializers
from rest_framework.serializers import HyperlinkedModelSerializer
#from rest_framework.fields import SerializerMethodField
from rest_framework.validators import UniqueValidator
from rest_framework.utils import model_meta

from common.serializers import QueryFieldsMixin

from .models import OLT, ModelDevice, ONU, LogDevice, AbnormalitieDevice,\
    Ethernet, InterfaceOLT, SSHConnection,\
    WirelessONUS, PortOLT, SystemVLAN, TypeVLAN, Operator,\
    OLTVLAN, SystemSpeeds, OLTSpeeds, ONUAutoFind, StatisticsPortOLT,\
    OLTConnection,MenuSmokeping,IPTable, MACTable
    
from customers.models import TypePLAN, Plan, Service
from dashboard.models import TaskModel    
from devices.ssh import MA5800T_SSH
from devices.wrappers import update_port_description_olt, update_alias_olt, create_new_speed
from devices.tasks import task_speeds_olt, task_new_olt
from dashboard.utils import LOG
from devices.utils import update_remote_info, ping_onu, is_valid_hostname,check_ip
from .postgres_views import ONUViewModel


from utp.models import Mikrotik, NotFoundMatrixEquipment
import re

__all__ = ['OLTSerializer', 'ModelDeviceSerializer', 'ONUSerializer', 'WifiSerializer',
           'UpdateRemoteOLTSerializer', 'LogDeviceSerializer', 'ConfigurationOnuSerializer',
           'AbnormalitieDeviceSerializer', 'EthernetSerializer', 'InterfaceOLTSerializer',
           'SSHOnuPingSerializer', 'OLTPortSerializer', 'SystemVLANSerializer',
           'OperatorSerializer', 'TypeVLANSerializer', 'OLTVLANSerializer',
           'SystemSpeedsSerializer', 'OLTSpeedsSerializer','ONUAutoFindSerializer', 'PortOLTSerializer',
           'StatisticsPortOLTSerializer', 'UpdateUptimeOLTSSerializer','ParentIPTableSerializers',
           'ONUProvisioningSerializer', 'OLTConnectionSerializer','OnuViewSerializer',
           'IPTableSerializers','MenuSmokepingSerializers', 'SSHOnuSignalSerializer']




#User = get_user_model()


_l = LOG()

class IPTableSerializers(serializers.ModelSerializer):

    def validate(self,attrs):
        errors = dict()
        ip = attrs.get('ip',None)
        
        if not ip:
            errors['ip'] = 'El campo IP no puede ser nulo'
        else:

            if len(ip) < 7:
                errors['ip'] = 'El campo IP debe contener como mínimo 7 dígitos'
            elif len(ip) > 15:
                errors['ip'] = 'El campo IP debe contener como máximo 15 dígitos'
            
            match = re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$",ip)
            if not match:
                errors['ip'] = 'El Patrón de la IP debe coincidir con los patrones de IPv4 o IPv6 válidos'

        if errors:
            raise serializers.ValidationError(errors)

        return attrs

    def filter_model(self,ip):
        return IPTable.objects.filter(ip=ip).exists()

    def validate_hostname(self,attrs):
        if len(attrs['host']) > 255:
            return False
        if attrs['host'][-1] == ".":
            attrs['host'] = attrs['host'][:-1] # strip exactly one dot from the right, if present
        allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)
        return all(allowed.match(x) for x in attrs['host'].split("."))
    
    def create(self, validated_data):
        errors = dict()
        ip = validated_data.pop('ip')
        
        if self.filter_model(ip):
            errors['ip'] = 'Esa IP ya se encuentra registrada dentro del sistema.'
        
        if errors:
            raise serializers.ValidationError(errors)
        else:
            host = validated_data['host'].lower() if validated_data['host'] else ip 
            validated_data['ip'] = ip
            validated_data['host'] = host
            validated_data['menu'] = validated_data['menu'].replace(' ','-').upper()
            validated_data['title'] = 'onu-' + validated_data['title'].replace(' ','-').upper()
            return IPTable.objects.create(**validated_data)

    def update(self, instance, validated_data):
        errors = dict()
        ip = validated_data.pop('ip')
        
        if ip != instance.ip and self.filter_model(ip):
            errors['ip'] = 'Esa IP ya se encuentra registrada dentro del sistema.'
        
        if errors:
            raise serializers.ValidationError(errors)
        else:
            validated_data['ip'] = ip

            for key,value in validated_data.items():
                setattr(instance,key,value)

            instance.save()
        return instance 
    
    class Meta:
        model = IPTable
        fields = [
            'pk',
            'ip',
            'host',
            'menu',
            'title',
        ]




class ModelDeviceSerializer(HyperlinkedModelSerializer):
    id = serializers.IntegerField(required=False, min_value=1)

    """
    Verificar en la creación si existe un modelo/brand de nombre similar
    para evitar duplicados

    1.- Eliminar espacios entre palabras y rstrip()
    2.- Pasar a upper case
    3.- Comparar resultados con los existentes en la base de datos

    Formato de nombre de modelo: sin espacios y en mayúsculas
    """

    def create (self, validated_data):

        # Hacemos el formato del nombre del modelo y de la marca
        model_name = validated_data.pop('model')
        model_name = model_name.replace(' ', '').upper().strip()
        print(model_name)

        brand_name = validated_data.pop('brand')
        brand_name = brand_name.replace(' ', '').upper().strip()
        print(brand_name)

        # Obtenemos todos los modelos que coincidan
        qs = ModelDevice.objects.filter(
            model__iexact=model_name,
            brand__iexact=brand_name)

        # Evaluamos si la marca es genérica para evitar duplicados
        if brand_name == "GENERICO" or brand_name == "1":
            brand_name = "GENERICO"
            qs = ModelDevice.objects.filter(
                model__iexact=model_name,
                brand__iexact=brand_name)

        # Si no existe el modelo entonces lo creamos
        if len(qs) == 0:
            newmodel = ModelDevice(
                brand=brand_name,
                model=model_name,
                kind=validated_data.pop('kind'))
            newmodel.save()
            return newmodel
        # Si ya existe entonces simplemente lo buscamos y devolvemos
        elif len(qs) == 1:
            print("El modelo ya existe, se devolverá la id del mismo")
            model = qs.last()
            return model
        elif len(qs) > 1:
            print("El modelo ya existe y está duplicado, se devolverá la id del mismo")
            model = qs.last()
            return model

    class Meta:
        model = ModelDevice
        fields = ('id', 'model', 'brand','kind')


class ListNodeField(serializers.Field):
    def to_internal_value(self, obj):
        if isinstance(obj, list):
            return obj[0]
        else:
            msg = self.error_messages['invalid']
            raise ValidationError(msg)
        pass



# from utp.serializers import MikrotikSerializer
class OLTSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    model = ModelDeviceSerializer(read_only=True)
    user = serializers.CharField(write_only=True)
    # ip = serializers.SlugRelatedField(slug_field='ip')
    mac = serializers.CharField(read_only=True)
    ip = serializers.CharField()
    # ip_node = serializers.CharField(required=True)
    # node = MikrotikSerializer(required=True)
    logic_node = serializers.PrimaryKeyRelatedField(required=False, allow_null=True, queryset=Mikrotik.objects.all())
    node = serializers.SerializerMethodField()
    password = serializers.CharField(write_only=True)
    # nodes_olt = ListNodeField(write_only=True)
    task_in_progress = serializers.SerializerMethodField()

    def get_node(self, obj):
        if obj.node is not None:
            return obj.node.alias
        else:
            return None

    def get_task_in_progress(self, obj):
        try:
            c = TaskModel.objects.get(id_obj = obj.id,
                                      model_obj = 'olt',
                                      name = 'task_new_olt',
                                      status_field = True)
            if c.result is True:
                return False
            else:
                return True

        except TaskModel.DoesNotExist as e:
            return False            


    def validate(self, attrs):
        
        if self.context['view'].action == 'partial_update' or\
            self.context['view'].action == 'update':
            
            return super().validate(attrs)

        # import ipaddress
        # try:
        #     ipaddress.ip_address(attrs['ip'])
        # except ValueError as e:
        #     _err = {'error':'La ip de la OLT es invalida'}
        #     raise serializers.ValidationError(_err)

        # try:
        #     ipaddress.ip_address(attrs['ip_node'])
        # except ValueError as e:
        #     _err = {'error':'La ip del nodo es invalida'}
        #     raise serializers.ValidationError(_err)

        return super().validate(attrs)

    # def validate_logic_node(self, id):
    #     try:
    #         node = Mikrotik.objects.get(id=id)
    #         return node
    #     except Mikrotik.DoesNotExist as e:
    #         _err = {'error':'El node es invalida'}
    #         raise serializers.ValidationError(_err)

    
    def validate_ip(self, ip):
        # if self.context['view'].action == 'partial_update' or\
        #     self.context['view'].action == 'update':

        import ipaddress
        try:
            ipaddress.ip_address(ip)
        except ValueError as e:
            _err = {'error':'La ip es invalida'}
            raise serializers.ValidationError(_err)

        ipt, created = IPTable.objects.get_or_create(ip=ip)

        return ipt
        # else:   
        #     return ip
        
    def create(self, validated_data):

        user = self.context['request'].user
        username = validated_data.pop('user')
        password = validated_data.pop('password')
        ip = validated_data.get('ip')
        # nodes = validated_data.pop('nodes_olt')
        id_data = uuid.uuid4()

        try:
            olt = None
            olt_ssh = MA5800T_SSH(**{
                          'host':str(ip),
                          'username':username,
                          'password':password
                          })
            olt_ssh.shell(None, user.username)
            model, firmware, uptime = olt_ssh.get_model_uptime_firmeware()
            try:
                model = ModelDevice.objects.get(model=model)
            except ModelDevice.DoesNotExist:
                model = ModelDevice.objects.create(model=model,id_data=id_data)

                _l.MODEL.ADD(by_representation=user, id_value=model.id,
                description=f'Modelo creado por crear OLT', entity_representation=repr(model))

            except Exception as e:
                print ("Error getmodel serializer OLT")
                print (e)
                raise serializers.ValidationError({
                  "error" : "No se puede obtener el modelo de la olt",
                  "detail" : str(e)
                })                      

            
            validated_data['model'] = model
            uptime = uptime.split(', ')
            days = int(uptime[0].replace('d',''))
            hours = int(uptime[1].replace('h',''))
            minutes = int(uptime[2].replace('m',''))
            sec = int(uptime[3].replace('s',''))
            value_uptime = timezone.now() - timedelta(days=days, hours=hours,
                                                      minutes=minutes, seconds=sec)


            olt = OLT.objects.create(**validated_data)
            olt.status = True
            olt.firmware = firmware            
            olt.uptime = value_uptime
            olt.time_uptime = timezone.now()
            olt.id_data = id_data
            olt.save()

            _l.OLT.ADD(by_representation=user, id_value=olt.uuid,
            description=f'Nueva OLT', entity_representation=repr(olt))

            # for node in nodes:
            #     try:
            #         olt.site.add(Mikrotik.objects.get(id=node))
            #     except Exception as e:
            #         pass

            OLTConnection.objects.create(olt=olt,
                                         username=username,
                                         password=password,
                                         id_data=id_data)

            """ task """             
            _task_new_olt = task_new_olt.delay(olt.uuid, uptime, id_data)
            TaskModel.objects.create(
              id_obj = olt.id,
              model_obj = 'olt',
              uuid = _task_new_olt.id,
              name = 'task_new_olt',
              id_data = id_data
            )

            # _task_vlans_olt = task_vlans_olt.delay(olt.uuid)
            # _task_speeds_olt = task_speeds_olt.delay(olt.uuid)
            # _task_get_onus = task_get_onus.delay(olt.uuid)
            # _task_ip_onus = task_ip_onus.delay(olt.uuid)
            # _task_uptime_onus = task_uptime_onus.delay(olt.uuid)
            return olt

        except AssertionError as e:
            
           
            _l.OLT.ADD(by_representation=user, id_value=ip, id_field='ip',
            description=f'fallo crear:{e}', entity_representation=repr(olt if olt else None))

            raise serializers.ValidationError({
              "error" : "No se puede conectar con la OLT",
              "detail" : str(e)
            })

    def update(self,instance, validated_data):
        alias = validated_data.get('alias',None)

        if alias:
            e, created = update_alias_olt(instance, alias, self.context['request'])
            if created:
              return super().create(validated_data)

            if e:
              raise serializers.ValidationError({
                "error" : "No se puede conectar con la OLT",
                "detail" : str(e)
              })
            else:
              raise serializers.ValidationError({
                "error" : "No se pudo actualizar el alias",
              })

        return super().update(instance, validated_data)

    class Meta:
        model = OLT
        fields = ('ip', 'mac', 'alias', 'description','model',
                  'status', 'uuid', 'onus', 'cards', 'uptime', 'ram',
                  'cpu', 'temperature', 'watts', 'cards_active', 'firmware',
                  'user', 'password','task_in_progress','id',
                  'logic_node', 'node')


class EthernetSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = Ethernet
        fields = ('port_type','port_id','status','speeds','duplex')


class InterfaceOLTSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = InterfaceOLT
        fields = ('interface','onus','onus_active','ports','ports_active','cpu','ram')   


class ONUSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):
    
    id = serializers.IntegerField(required=False, min_value=1)
    # olt = serializers.HyperlinkedRelatedField(queryset=OLT.objects.all(),
    #                                           view_name="olt_api-detail",
    #                                           lookup_field='uuid',)
    model = serializers.SerializerMethodField()
    # mac = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()
    uptime = serializers.SerializerMethodField()
    temperature = serializers.SerializerMethodField()
    ram = serializers.SerializerMethodField()
    cpu = serializers.SerializerMethodField()
    # ip = serializers.SerializerMethodField()
    wifi_old_band_ssid = serializers.SerializerMethodField()
    wifi_old_band_status = serializers.SerializerMethodField()
    wifi_old_band_mode = serializers.SerializerMethodField()
    wifi_old_band_devices = serializers.SerializerMethodField()
    wifi_new_band_ssid = serializers.SerializerMethodField()
    wifi_new_band_status = serializers.SerializerMethodField()
    wifi_new_band_mode = serializers.SerializerMethodField()
    wifi_new_band_devices = serializers.SerializerMethodField()
    lan = serializers.SerializerMethodField()
    olt_alias = serializers.CharField(read_only=True, source='olt.alias')
    service = serializers.SerializerMethodField()
    cache_wireless = None

    ip = serializers.CharField()
    mac = serializers.CharField()
    
    def validate_ip(self, ip):
        import ipaddress
        try:
            ipaddress.ip_address(ip)
        except ValueError as e:
            _err = {'error':'La ip es invalida'}
            raise serializers.ValidationError(_err)

        ipt, create = IPTable.objects.get_or_create(ip=ip)

        return ipt
    
    def validate_mac(self, mac):
        if mac:
            try:
                mact, create = MACTable.objects.get_or_create(mac=mac)
                return mact
                
            except Exception as e:
                print('***\n',e,'\n*****')
                _err = {'error':str(e)}
                raise serializers.ValidationError(_err)
        else:
            return None
    

    def get_wireless_model(self, obj):
        if not self.cache_wireless:
          cache = WirelessONUS.objects.filter(onu=obj).last()
          self.cache_wireless = cache
        return self.cache_wireless

    def get_model(self, obj):
        return obj.model.model if obj.model else None

    def get_service(self, obj):
        return obj.service.number if obj.service else None

    def get_lan(self, obj):
        try:
            id_d = Ethernet.objects.filter(status_field=True,
                                           onu=obj)\
                                           .latest('created').id_data
            qs = Ethernet.objects.filter(status_field=True,
                                         onu=obj,
                                         id_data=id_d)
        except Ethernet.DoesNotExist:
            qs = Ethernet.objects.none()

        d = EthernetSerializer(qs, many=True)
        return list(d.data)

    def get_mac(self, obj):
        return obj.get_mac()

    def get_ip(self, obj):
        return obj.get_ip()

    def get_status(self, obj):
        return obj.status

    def get_uptime(self, obj):
        return obj.get_uptime()

    def get_wifi_old_band_ssid(self, obj):
        try:
          return self.get_wireless_model(obj).wifi_old_band_ssid
        except Exception as e:
          return None

    def get_wifi_old_band_status(self, obj):
        try:
          return self.get_wireless_model(obj).wifi_old_band_status
        except Exception as e:
          return None

    def get_wifi_old_band_mode(self, obj):
        try:
          return self.get_wireless_model(obj).wifi_old_band_mode
        except Exception as e:
          return None

    def get_wifi_old_band_devices(self, obj):
        try:
          return self.get_wireless_model(obj).wifi_old_band_devices
        except Exception as e:
          return None

    def get_wifi_new_band_ssid(self, obj):
        try:
          return self.get_wireless_model(obj).wifi_new_band_ssid
        except Exception as e:
          return None

    def get_wifi_new_band_status(self, obj):
        try:
          return self.get_wireless_model(obj).wifi_new_band_status
        except Exception as e:
          return None

    def get_wifi_new_band_mode(self, obj):
        try:
          return self.get_wireless_model(obj).wifi_new_band_mode
        except Exception as e:
          return None

    def get_wifi_new_band_devices(self, obj):
        try:
          return self.get_wireless_model(obj).wifi_new_band_devices
        except Exception as e:
          return None

    def get_cpu(self, obj):
        return obj.get_cpu()

    def get_temperature(self, obj):
        return obj.get_temperature()

    def get_ram(self, obj):
        return obj.get_ram()

    def create(self, validated_data):
        
        ssid = validated_data.get('ssid', None)
        password = validated_data.get('password', None)
        ssid_5g = validated_data.get('ssid_5g', None)

        alias = validated_data.get('alias', None) 
        id = validated_data.get('id', None) 
        serial = validated_data.get('serial', None) 
        description = validated_data.get('description', None) 
        model = validated_data.get('model', None) 
        service = validated_data.get('service', None) 
        mac = validated_data.get('mac', None) 
        ip = validated_data.get('ip', None) 
        primary = validated_data.get('primary', False)
        model = validated_data.get('model', None) 

        try:
            onu = ONU.all_objects.get(serial=validated_data.get('serial', None))

            if ssid:
                onu.ssid = ssid
            if ssid_5g:
                onu.ssid_5g = ssid_5g
            if password:
                onu.password = password

            onu.alias = alias
            onu.description = description
            onu.model = model
            onu.service = service
            onu.mac = mac
            onu.ip = ip
            onu.primary = primary
            onu.model = model

            onu.save()
        except ONU.DoesNotExist:
            not_found = NotFoundMatrixEquipment.objects.create()
            not_found.alias = alias
            not_found.id = id
            not_found.serial = serial
            not_found.description = description
            not_found.model = model
            not_found.service = service
            not_found.mac = mac
            not_found.ip = ip
            not_found.primary = primary
            not_found.model = model
            not_found.save()
            print(not_found)
            # raise serializers.ValidationError({
            #     "error" : f'fallo crear: ONU con serial number con existe',
            #     "detail" : 'fallo crear: ONU con serial number con existe'
            # })
            # empty = ONU.objects.create(frame=1, slot=1, port=1,
            #                            description='aa', onu_id=1,
            #                            olt_id=1)
            return not_found
        except Exception as e:
            raise serializers.ValidationError({
                "error" : f'{e}',
                "detail" : str(e)
            })
        return onu
    class Meta:
        model = ONU
        fields = ('id', 'ip', 'mac', 'alias', 'serial', 'description', 'status',
                  'uuid', 'distance', 'service_port', 'onu_id',
                  'TX','RX', 'frame','slot','port','olt_alias',
                  'wifi_old_band_ssid','wifi_old_band_status','wifi_old_band_mode',
                  'wifi_old_band_devices','wifi_new_band_ssid','wifi_new_band_status',
                  'wifi_new_band_mode','wifi_new_band_devices','model', 'ram',
                  'service','service_number_verified', 'uptime','cpu', 'temperature',
                  'main_software','lan', 'ssid', 'password', 'ssid_5g')


class UpdateRemoteOLTSerializer(serializers.Serializer):
    updated = serializers.BooleanField()

    def update(self, instance, validated_data):
        serializers.raise_errors_on_nested_writes('update',
                                                  self,
                                                  validated_data)
        updated = update_remote_info(instance, validated_data)
        return {'updated':updated}


class UpdateUptimeOLTSSerializer(serializers.Serializer):

    def create(self, validated_data):
        from devices.management.commands.get_uptime_olts import Command
        command = Command()
        command.handle()
        return {'updated':True}


class LogDeviceSerializer(serializers.ModelSerializer):

    class Meta:
        model = LogDevice
        fields = ('created', 'entry', 'type')


class ConfigurationOnuSerializer(serializers.ModelSerializer):
    def update(self, instance, validated_data):
        serializers.raise_errors_on_nested_writes('update', self, validated_data)
        dmz_or_port = validated_data.pop('dmz_or_port_forwarding', None)

        if dmz_or_port:
            instance.dmz_or_port_forwarding = dmz_or_port
            dmz = validated_data.pop('dmz', None)
            if dmz:
                instance.dmz = dmz
                instance.save()
                return instance
            else:
                n_instance = WirelessONUS.objects.create(
                    onu=instance.onu,
                    **validated_data)
                return n_instance
        

    class Meta:
        model = WirelessONUS
        fields = ('wifi_old_band_ssid','wifi_old_band_status',
                  'wifi_new_band_ssid','wifi_new_band_status',
                  'wifi_new_band_mode','wifi_old_band_mode',
                  'wifi_old_band_password','wifi_new_band_password',
                  'wifi_old_band_channel','wifi_new_band_channel',
                  'dmz','port_forwarding','dmz_or_port_forwarding')


class AbnormalitieDeviceSerializer(QueryFieldsMixin, serializers.ModelSerializer):
    
    abnormalitie = serializers.CharField(source='abnormalitie.reason')
    link = serializers.CharField(source='abnormalitie.link')
    
    class Meta:
        model = AbnormalitieDevice
        fields = ('abnormalitie','link')   

       
class SSHOnuPingSerializer(QueryFieldsMixin, serializers.Serializer):

    ip = serializers.CharField(write_only=True)
    message = serializers.CharField(read_only=True)
    status = serializers.BooleanField(read_only=True)
    onu_id = serializers.CharField(write_only=True, required=False)
    fsp    = serializers.CharField(write_only=True, required=False, min_length=5)
    olt    = serializers.UUIDField(write_only=True, required=False)

    def validate(self, attrs):
        ip = attrs.get('ip', None)

        if not ip:
            errors= {'error':'Este campo es requerido'}
            raise serializers.ValidationError(errors)
        return super().validate(attrs)

    def update(self, instance, validated_data):
        from dashboard.simulate import Simulate
        with Simulate('provisioning_onu','ping_onu', True) as s:
            if s.response:
                import time
                time.sleep(2)
                return s.response

        ip = validated_data.get('ip', None)
        err, message = ping_onu(instance, ip, self.context['request'])
        if err:
            return {'status':False, 'message':err}
        return {'status':True, 'message':str(message)}


    def create(self, validated_data):
        #Se permite hacer ping con una onu sin estar registrada
        from dashboard.simulate import Simulate
        with Simulate('provisioning_onu','ping_onu', True) as s:
            if s.response:
                import time
                time.sleep(2)
                return s.response

        ip = validated_data.get('ip', None)
        fsp = validated_data.get('fsp').split('/')
        onu_id = validated_data.get('onu_id')
        olt = OLT.objects.get(uuid=validated_data.get('olt'))

        onu = ONU()
        onu.olt = olt
        onu.onu_id = onu_id
        onu.slot = fsp[1]
        onu.port = fsp[2]

        err, message = ping_onu(onu, ip, self.context['request'])
        if err:
            return {'status':False, 'message':err}
        return {'status':True, 'message':str(message)}


class SSHOnuSignalSerializer(QueryFieldsMixin, serializers.Serializer):

    signal = serializers.CharField(read_only=True)
    status = serializers.BooleanField(read_only=True)
    onu_id = serializers.CharField(write_only=True, required=False)
    fsp    = serializers.CharField(write_only=True, required=False, min_length=5)
    olt    = serializers.UUIDField(write_only=True, required=False)

    def update(self, instance, validated_data):
        #Se busca la señal de una onu ya registrada en el sistema
        from dashboard.simulate import Simulate
        with Simulate('provisioning_onu','signal_onu', True) as s:
            if s.response:
                import time
                time.sleep(2)              
                return s.response

        fsp = validated_data.get('fsp', f'{instance.frame}/{instance.slot}/{instance.port}')
        onu_id = validated_data.get('onu_id', instance.onu_id)
        res_optical = signal_onu(instance, fsp, onu_id, self.context['request'])
        signal_error = False
        if isinstance(res_optical, dict):
            signal_error = res_optical["signal_error"]

        if signal_error:
            return {'status':False, 'message':signal_error}
        return {'status':True, 'signal':str(res_optical)}

    def create(self, validated_data):
        #Se permite buscar la señal sin una onu aun registrada
        from dashboard.simulate import Simulate
        with Simulate('provisioning_onu','signal_onu', True) as s:
            if s.response:
                import time
                time.sleep(2)
                return s.response

        fsp = validated_data.get('fsp')
        onu_id = validated_data.get('onu_id')
        olt = OLT.objects.get(uuid=validated_data.get('olt'))
        res_optical = signal_onu(None, fsp, onu_id, self.context['request'], olt)
        signal_error = False
        
        if isinstance(res_optical, dict):
            try:
                signal_error = res_optical["signal_message"]
            except KeyError as e:
                signal_error = res_optical["signal_error"]
        
        if signal_error:
            return {'status':False, 'message':signal_error}
        return {'status':True, 'signal':str(res_optical)}


class OLTPortSerializer(QueryFieldsMixin, serializers.ModelSerializer):
    """
    Serializer solo para los puerto de una OLT (campo select)
    """
    id = serializers.SerializerMethodField()
    description = serializers.SerializerMethodField()

    def get_description(self, o):
        if o.description != '-' and o.description != '':
            desc = (o.interface.slot, o.port, o.description,o.onus_active, o.onus)
            return "0/%d/%d (%s) (%d-%d)" % desc
        return "0/%d/%d (%d-%d)" % (o.interface.slot, o.port,o.onus_active, o.onus)

    def get_id(self, o):
        return "0/%d/%d" % (o.interface.slot, o.port)

    class Meta:
        model = PortOLT
        fields = ('id','description',)   


class SystemVLANSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = SystemVLAN
        fields = ('id', 'vlan_id', 'vlan_type', 'description', 'operator')


class OLTVLANSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = OLTVLAN
        fields = ('vlan_id', 'vlan_type', 'vlan_system', 'description',
                  'stdn_port','service_port','olt','associated_ports')


class OLTSpeedsSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    def create(self, validated_data):
        _err, created = create_new_speed(
                          validated_data.get('olt'),
                          validated_data.get('tid'),
                          validated_data.get('name'),
                          validated_data.get('cir'),
                          validated_data.get('cbs'),
                          validated_data.get('pir'),
                          validated_data.get('pbs'),
                          validated_data.get('pri'),
                          self.context['request']
                        )
        if created:
            return super().create(validated_data)

        if _err:
            raise serializers.ValidationError({
                "error" : "No se puede conectar con la OLT",
                "detail" : str(_err)
            })
        else:
            raise serializers.ValidationError({'error':'No se creo velocidad en la OLT, index ya existe'})

    class Meta:
        model = OLTSpeeds
        fields = ('speeds_system', 'olt', 'tid', 'name',
                  'cir','cbs','pir','pbs','pri')


class OperatorSerializer(QueryFieldsMixin, serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, min_value=1)
    
    class Meta:
        model = Operator
        fields = ('id', 'name', 'code')


class TypeVLANSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = TypeVLAN
        fields = ('id', 'name',)

        

class SystemSpeedsSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = SystemSpeeds
        fields = ('id', 'name', 'cir','tidsis',
                  'cbs', 'pir','pbs','pri')


 


class ONUAutoFindSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    olt = serializers.CharField(source='olt.alias')
    model = serializers.CharField(source='model.model', default=None)

    class Meta:
        model = ONUAutoFind
        fields = ('serial','model','number','description','fsp',
                  'service','in_previus_olt','olt','uuid')


class ONUProvisioningSerializer(QueryFieldsMixin, serializers.Serializer):

    onu = serializers.CharField()

    def create(self, validated_data):
      
        return validated_data


class WifiSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = WirelessONUS
        fields = ('wifi_old_band_ssid','wifi_old_band_status',
                  'wifi_new_band_ssid','wifi_new_band_status',
                  'wifi_new_band_mode','wifi_old_band_mode',
                  'wifi_old_band_password','wifi_new_band_password',
                  'wifi_old_band_channel','wifi_new_band_channel',
                  'wifi_old_band_devices','wifi_new_band_devices')

class PortOLTSerializer(QueryFieldsMixin, serializers.ModelSerializer):
    """
    Serializer para obtener el detalle completo del puerto
    """
    interface = serializers.SerializerMethodField()
    olt = serializers.CharField(read_only=True, source='interface.olt.alias')

    def get_interface(self, obj):
        return "{0.frame}/{0.slot}".format(obj.interface)

    def update(self, instance, validated_data):
        serializers.raise_errors_on_nested_writes('update', self, validated_data)
        description = validated_data.get('description', None)
        if description:
            desc, res = update_port_description_olt(
                          instance.interface.olt,
                          instance.interface.slot,
                          instance.port,
                          description,
                          self.context['request']
                        )
            if res:
                return super().update(instance,validated_data)
            else:
                errors= {'error': desc}
                raise serializers.ValidationError(errors)

        else:
            return super().update(instance,validated_data)

    class Meta:
        model = PortOLT
        fields = ('port','fsp_field',
                  'description','interface',
                  'onus','onus_active','olt','pk')


class StatisticsPortOLTSerializer(QueryFieldsMixin, serializers.ModelSerializer):
    """
    Serializer 
    """
    class Meta:
        model = StatisticsPortOLT
        fields = ('unicast_rec','multicast_rec',
                  'broadcast_rec','discarded_rec',
                  'error_rec','unicast_send',
                  'multicast_send','broadcast_send')


class OLTConnectionSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    olt = serializers.UUIDField(source="olt.uuid")

    def create(self, validated_data):
        olt = OLT.objects.get(uuid=validated_data.pop('olt')['uuid'])
        ssh = OLTConnection.objects.create(olt=olt,
                                           **validated_data)
        return ssh

    class Meta:
        model = OLTConnection
        fields = ('username','password',
                  'port','key', 'olt')


#=====================================================================#
#================== MANEJO DE VISTAS POSTGRES ========================#
#=====================================================================#


class OnuViewSerializer(serializers.ModelSerializer):
    """
    Serializer para mostrar los datos de la vista de postgres
    onu_tx_rx_view.
    """
    time_date = serializers.DateField(format="%d/%m/%Y")
    class Meta:
        model = ONUViewModel
        fields = (
            'id',
            'serial',
            'tx',
            'rx',
            'time_tx',
            'time_rx',
            'time_date'
        )



class ParentIPTableSerializers(serializers.ModelSerializer):
    padre = serializers.CharField(max_length=200,read_only=True)
    class Meta:
        model = IPTable
        fields = [
            'pk',
            'ip',
            'host',
            'menu',
            'title',
            'padre',
        ]



class MenuSmokepingListIPSerializers(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    ip = serializers.IntegerField(read_only=True)
    title = serializers.CharField(read_only=True)
    order_field = serializers.IntegerField(read_only=True)
    iptable = IPTableSerializers(read_only=True,many=True)

class MenuSmokepingSerializers(serializers.ModelSerializer):

    def validate_iptable(self,iptables):
        if not self.instance:
            menus = MenuSmokeping.objects.filter(iptable__in=iptables)
            if menus.exists():
                raise serializers.ValidationError('Alguna de las IP está registrada en este u otro menú')
        return iptables

    def create(self, validated_data):

        iptable = validated_data.pop('iptable',list())
        validated_data['title'] = validated_data['title'].replace(' ','-').upper()
        validated_data['order_field']=int(validated_data['order_field'])

        instance = MenuSmokeping.objects.create(**validated_data)
        
        if iptable:
            for obj in iptable:
                instance.iptable.add(obj)
                instance.save()
        
        instance.save()

        # for ip in iptable:
        #     ip.menusmokeping_set.add(instance)
        #     ip.save()
            
        return instance

    def update(self, instance,validated_data):
        iptables = validated_data.pop('iptable',instance.iptable.all())

        for key,value in validated_data.items():
            if key == 'title':
                value = value.replace(' ','-').upper()
            setattr(instance,key,value)
            instance.save()

        instance.iptable.set(iptables)


        return instance 

    class Meta:
        model = MenuSmokeping
        fields = [
            'id',
            'title',
            'order_field',
            'iptable'
        ]

class SmokepingImgSerializer(serializers.Serializer):
    date = serializers.CharField(read_only=True)
    src = serializers.CharField(read_only=True)
    url = serializers.CharField(read_only=True)

class MacSerializer(serializers.ModelSerializer):
    class Meta:
        model = MACTable
        fields = ('id','mac')