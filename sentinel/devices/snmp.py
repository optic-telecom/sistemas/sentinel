from pysnmp.hlapi import *


class ManagerSNMP:

    list_oids = []

    def __init__(self, community=None, ip=None, port=161):
        self.community = community if community else '0pT1cSnmp'
        self.ip = ip
        self.port = port

    def __add__(self, oid):
        self.add(oid)

    def __neg__(self):
        self.list_oids.pop()

    def add(self, oid):
        self.list_oids.append(ObjectType(ObjectIdentity(oid)))

    def getdata(self):
        print ((self.ip, self.port))
        return next(
            getCmd(SnmpEngine(),
            CommunityData(self.community),
            UdpTransportTarget((self.ip, self.port)),
            ContextData(),
            *self.list_oids
            )
        )



class MA5608T_SNMP(ManagerSNMP):

    output_binary = None

    def oid_syslocation(self):
        self + '1.3.6.1.2.1.1.6.0'

    def oid_sysdescr(self):
        self + '1.3.6.1.2.1.1.1.0'

    def oid_consumption(self):
        self + 'iso.3.6.1.4.1.2011.2.6.7.1.1.1.1.11.0'

    def oid_temperature(self):
        self + 'iso.3.6.1.4.1.2011.6.3.3.2.1.13'

    def oid_sysuptime(self):
        self + 'iso.3.6.1.2.1.1.3'



class MA5800_SNMP(ManagerSNMP):

    output_binary = None

    def oid_consumption(self):
        self + 'iso.3.6.1.4.1.2011.2.6.7.1.1.1.1.11.0'

    def oid_temperature(self):
        self + 'iso.3.6.1.4.1.2011.6.3.3.2.1.13'
        #self.list_oids.append(ObjectType(ObjectIdentity('1.3.6.1.4.1.2011.6.3.3.2.1.13')))
        
    def oid_signal_all_onus(self):
        self + '.1.3.6.1.4.1.2011.6.128.1.1.2.51.1.4'

    def oid_sysdescr(self):
        self.list_oids.append(ObjectType(ObjectIdentity('SNMPv2-MIB', 'sysDescr', 0)))