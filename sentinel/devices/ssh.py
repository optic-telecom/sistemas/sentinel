import re, time
import logging
import socket
import paramiko
import os
import pwd
import grp
from os import getenv
from os.path import join
from datetime import datetime
from django.conf import settings
from dynamic_preferences.registries import global_preferences_registry
from dashboard.utils import LOG




def get_logger(name, is_in_cron=True):
    #logger = logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG) # WARNING ERROR

    now = "{0.year}-{0.month}-{0.day}-{0.hour}".format(datetime.now()) 

    if is_in_cron:
        uid = pwd.getpwnam("sentinel").pw_uid
        gid = grp.getgrnam("sentinel").gr_gid
        try:
        
            os.chown(join(settings.BASE_DIR_LOG,'ssh_%s_cron.log' % now), uid, gid)
        except:
            pass
        file_log = open(join(settings.BASE_DIR_LOG,'ssh_%s_cron.log' % now), "a")
    else:
        uid = pwd.getpwnam("sentinel").pw_uid
        gid = grp.getgrnam("sentinel").gr_gid
        try:
        
            os.chown(join(settings.BASE_DIR_LOG,'ssh_%s_web.log' % now), uid, gid)
        except:
            pass
        file_log = open(join(settings.BASE_DIR_LOG,'ssh_%s_web.log' % now), "a")
    format_log = "%(levelname)-.3s %(asctime)s.%(msecs)03d %(name)s %(message)s"

    handler1 = logging.StreamHandler(file_log)
    handler1.setFormatter(logging.Formatter(format_log, "%Y%m%d-%H:%M:%S"))

    logger.addHandler(handler1)
    return logger



class ManagerSSH:

    def __init__(self, host, username, password, port=22, key=None):
        self.host = host
        self.username = username
        self.password = password
        self.port = port
        self.key = key
        self.get_logger()
        self.ssh = None
        self.name_logger = None

    def get_logger(self):
        if hasattr(self,'name_logger') and self.name_logger is not None:
            self.logger = get_logger(self.name_logger)
        else:
            self.logger = get_logger('ManagerSSH')
        print ('MI get_logger es',self.logger)
    def __repr__(self):
        """
        Returns a string representation of this object, for debugging.
        """
        return "<ManagerSSH from sentinel.devices.ssh>"

    def connect(self):
        self.ssh = paramiko.SSHClient()
        try:
            logging.basicConfig(level=logging.DEBUG)
            self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            if self.key:
                self.ssh.connect(self.host, username=self.username,
                                 password=self.password,
                                 port=self.port, key_filename=self.key)
            else:
                self.ssh.connect(self.host, username=self.username,
                                 password=self.password, port=self.port, allow_agent=False, look_for_keys=False)                
                
            self.ssh._transport.get_banner()
            self.ssh._transport.set_log_channel('SSHSentinel')
            return True

        except paramiko.ssh_exception.NoValidConnectionsError as e:
            self.logger.error('Connection Failed')
            self.logger.error(e)
            return False

        except socket.gaierror as e:
            self.logger.error('Socket not being to resolve the address (%s)' % self.host)
            self.logger.error(e)
            return False

        except paramiko.SSHException as e:
            self.logger.error('Error connecting to host %s' % self.host)
            self.logger.error('Host is not a valid SSH server - Username is not valid or incorrect password')
            self.logger.error(e)
            return False

        except TimeoutError:
            self.logger.error('The %s host didn\'t respond for TimeoutError' % self.host)
            return False

        except Exception as err:
            self.logger.error('Exception %s' % str(err))
            return False


    def close(self):
        if self.ssh:
            self.ssh.close()

    def __del__(self):
        if self.ssh:
            self.ssh.close()


class DeviceSSHMixin(object):
    
    chan = None
    connected = False
    decode_type = 'UTF-8'
    raw = None
    buff = 1024 * 2
    _l = None
    try:
        global_preferences = global_preferences_registry.manager()
        time_sleep = global_preferences.get('times__sleep_high')
    except Exception as e:
        time_sleep = 5
    
    err_connection_msg = 'Usted no tiene conexion ssh.'

    def __init__(self, host, username, password, port=22, key=None):
        super().__init__(host, username, password, port, key)
        if self.connect():
            self.connected = True

    def get_sleep(self):
        env = getenv("TIME_COMMAND_SSH")
        if env:
            time.sleep(float(env))
        else:
            time.sleep(self.time_sleep)

    def get_sleep_onus(self):
        env = getenv("TIME_COMMAND_SSH_ONUS")
        time_onus = float(env) if env else self.time_sleep
        print ("time onus",time_onus)
        time.sleep(time_onus)

    def shell(self, olt=None, by_representation=None):
        global_preferences = global_preferences_registry.manager()
        assert self.connected, (self.err_connection_msg)
        self.chan = self.ssh.invoke_shell()
        self._in = self.chan.makefile('wb', -1)
        self._out = self.chan.makefile('rb', -1)
        self._err = self.chan.makefile_stderr('rb', -1)
        time.sleep(global_preferences['times__sleep_very_low'])
        self.command('enable\n')
        time.sleep(global_preferences['times__sleep_very_low'])
        self.command('config\n')
        self.get_sleep()
        if olt:
            self.olt = olt
        if by_representation:
            self.by_representation = by_representation

    def simple_shell(self):
        global_preferences = global_preferences_registry.manager()
        assert self.connected, (self.err_connection_msg)
        self.chan = self.ssh.invoke_shell()
        self._in = self.chan.makefile('wb', -1)
        self._out = self.chan.makefile('rb', -1)
        self._err = self.chan.makefile_stderr('rb', -1)
        time.sleep(global_preferences['times__sleep_very_low'])

    def command(self, cmd, buff=None, split=True, pause=None, flush=True):
        assert self.connected, (self.err_connection_msg)
        #self.logger.info("command: %s " % cmd)
        self.log_class.info("s: %s " % cmd)

        if hasattr(self,'_l') and hasattr(self,'olt'):
            try:
                by_representation = self.by_representation
            except AttributeError:
                by_representation = 'system'

            self._l.OLT.SHELL(by_representation=by_representation, id_value=self.olt.uuid,
            description=cmd, entity_representation=repr(self.olt))

        if flush:
            self._in.flush()
            self._out.flush() 
        buff = buff if buff else self.buff
        self.chan.send(cmd)
        
        if pause:
            time.sleep(pause)
        else:
            self.get_sleep()

        resp = self.chan.recv(buff)
        self.raw = resp
        output = resp.decode(self.decode_type)

        if hasattr(self,'_l') and hasattr(self,'olt'):
            try:
                by_representation = self.by_representation
            except AttributeError:
                by_representation = 'system'
            self._l.OLT.SHELL(by_representation=by_representation, id_value=self.olt.uuid,
            description=output, entity_representation=repr(self.olt))

        if split:
            o = output.replace("\x1b[37D", '')\
                .replace('\x1b[1DB48C09B','').split()
        else:
            o = output.replace("\x1b[37D", '')\
                .replace('\x1b[1DB48C09B','')
                
        self.log_class.info("r: %s " % o)
        self.last_command = cmd
        return o

    def cmd(self, cmd, **options):
        assert self.connected, (self.err_connection_msg)
        define_action = options.pop('define_action', None)

        if options.get('log' , True):
            #self.logger.info("cmd: %s " % cmd)
            self.log_class.info("s: %s " % cmd)
            if hasattr(self,'_l') and hasattr(self,'olt'):
                try:
                    by_representation = self.by_representation
                except AttributeError:
                    by_representation = 'system'

                if define_action:
                    met = getattr(self._l.OLT, define_action)
                    met(by_representation=by_representation, id_value=self.olt.uuid,
                    description=cmd, entity_representation=repr(self.olt))
                else:
                    self._l.OLT.SHELL(by_representation=by_representation, id_value=self.olt.uuid,
                    description=cmd, entity_representation=repr(self.olt))

        if options.get('flush', True):
            self._in.flush()
            self._out.flush() 
        buff = buff if options.get('buff', None) else self.buff
        self.chan.send(cmd)
        
        if options.get('pause', None):
            time.sleep(options.get('pause',None))
        else:
            self.get_sleep()

        resp = self.chan.recv(buff)
        self.raw = resp
        output = resp.decode(self.decode_type)
        o = output.replace("\x1b[37D", '')\
                .replace('\x1b[1DB48C09B','')

        if options.get('log' , True):
            self.log_class.info("r: %s " % o)
            if hasattr(self,'_l') and hasattr(self,'olt'):
                try:
                    by_representation = self.by_representation
                except AttributeError:
                    by_representation = 'system'

                if define_action:
                    met = getattr(self._l.OLT, define_action)
                    met(by_representation=by_representation, id_value=self.olt.uuid,
                    description=o, entity_representation=repr(self.olt))
                else:
                    self._l.OLT.SHELL(by_representation=by_representation, id_value=self.olt.uuid,
                    description=o, entity_representation=repr(self.olt))

        if options.get('split', None):
            o = o.split()

          
        self.last_command = cmd
        #print ('pre return')
        return o


    def execute(self, cmd):
        assert self.connected, (self.err_connection_msg)
        self.logger.info("execute: %s " % cmd)
        self._in.flush()
        self._out.flush()
        self.chan.send(cmd)
        

    def parse_integer(self, inputs, position, return_first=True):
        print ("inputs %s " % str(inputs))
        try:
            coincidences = re.findall('\d+', inputs[position])
            if return_first:
                return coincidences[0]
            return coincidences
        except IndexError as e:
            return 0


    def parse_join(self, inputs, start, end, separator=" "):
        return separator.join(inputs[start:end])

    def parse_lines(self, data):
        text = str(data)
        lines = text.split("\\r\\n")
        return_lines = []
        replaces = []
        a = "---- More ( Press 'Q' to break ) ----\\x1b[37D                                     \\x1b[37D"
        replaces.append(a)
        a = '<0x1b>'
        replaces.append(a)        
        for line in lines:

            for rep in replaces:
                line = line.replace(rep,'')

            return_lines.append(str(line))

        return return_lines

# class MA5608T_SSH(DeviceSSHMixin, ManagerSSH):
#     def get_temperature(self):
#         out = self.command('display temperature 0\n')
#         output = self.parse_integer(out, 8)
#         return output

#     def get_sysuptime(self):
#         out = self.command('display sysuptime\n')
#         output = self.parse_join(out, 5, 13)
#         return output

#     def get_power(self):
#         out = self.command('display power 0\n')
#         output = self.parse_integer(out, 8)
#         return output

#     def get_service_port_all(self):
#         out = self.command('display service-port all\n')
#         return out

#     def get_all_ont(self):
#         #out = self.command('display ont\n')
#         out = self.command('display ont info summary 0 \n', None, False)
#         print (out)
#         return out


#     def get_all_ont_c(self):
#         self.execute('display ont info summary 0 ')
#         self._in.write('0 \n')
#         #out = self.command('0 \n', None, False)
#         out = self._out.read()
#         print (out)

#         return out        

        
class MA5800T_SSH(DeviceSSHMixin, ManagerSSH):

    output_binary = None
    set_interface = False

    def __init__(self, *args, **kwargs):
        is_in_cron = kwargs.pop('is_in_cron', False)
        self._l = kwargs.pop('_l', LOG())
        if kwargs.get('olt',False):
            self.olt = kwargs.pop('olt')

        super().__init__(*args, **kwargs)
        self.get_logger_class(is_in_cron)

    def get_logger_class(self, is_in_cron):
        self.log_class = get_logger('MA5800T_SSH', is_in_cron)
        print('self.log_class',self.log_class)
        
    def get_model(self):
        self.command('display version\n\n', None, False)
        out = self.parse_lines(self.raw)
        return out

    def get_model_uptime_firmeware(self):
        self.command('display version \n\n', None, False, None, True)
        self.output_binary = self.raw
        self.append_to_binary(5)
        lines = self.parse_lines(self.output_binary)
        
        PRODUCT = None
        VERSION = None
        UPTIME = None

        for line in lines:    
              if 'PRODUCT' in line:
                  PRODUCT = line.split().pop()
              if 'VERSION' in line:
                  out = line.split()
                  VERSION = out[2]
              if 'Uptime' in line:
                  out = line.split()
                  UPTIME = "%sd, %sh, %sm, %ss" % (out[2],out[4],out[6],out[8])

        return PRODUCT,VERSION,UPTIME

    def get_sysuptime(self):
        self.command('display sysuptime\n\n',None, False)
        lines = self.parse_lines(self.raw)
        #print ("get_sysuptime lines",lines)
        for line in lines:
            if 'System up time' in line:
                out = line.split()
                output = "%sd, %sh, %sm, %ss" % (out[3],out[5],out[7],out[9])
                #System up time: 91 day 10 hour 11 minute 33 second
                return output
            if 'Uptime is' in line:
                out = line.split()
                #['Uptime', 'is', '91', 'day(s),', '10', 'hour(s),', '26', 'minute(s),', '23', 'second(s)']
                output = "%sd, %sh, %sm, %ss" % (out[2],out[4],out[6],out[8])
                return output
        return None

    def get_power(self):
        out = self.command('display power 0\n\n')
        output = self.parse_integer(out, 15)
        return output

    def get_cpu(self, slot=None):
        if slot:
            out = self.command('display cpu 0/%s\n\n' % str(slot))
        else:
            out = self.command('display cpu 0/8\n\n')
        if out[9] == 'Failure:':
            return 0
        try:
            if out[19] == 'executing...':
                output = self.parse_integer(out, 22).replace('%','')
            else:
                output = self.parse_integer(out, 12).replace('%','')
        except Exception as e:
            output = self.parse_integer(out, 12)

        self.log_class.info('get_cpu: return %s' % output)
        return int(output)

    def get_ram(self, slot=None):
        if slot:
            out = self.command('display mem 0/%s\n\n' % str(slot))
        else:
            out = self.command('display mem 0/8\n\n')
        print ("Out get_ram",out)

        try:
            if out[4] == 'Failure:':
                return 0
            output = self.parse_integer(out, 12).replace('%','')
        except IndexError as e:
            return 0
        except Exception as e:
            output = self.parse_integer(out, 12)   
        
        self.log_class.info('get_ram: return %s' % output)
        return int(output)

    def get_mac(self, board=8):
        global_preferences = global_preferences_registry.manager()
        time.sleep(global_preferences['times__sleep_low_medium'])
        out = self.command('display mac-address board 0/%s \n\n' % str(board))
        try:
            output = out[32].replace('-','') if out[39] == '1' else None 
            print ('get_mac',out)
            return output
        except IndexError as e:
            if 'Failure' in out[12]:
                return 'no_mac'

    def get_temperature(self, board=8):
        out = self.command('display temperature 0/%s\n\n' % str(board))
        print ("out temperature ", out)
        output = self.parse_integer(out, 15)
        return output

    def get_service_port_all(self):
        out = self.command('display service-port all\n')
        return out

    def get_firmware(self):
        out = self.command('display version \n\n')
        self._out.flush()
        output = out[11]
        return output

    def append_to_binary(self, iterations=50):
        #print (iterations)
        for __ in range(1, iterations):
            self.command(' ', None, True, 0.1)
            if self.output_binary:
                self.output_binary += self.raw
            else:
                self.output_binary = self.raw

    def get_all_ont(self):
        self.command('scroll 500\n')
        self.command('display ont info summary 0\n\n', None, False, 1)
        self.get_sleep_onus()
        
        self.output_binary = self.raw
        self.append_to_binary(100)

        # miarchivo = io.StringIO(self.output_binary)
        # print (miarchivo)
        # print (miarchivo.read())

        lines = self.parse_lines(self.output_binary)
        return lines

    def get_all_mac(self):
        self.command('scroll 500\n')
        self.command('display mac-address all\n\n', None, False)
        self.get_sleep()
        self.output_binary = self.raw
        self.append_to_binary()
        lines = self.parse_lines(self.output_binary)
        return lines

    def get_all_vlans(self):
        self.command('display vlan all\n\n', None, False)
        lines = self.parse_lines(self.raw)
        vlans = []
        flag = False

        for i, line in enumerate(lines, 0):
            if i <= 7 or flag:
                continue
            d = line.split()
            try:
                int(d[0])
                vlans.append(d)
            except Exception as e:
                pass

        return vlans

    def get_all_users(self):
        self.command('display terminal user all\n\n', None, False)
        lines = self.parse_lines(self.raw)
        users = []

        for line in lines:
            d = line.split()
            if len(d)>5:
                try:
                    int(d[3])
                    users.append(d)
                except ValueError as e:
                    pass      
   
        return users


    def get_all_speeds(self):
        self.command('display traffic table ip from-index 0\n\n', None, False)
        self.get_sleep()
        self.output_binary = self.raw
        self.append_to_binary(25)        
        lines = self.parse_lines(self.output_binary)
        speeds = []
        flag = False

        total = 0
        for i, line in enumerate(lines, 0):
            if i <= 7 or flag:
                continue
            d = line.split()
            try:
                int(d[0])
                speeds.append(d)
            except Exception as e:
                if 'Total' in line:
                    total = int(d[3])
                    
        if total != len(speeds):
            print ('total',total,len(speeds))
            return None

        return speeds


    def get_service_port(self, frame, slot, port, onu_id):
        options = {'pause' : 3, 'flush' : True, 'split':False, 'log':True}
        cmd = 'display service-port port %s/%s/%s ont %s\n\n' % (str(frame),
                                                                  str(slot),
                                                                  str(port),
                                                                  str(onu_id))
        self.cmd(cmd,**options)
        lines = self.parse_lines(self.raw)
        #print ("lines",lines)
        services = []
        for line in lines:
            print(1, line)
            data = line.split()
            try:
                index = int(data[0])
                #en la tabla siempre estan estos datos, si es el segundo dato no es entero, no es el index
                vlanId = int(data[1])    
                services.append(data)
            except Exception as e:
                if 'Failure: No service virtual port can be operated' in line:
                    return {'no_service_port':True}
                
        return services

    def undo_service_port(self, index):
        options = {'flush' : True, 'split':False, 'log':True}
        cmd = 'undo service-port %s\n\n' % (str(index))
        self.cmd(cmd,**options)               
        return True


    def get_traffic_table(self):
        self.command('display current-configuration section global-config\n\n', None, False)
        self.get_sleep()
        self.output_binary = self.raw
        self.append_to_binary(20)        
        lines = self.parse_lines(self.output_binary)
        trafic = []
        flag = False

        total = 0
        for i, line in enumerate(lines, 0):
            if i <= 12 or flag:
                continue
            
            if 'traffic table' in line:
                d = line.split()
                trafic.append([d[4],d[6]])

        return trafic

    def get_ports_vlans(self, vlan):
        self.command('display vlan %s\n\n' % str(vlan), None, False)
        self.output_binary = self.raw
        self.command(' ', None, True, 0.1)
        self.output_binary += self.raw
        self.command(' ', None, True, 0.1)
        self.output_binary += self.raw
        self.command(' ', None, True, 0.1)
        self.output_binary += self.raw
        #self.command('\n ', None, True, 0.1)
        lines = self.parse_lines(self.output_binary)
        #print ('lines',lines)
        ports = []

        for line in lines:
            
            d = line.split()
            try:
                if int(d[0]) == 0:
                    if d[1] != 'gpon':
                        ports.append(d)
            except Exception as e:
                pass
                
        return ports


    def get_all_services_numbers(self):
        self.command('scroll 500\n')
        self.command('display service-port all\n\n', None, False)
        self.get_sleep()
        self.output_binary = self.raw
        self.append_to_binary()
        lines = self.parse_lines(self.output_binary)
        return lines

    def get_all_cards(self):
        self.command('scroll 500\n')
        self.command('display board 0\n\n', None, False)
        self.get_sleep_onus()
        self.output_binary = self.raw
        self.append_to_binary(40)
        lines = self.parse_lines(self.output_binary)
        return lines

    def get_serials_all_cards(self):
        self.command('display board serial-number 0 \n\n', None, False)
        self.get_sleep()
        self.output_binary = self.raw
        self.append_to_binary(20)        
        lines = self.parse_lines(self.output_binary)
        serials = []
        for i, line in enumerate(lines, 0):
            d = line.split()
            try:
                int(d[0])
                serials.append([d[0],d[2]])
            except Exception as e:
                continue

        return serials


    def get_port_description(self, slot, port):
        self.command('display port desc 0/{}/{}\n\n'.format(slot,port), None, False)
        lines = self.parse_lines(self.raw)
        ports_desc = ""

        for i, line in enumerate(lines, 0):
            if i <= 7:
                continue
            d = line.split()
            try:
                int(d[0].replace('/',''))                

                if d[2] == '-':
                    description = " ".join(d[3:])
                    #port = d[1].split('/').pop()
                    ports_desc = description
                else:
                    description = " ".join(d[4:])
                    #port = d[2]
                    ports_desc = description
                
            except Exception as e:
                pass

        return ports_desc

    def get_all_ports(self, slot):
        self.command('display port desc 0/%d\n\n' % slot, None, False)
        lines = self.parse_lines(self.raw)
        ports = []

        for i, line in enumerate(lines, 0):
            if i <= 7:
                continue
            d = line.split()
            try:
                int(d[0].replace('/',''))                

                if d[2] == '-':
                    description = " ".join(d[3:])
                    port = d[1].split('/').pop()
                    ports.append([port,description])
                else:
                    description = " ".join(d[4:])
                    port = d[2]
                    ports.append([port,description])
                
            except Exception as e:
                pass

        return ports
        
    def get_ip_onu(self, slot, port, onu_id, **kwargs):

        if kwargs.get('pause',None):
            pause = kwargs.get('pause')
        else:
            pause = 4

        options = {'pause' : pause, 'flush' : True, 'split':False,'log':True}
        cmd = 'display ont wan-info 0/%s %s %s\n\n ' % (slot, port, onu_id)
        self.cmd(cmd,**options)
        lines = self.parse_lines(self.raw)       
        for line in lines:
            if 'IPv4 address' in line:
                data = line.split()
                return data.pop()
            if 'The ONT is not online' in line:
                return 'not_online'
                
        return None

    def get_uptime_onu(self, serial):
        cmd = 'display ont info by-sn %s | include ONT online duration\n\n' % serial
        self.command(cmd, None, False)
        lines = self.parse_lines(self.raw)

        for line in lines:
            if 'ONT online duration' in line:
                data = line.split()
                if data[4] == '-':
                    return None

                try:
                    uptime = '%sd, %sh, %sm, %ss' % (data[4],data[6], data[8], data[10])
                    return uptime
                except Exception as e:
                    self.logger.error(e)
                    return None

                return None

        return None

    def get_info_onu(self, serial):
        self.log_class.debug('get_info_onu open')
        cmd = 'display ont info by-sn %s\n\n' % serial
        options = {'flush' : True, 'split':False,'log':True}
        out = self.cmd(cmd,**options)        
        #self.command(cmd, None, False)
        #lines = self.parse_lines(self.raw)
        self.output_binary = self.raw
        self.append_to_binary(10)
        lines = self.parse_lines(self.output_binary)

        data_onu = {}

        for line in lines:
            
            if 'The required ONT does not exist' in line:
                return {'ont_does_not_exist': True}

            if 'Failure' in line:
                return {'error':line}

            if 'ONT online duration' in line:
                data = line.split()
                if data[4] != '-':
                    try:
                        uptime = '%sd, %sh, %sm, %ss' % (data[4],data[6], data[8], data[10])
                        data_onu['uptime']=uptime
                    except Exception as e:
                        self.log_class.error(e)


            if 'ONT-ID' in line:
                data = line.split()
                try:
                    data_onu['onu_id']=data[2]
                except Exception as e:
                    self.log_class.error("onu_id",e)            

            if 'F/S/P' in line:
                data = line.split()
                try:
                    data_onu['fsp']=data[2]
                except Exception as e:
                    self.log_class.error("fsp",e)

            if 'Control flag' in line:
                data = line.split()

                try:
                    data_onu['control_flag']=data[3]
                except Exception as e:
                    self.log_class.error("control_flag",e)

            if 'Run state' in line:
                data = line.split()
                try:
                    data_onu['run_state']=data[3] 
                except Exception as e:
                    self.log_class.error('run_state',e)

            if 'Config state' in line:
                data = line.split()
                try:
                    data_onu['config_state']=data[3] 
                except Exception as e:
                    self.log_class.error('config_state',e)
            
            if 'ONT distance(m)' in line:
                data = line.split()
                try:
                    data_onu['distance']= data[3]
                except Exception as e:
                    self.log_class.error('distance',e)

            if 'Memory occupation' in line:
                data = line.split()
                try:
                    data_onu['ram']=data[3]  
                except Exception as e:
                    self.log_class.error('ram',e)

            if 'CPU occupation' in line:
                data = line.split()
                try:
                    data_onu['cpu']=data[3] 
                except Exception as e:
                    self.log_class.error('cpu',e)

            if 'Temperature' in line:
                data = line.split()
                try:
                    data_onu['temperature']=data[2]                 
                except Exception as e:
                    self.log_class.error('temperature',data)

            if 'Line profile ID' in line:
                data = line.split()
                try:
                    data_onu['profile_id']=data[4] 
                except Exception as e:
                    self.log_class.error('profile_id',e)

            if 'Line profile name' in line:
                data = line.split()
                try:
                    data_onu['profile_name']=data[4]   
                except Exception as e:
                    self.log_class.error('profile_name',e)

            if 'Mapping mode' in line:
                data = line.split()
                try:
                    data_onu['mapping_mode']=data[2]
                except Exception as e:
                    self.log_class.error('mapping_mode',data)

            if 'TR069 management' in line:
                data = line.split()
                try:
                    data_onu['management']=data[2]
                except Exception as e:
                    self.log_class.error('management',e,data)

            if 'TR069 IP index' in line:
                data = line.split()
                try:
                    data_onu['ip_index']=data[3]                                 
                except Exception as e:
                    self.log_class.error('ip_index',e)

            if 'Description' in line:
                data = line.split(':')
                try:
                    data_onu['description']=data[1]                                 
                except Exception as e:
                    self.log_class.error('description',e)

        return data_onu


    def get_version_onu(self, f, s, p, id):
        global_preferences = global_preferences_registry.manager()
        cmd = 'display ont version %d %d %d %d \n\n' % (f, s, p, id)
        self.command(cmd, None, False)
        lines = self.parse_lines(self.raw)
        time.sleep(global_preferences['times__sleep_low_medium'])
        main = None
        standby = None

        for line in lines:
            if 'Main' in line:
                data = line.split()
                try:
                    main = data[4]
                except IndexError as e:
                    pass
            if 'Standby' in line:
                data = line.split()
                try:
                    standby = data[4]
                except IndexError as e:
                    pass
        return main , standby


    def get_wifi_onu(self, f, s, p, id):
        cmd = 'display ont wlan-info %d/%d/%d %d\n\n' % (f, s, p, id)
        options = {'flush' :True, 'split':False, 'log':True}
        out = self.cmd(cmd,**options)
        self.output_binary = self.raw
        self.append_to_binary(5)
        lines = self.parse_lines(self.output_binary)
        data_onu = {}
        total = 0
        index = 0

        for line in lines:
            if 'Failure' in line:
                return {'save_data':False}
            data = line.split()

            if 'The total' in line:
                total = data[6]

            if 'SSID Index' in line:
                index = int(data[3])

            if 'SSID' in line and 'Index' not in line:
                if index == 5:
                    data_onu['wifi_new_band_ssid'] = " ".join(data[2:])
                else:  
                    data_onu['wifi_old_band_ssid'] = " ".join(data[2:])

            if 'Wireless Standard' in line:
                if index == 5:
                    data_onu['wifi_new_band_mode'] = data[3] + ' ' + data[4]
                else:  
                    data_onu['wifi_old_band_mode'] = data[3] + ' ' + data[4] 


            if 'Administrative state' in line:
                if index == 5:
                    data_onu['wifi_new_band_administrative_status'] = \
                    True if data[3]=='enable' else False
                else:  
                    data_onu['wifi_old_band_administrative_status'] = \
                    True if data[3]=='enable' else False

            if 'Operational state' in line:
                if index == 5:
                    data_onu['wifi_new_band_status'] = \
                    True if data[3]=='up' else False
                else:  
                    data_onu['wifi_old_band_status'] = \
                    True if data[3]=='up' else False 


            if 'Current associate number' in line:
                if index == 5:
                    data_onu['wifi_new_band_devices'] = int(data[4])
                else:  
                    data_onu['wifi_old_band_devices'] = int(data[4])

        return data_onu

    def get_statistics_fsp(self, f, s, p):
        cmd = 'display gpon statistics ethernet %d/%d %d\n\n' % (f, s, p)
        options = {'split':False, 'log':True}
        out = self.cmd(cmd,**options)
        self.output_binary = self.raw
        self.append_to_binary(5)
        lines = self.parse_lines(self.output_binary)
        data_statistics = {}

        for line in lines:
            if 'Received unicast frames' in line:
                data = line.split()
                v = data[5].replace('%','')
                data_statistics['unicast_rec'] = v

            if 'Received multicast frames' in line:
                data = line.split()
                v = data[5].replace('%','')
                data_statistics['multicast_rec'] = v

            if 'Received broadcast frames' in line:
                data = line.split()
                v = data[5].replace('%','')
                data_statistics['broadcast_rec'] = v

            if 'Received discarded frames' in line:
                data = line.split()
                v = data[5].replace('%','')
                data_statistics['discarded_rec'] = v   

            if 'Received error frames' in line:
                data = line.split()
                v = data[5].replace('%','')
                data_statistics['error_rec'] = v   

            if 'Sent unicast frames' in line:
                data = line.split()
                v = data[5].replace('%','')
                data_statistics['unicast_send'] = v   

            if 'Sent multicast frames' in line:
                data = line.split()
                v = data[5].replace('%','')
                data_statistics['multicast_send'] = v

            if 'Sent broadcast frames' in line:
                data = line.split()
                v = data[5].replace('%','')
                data_statistics['broadcast_send'] = v

        return data_statistics

    def get_olt_autofind(self):
        cmd = 'display ont autofind all\n\n'
        options = {'flush' :True, 'split':False, 'log':True,'define_action':'FIND_AUTOFIND'}
        out = self.cmd(cmd,**options)
        self.output_binary = self.raw
        self.append_to_binary(5)
        lines = self.parse_lines(self.output_binary)
        return lines

    def set_port_description(self, slot, port, desc):
        cmd = 'port desc 0/{}/{} description "{}"\n\n'.format(slot, port, desc)
        options = {'flush' :False, 'split':True}
        out = self.cmd(cmd,**options)
        print ("OUt set_port_description",out)
        return out

    def set_alias(self, alias):
        cmd = 'sysname {}\n\n'.format(alias)
        options = {'flush' :False, 'split':True}
        out = self.cmd(cmd,**options)
        print ("OUt set_alias",out)
        return out

    def create_traffic_index(self, **kwargs):

        cmd = 'traffic table ip index {tid} name "{name}" cir {cir} cbs {cbs} pir {pir} '
        cmd += 'pbs {pbs} color-mode color-blind priority {priority} priority-policy tag-In-package\n\n'
        cmd = cmd.format(**kwargs)
        options = {'flush' :True, 'split':False, 'log':True}
        self.cmd(cmd, **options)
        self.output_binary = self.raw
        self.append_to_binary(5)
        lines = self.parse_lines(self.output_binary)
        
        for line in lines:
            if 'record successfully' in line:
                return True
            if 'Failure: The traffic table exists already' in line:
                return False

        return False

    def undo_traffic_index(self, tid):
        cmd = 'undo traffic table ip index %d\n\n' % tid
        options = {'flush' :True, 'split':False, 'log':True}
        self.cmd(cmd,**options)
        self.output_binary = self.raw
        self.append_to_binary(5)
        lines = self.parse_lines(self.output_binary)

        for line in lines:
            if 'Failure: The traffic table is used' in line:
                return False
        return True

    def verify_traffic_index(self, tid):
        cmd = 'display traffic table ip index %d\n\n' % tid
        options = {'flush' :True, 'split':False, 'log':True}
        self.cmd(cmd,**options)
        self.output_binary = self.raw
        self.append_to_binary(5)
        lines = self.parse_lines(self.output_binary)

        for line in lines:
            if 'Failure: The traffic table does not exist' in line:
                return False

        return True

    def get_wan_info(self, frame, slot, port, onu_id):
        cmd = "display ont wan-info {}/{} {} {}\n\n"
        self.command(cmd.format(str(frame), str(slot), str(port), str(onu_id)))
        lines = self.parse_lines(self.raw)       
        data_onu = {}
        for line in lines:
            if 'IPv4 address' in line:
                data = line.split()
                data_onu['ip'] = data.pop()
            if 'MAC address' in line:
                data = line.split()
                data_onu['mac'] = data.pop()
            if 'Default gateway' in line:
                data = line.split()
                data_onu['gateway'] = data.pop()
            if 'Service type' in line:
                data = line.split()
                data_onu['service_type'] = data.pop()
            if 'The ONT is not online' in line:
                return 'not_online'
            if 'wan port does not exist' in line.lower():
                return 'wan_port_not_exist'
        return data_onu

    """

    Dentro de la interfaz gpon de la OLT 
    commands onu by gpon interface

    """
    def onu_ping(self, slot, port, onu_id, ip):
        self.command('interface gpon 0/%s\n' % str(slot))
        command_ping = 'ont remote-ping {port} {onu_id} ip-address {ip}\n'.\
                       format(port=port, onu_id=onu_id, ip=ip)
        out = self.command(command_ping, pause=15)
        
        print('')
        print('')
        print('')
        print('------------------------------------------------------------------------------')
        print('------------------------------------------------------------------------------')
        print('------------------------------------------------------------------------------')
        print('')
        print('')
        print('')
        print(out)
        try:
            for o in out:
                print(o)
                if o == 'Failure:': 
                    if out[len(out)-2] == 'failed':
                        return 'failed'
                    elif out[len(out)-3] == 'not' and out[len(out)-2] == 'online':
                        return 'not_online'
            
                if o == 'error':
                    return 'error'
            
            raise IndexError

        except IndexError as e:
            self.get_sleep()
            # self.command(' \n')
            lines = self.parse_lines(self.raw)
            self.log_class.info('lines ping:')
            self.log_class.info(lines)
            return lines

    def get_lan_onu(self, slot, port, onu_id):
        self.command('interface gpon 0/%s\n' % str(slot))
        command_ = 'display ont port state {} {} eth-port all\n\n'.format(port,onu_id)
        out = self.command(command_)
        interfaces_lan = []
        lines = self.parse_lines(self.raw)

        for i, line in enumerate(lines, 0):
            #print (line)
            if i <= 4:
                continue

            d = line.split()
            try:
                int(d[0])                
                interfaces_lan.append([d[1],d[2],d[3],d[4],d[5]])

            except Exception as e:
                pass

        return interfaces_lan


    def ont_delete(self, slot, port, onu_id):
        if not self.set_interface:
            self.command('interface gpon 0/%s\n' % str(slot))
            self.set_interface = True

        command_ = 'ont delete {} {}\n\n'.format(port,onu_id)
        out = self.command(command_)
        lines = self.parse_lines(self.raw)

        for line in lines:
            if 'success: 1' in line:
                return True

        return False

    def ont_confirm(self, slot, port, serial, desc):
        if not self.set_interface:
            self.command('interface gpon 0/%s\n' % str(slot))
            self.set_interface = True

        cmd = 'ont confirm {} sn-auth {} omci ont-lineprofile-id 10 ont-srvprofile-id 10 desc "{}"\n\n'
        self.command(cmd.format(str(port), serial, desc))
        lines = self.parse_lines(self.raw)

        for line in lines:
            if 'ONTID' in line:
                d = line.split()
                return d[3].replace(':','')
        return False

    def optical_info(self, slot, port, onu_id):
        self.command('interface gpon 0/%s\n' % str(slot))
        cmd = "display ont optical-info {} {}      \n\n"
        self.command(cmd.format(str(port), onu_id))
        lines = self.parse_lines(self.raw)

        for line in lines:
            if 'Rx optical power' in line and 'CATV' not in line:
                d = line.split()
                r = d[4]
                return r
        return False

    def create_service_port(self, vlan, frame, slot, port, onu_id, gemport, vlan_2, tid_up, tid_down):
        cmd = "service-port vlan {} gpon {}/{}/{} ont {} gemport {} multi-service user-vlan {} tag-transform translate inbound traffic-table index {} outbound traffic-table index {} \n\n".format(
               vlan, frame, slot, port, onu_id, gemport, vlan_2, tid_up, tid_down)
        self.command(cmd)
        lines = self.parse_lines(self.raw)

        for line in lines:
            if 'Failure' in line:
                return {'error':line}

        self.command('save \n\n\n')

        return True


    def update_service_port(self, index, tid_up, tid_down):
        cmd = "service-port {} inbound traffic-table index {} outbound traffic-table index {} \n\n".format(
               index, tid_up, tid_down)
        self.command(cmd)
        lines = self.parse_lines(self.raw)

        for line in lines:
            if 'Failure' in line:
                return {'error':line}

        self.command('save \n\n\n')

        return True
    
    def quit_interface(self):
        self.command('quit\n\n')

        lines = self.parse_lines(self.raw)        
        for line in lines:
            if 'gpon' in line:
                self.command('quit\n\n')
                return True
        return False

