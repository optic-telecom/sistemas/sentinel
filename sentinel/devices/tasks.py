import time
import json
import subprocess as subp
from datetime import timedelta
from django.utils import timezone
from django.db.models import Q
from celery import Celery, task, shared_task
from celery.schedules import crontab
from config.celery import app
from devices.models import OLT, ONU
from devices.wrappers import OLTWrapperSSH
from dashboard.models import TaskModel
from dashboard.utils import LOG
from utp.tasks import *
from utp.models import Mikrotik

# redis_client = redis.StrictRedis(host='localhost', port=6379, db=0)

#User = get_user_model()

# def send_response(task, data):
#     redis_client.publish(task, json.dumps(data))


@task(bind=True)
def generate_csv(self):
    import time
    time.sleep(50)


@task(bind=True, default_retry_delay=120, max_retries=10)
def task_uptime_onus(self, olt):
    olt = OLT.objects.get(uuid=olt)
    from devices.management.commands.get_uptime_onus import Command as Getonus
    command = Getonus()
    command.getdata(olt)


@task(bind=True, default_retry_delay=120, max_retries=10)
def task_get_onus(self, olt):
    olt = OLT.objects.get(uuid=olt)
    from devices.management.commands.getonus import Command as Getonus
    command = Getonus()
    command.getdata(olt)


@task(bind=True, default_retry_delay=120, max_retries=10)
def task_vlans_olt(self, olt):
    olt = OLT.objects.get(uuid=olt)
    from devices.management.commands.get_vlans_olts import Command
    command = Command()
    command.getdata(olt)


@task(bind=True, default_retry_delay=120, max_retries=10)
def task_speeds_olt(self, olt):

    olt = OLT.objects.get(uuid=olt)
    from devices.management.commands.get_velocidades_olts import Command
    command = Command()
    command.getdata(olt)


@task(bind=True, default_retry_delay=120, max_retries=10)
def task_ip_onus(self, olt):
    #print ("self", self.request)
    onus = ONU.objects.filter(olt__uuid=olt, ip__isnull=True)
    from devices.management.commands.get_ip_onus import Command as Getonus
    command = Getonus()
    command.getdata_item(onus, **{'pre_status': False})


@task(bind=True, default_retry_delay=120, max_retries=10)
def task_wifi_onu(self, onu):

    from devices.management.commands.get_wifi_onus import Command as GetWifiOnus
    command = GetWifiOnus()
    command.getdata_item(onu)
    time.sleep(1)


@task(bind=True, default_retry_delay=120, max_retries=10)
def task_lan_onu(self, onu):

    from devices.management.commands.get_lan_onus import Command as GetLanOnus
    command = GetLanOnus()
    command.getdata_item(onu)
    time.sleep(0.5)


@task(bind=True, default_retry_delay=120, max_retries=10)
def task_statistics_fsp(self, port):

    from devices.management.commands.get_statistics_fsp import Command as GetstatisticsFPS
    command = GetstatisticsFPS()
    command.getdata_item(port)


@task(bind=True, default_retry_delay=12, max_retries=1)
def task_new_olt(self, olt, uptime, id_data):
    try:

        _l = LOG()
        _id_data = id_data
        obj_to_log = {'by_representation': 'system_task'}

        olt = OLT.objects.get(uuid=olt)
        _ssh = OLTWrapperSSH(olt, **{'_l': _l})
        olt_ssh = _ssh.get_me()
        try:
            olt_ssh.shell(olt, 'system_task')
            olt.set_status_online()
            _l.OLT.SHELL(locals())
        except Exception as e:
            olt.set_status_offline()
            _l.OLT.SHELL(locals())
            print("Error getdata task_new_olt", str(e))
            return False

        from devices.management.commands.getolts import Command as NewOLT
        command = NewOLT()
        command.getdata(olt, olt_ssh, **{'uptime': uptime})
        time.sleep(1)

        from devices.management.commands.get_vlans_olts import Command as VLANS
        command = VLANS()
        command.getdata(olt, olt_ssh)
        time.sleep(2)

        from devices.management.commands.getonus import Command as Getonus
        command = Getonus()
        command.getdata(olt, olt_ssh)
        time.sleep(1)

        ### interfaces and ports
        from devices.management.commands.get_fsp_olts import Command as GetFSP
        cmd = GetFSP()
        cmd.getdata(olt, olt_ssh, id_data)
        ## end interfaces

        from devices.management.commands.get_info_onus import Command as GetInfoOnus
        command = GetInfoOnus()
        command.getdata(olt, olt_ssh, id_data)
        time.sleep(1)

        from devices.management.commands.get_wifi_onus import Command as GetWifiOnus
        command = GetWifiOnus()
        command.getdata(olt, olt_ssh, id_data)
        time.sleep(1)

        # print('')
        # print('')
        # print('')
        # print('===========================================')
        # print('Aqui empieza las ips')
        # print('===========================================')
        # print('')
        # print('')
        # print('')
        onus = ONU.objects.filter(olt=olt, ip__isnull=True)
        from devices.management.commands.get_ip_onus import Command as GetIpsOnus
        command = GetIpsOnus()
        command.getdata(onus, olt_ssh, **{'pre_status': False})
        time.sleep(0.85)
        # print('')
        # print('')
        # print('')
        # print('===========================================')
        # print('Aqui Termina las ips')
        # print('===========================================')
        # print('')
        # print('')
        # print('')

        ### salud olt
        from devices.management.commands.get_health_olts import Command as HealthOLTs
        cmd = HealthOLTs()
        cmd.getdata(olt, olt_ssh)
        ## end

        del olt_ssh

        try:
            task_model = TaskModel.objects.get(id_obj=olt.id,
                                               model_obj='olt',
                                               name='task_new_olt',
                                               status_field=True,
                                               status='PENDING')
            task_model.date_done = timezone.now()
            task_model.result = True
            task_model.status = 'SUCCESS'
            task_model.save()
        except TaskModel.DoesNotExist:
            task_model = None

        try:
            task_model = TaskModel.objects.get(id_obj=olt.id,
                                               model_obj='olt',
                                               name='task_update_olt',
                                               status_field=True,
                                               status='PENDING')
            task_model.date_done = timezone.now()
            task_model.result = True
            task_model.status = 'SUCCESS'
            task_model.save()
        except TaskModel.DoesNotExist:
            task_model = None

    except Exception as e:
        raise self.retry(exc=e)


### TAREAS DE CELERY-BEAT ###

@task(bind=True, default_retry_delay=12, max_retries=1)
def clear_logs(self):
    from devices.management.commands.clear_logs import Command as clearlogs
    cmd = clearlogs()
    cmd.handle()


@task(bind=True, default_retry_delay=12, max_retries=1)
def task_all_info_olt(self):

    from devices.management.commands.get_all_info import Command as all_info
    command = all_info()
    command.handle()


@task(bind=True, default_retry_delay=12, max_retries=1)
def task_health_olts(self):

    from devices.management.commands.get_health_olts import Command as health
    command = health()
    command.handle()


@task(bind=True, default_retry_delay=12, max_retries=1)
def task_uptime_olts(self):

    from devices.management.commands.get_uptime_olts import Command as uptime
    command = uptime()
    command.handle()


@task(bind=True, default_retry_delay=120, max_retries=10)
def task_vlans_olts(self):
    from devices.management.commands.get_vlans_olts import Command as vlans
    command = vlans()
    command.handle()


@task(bind=True, default_retry_delay=120, max_retries=10)
def task_velocidades_olts(self):
    from devices.management.commands.get_velocidades_olts import Command as velocidades
    command = velocidades()
    command.handle()


@task(bind=True, default_retry_delay=120, max_retries=10)
def task_ips_onus(self):
    from devices.management.commands.get_ip_onus import Command as ips
    command = ips()
    command.handle()


@task(bind=True, default_retry_delay=120, max_retries=10)
def task_estado_onus(self):
    from devices.management.commands.get_uptime_onus import Command as uptime
    command = uptime()
    command.handle()


@task(bind=True, default_retry_delay=120, max_retries=10)
def task_all_info_onus(self):
    from devices.management.commands.get_info_onus import Command as info
    command = info()
    command.handle()


@task(bind=True, default_retry_delay=120, max_retries=10)
def ssh_release(self):
    from devices.management.commands.ssh_release import Command as ssh_release
    command = ssh_release()
    command.handle()


@task(bind=True, default_retry_delay=120, max_retries=10)
def get_services_network_status(self, device_id=None, device_type=None):

    from devices.management.commands.refresh_services_network_status import Command

    if device_id is None and device_type is None:
        command = Command()
        command.handle()
    else:
        if device_type == "OLT":
            device = OLT.objects.get(uuid=device_id)
        elif device_type == 'UTP':
            device = Mikrotik.objects.get(uuid=device_id)
        command = Command()
        command.getdata(device, device_type)


@task(bind=True, default_retry_delay=120, max_retries=10)
def assoc_service_to_mikrotik(self):

    from devices.management.commands.assoc_service_to_mikrotik import Command
    command = Command()
    command.handle()


@task(bind=True, default_retry_delay=120, max_retries=10)
def service_primary_equipments(self):

    from devices.management.commands.service_primary_equipments import Command
    command = Command()
    command.handle()
