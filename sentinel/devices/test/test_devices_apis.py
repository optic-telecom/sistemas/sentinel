import pytest


@pytest.mark.django_db
class TestDevicesApis():
    def test_create_operator(self, url, client, headers, fail):
        pass_payload = {
                "id": 2,
                "name": 'test',
                "code": 'ts'
            }

        fail_payload = {
            "id": 'algo',
            "name": 'test',
            "code": 'ts'
        }

        response = client.post('{}/operator/'.format(url),
                               data=pass_payload if not fail else fail_payload,
                               headers=headers, verify=False)
        print(response.json())
        assert response.status_code == 201

    def test_create_onu(self, url, client, headers, fail,
                        test_service, test_model, test_MACTable,
                        test_IPTable):
        fail_payload = {
                    'id': '1',
                    'serial': '8080',
                    'description': 'test',
                    'mac': test_MACTable,
                    'ip': '1.1.1.1',
                    'password': 'test',
                    'ssid_5g': 'test',
                    'onu_id': 1,
                    'frame': 1,
                    'slot': 1,
                    'port': 1
                }

        pass_payload = {
                    'id': '1',
                    'alias': 'test',
                    'serial': 'test',
                    'description': 'test',
                    'model': test_model.id,
                    'service': test_service.id,
                    'mac': test_MACTable.id,
                    'ip': test_IPTable,
                    'primary': True,
                    'ssid': 'test',
                    'password': 'test',
                    'ssid_5g': 'test',
                    'onu_id': 1,
                    'frame': 1,
                    'slot': 1,
                    'port': 1
                }

        response = client.post('{}/onu/'.format(url),
                               data=pass_payload if not fail else fail_payload,
                               headers=headers, verify=False)
        print(response.json())
        assert response.status_code == 201
