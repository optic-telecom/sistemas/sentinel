from rest_framework import routers
from .views import *
from .alt_apis import (ONUSAltern, UserAltern, LogDevicesAltern, VLANAltern,
                       SpeedAltern, InterfacesAltern, OnuUTP)

from .views.smokeping import (SmokepingImgROVS, IPTableModelViewSet,
                              IPTableReadOnlyViewSet, UploadCommandView,
                              DownloadTargetFile)

from .models import IPTable
from tv.autocompletes import AutocompleteSelect2
from django.urls import re_path, path
from.views.__init__ import UpdateNetworkStatusAPI

router = routers.DefaultRouter()

router.register(r'onus-alt/(?P<uuid>[-\w]+)', ONUSAltern, 'onus_alt_api')
router.register(r'users-alt/(?P<uuid>[-\w]+)', UserAltern, 'user_alt_api')
router.register(r'logs-devices-alt/(?P<uuid>[-\w]+)', LogDevicesAltern,
                'log_devices_alt_api')
router.register(r'vlans-alt/(?P<uuid>[-\w]+)', VLANAltern, 'vlan_alt_api')
#ports faltante
router.register(r'speeds-alt/(?P<uuid>[-\w]+)', SpeedAltern, 'spped_alt_api')
router.register(r'interfaces-alt/(?P<uuid>[-\w]+)', InterfacesAltern,
                'interfaces_alt_api')

## FILTROS PRO NRO SERVICIOS
router.register(r'filtered-serv-num/(?P<service_number>[-\w]+)', OnuUTP,
                'filtered_serv_num')

router.register(r'olt', OLTViewSet, 'olt_api')
router.register(r'onu', ONUViewSet, 'onu_api')
router.register(r'operator', OperatorViewSet, 'operator_api')
router.register(r'type_vlan', TypeVLANViewSet, 'type_vlan_api')
router.register(r'ssh_olt', OLTConnectionViewSet, 'ssh_olt_api')

#proxies matrix
router.register(r'matrix_plan', MatrixPlanViewSet, 'matrix_plan_api')
router.register(r'matrix_technician', MatrixTechnicianViewSet,
                'matrix_technician_api')
router.register(r'matrix_service_number', MatrixServiceNumberViewSet,
                'matrix_service_number_api')

# update and display.
router.register(r'update_remote/olt', UpdateRemoteOLTViewSet,
                'update_remote_olt_api')
router.register(r'update_remote/olt_uptime', UpdateUptimeOLTSViewSet,
                'update_uptime_olts_api')
router.register(r'update_remote/wifi', UpdateWifi, 'update_remote_wifi_api')
router.register(r'update_remote/lan', UpdateLan, 'update_remote_lan_api')
router.register(r'update_remote/statistics_fsp', UpdateStatisticsFsp,
                'update_remote_statistics_fsp_api')

# router.register(r'plan', PlanViewSet, 'plan_api')

# logs
router.register(r'log/olt', LogOltViewSet, 'log_olt_api')
router.register(r'log/onu', LogOnuViewSet, 'log_onu_api')

router.register(r'abnormalities/onu', AbnormalitieOnuViewSet,
                'anormalities_onu_api')
router.register(r'configuration/onu', ConfigurationOnuViewSet,
                'configuration_onu_api')

# interfaces
router.register(r'interface/olt', InterfaceOltViewSet, 'interface_olt_api')
router.register(r'interface/onu', InterfaceOnuViewSet, 'interface_onu_api')

# commands ssh for onus
# onus in system
router.register(r'ssh/onu/ping', SSHOnuPingViewSet, 'ssh_onu_ping_api')
router.register(r'ssh/onu/traceroute', SSHOnuTracerouteViewSet,
                'ssh_onu_traceroute_api')
router.register(r'ssh/onu/signal', SSHOnuSignalViewSet, 'ssh_onu_signal_api')
# onus for serial
router.register(r'ssh/command_onu/signal', SSHCommandOnuSignalViewSet,
                'ssh_command_onu_signal_api')
router.register(r'ssh/command_onu/ping', SSHCommandOnuPingViewSet,
                'ssh_command_onu_ping_api')

router.register(r'vlan/system', SystemVLANViewSet, 'system_vlan_api')
router.register(r'vlan/olt', OLTVLANViewSet, 'olt_vlan_api')

router.register(r'speed/system', SystemSpeedsViewSet, 'system_speeds_api')
router.register(r'speed/olt', OLTSpeedsViewSet, 'olt_speeds_api')

#router.register(r'provisioning/olt', InterfaceOltViewSet, 'interface_olt_api')
router.register(r'provisioning/onu', ProvisioningViewSet,
                'provisioning_onu_api')
router.register(r'provisioning/perform/onu', ProvisioningPerformViewSet,
                'provisioning_perform_onu_api')

#F/S/P
router.register(r'FSP', FSPViewSet, 'fsp_api')
router.register(r'fsp_for_olt', FSPforOLTViewSet, 'fsp_for_olt_api')
router.register(r'forms/FSP', FormFSPViewSet, 'form_fsp_api')

router.register(r'model_for_olt', ModelForOLTViewSet, 'model_for_olt_api')
router.register(r'model_device', ModelDeviceViewSet, 'model_device_api')

# =================================================================== #
# =================================================================== #

#VISTAS POSTGRES PARA LOS GRÁFICOS
router.register(r'onu-pg-view', OnuPGViewsets, 'onu_pg_api')
# VISTA PARA LAS IPTABLES
router.register(r'iptables', IPTableModelViewSet, 'iptable')
router.register('iptables-read', IPTableReadOnlyViewSet, 'iptable-read')
# VISTA PARA LOS MENÚS SMOKEPING
router.register(r'smokeping-menus', MenuSmokepingModelViewSet,
                'menu-smokeping')
# VIISTA PARA TRAER IMG DESDE EL SERVIDOR DE SMOKEPING
router.register(r'smokeping-server', SmokepingImgROVS, 'smokeping-src')

# VISTA PARA PUBLICAR ARCHIVO TARGET EN EL SERVER
router.register(r'publisher-target-file', UploadCommandView, 'target-remote')

urlpatterns = [
    # path('iptable-ac/',IPTableAutocomplete.as_view(),name="iptable-ac"),
    path('iptable-ac/',
         AutocompleteSelect2.as_view(model=IPTable,
                                     filter_by='ip__istartswith'),
         name="iptable-ac"),
    re_path(r'^change_plan/(?P<service_number>[0-9]+)/',
            ChangePlanView.as_view()),
    # VISTA PARA PUBLICAR ARCHIVO TARGET EN EL SERVER
    path(r'download-target-file/', DownloadTargetFile, name='target-local'),
    path('refresh_network_status/', UpdateNetworkStatusAPI.as_view(), name="refresh_network_status")
]
