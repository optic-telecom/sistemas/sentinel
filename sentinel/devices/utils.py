from .models import OLT, ONU
from devices.wrappers import ssh_all_data_olt, ssh_onu_command
from devices.wrappers import *
from devices.tasks import task_uptime_onus, task_get_onus, task_ip_onus
# from dashboard.utils import LOG

import re

# logging.basicConfig(level=logging.DEBUG)


def update_remote_info_olt(obj, fields, mode=None):
    updated = ssh_all_data_olt(obj)
    if updated:
    	return True
    return False


def update_remote_info_onu(obj, fields, mode=None):
    return fields


def update_remote_info(instance, fields, mode=None):
    
    if isinstance(instance, OLT):
        return update_remote_info_olt(instance, fields, mode)

    if isinstance(instance, ONU):
        return update_remote_info_onu(instance, fields, mode)


def ping_onu(onu, ip, request=None, olt_ssh=None ):
    error, message = ssh_onu_command('ping', onu=onu, ip=ip, request=request, olt_ssh=olt_ssh)

    if error:
        return error, message

    
    message = message.split('<br>')
    ping = {
         'enviados': int(message[0].split().pop()),  
         'recibidos': int(message[1].split().pop()),
         'perdidos': int(message[2].split().pop()),
         'ratio': int(message[3].split().pop()),
         'min': int(message[4].split().pop()),
         'max': int(message[5].split().pop()),
         'avg': int(message[6].split().pop())
     }
    print(ping)

    return False, ping



def tracerouter_onu(onu, ip, request=None):
    return ssh_onu_command('tracerouter', onu=onu, ip=ip, request=request)






def is_valid_hostname(hostname):
    if len(hostname) > 255:
        return False
    if hostname[-1] == ".":
        hostname = hostname[:-1] # strip exactly one dot from the right, if present
    allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)
    return all(allowed.match(x) for x in hostname.split("."))

def check_ip(ip):
    regex = '''^(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.( 
                25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.( 
                25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.( 
                25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)'''

    if(re.search(regex, ip)):  
        return ip
