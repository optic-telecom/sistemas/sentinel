import requests
from django.http import HttpResponse, JsonResponse
from django_filters.rest_framework import DjangoFilterBackend
from django.db.models import Q
from dynamic_preferences.registries import global_preferences_registry

from rest_framework import mixins, status, viewsets
from rest_framework.generics import ListAPIView, get_object_or_404
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.generics import RetrieveAPIView
from rest_framework.decorators import action
from rest_framework.views import APIView
from rest_framework.renderers import JSONRenderer

from multifiberpy.permissions import IsSystemInternalPermission

from common.mixins import MixinListRetrive
from common.pagination import LargeResultsSetPagination
from dashboard.simulate import Simulate
from dashboard.utils import LOG
from devices.models import (OLT, OLTVLAN, ONU, AbnormalitieDevice, Ethernet,
                     InterfaceOLT, LogDevice, ModelDevice, OLTConnection,
                     OLTSpeeds, ONUAutoFind, Operator,
                     PortOLT, StatisticsPortOLT, SystemSpeeds, SystemVLAN,
                     TypeVLAN, WirelessONUS,)
from customers.models import TypePLAN, Service, Plan
from devices.postgres_views import ONUViewModel
from devices.serializers import *
from devices.tasks import task_lan_onu, task_statistics_fsp, task_wifi_onu
from devices.wrappers import delete_speed, olt_autofind, provisioning_onu
from utp.models import Mikrotik
from .smokeping import * 
from .provisioning import * 

__all__ = ['OLTViewSet','ModelDeviceViewSet','ONUViewSet','UpdateRemoteOLTViewSet',
           'LogOnuViewSet','LogOltViewSet','OLTVLANViewSet','AbnormalitieOnuViewSet',
           'InterfaceOnuViewSet','InterfaceOltViewSet','SSHOnuPingViewSet','UpdateLan',
           'SSHOnuTracerouteViewSet','MatrixServiceNumberViewSet','UpdateStatisticsFsp',
           'FormFSPViewSet','SystemVLANViewSet','ConfigurationOnuViewSet','UpdateWifi',
           'OperatorViewSet','ModelForOLTViewSet','SystemSpeedsViewSet','TypeVLANViewSet',
           'FSPforOLTViewSet','FSPViewSet','ProvisioningViewSet',
           'OLTSpeedsViewSet','MatrixPlanViewSet','UpdateUptimeOLTSViewSet',
           'ProvisioningPerformViewSet','OLTConnectionViewSet','SSHOnuSignalViewSet',
           'SSHCommandOnuSignalViewSet','MatrixTechnicianViewSet','SSHCommandOnuPingViewSet',
           'OnuPGViewsets','MenuSmokepingModelViewSet','IPTableModelViewSet',
           'ChangePlanView']

#User = get_user_model()

# preferences = {
#     'url_endpoint_erp':'url_endpoint_erp'
# }


_l=LOG()



def build_response(res, status):
    """
        contruye un Response object con el objecto a enviar y el status
    """

    response =  Response(
                res,
                content_type="application/json",
                status=status,
            )
    response.accepted_renderer = JSONRenderer()
    response.accepted_media_type = "application/json"
        
    return response


class OLTViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for OLT
    retrieve:
        End point for OLT
    update:
        End point for OLT
    delete:
        End point for OLT
    """
    queryset = OLT.objects.all()
    
    # values('mac', 'mac_secondary', 'alias', 'status', 'model', 'description', 
    #                                 'firmware', 'onus', 'cards', 'onus_active', 'cards_active', 'ram', 
    #                                 'ram_secondary', 'cpu', 'cpu_secondary', 'temperature', 'ip__ip',
    #                                 'temperature_secondary', 'uptime', 'watts', 'time_uptime', 'site')
    serializer_class = OLTSerializer
    lookup_field = 'uuid'
    lookup_url_kwarg = 'uuid'
    filter_fields = '__all__'

class ModelDeviceViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for ModelDevice
    retrieve:
        End point for ModelDevice
    update:
        End point for ModelDevice
    delete:
        End point for ModelDevice
    list:
        Get all the models, they can be filtered by 'for_olt'
        default for None     
    """
    queryset = ModelDevice.objects.all()
    serializer_class = ModelDeviceSerializer

    # filter_backends = (DjangoFilterBackend,)
    # filter_fields = ('kind',)

    def get_queryset(self):
        qs = super().get_queryset()
        kind = self.request.GET.get('kind', None)
        for_iris = self.request.GET.get('for_iris', None)
        if kind:
            filtro = dict(kind__exact=int(kind))
            qs = qs.filter(**filtro)
        if for_iris:
            if for_iris.lower() == 'all':
                qs = qs.filter(for_iris=True)
            else:
                qs = qs.filter(for_iris=True, model=for_iris)
        return qs


class ONUViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for ONU
    retrieve:
        End point for ONU
    update:
        End point for ONU
    delete:
        End point for ONU
    """
    queryset = ONU.objects.all()
    serializer_class = ONUSerializer
    lookup_field = 'uuid'
    lookup_url_kwarg = 'uuid'


class UpdateRemoteOLTViewSet(mixins.UpdateModelMixin,
							 viewsets.GenericViewSet):
    """
    partial_update:
        Requests the device to update its information in the system.
    """
    queryset = OLT.objects.all()
    serializer_class = UpdateRemoteOLTSerializer
    lookup_field = 'uuid'
    lookup_url_kwarg = 'uuid'


class UpdateUptimeOLTSViewSet(mixins.CreateModelMixin,
                              viewsets.GenericViewSet):
    """
    create:
        Requests the device to update
    """
    queryset = OLT.objects.all()
    serializer_class = UpdateUptimeOLTSSerializer


class LogOnuViewSet(MixinListRetrive,
                    viewsets.GenericViewSet):
    
    queryset = LogDevice.objects.all()
    serializer_class = LogDeviceSerializer
    lookup_field = 'onu__uuid'
    lookup_url_kwarg = 'uuid'


class LogOltViewSet(MixinListRetrive,
                    viewsets.GenericViewSet):
    
    queryset = LogDevice.objects.all()
    serializer_class = LogDeviceSerializer
    lookup_field = 'olt__uuid'
    lookup_url_kwarg = 'uuid'


class ConfigurationOnuViewSet(mixins.RetrieveModelMixin,
                              mixins.UpdateModelMixin,
                              mixins.CreateModelMixin,
                              viewsets.GenericViewSet):

    queryset = WirelessONUS.objects.all()
    serializer_class = ConfigurationOnuSerializer
    lookup_field = 'onu__uuid'
    lookup_url_kwarg = 'uuid'

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        assert lookup_url_kwarg in self.kwargs, (
            'Expected view %s to be called with a URL keyword argument '
            'named "%s". Fix your URL conf, or set the `.lookup_field` '
            'attribute on the view correctly.' %
            (self.__class__.__name__, lookup_url_kwarg)
        )
        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        obj = queryset.filter(**filter_kwargs).last()
        self.check_object_permissions(self.request, obj)
        return obj


class AbnormalitieOnuViewSet(MixinListRetrive,
                             viewsets.GenericViewSet):

    queryset = AbnormalitieDevice.objects.all()
    serializer_class = AbnormalitieDeviceSerializer
    lookup_field = 'onu__uuid'
    lookup_url_kwarg = 'uuid'


class InterfaceOnuViewSet(viewsets.GenericViewSet):

    """
    Esta api solo trae los ultimos registros de 
    ethernet filtrados para una onu
    """
    queryset = Ethernet.objects.all()
    serializer_class = EthernetSerializer
    lookup_field = 'onu__uuid'
    lookup_url_kwarg = 'uuid'

    def get_queryset(self):
        try:
            id_d = Ethernet.objects.filter(status_field=True,
                                           onu__uuid=self.kwargs[self.lookup_url_kwarg])\
                                           .latest('created').id_data
            qs = Ethernet.objects.filter(status_field=True,
                                         onu__uuid=self.kwargs[self.lookup_url_kwarg],
                                         id_data=id_d)
        except Ethernet.DoesNotExist:
            qs = Ethernet.objects.none()
        return qs

    def retrieve(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class InterfaceOltViewSet(viewsets.GenericViewSet):

    queryset = InterfaceOLT.objects.all()
    serializer_class = InterfaceOLTSerializer
    lookup_field = 'olt__uuid'
    lookup_url_kwarg = 'uuid'

    def get_queryset(self):
        try:
            id_d = InterfaceOLT.objects.filter(status_field=True,
                                               olt__uuid=self.kwargs[self.lookup_url_kwarg])\
                                               .latest('created').id_data
            qs = InterfaceOLT.objects.filter(status_field=True,
                                             olt__uuid=self.kwargs[self.lookup_url_kwarg],
                                             id_data=id_d).exclude(interface='')
        except InterfaceOLT.DoesNotExist:
            qs = InterfaceOLT.objects.none()
        return qs

    def retrieve(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class SSHOnuPingViewSet(mixins.UpdateModelMixin,
                        viewsets.GenericViewSet):

    queryset = ONU.objects.all()
    serializer_class = SSHOnuPingSerializer
    lookup_url_kwarg = 'uuid'
    lookup_field = 'uuid'


class SSHOnuTracerouteViewSet(SSHOnuPingViewSet):
    pass

class SSHCommandOnuPingViewSet(mixins.CreateModelMixin,
                               viewsets.GenericViewSet):

    queryset = ONU.objects.all()
    serializer_class = SSHOnuPingSerializer

class SSHOnuSignalViewSet(mixins.UpdateModelMixin,
                          viewsets.GenericViewSet):

    queryset = ONU.objects.all()
    serializer_class = SSHOnuSignalSerializer
    lookup_url_kwarg = 'uuid'
    lookup_field = 'uuid'


class SSHCommandOnuSignalViewSet(mixins.CreateModelMixin,
                                 viewsets.GenericViewSet):

    queryset = ONU.objects.all()
    serializer_class = SSHOnuSignalSerializer


class FSPforOLTViewSet(viewsets.GenericViewSet):
    
    queryset = PortOLT.objects.all()
    serializer_class = OLTPortSerializer
    lookup_url_kwarg = 'uuid'

    def get_queryset(self):
        try:
            id_d = PortOLT.objects.filter(status_field=True,
                                          interface__olt__uuid=self.kwargs[self.lookup_url_kwarg])\
                                         .latest('created').id_data
            qs = PortOLT.objects.filter(status_field=True,
                                        interface__olt__uuid=self.kwargs[self.lookup_url_kwarg],
                                        id_data=id_d)
        except PortOLT.DoesNotExist:
            qs = PortOLT.objects.none()

        return qs


    def retrieve(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class FormFSPViewSet(viewsets.GenericViewSet):
    serializer_class = ONUSerializer 

    def get_queryset(self):
        return ONU.objects.all()

    def list(self, request, *args, **kwargs):
        from django.db.models import Max
        max_port = ONU.objects.filter(status_field=True).aggregate(Max('port'))
        max_port = int(max_port['port__max']) + 1
        max_slot = ONU.objects.filter(status_field=True).aggregate(Max('slot'))
        max_slot = int(max_slot['slot__max']) + 1
        all_response= []

        for slot in range(1,max_slot):
            for port in range(0, max_port):
                all_response.append("0/%d/%d" % (slot, port))

        return Response(all_response)


class FSPViewSet(viewsets.ModelViewSet):
    """
    create:
        
    retrieve:
        End point for
    update:
        End point for
    delete:
        End point for
    """
    queryset = PortOLT.objects.all()
    serializer_class = PortOLTSerializer        


class SystemVLANViewSet(viewsets.ModelViewSet):
    """
    create:
        Crea una nueva vlan en el sistema
    retrieve:
        End point for
    update:
        End point for
    delete:
        End point for
    """
    queryset = SystemVLAN.objects.all()
    serializer_class = SystemVLANSerializer


class OLTVLANViewSet(viewsets.ModelViewSet):
    """
    create:
        OLT VLAN
    retrieve:
        End point for
    update:
        End point for
    delete:
        End point for
    """
    queryset = OLTVLAN.objects.all()
    serializer_class = OLTVLANSerializer


class OLTSpeedsViewSet(viewsets.ModelViewSet):
    """
    create:
        OLT Speeds
    retrieve:
        End point for
    update:
        End point for
    delete:
        End point for
    """
    queryset = OLTSpeeds.objects.all()
    serializer_class = OLTSpeedsSerializer

    def create(self, request, *args, **kwargs):
        uuid_olt = request.data.get('olt', None)
        request.data['olt'] = OLT.objects.get(uuid=uuid_olt).pk
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()

        e, created = delete_speed(
                        instance.olt,
                        instance.tid,
                        request
                     )

        if created:
            _l.OLTSpeeds.delete(by_representation=request.user.username, id_value=self.instance.pk,
                                description='Borrada por api', entity_representation=repr(instance))              
            self.perform_destroy(instance)          
            return Response(status=status.HTTP_204_NO_CONTENT)
        if e:
            return Response({'error':e}, status=500)
        else:
            return Response({'error':'No se pudo eliminar la velocidad de la OLT'}, status=500)


class OperatorViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for
    retrieve:
        End point for
    update:
        End point for
    delete:
        End point for
    """
    queryset = Operator.objects.all()
    serializer_class = OperatorSerializer

    def get_queryset(self):
        qs = super().get_queryset()
        name = self.request.GET.get('q',None)
        if name:
            qs = qs.filter(Q(name__istartswith=name)|Q(code__istartswith=name))
        return qs

    # @action(detail=True, methods=['post'])
    # def add_history(self, request, version=None, pk=None):
    #     from customers.views_helper import error_response

    #     try:
    #         operator = Operator.objects.get(pk = pk)
    #     except ValueError:
    #         return error_response(f"El operador con id = {uuid} no fue encontrado")
        
    #     print('*********\n', request.data, '\n********')
    #     data = request.data
    #     try:
    #         Operator.history.create(
    #             id= data['id'],
    #             created= data['created'],
    #             name= data['name'],
    #             code= data['code'],
    #             history_date= data['history_date'],
    #             history_type= data['history_type'],
    #             history_id= data['history_id']

    #         )
    #     except Exception as e :
    #         print(e)
    #         return error_response(e)

    #     return Response(operator, status=status.HTTP_200_OK)



class TypeVLANViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for
    retrieve:
        End point for
    update:
        End point for
    delete:
        End point for
    """
    queryset = TypeVLAN.objects.all()
    serializer_class = TypeVLANSerializer    


# class TypePLANViewSet(viewsets.ModelViewSet):
#     """
#     create:
#         End point for
#     retrieve:
#         End point for
#     update:
#         End point for
#     delete:
#         End point for
#     """
#     queryset = TypePLAN.objects.all()
#     serializer_class = TypePLANSerializer   


class SystemSpeedsViewSet(viewsets.ModelViewSet):
    """
    create:
        Speed system
    retrieve:
        End point for
    update:
        End point for
    delete:
        End point for
    """
    queryset = SystemSpeeds.objects.all()
    serializer_class = SystemSpeedsSerializer


# class PlanViewSet(viewsets.ModelViewSet):
#     """
#     create:
#         End point for Plan
#     retrieve:
#         End point for Plan
#     update:
#         End point for Plan
#     delete:
#         End point for Plan
#     """
#     queryset = Plan.objects.all()
#     serializer_class = PlanSerializer


class UpdateWifi(mixins.RetrieveModelMixin,
                 viewsets.GenericViewSet):

    queryset = WirelessONUS.objects.all()
    serializer_class = WifiSerializer
    lookup_field = 'onu__uuid'
    lookup_url_kwarg = 'uuid'

    def retrieve(self, request, *args, **kwargs):
        obj = self.get_object()
        if obj.onu.status and obj.onu.olt.status:
            serializer = self.get_serializer(obj)
            return Response(serializer.data)
        return Response({"error" : "La onu esta offline"})

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        assert lookup_url_kwarg in self.kwargs, (
            'Expected view %s to be called with a URL keyword argument '
            'named "%s". Fix your URL conf, or set the `.lookup_field` '
            'attribute on the view correctly.' %
            (self.__class__.__name__, lookup_url_kwarg)
        )

        task_wifi_onu(ONU.objects.filter(uuid=self.kwargs['uuid']))        

        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        obj = queryset.filter(**filter_kwargs).last()
        self.check_object_permissions(self.request, obj)
        return obj


class UpdateLan(mixins.RetrieveModelMixin,
                viewsets.GenericViewSet):

    queryset = Ethernet.objects.all()
    serializer_class = EthernetSerializer
    lookup_field = 'onu__uuid'
    lookup_url_kwarg = 'uuid'

    def retrieve(self, request, *args, **kwargs):
        qs = self.get_queryset()
        if qs.count() > 0 and qs[0].status:
            serializer = self.get_serializer(qs, many=True)
            return Response(serializer.data)
        return Response({"error" : "La onu esta offline"})

    def get_queryset(self):
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        assert lookup_url_kwarg in self.kwargs, (
            'Expected view %s to be called with a URL keyword argument '
            'named "%s". Fix your URL conf, or set the `.lookup_field` '
            'attribute on the view correctly.' %
            (self.__class__.__name__, lookup_url_kwarg)
        )
        onu = ONU.objects.filter(uuid=self.kwargs['uuid'])
        
        task_lan_onu(onu)
        try:
            id_d = Ethernet.objects.filter(status_field=True,
                                           onu=onu[0])\
                                           .latest('created').id_data
            qs = Ethernet.objects.filter(status_field=True,
                                         onu=onu[0],
                                         id_data=id_d)
        except Ethernet.DoesNotExist:
            qs = Ethernet.objects.none()

        return qs


class UpdateStatisticsFsp(mixins.RetrieveModelMixin,
                          viewsets.GenericViewSet):

    queryset = StatisticsPortOLT.objects.all()
    serializer_class = StatisticsPortOLTSerializer
    lookup_field = 'port__pk'
    lookup_url_kwarg = 'pk'

    def retrieve(self, request, *args, **kwargs):
        obj = self.get_object()
        if obj and obj.port.interface.olt.status:
            serializer = self.get_serializer(obj)
            return Response(serializer.data)
        return Response({"error" : "La OLT esta offline"})

    def get_object(self):
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        assert lookup_url_kwarg in self.kwargs, (
            'Expected view %s to be called with a URL keyword argument '
            'named "%s". Fix your URL conf, or set the `.lookup_field` '
            'attribute on the view correctly.' %
            (self.__class__.__name__, lookup_url_kwarg)
        )
        port = PortOLT.objects.filter(status_field=True, pk=self.kwargs['pk'])
        
        task_statistics_fsp(port)
        try:
            id_d = StatisticsPortOLT.objects.filter(status_field=True,
                                                    port=port[0])\
                                                    .latest('created').id_data
            qs = StatisticsPortOLT.objects.get(status_field=True,
                                               port=port[0],
                                               id_data=id_d)
        except StatisticsPortOLT.DoesNotExist:
            qs = StatisticsPortOLT.objects.none()

        return qs


class ModelForOLTViewSet(mixins.RetrieveModelMixin,
                         viewsets.GenericViewSet):
    
    queryset = ModelDevice.objects.all()
    serializer_class = ModelDeviceSerializer
    lookup_field = 'uuid'

    def retrieve(self, request, *args, **kwargs):
        qs = self.get_queryset()
        serializer = self.get_serializer(qs, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        assert lookup_url_kwarg in self.kwargs, (
            'Expected view %s to be called with a URL keyword argument '
            'named "%s". Fix your URL conf, or set the `.lookup_field` '
            'attribute on the view correctly.' %
            (self.__class__.__name__, lookup_url_kwarg)
        )
        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        olt = get_object_or_404(OLT, **filter_kwargs)
        pks = []

        models = ModelDevice.objects.filter(kind = 1)

        for model in models:
            onus = ONU.objects.filter(model=model, olt=olt)
            if onus.count() > 0:
                pks.append(model.pk)
                continue

            onus_h = ONU.history.filter(model=model, olt=olt)
            if onus_h.count() > 0:
                pks.append(model.pk)

        qs = ModelDevice.objects.filter(pk__in=pks)
        return qs


class OLTConnectionViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for OLTConecction
    retrieve:
        End point for OLTConecction
    update:
        End point for OLTConecction
    delete:
        End point for OLTConecction
    """
    queryset = OLTConnection.objects.all()
    serializer_class = OLTConnectionSerializer



class OnuPGViewsets(viewsets.ReadOnlyModelViewSet):
    queryset = ONUViewModel.objects.all()
    serializer_class = OnuViewSerializer

    def get_queryset(self):
        qs = ONUViewModel.objects.none().order_by('-time_rx')
        serial = self.request.query_params.get('serial')
        time_date = self.request.query_params.get('time_date')

        if serial and time_date:
            qs = self.queryset
            serial = {'serial':serial,'time_date':time_date}
            qs = qs.filter(**serial).order_by('time_tx')
        return qs




class ChangePlanView(APIView):
    
    def get(self, request, version=None, service_number=None):
        preferences = global_preferences_registry.manager()
        
        try:
            service = Service.objects.get(number = service_number)
        except Service.DoesNotExist:
            response = Response(
                {"detail": f"El servicion con numero = {service_number} no fue encontrado"},
                content_type="application/json",
                status=status.HTTP_400_BAD_REQUEST,
            )
            response.accepted_renderer = JSONRenderer()
            response.accepted_media_type = "application/json"
            response.renderer_context = {}
            return response



        try:

            headers = {'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'}
            
            try:
                
                r = requests.get(f'{preferences["url_endpoint_erp"]}api/v1/services/{service.number}', headers=headers )
                
                if r.json().get('detail', False):
                    return build_response({'detail': 'Servicio no encontrado'}, 404)

            except Exception as e :
                print(e)

            service_matrix = r.json()

            try:
                r = requests.get(service_matrix['plan'], headers=headers )
                plan_matrix = r.json()  
            except Exception as e :
                print(e)

            plan = Plan.objects.get(matrix_plan = plan_matrix['id'])
            
            
            response = {
                'plan_matrix_id': plan_matrix['id'],
                'plan_matrix_price': plan_matrix['price'],
                'plan_matrix_name': plan_matrix['name'],
                'plan_sentinel_id': plan.id,
                'plan_sentinel_name': plan.name,
                'up':   plan.upload_speed or 0,
                'down': plan.download_speed or 0
                
            } 
            return build_response(response, 200)

        except:

            return build_response({'detail': 'Error al consultar api de Matrix'}, 400)


    def post(self, request, version=None, service_number=None):
        try:
            service = Service.objects.get(number = service_number)
        except Service.DoesNotExist:
            response = Response(
                {"detail": f"El servicion con numero = {service_number} no fue encontrado"},
                content_type="application/json",
                status=404,
            )
            response.accepted_renderer = JSONRenderer()
            response.accepted_media_type = "application/json"
            response.renderer_context = {}
            return response


        # try:
        onus = ONU.objects.filter(service=service)
        # except ONU.DoesNotExist as e:
        #     build_response({'detail': 'ONU no encontrado'}, status.HTTP_404_NOT_FOUND)
        
        import pprint
        print('')
        print('')
        print('')
        pprint.pprint(request.data)
        print(request.data.get('plan', None))

        try:
            plan = Plan.objects.get(id=request.data.get('plan', None))
        except Plan.DoesNotExist as e:
            return build_response({'detail': 'Plan no encontrado'}, 404)
        
        
        from devices.wrappers import OLTWrapperSSH

        for onu in onus:
            if onu and onu.status:
                olt = onu.olt 
                _ssh = OLTWrapperSSH(onu.olt)
                olt_ssh = _ssh.get_me()
                try:
                    olt_ssh.shell(olt)
                        
                except Exception as e:
                    olt.set_status_offline()
                    
                    print ("Error getdata get_getonus_olts", str(e))
                    return False

                info = olt_ssh.get_info_onu(onu.serial)

                if( isinstance(info, str) ):
                    return 
                
                onu_id = info.get('onu_id', None)
                f_s_p = info.get('fsp', None).split('/')

                service_port = olt_ssh.get_service_port(f_s_p[0], f_s_p[1], f_s_p[2], onu_id)
                print('')
                print('')
                print(service_port)
                index = service_port[0][0]

                updated = olt_ssh.update_service_port(index, plan.speeds_system_download.tidsis, plan.speeds_system_upload.tidsis )

                # Actualizar servicio SERIVCIO EN MATRIX 
                
                if updated == True:
                    return build_response({}, 200)
                else:
                    return build_response({'detail': updated}, 400)


class UpdateNetworkStatusAPI(APIView):
    def post(self, request, version=None):
        # Obtenemos el contenido del request
        device_type = request.data.get('device_type')
        device_id = request.data.get('device_id')

        if device_id == "":
            response = Response(
                {"detail": "El device_id no puede estar vacío"},
                content_type="application/json",
                status=status.HTTP_400_BAD_REQUEST,
            )
            response.accepted_renderer = JSONRenderer()
            response.accepted_media_type = "application/json"
            response.renderer_context = {}
            return response

        if device_type != 'OLT' and device_type != 'UTP':
            # Si no coincide con ninguno devolvemos error
            response = Response(
                {"detail": "El device_type no coincide con OLT o UTP"},
                content_type="application/json",
                status=status.HTTP_400_BAD_REQUEST,
            )
            response.accepted_renderer = JSONRenderer()
            response.accepted_media_type = "application/json"
            response.renderer_context = {}
            return response

        # importamos la tarea de refrescamiento de red en Celery
        from devices.tasks import get_services_network_status
        get_services_network_status.delay(device_id, device_type)

        return build_response({'detail': 'Estatus de Red Actualizado'}, 200)
