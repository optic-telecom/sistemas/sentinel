from dynamic_preferences.registries import global_preferences_registry
from rest_framework import viewsets, mixins
from rest_framework.generics import ListAPIView, get_object_or_404
from rest_framework.response import Response
from customers.models import TypePLAN, Service, Plan
from dashboard.simulate import Simulate
from devices.serializers import (IPTableSerializers, MenuSmokepingSerializers,
                                 ONUAutoFindSerializer,
                                 ONUProvisioningSerializer)
from customers.serializers import TypePLANSerializer
from devices.models import *

from devices.wrappers import delete_speed, olt_autofind, provisioning_onu
from utp.models import Mikrotik

__all__ = [
    'MatrixPlanViewSet', 'MatrixTechnicianViewSet',
    'MatrixServiceNumberViewSet', 'ProvisioningViewSet',
    'ProvisioningPerformViewSet'
]

# preferences = {
#     'url_endpoint_erp':'url_endpoint_erp',
#     'execute_autofind': True
# }


class MatrixPlanViewSet(viewsets.GenericViewSet):
    """
    list:
        End point for services matrix (planes)
    """
    queryset = TypePLAN.objects.all()
    serializer_class = TypePLANSerializer
    token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'
    id_value = 'planes_matrix'

    def list(self, request, *args, **kwargs):
        import requests

        preferences = global_preferences_registry.manager()
        url = f'{preferences["url_endpoint_erp"]}api/v1/plans/'

        r = requests.get(url,
                         headers={'Authorization': self.token},
                         verify=False)
        if r.status_code == 200:
            return Response(r.json())
        _l.matrix.get(by_representation=request.user.username,
                      id_value=self.id_value,
                      description=str(r.text),
                      entity_representation=repr(url))
        return Response(r.text, status=400)


class MatrixTechnicianViewSet(MatrixPlanViewSet):
    """
    list:
        End point for matrix Technician
    """
    id_value = 'technician_matrix'

    def list(self, request, *args, **kwargs):
        import requests

        preferences = global_preferences_registry.manager()
        url = f'{preferences["url_endpoint_erp"]}api/v1/technicians/'

        r = requests.get(url,
                         headers={'Authorization': self.token},
                         verify=False)
        if r.status_code == 200:
            return Response(r.json())
        _l.matrix.get(by_representation=request.user.username,
                      id_value=self.id_value,
                      description=str(r.text),
                      entity_representation=repr(url))
        return Response(r.text, status=400)


class MatrixServiceNumberViewSet(mixins.RetrieveModelMixin,
                                 viewsets.GenericViewSet):
    """
    list:
        End point for service number matrix (cliente)
    """
    queryset = TypePLAN.objects.all()
    serializer_class = TypePLANSerializer
    lookup_url_kwarg = 'service'

    def retrieve(self, request, *args, **kwargs):
        preferences = global_preferences_registry.manager()
        import requests
        h = {
            'Authorization':
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'
        }

        service_number = kwargs.pop('service', None)

        service, created = Service.objects.get_or_create(number=service_number)

        if request.GET.get('modal_info', None):
            print('+++++++++++++++++++++++++++++++++++++++++++++++++++++')
            """ Simulacion de respuesta"""
            with Simulate('matrix', 'matrix_service_number') as s:
                if s.response:
                    return Response(s.response)

            r1 = requests.get(
                f'{preferences["url_endpoint_erp"]}api/v1/services/?number=' +
                service,
                headers=h,
                verify=False)
            m1 = r1.json()
            r2 = requests.get(m1['results'][0]['customer'], verify=False)
            return Response(r2.json())

        onu = ONUAutoFind.objects.get(uuid=request.GET.get('uuid'))
        if not service:
            return Response(
                {'error': 'No proporciono ningun número de servicio'})

        if preferences['execute_search_contracts']:
            # requests to matrix
            try:

                matrix = requests.get(
                    f'{preferences["url_endpoint_erp"]}api/v1/services/?number='
                    + service_number,
                    headers=h,
                    verify=False)

            except Exception as e:
                return Response(
                    {'error': 'Api matrix error, el servidor no responde.'})

            if matrix.status_code and matrix.status_code == 200:
                matrix = matrix.json()
                if len(matrix['results']) == 0:
                    return Response({
                        'error':
                        'Cliente asociado al operador no existe en Matrix/Neo'
                    })

                if preferences['provisioning_onu_validate_status']:
                    if matrix['results'][0]['status'] not in [1, 4]:
                        return Response({
                            'error':
                            'Estado actual del servicio no permite asignarle una ONU'
                        })

                plan = requests.get(matrix['results'][0]['plan'],
                                    headers=h,
                                    verify=False)
                plan = plan.json()
                #category = int(plan['category'])

                if preferences['verify_fibra_in_plan']:
                    if not 'FIBRA' in plan['name'].upper():
                        return Response({
                            'error':
                            'El plan actual que tiene el cliente no es posible provisionar.'
                        })

                client = requests.get(matrix['results'][0]['customer'],
                                      headers=h,
                                      verify=False)
                client = client.json()

                # node = requests.get(matrix['results'][0]['node'],
                #                     headers=h,
                #                     verify=False)
                # node = node.json()
                try:
                    node = service.node
                except Exception as e:
                    return Response(
                        {'error': 'El servicio no tiene un Nodo asociado'})

                try:
                    sentinel_plan = Plan.objects.get(matrix_plan=plan['id'])
                except Plan.DoesNotExist:
                    return Response(
                        {'error': 'No se encontro plan de matrix en sentinel'})

                service_with_onu = ONU.objects.filter(service=service)

                data_provisining = {
                    'in_previus_olt':
                    onu.in_previus_olt,
                    'service':
                    service,
                    'client':
                    client,
                    'node':
                    node,
                    'service_status':
                    int(matrix['results'][0]['status']),
                    'get_status_display':
                    matrix['results'][0]['get_status_display'],
                    'plan':
                    plan,
                    'sentinel_plan':
                    sentinel_plan.id,
                    'sentinel_matrix_plan':
                    sentinel_plan.matrix_plan,
                    'uuid':
                    onu.uuid,
                    'composite_address':
                    matrix['results'][0]['composite_address'],
                    'service_with_onu':
                    True if service_with_onu.exists() else False
                }
                return Response(data_provisining)
            else:
                _l.matrix.get(by_representation=request.user.username,
                              id_value='',
                              description=str(matrix.text),
                              entity_representation=repr(matrix))
                return Response({
                    'error':
                    'Api matrix error, no respondio con el estado correcto'
                })
        else:
            # simulate response matrix
            print('==============================')
            with Simulate('provisioning_onu', 'matrix_service_number',
                          True) as s:
                return Response(s.response)


class ProvisioningViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """
    list:
        Ejecuta el comando de autofind en todas las olts
        o por una sola
    """
    serializer_class = ONUAutoFindSerializer

    def list(self, request, *args, **kwargs):
        preferences = global_preferences_registry.manager()
        if preferences['execute_autofind']:
            res = olt_autofind(request.user.username, request.GET.get('uuid'))
            if res:
                if isinstance(res, dict):
                    if 'message' in res:
                        return Response({'error': message}, status=400)
                    elif 'no_autofind' in res:
                        return Response(
                            {'error': 'No hay ninguna ONU para provisionar.'},
                            status=400)
            else:
                return Response({'error': 'Sin conexion ssh'}, status=400)
        else:
            import time
            print('sin execute')
            time.sleep(2)

        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        try:
            id_d = ONUAutoFind.objects.filter(status_field=True)\
                                             .latest('created').id_data
            qs = ONUAutoFind.objects.filter(status_field=True, id_data=id_d)
        except ONUAutoFind.DoesNotExist:
            qs = ONUAutoFind.objects.none()
        return qs


class ProvisioningPerformViewSet(mixins.CreateModelMixin,
                                 viewsets.GenericViewSet):
    """
    create:
        Comienza el proceso de aprovisionamiento de la onu
    """
    serializer_class = ONUProvisioningSerializer

    def create(self, request, *args, **kwargs):
        preferences = global_preferences_registry.manager()
        # from pdb import set_trace
        from django.contrib.sites.shortcuts import get_current_site
        from dashboard.utils import slack_exito_caso6, slack_escalar_caso2,\
             slack_exito_caso2, slack_exito_caso3, send_data_erp, send_status_change_matrix

        print('====================================================\n',
              self.request.data.get('onu'))

        with Simulate('provisioning_onu', 'provisioning_onu') as s:
            if s.response:
                import time
                time.sleep(2)

        print('++++++++', self.request.data.get('onu'))
        onu = get_object_or_404(ONUAutoFind, uuid=self.request.data.get('onu'))

        print('-----------------------------------------------------/n',
              self.request.data.get('onu'))

        #verifico si es el serial correcto
        if preferences['verify_serial']:
            digit_serial = self.request.data.get('serial').upper()
            if onu.serial[10:] != digit_serial:
                return Response(
                    {
                        'case':
                        0,
                        'case_message':
                        'Disculpe, el serial que escribio manualmente,'
                        ' no corresponde con el serial disponible para provisionar.'
                    },
                    status=201)

        full_url = ''.join(['http://', get_current_site(request).domain])
        client = self.request.data.get('client', None)

        # se enviara a matrix
        SSID = self.request.data.get('SSID', None)
        password_wifi = self.request.data.get('clave_wifi', None)
        technician = self.request.data.get('tecnico', None)

        # confirmo el plan
        select_planes = request.data.get('select_planes', None)
        sentinel_plan = self.request.data.get('sentinel_plan')
        if select_planes:
            plan = Plan.objects.get(id=select_planes)
        elif sentinel_plan:
            plan = Plan.objects.get(id=sentinel_plan)
        else:
            plan = None

        # aqui comienza el proceso de provisionamiento
        service_address = self.request.data.get('service_address')
        service_number = self.request.data.get('service')

        print('      111111  ')
        print(onu, onu.uuid, onu.serial)
        response_proc = provisioning_onu(onu, plan, request)
        case_message = getattr(response_proc, 'case_message', None)
        desc_client = getattr(response_proc, 'desc_client', None)
        onu_gateway = getattr(response_proc, 'onu_gateway', '8.8.8.8')
        onu_id = getattr(response_proc, 'onu_id', None)
        signal = getattr(response_proc, 'signal', None)
        case = getattr(response_proc, 'case', None)
        mac = getattr(response_proc, 'mac', None)
        fsp = getattr(response_proc, 'fsp', None)
        olt = getattr(response_proc, 'olt', None)
        ip = getattr(response_proc, 'ip', None)
        onu_sentinel = getattr(response_proc, 'onu', None)
        success = getattr(response_proc, 'success', None)

        # print('      222222222  ')
        # print(case_message)
        # print(desc_client)
        # print(onu_gateway)
        # print(onu_id)
        # print(signal)
        # print(case)
        # print(fsp)
        # print(olt)
        # print(ip)
        # print( onu_sentinel )
        # print(success)

        response_gui = {
            'case': case,
            'case_message': case_message,
            'signal': signal,
            'onu_id': onu_id,
            'fsp': fsp,
            'olt': str(olt),
            'ip': ip,
            'desc_client': desc_client,
            'node': str(onu.olt.node),
            'mac': mac,
            'pass_onus': preferences['pass_onus'],
            'user_onus': preferences['user_onus'],
            'SSID_2G': f'{SSID} - optic.cl - 2G',
            'SSID_5G': f'{SSID} - optic.cl - 5G',
            'password': password_wifi,
            'onu_gateway': onu_gateway
        }

        NETWORK_CABLE = 1
        GPON = 2
        GEPON = 3
        ANTENNA = 4

        # 'note':"- Codigo OLT - F/S/P ONT ID. \n\
        # - Niveles de potencia instalación. \n\
        # - Description Sentinel \n\
        # - Temperature al momento instalación \n\
        # - ONT distance \n\
        # - ONT online duration \n\
        # - Service Port INDEX".format()

        #status del servicio pasa a activo.
        if success:
            temperature = 'None'
            distance = 'None'
            service_port = 'None'
            uptime = 'None'

            if onu_sentinel:
                temperature = onu_sentinel.temperature if onu_sentinel.temperature else 'None'
                distance = onu_sentinel.distance if onu_sentinel.distance else 'None'
                service_port = onu_sentinel.service_port if onu_sentinel.service_port else 'None'
                uptime = onu_sentinel.uptime if onu_sentinel.uptime else 'None'

            # guardar en la onu
            # 'password':password_wifi,
            # 'ssid_2g': f'{SSID} - optic.cl - 2G',
            # 'ssid_5g': f'{SSID} - optic.cl - 5G',

            data_erp = {
                # 'technician':technician,
                # 'brand': onu.model.brand,
                # 'model': onu.model.model,
                # 'serial':onu.serial,
                'caso': case,
                'service': service_number,
                # 'technology_kind':GPON,

                # 'note':f"- {str(olt)} - {fsp} {onu_id}. \n\
                #         - Señal: {signal}. \n\
                #         - Description Sentinel: {onu.description}\n\
                #         - Temperature al momento instalación: {temperature} \n\
                #         - ONT distance: {distance} \n\
                #         - ONT online duration: {uptime}\n\
                #         - Service Port INDEX: {service_port}"
            }

            # if mac:
            #     data_erp['mac'] = mac

            # if ip:
            #     data_erp['ip'] = ip

        # caso 1 onu sin conf file
        if case == 1:
            pass
        elif case == 2:
            # dhcp lleno
            slack_exito_caso2(onu, plan, client, service_number,
                              service_address, request.user.username)
            slack_escalar_caso2(onu, service_number, request.user.username)
            # send_data_erp(
            #     request, data_erp,
            #     'Envio de información de provisonamiento a onu sin ip')
            send_status_change_matrix(
                request, data_erp,
                'Envio de información de provisonamiento a onu sin ip'
            )
        elif case == 3:
            # señal optica muy baja
            slack_exito_caso3(onu, plan, client, service_number,
                              service_address, signal, request.user.username)
            # send_data_erp(
            #     request, data_erp,
            #     'Envio de información de provisonamiento a onu con señal óptica muy baja'
            # )
            send_status_change_matrix(
                request, data_erp,
                'Envio de información de provisonamiento a onu con señal óptica muy baja'
            )
        # caso 4 falla en onu
        elif case == 4:
            pass
        elif case == 5:
            # onu provisionada pero ip en firewall
            # send_data_erp(
            #     request, data_erp,
            #     'Envio de información de provisonamiento a onu con ip en firewall'
            # )
            send_status_change_matrix(
                request, data_erp,
                'Envio de información de provisonamiento a onu con ip en firewall'
            )
        # elif case == 6:
        #     send_data_erp(request, data_erp,
        #                   'Envio de información de provisonamiento a onu')
            send_status_change_matrix(
                request, data_erp,
                'Envio de información de provisonamiento a onu')
            slack_exito_caso6(onu, plan, client, service_number,
                              service_address, signal, request.user.username)
        elif case == 7:
            pass

        return Response(response_gui, status=201)
