import os
import requests
from rest_framework.views import APIView
from dynamic_preferences.registries import global_preferences_registry
from rest_framework import viewsets

from devices.serializers import (IPTableSerializers, MenuSmokepingSerializers,
                                 MenuSmokepingListIPSerializers,
                                 SmokepingImgSerializer)
from devices.models import MenuSmokeping, IPTable
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework import serializers
from bs4 import BeautifulSoup
from dateutil import relativedelta
from datetime import date, datetime
from django.core.management import call_command

from dal import autocomplete

__all__ = [
    'IPTableModelViewSet', 'MenuSmokepingModelViewSet', 'SmokepingImgROVS',
    'UploadCommandView'
]

from dynamic_preferences.registries import global_preferences_registry

preferences = {'url_endpoint_erp': 'url_endpoint_erp'}


class IPTableModelViewSet(viewsets.ModelViewSet):
    serializer_class = IPTableSerializers
    queryset = IPTable.objects.all().order_by('-id')

    def get_queryset(self):
        qs = super().get_queryset()
        ip = self.request.GET.get('ip', None)
        if ip:
            qs = qs.filter(ip__istartswith=ip)
        return qs


class IPTableReadOnlyViewSet(viewsets.ReadOnlyModelViewSet):
    # Resultado no paginado
    serializer_class = IPTableSerializers
    queryset = IPTable.objects.all().order_by('-id')

    def get_queryset(self):
        qs = super().get_queryset()
        ip = self.request.GET.get('ip', None)
        if ip:
            qs = qs.filter(ip__istartswith=ip)
        return qs

    def list(self, request, *Args, **kwargs):
        qs = self.get_queryset()
        serializer = IPTableSerializers(qs, many=True)
        return Response(serializer.data)


class MenuSmokepingModelViewSet(viewsets.ModelViewSet):
    """
    API para la vista de configuracion smokeping
    """
    serializer_class = MenuSmokepingListIPSerializers

    def get_serializer_class(self):
        if self.request.method.lower() in ["post", "put"]:
            return MenuSmokepingSerializers
        return super().get_serializer_class()

    def get_queryset(self):
        qs = MenuSmokeping.objects.order_by('-id')

        title = self.request.GET.get('title', None)
        if title:
            qs = qs.filter(title__istartswith=title)

        return qs

    def destroy(self, request, *args, **kwargs):
        menu = self.get_object()
        if menu.iptable.count() > 0:
            msj = f"El Menú {menu.title} Tiene IP's registradas, de momento no se puede eliminar"
            return Response(data=dict(mensaje=msj, status=400))
        else:
            return super().destroy(request, *args, **kwargs)

    @action(methods=['get'], detail=False)
    def create_smokeping_file(self, request, version=None):
        # Generamos el archivo Targets
        creador = call_command('create_smokeping_file')
        return Response('Archivo Creado')

class SmokepingImgROVS(viewsets.ReadOnlyModelViewSet):

    serializer_class = SmokepingImgSerializer
    headers = {'Content-Type': 'application/xml'}
    xml = '<?xml version="1.0" encoding="iso-8859-15"?>'
    test = False

    def get_queryset(self):
        return list()

    def get_dates(self):
        hoy = date.today().strftime("%Y-%m-%d")
        endDateTime = self.request.GET.get('endDateTime', hoy)
        endDateTime = datetime.strptime(
            endDateTime, "%Y-%m-%d") + relativedelta.relativedelta(
                hours=23, minutes=59, seconds=59)

        DateTime = dict(
            dia=dict(startDateTime=(
                endDateTime -
                relativedelta.relativedelta(hours=23, minutes=59, seconds=59)),
                     endDateTime=endDateTime),
            semana=dict(
                startDateTime=(endDateTime - relativedelta.relativedelta(
                    weeks=1, hours=23, minutes=59, seconds=59)),
                endDateTime=endDateTime),
            mes=dict(startDateTime=(endDateTime - relativedelta.relativedelta(
                months=1, hours=23, minutes=59, seconds=59)),
                     endDateTime=endDateTime),
        )
        return DateTime

    def list(self, request, *args, **kwargs):
        global_preferences = global_preferences_registry.manager()
        # Fechas
        fechas = self.get_dates()
        date_1 = fechas.get('dia')
        date_2 = fechas.get('semana')
        date_3 = fechas.get('mes')

        fechas = [date_1, date_2, date_3]

        base = None if not self.test else 'ER1024'
        detail = None if not self.test else '168.90.156.174'

        if request.GET.get('ip_onu', detail):
            ip_onu = request.GET.get('ip_onu', detail).replace('.', '-')
        else:
            ip_onu = None

        data = {'menu': request.GET.get('menu', base), 'ip_onu': ip_onu}

        if global_preferences['url_smokeping'] and ip_onu:

            # entorno = os.environ.get('ENTORNO')
            # url_smokeping = 'http://127.0.0.1' if entorno == 'Local' else global_preferences['url_smokeping']

            url_smokeping = global_preferences['url_smokeping']
            try:
                contexto = list()
                for fecha in fechas:
                    data = {'url': url_smokeping, **data, **fecha}
                    url = "{url}/cgi-bin/smokeping.cgi?displaymode=n;start={startDateTime};end={endDateTime};target={menu}.{ip_onu}".format(
                        **data)
                    try:
                        response = requests.get(url,
                                                data=self.xml,
                                                headers=self.headers,
                                                timeout=5)
                        if response.status_code == 200:
                            soup = BeautifulSoup(response.text, "html.parser")
                            id = soup.find("img", {"id": "zoom"})
                            src = id['src'].replace('..', url)
                            contexto.append({
                                'date':
                                f"Imágen desde: {fecha['startDateTime'].strftime('%d-%m-%Y')} hasta: {fecha['endDateTime'].strftime('%d-%m-%Y')}.",
                                'url':
                                url,
                                'src':
                                id['src'].replace('..', url_smokeping)
                            })
                            many = True
                    except requests.exceptions.ConnectTimeout as timeout:

                        src = '/static/img/smokeping/Img_noDisponible.jpg'.replace(
                            '..', url_smokeping)

                        contexto.append({
                            'date':
                            f"Imágen desde: {fecha['startDateTime'].strftime('%d-%m-%Y')} hasta: {fecha['endDateTime'].strftime('%d-%m-%Y')}.",
                            'url': "javascript:;",
                            'src': src
                        })
                        many = True

            except requests.exceptions.Timeout as timeout:
                raise timeout

            return Response(SmokepingImgSerializer(contexto, many=many).data)


class UploadCommandView(viewsets.ReadOnlyModelViewSet):
    # Vista de acción para publicar el comando
    # de subir el archivo targets al server remoto de smokeping

    serializer_class = SmokepingImgSerializer

    def get_queryset(self):
        return []

    def list(self, request, *args, **kwargs):
        try:
            publisher = call_command('get_smokeping_file')
            response = 'Publicado'
        except Exception as e:
            response = str(e)

        return Response(response)



from django.conf import settings
import mimetypes
from django.http.response import HttpResponse
from datetime import datetime

def DownloadTargetFile(request, version):
    
    # El archivo tendrá la fecha del día
    date = datetime.now().strftime('%d-%m-%Y')
    filename = f'Targets-{date}.txt'
    localpath = os.path.join(settings.BASE_DIR, filename)
    file = open(localpath, 'r')
    mime_type, _ = mimetypes.guess_type(localpath)
    # Set the return value of the HttpResponse
    response = HttpResponse(file, content_type=mime_type)
    # Set the HTTP header for sending to browser
    response['Content-Disposition'] = f"attachment; filename={filename}"
    # Return the response value
    return response

# ================= AUTOCOMPLETE ======================= #


class IPTableAutocomplete(autocomplete.Select2QuerySetView):
    filtro = None

    def get_queryset(self):
        qs = IPTable.objects.none()

        if self.q:
            qs = IPTable.objects.filter(ip__istartswith=self.q)

        if self.filtro:
            qs = qs.filter(**self.filtro)

        return qs