from time import sleep
from dateutil import parser
from datetime import timedelta
from django.utils import timezone
from dateutil.relativedelta import relativedelta
#from django.db.models import Max

from dynamic_preferences.registries import global_preferences_registry
from common.utils import green, red, cyan, blue
from dashboard.utils import LOG

from devices.ssh import MA5800T_SSH
from devices.snmp import MA5608T_SNMP
from devices.models import OLTConnection, InterfaceOLT,\
    TYPE_ENERGY, TYPE_CLOCK, TYPE_SERVICE, TYPE_CONTROLLER, PortOLT,\
    OLT, ONU, ONUAutoFind, ModelDevice,\
    ServicePortONUS, ServicePortHistorical,\
    SystemSpeeds, OLTSpeeds, MACTable


class OLTWrapperSSH(object):
    def __init__(self, olt, **options):
        global_preferences = global_preferences_registry.manager()

        self.ssh = None
        self.olt_ssh = None

        multi_ssh = OLTConnection.objects.filter(olt=olt, status_field=True)

        for conn in multi_ssh:
            if conn.connections <= global_preferences['ssh_max_user_connection']:
                self.ssh = conn
                break

        if self.ssh:
            self.ssh.connection_usage()
        else:
            pass
            #sin conexion disponible?

        is_in_cron = options.get('is_in_cron', False)
        _l = options.get('_l', LOG())
        models_5800 = ['MA5800-X15', 'MA5800-X7']
        if olt.model.model in models_5800:
            olt_ssh = MA5800T_SSH(
                **{
                    'host': self.ssh.olt.ip.ip,
                    'username': self.ssh.username,
                    'password': self.ssh.password,
                    'is_in_cron': is_in_cron,
                    '_l': _l,
                    'olt': olt
                })
            self.olt_ssh = olt_ssh

        if olt.model.model == 'MA5605-X15':
            olt_ssh = MA5800T_SSH(
                **{
                    'host': self.ssh.olt.ip.ip,
                    'username': self.ssh.username,
                    'password': self.ssh.password,
                    'is_in_cron': is_in_cron,
                    '_l': _l,
                    'olt': olt
                })
            self.olt_ssh = olt_ssh

    def get_me(self):
        return self.olt_ssh

    def __del__(self):
        if self.ssh:
            self.ssh.release_connection()
        if self.olt_ssh:
            self.olt_ssh.close()

    def __repr__(self):
        """
        Returns a string representation of this object, for debugging.
        """
        return "<OLTWrapperSSHObject from sentinel.devices.Wrappers>"


def ssh_all_data_olt(olt, old_olt_ssh=None, **options):

    print("Modelo %s" % olt.model.model)

    if old_olt_ssh is not None:
        olt_ssh = old_olt_ssh
    else:
        _ssh = OLTWrapperSSH(olt)
        olt_ssh = _ssh.get_me()
        try:
            olt_ssh.shell(olt)
        except Exception as e:
            print("Error getdata,", str(e))
            olt.status = False
            olt.save()
            return False

    mac = olt_ssh.get_mac() if True else 'None'
    mac_secondary = olt_ssh.get_mac(9) if True else 'None'
    ram = olt_ssh.get_ram() if True else ''
    ram_secondary = olt_ssh.get_ram(9) if True else ''
    watts = olt_ssh.get_power()
    cpu = olt_ssh.get_cpu()
    cpu_secondary = olt_ssh.get_cpu(9)
    temperature = olt_ssh.get_temperature()
    temperature_secondary = olt_ssh.get_temperature(9)
    firmware = olt_ssh.get_firmware() if True else None
    print('ESTA ES LA MAC', mac)
    if mac is None:
        mac = 'None'
    if mac_secondary is None:
        mac_secondary = 'None'
    print('ESTA ES LA MAC', type(mac), mac)
    mact, created = MACTable.objects.get_or_create(mac=mac)
    mact_secondary, created = MACTable.objects.get_or_create(mac=mac_secondary)

    formatted_mac = MACTable.objects.format_mac(mac)
    mact, created = MACTable.objects.get_or_create(mac=formatted_mac)

    formatted_mac_secondary = MACTable.objects.format_mac(mac_secondary)
    mact_secondary, created = MACTable.objects.get_or_create(
        mac=formatted_mac_secondary)

    now = timezone.now()
    # olt.onus = 0
    olt.cards = 0
    # olt.onus_active = 0
    olt.firmware = firmware
    olt.time_cards_active = now
    olt.status = True
    olt.mac = mact
    olt.mac_secondary = mact_secondary
    olt.ram_secondary = ram_secondary
    olt.cpu_secondary = cpu_secondary
    olt.temperature_secondary = temperature_secondary
    olt.ram = ram
    olt.cpu = cpu
    olt.temperature = temperature
    olt.watts = watts
    olt.save()

    # olt_details = MoreDetailOLTS.objects.create(
    #     olt = olt,
    #     ram = ram,
    #     ram_secondary = ram_secondary,
    #     cpu_secondary = cpu_secondary,
    #     temperature_secondary = temperature_secondary,
    #     cpu = cpu,
    #     temperature = temperature,
    #     watts = watts
    # )

    # if mac:
    #     MacOLTS.objects.create(
    #         olt=olt,
    #         mac=mac,
    #         mac_secondary=mac_secondary)
    
    # Eliminamos la conexión ssh una vez terminado el proceso
    del olt_ssh

    # if options.get('set_uptime', False):
    #     if options.get('uptime'):
    #         uptime = options.get('uptime')
    #         del olt_ssh
    #     else:
    #         uptime = olt_ssh.get_sysuptime()
    #     uptime = uptime.split(', ')
    #     dias = int(uptime[0].replace('d', ''))
    #     horas = int(uptime[1].replace('h', ''))
    #     minutos = int(uptime[2].replace('m', ''))
    #     segundos = int(uptime[3].replace('s', ''))
    #     value_uptime = now + timedelta(
    #         days=-dias, hours=-horas, minutes=-minutos, seconds=-segundos)

    #     olt_details.uptime = value_uptime
    #     olt_details.time_uptime = now
    #     olt_details.save()

    return True


def update_alias_olt(olt, alias, request):

    print("Modelo olt update_alias_olt %s" % olt.model.model)
    _ssh = OLTWrapperSSH(olt)
    olt_ssh = _ssh.get_me()
    try:
        olt_ssh.shell(olt, request.user.username)
    except Exception as e:
        print("Error update_alias_olt", str(e))
        return str(e), False

    description = olt_ssh.set_alias(alias)
    return None, True


def update_port_description_olt(olt, slot, port, desc, request):

    print("Modelo olt update_port_olt %s" % olt.model.model)
    _ssh = OLTWrapperSSH(olt)
    olt_ssh = _ssh.get_me()
    try:
        olt_ssh.shell(olt, request.user.username)
    except Exception as e:
        print("Error update_port_olt", str(e))
        return str(e), False

    description = olt_ssh.set_port_description(slot, port, desc)
    olt_ssh.close()
    return None, True


def create_new_speed(olt, tid, name, cir, cbs, pir, pbs, priority, request):

    _ssh = OLTWrapperSSH(olt)
    olt_ssh = _ssh.get_me()
    try:
        olt_ssh.shell(olt, request.user.username)
    except Exception as e:
        print("Error create_new_speed", str(e))
        return str(e), False

    created = olt_ssh.create_traffic_index(tid=tid,
                                           name=name,
                                           cir=cir,
                                           cbs=cbs,
                                           pir=pir,
                                           pbs=pbs,
                                           priority=priority)
    olt_ssh.close()

    if created:
        return None, True
    return None, False


def delete_speed(olt, tid, request):

    _ssh = OLTWrapperSSH(olt)
    olt_ssh = _ssh.get_me()
    try:
        olt_ssh.shell(olt, request.user.username)
    except Exception as e:
        print("Error create_new_speed", str(e))
        return str(e), False

    deleted = olt_ssh.undo_traffic_index(tid)
    if deleted:
        exist = olt_ssh.verify_traffic_index(tid)
        if not exist:
            return None, True
        return 'No se pudo eliminar la velocidad, error desconocido', False
    return 'El indice de velocidad esta siendo usado por un service-port', False


def ssh_onu_command(command, **args):

    onu = args.get('onu')
    print("Modelo olt ssh_onu_command %s" % onu.olt.model.model)
    # print(onu.olt)

    olt_ssh = args.get('olt_ssh')
    if olt_ssh is None:
        _ssh = OLTWrapperSSH(onu.olt)
        olt_ssh = _ssh.get_me()
        try:
            request = args.pop('request', None)
            if request:
                user = request.user.username
            else:
                user = None
            olt_ssh.shell(onu.olt, user)
        except Exception as e:
            print("Error ssh_onu_command", str(e))
            return str(e), None

    if command == 'ping':
        ip = args.get('ip')
        ping = olt_ssh.onu_ping(onu.slot, onu.port, onu.onu_id, ip)
        if ping == 'error':
            return 'error', 'La ONT no esta online o introdujo una ip erronea'
        elif ping == 'failed':
            return 'failed', 'Remote ping fallo. La ip no es asignada o es erronea'
        elif ping == 'not_online':
            return 'not_online', 'La ONT no esta online'

        try:
            if 'gpon' in ping[2]:
                packages_sent = "Paquetes enviados: %s" % (
                    ping[10].split().pop())
                received_packages = "Paquetes recibidos: %s" % (
                    ping[11].split().pop())
                lost_packets = "Paquetes perdidos: %s" % (
                    ping[12].split().pop())
                lost_packets_ratio = "Proporcion de Paquetes perdidos: %s" % (
                    ping[13].split().pop())
                min_delay = "Tiempo minimo: %s" % (ping[14].split().pop())
                max_delay = "Tiempo maximo: %s" % (ping[15].split().pop())
                average_delay = "Tiempo medio: %s" % (ping[16].split().pop())
                message = "{}<br>{}<br>{}<br>{}<br>{}<br>{}<br>{}".\
                format(packages_sent, received_packages, lost_packets, lost_packets_ratio,
                    min_delay, max_delay, average_delay)
                return False, message
            else:
                packages_sent = "Paquetes enviados: %s" % (
                    ping[8].split().pop())
                received_packages = "Paquetes recibidos: %s" % (
                    ping[9].split().pop())
                lost_packets = "Paquetes perdidos: %s" % (
                    ping[10].split().pop())
                lost_packets_ratio = "Proporcion de paquetes perdidos: %s" % (
                    ping[11].split().pop())
                min_delay = "Tiempo minimo: %s" % (ping[12].split().pop())
                max_delay = "Tiempo maximo: %s" % (ping[13].split().pop())
                average_delay = "Tiempo medio: %s" % (ping[14].split().pop())
                message = "{}<br>{}<br>{}<br>{}<br>{}<br>{}<br>{}".\
                format(packages_sent, received_packages, lost_packets, lost_packets_ratio,
                    min_delay, max_delay, average_delay)
                return False, message
        except Exception as e:
            # print('555555\n\n')
            return 'error', 'Ocurrión un error al realizar remote ping'

    if command == 'tracerouter':
        ip = args.get('ip')
        return ip