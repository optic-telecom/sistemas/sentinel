import uuid
from time import sleep
from dateutil import parser
from datetime import timedelta
from django.utils import timezone
from dateutil.relativedelta import relativedelta
#from django.db.models import Max

from dynamic_preferences.registries import global_preferences_registry
from common.utils import green, red, cyan, blue
from common import Attrs
from dashboard.simulate import Simulate
from dashboard.utils import LOG, slack_notification_exito, slack_alert_escalamiento, slack_alert_incidencia, slack_alert_node, slack_alert_signal, slack_alert_error

from devices.ssh import MA5800T_SSH
from devices.snmp import MA5608T_SNMP
from devices.models import OLTConnection, InterfaceOLT,\
    TYPE_ENERGY, TYPE_CLOCK, TYPE_SERVICE, TYPE_CONTROLLER, PortOLT,\
    OLT, ONU, ONUAutoFind, ModelDevice,\
    ServicePortONUS, ServicePortHistorical,\
    SystemSpeeds, OLTSpeeds, Operator
from .all import OLTWrapperSSH

from customers.models import Service

from devices.utils import ping_onu

#global_preferences = global_preferences_registry.manager()


def olt_autofind(user, uuid_olt=None):
    """ Con esto simulo una respuesta de autofind"""
    with Simulate('provisioning_onu', 'olt_autofind') as s:
        if s.response:
            return s.response

    id_data = uuid.uuid4()
    alguna_onu = False
    obj_to_log = {'by_representation': user}

    if uuid_olt:
        olts = OLT.objects.filter(status_field=True, uuid=uuid_olt)
    else:
        olts = OLT.objects.filter(status_field=True)

    for olt in olts:

        _ssh = OLTWrapperSSH(olt)
        olt_ssh = _ssh.get_me()

        try:
            olt_ssh.shell(olt, str(user))
        except Exception as e:
            red("Error olt_autofind, ", str(olt), str(e))
            olt.status = False
            olt.save()
            return False

        lines = olt_ssh.get_olt_autofind()
        green('inicio')
        data = {}
        finds = []
        current_number = -1

        for line in lines:

            d = line.split()

            if 'Failure' in line:
                red('Sin onus a provisionar')
                data['save'] = False
                continue

            if 'Number' in line:
                data['number'] = d[2]
                current_number = int(d[2])

            if 'Ont SN' in line:
                if current_number != -1:
                    data['serial'] = d[3]

            if 'SoftwareVersion' in line:
                if current_number != -1:
                    data['software_version'] = d[3]

            if 'F/S/P' in line:
                if current_number != -1:
                    data['fsp'] = d[2]

            if 'VendorID' in line:
                if current_number != -1:
                    data['vendor'] = d[2]

            if 'EquipmentID' in line:
                if current_number != -1:
                    data['model'] = d[3]

            if 'autofind time' in line:
                cyan("autofind")
                if current_number != -1:
                    data['olt'] = olt
                    data['autofind_time'] = d[4] + ' ' + d[5]
                    finds.append(data)
                    green(data)
                    data = {}

        def condition(onu):
            try:
                if onu.get('save', True):
                    return True
            except Exception as e:
                return False

        finds = list(filter(condition, finds))
        onus = ONU.objects.filter(olt=olt, status_field=True)

        for onu_find in finds:
            ##obtengo modelo para el nuevo registro !?
            try:
                # model = ModelDevice.objects.filter(for_olt=False,
                model = ModelDevice.objects.filter(
                    status_field=True, model=onu_find['model']).last()
                if not model:
                    raise ModelDevice.DoesNotExist

                if onu_find.get('vendor', None) and \
                    not hasattr(model, 'brand'):
                    model.brand = onu_find['vendor']
                    model.save()

            except ModelDevice.DoesNotExist:
                model = ModelDevice()
                # model.for_olt=False
                model.model = onu_find['model']
                if onu_find.get('vendor', None):
                    model.brand = onu_find['vendor']
                model.save()

            for onu in onus:
                if onu_find['serial'] == onu.serial:

                    green('ONU ya en sentinel')
                    #No borro sino creo un nuevo reg historico
                    # onu_h = ONUHistorical()
                    # onu_h.id_data = id_data
                    # onu_h.onu_origin_id = onu.id
                    # onu_h.alias = onu.alias
                    # onu_h.serial = onu.serial
                    # onu_h.olt = onu.olt
                    # onu_h.frame = onu.frame
                    # onu_h.slot = onu.slot
                    # onu_h.port = onu.port
                    # onu_h.description = onu.description
                    # onu_h.model = onu.model
                    # onu_h.main_software = onu.main_software
                    # onu_h.standby_software = onu.standby_software
                    # onu_h.onu_id = onu.onu_id
                    # onu_h.distance = onu.distance
                    # onu_h.TX = onu.TX
                    # onu_h.RX = onu.RX
                    # onu_h.service_port = onu.service_port
                    # onu_h.upload_speed = onu.upload_speed
                    # onu_h.download_speed = onu.download_speed
                    # onu_h.service = onu.service
                    # onu_h.service_number_verified = onu.service_number_verified
                    # onu_h.control_flag = onu.control_flag
                    # onu_h.run_state = onu.run_state
                    # onu_h.config_state = onu.config_state
                    # onu_h.mapping_mode = onu.mapping_mode
                    # onu_h.TR069_management = onu.TR069_management
                    # onu_h.TR069_ip_index = onu.TR069_ip_index
                    # onu_h.line_profile_id = onu.line_profile_id
                    # onu_h.line_profile_name = onu.line_profile_name
                    # onu_h.status = onu.status
                    # onu_h.uptime = onu.uptime
                    # onu_h.downtime = onu.downtime
                    # onu_h.downcause = onu.downcause
                    # onu_h.time_uptime = onu.time_uptime
                    # onu_h.ram = onu.ram
                    # onu_h.cpu = onu.cpu
                    # onu_h.temperature = onu.temperature
                    # onu_h.mac = onu.mac
                    # onu_h.ip = onu.ip
                    # onu_h.autofind = onu.autofind
                    # onu_h.save()
                    #print ('onu_h',onu_h.pk)
                    alguna_onu = proc_onu_autofind(olt_ssh, onu, onu_find,
                                                   model, id_data, True)
                    onu_find['no_sentinel'] = False

        for onu_find in finds:
            try:
                onu_find['no_sentinel']
            except Exception as e:
                red('onu find no sentinel')
                ### proceso sino se encontro onu en sentinel
                ### pero si viene en el autofind
                alguna_onu = proc_onu_autofind_no_sentinel(
                    olt_ssh, onu_find, model, id_data, False)

    if alguna_onu:
        if isinstance(alguna_onu, dict):
            return alguna_onu
        return True
    return {'no_autofind': True}


def proc_onu_autofind_no_sentinel(olt_ssh, onu_find, model, id_data,
                                  in_previus_olt):
    import re

    data_onu = olt_ssh.get_info_onu(onu_find['serial'])

    if 'error' in data_onu:
        data_onu = olt_ssh.get_info_onu(onu_find['serial'])

    if 'error' in data_onu:
        data_onu = olt_ssh.get_info_onu(onu_find['serial'])

    if 'error' in data_onu:
        ###tercer reintento de comando
        return {'message': 'Error ejecución comando display ont info by-sn'}

    red('data_onu')
    red(data_onu)

    new_onu = ONUAutoFind()
    run_state = data_onu.get('run_state', None)
    fsp_array = onu_find['fsp'].split('/')

    if run_state == 'online':
        new_onu.in_previus_olt = 2
        ## Emitir aleta a slack
        from dashboard.utils import slack_error
        fields = [{
            "title": "Serial",
            "value": onu_find['serial'],
            "short": True
        }, {
            "title": "F/S/P",
            "value": onu_find['fsp'],
            "short": True
        }, {
            "title": "OLT",
            "value": onu_find['olt'].alias,
            "short": True
        }]
        err = 'Onu online aparecio en autofind'
        slack_error('provisionar ONU', err, fields)
        return {'message': err}
    else:

        out_desc_port = olt_ssh.get_port_description(int(fsp_array[1]),
                                                     int(fsp_array[2]))
        new_onu.description = out_desc_port

        if in_previus_olt:
            new_onu.in_previus_olt = 1
            # busco si esta onu tiene numero de servicio asociado
            coincidences = re.findall('\d{5}', out_desc_port)
            if len(coincidences) == 1:
                try:
                    new_onu.service = Service.objects.get(
                        number=coincidences[0])
                except Service.DoesNotExist:
                    print(e)
                    s = Service(number=coincidences[0])
                    s.save()
                    new_onu.service = s
            else:
                new_coincidences = re.findall('\d{4}', out_desc_port)
                if len(new_coincidences) > 0:
                    try:
                        new_onu.service = Service.objects.get(
                            number=new_coincidences[0])
                    except Service.DoesNotExist:
                        print(e)
                        s = Service(number=new_coincidences[0])
                        s.save()
                        new_onu.service = s

            # onu nunca en sentinel tabla principal
        else:
            new_onu.in_previus_olt = 0

    new_onu.serial = onu_find['serial']
    new_onu.olt = onu_find['olt']
    new_onu.model = model
    new_onu.number = onu_find['number']
    new_onu.autofind_time = parser.parse(onu_find['autofind_time'])
    new_onu.software_version = onu_find['software_version']
    new_onu.fsp = onu_find['fsp']
    new_onu.id_data = id_data
    new_onu.save()

    print(' -**** ', new_onu.uuid, new_onu.serial, id_data)
    return True


def proc_onu_autofind(olt_ssh, onu, onu_find, model, id_data, in_previus_olt):
    import re
    import time

    if in_previus_olt:
        pass

    time.sleep(0.1)
    data_onu = olt_ssh.get_info_onu(onu_find['serial'])

    if 'error' in data_onu:
        data_onu = olt_ssh.get_info_onu(onu_find['serial'])

    if 'error' in data_onu:
        data_onu = olt_ssh.get_info_onu(onu_find['serial'])

    if 'error' in data_onu:
        ###tercer reintento de comando
        return {'message': 'Error ejecución comando display ont info by-sn'}

    red('data_onu')
    red(data_onu)
    uptime = olt_ssh.get_uptime_onu(onu.serial)
    blue('uptime')
    blue(uptime)

    if uptime:
        # Actualizo solo si esta uptime
        u = uptime.split(', ')
        dias = int(u[0].replace('d', ''))
        horas = int(u[1].replace('h', ''))
        minutos = int(u[2].replace('m', ''))
        segundos = int(u[3].replace('s', ''))
        now = timezone.now()
        value_uptime = now + relativedelta(
            days=-dias, hours=-horas, minutes=-minutos, seconds=-segundos)
        onu.uptime = value_uptime
        onu.time_uptime = now
        onu.status = True

        #Actualizo el estatus en este modelo tambien
        # s = StatusONUS()
        # s.onu = onu
        # s.status = True
        # s.save()

    onu.autofind = True

    # details = MoreDetailONUS.objects.create(onu=onu)
    # details.id_data = id_data

    line_profile_name = data_onu.get('line_profile_name', None)
    if line_profile_name:
        onu.line_profile_name = line_profile_name
        # details.line_profile_name = line_profile_name

    line_profile_id = data_onu.get('profile_id', None)
    if line_profile_id:
        onu.line_profile_id = line_profile_id.replace(':', '')
        # details.line_profile_id = line_profile_id.replace(':','')

    TR069_ip_index = data_onu.get('ip_index', None)
    if TR069_ip_index:
        onu.TR069_ip_index = TR069_ip_index.replace(':', '')
        # details.TR069_ip_index = TR069_ip_index.replace(':','')

    TR069_management = data_onu.get('management', None)
    if TR069_management:
        if TR069_management == ':Disable':
            onu.TR069_management = False
            # details.TR069_management = False
        else:
            onu.TR069_management = True
            # details.TR069_management = True

    mapping_mode = data_onu.get('mapping_mode', None)
    if mapping_mode:
        onu.mapping_mode = mapping_mode.replace(':', '')
        # details.mapping_mode = mapping_mode.replace(':','')

    config_state = data_onu.get('config_state', None)
    if config_state:
        onu.config_state = config_state
        # details.config_state = config_state

    run_state = data_onu.get('run_state', None)
    if run_state:
        onu.run_state = run_state
        # details.run_state = run_state

    control_flag = data_onu.get('control_flag', None)
    if control_flag:
        onu.control_flag = control_flag
        # details.control_flag = control_flag

    ram = data_onu.get('ram', None)
    if ram:
        onu.ram = ram.replace('%', '')
        # details.ram = ram.replace('%','')

    cpu = data_onu.get('cpu', None)
    if cpu:
        onu.cpu = cpu.replace('%', '')
        # details.cpu = cpu.replace('%','')

    temperature = data_onu.get('temperature', None)
    if temperature:
        onu.temperature = temperature.replace('(C)', '')
        # details.temperature = temperature.replace('(C)','')

    new_onu = ONUAutoFind()
    new_onu.serial = onu_find['serial']
    new_onu.olt = onu_find['olt']
    new_onu.onu_origin = onu
    new_onu.model = model
    new_onu.number = onu_find['number']
    fsp_array = onu_find['fsp'].split('/')
    out_desc_port = olt_ssh.get_port_description(int(fsp_array[1]),
                                                 int(fsp_array[2]))
    new_onu.description = out_desc_port
    new_onu.autofind_time = parser.parse(onu_find['autofind_time'])
    if run_state == 'online':
        new_onu.in_previus_olt = 2
        ## Emitir aleta a slack
        from dashboard.utils import slack_error
        fields = [{
            "title": "Serial",
            "value": onu_find['serial'],
            "short": True
        }, {
            "title": "F/S/P",
            "value": onu_find['fsp'],
            "short": True
        }, {
            "title": "OLT",
            "value": onu_find['olt'].alias,
            "short": True
        }]
        err = 'Intento de provisionar ONU, con run_state online, posible clonación de SN Fibra Óptica'
        slack_error('provisionar ONU', err, fields)

    else:
        if in_previus_olt:
            new_onu.in_previus_olt = 1
        else:
            new_onu.in_previus_olt = 0
            # onu nunca en sentinel

    new_onu.software_version = onu_find['software_version']
    new_onu.fsp = onu_find['fsp']
    new_onu.id_data = id_data

    if onu.service:
        new_onu.service = onu.service
    else:
        coincidences = re.findall('\d{5}', out_desc_port)
        if len(coincidences) == 1:
            try:
                new_onu.service = Service.objects.get(number=coincidences[0])
            except Service.DoesNotExist:
                s = Service(number=coincidences[0])
                s.save()
                new_onu.service = s
        else:
            new_coincidences = re.findall('\d{4}', out_desc_port)
            if len(new_coincidences) > 0:
                try:
                    new_onu.service = Service.objects.get(
                        number=new_coincidences[0])
                except Service.DoesNotExist:
                    s = Service(number=new_coincidences[0])
                    s.save()
                    new_onu.service = s

    new_onu.save()

    onu.description = new_onu.description
    onu.model = model
    onu.save()
    print('111111', onu.uuid)
    return True


def provisioning_onu(onu, plan, request):
    """ Con esto simulo una respuesta de provisionamiento por casos"""
    with Simulate('provisioning_onu', 'provisioning_onu') as s:
        if s.response:
            return s.response

    id_data = uuid.uuid4()

    tower = request.data.get('tower', None)
    code = request.data.get('code', None)
    apartment_number = request.data.get('apartment_number', None)
    service_number = request.data.get('service')
    service_status = request.data.get('service_status')
    speed_up = request.data.get('speed_up', None)
    speed_down = request.data.get('speed_down', None)
    operator_id = request.data.get('select_operator', None)
    try:
        oper = Operator.objects.get(id=operator_id)
    except Operator.DoesNotExist:
        return Attrs(
            case_message=f'El operador seleccionado no existe en sentinel/db',
            success=False,
            case=0)

    _ssh = OLTWrapperSSH(onu.olt)
    olt_ssh = _ssh.get_me()

    try:
        olt_ssh.shell(onu.olt, str(request.user.username))
    except Exception as e:
        red("Error provisioning_onu, ", str(e))
        onu.olt.status = False
        onu.olt.save()
        return Attrs(
            case_message=f'No hay conexion SSH con la OLT {onu.olt.alias}',
            success=False,
            case=0)

    ### en la misma OlT?

    # Verifico si la onu existe en esa OLT dentro de sentinel
    onu_in_sentinel = ONU.objects.filter(olt=onu.olt, serial=onu.serial)

    # Verifico si el numero de servicio se encuentra dentro de alguna onu
    servicio_onu_asociada = ONU.objects.filter(olt=onu.olt,
                                               service__number=service_number)

    data_onu = olt_ssh.get_info_onu(onu.serial)
    fsp = onu.fsp.split('/')
    run_state = data_onu.get('run_state', None)
    signal_error = False
    if tower:
        desc = '{} - {} - {} - {} - {}'.format(service_number, oper.code, code,
                                               apartment_number, tower)
    else:
        desc = '{} - {} - {} - {}'.format(service_number, oper.code, code,
                                          apartment_number)

    # print('   33333 333  ')
    # print(data_onu)
    # print(run_state)
    if run_state:
        onu.run_state = run_state
        #onu.fsp = data_onu['fsp']
        onu.save()

        if run_state == 'online':
            from dashboard.utils import slack_error
            fields = [{
                "title": "Serial",
                "value": onu.serial,
                "short": True
            }, {
                "title": "F/S/P",
                "value": onu.fsp,
                "short": True
            }, {
                "title": "OLT",
                "value": onu.olt.alias,
                "short": True
            }]
            err = 'Intento de provisionar ONU, con run_state online, con numero de servicio introducido %s.' % str(
                service_number)
            m = "No se pudo eliminar la ONU anteriormente registrada ya que está online en la OLT"
            slack_error('provisionar ONU', err, fields)
            return Attrs(case_message=m, case=0, success=False)

        else:
            fsp_display = data_onu['fsp'].split('/')
            # Comprobar si hay o no un service-port asociado a dicho F/S/P ONT_ID,
            # usando el comando "display service-port port F/S/P ont ONT_ID"
            service = olt_ssh.get_service_port(fsp_display[0], fsp_display[1],
                                               fsp_display[2],
                                               data_onu['onu_id'])
            if len(service) > 0:
                try:
                    service_status = service[0][12]
                    if service_status == 'down':
                        print("aqui se elimina service port si existe")
                        olt_ssh.undo_service_port(service[0][0])
                        # se comprueba que se elimino efectivame el service_port
                        service = olt_ssh.get_service_port(
                            fsp_display[0], fsp_display[1], fsp_display[2],
                            data_onu['onu_id'])
                        try:
                            service['no_service_port']
                            # paso a historico
                            try:
                                sp = ServicePortONUS.objects.filter(
                                    service_port=service[0][0],
                                    status_field=True,
                                    onu__serial=onu.serial).last()
                                sph = ServicePortHistorical()
                                sph.service_port = service[0][0]
                                sph.vlan_id = service[0][1]
                                sph.vlan_attr = service[0][2]
                                sph.port_type = service[0][3]
                                sph.status = False
                                # datos de onu
                                sph.serial = onu.serial
                                sph.frame = onu.frame
                                sph.slot = onu.slot
                                sph.port = onu.port
                                sph.description = onu.description
                                sph.model = onu.model
                                sph.onu_id = onu.onu_id
                                sph.save()

                                # elimino de sentinel
                                sp.delete()
                            except ServicePortONUS.DoesNotExist as e:
                                pass

                        except Exception as e:
                            return Attrs(
                                case_message="El service port sigue activo",
                                success=False,
                                case=0)

                except KeyError as e:
                    pass
            else:
                return Attrs(message='No se encontro service port',
                             success=False,
                             case=0)

            o_d = olt_ssh.ont_delete(fsp_display[1], fsp_display[2],
                                     data_onu['onu_id'])

            if not o_d:
                o_d = olt_ssh.ont_delete(fsp_display[1], fsp_display[2],
                                         data_onu['onu_id'])

            if not o_d:
                o_d = olt_ssh.ont_delete(fsp_display[1], fsp_display[2],
                                         data_onu['onu_id'])

            if o_d:
                delete_onu_sentinel(onu, id_data, fsp)
                # borro tambien de sentinel y lo guardare en un nuevo historico

                new_ont_id = olt_ssh.ont_confirm(fsp[1], fsp[2], onu.serial,
                                                 desc)

                #obtengo la señal
                res_optical = optical_info(fsp, new_ont_id, olt_ssh)

                if isinstance(res_optical, dict):
                    #si me dio error lo omito e informo
                    signal_error = res_optical["signal_error"]

                #obitiene un array con las fila de salida del service port index
                get_index = create_new_service_port(olt_ssh, new_ont_id, fsp,
                                                    plan, onu, speed_up,
                                                    speed_down)

                if not get_index:
                    get_index = create_new_service_port(
                        olt_ssh, new_ont_id, fsp, plan, onu, speed_up,
                        speed_down)

                if not get_index:
                    return Attrs(message="No se pudo crear el service_port",
                                 success=False,
                                 case=0)

                # print('66666666')
                # print(get_index)
                # obtiene la ultima fila que contiene el numero index del service port
                new_service_port = get_index[len(get_index) - 1]
                # print(new_service_port)

                onu_sentinel = create_onu_sentinel(olt_ssh, onu, new_ont_id,
                                                   res_optical,
                                                   new_service_port[0],
                                                   service_number, fsp)
                create_service_port_sentinel(onu_sentinel, onu,
                                             new_service_port)
                sms_ip_mac, mac, ip, onu_sin_conf, service_type, gateway = get_ip_and_mac(
                    olt_ssh, fsp, new_ont_id)

                # print(onu_sentinel, sms_ip_mac, mac, ip, onu_sin_conf, service_type, gateway)
                # verifica si la la onu se configuro vien en la red
                if onu_sin_conf:
                    # Deberia borrar la ONT recien provisionada
                    delete_ont(new_service_port[0], fsp, onu_sentinel)
                    return Attrs(
                        case_message="No se pudo crear el service port",
                        success=False,
                        case=1)

                # ejecuto el comando remote-ping en la olt
                success, tag_gateway_or_error, tag_8888 = remote_ping(
                    gateway, onu_sentinel, ip, olt_ssh)
                case = 0
                m = ''
                # verfifico la ejecucion del remote-ping
                # si se ejcuto correctamento hago la revision de la salida
                if success:
                    case = review_matrix(tag_gateway_or_error, tag_8888,
                                         onu_sentinel, service_number,
                                         request.user.username, signal_error)

                else:
                    # si no se eecuta correctamente
                    # verifico si el service type es internet
                    m = ''
                    if service_type.lower() == 'internet':
                        case = 2
                        m = tag_gateway_or_error
                    else:
                        # verificio el nivel de señal devuelto
                        if signal_error:
                            if signal_error <= -25:
                                case = 3
                                m: 'Nivel de señal muy bajo'
                            else:
                                m: 'Hay una falla con la ONU'
                                case = 4
                                delete_ont(new_service_port[0], fsp,
                                           onu_sentinel)

                        message = tag_gateway_or_error + '<br>' + m
                        # return Attrs(case_message=message, case=case)

                if case not in [4, 7]:
                    if signal_error:
                        m = m + " Onu provisionada exitosamente, con nivel de señal bajo: {}".format(
                            signal_error)
                        signal = signal_error
                    else:
                        m = m + " Onu provisionada exitosamente."
                        signal = res_optical

                m = m + ' ' + sms_ip_mac
                return Attrs(onu=onu_sentinel, signal=signal, fsp=f'{fsp[0]}/{fsp[1]}/{fsp[2]}',\
                    olt=onu_sentinel.olt, onu_gateway=gateway, onu_id=onu_sentinel.onu_id, \
                    case_message=m, success=True, mac=mac, ip=ip, service_number=service_number, case=case)

            else:
                m = "No se pudo borrar la onu."
                return Attrs(message=m, success=False, case=0)

            return Attrs(message="Sin service port devuelto",
                         case=0,
                         success=False)
    else:

        green("ONU sin run_state, no registrada en la OLT")

        #Se comprueba si el numero de servicio posee una o mas onus ya asociada
        res_find_old = find_old_onu_service(servicio_onu_asociada, olt_ssh,
                                            id_data)
        if res_find_old.error:
            return Attrs(case_message=res_find_old.error,
                         case=0,
                         success=False)

        delete_onu_sentinel(onu, id_data, fsp, False)
        new_ont_id = olt_ssh.ont_confirm(fsp[1], fsp[2], onu.serial, desc)
        res_optical = optical_info(fsp, new_ont_id, olt_ssh)

        if isinstance(res_optical, dict):
            signal_error = res_optical["signal_error"]

        #obitiene un array con las fila de salida del service port index
        get_index = create_new_service_port(olt_ssh, new_ont_id, fsp, plan,
                                            onu, speed_up, speed_down)
        if not get_index:
            get_index = create_new_service_port(olt_ssh, new_ont_id, fsp, plan,
                                                onu, speed_up, speed_down)
        if not get_index:
            return Attrs(case_message="No se pudo crear el service_port",
                         success=False,
                         case=0)

        # print('4444444')
        # print(get_index)
        # obtiene la ultima fila que contiene el numero index del service port
        new_service_port = get_index[len(get_index) - 1]
        # print(new_service_port)

        onu_sentinel = create_onu_sentinel(olt_ssh, onu, new_ont_id,
                                           res_optical, new_service_port[0],
                                           service_number, fsp)
        create_service_port_sentinel(onu_sentinel, onu, new_service_port)
        sms_ip_mac, mac, ip, onu_sin_conf, service_type, gateway = get_ip_and_mac(
            olt_ssh, fsp, new_ont_id)

        print(onu_sentinel, sms_ip_mac, mac, ip, onu_sin_conf, service_type,
              gateway)
        if onu_sin_conf:
            delete_ont(new_service_port[0], fsp, onu_sentinel)
            return Attrs(case_message="No se pudo crear el service port",
                         success=False,
                         case=1)

        success, tag_gateway_or_error, tag_8888 = remote_ping(
            gateway, onu_sentinel, ip, olt_ssh)
        case = 0
        m = ''
        if success:
            case = review_matrix(tag_gateway_or_error, tag_8888, onu_sentinel,
                                 service_number, request.user.username,
                                 signal_error)

        else:
            m = ''
            if service_type.lower() == 'internet':
                case = 2
                m = tag_gateway_or_error
            else:
                if signal_error:
                    if signal_error <= -25:
                        case = 3
                        m: 'Nivel de señal muy bajo'
                    else:
                        m: 'Hay una falla con la ONU'
                        delete_ont(new_service_port[0], fsp, onu_sentinel)
                        case = 4

                message = tag_gateway_or_error + '<br>' + m
                # return Attrs(case_message=message, case=case)

        if case not in [4, 7]:
            if signal_error:
                m = m + " Onu provisionada exitosamente, con nivel de señal bajo: {}".format(
                    signal_error)
                signal = signal_error
            else:
                m = m + " Onu provisionada exitosamente."
                signal = res_optical

        m = m + ' ' + sms_ip_mac
        return Attrs(onu=onu_sentinel, signal=signal, fsp=f'{fsp[0]}/{fsp[1]}/{fsp[2]}',\
            olt=onu_sentinel.olt, onu_gateway=gateway, onu_id=onu_sentinel.onu_id, \
            case_message=m, success=True, mac=mac, ip=ip, service_number=service_number, case=case)


# esta funcion saca a la ont de la olt, borrando el service port y luego ont delete
def delete_ont(service_port_index, fsp, onu_sentinel):
    # Deberia borrar la ONT recien provisionada
    print('se deberia borrar la onu provisionada')
    olt_ssh.undo_service_port(service_port_index)
    # se comprueba que se elimino efectivame el service_port
    service = olt_ssh.get_service_port(fsp[0], fsp[1], fsp[2],
                                       onu_sentinel.onu_id)
    try:
        if service['no_service_port']:
            olt_ssh.ont_delete(fsp[1], fsp[2], onu_sentinel.onu_id)
    except Exception as e:
        pass


def find_old_onu_service(servicio_onu_asociada, olt_ssh, id_data):
    if servicio_onu_asociada.exists():
        for soa in servicio_onu_asociada:
            # primer intento
            info_old_onu = olt_ssh.get_info_onu(soa.serial)

            # segundo intento
            try:
                info_old_onu['error']
                info_old_onu = olt_ssh.get_info_onu(soa.serial)
            except KeyError:
                pass

            # tercer intento
            try:
                info_old_onu['error']
                info_old_onu = olt_ssh.get_info_onu(soa.serial)
            except KeyError:
                pass

            try:
                info_old_onu['error']
                return Attrs(
                    error=
                    f'Error ejecución comando ontinfo by-sn de otra ONU {soa.serial}'
                )
            except KeyError:
                pass

            if 'ont_does_not_exist' in info_old_onu:
                # Si la onu ya fue eliminada de esa otra olt solo pasamos a historico de ONT
                pass

            elif info_old_onu['control_flag'] == 'active':
                # Como esta activo se busca el service port

                fsp_display = info_old_onu['fsp'].split('/')
                res_service_port = olt_ssh.get_service_port(
                    fsp_display[0], fsp_display[1], fsp_display[2],
                    info_old_onu['onu_id'])
                res_service_port_sentinel = res_service_port
                if isinstance(res_service_port, dict):
                    # no deberia lanzar excepción
                    print(
                        'GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG'
                    )
                    print(res_service_port)
                    res_service_port['no_service_port']
                else:
                    print(
                        'HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH'
                    )

                    # Si trae numeros de servicio (normalmente 1)
                    if len(res_service_port) != 1:
                        return Attrs(
                            error=
                            f'unable to delete other client ONU, device with more than 1 service-port'
                        )

                    # si solo trajo un solo numero de servicio
                    # elimino el service port
                    olt_ssh.undo_service_port(res_service_port[0][0])
                    # se comprueba que se elimino efectivame el service_port
                    res_service_port = olt_ssh.get_service_port(
                        fsp_display[0], fsp_display[1], fsp_display[2],
                        info_old_onu['onu_id'])

                    if isinstance(res_service_port, dict):
                        res_service_port['no_service_port']
                    else:
                        # intento 2da vez eliminar el service port
                        olt_ssh.undo_service_port(res_service_port[0][0])
                        # se comprueba que se elimino efectivame el service_port
                        res_service_port = olt_ssh.get_service_port(
                            fsp_display[0], fsp_display[1], fsp_display[2],
                            info_old_onu['onu_id'])

                        if isinstance(res_service_port, dict):
                            res_service_port['no_service_port']
                        else:
                            # intento 3ra vez eliminar el service port
                            olt_ssh.undo_service_port(res_service_port[0][0])
                            # se comprueba que se elimino efectivame el service_port
                            res_service_port = olt_ssh.get_service_port(
                                fsp_display[0], fsp_display[1], fsp_display[2],
                                info_old_onu['onu_id'])
                            if not isinstance(res_service_port, dict):
                                return Attrs(
                                    error=
                                    f'Error no fue posible borrar service-port de la otra ONU del servicio.'
                                )

                # paso a historico de service port y elimino de sentinel el actual service port
                try:
                    print(
                        'JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ'
                    )
                    print(res_service_port)
                    onu = soa
                    sp = ServicePortONUS.objects.filter(
                        service_port=res_service_port_sentinel[0][0],
                        status_field=True,
                        onu__serial=onu.serial).last()
                    sph = ServicePortHistorical()
                    sph.service_port = res_service_port_sentinel[0][0]
                    sph.vlan_id = res_service_port_sentinel[0][1]
                    sph.vlan_attr = res_service_port_sentinel[0][2]
                    sph.port_type = res_service_port_sentinel[0][3]
                    sph.status = False
                    # datos de onu
                    sph.serial = onu.serial
                    sph.frame = onu.frame
                    sph.slot = onu.slot
                    sph.port = onu.port
                    sph.description = onu.description
                    sph.model = onu.model
                    sph.onu_id = onu.onu_id
                    sph.id_data = id_data
                    sph.save()

                    # elimino de sentinel
                    sp.delete()
                except ServicePortONUS.DoesNotExist as e:
                    pass
                except IndexError:
                    pass

                # no habia ó se elimino el service port disponible/activo
                # se procede a borrado de onu en la olt
                o_d = olt_ssh.ont_delete(fsp_display[1], fsp_display[2],
                                         info_old_onu['onu_id'])
                # segundo intento de eliminar la ont anterior
                if not o_d:
                    o_d = olt_ssh.ont_delete(fsp_display[1], fsp_display[2],
                                             info_old_onu['onu_id'])

                # tercer intento
                if not o_d:
                    o_d = olt_ssh.ont_delete(fsp_display[1], fsp_display[2],
                                             info_old_onu['onu_id'])

                if not o_d:
                    return Attrs(
                        error=
                        f'Error no fue posible eliminar la ONT {soa.serial} del servicio anterior'
                    )

                #verifico que se elimino efectivame la onu vieja
                info_old_onu = olt_ssh.get_info_onu(soa.serial)
                if 'ont_does_not_exist' not in info_old_onu:
                    return Attrs(
                        error=
                        f'Error no fue posible eliminar la ONT {soa.serial} del servicio anterior'
                    )

                # delete_onu_sentinel(soa, id_data, fsp_display, True)
                # en este punto el numero de servicio ya no debe tener ni servicer port asociado
                # ni onu, por lo que esta libre para provisionar

    return Attrs(error=False)


def create_new_service_port(olt_ssh, new_ont_id, fsp, plan, onu, speed_up,
                            speed_down):
    #return True
    from dashboard.utils import slack_error
    vlan = 100
    frame = fsp[0]
    slot = fsp[1]
    port = fsp[2]
    gemport = 100
    vlan_2 = 100

    if speed_up:
        system_up = speed_up
    else:
        system_up = plan.speeds_system_upload

    try:
        olt_speed_up = OLTSpeeds.objects.filter(
            olt=onu.olt, speeds_system=system_up).last()

    except Exception as e:

        fields = [{
            "title": "Serial",
            "value": onu.serial,
            "short": True
        }, {
            "title":
            "F/S/P ID",
            "value":
            str(fsp[0]) + "/" + str(fsp[1]) + "/" + str(fsp[2]) + " " +
            str(new_ont_id),
            "short":
            True
        }, {
            "title": "OLT",
            "value": onu.olt.alias,
            "short": True
        }, {
            "title": 'system_up',
            "value": str(system_up),
            "short": True
        }]
        slack_error('provisionar ONU', str(e), fields)
        raise e

    try:
        if speed_down:
            system_down = speed_down
        else:
            system_down = plan.speeds_system_download

        print(system_down)
        olt_speed_down = OLTSpeeds.objects.filter(
            olt=onu.olt, speeds_system=system_down).last()
    except Exception as e:
        fields = [{
            "title": "Serial",
            "value": onu.serial,
            "short": True
        }, {
            "title":
            "F/S/P ID",
            "value":
            str(fsp[0]) + "/" + str(fsp[1]) + "/" + str(fsp[2]) + " " +
            str(new_ont_id),
            "short":
            True
        }, {
            "title": "OLT",
            "value": onu.olt.alias,
            "short": True
        }, {
            "title": 'system_down',
            "value": str(system_down),
            "short": True
        }]
        slack_error('provisionar ONU', str(e), fields)
        raise e

    tid_up = olt_speed_up.tid
    tid_down = olt_speed_down.tid

    olt_ssh.quit_interface()

    ### Primer intento
    c_s = olt_ssh.create_service_port(vlan, frame, slot, port, new_ont_id,
                                      gemport, vlan_2, tid_up, tid_down)
    try:
        print('primer intento')
        r = olt_ssh.get_service_port(frame, slot, port, new_ont_id)
        if isinstance(r, dict):
            return False
        return r
    except (IndexError, KeyError) as e:
        pass

    ### Segundo intento
    c_s = olt_ssh.create_service_port(vlan, frame, slot, port, new_ont_id,
                                      gemport, vlan_2, tid_up, tid_down)
    try:

        print('segundo intento')
        r = olt_ssh.get_service_port(frame, slot, port, new_ont_id)
        if isinstance(r, dict):
            return False
        return r
    except (IndexError, KeyError) as e:
        return False


def delete_onu_sentinel(onu, id_data, fsp, onu_not_in_olt=True):

    # onu_h = ONUHistorical()
    # onu_h.deleted_sentinel = True
    # if onu_not_in_olt:
    #     onu_h.deleted_reason = "ONU no estaba en la OLT"
    # else:
    #     onu_h.deleted_reason = "ONU habia sido previamente borrada de la OLT"
    # onu_h.id_data = id_data
    # onu_h.serial = onu.serial
    # onu_h.olt = onu.olt
    # onu_h.frame = fsp[0]
    # onu_h.slot = fsp[1]
    # onu_h.port = fsp[2]
    # onu_h.description = onu.description
    # onu_h.run_state = False
    # onu_h.save()

    onu.deleted_sentinel = True
    if onu_not_in_olt:
        onu.deleted_reason = "ONU no estaba en la OLT"
    else:
        onu.deleted_reason = "ONU habia sido previamente borrada de la OLT"
    onu.id_data = id_data
    onu.serial = onu.serial
    onu.olt = onu.olt
    onu.frame = fsp[0]
    onu.slot = fsp[1]
    onu.port = fsp[2]
    onu.description = onu.description
    onu.run_state = False
    onu.save()

    # if onu.onu_origin:
    #     onu.onu_origin.delete()
    #     onu.onu_origin = None
    # else:
    #     pass

    return True


def optical_info(fsp, new_ont_id, olt_ssh, raise_signal=False):
    try:
        new_signal = olt_ssh.optical_info(fsp[1], fsp[2], new_ont_id)
        print('new_signal_string', new_signal)
        new_signal = float(new_signal)
        if not raise_signal:
            return new_signal

        if (new_signal < -8) & (new_signal >= -25):
            return new_signal
        else:
            return {'signal_error': new_signal, 'signal_message': '---'}
    except ValueError as e:
        return {'signal_error': 0, 'signal_message': str(e)}


def create_onu_sentinel(olt_ssh, onu_autofind, new_ont_id, signal,
                        service_port, service_number, fsp):

    #print ("aca123", onu_autofind, new_ont_id, signal, service_port, service_number, fsp)
    try:
        onu_sentinel = ONU.all_objects.get(serial=onu_autofind.serial,
                                           olt=onu_autofind.olt)
    except Exception as e:
        onu_sentinel = ONU()

    service, created = Service.objects.get_or_create(number=service_number)

    onu_autofind.service = service
    onu_autofind.save()

    data_onu = olt_ssh.get_info_onu(onu_autofind.serial)

    onu_sentinel.serial = onu_autofind.serial
    onu_sentinel.olt = onu_autofind.olt
    onu_sentinel.frame = fsp[0]
    onu_sentinel.slot = fsp[1]
    onu_sentinel.port = fsp[2]
    onu_sentinel.description = onu_autofind.description
    onu_sentinel.model = onu_autofind.model
    onu_sentinel.autofind = True
    onu_sentinel.main_software = onu_autofind.software_version
    onu_sentinel.onu_id = new_ont_id
    onu_sentinel.distance = data_onu['distance']
    onu_sentinel.temperature = data_onu['temperature']
    # onu_sentinel.uptime = data_onu['uptime']

    if not isinstance(signal, dict):
        onu_sentinel.RX = float(signal)

    onu_sentinel.service_port = service_port
    onu_sentinel.service = service
    onu_sentinel.service_number_verified = True
    onu_sentinel.run_state = True
    onu_sentinel.status = True
    onu_sentinel.deleted_sentinel = False
    onu_sentinel.save()

    return onu_sentinel


def create_service_port_sentinel(onu_sentinel, onu_autofind, service_port):

    ser = ServicePortONUS()
    if onu_autofind:
        ser.onu_autofind = onu_autofind
    if onu_sentinel:
        ser.onu = onu_sentinel
    ser.service_port = service_port[0]
    ser.vlan_id = service_port[1]
    ser.vlan_attr = service_port[2]
    ser.port_type = service_port[3]
    ser.status = True
    ser.save()
    return True


def get_ip_and_mac(olt_ssh, fsp, onu_id, reintentar=True):

    # espero 6 segundos para solicitud de dhcp
    sleep(5)

    onu_sin_Conf = False

    # pido la ip y mac
    info = olt_ssh.get_wan_info(fsp[0], fsp[1], fsp[2], onu_id)
    message = ''
    mac = info.get('mac', None)
    ip = info.get('ip', None)
    gateway = info.get('gateway', None)
    service_type = info.get('service_type', '')
    if isinstance(info, dict):
        if ip and mac:
            newMac = ':'.join(mac[i:i + 2] for i in range(0, 12, 2))
            message = 'La dirección ip es {} y la mac {}'.format(
                ip, newMac.upper())
        elif ip:
            message = 'La dirección ip es '.format(ip)
        else:
            message = 'No fue posible obtener dirección IP (posible causa: ONU en valores por defecto)'
    else:
        if reintentar:
            print('Se reintenta obtener nuevamente la ip y mac')
            return get_ip_and_mac(olt_ssh, fsp, onu_id, False)
        else:
            if 'wan_port_not_exist' in info:
                onu_sin_Conf = True
            message = 'No se pudo obtener dirección ip, onu offline'

    return message, mac, ip, onu_sin_Conf, service_type, gateway


def remote_ping(gateway, onu, ip, olt_ssh):

    if not ip:
        message = 'Remote Ping no ejecutado por falta de IP'
        return False, message, None

    # print(f'{fsp[0]}/{fsp[1]}/{fsp[2]} {onu_id}')
    # info = olt_ssh.get_wan_info(fsp[0], fsp[1], fsp[2], onu_id)
    # print(' ***** info ***', info)

    if not gateway:
        message = 'Remote Ping no ejecutado por falta de IP gateway'
        return False, message, None

    for i in range(11):

        err, res = ping_onu(onu, gateway, olt_ssh=olt_ssh)
        print(err, res)
        if not err:
            if isinstance(res, dict):
                if res['ratio'] < 30:
                    tag_gateway = 'ping_gateway_ok'
                elif res['ratio'] >= 30 and res['ratio'] <= 99:
                    tag_gateway = 'ping_gateway_packet_loss'
                elif res['ratio'] == 100:
                    tag_gateway = 'ping_gateway_failed'
                else:
                    tag_gateway = 'error_else'
            else:
                if 'failed' in res:
                    tag_gateway = 'ping_gateway_failed'
                elif 'not_online' in res:
                    tag_gateway = 'ping_gateway_onu_offline'
                else:
                    tag_gateway = 'error_else'

            break

        if i == 10:
            if res:
                return False, res, None
            else:
                return False, 'No se ejecuto correctamente remote-ping gateway', None

    for i in range(11):
        err, res = ping_onu(onu, '8.8.8.8', olt_ssh=olt_ssh)

        if not err:
            if isinstance(res, dict):
                if res['ratio'] < 30:
                    tag_8888 = 'ping_8888_ok'
                elif res['ratio'] >= 30 and res['ratio'] <= 99:
                    tag_8888 = 'ping_8888_packet_loss'
                elif res['ratio'] == 100:
                    tag_8888 = 'ping_8888_failed'
                else:
                    tag_8888 = 'error_else'
            else:
                if 'failed' in res:
                    tag_8888 = 'ping_8888_failed'
                elif 'not_online' in res:
                    tag_8888 = 'ping_8888_onu_offline'
                else:
                    tag_8888 = 'error_else'

            break

        if i == 10:
            if res:
                return False, res, None
            else:
                return False, 'No se ejecuto correctamente remote-ping 8.8.8.8', None

    return True, tag_gateway, tag_8888


def review_matrix(tag_gateway,
                  tag_8888,
                  onu,
                  service_number,
                  user,
                  signal='-'):
    tag_gateway = tag_gateway.lower()
    tag_8888 = tag_8888.lower()

    if 'ok' in tag_gateway:
        if 'ok' in tag_8888:
            # avisar el éxito en el canal de Slack #provisionar-clientes
            slack_notification_exito(onu, service_number, user)

            # seguir con configuración de WiFi  de ONU
            return 6

        elif 'packet_loss' in tag_8888:

            # avisar de incidencia (8.8.8.8 packet loss - gateway OK) en provisionamiento
            # en el canal de Slack #provisionar-clientes
            slack_alert_incidencia('8.8.8.8 packet loss - gateway OK', onu,
                                   service_number, user)

            # alerta ONU PACKET LOSS a 8.8.8.8 desde [nodo de corte] en canal #escalamiento-tecnico.
            slack_alert_escalamiento(
                'ONU PACKET LOSS a 8.8.8.8 desde nodo de corte', onu,
                service_number, user)

            # seguir con configuración de WiFi  de ONU
            return 6

        elif 'failed' in tag_8888:
            # avisar de incidencia (8.8.8.8 failed - gateway OK - posible IP en Address-List del firewall del nodo)
            # en provisionamiento en el canal de Slack #provisionar-clientes
            slack_alert_node(
                '8.8.8.8 failed - gateway OK - posible IP en Address-List del firewall del nodo',
                onu, service_number, user)

            # Caso de Revisión Firewall Nodo - wizard de solucion firewall nodo.
            return 5
        elif 'onu_offline' in tag_8888:

            # avisar de incidencia (8.8.8.8 ONU offline - gateway OK - posible problema señal o de ONU ) en provisionamiento en el canal de Slack #provisionar-clientes
            slack_alert_signal(
                '8.8.8.8 ONU offline - gateway OK - posible problema señal o de ONU',
                onu, service_number, user, signal)

            # alerta ONU offine a la 8.8.8.8 desde [nodo de corte] en canal #escalamiento-tecnico
            slack_alert_escalamiento(
                'ONU offine a la 8.8.8.8 desde nodo de corte', onu,
                service_number, user)

            # Caso de Revisión ONU - wizard de problema ONU (revisar niveles de señal, etc...)
            return 4
        elif 'error_else' in tag_8888:

            # avisar de incidencia (8.8.8.8 else - gateway OK - posible error) en provisionamiento en el canal de Slack #provisionar-clientes,
            slack_alert_incidencia('8.8.8.8 else - gateway OK - posible error',
                                   onu, service_number, user)

            # y avisar con detalle profundo en canal #errores-sentinel)
            slack_alert_error('8.8.8.8 else - gateway OK', onu, service_number,
                              user, signal, potencia_optica)

            # Caso de configuración de WiFi  de ONU
            return 6
    elif 'packet_loss' in tag_gateway:

        if 'ok' in tag_8888:
            # avisar de incidencia (8.8.8.8 ok - gateway packet loss - Revisar nivel de potencia óptica) en provisionamiento en el canal de Slack #provisionar-clientes
            slack_alert_signal(
                '8.8.8.8 ok - gateway packet loss - Revisar nivel de potencia óptica',
                onu, service_number, user, signal)

            # seguir con configuración de WiFi  de ONU
            return 6

        elif 'packet_loss' in tag_8888:

            # avisar de incidencia (8.8.8.8 packet loss - gateway packet loss - Revisar nivel de potencia óptica) en provisionamiento en el canal de Slack #provisionar-clientes
            slack_alert_signal(
                '8.8.8.8 packet loss - gateway packet loss - Revisar nivel de potencia óptica',
                onu, service_number, user, signal)

            # alerta ONU PACKET LOSS a 8.8.8.8 desde [nodo de corte] en canal #escalamiento-tecnico.
            slack_alert_escalamiento(
                'ONU PACKET LOSS a 8.8.8.8 desde nodo de corte', onu,
                service_number, user)

            # seguir con configuración de WiFi  de ONU
            return 6

        elif 'failed' in tag_8888:
            # avisar de incidencia (8.8.8.8 failed - gateway packet loss - posible problema señal o de ONU )
            # en provisionamiento en el canal de Slack #provisionar-clientes
            slack_alert_signal(
                '8.8.8.8 failed - gateway packet loss - posible problema señal o de ONU',
                onu, service_number, user, signal)

            # alerta ONU failed total desde [nodo de corte] en canal #escalamiento-tecnico.
            slack_alert_escalamiento('ONU failed total desde nodo de corte',
                                     onu, service_number, user)

            # Caso de Revisión Firewall Nodo - wizard de solucion firewall nodo.
            return 5
        elif 'onu_offline' in tag_8888:

            # avisar de incidencia (8.8.8.8 ONU offline - gateway packet loss - posible problema señal o de ONU )
            # en provisionamiento en el canal de Slack #provisionar-clientes
            slack_alert_signal(
                '8.8.8.8 ONU offline - gateway packet loss - posible problema señal o de ONU ',
                onu, service_number, user, signal)

            # alerta ONU offine a la 8.8.8.8 desde [nodo de corte] en canal #escalamiento-tecnico
            slack_alert_escalamiento(
                'ONU offine a la 8.8.8.8 desde nodo de corte', onu,
                service_number, user)

            # Caso de Revisión ONU - wizard de problema ONU (revisar niveles de señal, etc...)
            return 4
        elif 'error_else' in tag_8888:
            # Caso de Revisión ONU - wizard de problema ONU (revisar niveles de señal, etc...)

            # avisar de incidencia (8.8.8.8 else - gateway packet loss - posible problema señal o de ONU)
            # en provisionamiento en el canal de Slack #provisionar-clientes,
            slack_alert_signal(
                '8.8.8.8 else - gateway packet loss - posible problema señal o de ONU',
                onu, service_number, user, signal)

            # y avisar con detalle profundo en canal #errores-sentinel)
            slack_alert_error('8.8.8.8 else - gateway packet loss', onu,
                              service_number, user, signal, potencia_optica)

            return 4
    elif 'failed' in tag_gateway:
        if 'ok' in tag_8888:
            # avisar de incidencia (8.8.8.8 ok - gateway failed)
            # en provisionamiento en el canal de Slack #provisionar-clientes
            slack_alert_incidencia('8.8.8.8 ok - gateway failed', onu,
                                   service_number, user)

            # alerta ONU OK a 8.8.8.8 pero gateway failed desde [nodo de corte] posible falla de FIREWALL del nodo
            # en canal #escalamiento-tecnico.
            slack_alert_escalamiento(
                'ONU OK a 8.8.8.8 pero gateway failed desde nodo de corte posible falla de FIREWALL del nodo',
                onu, service_number, user)

            # seguir con configuración de WiFi  de ONU
            return 6

        elif 'packet_loss' in tag_8888:

            # avisar de incidencia (8.8.8.8 packet loss - gateway failed - Revisar nivel de potencia óptica)
            # en provisionamiento en el canal de Slack #provisionar-clientes
            slack_alert_signal(
                '8.8.8.8 packet loss - gateway failed - Revisar nivel de potencia óptica',
                onu, service_number, user, signal)

            # alerta ONU PACKET LOSS a 8.8.8.8 y gateway failed desde [nodo de corte] posible falla de FIREWALL del nodo en canal
            # #escalamiento-tecnico.
            slack_alert_escalamiento(
                'ONU PACKET LOSS a 8.8.8.8 y gateway failed desde nodo de corte - posible falla de FIREWALL del nodo',
                onu, service_number, user)

            # seguir con configuración de WiFi  de ONU
            return 6

        elif 'failed' in tag_8888:
            # avisar de incidencia (8.8.8.8 failed - gateway failed - posible problema señal o de ONU)
            # en provisionamiento en el canal de Slack #provisionar-clientes
            slack_alert_signal(
                '8.8.8.8 failed - gateway failed - posible problema señal o de ONU',
                onu, service_number, user, signal)

            # Caso de Revisión ONU - wizard de problema ONU (revisar niveles de señal, etc...)
            return 4
        elif 'onu_offline' in tag_8888:
            # avisar de incidencia (8.8.8.8 ONU offline - gateway failed - posible problema señal o de ONU)
            # en provisionamiento en el canal de Slack #provisionar-clientes
            slack_alert_signal(
                '8.8.8.8 ONU offline - gateway failed - posible problema señal o de ONU',
                onu, service_number, user, signal)

            # alerta ONU offine a la 8.8.8.8 desde [nodo de corte] en canal #escalamiento-tecnico
            slack_alert_escalamiento(
                'ONU offine a la 8.8.8.8 desde nodo de corte', onu,
                service_number, user)

            # Caso de Revisión ONU - wizard de problema ONU (revisar niveles de señal, etc...)
            return 6
        elif 'error_else' in tag_8888:
            # avisar de incidencia (8.8.8.8 else - gateway failed - posible problema señal o de ONU)
            # en provisionamiento en el canal de Slack #provisionar-clientes,
            slack_alert_signal(
                '8.8.8.8 else - gateway failed - posible problema señal o de ONU',
                onu, service_number, user, signal)

            # y avisar con detalle profundo en canal #errores-sentinel)
            slack_alert_error('8.8.8.8 else - gateway failed', onu,
                              service_number, user, signal, potencia_optica)

            # Caso de Revisión ONU - wizard de problema ONU (revisar niveles de señal, etc...)
            return 4
    elif 'onu_offline' in tag_gateway:
        if 'ok' in tag_8888:
            # avisar de incidencia (8.8.8.8 ok - gateway ONU offline - Revisar nivel de potencia óptica)
            # en provisionamiento en el canal de Slack #provisionar-clientes
            slack_alert_signal(
                '8.8.8.8 ok - gateway ONU offline - Revisar nivel de potencia óptica',
                onu, service_number, user, signal)

            # seguir con configuración de WiFi  de ONU
            return 6

        elif 'packet_loss' in tag_8888:

            # avisar de incidencia (8.8.8.8 packet loss - gateway ONU offline - Revisar nivel de potencia óptica)
            # en provisionamiento en el canal de Slack #provisionar-clientes
            slack_alert_signal(
                '8.8.8.8 packet loss - gateway ONU offline - Revisar nivel de potencia óptica',
                onu, service_number, user, signal)

            # alerta ONU PACKET LOSS a 8.8.8.8 y gateway ONU offline desde [nodo de corte] posible falla de FIREWALL del nodo
            # #escalamiento-tecnico.
            slack_alert_escalamiento(
                'ONU PACKET LOSS a 8.8.8.8 y gateway ONU offline desde nodo de corte posible falla de FIREWALL del nodo',
                onu, service_number, user)

            # seguir con configuración de WiFi  de ONU
            return 6

        elif 'failed' in tag_8888:
            # avisar de incidencia (8.8.8.8 failed - gateway ONU offline - posible problema señal o de ONU)
            # en provisionamiento en el canal de Slack #provisionar-clientes
            slack_alert_signal(
                '8.8.8.8 failed - gateway ONU offline - posible problema señal o de ONU',
                onu, service_number, user, signal)

            # Caso de Revisión ONU - wizard de problema ONU (revisar niveles de señal, etc...)
            return 4
        elif 'onu_offline' in tag_8888:
            # avisar de incidencia (8.8.8.8 ONU offline - gateway ONU offline - posible problema señal o de ONU)
            # en provisionamiento en el canal de Slack #provisionar-clientes
            slack_alert_signal(
                '8.8.8.8 ONU offline - gateway ONU offline - posible problema señal o de ONU',
                onu, service_number, user, signal)

            # Caso de Revisión ONU - wizard de problema ONU (revisar niveles de señal, etc...)
            return 4
        elif 'error_else' in tag_8888:
            # avisar de incidencia (8.8.8.8 else - gateway onu offline - posible problema señal o de ONU)
            # en provisionamiento en el canal de Slack #provisionar-clientes,
            slack_alert_signal(
                '8.8.8.8 else - gateway onu offline - posible problema señal o de ONU',
                onu, service_number, user, signal)

            # y avisar con detalle profundo en canal #errores-sentinel)
            slack_alert_error('8.8.8.8 else - gateway onu offline', onu,
                              service_number, user, signal, potencia_optica)

            # Caso de Revisión ONU - wizard de problema ONU (revisar niveles de señal, etc...)
            return 4
    elif 'error_else' in tag_gateway:
        if 'ok' in tag_8888:
            # avisar de incidencia (8.8.8.8 ok - gateway else)
            # en provisionamiento en el canal de Slack #provisionar-clientes
            slack_alert_incidencia('8.8.8.8 ok - gateway else', onu,
                                   service_number, user)

            # y avisar con detalle profundo en canal #errores-sentinel
            slack_alert_error('8.8.8.8 ok - gateway else', onu, service_number,
                              user, signal, potencia_optica)

            # seguir con configuración de WiFi  de ONU
            return 6

        elif 'packet_loss' in tag_8888:

            # avisar de incidencia (8.8.8.8 packet loss - gateway else)
            # en provisionamiento en el canal de Slack #provisionar-clientes
            slack_alert_incidencia('8.8.8.8 packet loss - gateway else', onu,
                                   service_number, user)

            # y avisar con detalle profundo en canal #errores-sentinel)
            slack_alert_error('8.8.8.8 packet loss - gateway else', onu,
                              service_number, user, signal, potencia_optica)

            # seguir con configuración de WiFi  de ONU
            return 6

        elif 'failed' in tag_8888:
            # avisar de incidencia (8.8.8.8 failed - gateway else - posible problema señal o de ONU))
            # en provisionamiento en el canal de Slack #provisionar-clientes
            slack_alert_incidencia(
                '8.8.8.8 failed - gateway else - posible problema señal o de ONU',
                onu, service_number, user)

            # alerta ONU PACKET LOSS a 8.8.8.8 y gateway ONU else desde [nodo de corte] posible falla desconocida
            # en canal #escalamiento-tecnico,
            slack_alert_escalamiento(
                'ONU Failed a 8.8.8.8 y gateway ONU else desde nodo de corte posible falla desconocida',
                onu, service_number, user)

            # Caso de Revisión ONU - wizard de problema ONU (revisar niveles de señal, etc...)

            # y avisar con detalle profundo en canal #errores-sentinel)
            slack_alert_error('8.8.8.8 failed - gateway else', onu,
                              service_number, user, signal, potencia_optica)
            return 4
        elif 'onu_offline' in tag_8888:
            # Caso de Revisión ONU - wizard de problema ONU (revisar niveles de señal, etc...)

            # avisar de incidencia (8.8.8.8 ONU failed - gateway else - posible problema señal o de ONU)
            # en provisionamiento en el canal de Slack #provisionar-clientes
            slack_alert_signal(
                '8.8.8.8 ONU offline - gateway else - posible problema señal o de ONU',
                onu, service_number, user, signal)

            # alerta ONU PACKET LOSS a 8.8.8.8 y gateway ONU else desde [nodo de corte] posible falla desconocida
            # en canal #escalamiento-tecnico,
            slack_alert_escalamiento(
                'ONU offline a 8.8.8.8 y gateway ONU else desde nodo de corte posible falla desconocida',
                onu, service_number, user)

            # y avisar con detalle profundo en canal #errores-sentinel)
            slack_alert_error('8.8.8.8 ONU offline - gateway else', onu,
                              service_number, user, signal, potencia_optica)
            return 4
        elif 'error_else' in tag_8888:
            # Caso de Revisión ONU - wizard de problema ONU (revisar niveles de señal, etc...)

            # avisar de incidencia (doble-else)
            # en provisionamiento en el canal de Slack #provisionar-clientes,
            slack_alert_incidencia('DOUBLE-ELSE', onu, service_number, user)

            # # alerta ONU PACKET LOSS a 8.8.8.8 y gateway ONU else desde [nodo de corte] posible falla desconocida
            # en canal #escalamiento-tecnico,
            slack_alert_escalamiento('DOUBLE ELSE', onu, service_number, user)

            # y avisar con detalle profundo en canal #errores-sentinel)
            slack_alert_error('Error DOBLE ELSE', onu, service_number, user,
                              signal, potencia_optica)
            return 7
