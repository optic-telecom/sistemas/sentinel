import os
from django.db import models

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill
from imagekit.processors import SmartResize
# Create your models here.
from .validators import validate_image_extension


class Pic(models.Model):
    photo = models.ImageField(_('photo'), upload_to='photos', validators=[validate_image_extension])
    photo_standard = ImageSpecField(source='photo',
                                    processors=[SmartResize(800, 600)],
                                    format='JPEG',
                                    options={'quality': 80})
    photo_thumbnail = ImageSpecField(source='photo',
                                     processors=[ResizeToFill(320, 240)],
                                     format='JPEG',
                                     options={'quality': 60})
    caption = models.CharField(_('caption'), max_length=255)
    created_at = models.DateTimeField(default=timezone.now)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return self.caption




class Document(models.Model):
    file = models.FileField(_('file'), upload_to='files')
    description = models.CharField(_('description'), max_length=255)
    tag = models.CharField(_('tag'), default="doc", max_length=3)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    created_at = models.DateTimeField(default=timezone.now)
    agent = models.ForeignKey('auth.User', on_delete=models.CASCADE, verbose_name=_('agent'), null=True, blank=True, )
    
    class Meta:
        ordering = ['-created_at']

    @property
    def extension(self):
        name, extension = os.path.splitext(self.file.name)
        return extension


    @property
    def name(self):
        return self.file.name

    def __str__(self):
        return self.description
