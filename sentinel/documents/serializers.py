


from rest_framework import serializers
from common.serializers import QueryFieldsMixin
from .models import Document, Pic

class PicSerializer(QueryFieldsMixin, serializers.ModelSerializer):
	photo = serializers.SerializerMethodField()
	photo_thumbnail = serializers.SerializerMethodField()


	def get_photo(self, obj):
		request = self.context.get('request')
		photo_url = obj.photo.url
		return request.build_absolute_uri(photo_url)

	def get_photo_thumbnail(self, obj):
		request = self.context.get('request')
		photo_url = obj.photo_thumbnail.url
		return request.build_absolute_uri(photo_url)

	class Meta:
		model = Pic
		fields = ('id','photo', 'photo_thumbnail', 'caption', 'created_at')



class DocSerializer(QueryFieldsMixin, serializers.ModelSerializer):
	file = serializers.SerializerMethodField()
	agent = serializers.SerializerMethodField()


	def get_file(self, obj):
		request = self.context.get('request')
		file_url = obj.file.url
		return request.build_absolute_uri(file_url)

	def get_agent(self, obj):
		if obj.agent:
			agent = obj.agent.username
		else:
			agent = 'matrix'

		return agent

	class Meta:
		model = Document
		fields = ('id', 'file', 'description', 'created_at', 'agent', 'extension', 'name')



