from rest_framework import routers
from django.urls import path, re_path
from .views import *


router = routers.DefaultRouter()

router.register(r'pic', PicViewSet, 'pic_api')
router.register(r'doc', DocViewSet, 'doc_api')

urlpatterns = [
    # re_path(r'^pic/(?P<pk>[0-9]+)', PicDestroyView.as_view(), name='pic_api-destroy'),
] + router.urls