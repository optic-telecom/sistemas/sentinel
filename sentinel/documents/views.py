

from rest_framework import viewsets
from .models import Pic, Document
from .serializers import PicSerializer, DocSerializer

class PicViewSet(viewsets.ModelViewSet):
    queryset = Pic.objects.all()
    serializer_class = PicSerializer
    # permission_classes = [IsAccountAdminOrReadOnly]


class DocViewSet(viewsets.ModelViewSet):
    queryset = Document.objects.all()
    serializer_class = DocSerializer
    # permission_classes = [IsAccountAdminOrReadOnly]
