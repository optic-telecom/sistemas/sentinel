function openAccordion(self) {
  var content=$(self).next(".accordion-content");
  if(content.css("display") == "none"){//open     
      $(self).addClass("open");  
      content.slideDown(250);
      if ($(self).attr('data-event')){
          window[$(self).attr('data-event')]()
      }      
  }
  else{ //close    
      $(self).removeClass("open");  
      content.slideUp(250);
  }
}