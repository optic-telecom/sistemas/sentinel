tooltip_sentinel = (d) => {
    const tooltips = d.getElementsByClassName('mf-tooltip');

    const tooltipBuilder = (tooltip) => {
        const tip_container = d.createElement('div')
        const tip = d.createElement('div')
        const data = tooltip.getAttribute('data-content');
        tip_container.classList.add('mf-tooltip__container');
        tip.classList.add('mf-tooltip__content');
        tip.innerHTML = data;
        tip_container.appendChild(tip);
        tooltip.appendChild(tip_container);
    }

    Array.from(tooltips).forEach(tooltip => {
        tooltipBuilder(tooltip);
        //tooltip.addEventListener('mouseover', e => {})
    })
}
