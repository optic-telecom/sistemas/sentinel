userInSession();
App.setPageTitle('Sentinel - Detalle de Cabecera');
var cabecera_id = getUrlLastParameter();
var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};

$("#menu_fmm").addClass('active');
$("#menu_fmm_cabecera").addClass('active');


//* events *//
$(document).ready(function (){
  
  url_ports = Django.datatable_port.replace(':val:',cabecera_id);

  table_ports = $('#table_ports').dataTable({
    "aaSorting":[[0,"desc"]],
      "language": {
      "sLengthMenu":     "Mostrar _MENU_ puertos",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando puerto del _START_ al _END_ de un total de _TOTAL_ puertos",
      "sInfoEmpty":      "Mostrando puerto del 0 al 0 de un total de 0 puertos",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ puertos)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },

      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
    "dom": '<"top">rt<"bottom"iflp><"clear">',
    "processing": true,
    "serverSide": true,
    "searchDelay": 400,
    "ajax": {
        "url": url_ports,
        "type": "POST",
        "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
    },
    "columnDefs": [
      {
        orderable: true,
        searchable: true,
        targets: [0,1,2]
      },
      {
        orderable: false,
        searchable: false,
        targets: [3]
      }
    ],    
  });

  table_ports.on( 'order.dt', function (e, settings) {
     var order = table_ports.api().order();
     $(".table_ports > thead > tr > th").css('color','black')
     $( ".table_ports > thead > tr > th:eq( "+order[0][0]+" )" )
     .css( "color", "#2271b3" );
  });
  
  /// get detail
  getDetailsCabecera();
  getConnectorTypes();

//   $('#filter_port_connector_type').change(function(){
//     table_ports.api().ajax.reload(null, false);
//   })

  // Error messages
  const customMessages = {
    valueMissing:    'Campo obligatorio'
  }

  function getCustomMessage (type, validity) {
    if (validity.typeMismatch) {
      return customMessages[`${type}Mismatch`]
    } else {
      for (const invalidKey in customMessages) {
        if (validity[invalidKey]) {
          return customMessages[invalidKey]
        }
      }
    }
  }

  var invalidClassName = 'invalid'
  var inputs = document.querySelectorAll('input, select, textarea')
  inputs.forEach(function (input) {
    // Each time the user types or submits, this will
    // check validity, and set a custom message if invalid.
    function checkValidity () {
      const message = input.validity.valid
        ? null
        : getCustomMessage(input.type, input.validity, customMessages)
      input.setCustomValidity(message || '')
      if (message!=null){
        input.classList.add(invalidClassName)
      }else{
        input.classList.remove(invalidClassName)
      }
      
    }
    input.addEventListener('input', checkValidity)
    input.addEventListener('invalid', checkValidity)

  })

  // Remove error messages when modal is closed
  $('#modal-update').on('hidden.bs.modal', function () {
    inputs.forEach(function (input) {
      input.classList.remove(invalidClassName)
    })
  })

});/// end ready

function getDetailsCabecera() {
  var url = DjangoURL('api_cabecera_detail', cabecera_id)
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      var url = DjangoURL('api_model_detail', res.data.model)
      axios.get(url, conf)
      .then(function(res){
        if (res.status == 200)
        {
          $("#model").text(res.data.name);
        }
        if (res.status == 401)
        {
          handleUnauthorizedAxios();
        }
      })//end then
      .catch(function(error) {
          handleErrorAxios(error);
      });

      var url = DjangoURL('api_manufacturer_detail', res.data.manufacturer)
      axios.get(url, conf)
      .then(function(res){
        if (res.status == 200)
        {
          $("#manufacturer").text(res.data.name);
        }
        if (res.status == 401)
        {
          handleUnauthorizedAxios();
        }
      })//end then
      .catch(function(error) {
          handleErrorAxios(error);
      });
      $("#modules").text(res.data.modules);
      $("#ports_per_module").text(res.data.ports_per_module);
      $("#comment").text(res.data.comment);
      if (res.data.physical_ext==null){
        $("#first").attr("selected","selected");
      }else{
        var url = DjangoURL('api_physicalextreme_detail', res.data.physical_ext)
        axios.get(url, conf)
        .then(function(res){
          if (res.status == 200)
          {
            $("#physical_ext").text(res.data.address+", "+res.data.commune+", "+res.data.location);
          }
          if (res.status == 401)
          {
            handleUnauthorizedAxios();
          }
        })//end then
        .catch(function(error) {
            handleErrorAxios(error);
        });
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

getConnectorTypes = () => {  
  var url = DjangoURL('api_cabecera_list');
  axios.options(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      $("#det_port_connector_type").append(`<option value="">Seleccione el tipo de conector</option>`);
      // Los choices estan actions > POST > common_conector_type > choices > value, display_name
      var length = res.data.actions.POST.common_connector_type.choices.length;
      for (var i = 0; i < length ; i++) {
        var value = res.data.actions.POST.common_connector_type.choices[i].value;
        var name = res.data.actions.POST.common_connector_type.choices[i].display_name;
        $("#det_port_connector_type").append(`<option value="${value}">${name}</option>`);
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

function getDetailsPort(id) {
  var url = DjangoURL('api_port_detail', id)
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
        $("#det_port_no").text(res.data.port_no);
        $("#det_port_connector_type > option").each(function() {
          if ($(this).val()==res.data.port_connector_type)
          {
            $(this).attr("selected","selected");
          }
        });
        $("#det_comment").text(res.data.comment);
        $("#update-form").attr("onsubmit","return updatePort("+id+")");
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

function updatePort(id) {

  $('#modal-update').modal('hide');
  $('#modal-update-sleep').modal('show');
  var url = DjangoURL('api_port_detail', id)
  var data = { 
    "port_connector_type": $("#det_port_connector_type").val(),
    "comment": $("#det_comment").val()
  }
  axios.patch(url, data, conf)
  .then(function(res){
    if (res.status == 200)
    {
      // $('#modal-create-sleep').modal('hide');
      swal({
      title: "Moficado",
        text: 'Puerto modificado exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        updateDatatable();
        $('#modal-update-sleep').modal('hide');
      });
    }
  
  })//end then
  .catch(function(error) {
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){
          $('#modal-update-sleep').modal('hide');
        });
      }else{
        _error("Por favor verifique los datos ingresados.").then(function(){
          $('#modal-update-sleep').modal('hide');
        });
      }
    }

    if (error.response.status == 500)
    {

      _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){
        $('#modal-update-sleep').modal('hide');
      });
      
    }     

    //handleErrorAxios(error);
  });
  return false;
};