userInSession();
App.setPageTitle('Sentinel - Cabeceras');

//$("#menu_Extremo > a ").css('cssText', "color: #FFF !important;");
$("#menu_fmm").addClass('active');
$("#menu_fmm_cabecera").addClass('active');
var oTable;
var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};


$(document).ready(function (){

  getPhysicalExtremes(detail=false);
  getPhysicalExtremes(detail=true);
  getManufacturers(detail=false);
  getManufacturers(detail=true);
  getCommonConectorTypes();

  url_dt = Django.datatable_cabecera_list;
  oTable = $('#table_cabecera').dataTable({
      "aaSorting":[[0,"desc"]],
      "language": {
      "sLengthMenu":     "Mostrar _MENU_ cabeceras",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando cabecera del _START_ al _END_ de un total de _TOTAL_ cabeceras",
      "sInfoEmpty":      "Mostrando cabecera del 0 al 0 de un total de 0 cabeceras",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ cabeceras)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "sProcessing": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },

      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
      "processing": true,
      "serverSide": true,
      "searchDelay": 400,
      "ajax": {
          "url": url_dt,
          "type": "POST",
          "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
      },
      "columnDefs": [
        {
          orderable: true,
          searchable: true,
          targets: [0,1,2,3,4,5]
        },
        {
          orderable: false,
          searchable: false,
          targets: [6]
        }
        ], 
  });
    
  oTable.on( 'order.dt', function (e, settings) {

    var order = oTable.api().order();
    $("#table_cabecera > thead > tr > th").css('color','black')
    $( "#table_cabecera > thead > tr > th:eq( "+order[0][0]+" )" )
    .css( "color", "#2271b3" );

  });

  // Error messages
  const customMessages = {
    valueMissing:    'Campo obligatorio',       // `required` attr
    rangeUnderflow: 'Debe ser un número mayor a cero'
  }

  function getCustomMessage (type, validity) {
    if (validity.typeMismatch) {
      return customMessages[`${type}Mismatch`]
    } else {
      for (const invalidKey in customMessages) {
        if (validity[invalidKey]) {
          return customMessages[invalidKey]
        }
      }
    }
  }

  var invalidClassName = 'invalid'
  var inputs = document.querySelectorAll('input, select, textarea')
  inputs.forEach(function (input) {
    // Each time the user types or submits, this will
    // check validity, and set a custom message if invalid.
    function checkValidity () {
      const message = input.validity.valid
        ? null
        : getCustomMessage(input.type, input.validity, customMessages)
      input.setCustomValidity(message || '')
      if (message!=null){
        input.classList.add(invalidClassName)
      }else{
        input.classList.remove(invalidClassName)
      }
      
    }
    input.addEventListener('input', checkValidity)
    input.addEventListener('invalid', checkValidity)

  })

  // Remove error messages when modal is closed
  $('#modal-create').on('hidden.bs.modal', function () {
    inputs.forEach(function (input) {
      input.classList.remove(invalidClassName)
    })
  })

  $('#modal-update').on('hidden.bs.modal', function () {
    inputs.forEach(function (input) {
      input.classList.remove(invalidClassName)
    })
  })

  // Dynamic model selector
  $(".manufacturer").change(function(){
    dynamicModelSelector(detail=false, $(this).val())
  });
  $(".det_manufacturer").change(function(){
    dynamicModelSelector(detail=true, $(this).val())
  });
});  

getPhysicalExtremes = (detail) => {  
  var url = DjangoURL('api_physicalextreme_list');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      if(detail){
        $("#det_physical_ext").append(`<option id="first" value="">Seleccione un extremo</option>`);
        for (var i = 0; i < res.data.length; i++) {
          $("#det_physical_ext").append(`<option value="${res.data[i].id}">${res.data[i].address}, ${res.data[i].commune}, ${res.data[i].location}</option>`);
        }
      }else{
        $("#physical_ext").append(`<option id="first-create" value="">Seleccione un extremo</option>`);
        for (var i = 0; i < res.data.length; i++) {
          $("#physical_ext").append(`<option value="${res.data[i].id}">${res.data[i].address}, ${res.data[i].commune}, ${res.data[i].location}</option>`);
        }
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

getManufacturers = (detail) => {  
  var url = DjangoURL('api_manufacturer_list');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      if(detail){
        $("#det_manufacturer").append(`<option id="first-manufacturer" value="">Seleccione un fabricante</option>`);
        for (var i = 0; i < res.data.length; i++) {
          $("#det_manufacturer").append(`<option value="${res.data[i].id}">${res.data[i].name}</option>`);
        }
      }else{
        $("#manufacturer").append(`<option id="first-create-manufacturer" value="">Seleccione un fabricante</option>`);
        for (var i = 0; i < res.data.length; i++) {
          $("#manufacturer").append(`<option value="${res.data[i].id}">${res.data[i].name}</option>`);
        }
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

getCommonConectorTypes = () => {  
  var url = DjangoURL('api_cabecera_list');
  axios.options(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      $("#common_connector_type").append(`<option id="first-create-connector" value="">Seleccione el más común</option>`);
      // Los choices estan actions > POST > common_conector_type > choices > value, display_name
      var length = res.data.actions.POST.common_connector_type.choices.length;
      for (var i = 0; i < length ; i++) {
        var value = res.data.actions.POST.common_connector_type.choices[i].value;
        var name = res.data.actions.POST.common_connector_type.choices[i].display_name;
        $("#common_connector_type").append(`<option value="${value}">${name}</option>`);
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

function dynamicModelSelector (detail, manufacturer) {
  var select;
  if(detail){
    select = $("#det_model");
  }else{
    select = $("#model");
  }
  $('option', select).remove();
  select.append(`<option id="first-create-model" value="">Seleccione el modelo</option>`);
  //Obtain models of manufacturer
  if(!detail){
    select.val("");
  }
  
  if(manufacturer!=""){
    var url = DjangoURL('api_manufacturer_models', manufacturer)
    axios.get(url, conf)
    .then(function(res){
      if (res.status == 200)
      {          
        for (var i = 0; i < res.data.length; i++) {
          // model_options[res.data[i].id]=res.data[i].name
          select.append(`<option value="${res.data[i].id}">${res.data[i].name}</option>`);
        }
      }
      if (res.status == 401)
      {
        handleUnauthorizedAxios();
      }
    })//end then
    .catch(function(error) {
        handleErrorAxios(error);
    });
  }
}

function newCabecera() {

  $('#modal-create').modal('hide');
  $('#modal-create-sleep').modal('show');
  var url = Django.FMM_URL + '/api/v1/cabecera/'
  var data;
  data = {
    "model": $("#model").val(),
    "manufacturer": $("#manufacturer").val(),
    "modules": $("#modules").val(),
    "ports_per_module": $("#ports_per_module").val(),
    "physical_ext": $("#physical_ext").val(),
    "common_connector_type": $("#common_connector_type").val(),
    "comment": $("#comment").val()
  }
  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 201)
    {
      // $('#modal-create-sleep').modal('hide');
      swal({
      title: "Creado",
        text: 'Cabecera agregada exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        updateDatatable();
        $('#modal-create-sleep').modal('hide');
      });
      $('option', $("#model")).remove();
      $("#model").append(`<option id="first-create-model" value="">Seleccione el modelo</option>`);
      $("#manufacturer").val('');
      $("#first-create-manufacturer").attr("selected","selected");
      $("#modules").val('');
      $("#ports_per_module").val('');
      $("#first-create").attr("selected","selected");
      $("#first-create-connector").attr("selected","selected");
      $("#comment").val('');
    }
  
  })//end then
  .catch(function(error) {
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }else{
        _error("Por favor verifique los datos ingresados.").then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }
    }

    if (error.response.status == 500)
    {

      _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){
        $('#modal-create-sleep').modal('hide');
      });
      
    }     
     
    //handleErrorAxios(error);
  });
  return false;
};

function getDetailsCabecera(id) {
  var url = DjangoURL('api_cabecera_detail', id)
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      $("#update-form").attr("onsubmit","return updateCabecera("+id+")");
      // First put options of manufacturer
      select = $("#det_model");
      $('option', select).remove();
      select.append(`<option value="" >Seleccione el modelo</option>`);

      var url = DjangoURL('api_manufacturer_models', res.data.manufacturer)
      axios.get(url, conf)
      .then(function(res_man){
        if (res_man.status == 200)
        {          
          for (var i = 0; i < res_man.data.length; i++) {
            if(res_man.data[i].id==res.data.model){
              select.append(`<option selected="selected" value="${res_man.data[i].id}">${res_man.data[i].name}</option>`);
            }else{
              select.append(`<option value="${res_man.data[i].id}">${res_man.data[i].name}</option>`);
            }            
          }
        }
        if (res_man.status == 401)
        {
          handleUnauthorizedAxios();
        }
      })//end then
      .catch(function(error) {
          handleErrorAxios(error);
      });
      $("#det_manufacturer > option").each(function() {
        if ($(this).val()==res.data.manufacturer)
        {
          $(this).attr("selected","selected");
        }
      })
      $("#det_modules").text(res.data.modules);
      $("#det_ports_per_module").text(res.data.ports_per_module);
      $("#det_comment").text(res.data.comment);
      if (res.data.physical_ext==null){
        $("#first").attr("selected","selected");
      }else{
        $("#det_physical_ext > option").each(function() {
          if ($(this).val()==res.data.physical_ext)
          {
            $(this).attr("selected","selected");
          }
        });
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
};

function updateCabecera(id) {

  $('#modal-update').modal('hide');
  $('#modal-create-sleep').modal('show');
  var url = DjangoURL('api_cabecera_detail', id)
  var data;
  data = { 
    "model": $("#det_model").val(),
    "manufacturer": $("#det_manufacturer").val(),
    "physical_ext": $("#det_physical_ext").val(),
    "comment": $("#det_comment").val()
  }
  axios.patch(url, data, conf)
  .then(function(res){
    if (res.status == 200)
    {
      // $('#modal-create-sleep').modal('hide');
      swal({
      title: "Moficado",
        text: 'Cabecera modificada exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        updateDatatable();
        $('#modal-create-sleep').modal('hide');
      });
    }
  
  })//end then
  .catch(function(error) {
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }else{
        _error("Por favor verifique los datos ingresados.").then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }
    }

    if (error.response.status == 500)
    {

      _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){
        $('#modal-create-sleep').modal('hide');
      });
      
    }
    //handleErrorAxios(error);
  });
  return false;
};

delete_cabecera = (id) => {
  var url = Django.FMM_URL + '/api/v1/cabecera/' + id + '/';
  delete_object(null, 'Cabecera', url, null, function(){
    oTable.api().ajax.reload(null, false);
  });
}


ayuda = () => {
  alert('Pagina de ayuda de fmm')
}