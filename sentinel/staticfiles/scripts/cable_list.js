userInSession();
App.setPageTitle('Sentinel - Cables');

//$("#menu_Extremo > a ").css('cssText', "color: #FFF !important;");
$("#menu_fmm").addClass('active');
$("#menu_fmm_cable").addClass('active');
var oTable;
var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};


$(document).ready(function (){

  getPhysicalExtremes(detail=false);
  getPhysicalExtremes(detail=true);
  getTerminations(detail=false);
  getTerminations(detail=true);
  getJacketTypes(detail=false);
  getJacketTypes(detail=true);

  url_dt = Django.datatable_cable_list;
  oTable = $('#table_cable').dataTable({
      "aaSorting":[[0,"desc"]],
      "language": {
      "sLengthMenu":     "Mostrar _MENU_ cables",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando cable del _START_ al _END_ de un total de _TOTAL_ cables",
      "sInfoEmpty":      "Mostrando cable del 0 al 0 de un total de 0 cables",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ cables)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "sProcessing": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },

      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
      "processing": true,
      "serverSide": true,
      "searchDelay": 400,
      "ajax": {
          "url": url_dt,
          "type": "POST",
          "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
      },
      "columnDefs": [
        {
          orderable: true,
          searchable: true,
          targets: [0,1,2,3,4,5,6,7,8,9,10,11]
        },
        {
          orderable: false,
          searchable: false,
          targets: [12]
        }
        ], 
  });
    
      oTable.on( 'order.dt', function (e, settings) {
    
        var order = oTable.api().order();
        $("#table_cable > thead > tr > th").css('color','black')
        $( "#table_cable > thead > tr > th:eq( "+order[0][0]+" )" )
        .css( "color", "#2271b3" );
    
     });
});  

getPhysicalExtremes = (detail) => {  
  var url = DjangoURL('api_physicalextreme_list');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      if(detail){
        $("#det_physical_ext_A").append(`<option id="first" value="">Seleccione un extremo A</option>`);
        $("#det_physical_ext_B").append(`<option id="first" value="">Seleccione un extremo B</option>`);
        for (var i = 0; i < res.data.length; i++) {
          $("#det_physical_ext_A").append(`<option value="${res.data[i].id}">${res.data[i].address}, ${res.data[i].commune}, ${res.data[i].location}</option>`);
          $("#det_physical_ext_B").append(`<option value="${res.data[i].id}">${res.data[i].address}, ${res.data[i].commune}, ${res.data[i].location}</option>`);
        }
      }else{
        $("#physical_ext_A").append(`<option value="">Seleccione un extremo A</option>`);
        $("#physical_ext_B").append(`<option value="">Seleccione un extremo B</option>`);
        for (var i = 0; i < res.data.length; i++) {
          $("#physical_ext_A").append(`<option value="${res.data[i].id}">${res.data[i].address}, ${res.data[i].commune}, ${res.data[i].location}</option>`);
          $("#physical_ext_B").append(`<option value="${res.data[i].id}">${res.data[i].address}, ${res.data[i].commune}, ${res.data[i].location}</option>`);
        }
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

getTerminations = (detail) => {  
  var url = DjangoURL('api_termination_list');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      if(detail){
        $("#det_termination_A").append(`<option id="first" value="">Seleccione una terminación A</option>`);
        $("#det_termination_B").append(`<option id="first" value="">Seleccione una terminación B</option>`);
        for (var i = 0; i < res.data.length; i++) {
          $("#det_termination_A").append(`<option value="${res.data[i].id}">${res.data[i].termination_type} en ${res.data[i].physical_ext}</option>`);
          $("#det_termination_B").append(`<option value="${res.data[i].id}">${res.data[i].termination_type} en ${res.data[i].physical_ext}</option>`);
        }
      }else{
        $("#termination_A").append(`<option value="">Seleccione una terminación A</option>`);
        $("#termination_B").append(`<option value="">Seleccione una terminación B</option>`);
        for (var i = 0; i < res.data.length; i++) {
          $("#termination_A").append(`<option value="${res.data[i].id}">${res.data[i].termination_type} en ${res.data[i].physical_ext}</option>`);
          $("#termination_B").append(`<option value="${res.data[i].id}">${res.data[i].termination_type} en ${res.data[i].physical_ext}</option>`);
        }
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

// Falta standard y filaments
getJacketTypes = (detail) => {  
  var url = DjangoURL('api_cable_list');
  axios.options(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      if(detail){
        $("#det_jacket_type").append(`<option id="first" value="">Seleccione un tipo de chaqueta</option>`);
        var length = res.data.actions.POST.jacket_type.choices.length;
        for (var i = 0; i < length ; i++) {
          var value = res.data.actions.POST.jacket_type.choices[i].value;
          var name = res.data.actions.POST.jacket_type.choices[i].display_name;
          $("#det_jacket_type").append(`<option value="${value}">${name}</option>`);
        }
      }else{
        $("#jacket_type").append(`<option value="">Seleccione un tipo de chaqueta</option>`);
        var length = res.data.actions.POST.jacket_type.choices.length;
        for (var i = 0; i < length ; i++) {
          var value = res.data.actions.POST.jacket_type.choices[i].value;
          var name = res.data.actions.POST.jacket_type.choices[i].display_name;
          $("#jacket_type").append(`<option value="${value}">${name}</option>`);
        }
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

function newCable() {

  $('#modal-create').modal('hide');
  $('#modal-create-sleep').modal('show');
  var url = Django.FMM_URL + '/api/v1/cable/'
  var data= {
    "cable_data": $("#cable_data").val(),
    "installation_date": $("#installation_date").val(),
    "jacket_type": $("#jacket_type").val(),
    "standard": $("#standard").val(),
    "filaments": $("#filaments").val(),
    "minitubes": $("#minitubes").val(),
    "filaments_per_minitube": $("#filaments_per_minitube").val(),
    "comment": $("#comment").val()
  }
  if ($('#physical_ext_A').val()!=""){
    data['physical_ext_A'] = $('#physical_ext_A').val();
  }
  if ($('#termination_A').val()!=""){
    data['termination_A'] = $('#termination_A').val();
  }
  if ($('#physical_ext_B').val()!=""){
    data['physical_ext_B'] = $('#physical_ext_B').val();
  }
  if ($('#termination_B').val()!=""){
    data['termination_B'] = $('#termination_B').val();
  }
  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 201)
    {
      // $('#modal-create-sleep').modal('hide');
      swal({
      title: "Creado",
        text: 'Cable agregado exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        updateDatatable();
        $('#modal-create-sleep').modal('hide');
      });
    }
  
  })//end then
  .catch(function(error) {
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }else{
        _error("Por favor verifique los datos ingresados.").then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }
    }

    if (error.response.status == 500)
    {

      _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){
        $('#modal-create-sleep').modal('hide');
      });
      
    }     
     
    //handleErrorAxios(error);
  });
  
};

function getDetailsCabecera(id) {
  var url = DjangoURL('api_cabecera_detail', id)
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      $("#btn_update").attr("onclick","updateCabecera("+id+")");
      $("#det_model").val(res.data.model);
      var url = DjangoURL('api_manufacturer_detail', res.data.manufacturer)
      axios.get(url, conf)
      .then(function(res){
        if (res.status == 200)
        {
          $("#det_manufacturer > option").each(function() {
            if ($(this).val()==res.data.id)
            {
              $(this).attr("selected","selected");
            }
          });
        }
        if (res.status == 401)
        {
          handleUnauthorizedAxios();
        }
      })//end then
      .catch(function(error) {
          handleErrorAxios(error);
      });        
      $("#det_modules").text(res.data.modules);
      $("#det_ports_per_module").text(res.data.ports_per_module);
      $("#det_comment").text(res.data.comment);
      if (res.data.physical_ext==null){
        $("#first").attr("selected","selected");
      }else{
        var url = DjangoURL('api_physicalextreme_detail', res.data.physical_ext)
        axios.get(url, conf)
        .then(function(res){
          if (res.status == 200)
          {
            $("#det_physical_ext > option").each(function() {
              if ($(this).val()==res.data.id)
              {
                $(this).attr("selected","selected");
              }
            });
          }
          if (res.status == 401)
          {
            handleUnauthorizedAxios();
          }
        })//end then
        .catch(function(error) {
            handleErrorAxios(error);
        });
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

function updateCabecera(id) {

  $('#modal-update').modal('hide');
  $('#modal-create-sleep').modal('show');
  var url = DjangoURL('api_cabecera_detail', id)
  var data;
  if ($('#det_physical_ext').val()!=""){
    data = { 
      "model": $("#det_model").val(),
      "manufacturer": $("#det_manufacturer").val(),
      "physical_ext": $("#det_physical_ext").val(),
      "comment": $("#det_comment").val()
    }
  }else{
    data = { 
      "model": $("#det_model").val(),
      "manufacturer": $("#det_manufacturer").val(),
      "comment": $("#det_comment").val()
    }
  }
  axios.patch(url, data, conf)
  .then(function(res){
    if (res.status == 200)
    {
      // $('#modal-create-sleep').modal('hide');
      swal({
      title: "Moficado",
        text: 'Cabecera modificada exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        updateDatatable();
        $('#modal-create-sleep').modal('hide');
      });
    }
  
  })//end then
  .catch(function(error) {
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }else{
        _error("Por favor verifique los datos ingresados.").then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }
    }

    if (error.response.status == 500)
    {

      _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){
        $('#modal-create-sleep').modal('hide');
      });
      
    }
    //handleErrorAxios(error);
  });
  
};

delete_cable = (id) => {
  var url = Django.FMM_URL + '/api/v1/cable/' + id + '/';
  delete_object(null, 'Cable', url, null, function(){
    oTable.api().ajax.reload(null, false);
  });
}


ayuda = () => {
  alert('Pagina de ayuda de fmm')
}