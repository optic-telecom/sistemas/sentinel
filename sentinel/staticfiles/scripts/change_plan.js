userInSession();
App.setPageTitle('Sentinel - Cambiar Plan');

//$("#menu_olt > a ").css('cssText', "color: #FFF !important;");
$("#menu_olt").addClass('active');
$("#menu_olt_cambiar_plan").addClass('active');




$( "#select_services" ).autocomplete({
    source: function( request, response ) {
      $.ajax( {
        url: "/api/v1/olt/?alias__icontains="+request.term+"&fields=alias,uuid",
        beforeSend: function (xhr) {
          xhr.setRequestHeader ("Authorization", "jwt " + HDD.get(Django.name_jwt));
        },
        data: {
          term: request.term
        },
        success: function( data ) {
          if(!data.length){
            var result = [
            {
              label: 'Sin resultados.', 
              value: response.term
            }];
            response(result);
          }
          else
          {
            res = $.map(data, function(item) {
                  return {
                    label: item.alias,
                    value: item.alias,
                    uuid: item.uuid
                  }
            });
            response(res);
          }
        }
      });
    },
    minLength: 2,
    select: function( event, ui ) {
      findONUs(ui.item.uuid)
    }
})





