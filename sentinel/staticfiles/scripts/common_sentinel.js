//* localStorage *//
var HDD = {
    get: function(key) {
        return localStorage.getItem(key);
    },
    get_default: function(key, value_default) {
    	try{
    		return localStorage.getItem(key);
    	}
    	catch(err){
    		return value_default;
    	}
    },
    set: function(key, val) {
        return localStorage.setItem(key, val);
    },
    unset: function(key) {
        return localStorage.removeItem(key);
    },
    setJ:function(key,val)
    {
        return localStorage.setItem(key, JSON.stringify(val));
    },
    getJ:function(key)
    {
        return JSON.parse(localStorage.getItem(key));
    }
};

var HSD = {
    get: function(key) {
        return sessionStorage.getItem(key);
    },
    get_default: function(key, value_default) {
      try{
        return sessionStorage.getItem(key);
      }
      catch(err){
        return value_default;
      }
    },
    set: function(key, val) {
        return sessionStorage.setItem(key, val);
    },
    unset: function(key) {
        return sessionStorage.removeItem(key);
    },
    setJ:function(key,val)
    {
        return sessionStorage.setItem(key, JSON.stringify(val));
    },
    getJ:function(key)
    {
        return JSON.parse(sessionStorage.getItem(key));
    }
};



function DjangoURL(url, id='', version='v1') {
  return Django[url].replace(':val:',id).replace('v1',version);
}


//* session *//
function sessionExit(redirect=true) {

    HDD.unset('username');
    HDD.unset(Django.name_jwt);
    
/*    for (var i = 0; i < localStorage.length; i++) {
        HDD.unset(localStorage.key(i)); 
    }

    for(var x = 0; x <= localStorage.length; x++) {
        localStorage.removeItem(localStorage.key(x))
    }*/

    Cookies.remove(Django.name_jwt)

	if (redirect)
	{
		setTimeout(function(){ location.href = '/'; }, 40);
	}
};

function invalidToken() {
    HDD.set('last_url', window.location.pathname);
    sessionExit(true);
};


function handleErrorAxios(error) {
    // console.info(error);
    if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        // console.log(error.response.data);

        if (error.response.status == 400)
        {
          if (error.response.data.error){
            _error(""+ error.response.data.error).then(function(){});
          }else{
            _error("Por favor verifique los datos ingresados.");
          }
        }
        else if (error.response.status == 401)
        {
          handleUnauthorizedAxios();
        }
        else if (error.response.status == 404)
        {
          console.log(error)
          console.log("error 404 ", error.request.responseURL)
        }
        else if(error.response.status == 500){
            _error('Hubo un error en el sistema. Contacte al administrador.')
            .then(function() {});
        }        
        else{
          console.info("Response:");
          console.log(error.response);
          // console.log(error.response.headers);
        }

    } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.info("Request");
        console.log(error.request);
        console.log(error)
    } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error message: ', error.message);
        console.log('Error stack: ', error.stack);
    }
    //console.log(error.config);
}

function handleUnauthorizedAxios() {
  _error_redirect("Se vencio su session, porfavor ingrese nuevamente",
  '/','Sesion expirada');
}

function handleTooManyRequestsAxios()
{
  console.log("Too many requests ");
}

function verifyJwtToken() {
  axios.post('/api/v1/auth/verify_jwt_token/', {token: HDD.get(Django.name_jwt)})
  .then(function(response){
    if (response.status != 200)
    {
      sessionExit(true);
    }
  })
  .catch(function(error) {
      sessionExit(true);
  });    
}

function refreshJwtToken() {
    var conf = {
        headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
    };

    axios.post('/api/v1/auth/refresh_token/', {token: HDD.get(Django.name_jwt)}, conf)
    .then(function(response){
      if (response.status == 200)
      {
        HDD.set(Django.name_jwt,response.data.token);
        Cookies.set(Django.name_jwt, response.data.token);
      }
    })
    .catch(function(error) {
        sessionExit(true);
    });

}

function userInSession(redirect=true) {

    if (HDD.get(Django.name_jwt) == null)
    {
      sessionExit(redirect);
      return;
    }
    unmark_li();
    if (HDD.get('username') == null)
    {
      var conf = {
          headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
      };
  
      axios.get(Django.api_user, conf)
      .then(function(response){
        if (response.status == 200)
        {
          HDD.set('username', response.data.username);
          $(".text_username").text(HDD.get('username'));
        }
      })
      .catch(function(error) {
          handleErrorAxios(error);
      });

    }
    else
    {
      $(".text_username").text(HDD.get('username'));
    }
    refreshJwtToken();
}


//* alerts sweetalert *//
function _info(textMessage) {
    swal({
        title: "",
        text: textMessage,
        icon: "info",
        buttons: false,
    });
};

function _error(textMessage) {
    noti = swal({
        title: "Error",
        text: textMessage,
        icon: "error",
        buttons: false,
    });
    return noti
};

function _error_redirect(textMessage,redirect,title='Error') {
    swal({  
        title: title,
        text: textMessage,
        icon: "error",
        showCancelButton: false,
        confirmButtonColor: "#23c6c8",
        confirmButtonText: "Cerrar",
        closeOnConfirm: false
    })
    .then(function() {
        location.href = redirect;
    });
};

function _exito(textMessage) {
	swal({
	title: "Información",
		text: textMessage,
		icon: "success",
    buttons: false
	});
};

function _exito_redirect(textMessage,dire) {
	swal({
	title: "Información",
		text: textMessage,
		icon: "success",
    buttons: false
	}).then(function() {
		location.href = dire;
	});
};

function _confirma(textMessage, yes, not) {

  swal({
      title: "Información",
      text: textMessage,
      icon: "warning",
      buttons: [
        'No',
        'Si'
      ],
      dangerMode: true,
    }).then(function(isConfirm) {
      if (isConfirm) {
        yes()
      } else {
        not()
      }
    })

}

function comments() {

  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };

  /// post save
  var url = DjangoURL('feedback_api');
  var commentary_input = document.createElement("textarea");
  commentary_input.classList.add('form-control');
  commentary_input.id = 'commentary_input';
  commentary_input.maxLength = "200";

  swal({
    text: 'Cuéntanos cómo mejorarías Sentinel',
    content: commentary_input,
    buttons: {
      cancel: "Luego",
      comentar: {
        text: "Comentar!",
      }
    },
  })
  .then(commentary => {
    if (!commentary) throw null;
  
    var data = {
      width:$(window).width(),
      height:$(window).height(),
      platform:navigator.platform,
      user_agent:navigator.userAgent,
      cookie_enabled:navigator.cookieEnabled,
      url:location.href,
      commentary:$("#commentary_input").val()
    };

    axios.post(url, data, conf)
    .then(function(res, conf, data){
      if (res.status == 201)
      {
        const movie = res.data;
        swal({
          title: "Exito",
          text: 'Su comentario fue guardado exitosamente. Gracias por su comentario.',
        });
      }
      if (res.status == 401)
      {
        handleUnauthorizedAxios();
      }
    })//end then
    .catch(err => {
      if (err) {
        swal("Oh no!", "Esto es vergonzoso, hubo un error, puedes intentarlo de nuevo?", "error");
      } else {
        swal.stopLoading();
        swal.close();
      }
    });

  })

}

//* functions  *//
function templater ( strings, ...keys ) {
  return function( data ) {
      let temp = strings.slice();
      keys.forEach( ( key, i ) => {
        if (data) {
          if(typeof(data[ key ]) !== 'undefined' &&
             data[ key ] !== null)
          {
            temp[ i ] = temp[ i ] + data[ key ];
          }
         }
      });
      return temp.join( '' );
  }
};

function extend(obj, src) {
  // retorna un nuevo objeto
  var object = {};
  var object = $.extend(object, obj, src);
  return object
}

function mergeObject(obj, src) {
  // afecta al objeto inicial
  Object.keys(src).forEach(function(key) { obj[key] = src[key]; });
  return obj;
}

function getUrlLastParameter(excludeParams=true) {
  var parts = location.href.split('/');
  var lastSegment = parts.pop() || parts.pop();

  if (excludeParams)
  {
    if (lastSegment.startsWith('?')){
      return parts.pop();
    }
    else
    {
      return lastSegment;
    }

  }
  else
  {
    return lastSegment;  
  }
}

function getParameterUrl(name, url) {
  if (!url) url = location.href;
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( url );
  return results == null ? null : results[1];
};


function updateDatatable(selector=null) {
  var target = selector ? selector : '.table';
  if ( $.fn.dataTable.isDataTable(target) ) {
    var table = $(target).DataTable();
    table.ajax.reload();
    //temp_table.ajax.url( temp_table.ajax.url() ).load();
  }
}

function formatMac(mac) {
  if (mac){
    var newMac = '';
    for (var i = 0; i < mac.length; i++) {
      newMac += mac[i];
      residuo = i % 2;
      if (residuo && (i != (mac.length - 1)))
      {
        newMac +=':'
      }
      
    }
    return newMac;
  }
  return null;
}

function openAccordion(self) {
  var content = $(self).next(".accordion-content");
  if(content.css("display") == "none"){//open     
      $(self).addClass("open");
      content.slideDown(250); 

      if ($(self).attr('data-event')){
          window[$(self).attr('data-event')]()
      }

  }
  else{ //close    
      $(self).removeClass("open");  
      content.slideUp(250);
  }
}



function openPanel(id, panel_type) {
  var targetContainer = '.theme-panel';
  var targetClass = 'active';
  if ($(targetContainer).hasClass(targetClass)) {

      if (HDD.get("panel_open") == null)
      {
          HDD.set("panel_open", id);
      }

      if(HDD.get("panel_open") == id)
      {
          $(document).trigger('closed_panel');
          HDD.set('panel_page','0');
          $(targetContainer).removeClass(targetClass);
      }
  }
  else
  {
      $(document).trigger('open_panel');
      HDD.set('panel_page','1');
      $(targetContainer).addClass(targetClass);
      if (panel_type == 'onu') {
        panel_onu_content('#panel_right_content',HDD.get("panel_open"));
      }
      if (panel_type == 'fsp'){
        panel_fsp_content('#panel_right_content',HDD.get("panel_open"));
      }
      if (panel_type == 'cpeutp'){
        panel_utpcpe_content('#panel_right_content',HDD.get("panel_open"));        
      }
      
      if (panel_type == 'setupbox'){
        panel_tv_content('#panel_right_content',HDD.get("panel_open"));
      }
      
  }
  
  HDD.set("panel_open", id);

}

function openPanelLastID(){
  if (HDD.get("panel_type") == 'onu')
  {
    openPanel(HDD.get("panel_open"),'onu');
  }
  if (HDD.get("panel_type") == 'fsp')
  {
    openPanel(HDD.get("panel_open"),'fsp');
  }
  if (HDD.get("panel_type") == 'cpeutp')
  {
    openPanel(HDD.get("panel_open"),'cpeutp');
  }  
}

function delete_object(id, obj, url=null, message=null, call=null) {
  message = message || '¿Desea eliminar este elemento?';
  url = url || `/api/v1/${obj}/${id}/`;

  swal({
      title: "Eliminar",
      text: message,
      icon: "warning",
      buttons: [
        'No',
        'Si'
      ],
      dangerMode: true,
    }).then(function(isConfirm) {
      if (isConfirm) {
        axios.delete(url, {headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}})
        .then(function(response){
          if (response.status == 204)
          {
            _exito(obj + ' borrado exitosamente.');
            if (call)
            {
              call();
            }
          }
        })
        .catch(function(error) {
            if (error.response.data.error){
              _error(error.response.data.error).then(function() {});
            }          
        });
      }
    })
}

function asynchronousTask(data, call=null) {

  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };
  
  var url = DjangoURL('api_task');
  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 201)
    {
      $.gritter.add({
        title: 'Tarea ejecutandose',
        time: 1000,
      });
      if (call)
      {
        call();
      }      
    }
  
  })//end then
  .catch(function(error) {
    if(error.response.status == 500){
      if (error.response.data.error){
        _error(error.response.data.error).then(function() {});
      }
    }else{
      handleErrorAxios(error);
    }     

  });
}


function appendScript(url){
   let script = document.createElement("script");
   script.src = url;
   script.async = false; //IMPORTANT
   document.getElementById("insert_script_here").appendChild(script);
}

function permite(elEvento, permitidos) {
    // Variables que definen los caracteres permitidos
    var numeros = "0123456789";
    var numeros_cifras = "0123456789,";
    var numeros_cifras_p = "0123456789,.";
    var numeros_tlf = " 0123456789-/";
    var caracteres = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZáéíóúÁÉÍÓÚ_-.,:@";
    var caracteres_s = "abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZáéíóúÁÉÍÓÚ";
    var numeros_caracteres = numeros + caracteres;
    var teclas_especiales = [8, 9, 13, 16, 18, 39];
   
    // Seleccionar los caracteres a partir del parámetro de la función
    switch(permitidos) {
        case 'num':
            permitidos = numeros;
            break;
        case 'num_cifras':
            permitidos = numeros_cifras;
            break;
        case 'num_cifras_p':
            permitidos = numeros_cifras_p;
            break;
        case 'num_tlf':
            permitidos = numeros_tlf;
            break;
        case 'car':
            permitidos = caracteres;
            break;
        case 'car_s':
            permitidos = caracteres_s;
            break;
        case 'num_car':
            permitidos = numeros_caracteres;
            break;
    }
   
    // Obtener la tecla pulsada 
    var evento = elEvento || window.event;
    var codigoCaracter = evento.charCode || evento.keyCode;
    var caracter = String.fromCharCode(codigoCaracter);
   
    // Comprobar si la tecla pulsada es alguna de las teclas especiales
    // (teclas de borrado y flechas horizontales)
    var tecla_especial = false;
    for(var i in teclas_especiales) {
        if(codigoCaracter == teclas_especiales[i]) {
            tecla_especial = true;
            break;
        }
    }
   
    // Comprobar si la tecla pulsada se encuentra en los caracteres permitidos o si es una tecla especial
    return permitidos.indexOf(caracter) != -1 || tecla_especial;
}

function unmark_li(){
  $('ul li').each(function(i)
    {
      $(this).removeClass('active');
    });
}

//* events *//
$("#btn_log_out").on('click',function(){
    sessionExit(true);
})

$(document).ready(function() {
    var tab_selected = getParameterUrl('tab');

    // select tab
    if (tab_selected)
    {
        $('a[href="#tab-'+ tab_selected +'"').click();
    }

    // open panel LastID
    if (!(HDD.get("panel_page") == null) && (HDD.get("panel_page") == 1))
    {
      openPanelLastID();
    }

    // unmark li 
    unmark_li();

    
   

});

$(document).on('click', '[data-click="panel-lateral"]', function(e) {

  var id = e.target.dataset.id;
  var nameFunc = e.target.dataset.panel;// $(this).data('panel');
  //ADICIONADAS PARA RENDERIZAR LOS GRAFICOS
  var serial = e.target.dataset.serial;
  var ip = e.target.dataset.ip;

  HDD.set('serialGraph',serial);
  HDD.set('ip_address',ip);
  //call function
 
  switch(nameFunc){
    case 'panel_fsp_content':
      panel_fsp_content('#panel_right_content', id)
    break;
    case 'panel_onu_content':
      panel_onu_content('#panel_right_content', id);
    break;
    case 'panel_utpcpe_content':
      panel_utpcpe_content('#panel_right_content', id);
    break;

    case 'panel_tv_content':
      panel_tv_content('#panel_right_content', id);
    break;
  }

  // window[nameFunc]('#panel_right_content', id);

  openPanel(id);

});
