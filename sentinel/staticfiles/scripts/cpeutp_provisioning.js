userInSession();
App.setPageTitle('Sentinel - Provisionar CPEs UTP');

//$("#menu_olt > a ").css('cssText', "color: #FFF !important;");
$("#menu_utp").addClass('active');
$("#menu_utp_provisionar_cpe").addClass('active');

var lastServiceNumber;

var CpeUtpTemplate = templater`
<div class="fade-in divParent ${'class_parent'}">
  <b class="text-white f-s-14 f-w-700">${'title'}</b>
  <div class="${'class_child'}">

    <table style="width: 90%; margin-left: auto; margin-right: auto;" cellpadding="2" class="f-s-13" border="0">
    <tbody>
    <tr>
    <td style="width: 11%;" class="f-w-700">Numero de serie:</td>
    <td style="width: 11%;" class="f-w-700">${'serial'}</td>
    <td style="width: 13%;" class="f-w-700">UTP:</td>
    <td style="width: 18%;" class="f-w-700">${'utp'}</td>
    <td style="width: 22%;">&nbsp;</td>
    <td rowspan="3">
    <button class="btn btn-${'color_btn'}" onclick="provisioningUTP('${'uuid'}')" type="button">Asociar a Servicio</button></td>
    </tr>
    <tr>
    <td class="f-w-700">Modelo</td>
    <td class="f-w-700">${'model'}</td>
    <td>F/S/P:</td>
    <td>${'fsp'}</td>
    </tr>
    <tr>
    <td class="f-w-700">${'service_number_text'}</td>
    <td class="f-w-700">${'service_number'}</td>
    <td>Descripción del puerto:</td>
    <td>${'description'}</td>
    </tr>
    </tbody>
    </table>
  </div>
</div>`;

var ServiceNumberTemplate = templater`
  <hr style="background: #e2e7eb; width: 100%">
  <div class="divParent successParent" style="margin-right: 8px; margin-left: 8px; width:100%;">
    <b class="text-white f-s-14 f-w-700">&nbsp;Número de servicio</b>
    <div class="successChild">

      <table style="width: 90%; margin-left: auto; margin-right: auto;" cellpadding="2" class="f-s-13" border="0">
      <tbody>

      <tr>
        <td>Rut cliente</td>
        <td>${'rut'}</td>
      </tr>

      <tr>
        <td>Dirección de servicio</td>
        <td>${'composite_address'}</td>
      </tr>

      <tr>
        <td>Comuna</td>
        <td>${'commune'}</td> 
      </tr>

      <tr><td>&nbsp;</td>
      </tr>

      <tr>
        <td class="f-w-700">Datos del servicio</td>
      </tr>

      <tr>
        <td>Plan contratado</td>
        <td title="${'plan_costo'}">${'plan'}</td>
      </tr>


      <tr>
        <td>Nodo instalación</td>
        <td>${'code'}</td>
      </tr>

      <tr>
        <td>Estado actual servicio</td>
        <td>${'get_status_display'}</td>
      </tr>
      </tbody>
      </table>

      <br>

      <table style="width: 90%; margin-left: auto; margin-right: auto;">
      <tbody>
      <tr>
        <td width="70%">
          ${'text_alert'}
        </td>
        <td>&nbsp;
        <button onclick="associate('${'service'}','${'uuid'}','${'previus'}','${'sentinel_plan'}','${'code'}',
        '${'apartment_number'}','${'tower'}','${'client'}','${'node_address'}','${'composite_address'}')" class="btn btn-green" type="button">Asociar a Servicio</button></td></td>
      </tr>
      </tbody>
      </table>
    </div>
  </div>`;

var ServiceNumberTemplateNoValid = templater`
<div class="alertChild">${'text_alert'}</div>`

function findCPEs(uuid=null) {

  $('#modal-create-sleep').modal('show');
  var url = DjangoURL('api_provisioning_olt_list')
  if (uuid){
    var conf_header = {
      headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)},
      params: {
        uuid: uuid
      }    
    };
  }else{
    var conf_header = {
      headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)},
    };    
  }


  axios.get(url, conf_header)
  .then(function(res){

    if (res.status == 200)
    {
      $("#content-cpesutp").html(" ");
      $('#modal-create-sleep').modal('hide');
      for (var i = 0; i < res.data.length; i++) {

          var previus = res.data[i].in_previus_utp;
          $("#content-cpesutp").append(OnuTemplate({
            title:previus==0 ? 'CPEUTP disponible para ser provisionada' : 'CPEUTP ya asociada' ,
            class_parent:previus==0 ? 'successParent':'alertParent',
            class_child:previus==0 ? 'successChild':'alertChild',
            color_btn:previus==0 ? 'green':'red',
            serial:res.data[i].serial,
            nodo_utp:res.data[i].nodo_utp,
            model:res.data[i].model,
            uuid:res.data[i].uuid,
            description:res.data[i].description,
            service_number_text: previus==0 ? '':'Numero de servicio',
            service_number:previus==0 ? '':res.data[i].service_number
          }));

      }

    }
  
  })//end then
  .catch(function(error) {
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        $('#modal-create-sleep').modal('hide');
        _error(""+ error.response.data.error).then(function(){
          
        });
      }else{
        _error("Por favor verifique los datos ingresados.").then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }
      return
    }

    if (error.response.status == 500)
    {

      _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){
        $('#modal-create-sleep').modal('hide');
      });
      return
    }     
     
    handleErrorAxios(error);
  });
  
};


function provisioningUTP(id) {
  var url = '/devices/provisioning/utp/'+id+'/';
  //$('#myModal').modal('show').find('.modal-content').load(url);

  // reset modal if it isn't visible
/*  if (!($('.modal.in').length)) {
    $('.modal-dialog').css({
      top: '140px',
      left: '450px'
    });
  }*/

  $('#myModal').modal({
    backdrop: false,
    show: true
  }).find('.modal-content').load(url);;

  //$('.modal-dialog-draggable').draggable({
  //  handle: ".modal-header"
  //});

}


function findServiceNumber(id){

  var serviceNumber = $("#service_number_search").val();

  if (serviceNumber == ''){
    _error('El número de servicio no puede estar en blanco.')
    return false;
  }

  if (false){
    if (lastServiceNumber == serviceNumber)
    {
      _error('Acaba de consultar este mismo Numero');
      return false;
    }
  }

  lastServiceNumber = serviceNumber;

  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)},
    params: {
      uuid: id
    }    
  };

  $("#data_service_number").html(`<center><img src="/static/img/loading_spinner.gif" width="100px"
    heigh="100px"></img></center>`);
  
  var url = DjangoURL('api_matrix_service_number_detail',
                      serviceNumber);
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      if (res.data.error){
          $("#data_service_number").html('<hr style="background: #e2e7eb; width: 100%">\
          <div class="text-danger">&nbsp;'+res.data.error+'</div>');
      }
      else
      {
        var text_alert_html = '<span class="text-danger"><span class="f-w-700">ADVERTENCIA:</span>\
        Este servicio ya tiene una CPE de la red UTP asignada, si continua con la asociación la CPE anterior será \
        eliminada de la UTP y dicho equipo quedará sin conexión. </span>';

        var previus = res.data.in_previus_olt;
        $("#data_service_number").html(ServiceNumberTemplate({
          text_alert : previus==0 ? '': text_alert_html,
          rut : res.data.client.rut,
          composite_address : res.data.composite_address,
          commune: res.data.client.commune,
          code: res.data.node.code,
          node_address: res.data.node.street + ' ' + res.data.node.house_number,
          apartment_number: res.data.client.apartment_number,
          tower: res.data.client.tower,
          client: res.data.client.name,
          get_status_display : res.data.get_status_display,
          service : res.data.service,
          uuid : res.data.uuid,
          plan: res.data.plan.name + ' - ' + res.data.plan.price,
          plan_costo: res.data.plan.price,
          previus: res.data.in_previus_olt,
          sentinel_plan:res.data.sentinel_plan
        }));

      }

    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      $("#data_service_number").html(' ');
      handleErrorAxios(error);
  });

}


function associate(service, id, previus, sentinel_plan,code,
                   apartment_number, tower, client, node_address, service_address) {

  $('#myModal').modal('hide');
  if (previus != 0){
    swal({
        title: "Aprovisionar",
        text: `¿Realmente esta seguro de proceder?
               Eliminará el servicio de la CPE anterior`,
        icon: "warning",
        buttons: [
          'No',
          'Si'
        ],
        dangerMode: true,
      }).then(function(isConfirm) {
        if (isConfirm) {
          associatePerform(service, id, sentinel_plan, code, apartment_number, tower, client, node_address, service_address)
        }
    })
  }else{
    associatePerform(service, id, sentinel_plan, code, apartment_number, tower, client, node_address, service_address)
  }

}

function associatePerform(service, id, sentinel_plan, code, apartment_number, tower, client, node_address, service_address) {
  
  $('#modal-create-sleep').modal('show');
  var url = DjangoURL('api_provisioning_perform_onu');
  //setTimeout(function(){ alert("Hello"); }, 3000);
  var data_send = {
    "onu":id,
    "service":service,
    'sentinel_plan':sentinel_plan,
    'code':code,
    'apartment_number':apartment_number,
    'tower':tower,
    'client':client,
    'node_address':node_address,
    'service_address':service_address
  }

  axios.post(url, data_send, header_common)
  .then(function(res){
    if (res.status == 201)
    {

      // Se borra la(s) onu(s) del fondo en la pantallla
      $("#content-onus").html(' ')

      if (res.data.message){
        var message_ = res.data.message;
      }
      else
      {
        var message_ = 'Onu aprovisionada satisfactoriamente';
      }
      

      $('#modal-create-sleep').modal('hide');
      if (res.data.ip)
      {
          swal({
            title: "Provisionado, configure wifi",
            text: message_,
            icon: "success",
            buttons: {
              cancel: true,
              confirm: "OK",
              roll: {
                text: "Configurar",
                value: "wifi",
              },
            },
          }).then(function(confirm) {
            if(confirm == 'wifi'){
              window.open('http://'+res.data.ip+'/');
            }

          });

      }
      else
      {
          swal({
            title: "Provisionado",
            text: message_,
            icon: "success",
            buttons: false
          })     
      }

    }
  
  })//end then
  .catch(function(error) {
    $('#modal-create-sleep').modal('hide');
    handleErrorAxios(error);
  });
}


$( "#select_utps" ).autocomplete({
  source: function( request, response ) {
    $.ajax( {
      url: "http://localhost:8080/api/v1/utp/?alias__icontains="+request.term+"&fields=alias,uuid",
      beforeSend: function (xhr) {
        xhr.setRequestHeader ("Authorization", "jwt " + HDD.get(Django.name_jwt));
      },
      data: {
        term: request.term
      },
      success: function( data ) {
        if(!data.length){
          var result = [
           {
           label: 'Sin resultados.', 
           value: response.term
           }
         ];
           response(result);
         }
        else{

          res = $.map(data, function(item) {
                return {
                  label: item.alias,
                  value: item.alias,
                  uuid: item.uuid
                }
          });
          response(res);
        }
      }
    });
  },
  minLength: 2,
  select: function( event, ui ) {
    //log( "Selected: " + ui.item.label + " aka " + ui.item.value );
    findCPEs(ui.item.uuid)
  }
})

ayuda = () => {
  window.open('https://nuevooptic.atlassian.net/wiki/spaces/SEN/pages/50233349/Provisionar+ONU', '_blank');
}

service_number_enter = (e,id) =>{
  if(e.keyCode == 13)
  {
    findServiceNumber(id)
  }
}

