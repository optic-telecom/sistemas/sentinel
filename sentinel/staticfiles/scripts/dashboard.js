var updaterInterval, gaugesData = [],
    gaugesFirstRun = false, target, gauge,
    percent, totalDevices, updaterActive;

for (var k = 0; k < 100; k++) {
    window['v' + k] = null;
}

var initGauges = function(){
  var opts = {
    angle: -0.01,
    lineWidth: 0.2,
    radiusScale: 1,
    pointer: {
      length: 0.6,
      strokeWidth: 0.035,
      color: '#030000'
    },
    staticZones: [
     {strokeStyle: "red", min: 0, max: 50},
     {strokeStyle: "#FFDD00", min: 50, max: 75},
     {strokeStyle: "green", min: 75, max: 100}
    ],
    limitMax: true,
    limitMin: false,
    colorStart: '#6FADCF',
    colorStop: '#8FC0DA',
    strokeColor: '#E0E0E0',
    generateGradient: true,
    highDpiSupport: false,
    renderTicks: {
      divisions: 5,
      divWidth: 1.1,
      divLength: 0.7,
      divColor: '#333333',
      subDivisions: 3,
      subLength: 0.5,
      subWidth: 0.6,
      subColor: '#666666'
    }
  };

  for (var i = 0; i < gaugesData.length; i++) {

    var id = gaugesData[i].id;
    target = document.getElementById('location_' + id)
    gauge = new Gauge(target).setOptions(opts);
    gauge.maxValue = 100;
    gauge.setMinValue(0);
    gauge.animationSpeed = 2;
    if (gaugesData[i].percent){
      gauge.set(gaugesData[i].percent);
    }else{
      gauge.set(0);
    }
    window['v' + id] = gauge;

  }
  gaugesFirstRun = true;

}// end function


var renderGauges = function(){

  var gauge_box = templater`
  <div class="col">
  <div>
    <h2>${ 'name' }</h2>
    <canvas id="location_${ 'id' }"></canvas>
    <h4 class="widget-list-title" id="percent_value_${ 'id' }" style="color:${ 'color' }">
    ${ 'percent' }% online
    </h4>
  </div>        
  </div>`;

  $("#div_gauges").empty();
  for (var i = 0; i < gaugesData.length; i++) {
    $("#div_gauges").append(gauge_box(gaugesData[i]));    
  }

  initGauges();
}/// end render


var getData = function(callback=null){
  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };
  var url = '/api/v1/dashboard/';

  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      gaugesData = [];

      for (var i = 0; i < res.data.length; i++) {

        var nodeObj = res.data[i];
        totalDevices = nodeObj.devices + nodeObj.devices_cpes;
        if (totalDevices != 0)
        {
          percent = (nodeObj.devices_online * 100 ) / totalDevices;
        }
        else
        {
          percent = 0;
        }
        percent = parseInt(percent);  
        color = '';

        if (percent > 98)
        {
          color = 'green';
        }
        if (percent >= 95 && percent < 99)
        {
          color = '#ffca76';
        }
        if (percent >= 90 && percent < 95)
        {
          color = '#fa9c05';
        }

        if (percent < 95)
        {
          color = 'red';
        }

        if (gaugesFirstRun)
        {
            gauge = window['v' + nodeObj.id];
            //gauge.maxValue = 100;
            //gauge.setMinValue(0);      
            gauge.animationSpeed = 4;     
            gauge.set(percent);
            percentText = $('#percent_value_'+nodeObj.id);
            if (percent != NaN)
            {
              percentText.text(percent + '% online');
              percentText.css('color',color);
            }

        }else{
          gaugesData.push({
            id:nodeObj.id,
            name:nodeObj.name,
            percent:percent,
            color:color
          });

        }
      }
      if (callback){
        callback();
      }

    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }

    if (res.status == 429)
    {
      handleTooManyRequestsAxios();
    }
  })
  .catch(function(error) {
      handleErrorAxios(error);
  });

}

var updater = function(){
  updaterActive = true;
  updaterInterval = setInterval(function(){
    getData(initGauges);
  }, Django.time_reload_dashboard);
  $("#btn_updater").removeClass('fa-play').addClass('fa-stop');
  return true;
}

var stopUpdater = function(){
  updaterActive = false;
  clearInterval(updaterInterval);
  $("#btn_updater").removeClass('fa-stop').addClass('fa-play');
  return true;
}

var toggleUpdater = function(){
  if(updaterActive){
    stopUpdater();
  }else{
    updater();
  }
}
//gauge.set(92); // set actual value

var initDashboard = function () {
  userInSession();
  App.setPageTitle('Sentinel - Dashboard');
  $("#menu_dashboard").addClass('active');
  getData(renderGauges);
  updater();
}();