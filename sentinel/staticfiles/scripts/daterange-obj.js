// ==================== MOMENT ========================== \\
var fecha = new Date();
var annio = fecha.getFullYear()
var annio_anterior = annio - 1
//===== TRIMESTRES =====\\
/******** FORMATO AAAA-MM-DD ********/
var Q1A = annio.toString() + '-01-01'
var Q1B = annio.toString() + '-03-31'

var Q2A = annio.toString() + '-04-01'
var Q2B = annio.toString() + '-06-30'

var Q3A = annio.toString() + '-07-01'
var Q3B = annio.toString() + '-09-30'

var Q4A = annio.toString() + '-10-01'
var Q4B = annio.toString() + '-12-31'

//===== SEMESTRE =====\\

var S1A = annio.toString() + '-01-01'
var S1B = annio.toString() + '-06-30'

var S2A = annio.toString() + '-07-01'
var S2B = annio.toString() + '-12-31'


var annioanteriorA = annio_anterior.toString() + '-01-01'
var annioanteriorB = annio_anterior.toString() + '-12-31'


var jsonDateRange = {
// autoApply: true,
// autoUpdateInput:true, 
format: 'MM/DD/YYYY',
startDate: moment().subtract(29, 'days'),
endDate: moment(),
// minDate: '01/01/2012',
// maxDate: '12/31/2015',
// dateLimit: { days: 60 },
"minYear": 2000,
"maxYear":2030,
showDropdowns: true,
showWeekNumbers: true,
timePicker: false,
timePickerIncrement: 1,
timePicker12Hour: true,
ranges: {

// 'Últimos 15 días': [moment().subtract(14, 'days'), moment()],
// 'Últimos 30 días': [moment().subtract(29, 'days'), moment()],
'Trimestre 1': [moment(Q1A,'YYYY-MM-DD'), moment(Q1B,'YYYY-MM-DD')],
'Trimestre 2': [moment(Q2A,'YYYY-MM-DD'), moment(Q2B,'YYYY-MM-DD')],
'Trimestre 3': [moment(Q3A,'YYYY-MM-DD'), moment(Q3B,'YYYY-MM-DD')],
'Trimestre 4': [moment(Q4A,'YYYY-MM-DD'), moment(Q4B,'YYYY-MM-DD')],

'Semestre 1': [moment(S1A,'YYYY-MM-DD'), moment(S1B,'YYYY-MM-DD')],
'Semestre 2': [moment(S2A,'YYYY-MM-DD'), moment(S2B,'YYYY-MM-DD')],
  
'Último año': [moment().subtract(364, 'days'), moment()],//
'Año anterior':[moment(annioanteriorA,'YYYY-MM-DD'),moment(annioanteriorB,'YYYY-MM-DD')],

'Hoy': [moment(), moment()],
'Mes actual': [moment().startOf('month'), moment().endOf('month')],
'Mes anterior': [moment().subtract(1, 'month').startOf('month'), 
                moment().subtract(1, 'month').endOf('month')],
},
opens: 'right',
drops: 'down',
buttonClasses: ['btn', 'btn-sm'],
applyClass: 'btn-warning',
cancelClass: 'btn-dark',
separator: ' hasta ',
locale: {
  applyLabel: 'Enviar',
  cancelLabel: 'Reset',
  fromLabel: 'From',
  toLabel: 'To',
  customRangeLabel: 'Personalizada',
  daysOfWeek: ['Dom', 'Lun', 'Mar', 'Mier', 'Jue', 'Vie','Sab'],
  monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
  firstDay: 1
}
}