"use strict";

userInSession();
App.setPageTitle('Sentinel - Lista de Setupbox');
$("#menu_tv").addClass('active');
$("#menu_setupbox").addClass('active');
var conf = {
  headers: {
    Authorization: 'jwt ' + HDD.get(Django.name_jwt)
  }
};
$(document).ready(function () {
  id_code = getUrlLastParameter();
  url_dt = "/datatables/setupbox/".concat(id_code);
  oTable = $('#table_setupbox').dataTable({
    "aaSorting": [[7, "desc"]],
    "language": {
      "sLengthMenu": "Mostrar _MENU_ Decodificadores",
      "sZeroRecords": "No se encontraron resultados",
      "sEmptyTable": "Ningún dato disponible en esta tabla",
      "sInfo": "Mostrando Decodificadores del _START_ al _END_ de un total de _TOTAL_ Decodificadores",
      "sInfoEmpty": "Mostrando Decodificadores del 0 al 0 de un total de 0 Decodificadores",
      "sInfoFiltered": "(filtrado de un total de _MAX_ Decodificadores)",
      "sInfoPostFix": "",
      "sSearch": "Buscar:",
      "sUrl": "",
      "sInfoThousands": ",",
      "sLoadingRecords": "Cargando...",
      "sProcessing": "Cargando...",
      "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": url_dt,
      "type": "POST",
      "headers": {
        'Authorization': HDD.get(Django.name_jwt)
      }
    },
    "columnDefs": [{
      targets: [9],
      orderable: false,
      searchable: false,
      className: ['text-center']
    }, {
      targets: [1, 2, 3, 4, 5, 6, 7, 8],
      orderable: true,
      searchable: true,
      className: ['text-center']
    }]
  });
});

getSelect2 = function getSelect2(url, idModal, attr) {
  var obj = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
  var val = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
  axios.get(url, conf).then(function (response) {
    if (response.status == 200) {
      var data = response.data.map(function (obj) {
        return {
          id: obj.id,
          text: obj[attr]
        };
      });
      $(idModal).select2({
        dropdownParent: $("#modal_create"),
        width: '100%',
        data: data
      });

      if (obj) {
        $(idModal).val(val).trigger('change');
      }
    }
  })["catch"](function (error) {
    handleErrorAxios(error);
  });
};

modalBodyCreate = function modalBodyCreate() {
  var title = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'Crear Setupbox';
  var funcion = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "newSetupbox";
  var id = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
  return "<div class=\"modal-dialog\">\n  <div class=\"modal-content\">\n    <form class=\"form-horizontal\" data-parsley-validate=\"true\" name=\"demo-form\">\n    <div class=\"modal-header\">\n      <h4 class=\"modal-title\">".concat(title, "</h4>\n      <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">\xD7</button>\n    </div>\n    <div class=\"modal-body\">\n      <div id=\"loader_modal\"></div>\n  \n      <div class=\"form-group row m-b-15\">\n        <label class=\"col-md-4 col-sm-4 col-form-label\" for=\"model\">Modelo</label>\n        <div class=\"col-md-8 col-sm-8\">\n          <select id=\"model\" class=\"form-control form-control-sm\" width=\"100%\"><option value=\"\">Modelo</option></select>\n          <span id=\"model_error\" class=\"text-danger\"></span>\n          </div>\n      </div>\n\n      <div class=\"form-group row m-b-15\">\n        <label class=\"col-md-4 col-sm-4 col-form-label\" for=\"mac\">Mac</label>\n        <div class=\"col-md-8 col-sm-8\">\n          <select id=\"mac\" class=\"form-control form-control-sm\" width=\"100%\"><option value=\"\">Mac</option></select>\n          <span id=\"mac_error\" class=\"text-danger\"></span>\n        </div>\n      </div>\n\n      <div class=\"form-group row m-b-15\">\n        <label class=\"col-md-4 col-sm-4 col-form-label\" for=\"status\">Status</label>\n        <div class=\"col-md-8 col-sm-8\">\n        <select id=\"status\" class=\"form-control form-control-sm\" width=\"100%\"><option value=\"\">Status</option></select>\n        <span id=\"status_error\" class=\"text-danger\"></span>\n        </div>\n      </div>\n       \n      <div class=\"form-group row m-b-15\">\n        <label class=\"col-md-4 col-sm-4 col-form-label\" for=\"operator\">Operador</label>\n        <div class=\"col-md-8 col-sm-8\">\n        <select id=\"operator\" class=\"form-control form-control-sm\" width=\"100%\"><option value=\"\">Operador</option></select>\n        <span id=\"operator_error\" class=\"text-danger\"></span>\n        </div>\n      </div>\n\n      <div class=\"form-group row m-b-15\">\n        <label class=\"col-md-4 col-sm-4 col-form-label\" for=\"service\">Nro de Servicio</label>\n        <div class=\"col-md-8 col-sm-8\">\n        <select id=\"service\" class=\"form-control form-control-sm\" width=\"100%\"><option value=\"\">Servicios</option></select>\n        <span id=\"service_error\" class=\"text-danger\"></span>\n        </div>\n      </div>\n\n\n    </div>\n    <div class=\"modal-footer\">\n      <a href=\"javascript:;\" class=\"btn btn-white\" data-dismiss=\"modal\">Cerrar</a>\n      <button type=\"button\" class=\"btn btn-primary\" onclick=\"").concat(funcion, "(").concat(id, ")\">").concat(title, "</button>\n    </div>\n    </form>\n  </div>\n</div>");
};

modalBodyDetail = function modalBodyDetail(title, obj) {
  return "<div class=\"modal-dialog\">\n  <div class=\"modal-content\">\n    <div class=\"modal-header\">\n      <h4 class=\"modal-title\">".concat(title, "</h4>\n      <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">\xD7</button>\n    </div>\n    <div class=\"modal-body\">\n      <table class=\"table table-stripped table-border\" style=\"width:100%\">\n        <tr>\n          <th>Modelo</th><td>").concat(obj.model, "</td>\n        </tr>\n        <tr>\n          <th>MAC Address</th><td>").concat(obj.mac, "</td>\n        </tr>\n        <tr>\n          <th>Status</th><td>").concat(obj.status, "</td>\n        </tr>\n        <tr>\n          <th>Operador</th><td>").concat(obj.operator, "</td>\n        </tr>\n        <tr>\n          <th>Servicio</th><td>").concat(obj.service, "</td>\n        </tr>\n        <tr>\n          <th>Asociado al servicio desde</th><td>").concat(obj.associated_service, "</td>\n        </tr>\n        <tr>\n          <th>Voucher</th><td>").concat(obj.voucher, "</td>\n        </tr>\n        <tr>\n          <th>Asociado al voucher desde</th><td>").concat(obj.associated_voucher, "</td>\n        </tr>\n      </table>\n    </div>\n  </div>\n</div>");
}; // ACTIONS


loadModal = function loadModal() {
  $("#modal_create").empty().html(modalBodyCreate()).modal({
    show: true
  });
  getSelect2('/api/v1/operator/', "#operator", 'name');
  getSelect2('/api/v1/model_device/', "#model", 'model');
  getSelect2('/api/v1/macs/', "#mac", 'mac');
  getSelect2('/api/v1/service/', "#service", 'number');
  getSelect2('/api/v1/status-stb/', "#status", 'name');
};

detailSetupBox = function detailSetupBox(obj, action) {
  $("#modal_create").empty().html(modalBodyDetail('Detalle del Setupbox', obj)).modal({
    show: true
  });
};

newSetupbox = function newSetupbox(id) {
  if (id == 0) {
    console.log('Creando setupbox');
  }

  var data = {
    operator: $("#operator").val(),
    model: $("#model").val(),
    mac: $("#mac").val(),
    service: $("#service").val(),
    voucher: getUrlLastParameter(),
    status: $("#status").val()
  };
  var falses = [];

  for (d in data) {
    if (d != 'service') {
      if (data[d] == '') {
        falses.push(false);
      }
    }
  }

  if (falses.length == 0) {
    var url = Django.api_setupbox_list;
    axios.post(url, data, conf).then(function (response) {
      if (response.status == 201) {
        _exito("Setupbox creado satisfactoriamente");

        $("#modal_create").empty().modal('toggle');
        setTimeout(updateDatatable(), 5);
      }
    })["catch"](function (error) {
      inputs = ['mac', 'voucher', 'service'];

      if (error.response.status == 400) {
        var $errors = error.response.data;

        for (i = 0; i < inputs.length; i++) {
          if (inputs[i] in $errors) {
            $("#".concat(inputs[i], "_error")).text($errors[inputs[i]][0]).fadeOut(4000);
            setTimeout(function () {
              $("#modal_create").modal('toggle');
            }, 4000);
          }
        }
      }
    });
  } else {
    $("#modal_create").empty().modal('toggle');

    _error('Los campos del formulario son todos, obligatorios.');
  }
};

getSetupBox = function getSetupBox(id) {
  var url = Django.api_setupbox_detail.replace(':val:', id);
  axios.get(url, conf).then(function (response) {
    if (response.status == 200) {
      var $data = response.data; // if(action == 'detail'){
      //   $("#modal_create").empty().html(modalBodyDetailOrEdit($data,action)).modal({show:true})
      // }else{

      $("#modal_create").empty().html(modalBodyCreate('Editar Setupbox', 'editSetupbox', $data.id)).modal({
        show: true
      });
      getSelect2('/api/v1/operator/', "#operator", 'name', true, $data.operator);
      getSelect2('/api/v1/model_device/', "#model", 'model', true, $data.model);
      getSelect2('/api/v1/macs/', "#mac", 'mac', true, $data.mac);
      getSelect2('/api/v1/service/', "#service", 'number', true, $data.service);
      getSelect2('/api/v1/status-stb/', "#status", 'name', true, $data.status); // }
    }
  })["catch"](function (error) {
    handleErrorAxios(error);
  });
};

editSetupbox = function editSetupbox(id) {
  var url = Django.api_setupbox_detail.replace(':val:', id);
  var data = {
    operator: $("#operator").val(),
    model: $("#model").val(),
    mac: $("#mac").val(),
    service: $("#service").val(),
    voucher: getUrlLastParameter(),
    status: $("#status").val()
  };
  axios.put(url, data, conf).then(function (response) {
    if (response.status == 200) {
      _exito('Edición Satisfactoria');

      $("#modal_create").empty().modal('toggle');
      setTimeout(updateDatatable(), 5);
    }
  })["catch"](function (error) {
    console.log(error.response.data);
    handleErrorAxios(error);
  });
};

deleteSetupbox = function deleteSetupbox(id) {
  swal({
    title: "¿Realmente quiere hacer esto?",
    text: "Recuerda que si eliminas este Setupbox no podrás revertir este cambio",
    icon: "warning",
    buttons: true,
    dangerMode: true
  }).then(function (willDelete) {
    if (willDelete) {
      var url = Django.api_setupbox_detail.replace(':val:', id);
      axios["delete"](url, conf).then(function (response) {
        if (response.status == 204) {
          _exito('El dispositivo ha sido eliminado satisfactoriamente');

          setTimeout(updateDatatable(), 5);
        }
      })["catch"](function (error) {
        handleErrorAxios(error);
      });
    } else {
      _info("Tú dispositivo está a salvo.");
    }
  });
};