"use strict";

userInSession();
App.setPageTitle('Sentinel - Lista de Setupbox');
$("#menu_tv").addClass('active');
$("#menu_setupbox").addClass('active');
var conf = {
  headers: {
    Authorization: 'jwt ' + HDD.get(Django.name_jwt)
  }
};
$(document).ready(function () {
  url_dt = Django.datatable_setupbox_list;
  oTable = $('#table_setupbox').dataTable({
    "aaSorting": [[7, "desc"]],
    "language": {
      "sLengthMenu": "Mostrar _MENU_ Decodificadores",
      "sZeroRecords": "No se encontraron resultados",
      "sEmptyTable": "Ningún dato disponible en esta tabla",
      "sInfo": "Mostrando Decodificadores del _START_ al _END_ de un total de _TOTAL_ Decodificadores",
      "sInfoEmpty": "Mostrando Decodificadores del 0 al 0 de un total de 0 Decodificadores",
      "sInfoFiltered": "(filtrado de un total de _MAX_ Decodificadores)",
      "sInfoPostFix": "",
      "sSearch": "Buscar:",
      "sUrl": "",
      "sInfoThousands": ",",
      "sLoadingRecords": "Cargando...",
      "sProcessing": "Cargando...",
      "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": url_dt,
      "type": "POST",
      "headers": {
        'Authorization': HDD.get(Django.name_jwt)
      }
    },
    "columnDefs": [{
      targets: [9],
      orderable: false,
      searchable: false,
      className: ['text-center']
    }, {
      targets: [1, 2, 3, 4, 5, 6, 7, 8],
      orderable: true,
      searchable: true,
      className: ['text-center']
    }]
  });
});

var SetupBox = function SetupBox(title, color) {
  this.title = title;
  this.color = color;
  this.conf = {
    headers: {
      Authorization: 'jwt ' + HDD.get(Django.name_jwt)
    }
  };
  this.select2 = [];

  this.modalFooter = function (title) {
    var funcion = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var id = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
    return "<div class=\"modal-footer\">\n    <a href=\"javascript:;\" class=\"btn btn-white\" data-dismiss=\"modal\">Cerrar</a>\n\n    ".concat(funcion ? "<a href=\"javascript:;\" class=\"btn btn-".concat(this.color, "\" onclick=\"").concat(funcion, "('").concat(id, "')\">").concat(title, "</a>") : "<a href=\"javascript:;\" class=\"btn btn-".concat(this.color, "\" data-dismiss=\"modal\">").concat(title, "</a>"), "\n\n    </div>");
  };

  this.modalBody = function () {
    var content = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var footer = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    return "<div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <form class=\"form-horizontal\" data-parsley-validate=\"true\" name=\"demo-form\">\n      <div class=\"modal-header bg-".concat(this.color, "\">\n        <h4 class=\"modal-title\">").concat(title, "</h4>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">\xD7</button>\n      </div>\n      <div class=\"modal-body\">\n        <div id=\"loader_modal\"></div>\n    \n        ").concat(content ? content : '', "\n      </div>\n        ").concat(footer ? footer : '', "\n      </form>\n    </div>\n  </div>");
  };

  this.modalInput = function (title, idInput) {
    var value = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    return "<div class=\"form-group row m-b-15\">\n              <label class=\"col-md-4 col-sm-4 col-form-label\" for=\"".concat(idInput, "\">").concat(title, "</label>\n              <div class=\"col-md-8 col-sm-8\">\n                <input name=\"").concat(idInput, "\" id=\"").concat(idInput, "\" class=\"form-control\" value=\"").concat(value ? value : '', "\"/>\n                <span id=\"").concat(idInput, "_error\" class=\"text-danger\"></span>\n                </div>\n            </div>");
  };

  this.modalSelect = function (title, idInput) {
    return "<div class=\"form-group row m-b-15\">\n              <label class=\"col-md-4 col-sm-4 col-form-label\" for=\"".concat(idInput, "\">").concat(title, "</label>\n              <div class=\"col-md-8 col-sm-8\">\n                <select id=\"").concat(idInput, "\" class=\"form-control form-control-sm\" width=\"100%\"><option value=\"\">").concat(title, "</option></select>\n                <span id=\"").concat(idInput, "_error\" class=\"text-danger\"></span>\n                </div>\n            </div>");
  };

  this.getSelect2 = function (url, idSelect) {
    var _this = this;

    var retrieve = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
    var value = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
    axios.get(url, conf).then(function (response) {
      if (response.status == 200) {
        _this.select2 = response.data.map(function (obj) {
          return {
            id: obj.id,
            text: obj.name || obj.code || obj.number || obj.model
          };
        });
        $(idSelect).select2({
          dropdownParent: $("#modal_create"),
          width: '100%',
          data: _this.select2
        });

        if (retrieve) {
          $(idSelect).val(value).trigger('change');
        }
      }
    })["catch"](function (error) {
      handleErrorAxios(error);
    });
  };
};

function loadModal() {
  var stb = new SetupBox('Crear un setupbox', 'success');
  var content = stb.modalSelect('Modelo', 'model') + stb.modalInput('MAC Address', 'mac');
  content += stb.modalSelect('Status', 'status') + stb.modalSelect('Operador', 'operator');
  content += stb.modalSelect('Nro de Servicio', 'service') + stb.modalSelect('Voucher', 'voucher');
  var footer = stb.modalFooter('Agregar', 'newSetupbox', 0);
  $("#modal_create").empty().append(stb.modalBody(content, footer)).modal({
    show: true
  });
  stb.getSelect2('/api/v1/operator/', "#operator");
  stb.getSelect2('/api/v1/model_device/?kind=6', "#model");
  stb.getSelect2('/api/v1/service/', "#service");
  stb.getSelect2('/api/v1/vouchers/', "#voucher");
  stb.getSelect2('/api/v1/status-stb/', "#status");
}

function tableDetail(obj) {
  return "<table class=\"table table-stripped table-border\" style=\"width:100%\">\n  <tr>\n    <th>Modelo</th><td>".concat(obj.model, "</td>\n  </tr>\n  <tr>\n    <th>MAC Address</th><td>").concat(obj.mac, "</td>\n  </tr>\n  <tr>\n    <th>Status</th><td>").concat(obj.status, "</td>\n  </tr>\n  <tr>\n    <th>Operador</th><td>").concat(obj.operator, "</td>\n  </tr>\n  <tr>\n    <th>Servicio</th><td>").concat(obj.service, "</td>\n  </tr>\n  <tr>\n    <th>Asociado al servicio desde</th><td>").concat(obj.associated_service, "</td>\n  </tr>\n  <tr>\n    <th>Voucher</th><td>").concat(obj.voucher, "</td>\n  </tr>\n  <tr>\n    <th>Asociado al voucher desde</th><td>").concat(obj.associated_voucher, "</td>\n  </tr>\n</table>");
} // ACTIONS


detailSetupBox = function detailSetupBox(obj) {
  var stb = new SetupBox('Detalle del SetupBox', 'info');
  $("#modal_create").empty().append(stb.modalBody(tableDetail(obj))).modal({
    show: true
  });
};

newSetupbox = function newSetupbox(id) {
  var data = {
    operator: $("#operator").val(),
    model: $("#model").val(),
    mac: $("#mac").val(),
    service: $("#service").val(),
    voucher: $("#voucher").val(),
    status: $("#status").val()
  };
  var falses = [];

  for (d in data) {
    if (d != 'service') {
      if (data[d] == '') {
        falses.push(false);
      }
    }
  }

  if (falses.length == 0) {
    var url = Django.api_setupbox_list;
    axios.post(url, data, conf).then(function (response) {
      if (response.status == 201) {
        _exito("Setupbox creado satisfactoriamente");

        $("#modal_create").empty().modal('toggle');
        setTimeout(updateDatatable(), 5);
      }
    })["catch"](function (error) {
      if (error.response.status == 400) {
        var $errors = error.response.data;

        if ($errors.mac) {
          $("#mac_error").text($errors.mac[0]).fadeOut(8000);
        }

        if ($errors.voucher) {
          $("#voucher_error").text($errors.voucher[0]).fadeOut(8000);
        }

        if ($errors.service) {
          $("#service_error").text($errors.service[0]).fadeOut(8000);
        }
      }
    });
  } else {
    $("#modal_create").empty().modal('toggle');

    _error('Los campos del formulario son todos, obligatorios.');
  }
};

loadModalEdit = function loadModalEdit(obj) {
  var stb = new SetupBox('Editar setupbox', 'warning');
  var content = stb.modalSelect('Modelo', 'model') + stb.modalInput('MAC Address', 'mac', obj.mac);
  content += stb.modalSelect('Status', 'status') + stb.modalSelect('Operador', 'operator');
  content += stb.modalSelect('Nro de Servicio', 'service') + stb.modalSelect('Voucher', 'voucher');
  var footer = stb.modalFooter('Editar', 'editSetupbox', obj.id);
  $("#modal_create").empty().append(stb.modalBody(content, footer)).modal({
    show: true
  });
  stb.getSelect2('/api/v1/operator/', "#operator", true, obj.operator);
  stb.getSelect2('/api/v1/model_device/?kind=6', "#model", true, obj.model);
  stb.getSelect2('/api/v1/service/', "#service", true, obj.service);
  stb.getSelect2('/api/v1/vouchers/', "#voucher", true, obj.voucher);
  stb.getSelect2('/api/v1/status-stb/', "#status", true, obj.status);
};

editSetupbox = function editSetupbox(id) {
  var url = Django.api_setupbox_detail.replace(':val:', id);
  var data = {
    operator: $("#operator").val(),
    model: $("#model").val(),
    mac: $("#mac").val(),
    service: $("#service").val(),
    voucher: $("#voucher").val(),
    status: $("#status").val()
  };
  axios.put(url, data, conf).then(function (response) {
    if (response.status == 200) {
      _exito('Edición Satisfactoria');

      $("#modal_create").empty().modal('toggle');
      setTimeout(updateDatatable(), 5);
    }
  })["catch"](function (error) {
    if (error.response.status == 400) {
      var $errors = error.response.data;

      if ($errors.mac) {
        $("#mac_error").text($errors.mac[0]).fadeOut(8000);
      }

      if ($errors.voucher) {
        $("#voucher_error").text($errors.voucher[0]).fadeOut(8000);
      }

      if ($errors.service) {
        $("#service_error").text($errors.service[0]).fadeOut(8000);
      }
    } else {
      handleErrorAxios(error);
    }
  });
};

deleteSetupbox = function deleteSetupbox(id) {
  swal({
    title: "¿Realmente quiere hacer esto?",
    text: "Recuerda que si eliminas este Setupbox no podrás revertir este cambio",
    icon: "warning",
    buttons: true,
    dangerMode: true
  }).then(function (willDelete) {
    if (willDelete) {
      var url = Django.api_setupbox_detail.replace(':val:', id);
      axios["delete"](url, conf).then(function (response) {
        if (response.status == 204) {
          _exito('El dispositivo ha sido eliminado satisfactoriamente');

          setTimeout(updateDatatable(), 5);
        }
      })["catch"](function (error) {
        handleErrorAxios(error);
      });
    } else {
      _info("Tú dispositivo está a salvo.");
    }
  });
};