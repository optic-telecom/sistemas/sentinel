"use strict";

userInSession();
App.setPageTitle('Sentinel - Listado de IPs');
$("#menu_configuraciones").addClass('active');
$("#menu_iptable").addClass('active');
var conf = {
  headers: {
    Authorization: 'jwt ' + HDD.get(Django.name_jwt)
  }
};
$(document).ready(function () {
  id = getUrlLastParameter();
  url_dt = "/ipt-settings-detail/".concat(id);

  language = function language() {
    var palabraClave = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'registros';
    return {
      sLengthMenu: "Mostrar _MENU_ ".concat(palabraClave),
      sZeroRecords: "No se encontraron resultados",
      sEmptyTable: "Ning\xFAn dato disponible en esta tabla",
      sInfo: "Mostrando ".concat(palabraClave, " del _START_ al _END_ de un total de _TOTAL_ ").concat(palabraClave),
      sInfoEmpty: "Mostrando ".concat(palabraClave, " del 0 al 0 de un total de 0 ").concat(palabraClave),
      sInfoFiltered: "(filtrado de un total de _MAX_ ".concat(palabraClave, ")"),
      sInfoPostFix: "",
      sSearch: "Buscar:",
      sUrl: "",
      sInfoThousands: ",",
      sLoadingRecords: "Cargando...",
      sProcessing: "Cargando...",
      oPaginate: {
        sFirst: "Primero",
        sLast: "Último",
        sNext: "Siguiente",
        sPrevious: "Anterior"
      },
      oAria: {
        sSortAscending: ": Activar para ordenar la columna de manera ascendente",
        sSortDescending: ": Activar para ordenar la columna de manera descendente"
      }
    };
  };

  oTable = $('#table_ips').dataTable({
    "aaSorting": [[0, "asc"]],
    "language": language('IPs'),
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": url_dt,
      "type": "POST",
      "headers": {
        'Authorization': HDD.get(Django.name_jwt)
      }
    },
    "columnDefs": [{
      orderable: true,
      searchable: true,
      targets: [0]
    }, {
      orderable: true,
      searchable: true,
      className: 'text-center',
      targets: [1, 2, 3]
    }, {
      orderable: false,
      searchable: false,
      className: 'text-center',
      targets: [4]
    }]
  });
});

var IPTable = function IPTable() {
  var title = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var color = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "primary";
  this.title = title;
  this.color = color;
  this.select2 = [];
  this.conf = {
    headers: {
      Authorization: 'jwt ' + HDD.get(Django.name_jwt)
    }
  };

  this.getSelect2 = function (path, selectorId) {
    var _this = this;

    var selected = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
    var value = arguments.length > 3 ? arguments[3] : undefined;
    axios.get(path, this.conf).then(function (response) {
      if (response.status == 200) {
        var $data = response.data;
        _this.select2 = $data.map(function (obj) {
          return {
            id: obj.id,
            text: obj.name
          };
        }); // Inicializamos el select2 

        $(selectorId).select2({
          width: '100%',
          dropdownParent: $("#modal_create"),
          data: _this.select2
        }); // si es un detalle o un edit, entonces preseleccionamos el objeto

        if (selected) {
          $(selectorId).val(value).trigger('change');
        }
      }
    })["catch"](function (error) {
      handleErrorAxios(error);
    });
  };

  this.formInput = function (title, idInput) {
    var value = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "";
    return "<div class=\"form-group row m-b-15\">\n                <label class=\"col-md-4 col-sm-4 col-form-label\" for=\"".concat(idInput, "\">").concat(title, " :</label>\n                <div class=\"col-md-8 col-sm-8\">\n                  <input class=\"form-control\" type=\"text\" value=\"").concat(value, "\" id=\"").concat(idInput, "\" name=\"").concat(idInput, "\" placeholder=\"").concat(title, "\" data-parsley-required=\"true\" />\n                  <span class=\"text-danger\" id=\"").concat(idInput, "_error\"></span>\n                </div>\n              </div>");
  };

  this.formSelect = function (title, idInput) {
    return "<div class=\"form-group row m-b-15\">\n              <label class=\"col-md-4 col-sm-4 col-form-label\" for=\"".concat(idInput, "\">").concat(title, " :</label>\n              <div class=\"col-md-8 col-sm-8\">\n                <select id=\"").concat(idInput, "\" name=\"").concat(idInput, "\" class=\"form-control\">\n                  <option value=\"\">").concat(title, "</option>\n                </select>\n                <span class=\"text-danger\" id=\"").concat(idInput, "_error\"></span>\n              </div>\n            </div>");
  };

  this.getSelect2Ajax = function (path, selectorId) {
    var selected = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
    var value = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
    var Select2 = $(selectorId).empty().select2({
      width: '100%',
      // minimumInputLength: 3,
      multiple: true,
      placeholder: 'IPs',
      dropdownParent: $("#modal_create"),
      ajax: {
        url: path,
        method: 'GET',
        headers: this.conf.headers,
        data: function data(params) {
          var query = {
            q: params.term
          }; // Query parameters will be ?search=[term]&type=public

          return query;
        },
        processResults: function processResults(data) {
          // Transforms the top-level key of the response object from 'items' to 'results'
          return {
            results: data.results,
            pagination: {
              more: true
            },
            data: $.map(data, function (obj) {
              obj.id = obj.id || obj.pk; // replace pk with your identifier
              // obj.text = obj.text || obj.ip

              return obj;
            })
          };
        }
      }
    });

    if (selected) {
      for (i = 0; i < value.length; i++) {
        var option = new Option(value[i].text, value[i].id, true, true);
        Select2.append(option).trigger('change');
      }
    }
  };

  this.modalFooter = function () {
    var title = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
    var funcion = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var id = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
    return "<div class=\"modal-footer\">\n    <a href=\"javascript:;\" class=\"btn btn-white\" data-dismiss=\"modal\">Cerrar</a>\n\n    ".concat(funcion ? "<a href=\"javascript:;\" class=\"btn btn-".concat(this.color, "\" onclick=\"").concat(funcion, "('").concat(id, "')\">").concat(title, "</a>") : "<a href=\"javascript:;\" class=\"btn btn-".concat(this.color, "\" data-dismiss=\"modal\">").concat(title, "</a>"), "\n\n    </div>");
  };

  this.modalBody = function () {
    var content = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var footer = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    return "<div class=\"modal-dialog\">\n            <div class=\"modal-content\">\n              <div class=\"modal-header bg-".concat(this.color, "\">\n                <h4 class=\"modal-title\" style=\"color:white;\">").concat(this.title, "</h4>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">\xD7</button>\n              </div>\n              <div class=\"modal-body\">\n                ").concat(content ? content : '', "\n              </div>\n              ").concat(footer ? footer : '', "\n            </div>\n          </div>");
  };
}; // 


modalDetail = function modalDetail(obj) {
  var content = "<table class=\"table table-border\" style=\"width:100%;\">\n                  <tr><th>IP</th><td>".concat(obj.ip, "</td></tr>\n                  <tr><th>Host</th><td>").concat(obj.host, "</td></tr>\n                  <tr><th>Men\xFA</th><td>").concat(obj.menu, "</td></tr>\n                  <tr><th>Titulo</th><td>").concat(obj.title, "</td></tr>\n                </table>");
  var ip = new IPTable('Detalle de la IP ' + obj.ip, 'info');
  $("#modal_create").empty().append(ip.modalBody(content)).modal({
    show: true
  });
};

updateModal = function updateModal(obj) {
  var ip = new IPTable('Editar la IP ' + obj.ip, 'warning');
  var content = ip.formInput('IP', 'ip', obj.ip) + ip.formInput('Host', 'host', obj.host);
  content += ip.formInput('Menú', 'menu', obj.menu) + ip.formInput('Titulo', 'title', obj.title);
  var footer = ip.modalFooter('Editar', 'updateIP', obj.id);
  $("#modal_create").empty().append(ip.modalBody(content, footer)).modal({
    show: true
  });
};

updateIP = function updateIP(id) {
  var data = {
    ip: $("#ip").val(),
    host: $("#host").val(),
    menu: $("#menu").val(),
    title: $("#title").val()
  };
  var url = "/api/v1/iptables/".concat(id, "/");
  axios.put(url, data, conf).then(function (response) {
    if (response.status == 200) {
      _exito('Se ha Editado la IP correctamente');

      $("#modal_create").modal('toggle');
      setTimeout(updateDatatable(), 40);
    }
  })["catch"](function (error) {
    handleErrorAxios(error);
  });
};

deleteIP = function deleteIP(id) {
  swal({
    title: "\xBFEliminar la IP?",
    text: "¿Realmente esta seguro?, Recuerde que esta acción no se puede deshacer",
    icon: "warning",
    buttons: true,
    dangerMode: true
  }).then(function (willDelete) {
    if (willDelete) {
      url = "/api/v1/iptables/".concat(id, "/");
      axios["delete"](url, conf).then(function (response) {
        if (response.status == 204) {
          _exito('La IP ha sido eliminada satisfactoriamente');

          setTimeout(updateDatatable(), 40);
        }
      })["catch"](function (error) {
        handleErrorAxios(error);
      });
    } else {
      _info("Su IP está a salvo.");
    }
  });
};