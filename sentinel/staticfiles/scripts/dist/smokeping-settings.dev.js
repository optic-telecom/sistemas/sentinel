"use strict";

userInSession();
App.setPageTitle('Sentinel - Menú Smokeping');
$("#menu_configuraciones").addClass('active');
$("#menu_smokeping").addClass('active');
var conf = {
  headers: {
    Authorization: 'jwt ' + HDD.get(Django.name_jwt)
  }
};
$(document).ready(function () {
  url_dt = '/ms-settings-list';

  language = function language() {
    var palabraClave = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'registros';
    return {
      sLengthMenu: "Mostrar _MENU_ ".concat(palabraClave),
      sZeroRecords: "No se encontraron resultados",
      sEmptyTable: "Ning\xFAn dato disponible en esta tabla",
      sInfo: "Mostrando ".concat(palabraClave, " del _START_ al _END_ de un total de _TOTAL_ ").concat(palabraClave),
      sInfoEmpty: "Mostrando ".concat(palabraClave, " del 0 al 0 de un total de 0 ").concat(palabraClave),
      sInfoFiltered: "(filtrado de un total de _MAX_ ".concat(palabraClave, ")"),
      sInfoPostFix: "",
      sSearch: "Buscar:",
      sUrl: "",
      sInfoThousands: ",",
      sLoadingRecords: "Cargando...",
      sProcessing: "Cargando...",
      oPaginate: {
        sFirst: "Primero",
        sLast: "Último",
        sNext: "Siguiente",
        sPrevious: "Anterior"
      },
      oAria: {
        sSortAscending: ": Activar para ordenar la columna de manera ascendente",
        sSortDescending: ": Activar para ordenar la columna de manera descendente"
      }
    };
  };

  oTable = $('#table_menusmokeping').dataTable({
    "aaSorting": [[2, "asc"]],
    "language": language('Menús'),
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": url_dt,
      "type": "POST",
      "headers": {
        'Authorization': HDD.get(Django.name_jwt)
      }
    },
    "columnDefs": [{
      orderable: true,
      searchable: true,
      targets: [0, 1]
    }, {
      orderable: true,
      searchable: true,
      className: 'text-center',
      targets: [2, 3]
    }, {
      orderable: false,
      searchable: false,
      className: 'text-center',
      targets: [4]
    }]
  });
});

var MenuSmokeping = function MenuSmokeping() {
  var title = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var color = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "white";
  this.title = title;
  this.select2 = [];
  this.conf = {
    headers: {
      Authorization: 'jwt ' + HDD.get(Django.name_jwt)
    }
  };
  this.color = color;

  this.inputForm = function (title, idInput) {
    var type = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "text";
    var value = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "";
    return "<div class=\"form-group row m-b-15\">\n                <label class=\"col-md-4 col-sm-4 col-form-label\" for=\"".concat(idInput, "\">").concat(title, " :</label>\n                <div class=\"col-md-8 col-sm-8\">\n                  <input class=\"form-control\" type=\"").concat(type, "\" value=\"").concat(value, "\" id=\"").concat(idInput, "\" name=\"").concat(idInput, "\" placeholder=\"").concat(title, "\" data-parsley-required=\"true\" />\n                  <span class=\"text-danger\" id=\"").concat(idInput, "_error\"></span>\n                </div>\n              </div>");
  };

  this.selectForm = function (title, idInput) {
    // si es un selector multiple no puede tener un option
    return "<div class=\"form-group row m-b-15\">\n              <label class=\"col-md-4 col-sm-4 col-form-label\" for=\"".concat(idInput, "\">").concat(title, " :</label>\n              <div class=\"col-md-8 col-sm-8\">\n                <select id=\"").concat(idInput, "\" name=\"").concat(idInput, "\" multiple class=\"form-control\">\n                </select>\n                <span class=\"text-danger\" id=\"").concat(idInput, "_error\"></span>\n              </div>\n            </div>");
  };

  this.selectFormAutocomplete = function (path, title, idInput) {
    // si es un selector multiple no puede tener un option
    return "<div class=\"form-group row m-b-15\">\n              <label class=\"col-md-4 col-sm-4 col-form-label\" for=\"".concat(idInput, "\">").concat(title, " :</label>\n              <div class=\"col-md-8 col-sm-8\">\n                <select \n                    name=\"").concat(idInput, "\" \n                    class=\"form-control\" \n                    style=\"width:100%\" \n                    data-placeholder=\"IPs\" \n                    data-minimum-input-length=\"3\" \n                    data-dropdownParent=\"#modal_create\" \n                    id=\"id_").concat(idInput, "\"\n                    data-autocomplete-light-language=\"es\" \n                    data-autocomplete-light-url=\"").concat(path, "\" \n                    data-autocomplete-light-function=\"select2\" multiple>\n                </select>\n                <span class=\"text-danger\" id=\"").concat(idInput, "_error\"></span>\n              </div>\n            </div>");
  };

  this.getSelect2Ajax = function (path, selectorId) {
    var selected = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
    var value = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
    var Select2 = $(selectorId).empty().select2({
      width: '100%',
      // minimumInputLength: 3,
      placeholder: 'IPs',
      dropdownParent: $("#modal_create"),
      ajax: {
        url: path,
        method: 'GET',
        headers: this.conf.headers,
        data: function data(params) {
          var query = {
            q: params.term
          }; // Query parameters will be ?search=[term]&type=public

          return query;
        },
        processResults: function processResults(data) {
          // Transforms the top-level key of the response object from 'items' to 'results'
          return {
            results: data.results,
            pagination: {
              more: true
            },
            data: $.map(data, function (obj) {
              obj.id = obj.id || obj.pk; // replace pk with your identifier
              // obj.text = obj.text || obj.ip

              return obj;
            })
          };
        }
      }
    });

    if (selected) {
      for (i = 0; i < value.length; i++) {
        var option = new Option(value[i].text, value[i].id, true, true);
        Select2.append(option).trigger('change');
      }
    }
  };

  this.getSelect2 = function (path, selectorId) {
    var _this = this;

    var selected = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
    var value = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
    axios.get(path, this.conf).then(function (response) {
      if (response.status == 200) {
        var $data = response.data.results;
        _this.select2 = $data; // Inicializamos el select2 

        var select2 = $(selectorId).select2({
          width: '100%',
          multiple: true,
          dropdownParent: $("#modal_create"),
          data: _this.select2
        }); // si es un detalle o un edit, entonces preseleccionamos el objeto

        if (selected) {
          for (i = 0; i < value.length; i++) {
            var option = new Option(value[i].text, value[i].id, true, true);
            select2.append(option).trigger('change');
          }
        }
      }
    })["catch"](function (error) {
      handleErrorAxios(error);
    });
  };

  this.modalFooter = function (title) {
    var funcion = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var id = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
    return "<div class=\"modal-footer\">\n                <a href=\"javascript:;\" class=\"btn btn-white\" data-dismiss=\"modal\">Cerrar</a>\n                ".concat(funcion ? "<a href=\"javascript:;\" class=\"btn btn-".concat(this.color, "\" onclick=\"").concat(funcion, "('").concat(id, "')\">").concat(title, "</a>") : "<a href=\"javascript:;\" class=\"btn btn-".concat(this.color, "\" data-dismiss=\"modal\">").concat(title, "</a>"), "\n                \n            </div>");
  };

  this.modalBody = function (content) {
    var footer = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
    return "<div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header bg-".concat(this.color, "\">\n                <h4 class=\"modal-title\">").concat(this.title, "</h4>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">\xD7</button>\n            </div>\n            <div class=\"modal-body\">\n                ").concat(content, "\n            </div>\n            ").concat(footer, "\n        </div>\n    </div>");
  };
}; // DETAIL MENÚ


detailMenu = function detailMenu(obj) {
  var content = "<div class=\"row\">\n                        <div class=\"col-md-6 offset-md-3\">\n                            <div class=\"row\">\n                                <div class=\"col-md-6\"><h5>Nombre del Men\xFA: <span class=\"small\">".concat(obj.title, "</span></h5></div>\n                                <div class=\"col-md-6\"><h5>Orden de Aparici\xF3n: <span class=\"small\">").concat(obj.order_field, "</span></h5></div>\n                            </div>\n                        </div>\n                    </div>\n                    <hr>\n                    <h4 class=\"text-center\">IP's asociadas</h4>\n                    <table id=\"iptable\" class=\"table table-stripped table-border\" style=\"width: 100%;\">\n                        <thead>\n                            <tr><th>IP</th><th>Host</th><th>Men\xFA Smokeping</th><th>Title</th></tr>\n                        </thead>\n                        <tbody></tbody>\n                    </table>\n            ");
  var menu = new MenuSmokeping('Detalle del menú ' + obj.title);
  $("#modal_create").empty().append(menu.modalBody(content)).modal({
    show: true
  }).addClass('modal-message');
  $("#iptable").DataTable({
    data: obj.iptable,
    "language": language('IPs'),
    columns: [{
      data: 'ip'
    }, {
      data: 'host'
    }, {
      data: 'menu'
    }, {
      data: 'title'
    }],
    columnDefs: [{
      orderable: true,
      searchable: true,
      className: 'text-center',
      targets: [0, 1, 2, 3]
    }]
  });
}; // CREATE


loadModal = function loadModal() {
  var menu = new MenuSmokeping('Crear un nuevo Menú smokeping', 'success');
  var content = menu.inputForm('Titulo', 'title', 'text');
  content += menu.inputForm('Orden de aparición', 'order_field', 'number');
  content += menu.selectForm('IPs', 'iptable');
  var footer = menu.modalFooter('Crear Menú', 'newMenuSmokeping', 0);
  $("#modal_create").empty().append(menu.modalBody(content, footer)).modal({
    show: true
  }).removeClass('modal-message');
  menu.getSelect2Ajax('/api/v1/iptable-ac/', '#iptable');
};

newMenuSmokeping = function newMenuSmokeping() {
  var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
  var data = {
    title: $("#title").val(),
    iptable: $("#iptable").val(),
    order_field: $("#order_field").val()
  };
  validator = [];

  for (d in data) {
    if (d == 'title' || d == 'order_field') {
      if (data[d] == '') {
        $("#".concat(d, "_error")).text('Campo requerido').fadeOut(5000);
        validator.push(id);
      }
    }
  }

  var path = '/api/v1/smokeping-menus/';

  if (validator.length == 0) {
    axios.post(path, data, conf).then(function (response) {
      if (response.status == 201) {
        _exito('Menú creado con éxito');

        $("#modal_create").modal('toggle');
        setTimeout(updateDatatable(), 5);
      }
    })["catch"](function (error) {
      handleErrorAxios(error);
    });
  } else {
    $.gritter.add({
      title: 'Cuidado!!!',
      text: 'Todos los campos del formulario para registrar un nuevo menú deben contener información',
      sticky: false,
      time: '5000',
      class_name: 'gritter-success gritter-light' // 'my-sticky-class'

    });
  }
}; // UPDATE


loadModalEdit = function loadModalEdit(obj) {
  var menu = new MenuSmokeping('Editar el Menú smokeping ' + obj.title, 'warning');
  var content = menu.inputForm('Titulo', 'title', 'text', obj.title) + menu.inputForm('Orden de aparición', 'order_field', 'number', obj.order_field);
  content += menu.selectForm("Ip's asociadas", 'iptable');
  var footer = menu.modalFooter('Editar Menú', 'updateMenuSmokeping', obj.id);
  $("#modal_create").empty().append(menu.modalBody(content, footer)).modal({
    show: true
  }).removeClass('modal-message');
  menu.getSelect2Ajax("/api/v1/iptable-ac/", '#iptable', true, obj.iptable);
};

updateMenuSmokeping = function updateMenuSmokeping(id) {
  var data = {
    title: $("#title").val(),
    iptable: $("#iptable").val(),
    order_field: $("#order_field").val()
  };
  validator = [];

  for (d in data) {
    if (d == 'title' || d == 'order_field') {
      if (data[d] == '') {
        $("#".concat(d, "_error")).text('Campo requerido').fadeOut(5000);
        validator.push(id);
      }
    }
  }

  var path = '/api/v1/smokeping-menus/' + id + '/';

  if (validator.length == 0) {
    axios.put(path, data, conf).then(function (response) {
      if (response.status == 200) {
        _exito('Menú editado con éxito');

        $("#modal_create").modal('toggle');
        setTimeout(updateDatatable(), 5);
      }
    })["catch"](function (error) {
      handleErrorAxios(error);
    });
  } else {
    $.gritter.add({
      title: 'Cuidado!!!',
      text: 'Todos los campos del formulario para editar un nuevo menú deben contener información',
      sticky: false,
      time: '5000',
      class_name: 'gritter-success gritter-light' // 'my-sticky-class'

    });
  }
}; // DELETE


deleteMenu = function deleteMenu(json) {
  var id = json.id;
  var menu_name = json.title;
  swal({
    title: "\xBFEliminar el men\xFA ".concat(menu_name, "?"),
    text: "¿Realmente esta seguro?, Recuerde que esta acción no se puede deshacer",
    icon: "warning",
    buttons: true,
    dangerMode: true
  }).then(function (willDelete) {
    if (willDelete) {
      url = "/api/v1/smokeping-menus/".concat(id, "/");
      axios["delete"](url, conf).then(function (response) {
        if (response.status == 204) {
          _exito('Su menú ha sido eliminado satisfactoriamente');

          setTimeout(updateDatatable(), 40);
        }
      })["catch"](function (error) {
        handleErrorAxios(error);
      });
    } else {
      _info("Su menú está a salvo.");
    }
  });
}; // PUBLISH ON SERVER SMOKEPING


publisherTarget = function publisherTarget() {
  var $conf = {
    "headers": {
      'Authorization': HDD.get(Django.name_jwt)
    }
  };
  axios.get('/api/v1/publisher-target-file/', $conf).then(function (response) {
    if (response.status == 200) {
      _info('El menú ha sido publicado en el servidor');
    }
  })["catch"](function (error) {
    handleErrorAxios(error);
  });
};