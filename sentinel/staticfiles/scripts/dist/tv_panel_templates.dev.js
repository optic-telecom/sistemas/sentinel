"use strict";

var header_common = {
  headers: {
    Authorization: 'jwt ' + HDD.get(Django.name_jwt)
  }
};

link = function (_link) {
  function link(_x, _x2, _x3) {
    return _link.apply(this, arguments);
  }

  link.toString = function () {
    return _link.toString();
  };

  return link;
}(function (link, linkTitle, funcion) {
  var active = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;
  return "<li class=\"nav-items\">\n            <a href=\"#".concat(link, "\" data-toggle=\"tab\" onclick=\"return ").concat(funcion, "()\" class=\"nav-link-panel ").concat(active ? 'active' : '', "\">\n              <span>").concat(linkTitle, "</span>\n            </a>\n          </li>");
});

tabBodyGeneral = function tabBodyGeneral() {
  var title = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var link = arguments.length > 1 ? arguments[1] : undefined;
  var active = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
  var content = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : '';
  return "<!-- begin tab-pane -->\n  <div class=\"tab-pane fade ".concat(active ? 'active show' : '', "\" id=\"").concat(link, "\">\n    <h3 class=\"m-t-10\"><i class=\"fa fa-cog\"></i> ").concat(title, "</h3>\n    \n    ").concat(content, "\n\n  </div>\n  <!-- end tab-pane -->");
};

panelBody = function panelBody(links) {
  return "\n  <div class=\"panel panel-inverse\" id=\"container-main\">\n\n\t<div class=\"panel-heading\">\n\t  <div class=\"panel-heading-btn\">\n\t\t<a class=\"btn btn-xs btn-icon btn-success\" onClick=\"closePanelAction()\">\n\t\t  <i class=\"fa fa-angle-double-right\"></i>\n\t\t</a>\n\t  </div>\n\t  <h4 class=\"panel-title\">Propiedades</h4>\n\t</div>\n\n\t<div class=\"\">\n  \n\t\t<div class=\"tab-content\">\n\t\n      <img src=\"".concat('img_model', "\" style=\"width:40px; heigh:50px;\" ></img>\n      <i class=\"fas fa-circle\" style=\"color: ", 'color_status', ";\"></i>\n      <i style=\"float: right;\" class=\"fas fa-power-off\"></i>\n    \n      <div class=\"tabbable-line\">\n      \n        <!-- begin nav-pills-panel -->\n        <ul class=\"nav nav-pills-panel\">\n          ", links, "\n        </ul>\n        <div class=\"tab-content\" id=\"tabs_content\">\n        </div>\n      </div>\n\n\t\n\t\t</div>\n\n  \t</div>\n</div>");
};

function panel_tv_content(el, id) {
  console.log(id);
  var links = link('general', 'General', 'loadGeneral', true) + link('mac', 'MAC Address', 'loadMAC', false) + link('modelo', 'Modelo', 'loadModel', false);
  links += link('servicio', 'Servicio', 'loadService', false) + link('operador', 'Operador', 'loadOperator', false);
  $(el).html(panelBody(links));
  loadGeneral();
}

loadGeneral = function loadGeneral() {
  var content = "<p>\n                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                  Integer ac dui eu felis hendrerit lobortis. Phasellus elementum, nibh eget adipiscing porttitor, \n                  est diam sagittis orci, a ornare nisi quam elementum tortor. Proin interdum ante porta est convallis \n                  dapibus dictum in nibh. Aenean quis massa congue metus mollis fermentum eget et tellus. \n                  Aenean tincidunt, mauris ut dignissim lacinia, nisi urna consectetur sapien, nec eleifend orci eros id lectus.\n                </p>";
  $("#tabs_content").empty().append(tabBodyGeneral('General', 'general', true, content));
};

loadMAC = function loadMAC() {
  $("#tabs_content").empty().append(tabBodyGeneral('MAC Address', 'mac', true));
};

loadService = function loadService() {
  $("#tabs_content").empty().append(tabBodyGeneral('Servicio', 'service', true));
};

loadOperator = function loadOperator() {
  $("#tabs_content").empty().append(tabBodyGeneral('Operador', 'operator', true));
};

loadModel = function loadModel() {
  $("#tabs_content").empty().append(tabBodyGeneral('Modelo', 'model', true));
}; ///** events **///


function closePanelAction() {
  $('.theme-panel').removeClass('active');
  HDD.set('panel_page', '0');
}