userInSession();
App.setPageTitle('Sentinel - Detalle de UTP');
var gear_id = getUrlLastParameter();
var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};

moment.locale('es');

$("#menu_switch").addClass('active');
$("#menu_gear_list").addClass('active');


//* events *//
$(document).ready(function (){


  getDetailsGear();
  getPicsGear();
  getDocsGear();
 
  $('#btn-add-gallery').click(addGallery)
  $('#btn-edit-gallery').click(editGallery)

  $('#btn-add-doc').click(addDoc)
  $('#btn-edit-doc').click(editDoc)

  
  $('#node').select2({
    dropdownParent: $('#modal-create'),
    width: 'resolve',
    language: {

      noResults: function() {
  
        return "No hay resultado";        
      },
      searching: function() {
  
        return "Buscando..";
      }
    }
  });
  $('#model').select2({
    dropdownParent: $('#modal-create'),
    width: 'resolve',
    language: {

      noResults: function() {
  
        return "No hay resultado";        
      },
      searching: function() {
  
        return "Buscando..";
      }
    }
  });

  
});/// end ready




function getDetailsGear() {
  var url = DjangoURL('api_gear_detail', gear_id)
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200){
		console.log(res.data)

        $("#gear_mac").text(res.data.mac);
        $("#gear_description").text(res.data.description);
        $("#gear_model").text(res.data.model_name);
        $("#gear_ip").text(res.data.ip);
        $("#gear_vlan").text(res.data.vlan);
        $("#gear_node").text(res.data.node_alias);
        $("#gear_firmware").text(res.data.firmware_version);
        $("#gear_notes").text(res.data.notes==="" ? 'Sin notas':res.data.notes);
        $("#gear_latest_backup_date").text(res.data.latest_backup_date? moment(res.data.latest_backup_date).format('LLLL'):'Desconocida');
        $("#gear_modified_at").text(moment(res.data.modified_at).format('LLLL'));
        $("#gear_days_since_last_backup").text(res.data.days_since_last_backup >= 0 && 
                                                res.data.days_since_last_backup !== null ? 
                                                res.data.days_since_last_backup:'Desconocida');

        $("#activities").empty()
        for (var i = 0; i < res.data.action_log.length; i++) {
        	$("#activities").append(`
        		<tr>
        			<td>
        				${res.data.action_log[i][0].activity}
        			</td>
        			<td title="${moment(res.data.action_log[i][0].dt).format('LLLL')}" >
        				${moment(res.data.action_log[i][0].dt).startOf('day').fromNow()}
        			</td>
        		</tr>
    		`)
        }
        
        getNodes(res.data.node);
        getModels(res.data.model);
        $("#mac").val(res.data.mac);
        $("#description").val(res.data.description);
        $("#ip").val(res.data.ip);
        $("#vlan").val(res.data.vlan);
        $("#firmware").val(res.data.firmware_version);
        $("#notes").val(res.data.notes);
        

    }

    if (res.status == 401){
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}




function getPicsGear() {
  var url = DjangoURL('api_gear_detail_pics', gear_id)
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200){
      console.log(res.data)

      $("#pics-content").html('')
  		
      if(res.data.length === 0){
  			$("#pics-content").html(`<p style="margin: auto; class="text-center">No hay fotos aún.</p>`);
  			
  		}else {
  			for (var i = 0; i < res.data.length; i++) {

          var created_at = moment(res.data[i].created_at).format('lll');
          var caption = res.data[i].caption;
  				$("#pics-content").append(`
  					<div class="col-lg-3 col-md-4 col-xs-6">
  					  	<p class="text-center"><small title="${created_at}"><i class="ti-time"> </i> ${ created_at }</small></p>
  					  	<a class="thumbnail" href="${ res.data[i].photo }" data-featherlight="image">
  					    	<img class="img-fluid" src="${ res.data[i].photo_thumbnail }" alt="${ caption }" title="${ caption }">
  					  	</a>
  					  	<div class="pull-left">
                    <small title="${ caption }">
                        <a  type="button" href="#modal-edit-gallery" 
                            onclick="fill_update('${caption}', ${res.data[i].id})" 
                            data-toggle="modal" >
                            <i class="fa fa-pen"> </i> 
                            ${ caption }
                        </a>
                    </small>
                </div> 
  					  	<div class="pull-right">
                    <a type="button"  class="" onclick="delete_pic(${res.data[i].id})">
                        <i class="fa fa-trash-alt btn btn-danger" ></i>
                    </a>                        
                </div>
  					</div>
  				`);
  			}
  		}


    }

    if (res.status == 401){
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}


function addGallery() {
	
	var formData = new FormData();
	var picfile = document.querySelector('#pic_file');
	
	if(picfile.files.length > 0)
		formData.append("pic", picfile.files[0]);
	
	formData.append('caption', $("#pic_foot").val());


  var url = DjangoURL('api_gear_add_pic', gear_id);
	axios.post(url+'/', formData, {
		headers: { 
			Authorization: 'jwt '+ HDD.get(Django.name_jwt),
			'Content-Type': 'multipart/form-data'
		},
	})
	.then(function(response){
		console.log(response);
		if (response.status == 200)
		{
    
      $('#modal-add-gallery').modal('hide');
    
			$.gritter.add({
				title: 'Foto subida',
				time: 1000,
			});

      getPicsGear();
		}
	})//end then
	.catch(function(error) {
    if (error.response.data.detail)
      _error(error.response.data.detail);
    else
      handleErrorAxios(error);
	});
}


      
function getDocsGear() {
  var url = DjangoURL('api_gear_detail_docs', gear_id)
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200){
      console.log(res.data)

      $("#docs-content").html('')
      
      if(res.data.length === 0){
        $("#docs-content").html(`<tr><td colspan="6"><p class="text-center">No hay documentos aún.</p></td></tr>`);
        
      }else {
        for (var i = 0; i < res.data.length; i++) {

          var created_at = moment(res.data[i].created_at).format('lll');
          
          $("#docs-content").append(`
            <tr>
              <td>
                <i class="ti-pencil-alt"> </i> 
                <a type="button" href="#modal-edit-doc" data-toggle="modal" onclick="fill_update_doc('${res.data[i].description}', ${res.data[i].id})" >${ res.data[i].name }</a>
              </td>
                <td>${ res.data[i].description }</td>
                <td>${ res.data[i].extension.toUpperCase() }</td>
                <td><span title="${ created_at }"><i class="ti-time"> </i> ${ created_at }</span></td>
                <td>${ res.data[i].agent}</td>
                <td>
                  <a href="${ res.data[i].file }" class="btn btn-primary" target="_blank">
                    <i class="fa fa-eye"></i>
                  </a>
                  <a href="${ res.data[i].file }" class="btn btn-success" target="_blank" download>
                    <i class="fa fa-download"></i>
                  </a>
                  <a type="button"  class="" onclick="delete_doc(${res.data[i].id})">
                    <i class="fa fa-trash-alt btn btn-danger" ></i>
                  </a>   
               </td>
            </tr>
          `);
        }
      }


    }

    if (res.status == 401){
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

function addDoc() {
  
  var formData = new FormData();
  var docfile = document.querySelector('#doc_file');
  
  if(docfile.files.length > 0)
    formData.append("file", docfile.files[0]);
  
  formData.append('description', $("#doc_description").val());
  formData.append('tag', '');


  var url = DjangoURL('api_gear_add_doc', gear_id);
  axios.post(url, formData, {
    headers: { 
      Authorization: 'jwt '+ HDD.get(Django.name_jwt),
      'Content-Type': 'multipart/form-data'
    },
  })
  .then(function(response){
    console.log(response);
    if (response.status == 200)
    {
    
      $('#modal-add-doc').modal('hide');
    
      $.gritter.add({
        title: 'Documento subido',
        time: 1000,
      });

      getDocsGear();
    }
  })//end then
  .catch(function(error) {
    if (error.response.data.detail)
      _error(error.response.data.detail);
    else
      handleErrorAxios(error);  });
}



delete_pic = (id) => {
  var url = DjangoURL('pic_api_detail', id);
  delete_object(null, 'foto', url, null, function(){
      getPicsGear();
  });
}

delete_doc = (id) => {
  var url = DjangoURL('doc_api_detail', id);
  delete_object(null, 'documento', url, null, function(){
      getDocsGear();
  });
}

fill_update = (caption, id) => {
    $("#pic_edit_foot").val(caption)
    $("#pic_edit_id").val(id)
}

editGallery = () => {
  console.log('olaaaaa')
  
  var id = $("#pic_edit_id").val()
  console.log(id)
 
  var formData = new FormData();
  formData.append('caption', $("#pic_edit_foot").val());
 

  var url = DjangoURL('pic_api_detail', id);
  axios.patch(url, formData, {
    headers: { 
      Authorization: 'jwt '+ HDD.get(Django.name_jwt),
    },
  })
  .then(function(response){
    console.log(response);
    if (response.status == 200)
    {
    

      getPicsGear();    
      $.gritter.add({
        title: 'Foto actualizada',
        time: 1000,
      });

      $('#modal-edit-gallery').modal('hide')

    }
  })//end then
  .catch(function(error) {
    if (error.response.data.detail)
      _error(error.response.data.detail);
    else
      handleErrorAxios(error);
  });
}


fill_update_doc = (caption, id) => {
    $("#doc_edit_description").val(caption)
    $("#doc_edit_id").val(id)
}

editDoc = () => {
  console.log('olaaaaa')
  
  var id = $("#doc_edit_id").val()
  console.log(id)
 
  var formData = new FormData();
  formData.append('description', $("#doc_edit_description").val());
 

  var url = DjangoURL('doc_api_detail', id);
  axios.patch(url, formData, {
    headers: { 
      Authorization: 'jwt '+ HDD.get(Django.name_jwt),
    },
  })
  .then(function(response){
    console.log(response);
    if (response.status == 200)
    {
    

      getDocsGear();    
      $.gritter.add({
        title: 'Documento actualizado',
        time: 1000,
      });
      $('#modal-edit-doc').modal('hide')


    }
  })//end then
  .catch(function(error) {

    if (error.response.data.detail)
      _error(error.response.data.detail);
    else
      handleErrorAxios(error);
  });
}








function getNodes (id) {  
  $("#node").empty();
  $("#node").append('<option value="" >Nodo</option>');
  $("#node").append('<option value="" >Nodo</option>');
  var url = DjangoURL('api_gear_nodes_list');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++)
        $("#node").append(`<option value="${res.data[i].node__id}">${res.data[i].node__alias}</option>`);


      $("#node").val(id);
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}


function getModels(id) {
  $("#model").empty();
  $("#model").append('<option value="" >Modelo</option>');

  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };

  var url = DjangoURL('api_model_device_list');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        $("#model").append(`<option value="${res.data[i].id}">${res.data[i].model}</option>`);
      }

      $("#model").val(id);

    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}



function editGear() {

  $('#modal-create').modal('hide');
  $('#modal-create-sleep').modal('show');
  var url = DjangoURL('api_gear_detail', gear_id)+'/'
  var data = { 
    "ip": $("#ip").val(),
    "node": $("#node").val(),
    "description": $("#description").val(),
    "mac": $("#mac").val(),
    "vlan": $("#vlan").val(),   
    "notes": $("#notes").val(), 
    "firmware_version": $("#firmware").val(),
    "model": $("#model").val(),

    // "nodes_olt": serealizeSelects($('#nodes_olt'))
  }
  console.log(data)

  axios.patch(url, data, conf)
  .then(function(res){
    console.log(res)
    if (res.status == 200)
    {
      $('#modal-create-sleep').modal('hide');
      
      getDetailsGear();
      

      swal({
      title: "Actualizado",
        text: 'Equipo actualizado exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        updateDatatable();      
        $('#modal-create-sleep').modal('hide');
      });
    }

      $('#error_firmware').text('')
      $('#error_node').text('')
      $('#error_vlan').text('')
      $('#error_ip').text('')
      $('#error_mac').text('')
      $('#error_model').text('')
      $('#error_notes').text('')

      $('#div_firmware').removeClass('has-error')
      $('#div_node').removeClass('has-error')
      $('#div_vlan').removeClass('has-error')
      $('#div_ip').removeClass('has-error')
      $('#div_mac').removeClass('has-error')
      $('#div_model').removeClass('has-error')
      $('#div_notes').removeClass('has-error')

  })//end then
  .catch(function(error) {
        console.log(error.response)
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){
          $('#modal-create-sleep').modal('hide');
          $('#modal-create').modal('show')
        });
      }
      else{
        _error("Por favor verifique los datos ingresados.").then(function(){
          $('#modal-create-sleep').modal('hide');
          $('#modal-create').modal('show')
        });
      }

      $('#error_firmware').text('')
      $('#error_node').text('')
      $('#error_vlan').text('')
      $('#error_ip').text('')
      $('#error_mac').text('')
      $('#error_model').text('')
      $('#error_notes').text('')

      $('#div_firmware').removeClass('has-error')
      $('#div_node').removeClass('has-error')
      $('#div_vlan').removeClass('has-error')
      $('#div_ip').removeClass('has-error')
      $('#div_mac').removeClass('has-error')
      $('#div_model').removeClass('has-error')
      $('#div_notes').removeClass('has-error')

      if (error.response.data.firmware_version){
        $("#div_firmware").addClass('has-error')
        $('#error_firmware').text(error.response.data.firmware_version.error ? error.response.data.firmware_version.error:error.response.data.firmware_version[0])
      }
      if (error.response.data.node){
        $("#div_node").addClass('has-error')
        $('#error_node').text(error.response.data.node.error ? error.response.data.node.error:error.response.data.node[0])
      }
      if (error.response.data.vlan){
        $("#div_vlan").addClass('has-error')
        $('#error_vlan').text(error.response.data.vlan.error ? error.response.data.vlan.error:error.response.data.vlan[0])
      }
      if (error.response.data.ip){
        $("#div_ip").addClass('has-error')
        $('#error_ip').text(error.response.data.ip.error ? error.response.data.ip.error:error.response.data.ip[0])
      }
      if (error.response.data.mac){
        $("#div_mac").addClass('has-error')
        $('#error_mac').text(error.response.data.mac.error ? error.response.data.mac.error:error.response.data.mac[0])
      }
      if (error.response.data.model){
        $("#div_model").addClass('has-error')
        $('#error_model').text(error.response.data.model.error ? error.response.data.model.error:error.response.data.model[0])
      }
      if (error.response.data.description){
        $("#div_description").addClass('has-error')
        $('#error_description').text(error.response.data.description.error ? error.response.data.description.error:error.response.data.description[0])
      }
      if (error.response.data.notes){
        $("#div_notes").addClass('has-error')
        $('#error_notes').text(error.response.data.notes.error ? error.response.data.notes.error:error.response.data.notes[0])
      }

    }

    if (error.response.status == 500)
    {

      _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){
        $('#modal-create-sleep').modal('hide');
      });
      
    }     
     
    //handleErrorAxios(error);
  });
  
};


