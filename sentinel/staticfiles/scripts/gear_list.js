userInSession();
App.setPageTitle('Sentinel - Lista de equipos internos');

//$("#menu_olt > a ").css('cssText', "color: #FFF !important;");
$("#menu_switch").addClass('active');
$("#menu_gear_list").addClass('active');
var oTable;
var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};


$(document).ready(function (){

  // getNodesOLT();

  url_dt = Django.datatable_gear;
  oTable = $('#table_gear').dataTable({
      "aaSorting":[[0,"desc"]],
      "language": {
      "sLengthMenu":     "Mostrar _MENU_ equipos",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando equipos del _START_ al _END_ de un total de _TOTAL_ equipos",
      "sInfoEmpty":      "Mostrando equipos del 0 al 0 de un total de 0 equipos",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ equipos)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "sProcessing": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },

      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
      "processing": true,
      "serverSide": true,
      "searchDelay": 400,
      "ajax": {
          "url": url_dt,
          "type": "POST",
          "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
          data: function (d) {
            d.filter_node = $("#filter_node").val();
            d.filter_model = $("#filter_model").val();
            d.filter_brand = $("#filter_brand").val();
            d.filter_vlan = $("#filter_vlan").val();
          },
      },
      "columnDefs": [
      {
        orderable: true,
        searchable: true,
        targets: [0,1,2,3,4,5,6]
      },
      {
        orderable: false,
        searchable: false,
        targets: [7]
      }
      ], 
  });

  oTable.on( 'order.dt', function (e, settings) {

     var order = oTable.api().order();
     $("#table_gear > thead > tr > th").css('color','black')
     $( "#table_gear > thead > tr > th:eq( "+order[0][0]+" )" )
     .css( "color", "#2271b3" );

  });

  getModels();
  getNodes();
  getVlans();  
  getBrands();
  $('.modal').css('overflow-y', 'auto');

  $('#filter_node').select2({});  
  $('#filter_vlan').select2({});
  $('#filter_brand').select2({});   
  $('#filter_model').select2({});

  $('#node').select2({
    dropdownParent: $('#div_node'),
    width: 'resolve',
    language: {

      noResults: function() {
  
        return "No hay resultado";        
      },
      searching: function() {
  
        return "Buscando..";
      },
      inputTooShort: function() {
        return 'Al menos 2 letras';
      }
    },
    ajax: {
      url: DjangoURL('api_node_list'),
      data: function (params) {
        var query = {
          search: params.term
        }
  
        // Query parameters will be ?search=[term]
        return query;
      },
      processResults: function (data, params) {
        console.log(data);
        var data = $.map(data, function (obj) {
          obj.text = obj.alias; // replace name with the property used for the text
          return obj;
        });

        return {
            results: data,
            pagination: {
                more: false
            }
        };
      }
    },
    minimumInputLength: 2
  });
  $('#model').select2({
    dropdownParent: $('#modal-create'),
    width: 'resolve',
    language: {

      noResults: function() {
  
        return "No hay resultado";        
      },
      searching: function() {
  
        return "Buscando..";
      }
    }
  });
  


  $('#filter_brand').change(function(){
    oTable.api().ajax.reload(null, false);
  })
  $('#filter_node').change(function(){
    oTable.api().ajax.reload(null, false);
  })
    $('#filter_vlan').change(function(){
    oTable.api().ajax.reload(null, false);
  })  
  $('#filter_model').change(function(){
    oTable.api().ajax.reload(null, false);
  })


});  

function getNodes () {  
  $("#filter_node").empty();
  $("#filter_node").append('<option value="" >Nodo</option>');
  // $("#node").append('<option value="" >Nodo</option>');
  var url = DjangoURL('api_gear_nodes_list');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        // $("#nodes_olt").append(`<option value="${res.data[i].id}">${res.data[i].alias}</option>`);
        $("#filter_node").append(`<option value="${res.data[i].node__id}">${res.data[i].node__alias} (${res.data[i].count} equipo/s)</option>`);
        // $("#node").append(`<option value="${res.data[i].node__id}">${res.data[i].node__alias}</option>`);
      }


    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

function getVlans ()  {  
  $("#filter_vlan").empty();
  $("#filter_vlan").append('<option value="" >VLAN</option>');
  var url = DjangoURL('api_gear_vlans_list');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        // $("#nodes_olt").append(`<option value="${res.data[i].id}">${res.data[i].alias}</option>`);
        $("#filter_vlan").append(`<option value="${res.data[i].vlan}">${res.data[i].vlan} (${res.data[i].count} equipo/s)</option>`);
      }


    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}


function getBrands () {  
  $("#filter_brand").empty();
  $("#filter_brand").append('<option value="" >Marca</option>');
  var url = DjangoURL('api_gear_brand_list');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        // $("#nodes_olt").append(`<option value="${res.data[i].id}">${res.data[i].alias}</option>`);
        $("#filter_brand").append(`<option value="${res.data[i].model__brand}">${res.data[i].model__brand} (${res.data[i].count} equipo/s)</option>`);
      }

    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}


function getModels() {
  $("#filter_model").empty();
  $("#filter_model").append('<option value="" >Modelo</option>');
  $("#model").empty();
  $("#model").append('<option value="" >Modelo</option>');

  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };
  var url = DjangoURL('api_gear_model_list');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        $("#filter_model").append(`<option value="${res.data[i].model__model}">${res.data[i].model__model} (${res.data[i].count} equipo/s)</option>`);
      }

    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });



  var url = DjangoURL('api_model_device_list');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        $("#model").append(`<option value="${res.data[i].id}">${res.data[i].model}</option>`);
      }

    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

Parsley.on('form:init', function() {
  /// get nodes
  /// getNodesOLT();
});

/**
 * Convert select to array with values
 */    
function serealizeSelects (select)
{
    var array = [];
    select.each(function(){ array.push($(this).val()) });
    return array;
}


function newGear() {

  $('#modal-create').modal('hide');
  $('#modal-create-sleep').modal('show');
  var url = DjangoURL('api_gear_list')+'/'
  var data = { 
    "ip": $("#ip").val(),
    "node": $("#node").val(),
    "description": $("#description").val(),
    "mac": $("#mac").val(),
    "vlan": $("#vlan").val(),   
    "notes": $("#notes").val(), 
    "firmware_version": $("#firmware").val(),
    "model": $("#model").val(),

    // "nodes_olt": serealizeSelects($('#nodes_olt'))
  }
  console.log(data)

  axios.post(url, data, conf)
  .then(function(res){
    console.log(res)
    if (res.status == 201)
    {
      $('#modal-create-sleep').modal('hide');

      oTable.api().ajax.reload(null, false);
  
      swal({
      title: "Creado",
        text: 'Equipo Creado exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        updateDatatable();
      });
    }

      $('#error_firmware').text('')
      $('#error_node').text('')
      $('#error_vlan').text('')
      $('#error_ip').text('')
      $('#error_mac').text('')
      $('#error_model').text('')
      $('#error_notes').text('')

      $('#div_firmware').removeClass('has-error')
      $('#div_node').removeClass('has-error')
      $('#div_vlan').removeClass('has-error')
      $('#div_ip').removeClass('has-error')
      $('#div_mac').removeClass('has-error')
      $('#div_model').removeClass('has-error')
      $('#div_notes').removeClass('has-error')

  })//end then
  .catch(function(error) {
        console.log(error.response)
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){
          $('#modal-create-sleep').modal('hide');
          $('#modal-create').modal('show')
        });
      }
      else{
        _error("Por favor verifique los datos ingresados.").then(function(){
          $('#modal-create-sleep').modal('hide');
          $('#modal-create').modal('show')
        });
      }

      $('#error_firmware').text('')
      $('#error_node').text('')
      $('#error_vlan').text('')
      $('#error_ip').text('')
      $('#error_mac').text('')
      $('#error_model').text('')
      $('#error_notes').text('')

      $('#div_firmware').removeClass('has-error')
      $('#div_node').removeClass('has-error')
      $('#div_vlan').removeClass('has-error')
      $('#div_ip').removeClass('has-error')
      $('#div_mac').removeClass('has-error')
      $('#div_model').removeClass('has-error')
      $('#div_notes').removeClass('has-error')

      if (error.response.data.firmware_version){
        $("#div_firmware").addClass('has-error')
        $('#error_firmware').text(error.response.data.firmware_version.error ? error.response.data.firmware_version.error:error.response.data.firmware_version[0])
      }
      if (error.response.data.node){
        $("#div_node").addClass('has-error')
        $('#error_node').text(error.response.data.node.error ? error.response.data.node.error:error.response.data.node[0])
      }
      if (error.response.data.vlan){
        $("#div_vlan").addClass('has-error')
        $('#error_vlan').text(error.response.data.vlan.error ? error.response.data.vlan.error:error.response.data.vlan[0])
      }
      if (error.response.data.ip){
        $("#div_ip").addClass('has-error')
        $('#error_ip').text(error.response.data.ip.error ? error.response.data.ip.error:error.response.data.ip[0])
      }
      if (error.response.data.mac){
        $("#div_mac").addClass('has-error')
        $('#error_mac').text(error.response.data.mac.error ? error.response.data.mac.error:error.response.data.mac[0])
      }
      if (error.response.data.model){
        $("#div_model").addClass('has-error')
        $('#error_model').text(error.response.data.model.error ? error.response.data.model.error:error.response.data.model[0])
      }
      if (error.response.data.description){
        $("#div_description").addClass('has-error')
        $('#error_description').text(error.response.data.description.error ? error.response.data.description.error:error.response.data.description[0])
      }
      if (error.response.data.notes){
        $("#div_notes").addClass('has-error')
        $('#error_notes').text(error.response.data.notes.error ? error.response.data.notes.error:error.response.data.notes[0])
      }

    }

    if (error.response.status == 500)
    {

      _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){
        $('#modal-create-sleep').modal('hide');
      });
      
    }     
     
    //handleErrorAxios(error);
  });
  
};


delete_olt = (id) => {    
  delete_object(id, 'gear', null, null, function(){
    oTable.api().ajax.reload(null, false);
  });
}





$(window).keydown(function(e){
  //console.log(e)
});

keydownTaskOLT = (e) => {
  if (e.shiftKey && e.keyCode === 70){
    console.log(e)
  }
}

ayuda = () => {
  alert('Pagina de ayuda de gear')
}