userInSession();
App.setPageTitle('Sentinel - Listado de IPs' );

$("#menu_configuraciones").addClass('active');
$("#menu_iptable").addClass('active');

var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};

$(document).ready(function (){
  url_dt = '/ipt-settings-list';
  
  language =(palabraClave='registros')=>{
      return {
        sLengthMenu:     `Mostrar _MENU_ ${palabraClave}`,
        sZeroRecords:    `No se encontraron resultados`,
        sEmptyTable:     `Ningún dato disponible en esta tabla`,
        sInfo:           `Mostrando ${palabraClave} del _START_ al _END_ de un total de _TOTAL_ ${palabraClave}`,
        sInfoEmpty:      `Mostrando ${palabraClave} del 0 al 0 de un total de 0 ${palabraClave}`,
        sInfoFiltered:   `(filtrado de un total de _MAX_ ${palabraClave})`,
        sInfoPostFix:    "",
        sSearch:         `Buscar:`,
        sUrl:            "",
        sInfoThousands:  ",",
        sLoadingRecords: `Cargando...`,
        sProcessing: `Cargando...`,
        oPaginate: {
          sFirst:    "Primero",
          sLast:     "Último",
          sNext:     "Siguiente",
          sPrevious: "Anterior"
        },

        oAria: {
          sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
          sSortDescending: ": Activar para ordenar la columna de manera descendente"
        }
      };
  } 
  
  oTable = $('#table_ips').dataTable({
        "aaSorting":[[0,"asc"]],
        "language": language('IPs'),
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": url_dt,
            "type": "POST",
            "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
        },

        "columnDefs": [
          {
              orderable: true,
              searchable: true,
              targets: [0]
          },
          {
              orderable: true,
              searchable: true,
              className:'text-center',
              targets: [1,2,3]
          },
          {
              orderable: false,
              searchable: false,
              className:'text-center',
              targets: [4]
          }
          ],


   });

}); 


url_iptable = '/api/v1/iptables/';
modalDetail = function(obj){
  var ip = new SPA('Detalle de la IP ' + obj.ip,'light');

  var content = ip.RowTable('IP',obj.ip)
  content += ip.RowTable('Host',obj.host)
  content += ip.RowTable('Menú',obj.menu)
  content += ip.RowTable('Titulo',obj.title)

  var html = ip.TableBody('IP '+obj.ip, content)

  $(ip.modalId)
  .empty()
  .append(html)
  .modal({show:true})

}

loadModal = function(){
  var ip = new SPA('Generar una nueva IP','success');

  var content = ip.Input('IP','ip','text')
  content += ip.Input('Host','host','text')
  content += ip.Input('Menú','menu','text')
  content += ip.Input('Titulo','title','text')

  var footer = ip.BtnAction()
  footer += ip.BtnAction('Agregar','function','newIP',0)

  var html = ip.modalBody(content,footer);

  $(ip.modalId)
  .empty()
  .append(html)
  .modal({show:true})

}

newIP = function(id=0){

  var data = {
    ip:$("#ip").val(),
    host:$("#host").val(),
    menu:$("#menu").val(),
    title:$("#title").val(),
  }

  validator = [];
  if(ValidatorField(data,'ip','ip')){
    validator.push(true);
  };
  if(ValidatorField(data,'host','host')){
    validator.push(true);
  };
  if(ValidatorField(data,'menu','menú')){
    validator.push(true);
  };
  if(ValidatorField(data,'title','titulo')){
    validator.push(true);
  };

  if(validator.length==0){
    axios.post(url_iptable,data,conf).then((response)=>{
      if(response.status == 201)
      {
        _exito('Se ha creado una nueva IP');
        $("#modal_create").modal('toggle')
        setTimeout(updateDatatable(),40)
      }
    })
    .catch((error)=>{
      handleErrorAxios(error)
    })
  }


}


loadModalEdit = function(obj){
  var ip = new SPA('Editar la IP '+obj.ip,'warning');

  var content = ip.Input('IP','ip','text',obj.ip)
  content += ip.Input('Host','host','text',obj.host)
  content += ip.Input('Menú','menu','text',obj.menu)
  content += ip.Input('Titulo','title','text',obj.title)

  var footer = ip.BtnAction()
  footer += ip.BtnAction('Editar','function','editIP',obj.id)

  var html = ip.modalBody(content,footer);

  $(ip.modalId)
  .empty()
  .append(html)
  .modal({show:true})

}

editIP = function(id){

  var data = {
    ip:$("#ip").val(),
    host:$("#host").val(),
    menu:$("#menu").val(),
    title:$("#title").val(),
  }

  validator = [];
  if(ValidatorField(data,'ip','ip')){
    validator.push(true);
  };
  if(ValidatorField(data,'host','host')){
    validator.push(true);
  };
  if(ValidatorField(data,'menu','menú')){
    validator.push(true);
  };
  if(ValidatorField(data,'title','titulo')){
    validator.push(true);
  };

  if(validator.length==0){
    url_iptable = url_iptable + id + '/'; 
    axios.put(url_iptable,data,conf).then((response)=>{
      if(response.status == 200)
      {
        _exito('Se ha editado satisfactoriamente la IP');
        $("#modal_create").modal('toggle')
        setTimeout(updateDatatable(),40)
      }
    })
    .catch((error)=>{
      handleErrorAxios(error)
    })
  }


}


deleteIP = (id) => {
  swal({
    title: `¿Eliminar la IP?`,
    text: "¿Realmente esta seguro?, Recuerde que esta acción no se puede deshacer",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      url = `/api/v1/iptables/${id}/`
      axios.delete(url,conf).then((response)=>{
        if(response.status == 204)
        {
          _exito('La IP ha sido eliminada satisfactoriamente')
          setTimeout(updateDatatable(),40)
        }
      })
      .catch((error)=>{
        handleErrorAxios(error)
      })

    } else {
      _info("Su IP está a salvo.");
    }
  });
}