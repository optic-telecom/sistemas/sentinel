function login() {

    axios.post('/api/v1/auth/obtain_token/', {
        username: $("#username").val(),
        password: $("#password").val()
    })
    .then(function(response){
      if(response.status == 200)
      {
        HDD.set(Django.name_jwt,response.data.token);
        HDD.set('username', $("#username").val());
        Cookies.set(Django.name_jwt, response.data.token);
        var next = location.hash;
        if (next)
        {
            setTimeout(function(){
                location.href = location.origin + '/' + location.hash;
            }, 50);
        }
        else
        {
            setTimeout(function(){ location.href = '/'; }, 50);
        }
      }
      
    })
    .catch(function(error) {
        if (error.response)
        {
            if(error.response.status == 400){
                _info('No se puede iniciar sesion con los datos proporcionados');
            }
        }
        else
        {
            _info('Error en el servidor');
            handleErrorAxios(error);
        }

    });

}

$("#btn_login").on("click",function(){
    login();
});

$("#password").keypress(function(e) {
    //mayor compatibilidad entre navegadores.   
    var code = (e.keyCode ? e.keyCode : e.which);
    if(code==13){
        login();
        return false;         
    }
});