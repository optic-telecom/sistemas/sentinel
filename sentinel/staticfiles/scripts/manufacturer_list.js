userInSession();
App.setPageTitle('Sentinel - Fabricantes');

//$("#menu_Extremo > a ").css('cssText', "color: #FFF !important;");
$("#menu_fmm").addClass('active');
$("#menu_fmm_manufacturer").addClass('active');
var oTable;
var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};


$(document).ready(function (){

  url_dt = Django.datatable_manufacturer_list;
  oTable = $('#table_manufacturer').dataTable({
      "aaSorting":[[0,"desc"]],
      "language": {
      "sLengthMenu":     "Mostrar _MENU_ fabricantes",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando fabricante del _START_ al _END_ de un total de _TOTAL_ fabricantes",
      "sInfoEmpty":      "Mostrando fabricante del 0 al 0 de un total de 0 fabricantes",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ fabricantes)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "sProcessing": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },

      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
      "processing": true,
      "serverSide": true,
      "searchDelay": 400,
      "ajax": {
          "url": url_dt,
          "type": "POST",
          "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
      },
      "columnDefs": [
        {
          orderable: true,
          searchable: true,
          targets: [0,1]
        },
        {
          orderable: false,
          searchable: false,
          targets: [2]
        }
        ], 
  });
    
  oTable.on( 'order.dt', function (e, settings) {

    var order = oTable.api().order();
    $("#table_manufacturer > thead > tr > th").css('color','black')
    $( "#table_manufacturer > thead > tr > th:eq( "+order[0][0]+" )" )
    .css( "color", "#2271b3" );

  });

  // Error messages
  const customMessages = {
    valueMissing:    'Campo obligatorio',       // `required` attr
  }

  function getCustomMessage (type, validity) {
    if (validity.typeMismatch) {
      return customMessages[`${type}Mismatch`]
    } else {
      for (const invalidKey in customMessages) {
        if (validity[invalidKey]) {
          return customMessages[invalidKey]
        }
      }
    }
  }

  var invalidClassName = 'invalid'
  var inputs = document.querySelectorAll('input, select, textarea')
  inputs.forEach(function (input) {
    // Each time the user types or submits, this will
    // check validity, and set a custom message if invalid.
    function checkValidity () {
      const message = input.validity.valid
        ? null
        : getCustomMessage(input.type, input.validity, customMessages)
      input.setCustomValidity(message || '')
      if (message!=null){
        input.classList.add(invalidClassName)
      }else{
        input.classList.remove(invalidClassName)
      }
      
    }
    input.addEventListener('input', checkValidity)
    input.addEventListener('invalid', checkValidity)

  })

  // Remove error messages when modal is closed
  $('#modal-create').on('hidden.bs.modal', function () {
    inputs.forEach(function (input) {
      input.classList.remove(invalidClassName)
    })
  })

  $('#modal-update').on('hidden.bs.modal', function () {
    inputs.forEach(function (input) {
      input.classList.remove(invalidClassName)
    })
  })

});  

function newManufacturer() {

  $('#modal-create').modal('hide');
  $('#modal-create-sleep').modal('show');
  var url = Django.FMM_URL + '/api/v1/manufacturer/'
  var data = { 
    "name": $("#name").val()
  }

  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 201)
    {
      // $('#modal-create-sleep').modal('hide');
      swal({
      title: "Creado",
        text: 'Fabricante agregado exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        updateDatatable();
        $('#modal-create-sleep').modal('hide');
      });
      $("#name").val('')
    }
  
  })//end then
  .catch(function(error) {
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }else{
        _error("Por favor verifique los datos ingresados.").then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }
    }

    if (error.response.status == 500)
    {

      _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){
        $('#modal-create-sleep').modal('hide');
      });
      
    }     
     
    //handleErrorAxios(error);
  });
  return false;
};

function getDetailsManufacturer(id) {
  var url = DjangoURL('api_manufacturer_detail', id)
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      $("#det_name").val(res.data.name);
      $("#update-form").attr("onsubmit","return updateManufacturer("+id+")");
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

function updateManufacturer(id) {

  $('#modal-update').modal('hide');
  $('#modal-create-sleep').modal('show');
  var url = DjangoURL('api_manufacturer_detail', id)
  var data = { 
    "name": $("#det_name").val()
  }
  axios.patch(url, data, conf)
  .then(function(res){
    if (res.status == 200)
    {
      // $('#modal-create-sleep').modal('hide');
      swal({
      title: "Moficado",
        text: 'Fabricante modificado exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        updateDatatable();
        $('#modal-create-sleep').modal('hide');
      });
    }
  
  })//end then
  .catch(function(error) {
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }else{
        _error("Por favor verifique los datos ingresados.").then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }
    }

    if (error.response.status == 500)
    {

      _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){
        $('#modal-create-sleep').modal('hide');
      });
      
    }     
     
    //handleErrorAxios(error);
  });
  return false;
};

delete_manufacturer = (id) => {
  var url = Django.FMM_URL + '/api/v1/manufacturer/' + id + '/';
  delete_object(null, 'Fabricante', url, null, function(){
    oTable.api().ajax.reload(null, false);
  });
}


ayuda = () => {
  alert('Pagina de ayuda de fmm')
}