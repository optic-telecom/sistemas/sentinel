userInSession();
App.setPageTitle('Sentinel - Mufas');

//$("#menu_Extremo > a ").css('cssText', "color: #FFF !important;");
$("#menu_fmm").addClass('active');
$("#menu_fmm_mufa").addClass('active');
var oTable;
var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};


$(document).ready(function (){

  getPhysicalExtremes(detail=false);
  getPhysicalExtremes(detail=true);

  url_dt = Django.datatable_mufa_list;
  oTable = $('#table_mufa').dataTable({
      "aaSorting":[[0,"desc"]],
      "language": {
      "sLengthMenu":     "Mostrar _MENU_ mufas",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando mufa del _START_ al _END_ de un total de _TOTAL_ mufas",
      "sInfoEmpty":      "Mostrando mufa del 0 al 0 de un total de 0 mufas",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ mufas)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "sProcessing": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },

      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
      "processing": true,
      "serverSide": true,
      "searchDelay": 400,
      "ajax": {
          "url": url_dt,
          "type": "POST",
          "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
      },
      "columnDefs": [
      {
        orderable: true,
        searchable: true,
        targets: [0,1,2,3]
      },
      {
        orderable: false,
        searchable: false,
        targets: [4]
      }
      ], 
  });

  oTable.on( 'order.dt', function (e, settings) {

     var order = oTable.api().order();
     $("#table_mufa > thead > tr > th").css('color','black')
     $( "#table_mufa > thead > tr > th:eq( "+order[0][0]+" )" )
     .css( "color", "#2271b3" );

  });

  // Error messages
  const customMessages = {
    valueMissing:    'Campo obligatorio',       // `required` attr
    rangeUnderflow: 'Debe ser un número mayor a cero'
  }

  function getCustomMessage (type, validity) {
    if (validity.typeMismatch) {
      return customMessages[`${type}Mismatch`]
    } else {
      for (const invalidKey in customMessages) {
        if (validity[invalidKey]) {
          return customMessages[invalidKey]
        }
      }
    }
  }

  var invalidClassName = 'invalid'
  var inputs = document.querySelectorAll('input, select, textarea')
  inputs.forEach(function (input) {
    // Each time the user types or submits, this will
    // check validity, and set a custom message if invalid.
    function checkValidity () {
      const message = input.validity.valid
        ? null
        : getCustomMessage(input.type, input.validity, customMessages)
      input.setCustomValidity(message || '')
      if (message!=null){
        input.classList.add(invalidClassName)
      }else{
        input.classList.remove(invalidClassName)
      }
      
    }
    input.addEventListener('input', checkValidity)
    input.addEventListener('invalid', checkValidity)

  })

  // Remove error messages when modal is closed
  $('#modal-create').on('hidden.bs.modal', function () {
    inputs.forEach(function (input) {
      input.classList.remove(invalidClassName)
    })
  })

  $('#modal-detail').on('hidden.bs.modal', function () {
    inputs.forEach(function (input) {
      input.classList.remove(invalidClassName)
    })
  })

});  

getPhysicalExtremes = (detail) => {  
  var url = DjangoURL('api_physicalextreme_list');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      if(detail){
        $("#det_physical_ext").append(`<option id="first" value="">Seleccione un extremo</option>`);
        for (var i = 0; i < res.data.length; i++) {
          $("#det_physical_ext").append(`<option value="${res.data[i].id}">${res.data[i].address}, ${res.data[i].commune}, ${res.data[i].location}</option>`);
        }
      }else{
        $("#physical_ext").append(`<option id="first-create" value="">Seleccione un extremo</option>`);
        for (var i = 0; i < res.data.length; i++) {
          $("#physical_ext").append(`<option value="${res.data[i].id}">${res.data[i].address}, ${res.data[i].commune}, ${res.data[i].location}</option>`);
        }
      }      
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

function newMufa() {

  $('#modal-create').modal('hide');
  $('#modal-create-sleep').modal('show');
  var url = Django.FMM_URL + '/api/v1/mufa/'
  var data;
  data = { 
    "in_outs": $("#in_outs").val(),
    "plates": $("#plates").val(),
    "comment": $("#comment").val(),
    "physical_ext": $('#physical_ext').val()
  }
  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 201)
    {
      // $('#modal-create-sleep').modal('hide');
      swal({
      title: "Creado",
        text: 'Mufa creada exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        updateDatatable();
        $('#modal-create-sleep').modal('hide');
      });
      $("#in_outs").val('');
      $("#plates").val('');
      $("#comment").val('');
      $("#first-create").attr("selected","selected");
    }
  
  })//end then
  .catch(function(error) {
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }else{
        _error("Por favor verifique los datos ingresados.").then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }
    }

    if (error.response.status == 500)
    {

      _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){
        $('#modal-create-sleep').modal('hide');
      });
      
    }     
     
    //handleErrorAxios(error);
  });
  return false;
};

function getDetailsMufa(id) {
  var url = DjangoURL('api_mufa_detail', id)
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      $("#det_in_outs").val(res.data.in_outs);
      $("#det_plates").val(res.data.plates);
      $("#det_comment").text(res.data.comment);
      if (res.data.physical_ext==null){
          $("#first").attr("selected","selected");
      }else{
        $("#det_physical_ext > option").each(function() {
          if ($(this).val()==res.data.physical_ext)
          {
            $(this).attr("selected","selected");
          }
        });
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

function allowEdit(id, edit) {
  if (edit)
  {
    // Modal header
    $("#detail").prop("hidden",true);
    $("#edit").prop("hidden",false);

    // Buttons for edit/save
    $("#btn_edit").prop("hidden",true);
    $("#btn_update").prop("hidden",false);
    $("#update-form").attr("onsubmit","return updateMufa("+id+")");

    $("#det_in_outs").prop("readonly",false);
    $("#det_plates").prop("readonly",false);
    $("#det_comment").prop("readonly",false);
    $("#det_physical_ext").attr("readonly",false);
    
  }else{
    // Modal header
    $("#detail").prop("hidden",false);
    $("#edit").prop("hidden",true);

    // Buttons for edit/save
    $("#btn_edit").prop("hidden",false);
    $("#btn_update").prop("hidden",true);
    $("#btn_edit").attr("onclick","allowEdit("+id+", "+true+")");

    $("#det_in_outs").prop("readonly",true);
    $("#det_plates").prop("readonly",true);
    $("#det_comment").prop("readonly",true);
    $("#det_physical_ext").attr("readonly",true);
  }
}

function updateMufa(id) {

  $('#modal-detail').modal('hide');
  $('#modal-create-sleep').modal('show');
  var url = DjangoURL('api_mufa_detail', id)
  var data;
  data = { 
    "in_outs": $("#det_in_outs").val(),
    "plates": $("#det_plates").val(),
    "comment": $("#det_comment").val(),
    "physical_ext": $('#det_physical_ext').val()
  }
  axios.patch(url, data, conf)
  .then(function(res){
    if (res.status == 200)
    {
      swal({
      title: "Moficado",
        text: 'Mufa modificada exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        updateDatatable();
        $('#modal-create-sleep').modal('hide');
      });
    }
  
  })//end then
  .catch(function(error) {
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }else{
        _error("Por favor verifique los datos ingresados.").then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }
    }

    if (error.response.status == 500)
    {

      _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){
        $('#modal-create-sleep').modal('hide');
      });
      
    }     
      
    //handleErrorAxios(error);
  });
  return false;
};

delete_mufa = (id) => {
  var url = Django.FMM_URL + '/api/v1/mufa/' + id + '/';
  delete_object(null, 'Mufa', url, null, function(){
    oTable.api().ajax.reload(null, false);
  });
}


ayuda = () => {
  alert('Pagina de ayuda de fmm')
}