userInSession();
App.setPageTitle('Sentinel - Lista de Nodos');

$("#menu_nodos").addClass('active')

$(document).ready(function (){

  url_dt = Django.datatable_node;
  oTable = $('.table').dataTable({
        "aaSorting":[[0,"desc"]],
        "language": {
        "sLengthMenu":     "Mostrar _MENU_ Nodo",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando Nodo del _START_ al _END_ de un total de _TOTAL_ Nodo",
        "sInfoEmpty":      "Mostrando Nodo del 0 al 0 de un total de 0 Nodo",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ Nodo)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "sProcessing": "Cargando...",
        "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
        },

        "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      },
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": url_dt,
            "type": "POST",
            "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
        }
   });
});  




newNode = () => {
  $("#loader_node").html('<span class="spinner"></span>');
  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };
  var url = DjangoURL('api_node_list')
  var data = { 
    "name": $("#name").val(),
    "address": $("#address").val(),
    "community": $("#community").val()
  }

  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 201)
    {
      $("#loader_node").html('');
      $('#modal-create').modal('toggle');
      swal({
      title: "Creado",
        text: 'Nodo Creada exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        updateDatatable();
      });
    }
  
  })//end then
  .catch(function(error) {
    $("#loader_node").html('');
    handleErrorAxios(error);
  });
};