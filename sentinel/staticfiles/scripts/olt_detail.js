userInSession();
App.setPageTitle('Sentinel - Detalle de OLT');
var olt_uuid = getUrlLastParameter();
var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};

$("#menu_olt").addClass('active');
$("#menu_olt_general").addClass('active');


//* events *//
$(document).ready(function (){



  url_onus = Django.datatable_unu_olt.replace(':val:',olt_uuid);
  url_vlans = Django.datatable_olt_vlans.replace(':val:',olt_uuid);
  url_speeds = Django.datatable_olt_speeds.replace(':val:',olt_uuid);
  url_ports = Django.datatable_olt_ports.replace(':val:',olt_uuid);
  url_interfaces = Django.datatable_olt_interfaces.replace(':val:',olt_uuid);
  url_logs = Django.datatable_olt_logs.replace(':val:',olt_uuid);
  url_olt_users = Django.datatable_olt_users.replace(':val:',olt_uuid);
  url_olt_syslog = Django.datatable_syslog.replace(':val:',olt_uuid);

  url_filter_logtype = Django.select2_syslog
                    .replace('selector','log_type')
                    .replace('dispositivo','olt')
                    .replace(':val:',olt_uuid)

  url_filter_user = Django.select2_syslog
                    .replace('selector','username')
                    .replace('dispositivo','olt')
                    .replace(':val:',olt_uuid)

  url_filter_origin = Django.select2_syslog
                    .replace('selector','origin')
                    .replace('dispositivo','olt')
                    .replace(':val:',olt_uuid)


    


  

  table_onus_olt = $('#table_onus_olt').dataTable({
    "aaSorting":[[0,"desc"]],
      "language": {
      "sLengthMenu":     "Mostrar _MENU_ ONU",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando ONU del _START_ al _END_ de un total de _TOTAL_ ONU",
      "sInfoEmpty":      "Mostrando ONU del 0 al 0 de un total de 0 ONU",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ ONU)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },

      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
    "dom": '<"top">rt<"bottom"iflp><"clear">',
    "processing": true,
    "serverSide": true,
    "searchDelay": 400,
    "ajax": {
        "url": url_onus,
        "type": "POST",
        "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
        "data": function (d) {
            d.filter_status = $("#filter_status").val();
            d.filter_model = $("#filter_model").val();
            d.filter_fsp = $("#filter_fsp").val();
            d.filter_desprovisionadas = $("#filter_desprovisionadas").val();
        },
        "dataSrc": function (response) {
          var neighbors = response;
          if( neighbors.data[0])
            $('.onus_last_update').text(` ${neighbors.data[0].slice(-1)}`)
          else
            $('.onus_last_update').text('No disponible')
  
          return response.data
        },
    },
    "columnDefs": [
      {
        orderable: true,
        searchable: true,
        targets: [0,1,2,3,4,5,6,7,9]
      },
      {
        orderable: true,
        searchable: true,
        targets: [8]
      }
    ],    
  });


  table_vlans = $('#table_vlans').dataTable({
      "aaSorting":[[0,"desc"]],
      "language": {
      "sLengthMenu":     "Mostrar _MENU_ VLAN",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando VLAN del _START_ al _END_ de un total de _TOTAL_ VLAN",
      "sInfoEmpty":      "Mostrando VLAN del 0 al 0 de un total de 0 VLAN",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ VLAN)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending":  ":Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ":Activar para ordenar la columna de manera descendente"
        }
      },
      "processing": true,
      "serverSide": true,
      "searchDelay": 400,
      "ajax": {
          "url": url_vlans,
          "type": "POST",
          "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
      },
      "columnDefs": [
      {
        orderable: true,
        searchable: true,
        targets: [0,1,2,3,4]
      },
      {
        orderable: false,
        searchable: false,
        targets: [5]
      }
      ], 
  });


  table_speeds = $('#table_speeds').dataTable({
      "aaSorting":[[0,"desc"]],
      "language": {
      "sLengthMenu":     "Mostrar _MENU_ Velocidad",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando Velocidad del _START_ al _END_ de un total de _TOTAL_ Velocidad",
      "sInfoEmpty":      "Mostrando Velocidad del 0 al 0 de un total de 0 Velocidad",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ Velocidad)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending":  ":Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ":Activar para ordenar la columna de manera descendente"
        }
      },
      "processing": true,
      "serverSide": true,
      "searchDelay": 400,
      "ajax": {
          "url": url_speeds,
          "type": "POST",
          "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
      },
      "columnDefs": [
      {
        orderable: true,
        searchable: true,
        targets: [0,1,2,3,4,5,6,7]
      },
      {
        orderable: false,
        searchable: false,
        targets: [8]
      }
      ], 
  });

  table_ports = $('#table_ports').dataTable({
      "aaSorting":[[0,"desc"]],
      "language": {
      "sLengthMenu":     "Mostrar _MENU_ Puerto",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando Puerto del _START_ al _END_ de un total de _TOTAL_ Puertos",
      "sInfoEmpty":      "Mostrando Puerto del 0 al 0 de un total de 0 Puertos",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ Puertos)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending":  ":Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ":Activar para ordenar la columna de manera descendente"
        }
      },
      "processing": true,
      "serverSide": true,
      "searchDelay": 400,
      "ajax": {
          "url": url_ports,
          "type": "POST",
          "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
      },
      "columnDefs": [
      {
        orderable: true,
        searchable: true,
        targets: [0,1,2,3]
      },
      {
        orderable: true,
        searchable: false,
        targets: [4]
      }
      ], 
      "lengthMenu": [[-1, 20, 30, 50], [ "Todos", 20, 30, 50]],
      "footerCallback": function ( row, data, start, end, display ) {
          var api = this.api(), data;
          // Remove the formatting to get integer data for summation
          var intVal = function ( i ) {
              return typeof i === 'string' ?
                  i.replace(/[\$,]/g, '')*1 :
                  typeof i === 'number' ?
                      i : 0;
          };

          // Total over all pages
          total = api
              .column( 4 )
              .data()
              .reduce( function (a, b) {
                  return intVal(a) + intVal(b);
              }, 0 );

          // Total over this page
          pageTotal = api
              .column( 3, { page: 'current'} )
              .data()
              .reduce( function (a, b) {
                  return intVal(a) + intVal(b);
              }, 0 );

          // Update footer
          $( api.column( 3 ).footer() ).html(
              'ONUs Online del grupo ' + pageTotal
          );
          $( api.column( 4 ).footer() ).html(
              'ONUs Total del grupo ' + total
          );          
          
      },
  });

  table_interfaces = $('#table_interfaces').dataTable({
      "aaSorting":[[0,"desc"]],
      "language": {
      "sLengthMenu":     "Mostrar _MENU_ Interface",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando Interface del _START_ al _END_ de un total de _TOTAL_ Interface",
      "sInfoEmpty":      "Mostrando Interface del 0 al 0 de un total de 0 Interface",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ Interface)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending":  ":Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ":Activar para ordenar la columna de manera descendente"
        }
      },
      "processing": true,
      "serverSide": true,
      "searchDelay": 400,
      "ajax": {
          "url": url_interfaces,
          "type": "POST",
          "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
      },
      "lengthMenu": [[20, 30, 50, -1], [20, 30, 50, "Todos"]],
      "order": [[ 0, "asc" ]],
      "footerCallback": function ( row, data, start, end, display ) {
          var api = this.api(), data;
          // Remove the formatting to get integer data for summation
          var intVal = function ( i ) {
              return typeof i === 'string' ?
                  i.replace(/[\$,]/g, '')*1 :
                  typeof i === 'number' ?
                      i : 0;
          };

          // Total de onus
          onus = api
              .column( 2 )
              .data()
              .reduce( function (a, b) {
                  return intVal(a) + intVal(b);
              }, 0 );

          // Total de onus online
          onus_online = api
              .column( 3, { page: 'current'} )
              .data()
              .reduce( function (a, b) {
                  return intVal(a) + intVal(b);
              }, 0 );

          // Total de port
          ports = api
              .column( 4 )
              .data()
              .reduce( function (a, b) {
                  return intVal(a) + intVal(b);
              }, 0 );

          // Total de port usados
          ports_online = api
              .column( 5, { page: 'current'} )
              .data()
              .reduce( function (a, b) {
                  return intVal(a) + intVal(b);
              }, 0 );

          // Update footer
          $( api.column( 2 ).footer() ).html(
              ''+ onus
          );
          $( api.column( 3 ).footer() ).html(
              ''+ onus_online
          );          
          $( api.column( 4 ).footer() ).html(
              ''+ ports
          );
          $( api.column( 5 ).footer() ).html(
              ''+ ports_online
          );             
      },      
  });

  // table_logs = $('#table_logs').dataTable({
  //     "aaSorting":[[0,"desc"]],
  //     "language": {
  //     "sLengthMenu":     "Mostrar _MENU_ LOG",
  //     "sZeroRecords":    "No se encontraron resultados",
  //     "sEmptyTable":     "Ningún dato disponible en esta tabla",
  //     "sInfo":           "Mostrando LOG del _START_ al _END_ de un total de _TOTAL_ LOG",
  //     "sInfoEmpty":      "Mostrando LOG del 0 al 0 de un total de 0 LOG",
  //     "sInfoFiltered":   "(filtrado de un total de _MAX_ LOG)",
  //     "sInfoPostFix":    "",
  //     "sSearch":         "Buscar:",
  //     "sUrl":            "",
  //     "sInfoThousands":  ",",
  //     "sLoadingRecords": "Cargando...",
  //     "oPaginate": {
  //       "sFirst":    "Primero",
  //       "sLast":     "Último",
  //       "sNext":     "Siguiente",
  //       "sPrevious": "Anterior"
  //     },
  //     "oAria": {
  //       "sSortAscending":  ":Activar para ordenar la columna de manera ascendente",
  //       "sSortDescending": ":Activar para ordenar la columna de manera descendente"
  //       }
  //     },
  //     "processing": true,
  //     "serverSide": true,
  //     "searchDelay": 400,
  //     "ajax": {
  //         "url": url_logs,
  //         "type": "POST",
  //         "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
  //         "data": function (d) {
  //           d.filter_type_log = $("#filter_type_log").val();
  //           d.filter_type_action = $("#filter_type_action").val();
  //       },
  //     },
  // });

  table_olt_users = $('#table_olt_users').dataTable({
      "aaSorting":[[0,"desc"]],
      "language": {
      "sLengthMenu":     "Mostrar _MENU_ USERs",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando USERS del _START_ al _END_ de un total de _TOTAL_ USERs",
      "sInfoEmpty":      "Mostrando USERS del 0 al 0 de un total de 0 USERs",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ USERs)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending":  ":Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ":Activar para ordenar la columna de manera descendente"
        }
      },
      "processing": true,
      "serverSide": true,
      "searchDelay": 400,
      "ajax": {
          "url": url_olt_users,
          "type": "POST",
          "headers": { 'Authorization':  HDD.get(Django.name_jwt) }
      },
  });



// syslog calendar



// // SYSLOG TABLE
//   table_syslog = $('#table_syslog').dataTable({
//     "aaSorting":[[0,"desc"]],
//     "language": {
//     "sLengthMenu":     "Mostrar _MENU_ Syslog",
//     "sZeroRecords":    "No se encontraron resultados",
//     "sEmptyTable":     "Ningún dato disponible en esta tabla",
//     "sInfo":           "Mostrando Syslog del _START_ al _END_ de un total de _TOTAL_ Syslog",
//     "sInfoEmpty":      "Mostrando Syslog del 0 al 0 de un total de 0 Syslog",
//     "sInfoFiltered":   "(filtrado de un total de _MAX_ Syslog)",
//     "sInfoPostFix":    "",
//     "sSearch":         "Buscar:",
//     "sUrl":            "",
//     "sInfoThousands":  ",",
//     "sLoadingRecords": "Cargando...",
//     "oPaginate": {
//       "sFirst":    "Primero",
//       "sLast":     "Último",
//       "sNext":     "Siguiente",
//       "sPrevious": "Anterior"
//     },
//     "oAria": {
//       "sSortAscending":  ":Activar para ordenar la columna de manera ascendente",
//       "sSortDescending": ":Activar para ordenar la columna de manera descendente"
//       }
//     },
//     "processing": true,
//     "serverSide": true,
//     "searchDelay": 400,
//     "ajax": {
//         "url": url_olt_syslog,//'datatable/syslogs/',//url_olt_syslog,
//         "type": "POST",
//         "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
//         "data": function (d) {
//           d.advance_daterange = $("#advance-daterange").val();
//           d.filter_user_sys = $("#filter_user_sys").val();
//           d.filter_type_syslog_sys = $("#filter_type_syslog_sys").val();
//           d.filter_origin_syslog_sys = $("#filter_origin_syslog_sys").val();
//       },
//     },
// });


  table_onus_olt.on( 'order.dt', function (e, settings) {
     var order = table_onus_olt.api().order();
     $(".table_onus_olt > thead > tr > th").css('color','black')
     $( ".table_onus_olt > thead > tr > th:eq( "+order[0][0]+" )" )
     .css( "color", "#2271b3" );
  });

  table_vlans.on( 'order.dt', function (e, settings) {
     var order = table_vlans.api().order();
     $(".table_vlans > thead > tr > th").css('color','black')
     $( ".table_vlans > thead > tr > th:eq( "+order[0][0]+" )" )
     .css( "color", "#2271b3" );
  });

  table_speeds.on( 'order.dt', function (e, settings) {
     var order = table_speeds.api().order();
     $(".table_speeds > thead > tr > th").css('color','black')
     $( ".table_speeds > thead > tr > th:eq( "+order[0][0]+" )" )
     .css( "color", "#2271b3" );
  });
  
  /// get detail
  getDetailsOLT();
  getModelsONU();
  // setTimeout(function() {getEntitysLogs()}, 150);



  $('#filter_fsp').select2({
  });
  $("#select2-filter_fsp-container").css('width','220px');
  getFSP();

  $('#filter_status').change(function(){
    table_onus_olt.api().ajax.reload(null, false);
  })

  $('#filter_model').change(function(){
    table_onus_olt.api().ajax.reload(null, false);
  })

  $('#filter_desprovisionadas').change(function(){
    table_onus_olt.api().ajax.reload(null, false);
  })

  $('#filter_fsp').change(function(){
    table_onus_olt.api().ajax.reload(null, false);
  })

  // $('#filter_type_log').change(function(){
  //   table_logs.api().ajax.reload(null, false);
  // })
  $('#filter_type_action').change(function(){
    table_logs.api().ajax.reload(null, false);
  })

  $('#filter_orig_action').change(function(){
    table_logs.api().ajax.reload(null, false);
  })



  // SYSLOG FILTER

  // $.when(
  //   $('#advance-daterange').daterangepicker(jsonDateRange, function(start, end, label) {
  //       $('#advance-daterange input').attr({placeholder:start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY')});
  //   }),
  
  //   $('#advance-daterange').on('apply.daterangepicker', function(ev, picker) {
  
  //     var startDate = picker.startDate.format('YYYY-MM-DD');
  //     var endDate = picker.endDate.format('YYYY-MM-DD');
      
  //     $('#advance-daterange').val(startDate + ',' + endDate)
  //     table_syslog.api().ajax.reload(null, false);

  //   }),
  // )


  // $('#filter_user_sys').change(function(){
  //   table_syslog.api().ajax.reload(null, false);
  // })
  // $('#filter_type_syslog_sys').change(function(){
  //   table_syslog.api().ajax.reload(null, false);
  // })
  // $('#filter_origin_syslog_sys').change(function(){
  //   table_syslog.api().ajax.reload(null, false);
  // })



// function getSysLogSelect(url,id_select){
//   var conf = {
//     headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
//   };

//   axios.get(url,conf)
//   .then(function(response){
//     if(response.status===200){
//       var $data = response.data;

//       $(id_select).select2({
//         width:'100%',
//         data:$data
//       })

//     }

//   })
//   .catch(function(error){
//     handleErrorAxios(error)
//   })

// }

// getSysLogSelect('api/v1/s2-syslog/created','#filter_created_sys')

// // getSysLogSelect('api/v1/s2-syslog/ip','#filter_ip_sys')

// getSysLogSelect(url_filter_user,'#filter_user_sys')

// getSysLogSelect(url_filter_logtype,'#filter_type_syslog_sys')

// getSysLogSelect(url_filter_origin,'#filter_origin_syslog_sys')


});/// end ready





$('[data-olt="true"]').on('blur',function(e){
  
  if (e.target.id == 'olt_description'){
    var data = {
      description: $("#olt_description").text()
    }
  }
  if (e.target.id == 'olt_alias'){
    var data = {
      alias: $("#olt_alias").text()
    }
  }
  var url = DjangoURL('api_olt_detail', olt_uuid);
  axios.patch(url, data, conf)
  .then(function(response){
    if (response.status == 200)
    {
      $.gritter.add({
        title: 'OLT actualizada',
        time: 650,
      });
    }
  })//end then
  .catch(function(error) {
    handleErrorAxios(error);
  });
})




// function getEntitysLogs(){
//   $("#filter_type_log").empty();
//   $("#filter_type_log").append('<option>Tipo</option>');
//   var conf = {
//     headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
//   };
//   var url = DjangoURL('api_entity_list');
//   axios.get(url, conf)
//   .then(function(res){
//     if (res.status == 200)
//     {
//       for (var i = 0; i < res.data.length; i++) {
//         $("#filter_type_log").append(`<option value="${res.data[i].entity}">${res.data[i].entity}</option>`);
//       }
//       get_actions_logs();
//     }
//     if (res.status == 401)
//     {
//       handleUnauthorizedAxios();
//     }
//   })//end then
//   .catch(function(error) {
//       handleErrorAxios(error);
//   });
// }

// get_actions_logs = () => {
//   $("#filter_type_action").empty();
//   $("#filter_type_action").append('<option>Aciones</option>');
//   var conf = {
//     headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
//   };
//   var url = DjangoURL('api_actions_log_list');
//   axios.get(url, conf)
//   .then(function(res){
//     if (res.status == 200)
//     {
//       for (var i = 0; i < res.data.actions.length; i++) {
//         var o = res.data.actions
//         $("#filter_type_action").append(`<option value="${o[i].id}">${o[i].name}</option>`);
//       }
//     }
//     if (res.status == 401)
//     {
//       handleUnauthorizedAxios();
//     }
//   })//end then
//   .catch(function(error) {
//       handleErrorAxios(error);
//   });  
// }

function getModelsONU() {
  $("#filter_model").empty();
  $("#filter_model").append('<option>Modelo</option>');
  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };
  var url = DjangoURL('api_model_for_olt',olt_uuid);
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        $("#filter_model").append(`<option value="${res.data[i].id}">${res.data[i].model}</option>`);
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

function getFSP() {
  $("#filter_fsp").empty();
  $("#filter_fsp").append('<option>Seleccione F/S/P &nbsp;&nbsp;&nbsp;&nbsp;</option>');
  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };
  var url =  DjangoURL('fsp_for_olt_api', olt_uuid);


  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        $("#filter_fsp").append(`<option value="${res.data[i].id}">${res.data[i].description}</option>`);
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}



function getDetailsOLT() {
  var url = DjangoURL('api_olt_detail',olt_uuid)
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
        $("#olt_id").text(res.data.olt_id);
        $("#olt_alias").text(res.data.alias);
        $(".olt_name").text(res.data.alias);
        $("#olt_ip").text(res.data.ip);
        $("#olt_description").text(res.data.description);
        $("#olt_brand").text(res.data.model.brand);
        $("#olt_model").text(res.data.model.model);
        $("#olt_firmware").text(res.data.firmware);
        $("#olt_nodo_utp").text(res.data.node);
        

        //// status OLt
        if (res.data.status)
        {
          $("#olt_estatus").css('color', Django.color_success).text('Online');
        }
        else
        {
          $("#olt_estatus").css('color', Django.color_error).text('Offline');
        }

        if (res.data.task_in_progress)
        {
          //PENDING
          //
          $("#olt_watts").text('Recoletando información');
          $("#olt_cpu").text('Recoletando información');
          $("#olt_ram").text('Recoletando información');
          $("#olt_temperature").text('Recoletando información');
          $("#interfaces").append('<center><strong>Recoletando información</strong></center>');
        }else{
          $("#olt_watts").text(res.data.watts + ' Watts');

          //// cpu
          if (res.data.cpu){
            var cpu = parseInt(res.data.cpu);
            if(cpu < 50){
              $("#olt_cpu").css('color', Django.color_success).text(res.data.cpu + ' %');
            }else{
              if (cpu > 51 && cpu < 79 ) {
                 $("#olt_cpu").css('color', Django.color_alert).text(res.data.cpu + ' %');
              }
              else{
                $("#olt_cpu").css('color', Django.color_error).text(res.data.cpu + ' %');
              }
            }          
          }


          //// ram
          if (res.data.ram){
            var ram = parseInt(res.data.ram);
            if(ram < 50){
              $("#olt_ram").css('color', Django.color_success).text(res.data.ram + ' %');
            }else{
              if (ram > 51 && ram < 79 ) {
                 $("#olt_ram").css('color', Django.color_alert).text(res.data.ram + ' %');
              }
              else{
                $("#olt_ram").css('color', Django.color_error).text(res.data.ram + ' %');
              }
            }          
          }


          ////temperature
          if (res.data.temperature){
            var temperature = parseInt(res.data.temperature);
            if ( temperature < 50 ){
              $("#olt_temperature").css('color', Django.color_success).text(res.data.temperature+' C');
            }
            else{
              if ( temperature > 51 && temperature < 59){
                $("#olt_temperature").css('color', Django.color_alert).text(res.data.temperature+' C');
              }
              else{
                $("#olt_temperature").css('color', Django.color_error).text(res.data.temperature+' C');
              }
            }          
          }          

          getCards();
        }
        



    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}



var interfaceTemplate = templater`
<a onclick="openAccordion(this);" class="accordion-title">
  <span class="toggle-icon"></span>Tarjeta ${'interface'}
  <span style="float: right;color: ${'message_color'}">${'message'}</span>
</a>
<div class="accordion-content">
  <span class="br">Puertos online: <i style="color: ${'cards_color'}">${'ports_active'} de ${'ports'} activos.</i></span>
  <span class="br">ONUs onlines: <i style="color: ${'onus_color'}">${'onus_active'} de ${'onus'} activos.</i></span>
  <span class="br">CPU: <i style="color: ${'cpu_color'}">${'cpu'}%</i></span>
  <span class="br">RAM: <i style="color: ${'ram_color'}">${'ram'}%</i></span>
</div>`;


function getCards() {
  var url = DjangoURL('api_interface_olt', olt_uuid);
  axios.get(url, conf)
  .then(function(response){
    if (response.status == 200)
    {
      $("#interfaces").html("");

      for (var i = 0; i < response.data.length; i++) {
        
        percent = (response.data[i].ports_active * 100 ) / response.data[i].ports;
        percent = parseInt(percent)
        
        if (percent >= 90){
          cards_color = Django.color_success;
        }
        else{
          if (percent >= 50 && percent <= 89) {
            cards_color = Django.color_alert;
          }
          else{
            cards_color = Django.color_error;
          }
        }
        
        percent = (response.data[i].onus_active * 100 ) / response.data[i].onus;
        percent = parseInt(percent)
        if (percent >= 90){
          onus_color = Django.color_success;
        }
        else{
          if (percent >= 50 && percent <= 89) {
            onus_color = Django.color_alert;
          }
          else{
            onus_color = Django.color_error;
          }
        }        


        //// ram
        var ram = parseInt(response.data[i].ram);
        if (ram < 50){
          ram_color = Django.color_success;
        }
        else{
          if (ram > 51 && ram < 79) {
            ram_color = Django.color_alert;
          }
          else{
            ram_color = Django.color_error;
          }
        }
        // end ram

        //// cpu
          var cpu = parseInt(response.data[i].cpu);
          if (cpu < 50){
            cpu_color = Django.color_success;
          }
          else{
            if (cpu > 51 && cpu < 79) {
              cpu_color = Django.color_alert;
            }
            else{
              cpu_color = Django.color_error;
            }
          }  
        // end cpu

        var actives = response.data[i].ports_active + response.data[i].onus_active;
        var total = response.data[i].ports + response.data[i].onus;
        percent = (actives * 100 ) / total;
        percent = parseInt(percent)
        if (percent >= 90){
          message_color = Django.color_success;
          message = 'Operacional';
        }
        else{
          if (percent >= 50 && percent <= 89) {
            message_color = Django.color_alert;
            message = 'Operacional';
          }
          else{
            message_color = Django.color_error;
            message = 'Problemas';
          }
        }

        $("#interfaces").append(interfaceTemplate(
                                extend({'message':message,
                                'message_color':message_color,
                                'cards_color':cards_color,
                                'onus_color':onus_color,
                                'ram_color':ram_color,
                                'cpu_color':cpu_color
                                },
                                response.data[i])
                                ));
      }
    }
    if (response.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}



function updateOLT() {
  $("#modal-dialog").modal('toggle');

  var url = DjangoURL('api_update_remote_olt', olt_uuid);
  axios.patch(url, conf)
  .then(function(response){
    if (response.status == 200)
    {
      if (response.data.updated)
      {
        $("#modal-dialog").modal('toggle');
        getDetailsOLT();
      }else{
        _error('Hubo un error de conexión, no se pudo actualizar la información.')
        .then(function() {
          $("#modal-dialog").modal('toggle');
        });
      }
    }
  })//end then
  .catch(function(error) {
        if (error.response.status == 400)
        {
          if (error.response.data.error){
            _error(error.response.data.error);
          }
        }
        else{
          if(error.response.status == 500){
            $("#modal-dialog").modal('toggle');
            _error('Hubo un error en el sistema. Contacte al administrador.')
            .then(function() {});
          }else{
            handleErrorAxios(error);
          }   
        }
  });
  
}


function vlanOltSystem(id,selected){
  $("#modal-vlan").modal('toggle');
  var url = DjangoURL('api_system_vlan');

  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      $("#vlan_system").empty();
      $("#vlan_system").append(`<option>Seleccione VLAN</option>`);
      for (var i = 0; i < res.data.length; i++) {
        if (selected){
          $("#vlan_system").append(`<option ${res.data[i].id == selected ? "selected":""} value="${res.data[i].id}">${res.data[i].vlan_id}</option>`);
        }
        else{
          $("#vlan_system").append(`<option value="${res.data[i].id}">${res.data[i].vlan_id}</option>`);
        }
      }
      $("#id_olt_vlan").val(id);
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });  
}

function vlanOltSystemAsociar() {

  if($("#vlan_system option:selected").val() == 'Seleccione VLAN')
  {
    msg = '<small class="text-danger">Debe selecciona una VLAN.</small>';
    $("#error_olt_vlan").html(msg);
    return false;
  }
  
  var url = DjangoURL('api_olt_vlan',$("#id_olt_vlan").val());
  var data = {
    vlan_system:$("#vlan_system option:selected").val()
  }

  axios.patch(url, data, conf)
  .then(function(res){
    if (res.status == 200)
    {
      $("#loader_modal").html('');
      $('#modal-vlan').modal('toggle');
      swal({
      title: "Asociada",
        text: 'VLAN asociada correctamente',
        icon: "success",
        buttons: false
      }).then(function() {
        updateDatatable('#table_vlans');
      });
    }
  
  })//end then
  .catch(function(error) {
    $("#loader_modal").html('');
    handleErrorAxios(error);
  });

}



function speedOltSystem(id, selected, name){
  $("#modal-speed").modal('toggle');
  var url = DjangoURL('api_system_speeds');
  $("#name_in_olt_speed").text('');

  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      $("#speed_system").empty();
      $("#name_in_olt_speed").text(name);
      $("#speed_system").append(`<option>Seleccione Velocidad</option>`);
      for (var i = 0; i < res.data.length; i++) {
        if (selected){
          $("#speed_system").append(`<option ${res.data[i].id == selected ? "selected":""} value="${res.data[i].id}">${res.data[i].tidsis} ${res.data[i].name} </option>`);
        }
        else{
          $("#speed_system").append(`<option value="${res.data[i].id}">${res.data[i].tidsis} ${res.data[i].name}</option>`);
        }
      }
      $("#id_olt_speed").val(id);
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });  
}

function speedOltSystemAsociar() {

  if($("#speed_system option:selected").val() == 'Seleccione Velocidad')
  {
    msg = '<small class="text-danger">Debe selecciona una Velocidad.</small>';
    $("#error_olt_speed").html(msg);
    return false;
  }

  var url = DjangoURL('api_olt_speed_detail',$("#id_olt_speed").val());
  var data = {
    speeds_system:$("#speed_system option:selected").val()
  }

  axios.patch(url, data, conf)
  .then(function(res){
    if (res.status == 200)
    {
      $("#loader_speed").html('');
      $('#modal-speed').modal('toggle');
      swal({
      title: "Asociada",
        text: 'Velocidad asociada correctamente',
        icon: "success",
        buttons: false
      }).then(()=>{
        updateDatatable('#table_speeds');
      });
    }
  
  })//end then
  .catch(function(error) {
    $("#loader_speed").html('');
    handleErrorAxios(error);
  });

}


taskUptimeOnus = () => {
  asynchronousTask({
    task:'uptime_onus',
    olt:olt_uuid
  });
}

taskGetIpOnus = () => {
  asynchronousTask({
    task:'ip_onus',
    olt:olt_uuid
  });
}

taskGetSpeedsOLT = () => {
  asynchronousTask({
    task:'speeds_olt',
    olt:olt_uuid
  });
}

taskGetONU = () => {

  asynchronousTask({
    task:'get_onus',
    olt:olt_uuid
  });
}

taskGetVlans = () => {

  asynchronousTask({
    task:'get_vlans',
    olt:olt_uuid
  });
}


createSpeedPerform = () =>{
  var url = DjangoURL('api_olt_speed_list');

  if ($("#cir").val() == '' ||
      $("#cbs").val() == '' ||
      $("#pir").val() == '' ||
      $("#pbs").val() == '' ||
      $("#pri").val() == '' ||
      $("#name_speed").val() == '' ||
      $("#tidsis").val() == ''){

      swal({
      title: "Error",
        text: 'Verifique los datos ingresados',
        icon: "error",
        buttons: false
      })
      return false;
  }

  $("#loader_modal_speed").html('<span class="spinner"></span>');
  var data = { 
    "cir": $("#cir").val(),
    "cbs": $("#cbs").val(),
    "pir": $("#pir").val(),
    "pbs": $("#pbs").val(),
    "pri": $("#pri").val(),
    "name": $("#name_speed").val(),
    "tid": $("#tid").val(),
    "olt":olt_uuid
  }
  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 201)
    {
      $("#loader_modal_speed").html('');
      $('#modal-create-speed').modal('toggle');

      $.gritter.add({
        title: 'Velocidad creada',
        text: 'Se creo la velocidad en la OLT y en Sentinel',
        time: 1800,
      });      
      updateDatatable("#table_speeds");
    }
  
  })//end then
  .catch(function(error) {
    $("#loader_modal_speed").html('');
    handleErrorAxios(error);
  });


}


deleteSpeed = (id) => {
  delete_object(id, 'speed/olt', null, null, function(){
    table_speeds.api().ajax.reload(null, false);
  });  
}


function activateMatrixService(matrix_id){

  $('#modal-sleep').modal('show')

  let matrixUrl = "https://optic.matrix2.cl/api/v1/services/" + matrix_id + '/' 
  console.log(matrix_id)

  let data = {
    'status': 1
  }

  axios.patch(matrixUrl, data, {
          headers: {'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'}
    }).then(function(res){
    if (res.status === 200){
      $('#modal-sleep').modal('hide')
      swal({
      title: "Activado",
        text: 'Servicio activado en Matrix correctamente',
        icon: "success",
        buttons: false
      })
    }
  }).catch(function(error) {
      handleErrorAxios(error);
  })
}

function corteDeServicio(service_number){

  $('#modal-sleep').modal('show')

  let corteUrl = '/api/v1/cutting_replacement/'
  let data = {
    'service_number': service_number,
    'type': 'corte'
  }
  axios.post(corteUrl, data, {
          headers: {'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'}
    }).then(function(res){
    if (res.status === 200){
      $('#modal-sleep').modal('hide')
      swal({
      title: "Cortado",
        text: 'El Servicio será cortado dentro de unos minutos',
        icon: "success",
        buttons: false
      })
    }
  }).catch(function(error) {
      handleErrorAxios(error);
  })
}

function reposicionDeServicio(service_number){

  $('#modal-sleep').modal('show')

  let corteUrl = '/api/v1/cutting_replacement/'
  let data = {
    'service_number': service_number,
    'type': 'reposicion'
  }
  axios.post(corteUrl, data, {
          headers: {'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'}
    }).then(function(res){
    if (res.status === 200){
      $('#modal-sleep').modal('hide')
      swal({
      title: "Conectado",
        text: 'El Servicio será conectado dentro de unos minutos',
        icon: "success",
        buttons: false
      })
    }
  }).catch(function(error) {
      handleErrorAxios(error);
  })
}

function updateNetworkStatus(){

  let networkStatusUrl = DjangoURL('refresh_network_status');
  let data = {
    'device_id': olt_uuid,
    'device_type': 'OLT'
  }
  axios.post(networkStatusUrl, data, conf)
  .then(function(res){
    if (res.status === 200){
      $.gritter.add({
        title: 'Tarea ejecutándose, por favor espere algunos minutos',
        time: 1000,
      })
    }
  }).catch(function(error) {
      handleErrorAxios(error);
  })
}


function setPrimaryEquipment(service_number, device_ip){

  $('#modal-sleep').modal('show')

 let primaryEquipmentUrl = DjangoURL('set_primary_equipment', service_number)
 let data = {
    'device_ip': device_ip,
    'device_type': 'GPON'
 }
 axios.post(primaryEquipmentUrl, data, conf)
 .then(function(res){
  if (res.status === 200){
      $('#modal-sleep').modal('hide')
      swal({
      title: "Asociado",
        text: 'El Equipo seleccionado fue asociado como primario correctamente',
        icon: "success",
        buttons: false
      })
    }
 }).catch(function(error) {
      handleErrorAxios(error);
  })
}