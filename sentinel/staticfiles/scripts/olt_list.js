userInSession();
App.setPageTitle('Sentinel - Lista de OLT');

//$("#menu_olt > a ").css('cssText', "color: #FFF !important;");
$("#menu_olt").addClass('active');
$("#menu_olt_general").addClass('active');
var oTable;
var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};


$(document).ready(function (){

  // getNodesOLT();

  url_dt = Django.datatable_olt;
  oTable = $('#table_olt').dataTable({
      "aaSorting":[[0,"desc"]],
      "language": {
      "sLengthMenu":     "Mostrar _MENU_ OLT",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando OLT del _START_ al _END_ de un total de _TOTAL_ OLT",
      "sInfoEmpty":      "Mostrando OLT del 0 al 0 de un total de 0 OLT",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ OLT)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "sProcessing": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },

      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
      "processing": true,
      "serverSide": true,
      "searchDelay": 400,
      "ajax": {
          "url": url_dt,
          "type": "POST",
          "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
      },
      "columnDefs": [
      {
        orderable: true,
        searchable: true,
        targets: [0,1,2,3,4,5,6]
      },
      {
        orderable: false,
        searchable: false,
        targets: [7]
      }
      ], 
  });

  oTable.on( 'order.dt', function (e, settings) {

     var order = oTable.api().order();
     $("#table_olt > thead > tr > th").css('color','black')
     $( "#table_olt > thead > tr > th:eq( "+order[0][0]+" )" )
     .css( "color", "#2271b3" );

  });


  
  $('.modal').css('overflow-y', 'auto');

  var url = DjangoURL('api_node_list');
  $('#node').select2({
    dropdownParent: $('#modal-create'),
    width: 'resolve',
    language: {

      noResults: function() {
  
        return "No hay resultado";        
      },
      searching: function() {
  
        return "Buscando..";
      },
      inputTooShort: function() {
        return 'Al menos 2 letras';
      }
    },
    ajax: {
      // url,
      url:"/api/v1/autocomplete-mikrotik/",
      data: function (params) {
        var query = {
          q: params.term
        }
  
        // Query parameters will be ?search=[term]
        return query;
      },
      processResults: function (data, params) {
        console.log(data);
        var data = $.map(data, function (obj) {
          obj.text = obj.alias; // replace name with the property used for the text
          return obj;
        });

        return {
            results: data,
            pagination: {
                more: false
            }
        };
      }
    },
    minimumInputLength: 2
  });

});  

// getNodesOLT = () => {  
//   var url = DjangoURL('api_node_list');
//   axios.get(url, conf)
//   .then(function(res){
//     if (res.status == 200)
//     {
//       for (var i = 0; i < res.data.length; i++) {
//         // $("#nodes_olt").append(`<option value="${res.data[i].id}">${res.data[i].alias}</option>`);
//         $("#node").append(`<option value="${res.data[i].id}">${res.data[i].alias}</option>`);
//       }
//     }
//     if (res.status == 401)
//     {
//       handleUnauthorizedAxios();
//     }
//   })//end then
//   .catch(function(error) {
//       handleErrorAxios(error);
//   });
// }

Parsley.on('form:init', function() {
  /// get nodes
  /// getNodesOLT();
});

/**
 * Convert select to array with values
 */    
function serealizeSelects (select)
{
    var array = [];
    select.each(function(){ array.push($(this).val()) });
    return array;
}


function newOLT() {

  $('#modal-create').modal('hide');
  $('#modal-create-sleep').modal('show');
  var url = DjangoURL('api_olt_list')
  var data = { 
    "ip": $("#ip").val(),
    "node": $("#node").val(),
    "alias": $("#alias").val(),
    "description": $("#description").val(),
    "user": $("#user").val(),
    "password": $("#password").val(),    
    // "nodes_olt": serealizeSelects($('#nodes_olt'))
  }

  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 201)
    {
      $('#modal-create-sleep').modal('hide');
      swal({
      title: "Creado",
        text: 'OLT Creada exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        updateDatatable();
      });
    }
  })//end then
  .catch(function(error) {
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }else{
        _error("Por favor verifique los datos ingresados.").then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }
    }

    if (error.response.status == 500)
    {

      _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){
        $('#modal-create-sleep').modal('hide');
      });
      
    }     
     
    //handleErrorAxios(error);
  });
  
};


delete_olt = (id) => {    
  delete_object(id, 'olt', null, null, function(){
    oTable.api().ajax.reload(null, false);
  });
}


taskUpdateOLT = (olt_uuid) => {
  asynchronousTask({
    task:'update_olt',
    olt:olt_uuid
  }, function(){
    updateDatatable("#table_olt");
  });
}

$(document).on('keydown', function(event) {  
  if(event.which == 118) { // F7

    var url = DjangoURL('api_update_uptime_olts');
    axios.post(url, conf)
    .then(function(res){
      if (res.status == 201)
      {
        oTable.api().ajax.reload(null, false);
      }
      if (res.status == 401)
      {
        handleUnauthorizedAxios();
      }
    })//end then
    .catch(function(error) {
        handleErrorAxios(error);
    });


  }
});

$(window).keydown(function(e){
  //console.log(e)
});

keydownTaskOLT = (e) => {
  if (e.shiftKey && e.keyCode === 70){
    console.log(e)
  }
}

ayuda = () => {
  alert('Pagina de ayuda de olt')
}