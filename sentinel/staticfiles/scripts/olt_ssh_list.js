userInSession();
App.setPageTitle('Sentinel - Lista de SSH');

//$("#menu_SSH > a ").css('cssText', "color: #FFF !important;");
$("#menu_ssh").addClass('active');
$("#menu_configuraciones").addClass('active');
var oTable;
var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};


$(document).ready(function (){

  url_dt = Django.datatable_SSH_list;
  oTable = $('#table_ssh').dataTable({
      "aaSorting":[[0,"desc"]],
      "language": {
      "sLengthMenu":     "Mostrar _MENU_ SSH",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando SSH del _START_ al _END_ de un total de _TOTAL_ SSH",
      "sInfoEmpty":      "Mostrando SSH del 0 al 0 de un total de 0 SSH",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ SSH)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "sProcessing": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },

      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
      "processing": true,
      "serverSide": true,
      "searchDelay": 400,
      "ajax": {
          "url": url_dt,
          "type": "POST",
          "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
      }
  });

  oTable.on( 'order.dt', function (e, settings) {

     var order = oTable.api().order();
     $("#table_ssh > thead > tr > th").css('color','black')
     $( "#table_ssh > thead > tr > th:eq( "+order[0][0]+" )" )
     .css( "color", "#2271b3" );

  });

  var url = DjangoURL('api_olt_list') + '?fields=alias,uuid'
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      var result = res.data;
      for (var i = 0; i < result.length; i++) {
        $("#olt").append(`<option value="${result[i].uuid}">${result[i].alias}</option>`);
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      _error('Error al obtener lista de OLTs')
  });


});  


Parsley.on('form:init', function() {
  /// get nodes
  /// getNodesSSH();
});


new_ssh = () => {

  $('#modal-create').modal('hide');
  $('#modal-create-sleep').modal('show');
  var url = DjangoURL('api_ssh_olt_list')
  var data = { 
    "username": $("#user").val(),
    "password": $("#password").val(),
    "olt":$("#olt option:selected").val(),
  }

  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 201)
    {
      swal({
      title: "Creado",
        text: 'SSH Creada exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        updateDatatable();
      });
    }
  
  })//end then
  .catch(function(error) {
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }else{
        _error("Por favor verifique los datos ingresados.").then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }
    }

    if (error.response.status == 500)
    {

      _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){
        $('#modal-create-sleep').modal('hide');
      });
      
    }     
     
    //handleErrorAxios(error);
  });
  
};


delete_ssh = (id) => {    
  delete_object(id, 'ssh_SSH', null, null, function(){
    oTable.api().ajax.reload(null, false);
  });
}

ayuda = () => {
  _exito('Pagina de ayuda de ssh')
}