userInSession();
App.setPageTitle('Sentinel - Lista de ONU');

var olt_uuid = getParameterUrl('filter');
var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};

$(document).ready(function (){

  if (olt_uuid)
  {
    url_dt = Django.datatable_unu_olt.replace(':val:',olt_uuid);  
    $("#menu_olt_onus").addClass('active'); 
    $("#menu_olt").addClass('active');
  }
  else
  {
    url_dt = Django.datatable_unu_list;
    $("#menu_onu_list").addClass('active');
    $("#menu_onu").addClass('active');
    $("#menu_cpes").addClass('active');    
  }

  oTable = $('#table_onus_olt').dataTable({
    "aaSorting":[[0,"desc"]],
      "language": {
      "sLengthMenu":     "Mostrar _MENU_ ONU",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando ONU del _START_ al _END_ de un total de _TOTAL_ ONU",
      "sInfoEmpty":      "Mostrando ONU del 0 al 0 de un total de 0 ONU",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ ONU)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "sProcessing": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },

      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
    "dom": '<"top">rt<"bottom"iflp><"clear">',
    "processing": true,
    "serverSide": true,
    "searchDelay": 400,
    "ajax": {
        "url": url_dt,
        "type": "POST",
        "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
        data: function (d) {
            d.filter_status = $("#filter_status").val();
            d.filter_model = $("#filter_model").val();
            d.filter_fsp = $("#filter_fsp").val();
            d.filter_olt = $("#filter_olt").val();
        },
    },
    "columnDefs": [
      {
        orderable: true,
        searchable: true,
        targets: [0,1,2,3,4,5,6,7,9]
      },
      {
        orderable: true,
        searchable: true,
        targets: [8]
      }
    ],    
  });

  oTable.on( 'order.dt', function (e, settings) {

     var order = oTable.api().order();
     $(".table > thead > tr > th").css('color','black')
     $( ".table > thead > tr > th:eq( "+order[0][0]+" )" )
     .css( "color", "#2271b3" );

  });

  getModelsONU();
  getOLTs();

  $('#filter_fsp').select2({});
  $("#select2-filter_fsp-container").css('width','220px');
  

  $('#filter_status').change(function(){
    oTable.api().ajax.reload(null, false);
  })

  $('#filter_model').change(function(){
    oTable.api().ajax.reload(null, false);
  })

  $('#filter_olt').change(function(){
    var selectedOlt = $(this).children("option:selected").val();
    getFSP(selectedOlt);
    oTable.api().ajax.reload(null, false);
  })

  $('#filter_fsp').change(function(){
    oTable.api().ajax.reload(null, false);
  })
  

});  


function getModelsONU() {
  $("#filter_model").empty();
  $("#filter_model").append('<option>Modelo</option>');
  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };
  var url = DjangoURL('api_model_device_list');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        // Con esta condición nos aseguramos de que solo traiga modelos
        // asociados a ONUS
        if (res.data[i].kind == 1) {
          $("#filter_model").append(`<option value="${res.data[i].id}">${res.data[i].model}</option>`);
        }
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}


function getOLTs() {
  $("#filter_olt").empty();
  $("#filter_olt").append('<option value="0">Seleccione OLT &nbsp;&nbsp;&nbsp;&nbsp;</option>');
  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };
  var url =  DjangoURL('api_olt_list') + '?fields=alias,uuid';
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        $("#filter_olt").append(`<option value="${res.data[i].uuid}">${res.data[i].alias}</option>`);
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}


function getFSP(olt_uuid) {
  $("#filter_fsp").empty();
  if (olt_uuid == 0){
    return false;
  }

  $("#filter_fsp").append('<option>Seleccione F/S/P &nbsp;&nbsp;&nbsp;&nbsp;</option>');
  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };
  var url =  DjangoURL('fsp_for_olt_api', olt_uuid);
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        $("#filter_fsp").append(`<option value="${res.data[i].id}">${res.data[i].description}</option>`);
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}


function activateMatrixService(matrix_id){

  $('modal-sleep').modal('show')

  let matrixUrl = "https://optic.matrix2.cl/api/v1/services/" + matrix_id + '/' 
  console.log(matrix_id)

  let data = {
    'status': 1
  }

  axios.patch(matrixUrl, data, {
          headers: {'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'}
    }).then(function(res){
    if (res.status === 200){
      $('modal-sleep').modal('hide')
      swal({
      title: "Activado",
        text: 'Servicio activado en Matrix correctamente',
        icon: "success",
        buttons: false
      })
    }
  }).catch(function(error) {
      handleErrorAxios(error);
  })
}

function corteDeServicio(service_number){

  $('modal-sleep').modal('show')

  let corteUrl = '/api/v1/cutting_replacement/'
  let data = {
    'service_number': service_number,
    'type': 'corte'
  }
  axios.post(corteUrl, data, {
          headers: {'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'}
    }).then(function(res){
    if (res.status === 200){
      $('modal-sleep').modal('hide')
      swal({
      title: "Cortado",
        text: 'El Servicio será cortado dentro de unos minutos',
        icon: "success",
        buttons: false
      })
    }
  }).catch(function(error) {
      handleErrorAxios(error);
  })
}

function reposicionDeServicio(service_number){

  $('modal-sleep').modal('show')

  let corteUrl = '/api/v1/cutting_replacement/'
  let data = {
    'service_number': service_number,
    'type': 'reposicion'
  }
  axios.post(corteUrl, data, {
          headers: {'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'}
    }).then(function(res){
    if (res.status === 200){
      $('modal-sleep').modal('hide')
      swal({
      title: "Conectado",
        text: 'El Servicio será conectado dentro de unos minutos',
        icon: "success",
        buttons: false
      })
    }
  }).catch(function(error) {
      handleErrorAxios(error);
  })
}


function setPrimaryEquipment(service_number, device_ip){

  $('#modal-sleep').modal('show')

 let primaryEquipmentUrl = DjangoURL('set_primary_equipment', service_number)
 let data = {
    'device_ip': device_ip,
    'device_type': 'GPON'
 }
 console.log(device_ip)
 axios.post(primaryEquipmentUrl, data, conf)
 .then(function(res){
  if (res.status === 200){
      $('#modal-sleep').modal('hide')
      swal({
      title: "Asociado",
        text: 'El Equipo seleccionado fue asociado como primario correctamente',
        icon: "success",
        buttons: false
      })
    }
 }).catch(function(error) {
      console.log(error)
      handleErrorAxios(error);
  })
}