userInSession();
App.setPageTitle('Sentinel - Provisionar ONUs');

//$("#menu_olt > a ").css('cssText', "color: #FFF !important;");
$("#menu_olt").addClass('active');
$("#menu_olt_provisionar_onu").addClass('active');

var lastServiceNumber;

/* Aca se busca las ont/onu que esten para autofind*/
function findONUs(uuid=null) {

  $('#modal-create-sleep').modal('show');
  var url = DjangoURL('api_provisioning_onu_list');
  if (uuid){
    var conf_header = {
      headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)},
      params: {
        uuid: uuid
      }    
    };
  }else{
    var conf_header = {
      headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)},
    };
  }


  axios.get(url, conf_header)
  .then(function(res){

    if (res.status == 200)
    {
      if (true)
      {
        $("#content-onus").html(" ");
      }

      if (document.visibilityState !== "visible")
      {
        notify_autofind();
      }

      $('#modal-create-sleep').modal('hide');
      for (var i = 0; i < res.data.length; i++) {

          var previus = res.data[i].in_previus_olt;
          $("#content-onus").append(OnuTemplate({
            title:previus==0 ? 'ONU disponible para ser provisionada' : 'ONU ya asociada' ,
            class_parent:previus==0 ? 'successParent':'alertParent',
            class_child:previus==0 ? 'successChild':'alertChild',
            color_btn:previus==0 ? 'green':'red',
            serial:res.data[i].serial,
            olt:res.data[i].olt,
            model:res.data[i].model,
            fsp:res.data[i].fsp,
            uuid:res.data[i].uuid,
            description:res.data[i].description,
            service_number_text: previus==0 ? '':'Numero de servicio',
            service_number:previus==0 ? '':res.data[i].service_number
          }));

      }

    }
  
  })//end then
  .catch(function(error) {
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        $('#modal-create-sleep').modal('hide');
        _error(""+ error.response.data.error).then(function(){
          
        });
      }else{
        _error("Por favor verifique los datos ingresados.").then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }
      return
    }

    if (error.response.status == 500)
    {

      _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){
        $('#modal-create-sleep').modal('hide');
      });
      return
    }
     
    handleErrorAxios(error);
  });
  
};


function provisioningOLT(id) {
  var url = '/devices/provisioning/olt/'+id+'/';
  //$('#myModal').modal('show').find('.modal-content').load(url);

  // reset modal if it isn't visible
/*  if (!($('.modal.in').length)) {
    $('.modal-dialog').css({
      top: '140px',
      left: '450px'
    });
  }*/

  $('#myModal').modal({
    // backdrop: false,
    show: true
  }).find('.modal-content').load(url);

  //$('.modal-dialog-draggable').draggable({
  //  handle: ".modal-header"
  //});

}


function findServiceNumber(id){

  var serviceNumber = $("#service_number_search").val();

  if (serviceNumber == ''){
    _error('El número de servicio no puede estar en blanco.')
    return false;
  }

  if (false){
    if (lastServiceNumber == serviceNumber)
    {
      _error('Acaba de consultar este mismo Numero');
      return false;
    }
  }

  lastServiceNumber = serviceNumber;

  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)},
    params: {
      uuid: id
    }    
  };

  $("#data_service_number").html(
    `<center><img src="/static/img/loading_spinner.gif" width="100px"
    heigh="100px"></img></center>`);
  
  var url = DjangoURL('api_matrix_service_number_detail',
                      serviceNumber);
  axios.get(url, conf)
  .then(function(response){
    // alias
    const info = response.data;
    console.log(info)

    //verifico el estatus 
    if (response.status == 200)
    {
      if (info.error){
          $("#data_service_number").html('<hr style="background: #e2e7eb; width: 100%">\
          <div class="text-danger">&nbsp;' + info.error + '</div>');
      }
      else
      {
        var text_alert_html = '<span class="text-danger"><span class="f-w-700">ADVERTENCIA:</span>\
        Este servicio ya tiene una ONU asignada, si continua con la asociación la ONU anterior será \
        eliminada de la OLT y dicho equipo quedará sin conexión. </span>';
        let service_status = info.service_status;
        let plan_field = '';
        let description_field = '';
        let tids_fields = '';

        if (service_status == 1 ||
            service_status == 8 ||
            service_status == 3 ||
            service_status == 2){
            let select_planes = '<select id="select_planes" class="form-control">';
            let operator = $("#operator :selected").val();            
            let planes = HDD.getJ('planes');

            planes.forEach(function(plan) {
              if (plan.operator == parseInt(operator))
              {
                if (info.sentinel_matrix_plan == plan.matrix_plan)
                {
                  select_planes += `<option selected="selected" value="${plan.id}">${plan.name}</option>`;
                }
                else
                {
                  select_planes += `<option value="${plan.id}">${plan.name}</option>`;
                }
              }
            });
            plan_field = select_planes + '</select>';

        }
        else
        {
          if(service_status != 4)
          {
            plan_field = info.plan.name + ' - ' + info.plan.price;
            description_field = `<tr><td>Actividad</td><td>
            <textarea class="form-control" id="description"></textarea></td></tr>`;
          }
        }

        if(info.plan.category == 2){
          let select_speed = '<tr><td>Velocidad subida</td><td><select id="speed_up" class="form-control">';
          let speeds = HDD.getJ('speeds');
          speeds.forEach(function(speed) {
            select_speed += `<option value="${speed.id}">${speed.name}</option>`;
          });
          tids_fields = select_speed + '</select></td></tr>';

          select_speed = '<tr><td>Velocidad bajada</td><td><select id="speed_down" class="form-control">';
          speeds.forEach(function(speed) {
            select_speed += `<option value="${speed.id}">${speed.name}</option>`;
          });
          tids_fields += select_speed + '</select></td></tr>';

          // ya se selecciona las velocidades por lo que no pide plan
          plan_field = info.plan.name + ' - ' + info.plan.price;
        }

        let tecnicos = HDD.getJ('tecnicos');
        let tecnicos_fields = '';
        console.log(tecnicos)
        tecnicos.forEach(function(tecnico) {
          tecnicos_fields += `<option value="${tecnico.id}">${tecnico.first_name}</option>`;
        });

        $("#data_service_number").html(ServiceNumberTemplate({
          text_alert : info.service_with_onu == false ? '': text_alert_html,
          rut : info.client.rut,
          composite_address : info.composite_address,
          commune: info.client.commune,
          code: info.node.code,
          node_address: info.node.street + ' ' + info.node.house_number,
          apartment_number: info.client.apartment_number,
          tower: info.client.tower,
          client: info.client.name,
          get_status_display : info.get_status_display,
          service_status:service_status,
          service : info.service,
          uuid : info.uuid,
          previus: info.in_previus_olt,
          sentinel_plan:info.sentinel_plan,
          description_field:description_field,
          tids_fields:tids_fields,
          tecnicos_fields:tecnicos_fields,
          plan_field: plan_field
          
        }));

      }

    }
    if (response.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      $("#data_service_number").html(' ');
      handleErrorAxios(error);
  });

}


function associate(service, id, previus, sentinel_plan,code,
                   apartment_number, tower, client, node_address,
                   service_address, service_status) {

  let serial = $("#ids_1").val() + $("#ids_2").val() + $("#ids_3").val() +
               $("#ids_4").val() + $("#ids_5").val() + $("#ids_6").val();
  let SSID = $("#SSID").val();
  let clave_wifi = $("#clave_wifi").val();
  let tecnico = $("#tecnico").val();


  if (tecnico == ''){
    swal({
        text: `Debe indicar el tecnico.`,
        icon: "warning"
    })
    return false;
  }

  if (SSID ==''){
    swal({
        text: `Debe indicar el SSID de la red WiFi.`,
        icon: "warning"
    })
    return false;
  }

  if (clave_wifi ==''){
    swal({
        text: `Debe indicar la clave de la red WiFi.`,
        icon: "warning"
    })
    return false;
  }

  if (serial.length < 6){
    swal({
        text: `Tiene el serial incompleto, porfavor verifique.`,
        icon: "warning"
    })
    return false;
  }

  if (clave_wifi.length < 8){
    swal({
        text: `La clave del WiFi debe tener minimo 8 caracteres`,
        icon: "warning"
    })
    return false;
  }

  $('#myModal').modal('hide');
  if (previus != 0){
    swal({
        title: "Aprovisionar",
        text: `¿Realmente esta seguro de proceder?
               Eliminará el servicio de la ONU anterior`,
        icon: "warning",
        buttons: [
          'No',
          'Si'
        ],
        dangerMode: false,
      }).then(function(isConfirm) {
        if (isConfirm) {
          console.log(id)
          associatePerform(service, id, sentinel_plan,
                           code, apartment_number, tower,
                           client, node_address, service_address,
                           service_status)
        }
    })
  }
  else
  {
    associatePerform(service, id, sentinel_plan,
                     code, apartment_number, tower, client,
                     node_address, service_address,
                     service_status)
  }
}


function associatePerform(service, id, sentinel_plan, code, apartment_number, tower,
                          client, node_address, service_address, service_status) {
  
  $('#modal-create-sleep').modal('show');
  var url = DjangoURL('api_provisioning_perform_onu');
  let serial = $("#ids_1").val() + $("#ids_2").val() + $("#ids_3").val() +
               $("#ids_4").val() + $("#ids_5").val() + $("#ids_6").val();
  let SSID = $("#SSID").val();
  let clave_wifi    = $("#clave_wifi").val();
  let tecnico       = $("#tecnico").val();
  let speed_up      = $("#speed_up").val();
  let speed_down    = $("#speed_down").val();
  let select_planes = $("#select_planes").val();
  let select_operator = $("#operator").val();

  var data_send  = {
    "onu":id,
    "service":service,
    'sentinel_plan':sentinel_plan,
    'code':code,
    'apartment_number':apartment_number,
    'tower':tower,
    'client':client,
    'node_address':node_address,
    'service_address':service_address,
    'service_status':service_status,
    'serial':serial,
    'SSID':SSID,
    'clave_wifi':clave_wifi,
    'tecnico':tecnico,
    'speed_up':speed_up,
    'speed_down':speed_down,
    'select_planes':select_planes,
    'select_operator':select_operator
  }
  console.log(url)
  axios.post(url, data_send, header_common)
  .then(function(res){
    if (res.status == 201)
    {
      // Se borra la(s) onu(s) del fondo en la pantallla
      $("#content-onus").html(' ')
      let no_cache_random =  Math.random().toString(36).substr(2, 5);
      let case_module = '/static/scripts/tmpl/provisioning_case_'+res.data.case+'.js?no_cache='+no_cache_random;
      import(case_module).then( ({default: modal_case}) => {
          // Ya obtube la respuesta de provisionamiento 
          let obj_data   = {'id':id,
                            'signal':res.data.signal,
                            'fsp': res.data.fsp,
                            'olt':res.data.olt,
                            'onu_id':res.data.onu_id,
                            'ip':res.data.ip,
                            'mac':res.data.mac,
                            'node':res.data.node,
                            'case':res.data.case,
                            'case_message':res.data.case_message,
                            'desc_client':res.data.desc_client,
                            'pass_onus':res.data.pass_onus,
                            'user_onus':res.data.user_onus,
                            'SSID_2G':res.data.SSID_2G,
                            'SSID_5G':res.data.SSID_5G,
                            'password':res.data.password,
                            'onu_gateway':res.data.onu_gateway                
                            }

          HDD.setJ('obj_data', obj_data);
          // se almancena en localstorage para interactividad con los modales
          let case_html  = modal_case(obj_data);

          $('#modal-create-sleep').modal('hide');
          $('#modal_provisioning').modal({
            backdrop: false,
            show: true
          }).find('.modal-content').html(case_html);

          $('#modal-create-sleep').modal('hide');
          
          if ($('[data-toggle="tooltip"]').length !== 0) {
            $('[data-toggle=tooltip]').tooltip();
          }
          tooltip_sentinel(document);

      });

    }
  })//end then
  .catch(function(error) {
    $('#modal-create-sleep').modal('hide');
    handleErrorAxios(error);
  });
}


$( "#select_olts" ).autocomplete({
  source: function( request, response ) {
    $.ajax( {
      url: "/api/v1/olt/?alias__icontains="+request.term+"&fields=alias,uuid",
      beforeSend: function (xhr) {
        xhr.setRequestHeader ("Authorization", "jwt " + HDD.get(Django.name_jwt));
      },
      data: {
        term: request.term
      },
      success: function( data ) {
        if(!data.length){
          var result = [
          {
            label: 'Sin resultados.', 
            value: response.term
          }];
          response(result);
        }
        else
        {
          res = $.map(data, function(item) {
                return {
                  label: item.alias,
                  value: item.alias,
                  uuid: item.uuid
                }
          });
          response(res);
        }
      }
    });
  },
  minLength: 2,
  select: function( event, ui ) {
    findONUs(ui.item.uuid)
  }
})

ayuda = () => {
  window.open('https://nuevooptic.atlassian.net/wiki/spaces/DDP/pages/155156957/Provisionamiento+de+Equipos.', '_blank');
}


service_number_enter = (e,id) =>{
  // funcion para al pulsar enter buscar el número de servicio
  if(e.keyCode == 13)
  {
    findServiceNumber(id)
  }
  if (e.keyCode >= 96 & e.keyCode <= 105){
    return true;
  }
  return permite(e,'num');
}


input_digit_serial = (e, next_id) => {
  var valid = serial_check(e);
  if (! valid ){
    $(e.target).val('');
    return false;
  }
  if (next_id != 0)
  {
    $(e.target).val($(e.target).val().toUpperCase())
    $("#ids_" + next_id).focus();
  }
}


serial_check = (e) => {
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[A-Fa-f0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}


reanudar_modal = () => {
  $("#myModal").modal('show');
  $("#modal_provisioning").modal('hide');
  $("#modal-create-sleep").modal('hide');
}


reintentar_señal = (olt, onu_id, fsp, ip) => {
  $('#modal-create-sleep').modal('show');
  $("#modal_provisioning").modal('hide');
  //$("#modal_provisioning").modal('show');
  
  // consulta el nivel de señal de la onu
  let url = DjangoURL('api_ssh_command_onu_signal')
  let data_send = {
    onu_id: onu_id,
    fsp: fsp,
    olt: olt
  }

  axios.post(url, data_send, header_common)
  .then(function(res){
    if (res.status == 201)
    {
      $('#modal-create-sleep').modal('hide');
      let obj_data = HDD.getJ('obj_data');
      let no_cache_random =  Math.random().toString(36).substr(2, 5);
      let case_module = ''
      if( !(res.data.signal < -26) )
      {
        //caso de exito
        case_module = '/static/scripts/tmpl/provisioning_case_6.js?no_cache' + no_cache_random;
      }
      else
      {
        case_module = '/static/scripts/tmpl/provisioning_case_3.js?no_cache' + no_cache_random;
      }

      import(case_module).then( ({default: modal_case}) => {
        $('#modal_provisioning').modal({
          backdrop: false,
          show: true
        }).find('.modal-content').html(
          modal_case(obj_data)
        )//fin html
        tooltip_sentinel(document);
      });

    }

    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
    
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

reintentar_ip = (olt) => {
  $('#modal-create-sleep').modal('show');
  $("#modal_provisioning").modal('hide');
  //$("#modal_provisioning").modal('show');
  
  // hace un remote ping
  let obj_data = HDD.getJ('obj_data');  
  let url = DjangoURL('api_ssh_command_onu_ping')
  let data_send = {
    onu_id: obj_data.onu_id,
    fsp: obj_data.fsp,
    olt: olt,
    ip: obj_data.onu_gateway
  }

  axios.post(url, data_send, header_common)
  .then(function(res){
    if (res.status == 201)
    {
      $('#modal-create-sleep').modal('hide');
      let no_cache_random =  Math.random().toString(36).substr(2, 5);
      let case_module = '';
      if( res.data.status )
      {
        //caso de exito en remote ping
        case_module = '/static/scripts/tmpl/provisioning_case_6.js?no_cache' + no_cache_random;
      }
      else
      {
        //fallo, se vuelve a mostrar el modal 5 para reintentar
        case_module = '/static/scripts/tmpl/provisioning_case_5.js?no_cache' + no_cache_random;
      }
    
      import(case_module).then( ({default: modal_case}) => {
        $('#modal_provisioning').modal({
          backdrop: false,
          show: true
        }).find('.modal-content').html(
          modal_case(obj_data)
          )//fin html
          tooltip_sentinel(document);
      });


    }
    // fin 201
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
    
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });  



}

copy_ip = (ip) => {
  navigator.clipboard.writeText(ip).then(function() {
    $.gritter.add({
      title: 'Valor copiado al portapapeles',
      time: 1000
    });
  },function() {
    $.gritter.add({
      title: 'Porfavor copie el valor manualmente',
      time: 2500
    });
  });

}

comprobar_wifi = () => {
  $('#modal-create-sleep').modal('hide');
  $("#modal_provisioning").modal('hide');

  swal({
    title: "¡Listo! La onu se encuentra provisionada y configurada",
    text: 'Ahora comprueba con el técnico que el cliente esté satisfecho.',
    icon: "success",
    buttons: false
  })  
}


notify_autofind = () => {
  // Let's check if the browser supports notifications
  if (!("Notification" in window)) {
    return;
  }
  else if (Notification.permission === "granted") {
    var notification = new Notification("Se termino de ejecutar el autofind.");
  }

  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {
      // If the user accepts, let's create a notification
      if (permission === "granted") {
        var notification = new Notification("Se termino de ejecutar el autofind.");
      }
    });
  }

}

/** eventos **/
$(() => {
  let conf = {
    headers: { Authorization: `jwt ${HDD.get(Django.name_jwt)}`}
  };

  // consulta de planes
  let url = DjangoURL('api_plan_list');
  axios.get(url+'?with_system_speed=True', conf)
  .then(function(res){
    if (res.status == 200)
    {
      //HDD.unset('planes');
      HDD.setJ('planes' , res.data);
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });

  // consulta de velocidades de sistemas
  url = DjangoURL('api_system_speeds');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      //HDD.unset('speeds');
      HDD.setJ('speeds' , res.data);
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });

  if (true)
  {
    //obtengo los tecnicos de matrix
    //url = DjangoURL('api_matrix_technician_list');
    url = DjangoURL('api_technician_list');
    axios.get(url, conf)
    .then(function(res){
      if (res.status == 200)
      {
        //HDD.unset('tecnicos');
        console.log(res.data)
        HDD.setJ('tecnicos', res.data);
      }
      if (res.status == 401)
      {
        handleUnauthorizedAxios();
      }
    })//end then
    .catch(function(error) {
        handleErrorAxios(error);
    });    
  }

  Notification.requestPermission().then(function(result) {
    //console.log(result);
  });

});

/*
document.addEventListener("visibilitychange", function() {
  console.log( document.visibilityState );
});*/


function mi_demo() {
  import('/static/scripts/tmpl/provisioning_case_6.js').then( ({default: modal_case}) => {
    $('#modal_provisioning').modal({
      backdrop: false,
      show: true
    }).find('.modal-content').html(
      modal_case({
      "service_number":"12345",
      "case":6,
      "message":"Exitos",
      "fsp":"0/1/1",
      "onu_id":44,
      "olt":"8bcb4664-601e-4f59-b025-3d633fad4fe6",
      "ip":"192.168.1.1",
      "node":"alb",
      "desc_client":"12345 - OPT - APT",
      'id':1,
      'signal':'-15',
      'mac':'',
      'case_message':'bien',
      'pass_onus':'pass_onus',
      'user_onus':'user_onus',
      'SSID_2G':'SSID_2G',
      'SSID_5G':'SSID_5G',
      'password':'password',
      'onu_gateway':'0.0.0.0'
      })
    )//fin html
    tooltip_sentinel(document);
  });
}
