userInSession();
App.setPageTitle('Sentinel - Lista de Operadores sistema');

$("#menu_configuraciones").addClass('active');
$("#menu_operadores").addClass('active');

$(document).ready(function (){

  url_dt = Django.datatable_operators;
  oTable = $('#table_operator').dataTable({
        "aaSorting":[[0,"desc"]],
        "columnDefs":[
          {
            targets:[2,3,4],
            orderable:false,
            searchable:false
          },
        ],
        "language": {
        "sLengthMenu":     "Mostrar _MENU_ Operador",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando Operador del _START_ al _END_ de un total de _TOTAL_ Operador",
        "sInfoEmpty":      "Mostrando Operador del 0 al 0 de un total de 0 Operador",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ Operador)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "sProcessing": "Cargando...",
        "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
        },

        "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      },
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": url_dt,
            "type": "POST",
            "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
        },
      

   });
});  



function newOperator() {
  $("#loader_modal").html('<span class="spinner"></span>');
  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };
  var url = DjangoURL('api_operator_list');
  var data = { 
    "code": $("#code").val(),
    "name": $("#name").val()
  }

  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 201)
    {
      $("#loader_modal").html('');
      $('#modal-create').modal('toggle');
      swal({
      title: "Creado",
        text: 'Operador creado exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        updateDatatable();
      });
    }
  
  })//end then
  .catch(function(error) {
    $("#loader_modal").html('');
    handleErrorAxios(error);
  });
  
};