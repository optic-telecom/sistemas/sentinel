var header_common = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};

var PanelFSPTemplate = templater`
<div class="panel panel-inverse" id="container-main">
  <div class="panel-heading">
    <div class="panel-heading-btn">
      <a class="btn btn-xs btn-icon btn-success" onClick="closePanelAction()">
        <i class="fa fa-angle-double-right"></i>
      </a>
    </div>
    <h4 class="panel-title">Propiedades ${ 'olt' } (${ 'interface' }/${ 'port' })</h4>
  </div>
  <div>

  <div class="tab-content">

    <div class="tabbable-line">
    <!-- begin nav-pills-panel -->
    <ul class="nav nav-pills-panel">
      <li class="nav-items">
        <a href="#fsp_tab_general" data-toggle="tab" class="nav-link-panel active">
          <span>General</span>
        </a>
      </li>

      <li class="nav-items">
        <a href="#fsp_tab_graficos" data-toggle="tab" class="nav-link-panel">
          <span>Gráficos</span>
        </a>
      </li>

      <li class="nav-items">
        <a href="#fsp_tab_estadisticas" data-toggle="tab" class="nav-link-panel tab-line" onclick="openFSP()">
          <span>Estadisticas Avanzadas</span>
        </a>
      </li>

    </ul>
    <br>
    </div>

    <!-- begin tab-pane -->
    <div class="tab-pane fade show active" id="fsp_tab_general">
      <div>
        <span class="br">OLT: <i>${ 'olt' }</i> </span>
        <span class="br">ID:<i>${ 'pk' }</i></span>
        <span class="br">F/S/P:<i>${ 'interface' }/${ 'port' }</i></span>
        <span class="br">Descripción:<i contenteditable="true" onblur="updatePortDesc(this)"
        data-placement="right" data-toggle="tooltip" title='<i class="icon-pencil"></i>' data-html="true">${ 'description' }</i></span>
        <span class="br">ONUs online<i>${ 'onus_active' }</i></span>
        <span class="br">ONUs total<i>${ 'onus' }</i></span>
      </div>
    </div>
    <!-- end tab-pane -->


    <!-- begin tab-pane -->
    <div class="tab-pane fade show" id="fsp_tab_graficos">
      <div>
        <span class="br">Graficos</span>
      </div>
    </div>
    <!-- end tab-pane -->    


    <!-- begin tab-pane -->
    <div class="tab-pane fade show" id="fsp_tab_estadisticas">
      <div>
        <button class="btn btn-green btn-sm" onclick="openFSP()"
          style="float: right; display:block;">Refrescar info</button>
      </div>
      <br><br>
      <div id="content_fsp"></div>
    </div>
    <!-- end tab-pane -->    

    </div>
  </div>
</div>`;

var span_br = templater`
<span class="br">${ 't' }: <i>${ 'v' }</i> </span>`;

var PanelBoxTemplate = templater`
<div class="panel panel-inverse" id="container-main">
  <div class="panel-heading">
    <div class="panel-heading-btn">
      <a class="btn btn-xs btn-icon btn-success" onClick="closePanelAction()">
        <i class="fa fa-angle-double-right"></i>
      </a>
    </div>
    <h4 class="panel-title">Propiedades</h4>
  </div>
  <div>

  <div class="tab-content">

    <img src="${ 'img_model' }" width="25px" heigh="25px"></img>
    <i class="fas fa-circle" style="color: ${ 'color_status' };"></i>
    <i style="float: right;" class="fas fa-power-off"></i>

    <div class="tabbable-line">
    <!-- begin nav-pills-panel -->
    <ul class="nav nav-pills-panel">
      <li class="nav-items">
        <a href="#general" data-toggle="tab" class="nav-link-panel active">
          <span>General</span>
        </a>
      </li>



      <li class="nav-items">
        <a href="#graficos" data-toggle="tab" class="nav-link-panel">
          <span>Gráfico</span>
        </a>
      </li>

      <li class="nav-items">
        <a href="#pruebas" data-toggle="tab" class="nav-link-panel tab-line">
          <span>Pruebas</span>
        </a>
      </li>

      <li class="nav-items">
        <a href="#configuracion" data-toggle="tab" class="nav-link-panel">
          <span>Configuraci&oacute;n</span>
        </a>
      </li>

      <li class="nav-items">
        <a href="#logs" data-toggle="tab" class="nav-link-panel">
          <span>Logs</span>
        </a>
      </li>
    </ul>
    </div>

    <hr>

    <span class="br">Número de Servicio: ${ 'service_number_html' }</span>
    <span class="br">descripción:<i>${ 'description' }</i></span>
    <span class="br">IP:<i>${ 'ip' }</i></span>
    <span class="br">MAC:<i>${ 'mac' }</i></span>
    <span class="br">Tiempo ONLINE:<i>${ 'uptime' }</i></span>
    <p></p>
    <!-- begin tab-pane -->

    <div class="tab-pane fade show active" id="general">

      <div class="accordion-container">
        <a onclick="openAccordion(this);" class="accordion-title">
        <span class="toggle-icon"></span>Información general</a>
        <div class="accordion-content" id="accordion_general_info">
        ${ 'generalInfoTemplate' }
        </div>
      </div>
      <div class="accordion-container">
        <a onclick="openAccordion(this);" class="accordion-title" data-event="openWifi">
        <span class="toggle-icon"></span>WiFi</a>
        <div class="accordion-content">
          <button class="btn btn-green btn-sm" onclick="openWifi()"
          style="float: right; display:block;" >Refrescar info</button>
          <div id="accordion_wifi_info"></div>
          
        </div>
      </div>
       <div class="accordion-container">
        <a onclick="openAccordion(this);" class="accordion-title" data-event="openLan">
        <span class="toggle-icon"></span>LAN</a>
        <div class="accordion-content">
          <button class="btn btn-green btn-sm" onclick="openLan()"
          style="float: right; display:block;" >Refrescar info</button>
          <div id="accordion_lan_info"></div>
        </div>
      </div>  

    </div>
    <!-- end tab-pane -->

    <!-- begin tab-pane -->
    <div class="tab-pane fade show" id="anormalidades">
      <div class="accordion-container">
        <a onclick="openAccordion(this);" class="accordion-title">
        <span class="toggle-icon"></span>Anormalidades</a>
        <div class="accordion-content" id="accordion_anormalidades">
        </div>
      </div>
    </div>
    <!-- end tab-pane -->

    <!-- begin tab-pane GRÁFICOS-->
    <div class="tab-pane fade show" id="graficos">
      <div class="accordion-container">
        <a onclick="openAccordion(this), date()"; class="accordion-title">
        <span class="toggle-icon"></span>Gráfico</a>
        <div class="accordion-content" id="accordion_grafico">

          <div class="form-group row">
            <div class="col-lg-12">
              <input type="text" class="form-control" id="datepicker-autoClose" placeholder="Seleccione una fecha." data-date-format="dd/mm/yyyy" />
            </div>
          </div>

          <div id="chartdiv" style="width: 100%; height: 280px;"></div>
        </div>
      </div>

      <div class="accordion-container">
        <a onclick="openAccordion(this), __init_smokeping()"; class="accordion-title">
        <span class="toggle-icon"></span>Smokeping</a>
        <div class="accordion-content" id="accordion_smokeping">
          <div class="col-lg-12">
            <input type="text" autocomplete="off" class="form-control" id="datepicker2-start" placeholder="Fecha Inicial." data-date-format="yyyy-mm-dd" >
          </div>
          
        </div>
      </div>

    </div>
    <!-- end tab-pane -->

    <!-- begin tab-pane -->
    <div class="tab-pane fade show" id="configuracion">
      <div class="accordion-container">
        <a onclick="openAccordion(this);" class="accordion-title">
        <span class="toggle-icon"></span>Configurar WiFi</a>
        <div class="accordion-content" id="accordion_wifi">
        </div>
      </div>

      <div class="accordion-container">
        <a onclick="openAccordion(this);" class="accordion-title">
        <span class="toggle-icon"></span>Plan del Servicio</a>
        <div class="accordion-content" id="accordion_plan">
        </div>
      </div>

      <div class="accordion-container">
        <a onclick="openAccordion(this);" class="accordion-title">
        <span class="toggle-icon"></span>Port forwarding y DMZ</a>
        <div class="accordion-content" id="accordion_configuracion">
        </div>
      </div>      
    </div>
    <!-- end tab-pane -->


    <!-- begin tab-pane -->
    <div class="tab-pane fade show" id="pruebas">
      <div class="accordion-container">
        <a onclick="openAccordion(this);" class="accordion-title">
        <span class="toggle-icon"></span>Ping</a>
        <div class="accordion-content" id="accordion_ping">
          ${ 'pingTemplate' }
        </div>
      </div>
      <div class="accordion-container">
        <a onclick="openAccordion(this);" class="accordion-title">
        <span class="toggle-icon"></span>Traceroute</a>
        <div class="accordion-content" id="accordion_traceroute">
          ${ 'tracerouteTemplate' }
        </div>
      </div>        
    </div>
    <!-- end tab-pane -->


    <!-- begin tab-pane -->
    <div class="tab-pane fade show" id="logs">
      <div class="accordion-container">
        <a onclick="openAccordion(this);" class="accordion-title">
        <span class="toggle-icon"></span>Logs</a>
        <div class="accordion-content" id="accordion_log">
        </div>
      </div>
    </div>
    <!-- end tab-pane -->

  </div>
</div>
</div>`;

var generalWifiTemplate = templater`
<br><br>
<b>WiFi 2G</b>
<span class="br">2.4GHz SSID: <i>${ 'wifi_old_band_ssid' }</i></span>
<span class="br">2.4GHz Estado: <i>${ 'old_band_status' }</i></span>
<span class="br">2.4GHz Modo 802.11: <i>${ 'wifi_old_band_mode' }</i></span>
<span class="br">2.4GHz Equipos conectados: <i>${ 'wifi_old_band_devices' }</i></span>
<hr style="background: #4488de;">
<b>WiFi 5G</b>
<span class="br">5GHz SSID: <i>${ 'wifi_new_band_ssid' }</i></span>
<span class="br">5GHz Estado: <i>${ 'new_band_status' }</i></span>
<span class="br">5GHz Modo 802.11: <i>${ 'wifi_new_band_mode' }</i></span>
<span class="br">5GHz Equipos conectados: <i>${ 'wifi_new_band_devices' }</i></span>`;

var generalLanTemplate = templater`
<div class="table">
  <table class="table" id="table_general_lan">
    <thead>
      <tr>
        <th nowrap="">ID</th>
        <th nowrap="">Tipo</th>
        <th nowrap="">Speed</th>
        <th nowrap="">Duplex</th>
        <th nowrap="">Status</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table></div>`;

var generalInfoTemplate = templater`
<b>Información Red</b>
<span class="br">OLT: <i>${ 'olt_alias' }</i> </span>
<span class="br">Frame: <i>${ 'frame' }</i></span>
<span class="br">Slot: <i>${ 'slot' }</i></span>
<span class="br">Port: <i>${ 'port' }</i></span>
<span class="br">ONT_ID : <i>${ 'onu_id' }</i></span>
<hr style="background: #4488de;">

<span class="br">Service-Port: <i>${ 'service_port' }</i></span>
<span class="br">Main software: <i>${ 'main_software' }</i></span>
<span class="br">Serial Number: <i>${ 'serial' }</i></span>
<span class="br">Distancia: <i>${ 'distance' }m</i></span>
<span class="br">Tx power: <i>${ 'TX' }</i></span>
<span class="br">Rx power: <i>${ 'RX' }</i></span>

<hr style="background: #4488de;">
<b>Salud</b>
<span class="br">CPU: <i>${ 'cpu' } %</i></span>
<span class="br">Memoria: <i>${ 'ram' } %</i></span>
<span class="br">Temperatura: <i>${ 'temperature' } C</i></span>
<span class="br">Tiempo ONLINE: <i>${ 'uptime' }</i></span>`;

var abnormalitiesTemplate = templater`
<ul class="list-group list-group-flush" id="abnormalities_log" style="padding: 10px;"></ul>
${'paginationTemplate'}`;

var abnormalitiesLogTemplate = templater`
<li>${'abnormalitie'} <a href="${'link'}"" target="_blank">
<i style="float: right;" class="fas fa-question-circle"></i></a></li>`;

var logTemplate = templater`
<form class="form-inline">
<div class="form-group m-r-10"><input id="" type="text" class="form-control"></input></div>
<div class="form-group m-r-10"><button class="btn btn-warning" type="button">Buscar</button></div>
</form><ul class="list-group list-group-flush" id="events_log" style="padding: 10px;"></ul>
${'paginationTemplate'}`;

var pingTemplate = templater`
<form class="form-inline" data-parsley-validate="true" id="ping-form">
<div class="row">
<div class="col-4">
<input id="ip_to_ping" required="true" data-parsley-required="true" type="text" class="form-control"></input>
</div>
<div class="col-6">
<button class="btn btn-warning float-right" type="submit">Hacer ping</button>
</div>
</div>
<div id="response_ping"></div>
</form>`;

var paginationTemplate = templater`
<ul class="pagination pagination-sm">
    <li class="page-item ${'disabled_prev'}">
      <a class="page-link" onclick="getLogsOnu('${'id'}',${'prev'})">Anterior</a>
    </li>
    <li class="page-item ${'disabled_next'}">
      <a class="page-link" onclick="getLogsOnu('${'id'}',${'next'})">Siguiente</a>
    </li>
</ul>`;
var eventLogTemplate = templater`<li class="${'color'}">${'entry'}</li>`;


var wifiTemplate = templater`
<div class="row">
  <div class="col-md-12 f-s-11">
    <b>WiFi 2.4 GHz</b>
    <div class="form-group row m-b-2">
      <span class="col-md-4 col-form-label">SSID</span>
      <div class="col-md-8">
        <input type="text" id="wifi_old_band_ssid" value="${'wifi_old_band_ssid'}" class="form-control" />
      </div>
    </div>
    <div class="form-group row m-b-2">
      <span class="col-md-4 col-form-label">Contraseña</span>
      <div class="col-md-8">
        <input type="password" id="wifi_old_band_password" value="${'wifi_old_band_password'}" class="form-control" />
      </div>
    </div>
    <div class="form-group row m-b-2">
      <span class="col-md-4 col-form-label">Canal operaci&oacute;n</span>
      <div class="col-md-8">${'select_old_band_channel'}</div>
    </div>

    <div class="p-t-10">
    <b>WiFi 5 GHz</b>
    <div class="form-group row m-b-2">
      <span class="col-md-4 col-form-label">SSID</span>
      <div class="col-md-8">
        <input type="text" id="wifi_new_band_ssid" value="${'wifi_new_band_ssid'}" class="form-control" />
      </div>
    </div>
    <div class="form-group row m-b-2">
      <span class="col-md-4 col-form-label">Contraseña</span>
      <div class="col-md-8">
        <input type="password" id="wifi_new_band_password" value="${'wifi_new_band_password'}" class="form-control" />
      </div>
    </div>
    <div class="form-group row m-b-2">
      <span class="col-md-4 col-form-label">Canal operaci&oacute;n</span>
      <div class="col-md-8">${'select_new_band_channel'}</div>
    </div>
    <div class="form-group row m-b-2 pull-right m-t-2 m-r-2">
      <button class="btn btn-warning" type="button" onClick="changePlan('${'id'}')">Aplicar</button>
    </div>    
    </div>

  </div>
</div>`;

var configurationTemplate = templater`
<div class="row">
  <div class="col-md-12 col-xl-12 f-s-11">
    <div class="form-group row m-b-2">
      <span class="col-md-5 col-form-label">DMZ</span>
      <div class="col-md-7">
      <input type="radio" class="form-check-input m-t-5" name="config" value="dmz" id="config_dmz">
      <input id="ip_dmz" type="text" class="form-control m-b-2" placeholder="IP" value="${'dmz'}"></input>
      </div>
    </div>
    <div class="form-group row m-b-2">
      <span class="col-md-5 col-form-label">Port forwarding</span>
      <div class="col-md-7">
      <input type="radio" class="form-check-input m-t-5" name="config" value="pf" id="config_pf">
      <label class="form-check-label">Aún no disponible.</label>
      </div>
    </div>
    <div class="form-group row m-b-2 pull-right m-t-2 m-r-2">
      <button class="btn btn-warning" type="button" onClick="saveConfig('${'id'}')">Aplicar</button>
    </div>
  </div>
</div>`;


function addLogPanel(data) {

  if (data.type < 10) {
    color = '';
  }
  else{
    if (data.type >= 20){
      color = 'text-green-lighter';
    }
    else{
      color = 'text-red-lighter';
    }
  }

  var obj = {
    entry:data.entry,
    color: color
  };
  $("#events_log").append(eventLogTemplate(obj));
}


function getLogsOnu(id, page=1) {

  var url = DjangoURL('api_log_onu',id) + '?page=' + page;

  axios.get(url, header_common)
  .then(function(response){
    if (response.status == 200)
    {
      var html_log,
          go_next = (response.headers['go_next'] && response.headers['go_next'] != 0),
          go_prev = (response.headers['go_prev'] && response.headers['go_prev'] != 0)

      if (go_next || go_prev){
        disabled_prev = go_prev ? '' : 'disabled';
        disabled_next = go_next ? '' : 'disabled';

        html_log = logTemplate({
          paginationTemplate:paginationTemplate({
            disabled_prev:disabled_prev,
            disabled_next:disabled_next,
            prev:response.headers['go_prev'],
            next:response.headers['go_next'],
            id:id
          })
        })
      }
      else{
        html_log = logTemplate();
      }
      
      $("#accordion_log > *").remove();
      $("#accordion_log").html(html_log);
      $("#events_log").html("");

      for (var i = 0; i < response.data.length; i++) {
        addLogPanel(response.data[i])
      }

    }
    if (response.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}


function getAnormalities(id, page=1) {

  var url = DjangoURL('api_anormalitie_onu',id) + '?page=' + page;
  axios.get(url, header_common)
  .then(function(response){
    if (response.status == 200)
    {

      html_log = abnormalitiesTemplate();
      $("#accordion_anormalidades > *").remove();
      $("#accordion_anormalidades").html(html_log);
      $("#abnormalities_log").html("");
      for (var i = 0; i < response.data.length; i++) {
        $("#abnormalities_log").append(abnormalitiesLogTemplate(response.data[i]));
      }

    }
    if (response.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}



var changePlanTemplate = templater`
<div class="row">  
  <div class="col-md-12 f-s-11">
    <b>Plan Actual Sentinel:</b> <br>
    <p>${ 'plan_sentinel_name' }</p>

    <b>Plan Actual Matrix:</b> <br>
    <p>${ 'plan_matrix_name' }</p>

    <b>Velocidad bajada:</b> <br>
    <p>${ 'down' }</p>

    <b>Velocidad Subida:</b> <br>
    <p>${ 'up' }</p>

    <div class="p-t-10">
      <div class="form-group row m-b-2 pull-right m-t-2 m-r-2">
        <div class="m-b-10"> <a href="#modal-change-plan" class="btn btn-sm btn-warning" data-toggle="modal">Cambiar Plan</a></div>
      </div> 
    </div>


  </div>

</div>
`;

// ============== GRAFICO ======================\\

function getGrafico(serial, fecha, fecha2) {

  var url = 'api/v1/onu-pg-view/';
  header_common['params'] = {
    serial:serial,
    time_date:fecha
  }


  axios.get(url, header_common)
  .then(function(response){
    if (response.status == 200)
    {
      var $data = response.data

      displayGraph($data,fecha2)

    }

  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });


}

// ============== GRAFICO ======================\\


function getConfigurationOnu(id) {
 
  var url = DjangoURL('api_config_onu', id);
  axios.get(url, header_common)
  .then(function(response){

    if (response.status == 200)
    {
      var valueToCompare = response.data.wifi_old_band_channel;
      var select_configTemplate_1 = `<select class="form-control" id="selectOldWifi">
        <option value="1" ${valueToCompare == 1 ? "selected":""}>1</option>
        <option value="2" ${valueToCompare == 2 ? "selected":""}>2</option>
        <option value="3" ${valueToCompare == 3 ? "selected":""}>3</option>
        <option value="4" ${valueToCompare == 4 ? "selected":""}>4</option>
        <option value="5" ${valueToCompare == 5 ? "selected":""}>5</option>
        <option value="6" ${valueToCompare == 6 ? "selected":""}>6</option>
        <option value="7" ${valueToCompare == 7 ? "selected":""}>7</option>
        <option value="8" ${valueToCompare == 8 ? "selected":""}>8</option>
        <option value="9" ${valueToCompare == 9 ? "selected":""}>9</option>
        <option value="10" ${valueToCompare == 10 ? "selected":""}>10</option>
        <option value="11" ${valueToCompare == 11 ? "selected":""}>11</option>
        <option value="12" ${valueToCompare == 12 ? "selected":""}>12</option>
        <option value="13" ${valueToCompare == 13 ? "selected":""}>13</option>
        <option value="14" ${valueToCompare == 14 ? "selected":""}>14</option>
      </select>`;

      var valueToCompare = response.data.wifi_new_band_channel;
      var select_configTemplate_2 = ` <select class="form-control" id="selectNewWifi">
        <option value="1" ${valueToCompare == 1 ? "selected":""}>1</option>
        <option value="2" ${valueToCompare == 2 ? "selected":""}>2</option>
        <option value="3" ${valueToCompare == 3 ? "selected":""}>3</option>
        <option value="4" ${valueToCompare == 4 ? "selected":""}>4</option>
        <option value="5" ${valueToCompare == 5 ? "selected":""}>5</option>
        <option value="6" ${valueToCompare == 6 ? "selected":""}>6</option>
        <option value="7" ${valueToCompare == 7 ? "selected":""}>7</option>
        <option value="8" ${valueToCompare == 8 ? "selected":""}>8</option>
        <option value="9" ${valueToCompare == 9 ? "selected":""}>9</option>
        <option value="10" ${valueToCompare == 10 ? "selected":""}>10</option>
        <option value="11" ${valueToCompare == 11 ? "selected":""}>11</option>
        <option value="12" ${valueToCompare == 12 ? "selected":""}>12</option>
        <option value="13" ${valueToCompare == 13 ? "selected":""}>13</option>
        <option value="14" ${valueToCompare == 14 ? "selected":""}>14</option>
      </select>`;

      var obj = extend({
                'id':id,
                'select_old_band_channel':select_configTemplate_1,
                'select_new_band_channel':select_configTemplate_2
                },
                response.data);

      $("#accordion_wifi").html(wifiTemplate(obj));
      $("#accordion_configuracion").html(configurationTemplate(obj));
      if (obj.dmz_or_port_forwarding)
      {
        $( "#config_dmz" ).prop( "checked", true );
        $( "#config_pf" ).prop( "checked", false );
      }else{
        $( "#config_dmz" ).prop( "checked", false );
        $( "#config_pf" ).prop( "checked", true );
      }

    }
    if (response.status == 401)
    {
      handleUnauthorizedAxios();
    }

  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });

}

function panel_fsp_content(el, id) {
  HDD.set("panel_type",'fsp');

  var url = DjangoURL('fsp_api',id)
  axios.get(url, header_common)
  .then(function(res){
    if (res.status == 200)
    {

      var _content = extend(res.data,{});
      $(el).html(PanelFSPTemplate(_content));
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });

}

function panel_onu_content(el, id) {

  
  HDD.unset('olt_alias')


  var url = DjangoURL('api_onu_detail',id)
  axios.get(url, header_common)
  .then(function(res){

    console.log(res)

    if (res.status == 200)
    {

      HDD.set("panel_type",'onu');
      
      HDD.set("olt_alias",res.data.olt_alias);



      service_num = res.data.service;
      if (service_num){

        if (res.data.service_number_verified)
        {
          var html_service_verified = `<i class="fas fa-check"
          ondblclick="serviceDetail(${service_num})" style="color: ${Django.color_success}"></i>`;
        }else{
          var html_service_verified = `<i class="fas fa-check" 
          onClick="serviceVerified(this,'${res.data.uuid}')" style="color: orange"></i>`;          
        }
        
        var service_number_html = `
        <i><a target="_blank" href="https://optic.matrix2.cl/contracts/${service_num}">${service_num}</a>
        ${html_service_verified}</i>`;

        getPlanOnu(service_num)
        

      }else{
        var service_number_html = '<i style="color:red">Sin número agregado</i>';
      }
      
      var old_band_status = res.data.wifi_old_band_status ? 'conectado' : 'desconectado'; 
      var new_band_status = res.data.wifi_new_band_status ? 'conectado' : 'desconectado';
      var content_general = extend(res.data,{old_band_status:old_band_status,
                                             new_band_status:new_band_status});
      
      

      var color_status = res.data.status ? Django.color_success : Django.color_error;

      var content = extend(res.data,{
          mac:res.data.mac,
          generalInfoTemplate:generalInfoTemplate(content_general),
          generalWifiTemplate:generalWifiTemplate(content_general),
          generalLanTemplate:generalLanTemplate(res.data),
          color_status: color_status,
          img_model:Django.STATIC_URL + 'img/devices/'+res.data.model +'.jpg',
          service_number_html:service_number_html,
          pingTemplate:pingTemplate({})
      });

      getLogsOnu(id);
      getConfigurationOnu(id);
      getAnormalities(id);          

      $(el).html(PanelBoxTemplate(content));
  


      $('#ping-form').parsley();

      for (var i = 0; i < res.data.lan.length; i++) {
        var status_l = res.data.lan[i].status ? 'up' : 'dows';
        var speed_l = res.data.lan[i].speeds ? res.data.lan[i].speeds : '-';
        var duplex_l = res.data.lan[i].duplex ? res.data.lan[i].duplex : '-';
        var reg = '<tr><td>'+res.data.lan[i].port_id+
        '</td><td>'+res.data.lan[i].port_type+'</td><td>'+speed_l+
        ' Mbps</td><td>'+duplex_l+'</td><td>'+status_l+'</td></tr>';
        $("#table_general_lan").append(reg)
      }

    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });

}



///** events **///
function closePanelAction() {
  $('.theme-panel').removeClass('active');
  HDD.set('panel_page','0');
}


function saveConfig(id) {
  var url = DjangoURL('api_config_onu',id)
  var configChecked = $('input[name=config]:checked').val()
  if (configChecked == 'dmz'){
    var data = { 
      dmz_or_port_forwarding:true,
      dmz:$("#ip_dmz").val()
    }
  }
  else
  {
    var data = { 
      dmz_or_port_forwarding:false
    }
  }

  axios.patch(url, data, header_common)
  .then(function(res){
    if (res.status == 200)
    {
      _exito('Configuración guardada exitosamente.');
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });

}


function saveConfigWifi(id) {

  var url = DjangoURL('api_config_onu',id)
  var data = {
    wifi_new_band_ssid:$("#wifi_new_band_ssid").val(),
    wifi_new_band_password:$("#wifi_new_band_password").val(),
    wifi_new_band_channel:$('#selectNewWifi option:selected').val(),
    wifi_old_band_ssid:$("#wifi_old_band_ssid").val(),
    wifi_old_band_password:$("#wifi_old_band_password").val(),
    wifi_old_band_channel:$('#selectOldWifi option:selected').val()    
  }

  axios.patch(url, data, header_common)
  .then(function(res){
    if (res.status == 200)
    {
      _exito('Configuración WiFi guardada exitosamente.');
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });

}


function serviceVerified(self, id){
  var url = DjangoURL('api_onu_detail', id)
  axios.patch(url, {service_number_verified: true}, header_common)
  .then(function(res){
    if (res.status == 200)
    {
      $(self).css('color',Django.color_success);
      _exito('Número de servicio verificado.');
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

function serviceDetail(service){
  var el = document.createElement("div");

  var url = DjangoURL('api_matrix_service_number_detail', service) + '?modal_info=true'
  axios.get(url, header_common)
  .then(function(res){
    if (res.status == 200)
    {
      d = res.data
      el.innerHTML = `${d.name}<br>rut: ${d.rut}<br>
      <a href="https://optic.matrix2.cl/contracts/${service}">Ver en matrix</a>`;
      swal("Detalle del cliente", {
        content: el,
      });
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });



}


function ping() {
  $("#response_ping").html('<span class="spinner"></span>');
  var url = DjangoURL('api_ssh_onu_ping', HDD.get('panel_open'))
  axios.put(url, {ip: $("#ip_to_ping").val()}, header_common)
  .then(function(res){
    if (res.status == 200)
    {
      $("#response_ping").html(res.data.message);
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}


Parsley.on('form:submit', function() {
  if(this.element.id == "ping-form") ping();
  return false; // Don't submit form for this demo
});



function openWifi(){
  $("#accordion_wifi_info").html('<center><img src="/static/img/loading_spinner.gif" width="100px" heigh="100px"></img></center>');

  var url = DjangoURL('api_update_remote_wifi', HDD.get('panel_open'));
  axios.get(url, header_common)
  .then(function(response){
    if (response.data.error){
      $("#accordion_wifi_info").html('<center>'+response.data.error+'</center>');
    }else{
      var old_band_status = response.data.wifi_old_band_status ? 'conectado' : 'desconectado'; 
      var new_band_status = response.data.wifi_new_band_status ? 'conectado' : 'desconectado';
      var content = extend(response.data,
                          {old_band_status:old_band_status,new_band_status:new_band_status})
      $("#accordion_wifi_info").html(generalWifiTemplate(content));      
    }

  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
      
}

function openLan(){
  $("#accordion_lan_info").html('<center><img src="/static/img/loading_spinner.gif" width="100px" heigh="100px"></img></center>');

  var url = DjangoURL('api_update_remote_lan', HDD.get('panel_open'));
  axios.get(url, header_common)
  .then(function(res){
    if (res.data.error){
      $("#accordion_lan_info").html('<center>'+res.data.error+'</center>');
    }else{

      $("#accordion_lan_info").html(generalLanTemplate({}));

      for (var i = 0; i < res.data.length; i++) {
        var status_l = res.data[i].status ? 'up' : 'dows';
        var speed_l = res.data[i].speeds ? res.data.speeds : '-';
        var duplex_l = res.data[i].duplex ? res.data.duplex : '-';
        var reg = '<tr><td>'+res.data[i].port_id+
        '</td><td>'+res.data[i].port_type+'</td><td>'+speed_l+
        ' Mbps</td><td>'+duplex_l+'</td><td>'+status_l+'</td></tr>';
        $("#table_general_lan").append(reg);
      }
   
    }

  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
      
}


function openFSP(){
  $("#content_fsp").html('<center><img src="/static/img/loading_spinner.gif" width="100px" heigh="100px"></img></center>');

  var url = DjangoURL('api_update_remote_statistics_fsp', HDD.get('panel_open'));
  axios.get(url, header_common)
  .then(function(res){
    if (res.data.error){
      $("#content_fsp").html('<center>'+res.data.error+'</center>');
    }else{

      discarded_rec = res.data.discarded_rec;
      if (discarded_rec > 2) { discarded_rec='<span style="color: red">'+discarded_rec+'%</span>'}

      error_rec = res.data.error_rec;
      if (error_rec > 2) { error_rec='<span style="color: red">'+error_rec+'%</span>'}

      var html_fsp = span_br({t:'Frames Unicast Recibidos',v:res.data.unicast_rec + '%'});
      html_fsp += span_br({t:'Frames Multicast Recibidos',v:res.data.multicast_rec + '%'});
      html_fsp += span_br({t:'Frames Broadcast Recibidos',v:res.data.broadcast_rec + '%'});
      html_fsp += span_br({t:'Frames Descartados Recibidos',v:discarded_rec});
      html_fsp += span_br({t:'Frames Recibidos con Errores',v:error_rec});
      html_fsp += span_br({t:'Frames Unicast Enviados',v:res.data.unicast_send + '%'});
      html_fsp += span_br({t:'Frames Multicast Enviados',v:res.data.multicast_send + '%'});
      html_fsp += span_br({t:'Frames Broadcast Enviados',v:res.data.broadcast_send + '%'});
      $("#content_fsp").html(html_fsp);   
    }

  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
      
}

function updatePortDesc(self) {

  var url = DjangoURL('fsp_api', HDD.get('panel_open'));
  axios.patch(url, {description: self.innerText}, conf)
  .then(function(response){
    if (response.status == 200)
    {
      $.gritter.add({
        title: 'Puerto actualizado',
        time: 650,
      });
    }
  })//end then
  .catch(function(error) {
    handleErrorAxios(error);
  });
}


get_availables_plans =  (id) => {
  let conf = {
    headers: { Authorization: `jwt ${HDD.get(Django.name_jwt)}`}
  };

  // consulta de planes
  let url = DjangoURL('api_plan_list');
  axios.get(url+'?with_system_speed=True', conf)
  .then(function(res){
    if (res.status == 200)
    {
      let select_planes = $('#change_plan_availables_plans')  
      console.log(res.data);
      
      res.data.forEach(function(plan) {
        
        if (id == plan.id)
        {
          select_planes.append(`<option selected="selected" value="${plan.id}">${plan.name}</option>`);
        }
        else
        {
          select_planes.append(`<option value="${plan.id}">${plan.name}</option>`);
        }
        
      });
      



    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });

};


function getPlanOnu(service_number) {
 
  var url = DjangoURL('change_plan', service_number);
  axios.get(url, header_common)
  .then(function(response){
    
    console.log(response)

    if (response.status == 200)
    {

      var obj = extend({},
                response.data);

      $("#accordion_plan").html(changePlanTemplate(obj));
      $("#change_plan_plan_actual_matrix").text(response.data.plan_matrix_name)
      $("#change_plan_plan_actual_sentinel").text(response.data.plan_sentinel_name)
      $("#change_plan_velocidad_bajada").text(response.data.down)
      $("#change_plan_velocidad_subida").text(response.data.up)
      $("#change_plan_precio_mensual").text(`$${response.data.plan_matrix_price}`)
      $("#change_plan_service_number").val(service_number)

      get_availables_plans(response.data.plan_sentinel_id)

    }
    if (response.status == 401)
    {
      handleUnauthorizedAxios();
    }

  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });

}



function changePlan() {

  var url = DjangoURL('change_plan', $("#change_plan_service_number").val())
  var data = {
    plan: parseInt($('#change_plan_availables_plans').val()),    
  }

  axios.post(url, data, header_common)
  .then(function(res){
    if (res.status == 200)
    {
      _exito('Plan Actualizado exitosamente.');
    }
    if (res.status == 400 || res.status == 404)
    {
      _error(""+ res.response.data.detail).then(function(){  
      });
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      alert(error.response.data.detail)
      handleErrorAxios(error);
  });

}


// ========================= BEGIN GRÁFICO RX Y TX ========================= \\


function date(){


  $('#datepicker-autoClose').datepicker({
    todayHighlight: true,
    autoclose: true,
    language: 'es',
    
  }).on('change',function(){
    var serial = HDD.get("serialGraph");

    if(serial){
      var fecha = $(this).val();
      var fecha2 = fecha;

      fecha = fecha.replace('/','-').replace('/','-').split('-')

      var annio = fecha[2]
      var mes = fecha[1]
      var dia = fecha[0]
    
      fecha = annio + '-' + mes + '-' + dia

      getGrafico(serial,fecha,fecha2)

    }else{
      _info('Se debe seleccionar previamente un elemento en el panel de las onu.')
    }

  
  });
  
}

function displayGraph(data,fecha){
  
  am4core.ready(function() {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    am4core.useTheme(am4themes_animated);

    // Themes end
  
    // Create chart
    var chart = am4core.create("chartdiv", am4charts.XYChart);
    chart.paddingRight = 20;
    
    data = data.map(function(x) {
        x.time_tx = new Date(x.time_tx);
        x.time_rx = new Date(x.time_rx);
      return x
    });

    chart.language.locale = am4lang_es_ES;
    chart.numberFormatter.language = new am4core.Language();
    chart.numberFormatter.language.locale = am4lang_es_ES;
    chart.dateFormatter.language = new am4core.Language();
    chart.dateFormatter.language.locale = am4lang_es_ES;

    chart.language.locale["_date_millisecond"] = "mm:ss SSS";
    chart.language.locale["_date_second"] = "HH:mm:ss";
    chart.language.locale["_date_minute"] = "HH:mm";
    chart.language.locale["_date_hour"] = "HH:mm";
    chart.language.locale["_date_day"] = "dd/MM";
    chart.language.locale["_date_week"] = "ww";
    chart.language.locale["_date_month"] = "MMM";
    chart.language.locale["_date_year"] = "yyyy";
    chart.data = data;

    var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.baseInterval = {
    "timeUnit": "second",
    "count": 1
    };
    // dateAxis.tooltipDateFormat = "i";
    // dateAxis.dateFormats.setKey("millesecond", "i");

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.axisRanges.clear()
    valueAxis.tooltip.disabled = true;
    valueAxis.title.text = "TX - RX " + fecha;

    var series = chart.series.push(new am4charts.LineSeries());
    series.dataFields.dateX = "time_tx";
    series.dataFields.valueY = "tx";
    series.tooltipText = "TX: [bold]{valueY}[/]";
    series.fillOpacity = 0.3;
    series.name = 'TX'

    var series2 = chart.series.push(new am4charts.LineSeries());
    series2.dataFields.dateX = "time_rx";
    series2.dataFields.valueY = "rx";
    series2.tooltipText = "RX: [bold]{valueY}[/]";
    series2.hidden = true;
    series2.fillOpacity = 0.3;
    series2.name = 'RX'


    chart.cursor = new am4charts.XYCursor();
    chart.cursor.lineY.opacity = 0;
    chart.scrollbarX = new am4charts.XYChartScrollbar();
    chart.scrollbarX.series.push(series);

    // Add legend
    chart.legend = new am4charts.Legend();

    chart.events.on("datavalidated", function () {
        dateAxis.zoom({start:0.8, end:1});
    });

    
  }); // end am4core.ready()

}





// ========================= END GRÁFICO RX Y TX ========================= \\


var fecha = moment().format("YYYY-MM-DD")


function requestSmokeping(menu=HDD.get('olt_alias'),ip_onu=HDD.get('ip_address'),date=fecha){
  
  if((HDD.get('ip_address')=='None')||(HDD.get('ip_address')=='undefined'))
  {
    $.gritter.add({
      time: '10000',
      title: 'Noticias Smokeping',
      text: 'La selección de una ONU con una IP vacía o nula no permite mostrar ningún tipo de gráfico desde el servidor Smokeping, por favor seleccione una con una OLT-ONU con IP válida.'
    });
  }
  else
  {

    var $data = {
      menu : menu,
      ip_onu : ip_onu,
      endDateTime : date
    }

    var conf = {
      headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
    };

    conf['params'] = $data;
    
    var api = `api/v1/smokeping-server/`
  
    axios.get(api,conf)
    .then((response)=>{
      if(response.status === 200)
      {
        $("#accordion_smokeping").empty();
  
        var $data = response.data;
  
        for(i=0;i<$data.length;i++){
          periodo = i+1;
          $("#accordion_smokeping").append(imgSmokepingTemplate($data[i],periodo))
        }
  
        $("#accordion_smokeping")
        .append(`<div class="form-group row">
                  <div class="col-lg-4 offset-lg-4">
                    <button 
                      type="button" 
                      title="Reset"
                      class="btn btn-default btn-sm form-control form-control-sm" 
                      id="smokeping_restart_btn"
                      onload = "this."
                      onclick="return cleanDatepicker();">
                      <i class="fa fa-redo"></i>
                    </button>
                  </div>
                </div>`)
        
        $("#datepicker2-start").trigger('reset')

        $('#datepicker2-start').on('click', function(e) {
          e.preventDefault();
          $(this).attr("autocomplete", "off");  
          });
  
        $("#imgA").on('click', function(){
          load_modal(ip_onu,$data[0].src,'del día',$data[0].url)
        })
  
        $("#imgB").on('click', function(){
          load_modal(ip_onu,$data[1].src,'de la semana',$data[1].url)
        })
  
        $("#imgC").on('click', function(){
          load_modal(ip_onu,$data[2].src,'del mes',$data[2].url)
        })
  
  
      }
    })
    .catch((error)=>{
      handleErrorAxios(error)
    })
  }

}

function imgSmokepingTemplate($obj,periodo){
  
  var id = {1:'imgA',2:'imgB',3:'imgC'}

  var tmp = `
  <div class="form-group row">
      <h5 class="text-center text-title">${$obj.date}</h5>
      <img src="${$obj.src}" class="img-thumbnail" id="${id[periodo]}"/>
  </div>
  `;

  return tmp;
}


function load_modal(ip,url,tiempo,urlGral){

  var modal_body = `<div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Gráfico Smokeping ${tiempo} para la IP ${ip}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <p>
          <a href="${urlGral}" target="_blank" title="Smokeping Server">
            <img src="${url}" class="img-thumbnail"></img>
          </a>
        </p>
      </div>
      <div class="modal-footer">
        <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Cerrar</a>
      </div>
    </div>
  </div>`


  $("#modal-message").html(modal_body).modal('show')

}

//Smokeping por default

function __init_smokeping(){

  $("#accordion_smokeping")
  .empty()
  .append(`<div class="form-group row">
            <div class="col-lg-12">
              <input 
                type="text" 
                class="form-control" 
                id="datepicker2-start" 
                placeholder="Fecha Inicial."
                autocomplete="off"
                data-date-format="yyyy-mm-dd" />
            </div>
          </div>`)

  $("#datepicker2-start").attr('autocomplete','off')


  $('#datepicker2-start').datepicker({
    todayHighlight: true,
    autoclose: true,
    language: 'es',
    format:'yyyy-mm-dd'
    
  }).on('change',function(){
    $(this).attr("autocomplete", "off");
    var fecha = $(this).val(); 
    var ip_onu = HDD.get('ip_address');

    if(ip_onu=='None')
    {
      $.gritter.add({
        time: '10000',
        title: 'Noticias Smokeping',
        text: 'La selección de una ONU con una IP vacía no permite mostrar ningún tipo de gráfico desde el servidor Smokeping, por favor seleccione una con una ONU con IP válida.'
      });
    }else{
      requestSmokeping(HDD.get('olt_alias'),ip_onu,fecha)
    }
  
    
  })

}



function cleanDatepicker(){
  $("#accordion_smokeping")
  .empty()
  .append(`<div class="form-group row">
            <div class="col-lg-12">
              <input 
                type="text" 
                class="form-control" 
                id="datepicker2-start" 
                placeholder="Fecha Inicial."
                autocomplete="off"
                data-date-format="yyyy-mm-dd" />
            </div>
          </div>`)
  $("#datepicker2-start").val('').trigger('change');

  var old_fn = $.datepicker._updateDatepicker;

  $.datepicker._updateDatepicker = function(inst) {
     old_fn.call(this, inst);

     var buttonPane = $(this).datepicker("widget").find(".ui-datepicker-buttonpane");

     $("<button type='button' class='ui-datepicker-clean ui-state-default ui-priority-primary ui-corner-all'>Delete</button>").appendTo(buttonPane).click(function(ev) {
         $.datepicker._clearDate(inst.input);
     }) ;
  }

  __init_smokeping();
}
// ========================= END SMOKEPING ========================= \\