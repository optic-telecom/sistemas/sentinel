userInSession();
App.setPageTitle('Sentinel - Extremos físicos');

//$("#menu_Extremo > a ").css('cssText', "color: #FFF !important;");
$("#menu_fmm").addClass('active');
$("#menu_fmm_physicalextreme").addClass('active');
var oTable;
var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};


$(document).ready(function (){

  url_dt = Django.datatable_physicalextreme_list;
  oTable = $('#table_physicalextreme').dataTable({
    "aaSorting":[[0,"desc"]],
    "language": {
    "sLengthMenu":     "Mostrar _MENU_ extremos",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando extremo del _START_ al _END_ de un total de _TOTAL_ extremos",
    "sInfoEmpty":      "Mostrando extremo del 0 al 0 de un total de 0 extremos",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ extremos)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "sProcessing": "Cargando...",
    "oPaginate": {
      "sFirst":    "Primero",
      "sLast":     "Último",
      "sNext":     "Siguiente",
      "sPrevious": "Anterior"
    },

    "oAria": {
      "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
  },
    "processing": true,
    "serverSide": true,
    "searchDelay": 400,
    "ajax": {
        "url": url_dt,
        "type": "POST",
        "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
    },
    "columnDefs": [
    {
      orderable: true,
      searchable: true,
      targets: [0,1,2]
    },
    {
      orderable: false,
      searchable: false,
      targets: [3]
    }
    ], 
  });

  oTable.on( 'order.dt', function (e, settings) {

    var order = oTable.api().order();
    $("#table_physicalextreme > thead > tr > th").css('color','black')
    $( "#table_physicalextreme > thead > tr > th:eq( "+order[0][0]+" )" )
    .css( "color", "#2271b3" );

  });

  // Error messages
  const customMessages = {
    valueMissing:    'Campo obligatorio',       // `required` attr
  }

  function getCustomMessage (type, validity) {
    if (validity.typeMismatch) {
      return customMessages[`${type}Mismatch`]
    } else {
      for (const invalidKey in customMessages) {
        if (validity[invalidKey]) {
          return customMessages[invalidKey]
        }
      }
    }
  }

  var invalidClassName = 'invalid'
  var inputs = document.querySelectorAll('input, select, textarea')
  inputs.forEach(function (input) {
    // Each time the user types or submits, this will
    // check validity, and set a custom message if invalid.
    function checkValidity () {
      const message = input.validity.valid
        ? null
        : getCustomMessage(input.type, input.validity, customMessages)
      input.setCustomValidity(message || '')
      if (message!=null){
        input.classList.add(invalidClassName)
      }else{
        input.classList.remove(invalidClassName)
      }
      
    }
    input.addEventListener('input', checkValidity)
    input.addEventListener('invalid', checkValidity)

  })

  // Remove error messages when modal is closed
  $('#modal-create').on('hidden.bs.modal', function () {
    inputs.forEach(function (input) {
      input.classList.remove(invalidClassName)
    })
  })

  $('#modal-update').on('hidden.bs.modal', function () {
    inputs.forEach(function (input) {
      input.classList.remove(invalidClassName)
    })
  })

});  

function newExtreme() {

  $('#modal-create').modal('hide');
  $('#modal-create-sleep').modal('show');
  var url = Django.FMM_URL + '/api/v1/physicalextreme/'
  var data = { 
    "address": $("#address").val(),
    "commune": $("#commune").val(),
    "location": $("#location").val()
  }

  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 201)
    {
      swal({
      title: "Creado",
        text: 'Extremo físico creado exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        updateDatatable();
        $('#modal-create-sleep').modal('hide');
      });
      $("#address").val('');
      $("#commune").val('');
      $("#location").val('');
    }
  
  })//end then
  .catch(function(error) {
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }else{
        _error("Por favor verifique los datos ingresados.").then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }
    }

    if (error.response.status == 500)
    {

      _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){
        $('#modal-create-sleep').modal('hide');
      });
      
    }     
     
    //handleErrorAxios(error);
  });
  return false;
};

function getDetailsPhysicalExtreme(id) {
  var url = DjangoURL('api_physicalextreme_detail', id)
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      $("#det_address").val(res.data.address);
      $("#det_commune").val(res.data.commune);
      $("#det_location").val(res.data.location);
      $("#update-form").attr("onsubmit","return updatePhysicalExtreme("+id+")");
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

function updatePhysicalExtreme(id) {

  $('#modal-update').modal('hide');
  $('#modal-create-sleep').modal('show');
  var url = DjangoURL('api_physicalextreme_detail', id)
  var data = { 
    "address": $("#det_address").val(),
    "commune": $("#det_commune").val(),
    "location": $("#det_location").val()
  }
  axios.patch(url, data, conf)
  .then(function(res){
    if (res.status == 200)
    {
      // $('#modal-create-sleep').modal('hide');
      swal({
      title: "Moficado",
        text: 'Extremo físico modificado exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        updateDatatable();
        $('#modal-create-sleep').modal('hide');
      });
    }
  
  })//end then
  .catch(function(error) {
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }else{
        _error("Por favor verifique los datos ingresados.").then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }
    }

    if (error.response.status == 500)
    {

      _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){
        $('#modal-create-sleep').modal('hide');
      });
      
    }     
      
    //handleErrorAxios(error);
  });
  return false;
};

delete_extreme = (id) => {
  var url = Django.FMM_URL + '/api/v1/physicalextreme/' + id + '/';
  delete_object(null, 'Extremo', url, null, function(){
    oTable.api().ajax.reload(null, false);
  });
}


ayuda = () => {
  alert('Pagina de ayuda de fmm')
}