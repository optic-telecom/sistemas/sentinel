userInSession();
App.setPageTitle('Sentinel - Lista de Planes');
var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};

$("#menu_planes").addClass('active')

var AggregationRateMask, DownloadSpeedMask, UploadSpeedMask, AggregationRateMaskEdit, DownloadSpeedMaskEdit, UploadSpeedMaskEdit;
var AggregationRateMaskUTP,AggregationRateMaskEditUTP;
var modal_create_utp_loaded = false;

$(document).ready(function (){

  AggregationRateMask = new IMask(
    document.getElementById('aggregation_rate'), {
      mask: '{1:}000'
  });

  AggregationRateMaskUTP = new IMask(
    document.getElementById('utp_aggregation_rate'), {
      mask: '{1:}000',
      lazy:false
  });

  DownloadSpeedMask = new IMask(
    document.getElementById('download_speed'), {
      mask: '0[****] Mbps',
      lazy:false
  });

  UploadSpeedMask = new IMask(
    document.getElementById('upload_speed'), {
      mask: '0[****] Mbps',
      lazy:false
  });

  AggregationRateMaskEdit = new IMask(
    document.getElementById('aggregation_rate_edit'), {
      mask: '{1:}000'
  });

  AggregationRateMaskEditUTP = new IMask(
    document.getElementById('utp_aggregation_rate_edit'), {
      mask: '{1:}000'
  });

  DownloadSpeedMaskEdit = new IMask(
    document.getElementById('download_speed_edit'), {
      mask: '0[****] Mbps',
      lazy:false
  });

  UploadSpeedMaskEdit = new IMask(
    document.getElementById('upload_speed_edit'), {
      mask: '0[****] Mbps',
      lazy:false
  });
  

  url_dt = Django.datatable_plan;
  oTable = $('#table_plan').dataTable({
        "aaSorting":[[0,"desc"]],
        "language": {
        "sLengthMenu":     "Mostrar _MENU_ Plan",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando Plan del _START_ al _END_ de un total de _TOTAL_ Plan",
        "sInfoEmpty":      "Mostrando Plan del 0 al 0 de un total de 0 Plan",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ Plan)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "sProcessing": "Cargando...",
        "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
        },

        "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      },
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": url_dt,
            "type": "POST",
            "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
        },
      "columnDefs": [
      {
        orderable: true,
        searchable: true,
        targets: [0,1,2,3,4,5,6,8]
      },
      {
        orderable: false,
        searchable: false,
        targets: [9, 7]
      },
      { targets: [7], visible: false }
      ],
   });

  url_dt = Django.datatable_utp_plan;
  oTable = $('#table_utp_plan').dataTable({
        "aaSorting":[[0,"desc"]],
        "language": {
        "sLengthMenu":     "Mostrar _MENU_ Plan UTP",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando Plan UTP del _START_ al _END_ de un total de _TOTAL_ Plan UTP",
        "sInfoEmpty":      "Mostrando Plan UTP del 0 al 0 de un total de 0 Plan UTP",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ Plan UTP)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "sProcessing": "Cargando...",
        "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
        },

        "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      },
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": url_dt+'?type_plan=utp',
            "type": "POST",
            "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
        },
      "columnDefs": [
      {
        orderable: true,
        searchable: true,
        targets: [0,1,2,3,6,7,8]
      },
      {
        orderable: false,
        searchable: false,
        targets: [9]
      },
      { targets: [4,5], visible: false }
      ],
   });

  var invalidClassName = 'invalid';
  var inputs = document.querySelectorAll('input','textarea');
  inputs.forEach(function (input) {
    // Each time the user types or submits, this will
    // check validity, and set a custom message if invalid.
    function checkValidity () {
      const message = input.validity.valid
        ? null
        : getCustomMessage(input.type, input.validity)
      input.setCustomValidity("")
      if (message!=null){
        input.classList.add(invalidClassName)
      }else{
        input.classList.remove(invalidClassName)
      }
      
    }
    input.addEventListener('input', checkValidity)
    input.addEventListener('invalid', checkValidity)

  })

  // Remove error messages when modal is closed
  $('#modal-create').on('hidden.bs.modal', function () {
    inputs.forEach(function (input) {
      input.classList.remove(invalidClassName)
    })
  })

});  

$('#modal-create').on('show.bs.modal', function (e) {
  $('#buttonFibra').click();
  getSelects();
})

$('#modal-create_utp').on('show.bs.modal', function (e) {
  $('#buttonUTP').click();
  getSelectsUTP();
})

$('#modal-create-type').on('show.bs.modal', function (e) {
  $('#buttonFibra').click();
})

$('#modal-create-type_utp').on('show.bs.modal', function (e) {
  $('#buttonUTP').click();
})

function getSelects(action='create') {
  var url = DjangoURL('api_type_plan_list');  
  if (action == 'create'){
    $("#type_plan").empty();
    $("#operator").empty();
    $("#speeds_system_download").empty();
    $("#speeds_system_upload").empty();
  } else {
    $("#type_plan_edit").empty();
    $("#operator_edit").empty();
    $("#speeds_system_download_edit").empty();
    $("#speeds_system_upload_edit").empty();
  }
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        if (action == 'create'){
          $("#type_plan").append(`<option value="${res.data[i].id}">${res.data[i].name}</option>`);
        }else{
          $("#type_plan_edit").append(`<option value="${res.data[i].id}">${res.data[i].name}</option>`);
        }
      }
      getSelectPlans(action);
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
  var url = DjangoURL('api_operator_list');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        if (action == 'create'){
          $("#operator").append(`<option value="${res.data[i].id}">${res.data[i].name}</option>`);
        }else{
          $("#operator_edit").append(`<option value="${res.data[i].id}">${res.data[i].name}</option>`);
        }
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
  var url = DjangoURL('api_system_speeds');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        if (action == 'create'){
          $("#speeds_system_download").append(
            `<option value="${res.data[i].id}">${res.data[i].tidsis} ${res.data[i].name}</option>`);
          $("#speeds_system_upload").append(
            `<option value="${res.data[i].id}">${res.data[i].tidsis} ${res.data[i].name}</option>`);          
        }
        else{
          $("#speeds_system_download_edit").append(
            `<option value="${res.data[i].id}">${res.data[i].tidsis} ${res.data[i].name}</option>`);
          $("#speeds_system_upload_edit").append(
            `<option value="${res.data[i].id}">${res.data[i].tidsis} ${res.data[i].name}</option>`);
        }
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

function getSelectsUTP(action='create') {
  var url = DjangoURL('api_utp_typeplan');
  if (action == 'create'){
    $("#utp_type_plan").empty();
    $("#utp_operator").empty();
    $("#utp_profile").empty();
  } else {
    $("#utp_type_plan_edit").empty();
    $("#utp_operator_edit").empty();
    $("#utp_profile_edit").empty();
  }
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        if (action == 'create'){
          $("#utp_type_plan").append(`<option value="${res.data[i].id}">${res.data[i].name}</option>`);
        }else{
          $("#utp_type_plan_edit").append(`<option value="${res.data[i].id}">${res.data[i].name}</option>`);
        }
      }
      getSelectPlansUTP(action);
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
  var url = DjangoURL('api_utp_operator');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        if (action == 'create'){
          $("#utp_operator").append(`<option value="${res.data[i].id}">${res.data[i].name}</option>`);
        }else{
          $("#utp_operator_edit").append(`<option value="${res.data[i].id}">${res.data[i].name}</option>`);
        }
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
  var url = DjangoURL('api_utp_system_profile');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        if (action == 'create'){
          $("#utp_profile").append(
            `<option value="${res.data[i].id}">${res.data[i].name}</option>`);         
        }
        else{
          $("#utp_profile_edit").append(
            `<option value="${res.data[i].id}">${res.data[i].name}</option>`);
        }

      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

function getSelectPlans(action){
  if (action == 'create'){
    $("#matrix_plan").empty();
  } else {
    $("#matrix_plan_edit").empty();
  }
  var url = DjangoURL('api_matrix_plan_list');
  axios.get(url,conf)
  .then(function(res){
    if (res.status == 200)
    {
      var result = res.data.results;
      for (var i = 0; i < result.length; i++) {
        if (action=='create'){
          $("#matrix_plan").append(`<option value="${result[i].id}">${result[i].name}</option>`);
        }else{
          $("#matrix_plan_edit").append(`<option value="${result[i].id}">${result[i].name}</option>`);
        }
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      _error('No se pudo conectar a matrix para obtener los planes.')
  });

}



function getSelectPlansUTP(action){
  if (action == 'create'){
    $("#utp_matrix_plan").empty();
  } else {
    $("#utp_matrix_plan_edit").empty();
  }
  var url = DjangoURL('api_matrix_plan_list');
  axios.get(url,conf)
  .then(function(res){
    if (res.status == 200)
    {
      var result = res.data.results;
      for (var i = 0; i < result.length; i++) {
        if (action=='create'){
          $("#utp_matrix_plan").append(`<option value="${result[i].id}">${result[i].name}</option>`);
        }else{
          $("#utp_matrix_plan_edit").append(`<option value="${result[i].id}">${result[i].name}</option>`);
        }
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      _error('No se pudo conectar a matrix para obtener los planes.')
  });

}

newTypePlan = () => {
  if ($("#name_type_plan").val() == ''){
    swal({
      title: "Error",
        text: "Por favor verifique los datos ingresados",
        icon: "error",
        buttons: false
      });
    var input = document.querySelector('#name_type_plan');
    input.classList.add('invalid');
    input.setCustomValidity("Campo requerido");
    return false;
  } 
  var url = DjangoURL('api_type_plan_list')
  var data = { 
    "name": $("#name_type_plan").val(),
  }
  $("#loader_type_plan").html('<span class="spinner"></span>');
  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 201)
    {
      $("#loader_type_plan").html('');
      $('#modal-create-type').modal('toggle');
      var input = document.querySelector('#name_type_plan');
      input.classList.remove('invalid');
      input.setCustomValidity("");
      swal({
        title: "Creado",
        text: 'Tipo plan creado exitosamente.',
        icon: "success",
        buttons: false
      });
    }
  
  })//end then
  .catch(function(error) {
    $("#loader_type_plan").html('');
    handleErrorAxios(error);
  });
}

newTypePlanUTP = () => {
  if ($("#utp_name_type_plan").val() == ''){
    swal({
      title: "Error",
        text: "Por favor verifique los datos ingresados",
        icon: "error",
        buttons: false
      });
    var input = document.querySelector('#utp_name_type_plan');
    input.classList.add('invalid');
    input.setCustomValidity("Campo requerido");
    return false;
  } 
  var url = DjangoURL('api_utp_typeplan');
  var data = { 
    "name": $("#utp_name_type_plan").val(),
  }
  $("#loader_type_plan").html('<span class="spinner"></span>');
  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 201)
    {
      $("#loader_type_plan").html('');
      $('#modal-create-type_utp').modal('toggle');
      var input = document.querySelector('#utp_name_type_plan');
      input.classList.remove('invalid');
      input.setCustomValidity("");
      swal({
        title: "Creado",
        text: 'Tipo plan UTP creado exitosamente.',
        icon: "success",
        buttons: false
      });
    }
  
  })//end then
  .catch(function(error) {
    $("#loader_type_plan").html('');
    handleErrorAxios(error);
  });
}


function newPlan() {

  if ($("#name").val() == ''){
    swal({
      title: "Error",
        text: "Por favor verifique los datos ingresados",
        icon: "error",
        buttons: false
      });
    var input = document.querySelector('#name');
    input.classList.add('invalid');
    input.setCustomValidity("Campo requerido");
    return false;
  } 
  ar_value = AggregationRateMask.el.value.replace('1:','');

  if (ar_value != ''){
    if (ar_value < 1 || ar_value > 100){
      _error("La tasa de agregación debe ser mayor a 1 y menor que 100");
      return false;
    }    
  }

  us_value = UploadSpeedMask.el.value.replace(' Mbps','')
  ds_value = DownloadSpeedMask.el.value.replace(' Mbps','')

  $("#loader_plan").html('<span class="spinner"></span>');
  var url = DjangoURL('api_plan_list')
  var data = { 
    "name": $("#name").val(),
    "type_plan": $("#type_plan option:selected").val(),
    "operator": $("#operator option:selected").val(),
    "matrix_plan": $("#matrix_plan option:selected").val(),
    "speeds_system_download": $("#speeds_system_download option:selected").val(),
    "speeds_system_upload": $("#speeds_system_upload option:selected").val(),
  }

  if (ar_value != ''){
    mergeObject(data,{"aggregation_rate":ar_value})
  }
  if (ar_value != ''){
    mergeObject(data,{"download_speed":ds_value})
  }
  if (ar_value != ''){
    mergeObject(data,{"upload_speed":us_value})
  }

  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 201)
    {
      $("#loader_plan").html('');
      $('#modal-create').modal('toggle');
      swal({
        title: "Creado",
        text: 'Plan creado exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        var input = document.querySelector('#name');
        input.classList.remove('invalid');
        input.setCustomValidity("");
        updateDatatable();
      });
    }
  
  })//end then
  .catch(function(error) {
    $("#loader_plan").html('');
    handleErrorAxios(error);
  });
  
};

function newPlanUTP() {

  if ($("#utp_name").val() == ''){
    swal({
      title: "Error",
        text: "Por favor verifique los datos ingresados",
        icon: "error",
        buttons: false
      });
    var input = document.querySelector('#utp_name');
    input.classList.add('invalid');
    input.setCustomValidity("Campo requerido");
    return false;
  } 

  ar_value = AggregationRateMaskUTP.el.value.replace('1:','').replace("_","");

  if (ar_value != ''){
    if (ar_value < 1 || ar_value > 100){
      _error("La tasa de agregación debe ser mayor a 1 y menor que 100");
      return false;
    }    
  }

  $("#loader_plan").html('<span class="spinner"></span>');
  var url = DjangoURL('api_utp_plan')
  var data = { 
    "name": $("#utp_name").val(),
    "type_plan": $("#utp_type_plan option:selected").val(),
    "operator": $("#utp_operator option:selected").val(),
    "profile_name": $("#utp_profile option:selected").val(),
    "matrix_plan": 1,
  }

  if (ar_value != ''){
    mergeObject(data,{"aggregation_rate":ar_value})
  }

  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 201)
    {
      $("#loader_plan").html('');
      $('#modal-create_utp').modal('toggle');
      swal({
        title: "Creado",
        text: 'Plan UTP creado exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        var input = document.querySelector('#utp_name');
        input.classList.add('invalid');
        input.setCustomValidity("Campo requerido");
        updateDatatable();
      });
    }
  
  })//end then
  .catch(function(error) {
    $("#loader_plan").html('');
    handleErrorAxios(error);
  });
  
};


ayuda = () => {
  alert('Pagina de ayuda de plan')
}


editPlan = (id) => {
  $('#modal-edit').modal('toggle');
  getSelects('edit');
  setTimeout(function() {

      $('#type_plan_edit').val('').change();
      $('#speeds_system_download_edit').val('').change();
      $('#speeds_system_upload_edit').val('').change();
      $('#operator_edit').val('').change();
      $('#matrix_plan_edit').val('').change();

      $("#download_speed_edit").val('');
      $("#upload_speed_edit").val('');
      $("#aggregation_rate_edit").val('');
      $("#name_edit").val('');
      HSD.unset('selected_plan_id');


      var url = DjangoURL('api_plan_detail', id);
      var conf = {
          headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
      };
      axios.get(url, conf)
      .then(function(res){
        if (res.status == 200)
        {

          $('#type_plan_edit').val(res.data.type_plan).change();
          $('#speeds_system_download_edit').val(res.data.speeds_system_download).change();
          $('#speeds_system_upload_edit').val(res.data.speeds_system_upload).change();
          $('#operator_edit').val(res.data.operator).change();
          $('#matrix_plan_edit').val(res.data.matrix_plan).change();

          $("#download_speed_edit").val(res.data.download_speed);
          $("#upload_speed_edit").val(res.data.upload_speed);
          if (res.data.aggregation_rate){
            $("#aggregation_rate_edit").val('1:' + res.data.aggregation_rate);
          }
          $("#name_edit").val(res.data.name);
          HSD.set('selected_plan_id', id);
        }
        if (res.status == 401)
        {
          handleUnauthorizedAxios();
        }
      })//end then
      .catch(function(error) {
          handleErrorAxios(error);
      });  
  
  }, 550);

}

editPlanPerform = () => {

  ar_value = AggregationRateMaskEdit.el.value.replace('1:','');

  if (ar_value != ''){
    if (ar_value < 1 || ar_value > 100){
      _error("La tasa de agregación debe ser mayor a 1 y menor que 100");
      return false;
    }    
  }

  us_value = UploadSpeedMaskEdit.el.value.replace(' Mbps','')
  ds_value = DownloadSpeedMaskEdit.el.value.replace(' Mbps','')

  $("#loader_plan").html('<span class="spinner"></span>');
  var url = DjangoURL('api_plan_detail', HSD.get('selected_plan_id'));
  var data = { 
    "name": $("#name_edit").val(),
    "type_plan": $("#type_plan_edit option:selected").val(),
    "operator": $("#operator_edit option:selected").val(),
    "matrix_plan": $("#matrix_plan_edit option:selected").val(),
    "speeds_system_download": $("#speeds_system_download_edit option:selected").val(),
    "speeds_system_upload": $("#speeds_system_upload_edit option:selected").val(),
  }

  if (ar_value != ''){
    mergeObject(data,{"aggregation_rate":ar_value})
  }
  if (ar_value != ''){
    mergeObject(data,{"download_speed":ds_value})
  }
  if (ar_value != ''){
    mergeObject(data,{"upload_speed":us_value})
  }

  var conf = {
      headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };

  axios.patch(url, data, conf)
  .then(function(res){
    if (res.status == 200)
    {
      $("#loader_plan").html('');
      $('#modal-edit').modal('toggle');
      swal({
        title: "Creado",
        text: 'Plan actualizado exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        updateDatatable();
      });
    }
  
  })//end then
  .catch(function(error) {
    $("#loader_plan").html('');
    handleErrorAxios(error);
  });


}

$(".btn-group > .btn").click(function(){
    $(".btn-group > .btn").removeClass("active");
    $(this).addClass("active");
    hideElemShowElem("div_table_plan","div_table_utp_plan");
    hideElemShowElem("buttons_plan","buttons_utp_plan")
});

function hideElemShowElem (strelem1,strelem2) {
  var elem1 = document.getElementById(strelem1);
  var elem2 = document.getElementById(strelem2);
  if (elem1.style.display === "none") {
    elem1.style.display = "block";
    elem2.style.display = "none";
  } else {
    elem2.style.display = "block";
    elem1.style.display = "none";
  }
}