userInSession();
App.setPageTitle('Sentinel - Reportes de status de Servicio');

$("#menu_reports").addClass('active');
$("#menu_report_nodo").addClass('active');

var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};

$(document).ready(function (){


  var selector = new SPA();
  selector.Select2AjaxWithoutModal('/api/v1/node-ac/',"mikrotik",'Nodos',false);
  selector.Select2AjaxWithoutModal('/api/v1/status-matrix-ac/',"status",'Status',false);


  url_dt = '/datatables/status-node-change-list';
  var groupColumn = 0;

  oTable = $('#table_status_node_change').dataTable({
    ajax:{
      url:url_dt,
      method:'post',
      type:'json',
      headers:{ Authorization: HDD.get(Django.name_jwt)},
      data:function(d){
        d.filter_fecha = $("#advance-daterange").val();
        d.filter_node = $("#mikrotik").val();
        d.filter_status = $("#status").val();
      }

    },
    order:[[0,'asc']],
    responsive:true,
    destroy:true,
    serverSide:true,
    processing: true,
    language:dt_language('status'),

    columnDefs:[
      {targets:[0],visible:false,},
      { targets:[2,3],
        className:'text-center',
        render: function (data, type, row) {
              return '<b>'+ data +'</b>'
        }
      },
      { targets:[1],
        render: function (data, type, row) {
              return '<b>'+ data.toUpperCase() +'</b>'
      },}


    ],
    drawCallback: function ( settings ) {
          var api = this.api();
          var rows = api.rows( {page:'current'} ).nodes();
          var last=null;

          api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
              if ( last !== group ) {
                  $(rows).eq( i ).before(
                      '<tr class="group"><td colspan="4"><strong>Nodo: '+group+'</strong></td></tr>'
                  );

                  last = group;
              }
          } );

      },

    footerCallback:function ( row, data, start, end, display ) {
      var api = this.api(), data;

      // Remove the formatting to get integer data for summation
      var intVal = function ( i ) {
          return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
      };

       total = api
          .column( 2 )
          .data()
          .reduce( function (a, b) {
              return intVal(a) + intVal(b);
          }, 0 );

      // Update footer
      $( api.column( 2 ).footer() ).html(
          '<b>Total: ' + total + '</b>'
      );

  }


  })


       // Order by the grouping
  $('#table_status_node_change tbody').on( 'click', 'tr.group', function () {
      var currentOrder = oTable.aaSorting()[0];
      if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
          oTable.order( [ groupColumn, 'desc' ] ).draw();
      }
      else {
          oTable.order( [ groupColumn, 'asc' ] ).draw();
      }
  } );

  function cb(start, end) {
    if(start._isValid && end._isValid)
    {
        $('#advance-daterange input').val(start.format('DD MMM YYYY') + ' - ' + end.format('DD MMM YYYY'));
    }
    else
    {
        $('#advance-daterange input').val('');
    }
  }


$.when(
  $('#advance-daterange').daterangepicker(jsonDateRange, function(start, end, label) {
      $('#advance-daterange input').attr({placeholder:start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY')});
  },cb),

  $('#advance-daterange').on('apply.daterangepicker', function(ev, picker) {
    var initiald = picker.startDate.format('YYYY-MM-DD');
    var finald = picker.endDate.format('YYYY-MM-DD');

    if(picker.startDate._isValid && picker.endDate._isValid){
      $('#advance-daterange').val(initiald + ',' + finald)
      oTable.api().ajax.reload(null, false);
    }


    }),

)



$('#mikrotik').change(function(){
  oTable.api().ajax.reload(null, false);
})

$('#status').change(function(){
  oTable.api().ajax.reload(null, false);
})


$("#btnResetStatus").on('click',function(){

  $("#status").val('').trigger('change');
  oTable.api().ajax.reload(null, false);
  
})
$("#btnResetNode").on('click',function(){

  $("#mikrotik").val('').trigger('change');
  oTable.api().ajax.reload(null, false);
  
})


}); 


