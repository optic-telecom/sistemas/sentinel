userInSession();
App.setPageTitle('Sentinel - Reportes de status de Servicio');

$("#menu_reports").addClass('active');
$("#menu_report_status_servicio").addClass('active');

var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};

$(document).ready(function(){

    url_dt = '/datatables/status-service-list';

    oTable = $('#table_status_service').dataTable({
        ajax:{
          url:url_dt,
          method:'post',
          type:'json',
          headers:{ Authorization: HDD.get(Django.name_jwt)},
    
        },
        columnDefs:[{
          searchable:false,
          orderable:false,
          targets:[2,3]
        }],
        order:[[0,'asc']],
        responsive:true,
        destroy:true,
        serverSide:true,
        processing: true,
        language:dt_language('servicios'),

    
      })



})