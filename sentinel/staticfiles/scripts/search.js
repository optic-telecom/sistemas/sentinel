userInSession();
App.setPageTitle('Sentinel - Busqueda');

var resultSearchTemplate = templater`<div class="col-lg-3 col-md-4 col-md-4">
  <div class="widget widget-stats bg-gradient-${ 'color' } m-b-10">
    <div class="stats-icon stats-icon-lg"><i class="fa fa-comment-alt fa-fw"></i></div>
    <div class="stats-content">
      <div class="stats-title">${ 'title' } ip: ${ 'ip' }</div>
      <div class="stats-number">${ 'desc' }</div>
      <div class="stats-desc">uptime ${ 'uptime' }</div>
    </div>
  </div>
</div>`;


$(document).ready(function (){

  $("#content_search").html('<center><img src="/static/img/loading_spinner.gif" width="100px" heigh="100px"></img></center>');
  const q = getParameterUrl('q');
  const ip = getParameterUrl('ip');
  let url = DjangoURL('api_search');

  if (ip){
      url = `${url}?ip=${ip}`;    
  }else{
    if (q){
      url = `${url}?q=${q}`;   
    }
  }

  axios.get(url, header_common)
  .then(function(res){
    
    $("#number_result").text(res.data.length);
    $("#content_search").html('');
    if (res.data.length > 0){
        //recorro los resultados
        res.data.forEach( (result) =>{
          if (result.type == 'ONU'){

            result = extend(
              {'desc':`<a data-click="panel-lateral" data-id="${result.uuid}" data-panel="panel_onu_content">${result.serial}</a>`},
              result
            )

          }

          if (result.type == 'CPE'){

            result = extend(
              {'desc':`<a data-click="panel-lateral" data-id="${result.uuid}" data-panel="panel_utpcpe_content">${result.serial}</a>`},
              result
            )
            
          }


          $("#content_search").append(resultSearchTemplate(result));
        });
    }else{
        $("#content_search").html('No se encontraron resultados');
    }

   
  })//end then
  .catch(function(error) {
      $("#content_search").html('');
      handleErrorAxios(error);
  });

}); 