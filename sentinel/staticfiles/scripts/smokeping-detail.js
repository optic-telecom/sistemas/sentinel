userInSession();
App.setPageTitle('Sentinel - Listado de IPs' );

$("#menu_configuraciones").addClass('active');
$("#menu_iptable").addClass('active');

var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};

$(document).ready(function (){
  id = getUrlLastParameter()
  url_dt = `/ipt-settings-detail/${id}`;
  
  language =(palabraClave='registros')=>{
      return {
        sLengthMenu:     `Mostrar _MENU_ ${palabraClave}`,
        sZeroRecords:    `No se encontraron resultados`,
        sEmptyTable:     `Ningún dato disponible en esta tabla`,
        sInfo:           `Mostrando ${palabraClave} del _START_ al _END_ de un total de _TOTAL_ ${palabraClave}`,
        sInfoEmpty:      `Mostrando ${palabraClave} del 0 al 0 de un total de 0 ${palabraClave}`,
        sInfoFiltered:   `(filtrado de un total de _MAX_ ${palabraClave})`,
        sInfoPostFix:    "",
        sSearch:         `Buscar:`,
        sUrl:            "",
        sInfoThousands:  ",",
        sLoadingRecords: `Cargando...`,
        sProcessing: `Cargando...`,
        oPaginate: {
          sFirst:    "Primero",
          sLast:     "Último",
          sNext:     "Siguiente",
          sPrevious: "Anterior"
        },

        oAria: {
          sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
          sSortDescending: ": Activar para ordenar la columna de manera descendente"
        }
      };
  } 
  
  oTable = $('#table_ips').dataTable({
        "aaSorting":[[0,"asc"]],
        "language": language('IPs'),
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": url_dt,
            "type": "POST",
            "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
        },

        "columnDefs": [
          {
              orderable: true,
              searchable: true,
              targets: [0]
          },
          {
              orderable: true,
              searchable: true,
              className:'text-center',
              targets: [1,2,3]
          },
          {
              orderable: false,
              searchable: false,
              className:'text-center',
              targets: [4]
          }
          ],


   });

}); 


modalDetail = (obj) =>{

  var ip = new SPA('Detalle','info');
  var content = ip.RowTable('IP',obj.ip) + ip.RowTable('Host',obj.host)
  content += ip.RowTable('Menú',obj.menu) + ip.RowTable('Title',obj.title)

  var html = ip.TableBody('IP '+obj.ip, content);
  
  $("#modal_create")
  .empty()
  .append(html)
  .modal({show:true})

}


loadModalEdit = (obj) =>{

  var ip = new SPA('Editar la IP '+obj.ip,'warning')
  
  var content = ip.Input('IP','ip','text',obj.ip) + ip.Input('Host','host','text',obj.host) 
  content += ip.Input('Menú','menu','text',obj.menu) + ip.Input('Titulo','title','text',obj.title);
  var footer = ip.BtnAction()
  footer += ip.BtnAction('Editar','function','updateIP',obj.id)

  var html = ip.modalBody(content,footer); 

  $("#modal_create").empty()
  .append(html)
  .modal({show:true});

}

updateIP =(id)=>{
  var data = {
    ip:$("#ip").val(),
    host:$("#host").val(),
    menu:$("#menu").val(),
    title:$("#title").val(),
  }
  var url = `/api/v1/iptables/${id}/`;
  axios.put(url,data,conf).then((response)=>{
    if(response.status == 200)
    {
      _exito('Se ha Editado la IP correctamente');
      $("#modal_create").modal('toggle')
      setTimeout(updateDatatable(),40)
    }
  })
  .catch((error)=>{
    handleErrorAxios(error)
  })

}

deleteIP = (id) => {
  swal({
    title: `¿Eliminar la IP?`,
    text: "¿Realmente esta seguro?, Recuerde que esta acción no se puede deshacer",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      url = `/api/v1/iptables/${id}/`
      axios.delete(url,conf).then((response)=>{
        if(response.status == 204)
        {
          _exito('La IP ha sido eliminada satisfactoriamente')
          setTimeout(updateDatatable(),40)
        }
      })
      .catch((error)=>{
        handleErrorAxios(error)
      })

    } else {
      _info("Su IP está a salvo.");
    }
  });
}