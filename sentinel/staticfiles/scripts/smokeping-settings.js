userInSession();
App.setPageTitle('Sentinel - Menú Smokeping');


$("#menu_configuraciones").addClass('active');
$("#menu_smokeping").addClass('active');

var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};

$(document).ready(function (){
  url_dt = '/ms-settings-list';
  

  
  oTable = $('#table_menusmokeping').dataTable({
        "aaSorting":[[2,"asc"]],
        "language": dt_language('Menús'),
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": url_dt,
            "type": "POST",
            "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
        },

        "columnDefs":[
        {
            orderable: true,
            searchable: true,
            targets: [0,1]
        },
        {
            orderable: true,
            searchable: true,
            className:'text-center',
            targets: [2]
        },
        {
            orderable: false,
            searchable: false,
            className:'text-center',
            targets: [3,4]
        }
        ],


   });
}); 



var url_menus_list = '/api/v1/smokeping-menus/';



loadModal = function(){

    var menu = new SPA('Generar un nuevo Menú','success');
    var content = menu.Input('Nombre del Menú','title','text',)
    content += menu.Input('Orden de aparición','order_field','number')
    content += menu.Select('IPs','iptable')

    var footer = menu.BtnAction()
    footer += menu.BtnAction('Crear','function','newMenu',0)

    var html = menu.modalBody(content,footer);

    $(menu.modalId)
    .empty()
    .append(html)
    .modal({show:true})
    .removeClass('modal-message');

    menu.Select2Ajax('/api/v1/iptable-ac/','iptable',"IP's",true)
}

newMenu = function(id=0)
{
    var data = {
        title:$("#title").val(),
        order_field:$("#order_field").val(),
        iptable:$("#iptable").val(),
    }

    validator = [];
    if(ValidatorField(data,'title','menú')){
        validator.push(true);
    };
    if(ValidatorField(data,'order_field','número de aparición')){
        validator.push(true);
    };
    if(ValidatorField(data,'iptable','listado de ip')){
        validator.push(true);
    };

    if(validator.length==0){

        axios.post(url_menus_list,data,conf).then((response)=>{
        if(response.status ==201)
        {
            _exito('El Menú se ha registrado satisfactoriamente')
            setTimeout(updateDatatable(),40)
            $("#modal_create").modal('toggle')
        }
        })
        .catch((error)=>{
        handleErrorAxios(error)
        })

    }
  
}

loadModalEdit = function(obj){

    var menu = new SPA('Editar el Menú '+obj.title,'warning');
    var content = menu.Input('Nombre del Menú','title','text',obj.title)
    content += menu.Input('Orden de aparición','order_field','number',obj.order_field)
    content += menu.Select('IPs','iptable')

    var footer = menu.BtnAction()
    footer += menu.BtnAction('Crear','function','editMenu',obj.id)

    var html = menu.modalBody(content,footer);

    $(menu.modalId)
    .empty()
    .append(html)
    .modal({show:true})
    .removeClass('modal-message');

    menu.Select2Ajax('/api/v1/iptable-ac/','iptable',"IP's",true,true,obj.iptable)
}

editMenu = function(id)
{
    var data = {
        title:$("#title").val(),
        order_field:$("#order_field").val(),
        iptable:$("#iptable").val(),
    }

    validator = [];
    if(ValidatorField(data,'title','menú')){
        validator.push(true);
    };
    if(ValidatorField(data,'order_field','número de aparición')){
        validator.push(true);
    };
    if(ValidatorField(data,'iptable','listado de ip')){
        validator.push(true);
    };

    if(validator.length==0){
        url_menus_detail = url_menus_list + id + '/'
        axios.put(url_menus_detail,data,conf).then((response)=>{
        if(response.status ==200)
        {
            _exito('El Menú se ha editado satisfactoriamente')
            setTimeout(updateDatatable(),40)
            $("#modal_create").modal('toggle')
        }
        })
        .catch((error)=>{
        handleErrorAxios(error)
        })

    }
  
}

deleteMenu = function(json){
    var id = json.id;
    var menu_name = json.title;
    swal({
        title: `¿Eliminar el menú ${menu_name}?`,
        text: "¿Realmente esta seguro?, Recuerde que esta acción no se puede deshacer",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
        url = `/api/v1/smokeping-menus/${id}/`
        axios.delete(url,conf).then((response)=>{
            if(response.status == 204)
            {
            _exito('Su menú ha sido eliminado satisfactoriamente')
            setTimeout(updateDatatable(),40)
            }
        })
        .catch((error)=>{
            handleErrorAxios(error)
        })

        } else {
        _info("Su menú está a salvo.");
        }
    });
    
}

detailMenu = (obj)=> {

    var content = (`<div class="row">
                        <div class="col-md-6 offset-md-3">
                            <div class="row">
                                <div class="col-md-6"><h5>Nombre del Menú: <span class="small">${obj.title}</span></h5></div>
                                <div class="col-md-6"><h5>Orden de Aparición: <span class="small">${obj.order_field}</span></h5></div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h4 class="text-center">IP's asociadas</h4>
                    <table id="iptable" class="table table-stripped table-border" style="width: 100%;">
                        <thead>
                            <tr><th>IP</th><th>Host</th><th>Menú Smokeping</th><th>Title</th></tr>
                        </thead>
                        <tbody></tbody>
                    </table>
            `)
    
    var menu = new SPA('Detalle del menú '+ obj.title,'light')
    // var footer = menu.BtnAction()
    var html = menu.modalBody(content)

    $(menu.modalId)
    .empty()
    .append(html)
    .modal({show:true})
    .addClass('modal-message')

    $("#iptable").DataTable({
        data:obj.iptable,
        "language": dt_language('IPs'),
        columns:[
            {data:'ip'},
            {data:'host'},
            {data:'menu'},
            {data:'title'},
        ],
        columnDefs:[{
            orderable: true,
            searchable: true,
            className:'text-center',
            targets: [0,1,2,3]
        },]
    })
}

// PUBLISH ON SERVER SMOKEPING
publisherTarget = () => {
  var $conf = {"headers": { 'Authorization':  HDD.get(Django.name_jwt) },}
  axios.get('/api/v1/publisher-target-file/', $conf).then((response)=>{
    if(response.status == 200)
    {
      _info('El menú ha sido publicado en el servidor')
    }
  })
  .catch((error)=>{
    handleErrorAxios(error)
  })

}

// DOWNLOAD SMOKEPING FILE
downloadTargetFile = () => {
  var $conf = {"headers": { 'Authorization':  HDD.get(Django.name_jwt) },}
  let url = DjangoURL('api_smokeping') + '/create_smokeping_file/'
  // Activamos el modal
  $('#modal-sleep').modal('show')
  
  // Generamos el archivo en el servidor
  axios.get(url, $conf).then((response)=>{
    if(response.status == 200)
    {
      $('#modal-sleep').modal('hide')
      // Notificación de éxito
      swal({
      title: "Creado",
        text: 'Archivo Target creado con éxito.',
        icon: "success",
        buttons: false
      }).then(function(){
        // Abrimos el enlace con el archivo generado para la descarga
        window.open('/api/v1/download-target-file/', '_blank')
      })
      
    }
  })
  .catch((error)=>{
    handleErrorAxios(error)
  })

}
