userInSession();
App.setPageTitle('Sentinel - Lista de velocidades');
var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};

$("#menu_configuraciones").addClass('active');
$("#menu_velocidades").addClass('active');

$(document).ready(function (){
  url_dt = Django.datatable_speeds;
  oTable = $('#table_speeds').dataTable({
        "aaSorting":[[0,"desc"]],
        "language": {
        "sLengthMenu":     "Mostrar _MENU_ System speed",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando System speed del _START_ al _END_ de un total de _TOTAL_ System speed",
        "sInfoEmpty":      "Mostrando System speed del 0 al 0 de un total de 0 System speed",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ System speed)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "sProcessing": "Cargando...",
        "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
        },

        "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      },
      "processing": true,
      "serverSide": true,
      "ajax": {
          "url": url_dt,
          "type": "POST",
          "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
      },
      "columnDefs": [
      {
        orderable: true,
        searchable: true,
        targets: [0,1,2,3,4,5]
      },
      {
        orderable: false,
        searchable: false,
        targets: [6]
      }
      ], 
   });
});  

function newSpeed() {
  var url = DjangoURL('api_system_speeds');

  if ($("#cir").val() == '' ||
      $("#cbs").val() == '' ||
      $("#pir").val() == '' ||
      $("#pbs").val() == '' ||
      $("#pri").val() == '' ||
      $("#name").val() == '' ||
      $("#tidsis").val() == ''){

      swal({
      title: "Error",
        text: 'Verifique los datos ingresados',
        icon: "error",
        buttons: false
      })
      return false;
  }

  $("#loader_modal").html('<span class="spinner"></span>');
  var data = { 
    "cir": $("#cir").val(),
    "cbs": $("#cbs").val(),
    "pir": $("#pir").val(),
    "pbs": $("#pbs").val(),
    "pri": $("#pri").val(),
    "name": $("#name").val(),
    "tidsis": $("#tidsis").val(),
  }
  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 201)
    {
      $("#loader_modal").html('');
      $('#modal-create').modal('toggle');
      swal({
      title: "Creada",
        text: 'Velocidad creada exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        updateDatatable();
      });
    }
  
  })//end then
  .catch(function(error) {
    $("#loader_modal").html('');
    handleErrorAxios(error);
  });
};