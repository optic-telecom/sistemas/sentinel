window.customElements.define('button-close', class ButtonCloseModal extends HTMLElement {
    connectedCallback () {
        this.innerHTML = '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'
    }
});


window.customElements.define('img-alert', class ImgAlert extends HTMLElement {
  constructor() {
    super()
    this.attachShadow({ mode: 'open' })
    this.shadowRoot.innerHTML = `<img src="/static/img/alerta.png" width="45px"/>`
  }
});


const OnuTemplate = templater`
<div class="fade-in divParent ${'class_parent'}">
  <b class="text-white f-s-14 f-w-700">${'title'}</b>
  <div class="${'class_child'}">

    <table style="width: 90%; margin-left: auto; margin-right: auto;" cellpadding="2" class="f-s-13" border="0">
      <tbody>
      <tr>
	      <td style="width: 11%;" class="f-w-700">Numero de serie:</td>
	      <td style="width: 11%;" class="f-w-700">${'serial'}</td>
	      <td style="width: 13%;" class="f-w-700">OLT:</td>
	      <td style="width: 18%;" class="f-w-700">${'olt'}</td>
	      <td style="width: 22%;">&nbsp;</td>
	      <td rowspan="3">
	      <button class="btn btn-${'color_btn'}" onclick="provisioningOLT('${'uuid'}')" type="button">Asociar a Servicio</button>
        </td>
      </tr>
      <tr>
	      <td class="f-w-700">Modelo</td>
	      <td class="f-w-700">${'model'}</td>
	      <td>F/S/P:</td>
	      <td>${'fsp'}</td>
      </tr>
      <tr>
	      <td class="f-w-700">${'service_number_text'}</td>
	      <td class="f-w-700">${'service_number'}</td>
	      <td>Descripción del puerto:</td>
	      <td>${'description'}</td>
      </tr>
      </tbody>
    </table>
  </div>
</div>`;



const ServiceNumberTemplate = templater`
  <hr style="background: #e2e7eb; width: 100%">
  <div class="divParent successParent" style="margin-right: 6px; margin-left: 8px; width:100%;">
    <b class="text-white f-s-14 f-w-700">&nbsp;Número de servicio</b>
    <div class="successChild">

      <table style="width: 90%; margin-left: auto; margin-right: auto;" cellpadding="2" class="f-s-13" border="0">
      <tbody>

      <tr>
        <td>Rut cliente</td>
        <td>${'rut'}</td>
      </tr>

      <tr>
        <td>Dirección de servicio</td>
        <td>${'composite_address'}</td>
      </tr>

      <tr>
        <td>Comuna</td>
        <td>${'commune'}</td> 
      </tr>

      <tr><td>&nbsp;</td>
      </tr>

      <tr>
        <td class="f-w-700">Datos del servicio</td>
      </tr>

      <tr>
        <td>Plan contratado</td>
        <td>${'plan_field'}</td>
      </tr>

      ${'description_field'}

      ${'tids_fields'}

      <tr>
        <td>Nodo instalación</td>
        <td>${'code'}</td>
      </tr>

      <tr>
        <td>Estado actual servicio</td>
        <td>${'get_status_display'}</td>
      </tr>

      <tr>
        <td>Tenico en campo</td>
        <td>
          <select class="form-control" id="tecnico">
            <option>Seleccione</option>
            ${'tecnicos_fields'}
          </select>
        </td>
      </tr>

      <tr>
        <td>SSID</td>
        <td><input id="SSID" class="form-control col-md-8" maxlength="60"></input></td>
      </tr>

      <tr>
        <td>Clave WiFi</td>
        <td><input id="clave_wifi" class="form-control col-md-8" maxlength="20" minlenght="8"></input></td>
      </tr>

      <tr>
        <td>Ultimos 6 digitos SN</td>
        <td>
        <div class="row">
          <input id="ids_1" maxlength="1" style="padding: 2px 9px;" class="form-control col-md-1" onkeypress="return input_digit_serial(event, '2')"></input>
          <input id="ids_2" maxlength="1" style="padding: 2px 9px;" class="form-control col-md-1" onkeypress="return input_digit_serial(event, '3')"></input>
          <input id="ids_3" maxlength="1" style="padding: 2px 9px;" class="form-control col-md-1" onkeypress="return input_digit_serial(event, '4')"></input>
          <input id="ids_4" maxlength="1" style="padding: 2px 9px;" class="form-control col-md-1" onkeypress="return input_digit_serial(event, '5')"></input>
          <input id="ids_5" maxlength="1" style="padding: 2px 9px;" class="form-control col-md-1" onkeypress="return input_digit_serial(event, '6')"></input>
          <input id="ids_6" maxlength="1" style="padding: 2px 9px;" class="form-control col-md-1" onkeypress="return input_digit_serial(event, '0')"></input>
        </div>
        </td>
      </tr>

      </tbody>
      </table>

      <br>

      <table style="width: 90%; margin-left: auto; margin-right: auto;">
      <tbody>
      <tr>
        <td width="70%">
          ${'text_alert'}
        </td>
        <td>&nbsp;
        <button onclick="associate('${'service'}','${'uuid'}','${'previus'}','${'sentinel_plan'}','${'code'}',
        '${'apartment_number'}','${'tower'}','${'client'}','${'node_address'}','${'composite_address'}','${'service_status'}')" class="btn btn-green" type="button">Asociar a Servicio</button></td></td>
      </tr>
      </tbody>
      </table>
    </div>
  </div>`;

const ServiceNumberTemplateNoValid = templater`<div class="alertChild">${'text_alert'}</div>`;