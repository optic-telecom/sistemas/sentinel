const modal = templater`
<div class="modal-header">
  <h4 class="modal-title"><img-alert></img-alert><span style="color:#0476b4">
  &nbsp;Error al enviar u obtener datos.</span>
  </h4>
  <button-close />
  </div>

  <div class="modal-body">
    <div class="row">
      ${'case_message'}
    </div>
  </div>

</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" onclick="reanudar_modal()">Volver</button>
</div>`;

export default modal;