const modal = templater`
<div class="modal-header"><img-alert></img-alert>
	<h4 class="modal-title">
	<span style="color:#0476b4">&nbsp;ONU sin config file cargado</span></h4>
	<button-close />
	</div>

	<div class="modal-body">
		<div class="row">
			Por favor pídele al técnico que instale otra ONU de su camioneta. La que instaló recién está en blanco.
		</div>
	</div>

</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
</div>`;

export default modal;