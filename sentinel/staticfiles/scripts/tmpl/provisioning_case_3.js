const modal = templater`
<div class="modal-header">
	<h4 class="modal-title"><img-alert></img-alert><span style="color:#0476b4">
	&nbsp;Señal Optica muy baja</span>
	</h4>
	<button-close />
	</div>

	<div class="modal-body">
		<div class="row">
			No paso la prueba de ping y la señal es muy mala ${'signal'}. Debe pedirle al técnico que vuelva a comprobar las fusiones y conectores.
		</div>
	</div>

</div>

<div class="modal-footer">
<button type="button" class="btn btn-default" onclick="reintentar_señal('${'olt'}','${'onu_id'}', '${'fsp'}', '${'ip'}')">Volver a intentar</button>
	<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
</div>`;

export default modal;