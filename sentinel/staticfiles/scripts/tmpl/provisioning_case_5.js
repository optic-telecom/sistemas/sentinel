const modal = templater`
<div class="modal-header"><img src="/static/img/alerta.png" width="31px">
	<h4 class="modal-title"><span style="color:#0476b4">
	&nbsp;<strong>¡Oops!</strong> Hubo un problema y necesitamos tu ayuda</span>
	</h4>
	<button-close />
	</div>

	<div class="modal-body f-s-13">
		<div class="row" style>
      <ol class="modal_confirmacion">
      <li>Ingresa mediante Winbox al Mikrotik del Nodo ${'node'}

      <span class="mf-tooltip" data-content="Si no sabes cómo ingresar a la IP del nodo puede seguir esta
      <a target='_blak' href='https://nuevooptic.atlassian.net/wiki/spaces/ATI/pages/67862664/Primeros+pasos+con+Winbox'>guía</a>.">
      <i style="color:#0476b4 !important;" class="ion ion-md-help-circle  fa-fw text-black-lighter"></i>
      </span>

      </li>
      <li>

      Anda al menú IP, luego al Firewall y a la sección Address List
      <span class="mf-tooltip" data-content=<a target='_blak' href='/static/img/help/image2019-8-15_22-5-4.png'><img src='/static/img/help/image2019-8-15_22-5-4.png' width='310%'></a>">
      <i style="color:#0476b4 !important;" class="ion ion-md-help-circle  fa-fw text-black-lighter"></i>
      </span>

      </li>

      <li>
      Una vez ahi busca la IP de la ONU ${'ip'} <i onclick="copy_ip('${'ip'}')" class="far fa-lg fa-fw fa-copy"></i>
      <br>Si encuentra un registro con esa IP, por favor marca y eliminalo.
      <span class="mf-tooltip" data-content=<a target='_blak' href='/static/img/help/image2019-8-15_22-3-14.png'><img src='/static/img/help/image2019-8-15_22-3-14.png' width='310%'></a>">
      <i class="ion ion-md-help-circle  text-black-lighter"></i>
      </span>
      </li>

      <li>Ahora haz click en el botón Volver a Intentar</li>
      </ol>			
		</div>
	</div>

</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" onclick="reintentar_ip('${'olt'}')">Volver a intentar</button>
  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
</div>`;

export default modal;