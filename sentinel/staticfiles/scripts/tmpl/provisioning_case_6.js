const modal = templater`
<div class="modal-header">
  <h4 class="modal-title"><span style="color:#0476b4">
  &nbsp;<strong>¡Genial!</strong> ahora sólo queda configurar el equipo.</span>
  </h4>
  <button-close />
  </div>

  <div class="modal-body">
  	<div class="row">
  <ol class="modal_confirmacion">

  <li>Ingresa a la ONU [${'ip'}] <i onclick="copy_ip('${'ip'}')" class="far fa-lg fa-fw fa-copy"></i> con el usuario 
    ${'user_onus'} <i onclick="copy_ip('${'user_onus'}')" class="far fa-lg fa-fw fa-copy"></i> y la contraseña ${'pass_onus'}
    <i onclick="copy_ip('${'pass_onus'}')" class="far fa-lg fa-fw fa-copy"></i>
    
    <span class="mf-tooltip" data-content="Si no sabes como ingresar a la ONU puedes revisar <a target='_blak' href='https://nuevooptic.atlassian.net/wiki/spaces/ATI/pages/68222992/Primeros+pasos+para+ingresar+Huawei+GPON+ONU+Cliente'>esta guía</a>">
    <i class="ion ion-md-help-circle text-black-lighter"></i>
    </span>

  </li>

  <li>Anda a la pestaña <strong>WAN</strong> y en <strong>WAN Configuration</strong> has click en 
      <strong>1_INTERNET_R_VID_100</strong> y luego en <strong>User ID</strong> pega esto 
      ${'desc_client'} y luego hacemos click en <strong>Apply</strong>

      <span class="mf-tooltip" data-content="<a target='_blak' href='/static/img/help/image2019-8-15_22-9-41.png'> <img src='/static/img/help/image2019-8-15_22-9-41.png' width='310%'></a>">
      <i class="ion ion-md-help-circle text-black-lighter"></i>
      </span>

  </li>

  <li>Luego ir a la pestana <strong>WLAN</strong> y en:<br>
      <span style="color:#0476b4">2.4G Basic Network Settings</span>,
      anda a la sección <strong>SSID Name</strong> y copia el SSID ${'SSID_2G'} <i onclick="copy_ip('${'SSID_2G'}')" class="far fa-lg fa-fw fa-copy"></i>
      luego anda a <strong>WPA PreSharedKey</strong> y copia la contraseña del wifi ${'password'} <i onclick="copy_ip('${'password'}')" class="far fa-lg fa-fw fa-copy"></i>
      has click en el botón <strong>Apply</strong>
      
      <span class="mf-tooltip" data-content="<a target='_blak' href='/static/img/help/image2019-8-15_22-13-0.png'> <img src='/static/img/help/image2019-8-15_22-13-0.png' width='310%'></a>">
      <i class="ion ion-md-help-circle text-black-lighter"></i>
      </span>

      <br><br>
      <span style="color:#0476b4">5G Basic Network Settings</span>,
      anda a la sección <strong>SSID Name</strong> y copia el SSID ${'SSID_5G'} <i onclick="copy_ip('${'SSID_5G'}')" class="far fa-lg fa-fw fa-copy"></i>
      luego anda a <strong>WPA PreSharedKey</strong> y copia la contraseña del wifi ${'password'} <i onclick="copy_ip('${'password'}')" class="far fa-lg fa-fw fa-copy"></i>
      has click en el botón <strong>Apply</strong>
      
      <span class="mf-tooltip" data-content="<a target='_blak' href='/static/img/help/image2019-8-15_22-14-45'><img src='/static/img/help/image2019-8-15_22-14-45.png' width='310%'></a>">
      <i class="ion ion-md-help-circle text-black-lighter"></i>
      </span>

      <br>

  </li>

  <li>Una vez que estés listo haz click en el botón de comprobar.</li>

  </ol>			
  	</div>
  </div>

</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
  <button type="button" class="btn btn-default" onclick="comprobar_wifi()">Comprobar</button>
</div>`;
export default modal;