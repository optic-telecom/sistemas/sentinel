userInSession();
App.setPageTitle('Sentinel - Lista de Setupbox');

$("#menu_tv").addClass('active');
$("#menu_vouchers").addClass('active');

var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};

$(document).ready(function (){
  var id = getUrlLastParameter()
  var url_dt = `/datatables/setupbox-detail/${id}`;
  var oTable = $('#table_setupbox').dataTable({
        "aaSorting":[[7,"desc"]],
        "language": {
        "sLengthMenu":     "Mostrar _MENU_ Decodificadores",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando Decodificadores del _START_ al _END_ de un total de _TOTAL_ Decodificadores",
        "sInfoEmpty":      "Mostrando Decodificadores del 0 al 0 de un total de 0 Decodificadores",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ Decodificadores)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "sProcessing": "Cargando...",
        "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
        },

        "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      },
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": url_dt,
            "type": "POST",
            "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
        },

        "columnDefs":[
          {
          targets:[9],
          orderable:false,
          searchable:false,
          className:['text-center',]
          },
          {
            targets:[1,2,3,4,5,6,7,8],
            orderable:true,
            searchable:true,
            className:['text-center',]
          }
        ],


   });

}); 

url_model_ac = "/api/v1/model-ac/";
url_status_ac = "/api/v1/status-stb-ac/";
url_operator_ac = "/api/v1/operator-ac/";
url_service_ac = "/api/v1/service-ac/";
url_voucher_ac = "/api/v1/voucher-ac/";

var id = getUrlLastParameter();
path_setupbox = "/api/v1/setupboxs/";


detailSetupBox=function(obj){

  var stb = new SPA('Detalle del SetupBox','light');

  var content = stb.RowTable('Modelo',obj.model);
  content += stb.RowTable('MAC Address',obj.mac);
  content += stb.RowTable('Status',obj.status);
  content += stb.RowTable('Operador',obj.operator);
  content += stb.RowTable('Servicio',obj.service);
  content += stb.RowTable('Voucher',obj.voucher);

  var html = stb.TableBody('SetupBox '+ obj.mac,content);

  $(stb.modalId)
  .empty()
  .append(html)
  .modal({show:true})


}

loadModal = function(){

  var stb = new SPA('Generar un nuevo SetupBox','success');
  var content = stb.Select('Modelo','model')
  content += stb.Input('MAC Address','mac','text')
  content += stb.Select('Status','status')
  content += stb.Select('Operador','operator')
  content += stb.Select('Servicio','service')
  // content += stb.Select('Voucher','voucher');

  var footer = stb.BtnAction()
  footer += stb.BtnAction('Crear','function','newSetupbox',0);

  var html = stb.modalBody(content,footer);

  $(stb.modalId)
  .empty()
  .append(html)
  .modal({show:true})

  stb.Select2Ajax(url_model_ac,'model','Modelo',false);
  stb.Select2Ajax(url_status_ac,'status','Status',false);
  stb.Select2Ajax(url_operator_ac,'operator','Operador',false);
  stb.Select2Ajax(url_service_ac,'service','Servicio',false);
  // stb.Select2Ajax(url_voucher_ac,'voucher','Voucher',false);
  
}

newSetupbox = function(id=0){
  var data = {
    model:$("#model").val(),
    mac:$("#mac").val(),
    status:$("#status").val(),
    operator:$("#operator").val(),
    service:$("#service").val(),
    voucher:getUrlLastParameter()
  }

  validator = [];
  if(ValidatorField(data,'model','código')){
    validator.push(true);
  };
  if(ValidatorField(data,'mac','mac address')){
    validator.push(true);
  };
  if(ValidatorField(data,'status','status')){
    validator.push(true);
  };
  if(ValidatorField(data,'operator','operador')){
    validator.push(true);
  };
  if(ValidatorField(data,'service','servicio')){
    validator.push(true);
  };
  if(validator.length==0){

    axios.post(path_setupbox,data,conf).then((response)=>{
      if(response.status ==201)
      {
        _exito('El setupbox se ha registrado satisfactoriamente')
        setTimeout(updateDatatable(),40)
        $("#modal_create").modal('toggle')
      }
    })
    .catch((error)=>{
      handleErrorAxios(error)
    })

  }

}

// EDICIÓN

loadModalEdit = function(obj){

  var stb = new SPA('Editar el SetupBox '+obj.mac,'warning');
  var content = stb.Select('Modelo','model')
  content += stb.Input('MAC Address','mac','text',obj.mac)
  content += stb.Select('Status','status')
  content += stb.Select('Operador','operator')
  content += stb.Select('Servicio','service')
  // content += stb.Select('Voucher','voucher');

  var footer = stb.BtnAction()
  footer += stb.BtnAction('Editar','function','editSetupbox',obj.id);

  var html = stb.modalBody(content,footer);

  $(stb.modalId)
  .empty()
  .append(html)
  .modal({show:true})

  stb.Select2Ajax(url_model_ac,'model','Modelo',false,true,obj.model);
  stb.Select2Ajax(url_status_ac,'status','Status',false,true,obj.status);
  stb.Select2Ajax(url_operator_ac,'operator','Operador',false,true,obj.operator);
  stb.Select2Ajax(url_service_ac,'service','Servicio',false,true,obj.service);
  // stb.Select2Ajax(url_voucher_ac,'voucher','Voucher',false,true,obj.);
  
}

editSetupbox = function(id){
  var data = {
    model:$("#model").val(),
    mac:$("#mac").val(),
    status:$("#status").val(),
    operator:$("#operator").val(),
    service:$("#service").val(),
    voucher:getUrlLastParameter()
  }

  validator = [];
  if(ValidatorField(data,'model','código')){
    validator.push(true);
  };
  if(ValidatorField(data,'mac','mac address')){
    validator.push(true);
  };
  if(ValidatorField(data,'status','status')){
    validator.push(true);
  };
  if(ValidatorField(data,'operator','operador')){
    validator.push(true);
  };
  if(ValidatorField(data,'service','servicio')){
    validator.push(true);
  };
  if(validator.length==0){
    path_setupbox_detail = "/api/v1/setupboxs/:val:/".replace(':val:',id);

    axios.put(path_setupbox_detail,data,conf).then((response)=>{
      if(response.status ==200)
      {
        _exito('El setupbox se ha editado satisfactoriamente')
        setTimeout(updateDatatable(),40)
        $("#modal_create").modal('toggle')
      }
    })
    .catch((error)=>{
      handleErrorAxios(error)
    })

  }

}

// ELIMINACIÓN

deleteSetupbox = (id) => {
  swal({
    title: `¿Eliminar el setupbox?`,
    text: "¿Realmente esta seguro?, Recuerde que esta acción no se puede deshacer",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      path_setupbox_detail = "/api/v1/setupboxs/:val:/".replace(':val:',id);
      axios.delete(path_setupbox_detail,conf).then((response)=>{
        if(response.status == 204)
        {
          _exito('Su setupbox ha sido eliminado satisfactoriamente')
          setTimeout(updateDatatable(),40)
        }
      })
      .catch((error)=>{
        handleErrorAxios(error)
      })

    } else {
      _info("Su setupbox está a salvo.");
    }
  });
}

