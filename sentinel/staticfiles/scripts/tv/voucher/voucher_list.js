userInSession();
App.setPageTitle('Sentinel - Lista de Vouchers');

$("#menu_tv").addClass('active');
$("#menu_vouchers").addClass('active');

var conf = {
  headers: { Authorization: 'jwt ' + HDD.get(Django.name_jwt) }
};

$(document).ready(function () {

  url_dt = Django.datatable_voucher_list;
  oTable = $('#table_vouchers').dataTable({
    "aaSorting": [[0, "desc"]],
    "language": {
      "sLengthMenu": "Mostrar _MENU_ Voucher",
      "sZeroRecords": "No se encontraron resultados",
      "sEmptyTable": "Ningún dato disponible en esta tabla",
      "sInfo": "Mostrando Voucher del _START_ al _END_ de un total de _TOTAL_ Voucher",
      "sInfoEmpty": "Mostrando Voucher del 0 al 0 de un total de 0 Voucher",
      "sInfoFiltered": "(filtrado de un total de _MAX_ Voucher)",
      "sInfoPostFix": "",
      "sSearch": "Buscar:",
      "sUrl": "",
      "sInfoThousands": ",",
      "sLoadingRecords": "Cargando...",
      "sProcessing": "Cargando...",
      "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
      },

      "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": url_dt,
      "type": "POST",
      "headers": { 'Authorization': HDD.get(Django.name_jwt) },
    },
    "columnDefs": [
      {
        targets: [6],
        orderable: false,
        searchable: false,
        className: ['text-center',]
      },
      {
        targets: [1, 2, 3, 4, 5],
        orderable: true,
        searchable: true,
        className: ['text-center',]
      }
    ],


  });

});

url_supplier_status = '/api/v1/status-voucher-ac/';
url_operator = '/api/v1/operator-ac/'

url_voucher = Django.api_voucher_list;
url_voucher_detail = Django.api_voucher_detail;

// DETALLE
detailVoucher = function(obj){
  var voucher = new SPA('Detalle del Voucher '+obj.code,'light');
  var content = voucher.RowTable('Código',obj.code)
  content += voucher.RowTable('Operador',obj.operator)
  content += voucher.RowTable('Status',obj.status)

  if(obj.stb_assoc.length==1){
    content += voucher.RowTable('SetupBox Asoc #1',obj.stb_assoc[0].mac)
  }else if(obj.stb_assoc.length==2){
    content += voucher.RowTable('SetupBox Asoc #1',obj.stb_assoc[0].mac)
    content += voucher.RowTable('SetupBox Asoc #2',obj.stb_assoc[1].mac)
  }

  var html = voucher.TableBody('Voucher '+obj.code, content);

  $(voucher.modalId)
  .empty()
  .append(html)
  .modal({show:true})



}

// CREACIÓN
loadModal = function() {

  // Renderiza el modal para poder crear objetos
  var voucher = new SPA('Generar un nuevo Voucher','success');
  var content = voucher.Input('Código','code','text') + voucher.Select('Operador','operator') 
  content += voucher.Select('Status','supplier_status')

  var footer = voucher.BtnAction()
  footer += voucher.BtnAction('Crear','function','newVoucher',0);

  var html = voucher.modalBody(content,footer);

  $(voucher.modalId)
  .empty()
  .append(html)
  .modal({show:true})

  voucher.Select2Ajax(url_supplier_status,'supplier_status','Status',false);
  voucher.Select2Ajax(url_operator,'operator','Operador',false);



}

newVoucher = function(id=0){
  var data = {
    code:$("#code").val(),
    operator:$("#operator").val(),
    supplier_status:$("#supplier_status").val(),

  }
  validator = [];
  if(ValidatorField(data,'code','código')){
    validator.push(true);
  };
  if(ValidatorField(data,'operator','operador')){
    validator.push(true);
  };
  if(ValidatorField(data,'supplier_status','status')){
    validator.push(true);
  };

  if(validator.length==0){

    axios.post(url_voucher,data,conf).then((response)=>{
      if(response.status ==201)
      {
        _exito('El voucher se ha registrado satisfactoriamente')
        setTimeout(updateDatatable(),40)
        $("#modal_create").modal('toggle')
      }
    })
    .catch((error)=>{
      handleErrorAxios(error)
    })

  }
}

// EDICIÓN
loadModalEdit = function(obj) {

  // Renderiza el modal para poder crear objetos
  var voucher = new SPA('Editar el Voucher '+obj.code,'warning');
  var content = voucher.Input('Código','code','text',obj.code) + voucher.Select('Operador','operator') 
  content += voucher.Select('Status','supplier_status')

  var footer = voucher.BtnAction()
  footer += voucher.BtnAction('Editar','function','editVoucher',obj.id);

  var html = voucher.modalBody(content,footer);

  $(voucher.modalId)
  .empty()
  .append(html)
  .modal({show:true})

  voucher.Select2Ajax(url_supplier_status,'supplier_status','Status',false,true,obj.supplier_status);
  voucher.Select2Ajax(url_operator,'operator','Operador',false,true,obj.operator);



}

editVoucher = function(id){
  var data = {
    code:$("#code").val(),
    operator:$("#operator").val(),
    supplier_status:$("#supplier_status").val(),

  }
  validator = [];
  if(ValidatorField(data,'code','código')){
    validator.push(true);
  };
  if(ValidatorField(data,'operator','operador')){
    validator.push(true);
  };
  if(ValidatorField(data,'supplier_status','status')){
    validator.push(true);
  };

  if(validator.length==0){
    url_voucher_detail = url_voucher_detail.replace(':val:',id);

    axios.put(url_voucher_detail,data,conf).then((response)=>{
      if(response.status ==200)
      {
        _exito('El voucher se ha editado satisfactoriamente')
        setTimeout(updateDatatable(),40)
        $("#modal_create").modal('toggle')
      }
    })
    .catch((error)=>{
      handleErrorAxios(error)
    })

  }





  


}

// ELIMINACIÓN
deleteVoucher = (id,code,stb_assoc) => {
  swal({
    title: `¿Eliminar el voucher ${code}?`,
    text: "¿Realmente esta seguro?, Recuerde que esta acción no se puede deshacer",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      if(stb_assoc>0){
        _info("Primero debe eliminar los Setupbox asociados");
      }else{

        url = url_voucher_detail.replace(':val:',id)
        axios.delete(url,conf).then((response)=>{
          if(response.status == 204)
          {
            _exito('Su voucher ha sido eliminado satisfactoriamente')
            setTimeout(updateDatatable(),40)
          }
        })
        .catch((error)=>{
          handleErrorAxios(error)
        })
      }

    } else {
      _info("Su voucher está a salvo.");
    }
  });
}