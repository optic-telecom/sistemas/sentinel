var header_common = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};

link = function (link,linkTitle,funcion,active=true) {
  return(`<li class="nav-items">
            <a href="#${link}" data-toggle="tab" onclick="return ${funcion}()" class="nav-link-panel ${active?'active':''}">
              <span>${linkTitle}</span>
            </a>
          </li>`)
}

tabBodyGeneral = function(title="",link,active=true,content=''){
  return(`<!-- begin tab-pane -->
  <div class="tab-pane fade ${active?'active show':''}" id="${link}">
    <h3 class="m-t-10"><i class="fa fa-cog"></i> ${title}</h3>
    
    ${content}

  </div>
  <!-- end tab-pane -->`)
}

panelBody = function(links){
  return (`
  <div class="panel panel-inverse" id="container-main">

	<div class="panel-heading">
	  <div class="panel-heading-btn">
		<a class="btn btn-xs btn-icon btn-success" onClick="closePanelAction()">
		  <i class="fa fa-angle-double-right"></i>
		</a>
	  </div>
	  <h4 class="panel-title">Propiedades</h4>
	</div>

	<div class="">
  
		<div class="tab-content">
	
      <img src="${ 'img_model' }" style="width:40px; heigh:50px;" ></img>
      <i class="fas fa-circle" style="color: ${ 'color_status' };"></i>
      <i style="float: right;" class="fas fa-power-off"></i>
    
      <div class="tabbable-line">
      
        <!-- begin nav-pills-panel -->
        <ul class="nav nav-pills-panel">
          ${links}
        </ul>
        <div class="tab-content" id="tabs_content">
        </div>
      </div>

	
		</div>

  	</div>
</div>`)
}

function panel_tv_content(el, id) {
  console.log(id)
  
  var links = link('general','General','loadGeneral',true)  + link('mac','MAC Address','loadMAC',false) + link('modelo','Modelo','loadModel',false) 
  links += link('servicio','Servicio','loadService',false) + link('operador','Operador','loadOperator',false) 
  
  $(el).html(panelBody(links))

  loadGeneral()
}

loadGeneral=()=>{
  var content = `<p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                  Integer ac dui eu felis hendrerit lobortis. Phasellus elementum, nibh eget adipiscing porttitor, 
                  est diam sagittis orci, a ornare nisi quam elementum tortor. Proin interdum ante porta est convallis 
                  dapibus dictum in nibh. Aenean quis massa congue metus mollis fermentum eget et tellus. 
                  Aenean tincidunt, mauris ut dignissim lacinia, nisi urna consectetur sapien, nec eleifend orci eros id lectus.
                </p>`;
  $("#tabs_content").empty().append(tabBodyGeneral('General','general',true,content))
}
loadMAC=()=>{
  $("#tabs_content").empty().append(tabBodyGeneral('MAC Address','mac',true))
}
loadService=()=>{
  $("#tabs_content").empty().append(tabBodyGeneral('Servicio','service',true))
}
loadOperator=()=>{
  $("#tabs_content").empty().append(tabBodyGeneral('Operador','operator',true))
}
loadModel=()=>{
  $("#tabs_content").empty().append(tabBodyGeneral('Modelo','model',true))
}


///** events **///
function closePanelAction() {
  $('.theme-panel').removeClass('active');
  HDD.set('panel_page','0');
}

