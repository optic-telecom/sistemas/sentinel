userInSession();
App.setPageTitle('Sentinel - Detalle de UTP');
var utp_uuid = getUrlLastParameter();
var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};


moment.locale('es');

$("#menu_utp").addClass('active');
$("#menu_utp_general").addClass('active');


//* events *//
$(document).ready(function (){

  url_cpes = Django.datatable_cpes_utp.replace(':val:',utp_uuid);
  url_logs = Django.datatable_logs_utp.replace(':val:',utp_uuid);

  url_syslog = Django.datatable_utpsyslog.replace(':val:',utp_uuid);

  // URL PARA FILTRAR POR LOS SELECTORES

  url_filter_user = Django.select2_syslog
                    .replace('selector','username')
                    .replace('dispositivo','mikrotik')
                    .replace(':val:',utp_uuid)

  url_filter_logtype = Django.select2_syslog
                    .replace('selector','log_type')
                    .replace('dispositivo','mikrotik')
                    .replace(':val:',utp_uuid)

  // url_filter_userip = Django.select2_syslog
  //                   .replace('selector','user_ip')
  //                   .replace('dispositivo','mikrotik')
  //                   .replace(':val:',utp_uuid)

  url_filter_origin = Django.select2_syslog
                    .replace('selector','origin')
                    .replace('dispositivo','mikrotik')
                    .replace(':val:',utp_uuid)

  // url_filter_identificador = Django.select2_syslog
  //                   .replace('selector','identificador')
  //                   .replace('dispositivo','mikrotik')
  //                   .replace(':val:',utp_uuid)


  table_cpes_utp = $('#table_cpes_utp').dataTable({
    "aaSorting":[[0,"desc"]],
      "language": {
      "sLengthMenu":     "Mostrar _MENU_ CPE",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando CPE del _START_ al _END_ de un total de _TOTAL_ CPE",
      "sInfoEmpty":      "Mostrando CPE del 0 al 0 de un total de 0 CPE",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ CPE)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },

      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
    "dom": '<"top">rt<"bottom"iflp><"clear">',
    "processing": true,
    "serverSide": true,
    "searchDelay": 400,
    "ajax": {
        "url": url_cpes,
        "type": "POST",
        "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
        "data": function (d) {
            d.filter_status = $("#filter_status").val();
            d.filter_model = $("#filter_model").val();
            d.filter_fsp = $("#filter_utp").val();
            d.last_seen_filter = $("#last_seen_filter").val();
        },
    },
    "columnDefs": [
      {
        orderable: true,
        searchable: true,
        targets: [0,1,2,3,5,6,7,8,9]
      },
      {
        orderable: true,
        searchable: true,
        targets: [4]
      }
    ],    
  });




  // table_logs = $('#table_logs').dataTable({
  //     "aaSorting":[[0,"desc"]],
  //     "language": {
  //     "sLengthMenu":     "Mostrar _MENU_ LOG",
  //     "sZeroRecords":    "No se encontraron resultados",
  //     "sEmptyTable":     "Ningún dato disponible en esta tabla",
  //     "sInfo":           "Mostrando LOG del _START_ al _END_ de un total de _TOTAL_ LOG",
  //     "sInfoEmpty":      "Mostrando LOG del 0 al 0 de un total de 0 LOG",
  //     "sInfoFiltered":   "(filtrado de un total de _MAX_ LOG)",
  //     "sInfoPostFix":    "",
  //     "sSearch":         "Buscar:",
  //     "sUrl":            "",
  //     "sInfoThousands":  ",",
  //     "sLoadingRecords": "Cargando...",
  //     "oPaginate": {
  //       "sFirst":    "Primero",
  //       "sLast":     "Último",
  //       "sNext":     "Siguiente",
  //       "sPrevious": "Anterior"
  //     },
  //     "oAria": {
  //       "sSortAscending":  ":Activar para ordenar la columna de manera ascendente",
  //       "sSortDescending": ":Activar para ordenar la columna de manera descendente"
  //       }
  //     },
  //     "processing": true,
  //     "serverSide": true,
  //     "searchDelay": 400,
  //     "ajax": {
  //         "url": url_logs,
  //         "type": "POST",
  //         "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
  //         "data": function (d) {
  //           d.filter_type_log = $("#filter_type_log").val();
  //           d.filter_type_action = $("#filter_type_action").val();
  //       },
  //     },
  // });

  table_cpes_utp.on( 'order.dt', function (e, settings) {
     var order = table_cpes_utp.api().order();
     $(".table_cpes_utp > thead > tr > th").css('color','black')
     $( ".table_cpes_utp > thead > tr > th:eq( "+order[0][0]+" )" )
     .css( "color", "#2271b3" );
  });



  url_lease = Django.datatable_lease_utp.replace(':val:',utp_uuid);
  
  table_lease_utp = $('#table_lease').dataTable({
    "aaSorting":[[0,"desc"]],
      "language": {
      "sLengthMenu":     "Mostrar _MENU_ Lease",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando Lease del _START_ al _END_ de un total de _TOTAL_ Lease",
      "sInfoEmpty":      "Mostrando Lease del 0 al 0 de un total de 0 Lease",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ Lease)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },

      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
    "dom": '<"top">rt<"bottom"iflp><"clear">',
    "processing": true,
    "serverSide": true,
    "searchDelay": 400,
    "ajax": {
        "url": url_lease,
        "type": "POST",
        "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
        "data": function (d) {
            if ($("#filter_date_ini").val() !== '' && $("#filter_date_end") !== ''){
              d.filter_date_ini = moment($("#filter_date_ini").val(), 'DD/MM/YYYY HH:mm' ).format('YYYY-DD-MM HH:mm') || '';
              d.filter_date_end = moment($("#filter_date_end").val(), 'DD/MM/YYYY HH:mm' ).format('YYYY-DD-MM HH:mm') || '';
            }
        },
        "dataSrc": function (response) {
          var neighbors = response;
          if( neighbors.data[0])
            $('.lease_last_update').text(` ${neighbors.data[0][0]}`)
          else
            $('.lease_last_update').text('No disponible')
  
          return response.data
        },
    }    
  });

  url_neighbors = Django.datatable_neighbor_utp.replace(':val:',utp_uuid);
  
  table_neighbors_utp = $('#table_neighbors').dataTable({
    "aaSorting":[[0,"desc"]],
      "language": {
      "sLengthMenu":     "Mostrar _MENU_ Lease",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando Lease del _START_ al _END_ de un total de _TOTAL_ Lease",
      "sInfoEmpty":      "Mostrando Lease del 0 al 0 de un total de 0 Lease",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ Lease)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },

      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
    "dom": '<"top">rt<"bottom"iflp><"clear">',
    "processing": true,
    "serverSide": true,
    "searchDelay": 400,
    "ajax": {
        "url": url_neighbors,
        "type": "POST",
        "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
        "dataSrc": function (response) {
          var neighbors = response;
          if (neighbors.data[0])
            $('.neighbors_last_update').text(neighbors.data[0][6])
          else
            $('.neighbors_last_update').text('No disponible')
          return response.data
        },
        
    },   
    "columnDefs": [
      {
          "targets": [ 6 ],
          "visible": false,
          "searchable": false
      }
  ]
   
  });
  
  $('.datetimepicker').datetimepicker({format: 'dd/mm/yyyy hh:ii'})
  
  $('#filter_date_end').change(function(){
    table_lease_utp.api().ajax.reload(null, false);
  })


  fetch(DjangoURL('api_utp_detail_gears', utp_uuid)).then(res => res.json()).then(res => {

    $('#table_gears').dataTable({
      'data': res,
      "language": {
        "sLengthMenu":     "Mostrar _MENU_ equipos",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando equipos del _START_ al _END_ de un total de _TOTAL_ equipos",
        "sInfoEmpty":      "Mostrando equipos del 0 al 0 de un total de 0 equipos",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ equipos)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
        },
        "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        },
      },

      "columnDefs": [
        {
            // The `data` parameter refers to the data for the cell (defined by the
            // `data` option, which defaults to the column being worked with, in
            // this case `data: 0`.
            // "data": null,
            "render": function ( data, type, row ) {
                return '<a href="/#/devices/gear/'+row[0]+'">'+data+'</a>';
            },
            "targets": 0
        }
        // { "visible": false,  "targets": [ 3 ] }
      ], 
    });
  })

  
// ================================================================ \\

// SYSLOG TABLE
// table_utpsyslog = $('#table_utpsyslog').dataTable({
//   "aaSorting":[[0,"desc"]],
//   "language": {
//   "sLengthMenu":     "Mostrar _MENU_ Syslog",
//   "sZeroRecords":    "No se encontraron resultados",
//   "sEmptyTable":     "Ningún dato disponible en esta tabla",
//   "sInfo":           "Mostrando Syslog del _START_ al _END_ de un total de _TOTAL_ Syslog",
//   "sInfoEmpty":      "Mostrando Syslog del 0 al 0 de un total de 0 Syslog",
//   "sInfoFiltered":   "(filtrado de un total de _MAX_ Syslog)",
//   "sInfoPostFix":    "",
//   "sSearch":         "Buscar:",
//   "sUrl":            "",
//   "sInfoThousands":  ",",
//   "sLoadingRecords": "Cargando...",
//   "oPaginate": {
//     "sFirst":    "Primero",
//     "sLast":     "Último",
//     "sNext":     "Siguiente",
//     "sPrevious": "Anterior"
//   },
//   "oAria": {
//     "sSortAscending":  ":Activar para ordenar la columna de manera ascendente",
//     "sSortDescending": ":Activar para ordenar la columna de manera descendente"
//     }
//   },
//   "columnDefs":[{
//     orderable:false,
//     searchable:false,
//     targets : [1,8]
//   }],
//   "processing": true,
//   "serverSide": true,
//   "searchDelay": 400,
//   "ajax": {
//       "url": url_syslog,//'datatable/syslogs/',//url_syslog,
//       "type": "POST",
//       "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
//       "data": function (d) {
//         d.advance_daterange = $("#advance-daterange").val();
//         d.filter_user_sys = $("#filter_user_sys").val();
//         d.filter_type_syslog_sys = $("#filter_type_syslog_sys").val();
//         d.filter_origin_syslog_sys = $("#filter_origin_syslog_sys").val();
//         // d.filter_identificador_syslog_sys = $("#filter_identificador_syslog_sys").val();
//     },
//   },

// });

//   // SYSLOG FILTER

  // function adjustWidth(control) {
  //     control.attr('size', control.val().length);
  // }


  // $("#clear_date").on('click',function(){
  //   $("#advance-daterange input").val("")
  //   table_utpsyslog.api().ajax.reload(null, false);
  // })

// $("#btn_reset").on('click',function(){
//   $('#filter_user_sys,#filter_type_syslog_sys,#filter_origin_syslog_sys').val(null).trigger('change'); // Select the option with a value of '1'
//   $("#ad").val("").attr('placeholder',"Seleccione un rango de fecha");

//   $(".cancelBtn").trigger('click')
  
//   table_utpsyslog.api().ajax.reload(null, false);
// })

  // $.when(
  //   $('#advance-daterange').daterangepicker(jsonDateRange, function(start, end, label) {
  //       $('#advance-daterange input').attr({placeholder:start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY')});
  //   }),
  
  //   $('#advance-daterange').on('apply.daterangepicker', function(ev, picker) {
  
  //     var startDate = picker.startDate.format('YYYY-MM-DD');
  //     var endDate = picker.endDate.format('YYYY-MM-DD');
      
  //   $('#advance-daterange').val(startDate + ',' + endDate)
  //     table_utpsyslog.api().ajax.reload(null, false);

  //   }),

  //   $("#advance-daterange").on("cancel.daterangepicker", function (event, picker) {
  //     $("#ad").val("").attr('placeholder',"Seleccione un rango de fecha")
  //       picker.element.val("");
  //       adjustWidth($(this));
  //       table_utpsyslog.api().ajax.reload(null, false);

  //   }),

  // )


  // $('#filter_user_sys').change(function(){
  //   table_utpsyslog.api().ajax.reload(null, false);
  // })
  // $('#filter_type_syslog_sys').change(function(){
  //   table_utpsyslog.api().ajax.reload(null, false);
  // })
  // $('#filter_origin_syslog_sys').change(function(){
  //   table_utpsyslog.api().ajax.reload(null, false);
  // })
  // // $('#filter_identificador_syslog_sys').change(function(){
  // //   table_utpsyslog.api().ajax.reload(null, false);
  // // })


function getSysLogSelect(url,id_select){
  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };

  axios.get(url,conf)
  .then(function(response){
    if(response.status===200){
      var $data = response.data;

      $(id_select).select2({
        width:'100%',
        placeholder:'filtro',
        data:$data
      })

    }

  })
  .catch(function(error){
    handleErrorAxios(error)
  })

}

getSysLogSelect('api/v1/s2-syslog/created','#filter_created_sys')

// getSysLogSelect('api/v1/s2-syslog/ip','#filter_ip_sys')

getSysLogSelect(url_filter_user,'#filter_user_sys')

getSysLogSelect(url_filter_logtype,'#filter_type_syslog_sys')

getSysLogSelect(url_filter_origin,'#filter_origin_syslog_sys')





// =================================================================================
  getServicesReport()

  getDetailsUTP();
  setTimeout(function() {getEntitysLogs()}, 150);



  getPicsUtp();
  getDocsUtp();
 
  $('#btn-add-gallery').click(addGallery)
  $('#btn-edit-gallery').click(editGallery)

  $('#btn-add-doc').click(addDoc)
  $('#btn-edit-doc').click(editDoc)
  // $('#btn-add-activity').click(addActivity)
  // $('#btn-edit-activity').click(editActivity)


  $('.modal').css('overflow-y', 'auto');

  
  
});/// end ready



function getEntitysLogs(){
  $("#filter_type_log").empty();
  $("#filter_type_log").append('<option>Tipo</option>');
  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };
  var url = DjangoURL('api_entity_list');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        $("#filter_type_log").append(`<option value="${res.data[i].entity}">${res.data[i].entity}</option>`);
      }
      get_actions_logs();
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

get_actions_logs = () => {
  $("#filter_type_action").empty();
  $("#filter_type_action").append('<option>Aciones</option>');
  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };
  var url = DjangoURL('api_actions_log_list');
  // console.log('actions logs', url)
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.actions.length; i++) {
        var o = res.data.actions
        $("#filter_type_action").append(`<option value="${o[i].id}">${o[i].name}</option>`);
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });  
}

var detailUTP = {}

function getDetailsUTP() {
  var url = DjangoURL('api_utp_detail', utp_uuid)
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
        // console.log(res.data)
        
        $("#utp_id").text(res.data.utp_id);
        $("#utp_alias").text(res.data.alias);
        $(".utp_name").text(res.data.alias);
        $("#utp_ip").text(res.data.ip);
        $("#utp_description").text(res.data.description);
        if(res.data.comment){
          $("#utp_comment").text(res.data.comment);
        } else{
          $("#utp_comment").text("");
        }
        $("#utp_brand").text(res.data.model.brand ? res.data.model.brand : 'NO DISPONIBLE' );
        $("#utp_model").text(res.data.model.model);
        $("#utp_firmware").text(res.data.firmware ? res.data.firmware : 'NO DISPONIBLE');       
        $("#utp_architecture").text(res.data.architecture ? res.data.architecture : 'NO DISPONIBLE');       
        $("#utp_serial").text(res.data.serial ? res.data.serial : 'NO DISPONIBLE');       
        //$("#utp_uptime").text(res.data.get_uptime_delta.concat(" (").concat(res.data.get_uptime_date)
        //                              .concat(")"));   
        $("#utp_uptime").text(res.data.get_uptime_delta?res.data.get_uptime_delta:'NO DISPONIBLE');


        $("#utp_region").text(res.data.region?res.data.region:'NO DISPONIBLE');
        $("#utp_commune").text(res.data.commune?res.data.commune:'NO DISPONIBLE');
        $("#utp_street").text(res.data.street?res.data.street:'NO DISPONIBLE');
        $("#utp_house_number").text(res.data.house_number?res.data.house_number:'NO DISPONIBLE');
        $("#utp_floor").text(res.data.floor?res.data.floor:'NO DISPONIBLE');
        $("#utp_tower").text(res.data.tower?res.data.tower:'NO DISPONIBLE');
        $("#utp_parent").text(res.data.parent_alias?res.data.parent_alias:'NO DISPONIBLE');
        $("#utp_towers").text(res.data.towers?res.data.towers:'NO DISPONIBLE');
        $("#utp_apartments").text(res.data.apartments?res.data.apartments:'NO DISPONIBLE');
        
        detailUTP.ip = res.data.ip
        // detailUTP.default_connection_node = res.data.default_connection_node
        detailUTP.ip_range = res.data.ip_range
        detailUTP.plans = res.data.plans
        detailUTP.parent = res.data.parent
        detailUTP.radius = res.data.radius
        detailUTP.status = res.data.status
        detailUTP.node_id = res.data.id

        detailUTP.parent_node = {
          id:  res.data.parent,
          text: res.data.parent_alias?res.data.parent_alias:''}
          
        detailUTP.default_connection_node = {
          id:  res.data.default_connection_node,
          text: res.data.default_connection_node_alias?res.data.default_connection_node_alias:''}
        
        detailUTP.street = {id: res.data.street_id, text: res.data.street}
        detailUTP.region = {id: res.data.region_id, text: res.data.region}
        detailUTP.commune = {id: res.data.commune_id, text: res.data.commune}
        detailUTP.street = {id: res.data.street_id, text: res.data.street}
        detailUTP.number = {id: res.data.address_pulso, text: res.data.house_number}
        detailUTP.floor = {id: res.data.address_pulso, text: res.data.floor}
        detailUTP.tower = {id: res.data.address_pulso, text: res.data.tower}

        
        getPlans()
        getRadius()
        // getNodes()
        
        initSelect2()
        
        $("#ip").val(res.data.ip),
        $("#alias").val(res.data.alias),
        $("#ip_range").val(res.data.ip_range),
        $("#user").val(res.data.username),
        $("#password").val(res.data.password),
        
        $("#apartments").val(res.data.apartments),
        $("#towers").val(res.data.towers),
        $("#comment").val(res.data.comment);



     
        //// status UTP
        if (res.data.status === 1)
        {
          $("#utp_estatus").css('color', Django.color_success).text(res.data.status_display);
        }
        else
        {
          $("#utp_estatus").css('color', Django.color_error).text(res.data.status_display);
        }

        if (res.data.task_in_progress)
        {
          //PENDING
          //
          $("#utp_voltage").text('Recoletando información');
          $("#utp_energycomsumpt").text('Recoletando información');
          $("#utp_cpu").text('Recoletando información');
          $("#utp_ram").text('Recoletando información');
          $("#utp_hdd").text('Recoletando información');
          $("#utp_temperature").text('Recoletando información');
          $("#utp_temperature_cpu").text('Recoletando información');
          $("#interfaces").append('<center><strong>Recoletando información</strong></center>');
        }else{
          var templist = [];
          // Voltage
          if (res.data.voltage){
            templist = ["CHR", "RB750", "hAP lite", "hAP ac lite", "RB2011L"];
            if (res.data.model && templist.includes(res.data.model.model)){
              $("#utp_voltage").css('color','gray').text("NO DISPONIBLE");  
            } else {
              $("#utp_voltage").text(res.data.voltage + 'V');
            }
          } else {
            $("#utp_voltage").css('color','gray').text("NO DISPONIBLE"); 
          }
          
          // Power consumption
          // if (res.data.power_consumption){
          //   templist = ["RB450G", "CHR", "RB750", "RB750UP", "RB850Gx2", "hAP lite", "hAP ac lite", "RB2011L", "RB3011UiAS"];
          //   if (res.data.model && templist.includes(res.data.model.model)){
          //     $("#utp_energycomsumpt").css('color','gray').text("NO DISPONIBLE");
          //   } else if (res.data.model && res.data.model.model == "CCR") {
          //     $("#utp_energycomsumpt").text(res.data.power_consumption);
          //   } else {
          //     $("#utp_energycomsumpt").text("NO DISPONIBLE");
          //   }  
          // } else {
            if(res.data.power_consumption){
              $("#utp_energycomsumpt").text(res.data.power_consumption);
            }else{
              $("#utp_energycomsumpt").text("NO DISPONIBLE");
            }
            // $("#utp_energycomsumpt").css('color','gray').text("NO DISPONIBLE");
          // }

          //// cpu
          if (res.data.cpu){
            var cpu = parseInt(res.data.cpu);
            
            if (cpu){
              if(cpu < 50){
                $("#utp_cpu").css('color', Django.color_success).text(res.data.cpu + ' %');
              }else{
                if (cpu > 51 && cpu < 79 ) {
                   $("#utp_cpu").css('color', Django.color_alert).text(res.data.cpu + ' %');
                }
                else{
                  $("#utp_cpu").css('color', Django.color_error).text(res.data.cpu + ' %');
                }
              }       
            }else {
              $("#utp_cpu").text('NO DISPONIBLE')
            }   
          }


          //// ram
          if (res.data.ram){
            var ram = parseInt(res.data.ram);
            if (ram){
              if(ram < 50){
                $("#utp_ram").css('color', Django.color_success).text(res.data.ram + ' %');
              }else{
                if (ram > 51 && ram < 79 ) {
                   $("#utp_ram").css('color', Django.color_alert).text(res.data.ram + ' %');
                }
                else{
                  $("#utp_ram").css('color', Django.color_error).text(res.data.ram + ' %');
                }
              }        
            }else{
              $("#utp_ram").text('NO DISPONIBLE')
            }  
          }

          //// hdd
          if (res.data.storage){
            var storage = parseInt(res.data.storage);
            if (storage){
              if(storage < 50){
                $("#utp_hdd").css('color', Django.color_success).text(res.data.storage + ' %');
              }else{
                if (storage > 51 && storage < 79 ) {
                   $("#utp_hdd").css('color', Django.color_alert).text(res.data.storage + ' %');
                }
                else{
                  $("#utp_hdd").css('color', Django.color_error).text(res.data.storage + ' %');
                }
              }        
            }else{
              $("#utp_hdd").text('NO DISPONIBLE')
            }            
          }


          ////temperature
          if (res.data.temperature){
            templist = ["CHR", "RB750", "hAP lite", "hAP ac lite", "RB2011L"];
            var temperature = parseInt(res.data.temperature);
            if (res.data.model && templist.includes(res.data.model.model)){
              $("#utp_temperature").css('color','gray').text("NO DISPONIBLE");
            } else {
              if ( temperature < 50 ){
                $("#utp_temperature").css('color', Django.color_success).text(res.data.temperature+' C');
              } else{
                if ( temperature > 51 && temperature < 59){
                  $("#utp_temperature").css('color', Django.color_alert).text(res.data.temperature+' C');
                }
                else{
                  $("#utp_temperature").css('color', Django.color_error).text(res.data.temperature+' C');
                }
              } 
            }   
          } else {
            $("#utp_temperature").css('color','gray').text("NO DISPONIBLE");
          }


          ////temperature cpu
          if (res.data.temperature_cpu){
            var templist = ["RB450G", "CHR", "RB750", "RB750UP", "hAP lite", "hAP ac lite", "RB2011L", "RB3011UiAS"];
            if (res.data.model && templist.includes(res.data.model.model)){
              $("#utp_temperature_cpu").css('color','gray').text("NO DISPONIBLE");
            } else { 
              var temperature_cpu = parseInt(res.data.temperature_cpu);
              if ( temperature_cpu < 50 ){
                $("#utp_temperature_cpu").css('color', Django.color_success).text(res.data.temperature_cpu+' C');
              }
              else{
                if ( temperature_cpu > 51 && temperature_cpu < 59){
                  $("#utp_temperature_cpu").css('color', Django.color_alert).text(res.data.temperature_cpu+' C');
                }
                else{
                  $("#utp_temperature_cpu").css('color', Django.color_error).text(res.data.temperature_cpu+' C');
                }
              }   
            }       
          } else {
            $("#utp_temperature_cpu").css('color','gray').text("NO DISPONIBLE");
          }
        }
        
        if (res.data.get_radius_alias){
          $("#utp_radius").text(res.data.get_radius_alias);  
        } else {
          $("#utp_radius").css('color','gray').text("NO ASIGNADO");
        }

        
        // getAvailabilities()
        // getActivities()
        // getAtivitiesNode()
 
        // try {
        //   showMap(detailUTP.street.text +  detailUTP.number.text)  
        // } catch (error) {
        //   console.log(error);
          
        // }
        

    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}


/**
 * Convert select to array with values
 */    
function serealizeSelects (select)
{
    var array = [];
    select.each(function(){ array.push(parseInt($(this).val())) });
    return array;
}

function editDescription(button1) {
  $('#modal-edit-description').modal('hide');
  // Obtenemos los datos del cpe a modificar
  let newDescription = document.querySelector('#edit-description-area').value 
  let currentID = document.querySelector('#edit-id').innerText
  console.log(newDescription)
  console.log(currentID)
  let url = DjangoURL('api_utpcpe') + currentID  + '/modify_description/'
  console.log(url)
  let data = {
    "id": currentID,
    "description": newDescription
  }
  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 200)
    {      
      swal({
      title: "Actualizado",
        text: 'Descripción del CPE actualizada correctamente',
        icon: "success",
        buttons: false
      }).then(function() {
          updateDatatable("#table_cpes_utp");
        });
     }
     
  })
  .catch(function(error) {
    console.log(error.response);
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){
          $('#modal-sleep').modal('hide');
        });
      }else{
        _error("Por favor verifique los datos ingresados.").then(function(){
          $('#modal-sleep').modal('hide');
        });
      }
    }
})
}

function modalDescription(button1) {
  let currentDescription = button1.parentElement.previousSibling.previousSibling.previousSibling.previousSibling.previousSibling.innerText
  let currentID = button1.parentElement.parentElement.firstChild.nextSibling.innerText
  document.querySelector('#edit-description-area').value = currentDescription 
  document.querySelector('#edit-id').innerText = currentID
  
}


function newUTP() {

  $('#modal-create').modal('hide');
  $('#modal-sleep').modal('show');
  var url = DjangoURL('api_utp_detail', utp_uuid);
  var data = { 
    "ip": $("#ip").val(),
    "alias": $("#alias").val(),
    "ip_range": $("#ip_range").val(),
    "description": "NaN",
    "user": $("#user").val(),
    "password": $("#password").val(), 
    "status": $("#status").val(),
    "default_connection_node": parseInt($("#default_connection_node").val()),
    "parent": parseInt($("#parent_node").val()) ,
    "plans": serealizeSelects($("#plans option:selected")),
    "comment": $("#comment").val(),
    "towers": parseInt($("#towers").val()),
    "apartments": parseInt($("#apartments").val())
  }

  floor = $("#floor").val()
  number = $("#house_number").val()
  data.address_pulso = floor ? floor : number

  // Si el selector no saca valor de piso, significa que es un street_location
  if (floor == null) {
    let url = "https://pulso.multifiber.cl/api/v1/search-complete-location/" + data.address_pulso + "/"
    axios.get(url, {
          headers: {'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'}
    })
    .then(function(res){
       if (res.status == 200){

        data.address_pulso = res['data'][0]['id']
        let url = DjangoURL('api_utp_detail', utp_uuid);
        axios.patch(url, data, conf)
        .then(function(res){
          if (res.status == 200)
          {
            getDetailsUTP()
            
            $('#modal-sleep').modal('hide');
            swal({
            title: "Creado",
              text: 'UTP Actualizada exitosamente.',
              icon: "success",
              buttons: false
            }).then(function() {
              var inputs = document.querySelectorAll('input');
              inputs.forEach(function (input) {
                if (input.type!="search"){
                  input.classList.remove('invalid');
                  input.setCustomValidity("Campo requerido");
                }
              });
              $('#modal-sleep').modal('hide');
              
            });
            clearErrors()
          }
        
        })//end then
        .catch(function(error) {
          console.log(error.response);
          if (error.response.status == 400)
          {
            if (error.response.data.error){
              _error(""+ error.response.data.error).then(function(){
                $('#modal-sleep').modal('hide');
              });
            }else{
              _error("Por favor verifique los datos ingresados.").then(function(){
                $('#modal-sleep').modal('hide');
                $('#modal-create').modal('show')
              });
            }

            
            clearErrors()

            if (error.response.data.radius_node){
              $("#div_radius_node").addClass('has-error')
              $('#error_radius_node').text(error.response.data.radius_node.error ? error.response.data.radius_node.error:error.response.data.radius_node[0])
            }
            if (error.response.data.comment){
              $("#div_comment").addClass('has-error')
              $('#error_comment').text(error.response.data.comment.error ? error.response.data.comment.error:error.response.data.comment[0])
            }
            if (error.response.data.apartments){
              $("#div_apartments").addClass('has-error')
              $('#error_apartments').text(error.response.data.apartments.error ? error.response.data.apartments.error:error.response.data.apartments[0])
            }
            if (error.response.data.towers){
              $("#div_towers").addClass('has-error')
              $('#error_towers').text(error.response.data.towers.error ? error.response.data.towers.error:error.response.data.towers[0])
            }
            if (error.response.data.alias){
              $("#div_alias").addClass('has-error')
              $('#error_alias').text(error.response.data.alias.error ? error.response.data.alias.error:error.response.data.alias[0])
            }

            if (error.response.data.status){
              $("#div_status").addClass('has-error')
              $('#error_status').text(error.response.data.status.error ? error.response.data.status.error:error.response.data.status[0])
            }

            if (error.response.data.parent){
              $("#div_parent_node").addClass('has-error')
              $('#error_parent_node').text(error.response.data.parent.error ? error.response.data.parent.error:error.response.data.parent[0])
            }

            if (error.response.data.default_connection_node){
              $("#div_default_connection_node").addClass('has-error')
              $('#error_default_connection_node').text(error.response.data.default_connection_node.error ? error.response.data.default_connection_node.error:error.response.data.default_connection_node[0])
            }

            if (error.response.data.ip){
              $("#div_ip").addClass('has-error')
              $('#error_ip').text(error.response.data.ip.error ? error.response.data.ip.error:error.response.data.ip[0])
            }

            if (error.response.data.ip_range){
              $("#div_ip_range").addClass('has-error')
              $('#error_ip_range').text(error.response.data.ip_range.error ? error.response.data.ip_range.error:error.response.data.ip_range[0])
            }

            if (error.response.data.user){
              $("#div_user").addClass('has-error')
              $('#error_user').text(error.response.data.user.error ? error.response.data.user.error:error.response.data.user[0])
            }

            if (error.response.data.password){
              $("#div_password").addClass('has-error')
              $('#error_password').text(error.response.data.password.error ? error.response.data.password.error:error.response.data.password[0])
            }

            if (error.response.data.plans){
              $("#div_plans").addClass('has-error')
              $('#error_plans').text(error.response.data.plans.error ? error.response.data.plans.error:error.response.data.plans[0])
            }
            
            if (error.response.data.address_pulso){
              $("#div_address_pulso").addClass('has-error')
              $('#error_address_pulso').text(error.response.data.address_pulso.error ? error.response.data.address_pulso.error:error.response.data.address_pulso[0])
            }

          }

          if (error.response.status == 500)
          {

            _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){
              $('#modal-sleep').modal('hide');
            });
            
          }     
        });
       } else {
        console.log('falló')
        console.log(res)
       }
    })
  }

  if ($("#radius_node option:selected").val()){
    data.radius = parseInt($("#radius_node option:selected").val());
  } else {
    data.radius = 0
  }

  axios.patch(url, data, conf)
  .then(function(res){
    if (res.status == 200)
    {
      getDetailsUTP()
      // console.log('aquiiii')

      
      $('#modal-sleep').modal('hide');
      swal({
      title: "Creado",
        text: 'UTP Actualizada exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        var inputs = document.querySelectorAll('input');
        inputs.forEach(function (input) {
          if (input.type!="search"){
            input.classList.remove('invalid');
            input.setCustomValidity("Campo requerido");
          }
        });
        $('#modal-sleep').modal('hide');
        
      });
      clearErrors()
    }
  
  })//end then
  .catch(function(error) {
    console.log(error.response);
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){
          $('#modal-sleep').modal('hide');
        });
      }else{
        _error("Por favor verifique los datos ingresados.").then(function(){
          $('#modal-sleep').modal('hide');
          $('#modal-create').modal('show')
        });
      }

      
      clearErrors()

      if (error.response.data.radius_node){
        $("#div_radius_node").addClass('has-error')
        $('#error_radius_node').text(error.response.data.radius_node.error ? error.response.data.radius_node.error:error.response.data.radius_node[0])
      }
      if (error.response.data.comment){
        $("#div_comment").addClass('has-error')
        $('#error_comment').text(error.response.data.comment.error ? error.response.data.comment.error:error.response.data.comment[0])
      }
      if (error.response.data.apartments){
        $("#div_apartments").addClass('has-error')
        $('#error_apartments').text(error.response.data.apartments.error ? error.response.data.apartments.error:error.response.data.apartments[0])
      }
      if (error.response.data.towers){
        $("#div_towers").addClass('has-error')
        $('#error_towers').text(error.response.data.towers.error ? error.response.data.towers.error:error.response.data.towers[0])
      }
      if (error.response.data.alias){
        $("#div_alias").addClass('has-error')
        $('#error_alias').text(error.response.data.alias.error ? error.response.data.alias.error:error.response.data.alias[0])
      }

      if (error.response.data.status){
        $("#div_status").addClass('has-error')
        $('#error_status').text(error.response.data.status.error ? error.response.data.status.error:error.response.data.status[0])
      }

      if (error.response.data.parent){
        $("#div_parent_node").addClass('has-error')
        $('#error_parent_node').text(error.response.data.parent.error ? error.response.data.parent.error:error.response.data.parent[0])
      }

      if (error.response.data.default_connection_node){
        $("#div_default_connection_node").addClass('has-error')
        $('#error_default_connection_node').text(error.response.data.default_connection_node.error ? error.response.data.default_connection_node.error:error.response.data.default_connection_node[0])
      }

      if (error.response.data.ip){
        $("#div_ip").addClass('has-error')
        $('#error_ip').text(error.response.data.ip.error ? error.response.data.ip.error:error.response.data.ip[0])
      }

      if (error.response.data.ip_range){
        $("#div_ip_range").addClass('has-error')
        $('#error_ip_range').text(error.response.data.ip_range.error ? error.response.data.ip_range.error:error.response.data.ip_range[0])
      }

      if (error.response.data.user){
        $("#div_user").addClass('has-error')
        $('#error_user').text(error.response.data.user.error ? error.response.data.user.error:error.response.data.user[0])
      }

      if (error.response.data.password){
        $("#div_password").addClass('has-error')
        $('#error_password').text(error.response.data.password.error ? error.response.data.password.error:error.response.data.password[0])
      }

      if (error.response.data.plans){
        $("#div_plans").addClass('has-error')
        $('#error_plans').text(error.response.data.plans.error ? error.response.data.plans.error:error.response.data.plans[0])
      }
      
      if (error.response.data.address_pulso){
        $("#div_address_pulso").addClass('has-error')
        $('#error_address_pulso').text(error.response.data.address_pulso.error ? error.response.data.address_pulso.error:error.response.data.address_pulso[0])
      }

    }

    if (error.response.status == 500)
    {

      _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){
        $('#modal-sleep').modal('hide');
      });
      
    }     
     
    //handleErrorAxios(error);
  });
  
};



clearErrors = () => {
  $('#error_radius_node').text('')
  $('#error_alias').text('')
  $('#error_status').text('')
  $('#error_parent_node').text('')
  $('#error_default_connection_node').text('')
  $('#error_ip').text('')
  $('#error_ip_range').text('')
  $('#error_user').text('')
  $('#error_password').text('')
  $('#error_plans').text('')
  $('#error_address_pulso').text('')
  $('#error_comment').text('')
  $('#error_apartments').text('')
  $('#error_towers').text('')
  
  $('#div_radius_node').removeClass('has-error')
  $('#div_alias').removeClass('has-error')
  $('#div_status').removeClass('has-error')
  $('#div_parent_node').removeClass('has-error')
  $('#div_default_connection_node').removeClass('has-error')
  $('#div_ip').removeClass('has-error')
  $('#div_ip_range').removeClass('has-error')
  $('#div_user').removeClass('has-error')
  $('#div_password').removeClass('has-error')
  $('#div_plans').removeClass('has-error')
  $('#div_address_pulso').removeClass('has-error')
  $('#div_comment').removeClass('has-error')
  $('#div_apartments').removeClass('has-error')
  $('#div_towers').removeClass('has-error')
}


deleteSpeed = (id) => {
  delete_object(id, 'speed/olt', null, null, function(){
    table_speeds.api().ajax.reload(null, false);
  });  
}

function updateCPElist(){
  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };
  $('#modal-sleep').modal('show');
  var url = DjangoURL('api_update_list_utpcpe',utp_uuid);
  var data = {"utp_uuid" : utp_uuid};
  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 200)
    {
      $("#loader_modal_speed").html('');
      //$('#modal-create-speed').modal('toggle');
      $('#modal-sleep').modal('hide');
      $.gritter.add({
        title: 'Actualizado',
        text: 'La lista CPE fue actualizada correctamente',
        time: 1800,
      });      
      updateDatatable("#table_cpes_utp");
      updateDatatable("#table_lease");
    }
  })//end then
  .catch(function(error) {
    $('#modal-sleep').modal('hide');
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){
        });
      }else{
        _error("Por favor verifique los datos ingresados.").then(function(){
        });
      }
      return
    }else{
      handleErrorAxios(error);
    }
  });
}


function updateNeighborsList() {
  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };
  $('#modal-sleep').modal('show');
  var url = DjangoURL('api_utp_detail_get_neighbors',utp_uuid);
  var data = {"utp_uuid" : utp_uuid};
  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 200)
    {
      $("#loader_modal_speed").html('');
      //$('#modal-create-speed').modal('toggle');
      $('#modal-sleep').modal('hide');
      $.gritter.add({
        title: 'Actualizado',
        text: 'La lista de Neighbors fue actualizada correctamente',
        time: 1800,
      });      
      updateDatatable("#table_neighbors");
    }
  })//end then
  .catch(function(error) {
    $('#modal-sleep').modal('hide');
    if (error.response.status == 422)
    {
      if (error.response.data.detail){
        _error(""+ error.response.data.detail).then(function(){
        });
      }else{
        _error("Ocurrio un error inesperado.").then(function(){
        });
      }
      return
    }else{
      handleErrorAxios(error);
    }
  });
}







// getServicesReport = () => {
//   var url = DjangoURL('api_utp_detail_services_report', utp_uuid)
  

//   fetch(url).then(res => res.json()).then(res => {
//     // console.log(res);
    
//     $('#table_services').dataTable({
//       data: res.services,
//       language: {
//         "sLengthMenu":     "Mostrar _MENU_ servicios",
//         "sZeroRecords":    "No se encontraron resultados",
//         "sEmptyTable":     "Ningún dato disponible en esta tabla",
//         "sInfo":           "Mostrando servicios del _START_ al _END_ de un total de _TOTAL_ servicios",
//         "sInfoEmpty":      "Mostrando servicios del 0 al 0 de un total de 0 servicios",
//         "sInfoFiltered":   "(filtrado de un total de _MAX_ servicios)",
//         "sInfoPostFix":    "",
//         "sSearch":         "Buscar:",
//         "sUrl":            "",
//         "sInfoThousands":  ",",
//         "sLoadingRecords": "Cargando...",
//         "oPaginate": {
//           "sFirst":    "Primero",
//           "sLast":     "Último",
//           "sNext":     "Siguiente",
//           "sPrevious": "Anterior"
//         },
//         "oAria": {
//           "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
//           "sSortDescending": ": Activar para ordenar la columna de manera descendente"
//         },
//       }, 
//       columns: [
//         { data: "number" },
//         { data: "composite_address" },
//         { data: "status" },
//       ],
//       columnDefs:[
//         {
//           render: function(data, type, row) {
//             if (data === 1) {
//                 var label = 'success';
//             } else if (data === 2) {
//                 var label = 'warning';
//             } else if (data === 3) {
//                 var label = 'danger';
//             } else {
//                 var label = 'default';
//             }

//             if(data === 1){
//               data = 'Activo'
//             }else if(data === 2){
//               data = 'Por retirar'
//             }else if(data === 3){
//               data = 'Retirado'
//             }else if(data === 4){
//               data = 'No instalado'
//             }else if(data === 5){
//               data = 'Canje'
//             }else if(data === 6){
//               data = 'Instalación rechazada'
//             }else if(data === 7){
//               data = 'Venta en verde'
//             }else if(data === 8){
//               data = 'Moroso'
//             }else if(data === 9){
//               data = 'Por re-agendar'
//             }else if(data === 10){
//               data = 'Cambio de titular'
//             }else if(data === 11){
//               data = 'Descartado'
//             }else if(data === 12){
//               data = 'Baja temporal'
//             }else if(data === 13){
//               data = 'Perdido, no se pudo retira'
//             }

//             return '<span class="label label-'+label+'">'+data+'</span>';
//           },
//           targets: 2,
//         },
//       ],
//     });
//     if(res.indicators){
//       $('#actives').text(res.indicators.actives)
//       $('#pending').text(res.indicators.pending)
//       $('#inactive').text(res.indicators.inactive)
//       $('#not_installed').text(res.indicators.not_installed)
//       $('#free').text(res.indicators.free)
//       $('#install_rejected').text(res.indicators.install_rejected)
//       $('#off_plan_sale').text(res.indicators.off_plan_sale)
//       $('#moroso').text(res.indicators.moroso)
//       $('#to_reschedule').text(res.indicators.to_reschedule)
//       $('#holder_change').text(res.indicators.holder_change)
//       $('#discared').text(res.indicators.discared)
//       $('#on_hold').text(res.indicators.on_hold)
//       $('#lost').text(res.indicators.lost)
//     }
//   })
// }



function getPicsUtp() {
  var url = DjangoURL('api_utp_detail_pics', utp_uuid)
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200){
      // console.log(res.data)

      $("#pics-content").html('')
      
      if(res.data.length === 0){
        $("#pics-content").html(`<p style="margin: auto; " class="text-center">No hay fotos aún.</p>`);
        
      }else {
        for (var i = 0; i < res.data.length; i++) {

          var created_at = moment(res.data[i].created_at).format('lll');
          var caption = res.data[i].caption;
          $("#pics-content").append(`
            <div class="col-lg-3 col-md-4 col-xs-6">
                <p class="text-center"><small title="${created_at}"><i class="ti-time"> </i> ${ created_at }</small></p>
                <a class="thumbnail" href="${ res.data[i].photo }" data-featherlight="image">
                  <img class="img-fluid" src="${ res.data[i].photo_thumbnail }" alt="${ caption }" title="${ caption }">
                </a>
                <div class="pull-left">
                    <small title="${ caption }">
                        <a  type="button" href="#modal-edit-gallery" 
                            onclick="fill_update('${caption}', ${res.data[i].id})" 
                            data-toggle="modal" >
                            <i class="fa fa-pen"> </i> 
                            ${ caption }
                        </a>
                    </small>
                </div> 
                <div class="pull-right">
                    <a type="button"  class="" onclick="delete_pic(${res.data[i].id})">
                        <i class="fa fa-trash-alt btn btn-danger" ></i>
                    </a>                        
                </div>
            </div>
          `);
        }
      }


    }

    if (res.status == 401){
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}


function addGallery() {
  
  var formData = new FormData();
  var picfile = document.querySelector('#pic_file');
  
  if(picfile.files.length > 0)
    formData.append("pic", picfile.files[0]);
  
  formData.append('caption', $("#pic_foot").val());


  var url = DjangoURL('api_utp_add_pic', utp_uuid);
  axios.post(url, formData, {
    headers: { 
      Authorization: 'jwt '+ HDD.get(Django.name_jwt),
      'Content-Type': 'multipart/form-data'
    },
  })
  .then(function(response){
    // console.log(response);
    if (response.status == 200)
    {
    
      $('#modal-add-gallery').modal('hide');
    
      $.gritter.add({
        title: 'Foto subida',
        time: 1000,
      });

      getPicsUtp();
    }
  })//end then
  .catch(function(error) {
    if (error.response.data.detail)
      _error(error.response.data.detail);
    else
      handleErrorAxios(error);
  });
}


      
function getDocsUtp() {
  var url = DjangoURL('api_utp_detail_docs', utp_uuid)
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200){
      // console.log(res.data)

      $("#docs-content").html('')
      
      if(res.data.length === 0){
        $("#docs-content").html(`<tr><td colspan="6"><p class="text-center">No hay documentos aún.</p></td></tr>`);
        
      }else {
        for (var i = 0; i < res.data.length; i++) {

          var created_at = moment(res.data[i].created_at).format('lll');
          
          $("#docs-content").append(`
            <tr>
              <td>
                <i class="ti-pencil-alt"> </i> 
                <a type="button" href="#modal-edit-doc" data-toggle="modal" onclick="fill_update_doc('${res.data[i].description}', ${res.data[i].id})" >${ res.data[i].name }</a>
              </td>
                <td>${ res.data[i].description }</td>
                <td>${ res.data[i].extension.toUpperCase() }</td>
                <td><span title="${ created_at }"><i class="ti-time"> </i> ${ created_at }</span></td>
                <td>${ res.data[i].agent}</td>
                <td>
                  <a href="${ res.data[i].file }" class="btn btn-primary" target="_blank">
                    <i class="fa fa-eye"></i>
                  </a>
                  <a href="${ res.data[i].file }" class="btn btn-success" target="_blank" download>
                    <i class="fa fa-download"></i>
                  </a>
                  <a type="button"  class="" onclick="delete_doc(${res.data[i].id})">
                    <i class="fa fa-trash-alt btn btn-danger" ></i>
                  </a>   
               </td>
            </tr>
          `);
        }
      }


    }

    if (res.status == 401){
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

function addDoc() {
  
  var formData = new FormData();
  var docfile = document.querySelector('#doc_file');
  
  if(docfile.files.length > 0)
    formData.append("file", docfile.files[0]);
  
  formData.append('description', $("#doc_description").val());
  formData.append('tag', '');


  var url = DjangoURL('api_utp_add_doc', utp_uuid);
  axios.post(url, formData, {
    headers: { 
      Authorization: 'jwt '+ HDD.get(Django.name_jwt),
      'Content-Type': 'multipart/form-data'
    },
  })
  .then(function(response){
    // console.log(response);
    if (response.status == 200)
    {
    
      $('#modal-add-doc').modal('hide');
    
      $.gritter.add({
        title: 'Documento subido',
        time: 1000,
      });

      getDocsUtp();
    }
  })//end then
  .catch(function(error) {
    if (error.response.data.detail)
      _error(error.response.data.detail);
    else
      handleErrorAxios(error);
  });
}



delete_pic = (id) => {
  var url = DjangoURL('pic_api_detail', id);
  delete_object(null, 'foto', url, null, function(){
      getPicsUtp();
  });
}

delete_doc = (id) => {
  var url = DjangoURL('doc_api_detail', id);
  delete_object(null, 'documento', url, null, function(){
      getDocsUtp();
  });
}

fill_update = (caption, id) => {
    $("#pic_edit_foot").val(caption)
    $("#pic_edit_id").val(id)
}

editGallery = () => {
  // console.log('olaaaaa')
  
  var id = $("#pic_edit_id").val()
  // console.log(id)
 
  var formData = new FormData();
  formData.append('caption', $("#pic_edit_foot").val());
 

  var url = DjangoURL('pic_api_detail', id);
  axios.patch(url, formData, {
    headers: { 
      Authorization: 'jwt '+ HDD.get(Django.name_jwt),
    },
  })
  .then(function(response){
    // console.log(response);
    if (response.status == 200)
    {
    

      getPicsUtp();    
      $.gritter.add({
        title: 'Foto actualizada',
        time: 1000,
      });

      $('#modal-edit-gallery').modal('hide')

    }
  })//end then
  .catch(function(error) {

    if (error.response.data.detail)
      _error(error.response.data.detail);
    else
      handleErrorAxios(error);
  });
}


fill_update_doc = (caption, id) => {
    $("#doc_edit_description").val(caption)
    $("#doc_edit_id").val(id)
}

editDoc = () => {
  // console.log('olaaaaa')
  
  var id = $("#doc_edit_id").val()
  // console.log(id)
 
  var formData = new FormData();
  formData.append('description', $("#doc_edit_description").val());
 

  var url = DjangoURL('doc_api_detail', id);
  axios.patch(url, formData, {
    headers: { 
      Authorization: 'jwt '+ HDD.get(Django.name_jwt),
    },
  })
  .then(function(response){
    // console.log(response);
    if (response.status == 200)
    {
    

      getDocsGear();    
      $.gritter.add({
        title: 'Documento actualizado',
        time: 1000,
      });
      $('#modal-edit-doc').modal('hide')


    }
  })//end then
  .catch(function(error) {

    if (error.response.data.detail)
      _error(error.response.data.detail);
    else
      handleErrorAxios(error);
  });
}





getRadius = () => {  
  var url = DjangoURL('api_utp_radius');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      $("#radius_node").append(`<option value="">Selecciona una radius</option>`);
      for (var i = 0; i < res.data.length; i++) {
        if( detailUTP.radius === res.data[i].id)
          $("#radius_node").append(`<option value="${res.data[i].id}" selected >${res.data[i].alias}</option>`);
        else
          $("#radius_node").append(`<option value="${res.data[i].id}">${res.data[i].alias}</option>`);
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}


function getPlans () { 

  $("#plans").empty();
  $("#plans_list").empty();
  $("#plans").append('<option value="" >Planes</option>');

  var url = DjangoURL('api_plan_list');
  // console.log('**************');
  
  // console.log(url);
  
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      // console.log(res.data)
      for (var i = 0; i < res.data.length; i++) {
        if( detailUTP.plans.includes(res.data[i].id)){
          $("#plans").append(`<option value="${res.data[i].id}" selected >${res.data[i].name} </option>`);
          $("#plans_list").append(`<li>${res.data[i].name}</li>`)
        }else      
          $("#plans").append(`<option value="${res.data[i].id}" >${res.data[i].name} </option>`);
          
      }


    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

// function getActivities () { 

//   $("#activity_name").empty();
//   $("#activity_name").append('<option value="" >Seleciona una actividad</option>');
//   $("#edit_activity_name").empty();
//   $("#edit_activity_name").append('<option value="" >Seleciona una actividad</option>');
  

//   var url = DjangoURL('api_duration_activities');
//   console.log('**************');
  
//   console.log(url);
  
//   axios.get(url, conf)
//   .then(function(res){
//     if (res.status == 200)
//     {
//       console.log(res.data)
//       for (var i = 0; i < res.data.length; i++) {
//         $("#activity_name").append(`<option value="${res.data[i].id}" >${res.data[i].name} </option>`);
//         $("#edit_activity_name").append(`<option value="${res.data[i].id}" >${res.data[i].name} </option>`);
//       }


//     }
//     if (res.status == 401)
//     {
//       handleUnauthorizedAxios();
//     }
//   })//end then
//   .catch(function(error) {
//       handleErrorAxios(error);
//   });
// }

// function addActivity() {
  
//   var formData = new FormData();
//   formData.append('duration', $("#activity_Duration").val());
//   formData.append('activity_id', $("#activity_name").val());
//   formData.append('node', detailUTP.node_id);


//   var url = DjangoURL('api_duration_list');
//   axios.post(url, formData, {
//     headers: { 
//       Authorization: 'jwt '+ HDD.get(Django.name_jwt),
//       'Content-Type': 'multipart/form-data'
//     },
//   })
//   .then(function(response){
//     console.log(response);
//     if (response.status == 201)
//     {
    
//       $('#modal-add-activity').modal('hide');
    
//       $.gritter.add({
//         title: 'Actividad creada',
//         time: 1000,
//       });

//       getAtivitiesNode();

      
//       $('#error_activity_name').text('')
//       $('#error_activity_Duration').text('')
//       $('#div_activity_name').removeClass('has-error')
//       $('#div_activity_Duration').removeClass('has-error')
//     }
//   })//end then
//   .catch(function(error) {
//     console.log(error.response)
//     if (error.response.status == 400)
//     {
//       if (error.response.data.error){
//         _error(""+ error.response.data.error).then(function(){
//           $('#modal-sleep').modal('hide');
//         });
//       }else{
//         _error("Por favor verifique los datos ingresados.").then(function(){
//           $('#modal-sleep').modal('hide');
//         });
//       }

      
//       if (error.response.data.activity_id){
//         $("#div_activity_name").addClass('has-error')
//         $('#error_activity_name').text(error.response.data.activity_id.error ? error.response.data.activity_id.error:error.response.data.activity_id[0])
//       }      
//       if (error.response.data.duration){
//         $("#div_activity_Duration").addClass('has-error')
//         $('#error_activity_Duration').text(error.response.data.duration.error ? error.response.data.duration.error:error.response.data.duration[0])
//       }

//     }else{
//       handleErrorAxios(error);
//     }
      
//   });
// }

// getAtivitiesNode = () => {
//   $("#activities-content").empty();
  
//   var url = DjangoURL('api_duration_list')+'?node='+utp_uuid;
//   console.log('+++++++++++++++++');
  
//   console.log(url);
  
//   axios.get(url, conf)
//   .then(function(res){
//     if (res.status == 200)
//     {
//       console.log('+++++++++++++++++');
//       console.log(res.data)
//       for (var i = 0; i < res.data.length; i++) {
//         $("#activities-content").append(`
//         <tr>
//           <td>
//             <a type="button" href="#modal-edit-activity" data-toggle="modal" 
//               onclick="fill_update_activity('${res.data[i].activity_id}', ${res.data[i].duration}, ${res.data[i].id})" >
//               ${res.data[i].activity.name}</td>
//             </a>
            
//           <td>${res.data[i].activity.description}</td>
//           <td>${res.data[i].get_durantion_display}</td>
//         </tr>`)  
//       }
// {/* <button class="btn btn-xs btn-danger" onclick="deleteAvailability(${res.data[i].id})" >Eliminar</a> */}

//     }
//     if (res.status == 401)
//     {
//       handleUnauthorizedAxios();
//     }
//   })//end then
//   .catch(function(error) {
//       handleErrorAxios(error);
//   });
// }



// function editActivity(id) {
  
//   var formData = new FormData();
//   formData.append('duration', $("#edit_activity_Duration").val());
//   formData.append('activity_id', $("#edit_activity_name").val());
//   formData.append('node', detailUTP.node_id);


//   var url = DjangoURL('api_duration_detail', $("#edit_duration_id").val());
//   axios.patch(url, formData, conf)
//   .then(function(response){
//     console.log(response);
//     if (response.status == 200)
//     {
    
//       $('#modal-edit-activity').modal('hide');
    
//       $.gritter.add({
//         title: 'Actividad creada',
//         time: 1000,
//       });

//       getAtivitiesNode();

      
//       $('#error_edit_activity_name').text('')
//       $('#error_edit_activity_Duration').text('')
//       $('#div_edit_activity_name').removeClass('has-error')
//       $('#div_edit_activity_Duration').removeClass('has-error')
//     }
//   })//end then
//   .catch(function(error) {
//     console.log(error.response)
//     if (error.response.status == 400)
//     {
//       if (error.response.data.error){
//         _error(""+ error.response.data.error).then(function(){
//           $('#modal-sleep').modal('hide');
//         });
//       }else{
//         _error("Por favor verifique los datos ingresados.").then(function(){
//           $('#modal-sleep').modal('hide');
//         });
//       }

      
//       if (error.response.data.activity_id){
//         $("#div_edit_activity_name").addClass('has-error')
//         $('#error_edit_activity_name').text(error.response.data.activity_id.error ? error.response.data.activity_id.error:error.response.data.activity_id[0])
//       }      
//       if (error.response.data.duration){
//         $("#div_edit_activity_Duration").addClass('has-error')
//         $('#error_edit_activity_Duration').text(error.response.data.duration.error ? error.response.data.duration.error:error.response.data.duration[0])
//       }

//     }else{
//       handleErrorAxios(error);
//     }
      
//   });
// }


function getAvailabilities () { 

  $("#availabilities").empty();
  
  var url = DjangoURL('api_availability_list')+'?node='+utp_uuid;
  // console.log('**************');
  
  // console.log(url);
  
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      // console.log('**************');
      // console.log(res.data)
      for (var i = 0; i < res.data.length; i++) {
        $("#availabilities").append(`
        <li>
          ${res.data[i].address}
          <button class="btn btn-xs btn-danger" onclick="deleteAvailability(${res.data[i].id})" >Eliminar</a>
        </li>`)  
      }


    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

fill_update_activity = (activity_id, duration, id) => {
  $("#edit_activity_name").val(activity_id)
  $("#edit_activity_Duration").val(duration)
  $("#edit_duration_id").val(id)
}

deleteAvailability = (id) => {
  var url = DjangoURL('api_availability_detail', id);
  delete_object(null, 'Dirección asociada', url, null, function(){
    getAvailabilities()
  });
}







var language = {

  noResults: function() {

    return "No hay resultado";        
  },
  searching: function() {

    return "Buscando..";
  },
  inputTooShort: function() {
    return 'Al menos 2 letras';
  }
}


getSelect2Object = (parent, ajax) => (
  {
    dropdownParent: parent,
    ajax: {
      
      headers: {
          "Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s",
      },
      processResults: function (data) {
        // Transforms the top-level key of the response object from 'items' to 'results'
        
        if(data.results)
          regions = data.results.map((region) => ({id: region.id, text: region.name}))
        else
          regions = data.map((region) => ({id: region.id, text: region.name}))
        
        
        return {
          results: regions
        };
      },
      ...ajax
    },
    width: 'resolve',
    language: language,
    minimumInputLength: 2
  }
)

initSelect2WithParents = (
  region, region_parent, 
  commune, commune_parent,
  street, street_parent,
  house_number, house_number_parent,
  // tower, tower_parent,
  floor, floor_parent,
  isEdit
  ) => {



    axios.get(Django.PULSO_URL + '/api/v1/region/', {
      headers: {
        "Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s",
      }
    })
    .then(function(res){
      if (res.status == 200)
      {
        // console.log(res.data.results)
        for (var i = 0; i < res.data.results.length; i++) {
          // $("#nodes_olt").append(`<option value="${res.data[i].id}">${res.data[i].alias}</option>`);
          $("#region").append(`<option value="${res.data.results[i].id}">${res.data.results[i].name} </option>`);
        }

        $('#region').select2({dropdownParent:$('.region_parent'), language});
      }
      if (res.status == 401)
      {
        handleUnauthorizedAxios();
      }
    })//end then
    .catch(function(error) {
        handleErrorAxios(error);
    });

    

  // $(region).select2(
  //   getSelect2Object(
  //     $(region_parent),
  //     {
  //       url: function() {
  //         return Django.PULSO_URL + '/api/v1/region/'
  //       },
  //       data: function (params) {
  //         return {region: params.term }
  //       }
  //     }
    
  //   )
  // );

  $(commune).select2(
    getSelect2Object(
      $(commune_parent),
      {
        url: function() {
          return Django.PULSO_URL + `/api/v1/search-commune/${$(region).val()}/`
        },
        data: function (params) {
          return { commune: params.term }
        }
      }
    
    )
  );

  $(street).select2(
    getSelect2Object(
      $(street_parent),
      {
        url: function() {
          return Django.PULSO_URL + `/api/v1/search-street/${$(commune).val()}/`
        },
        data: function (params) {
          return { street: params.term }
        }
      }
    
    )
  );

  $(house_number).select2(
    getSelect2Object(
      $(house_number_parent),
      {
        url: function() {
          return Django.PULSO_URL + `/api/v1/search-street-location/${$(street).val()}/`
        },
        data: function (params) {
          return { streetlocation: params.term }
        },
        processResults: function (data) {
          // Transforms the top-level key of the response object from 'items' to 'results'
          // locations = data.map((location) => ({id: location.id+'|'+location.edification_type, text: location.number}))
          locations = data.map((location) => ({id: location.id, text: location.street_no}))
          return {
            results: locations
          };
        }
      }
    
    )
  );

  // $(tower).select2(
  //   getSelect2Object(
  //     $(tower_parent),
  //     {
  //       url: function() {
  //         return Django.PULSO_URL + `/api/v1/search-location/${$(region).val()}/${$(commune).val()}/${$(street).val()}/`
  //       },
  //       data: function (params) {
  //         return { tower: params.term, number: $('#house_number option:selected').html().split('|')[0] }
  //       },
  //       processResults: function (data) {
  //         // Transforms the top-level key of the response object from 'items' to 'results'
  //         locations = data.map((location) => ({id: location.id, text: location.location}))
  //         return {
  //           results: locations
  //         };
  //       }
  //     }
    
  //   )
  // );

  $(floor).select2(

    {
      dropdownParent: $(floor_parent),
      ajax: {
        url: function() {
          return Django.PULSO_URL + `/api/v1/search-complete-location/${$(house_number).val()}/`
        },
        headers: {
            "Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s",
        },
        data: function (params) {
          return { 
            floor_num: params.term, 
            // tower: $('#tower option:selected').html(), 
            // number: $('#house_number option:selected').html().split('|')[0] 
          }
        },
        processResults: function (data) {
          // Transforms the top-level key of the response object from 'items' to 'results'
          // locations = data.map((location) => ({id: location.id+'|'+location.edification_type, text: location.number}))
          locations = data.map((location) => ({id: location.id, text: location.floor_num}))
          return {
            results: locations
          };
        }
      },
      width: 'resolve',
      language: language,
      minimumInputLength: 2
    }




    
  );
  
  $(commune).prop("disabled", true);
  $(house_number).prop("disabled", true);
  $(street).prop("disabled", true);
  // $(tower).prop("disabled", true);
  $(floor).prop("disabled", true);

  
  

  
  $(region).on('change', (e) => {
    // alert(1)
    $(commune).prop("disabled", false);
    $(house_number).prop("disabled", true);
    $(street).prop("disabled", true);
    // $(tower).prop("disabled", true);
    $(floor).prop("disabled", true);
    
    $(commune).empty();
    $(house_number).empty()
    $(street).empty()
    // $(tower).empty()
    $(floor).empty()

  })

  $(commune).on('change', (e) => {
    $(street).prop("disabled", false);
    $(house_number).prop("disabled", true);
    // $(tower).prop("disabled", true);
    $(floor).prop("disabled", true);

    $(house_number).empty()
    $(street).empty()
    // $(tower).empty()
    $(floor).empty()

  })  
  
  $(street).on('change', (e) => {
    $(house_number).prop("disabled", false);
    // $(tower).prop("disabled", true);
    $(floor).prop("disabled", true);
    $(house_number).empty()
    // $(tower).empty()
    $(floor).empty()
  })  

  $(house_number).on('change', (e) => {
    if($(house_number).val().split('|')[1] !== 'Casa')
    { 
      // $(tower).prop("disabled", false);
      $(floor).prop("disabled", false);
      // $(tower).empty()
      $(floor).empty()
    }
  })  
  
  // $(tower).on('change', (e) => {
  //   $(floor).prop("disabled", false);
  //   $(floor).empty()
  // })  


  if(isEdit){
    var newOption = new Option(detailUTP.region.text, detailUTP.region.id, false, false);
    $(region).append(newOption).trigger('change');
    newOption = new Option(detailUTP.commune.text, detailUTP.commune.id, false, false);
    $(commune).append(newOption).trigger('change');
    newOption = new Option(detailUTP.street.text, detailUTP.street.id, false, false);
    $(street).append(newOption).trigger('change');
    newOption = new Option(detailUTP.number.text, detailUTP.number.id, false, false);
    $(house_number).append(newOption).trigger('change');
    newOption = new Option(detailUTP.floor.text, detailUTP.floor.id, false, false);
    $(floor).append(newOption).trigger('change');
    // newOption = new Option(detailUTP.tower.text, detailUTP.tower.id, false, false);
    // $(tower).append(newOption).trigger('change');
  }
}

initSelect2 = () => {
  $('select').css('width','100%')
  $('#radius_node').select2({dropdownParent: $('.radius_node_parent'), language});
  $('#status').select2({dropdownParent:$('.status_parent'), language});
  // $('#parent_node').select2({dropdownParent: $('.parent_node_parent'), language});
  // $('#default_connection_node').select2({dropdownParent: $('.default_connection_node_parent'), language});
  $('#plans').select2({dropdownParent:$('.plans_parent'), language});

  
  var newOption = new Option(detailUTP.parent_node.text, detailUTP.parent_node.id, false, false);
  $('#parent_node').append(newOption).trigger('change');
  var newOption = new Option(detailUTP.default_connection_node.text, detailUTP.default_connection_node.id, false, false);
  $('#default_connection_node').append(newOption).trigger('change');
    
  $('#parent_node').select2(
    {
      dropdownParent: $('.parent_node_parent'),
      ajax: {
        url: function() {
          return DjangoURL('api_node_list')
        },
        headers: {
            "Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s",
        },
        processResults: function (data) {
          // Transforms the top-level key of the response object from 'items' to 'results'
          
          if(data.results)
            nodes = data.results.map((node) => ({id: node.id, text: node.alias}))
          else
            nodes = data.map((node) => ({id: node.id, text: node.alias}))
          
          
          return {
            results: nodes
          };
        },
        data: function (params) {
          return {search: params.term }
        }
      },
      width: 'resolve',
      language: language,
      minimumInputLength: 2
    }
  
  );

  $('#default_connection_node').select2(
    {
      dropdownParent: $('.default_connection_node_parent'),
      ajax: {
        url: function() {
          return DjangoURL('api_node_list')
        },
        headers: {
            "Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s",
        },
        processResults: function (data) {
          // Transforms the top-level key of the response object from 'items' to 'results'
          
          if(data.results)
            nodes = data.results.map((node) => ({id: node.id, text: node.alias}))
          else
            nodes = data.map((node) => ({id: node.id, text: node.alias}))
          
          
          return {
            results: nodes
          };
        },
        data: function (params) {
          return {search: params.term }
        }
      },
      width: 'resolve',
      language: language,
      minimumInputLength: 2
    }
  
  );


  initSelect2WithParents(
    '#region', '.region_parent',
    '#commune', '.commune_parent',
    '#street', '.street_parent',
    '#house_number', '.house_number_parent',
    // '#tower', '.tower_parent',
    '#floor', '.floor_parent',
    true
  )

  initSelect2WithParents(
    '#region', '.region_parent',
    '#commune', '.commune_parent',
    '#street', '.street_parent',
    '#house_number', '.house_number_parent',
    // '#tower', '.tower_parent',
    '#floor', '.floor_parent',
    true
  )

  initSelect2WithParents(
    '#region_dir', '.region_parent_dir',
    '#commune_dir', '.commune_parent_dir',
    '#street_dir', '.street_parent_dir',
    '#house_number_dir', '.house_number_parent_dir',
    // '#tower_dir', '.tower_parent_dir',
    '#floor_dir', '.floor_parent_dir',
    false
  )

}



newAvailability = () => {
  floor = $("#floor_dir").val()
  tower = $("#tower_dir").val()
  number = $("#house_number_dir").val()
  address_pulso = floor ? floor : tower ? tower : number 

  data =  {
    node: detailUTP.node_id,
    address: address_pulso
  }

  
  var url = DjangoURL('api_availability_list');

  $('#modal-sleep').modal('show');
  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 201)
    {
     
      $('#modal-sleep').modal('hide');
      swal({
      title: "Creado",
        text: 'Direccion asociada exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        var inputs = document.querySelectorAll('input');
        inputs.forEach(function (input) {
          if (input.type!="search"){
            input.classList.remove('invalid');
            input.setCustomValidity("Campo requerido");
          }
        });
        $('#modal-sleep').modal('hide');
        
      });

      getAvailabilities()
      
      $('#error_address_pulso_dir').text('')
      $('#div_address_pulso_dir').removeClass('has-error')
    }
  
  })//end then
  .catch(function(error) {
    // console.log(error.response);
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){
          $('#modal-sleep').modal('hide');
        });
      }else{
        _error("Por favor verifique los datos ingresados.").then(function(){
          $('#modal-sleep').modal('hide');
        });
      }

      
      if (error.response.data.parent_node){
        $("#div_node_dir").addClass('has-error')
        $('#error_node_dir').text(error.response.data.parent.error ? error.response.data.parent.error:error.response.data.node[0])
      }      
      if (error.response.data.address){
        $("#div_address_pulso_dir").addClass('has-error')
        $('#error_address_pulso_dir').text(error.response.data.address.error ? error.response.data.address.error:error.response.data.address[0])
      }

    }

    if (error.response.status == 500)
    {

      _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){
        $('#modal-sleep').modal('hide');
      });
      
    }     
     
    //handleErrorAxios(error);
  });
  






}



// showMap = (adress) => {
//   log(address)
//   var adress_map = {
//       data: []
//   };

//   $.ajax
//   ({
//       async: false,
//       url:"https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyB4AeY8Ts757OtL6PHEqHCLzsHk0L6NXUo&address="+adress,
//       type: "POST",
//         success:function(res)
//         {
//           adress_map.data.push
//           ({ 
//               "address"  :  adress, 
//               "latitude" :  res.results[0].geometry.location.lat,
//               "longitud" :  res.results[0].geometry.location.lng,
              
//           });
//         }
//   });

  
//   var map = new google.maps.Map(document.getElementById('map'), {
//       zoom: 18,
//       center: new google.maps.LatLng(adress_map.data[0]["latitude"], adress_map.data[0]["longitud"]),
//       mapTypeId: google.maps.MapTypeId.ROADMAP
//   });

//   var infowindow = new google.maps.InfoWindow();
//   var marker, i;
//   marker = new google.maps.Marker({
//           position: new google.maps.LatLng(adress_map.data[0]["latitude"], adress_map.data[0]["longitud"]),
//           map: map
//   });

//   google.maps.event.addListener(marker, 'click', (function(marker, i){
//       return function(){
//           infowindow.setContent(adress_map.data[0]["address"]);
//           infowindow.open(map, marker);
//       }
//   })(marker, i));
  
// }




function activateMatrixService(matrix_id){

  $('#modal-sleep').modal('show')

  let matrixUrl = "https://optic.matrix2.cl/api/v1/services/" + matrix_id + '/' 
  console.log(matrix_id)

  let data = {
    'status': 1
  }

  axios.patch(matrixUrl, data, {
          headers: {'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'}
    }).then(function(res){
    if (res.status === 200){
      $('#modal-sleep').modal('hide')
      swal({
      title: "Activado",
        text: 'Servicio activado en Matrix correctamente',
        icon: "success",
        buttons: false
      })
    }
  }).catch(function(error) {
      handleErrorAxios(error);
  })
}

function corteDeServicio(service_number){

  $('#modal-sleep').modal('show')

  let corteUrl = '/api/v1/cutting_replacement/'
  let data = {
    'service_number': service_number,
    'type': 'corte'
  }
  axios.post(corteUrl, data, {
          headers: {'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'}
    }).then(function(res){
    if (res.status === 200){
      $('#modal-sleep').modal('hide')
      swal({
      title: "Cortado",
        text: 'El Servicio será cortado dentro de unos minutos',
        icon: "success",
        buttons: false
      })
      updateDatatable("#table_cpes_utp");
    }
  }).catch(function(error) {
      handleErrorAxios(error);
  })
}

function reposicionDeServicio(service_number){

  $('#modal-sleep').modal('show')

  let corteUrl = '/api/v1/cutting_replacement/'
  let data = {
    'service_number': service_number,
    'type': 'reposicion'
  }
  axios.post(corteUrl, data, {
          headers: {'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'}
    }).then(function(res){
    if (res.status === 200){
      $('#modal-sleep').modal('hide')
      swal({
      title: "Conectado",
        text: 'El Servicio será conectado dentro de unos minutos',
        icon: "success",
        buttons: false
      })
      updateDatatable("#table_cpes_utp");
    }
  }).catch(function(error) {
      handleErrorAxios(error);
  })
}

function updateNetworkStatus(){
  let networkStatusUrl = DjangoURL('refresh_network_status');
  let data = {
    'device_id': utp_uuid,
    'device_type': 'UTP'
  }
  axios.post(networkStatusUrl, data, conf)
  .then(function(res){
    if (res.status === 200){
      $.gritter.add({
        title: 'Tarea ejecutándose, por favor espere algunos minutos',
        time: 1000,
      })
    }
  }).catch(function(error) {
      handleErrorAxios(error);
  })
}


function setPrimaryEquipment(service_number, device_ip){

  $('#modal-sleep').modal('show')

 let primaryEquipmentUrl = DjangoURL('set_primary_equipment', service_number)
 let data = {
    'device_ip': device_ip,
    'device_type': 'UTP'
 }
 axios.post(primaryEquipmentUrl, data, conf)
 .then(function(res){
  if (res.status === 200){
      $('#modal-sleep').modal('hide')
      swal({
      title: "Asociado",
        text: 'El Equipo seleccionado fue asociado como primario correctamente',
        icon: "success",
        buttons: false
      })
    }
 }).catch(function(error) {
      handleErrorAxios(error);
  })
}