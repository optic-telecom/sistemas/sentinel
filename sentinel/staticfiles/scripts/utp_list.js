userInSession();
App.setPageTitle('Sentinel - Lista de UTP');

//$("#menu_utp > a ").css('cssText', "color: #FFF !important;");
$("#menu_utp").addClass('active');
$("#menu_utp_general").addClass('active');
var oTable;
var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};


$(document).ready(function (){

  getRadius();
  // getNodes();
  getPlans();

  $('.modal').css('overflow-y', 'auto');

  url_dt = Django.datatable_utp_list;
  oTable = $('#table_utp').dataTable({
      "aaSorting":[[0,"desc"]],
      "language": {
      "sLengthMenu":     "Mostrar _MENU_ UTP",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando UTP del _START_ al _END_ de un total de _TOTAL_ UTP",
      "sInfoEmpty":      "Mostrando UTP del 0 al 0 de un total de 0 UTP",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ UTP)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "sProcessing": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },

      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
      "processing": true,
      "serverSide": true,
      "searchDelay": 400,
      "ajax": {
          "url": url_dt,
          "type": "POST",
          "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
          "data": function (d) {
            d.filter_type = parseInt($("#filter_type").val());
          },
      },
      "columnDefs": [
      {
        orderable: true,
        searchable: true,
        targets: [0,1,2,3,4,5,6,7]
      },
      {
        orderable: false,
        searchable: false,
        targets: [8]
      }
      ], 
  });

    
  $('#filter_type').change(function(){
    oTable.api().ajax.reload(null, false);
  })

  oTable.on( 'order.dt', function (e, settings) {

     var order = oTable.api().order();
     $("#table_utp > thead > tr > th").css('color','black')
     $( "#table_utp > thead > tr > th:eq( "+order[0][0]+" )" )
     .css( "color", "#2271b3" );

  });

  // Error messages
  const customMessages = {
    valueMissing:    'Campo obligatorio',       // `required` attr
  }

  function getCustomMessage (type, validity) {
    if (validity.typeMismatch) {
      return customMessages[`${type}Mismatch`]
    } else {
      for (const invalidKey in customMessages) {
        if (validity[invalidKey]) {
          return customMessages[invalidKey]
        }
      }
    }
  }

  initSelect2()

  var invalidClassName = 'invalid';
  var inputs = document.querySelectorAll('input','textarea');
  inputs.forEach(function (input) {
    // Each time the user types or submits, this will
    // check validity, and set a custom message if invalid.
    function checkValidity () {
      const message = input.validity.valid
        ? null
        : getCustomMessage(input.type, input.validity)
      input.setCustomValidity("")
      if (message!=null){
        input.classList.add(invalidClassName)
      }else{
        input.classList.remove(invalidClassName)
      }
      
    }
    input.addEventListener('input', checkValidity)
    input.addEventListener('invalid', checkValidity)

  })

  // Remove error messages when modal is closed
  $('#modal-create').on('hidden.bs.modal', function () {
    inputs.forEach(function (input) {
      input.classList.remove(invalidClassName)
    })
    document.getElementById("radius_node").classList.remove(invalidClassName);
  })

});  


getRadius = () => {  
  var url = DjangoURL('api_utp_radius');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      $("#radius_node").append(`<option value="">Selecciona una radius</option>`);
      for (var i = 0; i < res.data.length; i++) {
        $("#radius_node").append(`<option value="${res.data[i].id}">${res.data[i].alias}</option>`);
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}



function getPlans () { 

  $("#plans").empty();
  $("#plans").append('<option value="" >Nodo</option>');

  var url = DjangoURL('api_plan_list');
  console.log('**************');
  
  console.log(url);
  
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      console.log(res.data)
      for (var i = 0; i < res.data.length; i++) {
        $("#plans").append(`<option value="${res.data[i].id}">${res.data[i].name} </option>`);
        
      }


    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

Parsley.on('form:init', function() {
  /// get nodes
  /// getNodesUTP();
});

/**
 * Convert select to array with values
 */    
function serealizeSelects (select)
{
    var array = [];
    select.each(function(){ array.push(parseInt($(this).val())) });
    return array;
}


function newUTP() {

  $('#modal-create').modal('hide');
  $('#modal-sleep').modal('show');
  var url = DjangoURL('api_utp');
  
  var data = { 
    "ip": $("#ip").val(),
    "alias": $("#alias").val(),
    "ip_range": $("#ip_range").val(),
    "description": "NaN",
    "user": $("#user").val(),
    "password": $("#password").val(), 
    "status": $("#status").val(),
    "default_connection_node": parseInt($("#default_connection_node").val()),
    "parent": parseInt($("#parent_node").val()) ,
    "plans": serealizeSelects($("#plans option:selected")),
    "comment": $("#comment").val(),
    "towers": parseInt($("#towers").val()),
    "apartments": parseInt($("#apartments").val())
  }

  floor = $("#floor").val()
  number = $("#house_number").val()
  data.address_pulso = floor ? floor : number

  // Si el selector no saca valor de piso, significa que es un street_location
  if (floor == null) {
    let url = "https://pulso.multifiber.cl/api/v1/search-complete-location/" + data.address_pulso + "/"
    axios.get(url, {
          headers: {'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'}
    })
    .then(function(res){
       if (res.status == 200){

        data.address_pulso = res['data'][0]['id']
        let url = DjangoURL('api_utp');
        axios.post(url, data, conf)
        .then(function(res){
          if (res.status == 200)
          {
            getDetailsUTP()
            
            $('#modal-sleep').modal('hide');
            swal({
            title: "Creado",
              text: 'UTP Actualizada exitosamente.',
              icon: "success",
              buttons: false
            }).then(function() {
              var inputs = document.querySelectorAll('input');
              inputs.forEach(function (input) {
                if (input.type!="search"){
                  input.classList.remove('invalid');
                  input.setCustomValidity("Campo requerido");
                }
              });
              $('#modal-sleep').modal('hide');
              
            });
            clearErrors()
          }
        
        })//end then
        .catch(function(error) {
          console.log(error.response);
          if (error.response.status == 400)
          {
            if (error.response.data.error){
              _error(""+ error.response.data.error).then(function(){
                $('#modal-sleep').modal('hide');
              });
            }else{
              _error("Por favor verifique los datos ingresados.").then(function(){
                $('#modal-sleep').modal('hide');
                $('#modal-create').modal('show')
              });
            }

            
            clearErrors()

            if (error.response.data.radius_node){
              $("#div_radius_node").addClass('has-error')
              $('#error_radius_node').text(error.response.data.radius_node.error ? error.response.data.radius_node.error:error.response.data.radius_node[0])
            }
            if (error.response.data.comment){
              $("#div_comment").addClass('has-error')
              $('#error_comment').text(error.response.data.comment.error ? error.response.data.comment.error:error.response.data.comment[0])
            }
            if (error.response.data.apartments){
              $("#div_apartments").addClass('has-error')
              $('#error_apartments').text(error.response.data.apartments.error ? error.response.data.apartments.error:error.response.data.apartments[0])
            }
            if (error.response.data.towers){
              $("#div_towers").addClass('has-error')
              $('#error_towers').text(error.response.data.towers.error ? error.response.data.towers.error:error.response.data.towers[0])
            }
            if (error.response.data.alias){
              $("#div_alias").addClass('has-error')
              $('#error_alias').text(error.response.data.alias.error ? error.response.data.alias.error:error.response.data.alias[0])
            }

            if (error.response.data.status){
              $("#div_status").addClass('has-error')
              $('#error_status').text(error.response.data.status.error ? error.response.data.status.error:error.response.data.status[0])
            }

            if (error.response.data.parent){
              $("#div_parent_node").addClass('has-error')
              $('#error_parent_node').text(error.response.data.parent.error ? error.response.data.parent.error:error.response.data.parent[0])
            }

            if (error.response.data.default_connection_node){
              $("#div_default_connection_node").addClass('has-error')
              $('#error_default_connection_node').text(error.response.data.default_connection_node.error ? error.response.data.default_connection_node.error:error.response.data.default_connection_node[0])
            }

            if (error.response.data.ip){
              $("#div_ip").addClass('has-error')
              $('#error_ip').text(error.response.data.ip.error ? error.response.data.ip.error:error.response.data.ip[0])
            }

            if (error.response.data.ip_range){
              $("#div_ip_range").addClass('has-error')
              $('#error_ip_range').text(error.response.data.ip_range.error ? error.response.data.ip_range.error:error.response.data.ip_range[0])
            }

            if (error.response.data.user){
              $("#div_user").addClass('has-error')
              $('#error_user').text(error.response.data.user.error ? error.response.data.user.error:error.response.data.user[0])
            }

            if (error.response.data.password){
              $("#div_password").addClass('has-error')
              $('#error_password').text(error.response.data.password.error ? error.response.data.password.error:error.response.data.password[0])
            }

            if (error.response.data.plans){
              $("#div_plans").addClass('has-error')
              $('#error_plans').text(error.response.data.plans.error ? error.response.data.plans.error:error.response.data.plans[0])
            }
            
            if (error.response.data.address_pulso){
              $("#div_address_pulso").addClass('has-error')
              $('#error_address_pulso').text(error.response.data.address_pulso.error ? error.response.data.address_pulso.error:error.response.data.address_pulso[0])
            }

          }

          if (error.response.status == 500)
          {

            _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){
              $('#modal-sleep').modal('hide');
            });
            
          }     
        });
       } else {
        console.log('falló')
        console.log(res)
       }
    })
  }

  if ($("#radius_node option:selected").val()){
    data.radius = parseInt($("#radius_node option:selected").val());
  } else {
    data.radius = 0
  }

  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 200)
    {
      getDetailsUTP()
      // console.log('aquiiii')

      
      $('#modal-sleep').modal('hide');
      swal({
      title: "Creado",
        text: 'UTP Actualizada exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        var inputs = document.querySelectorAll('input');
        inputs.forEach(function (input) {
          if (input.type!="search"){
            input.classList.remove('invalid');
            input.setCustomValidity("Campo requerido");
          }
        });
        $('#modal-sleep').modal('hide');
        
      });
      clearErrors()
    }
  
  })//end then
  .catch(function(error) {
    console.log(error.response);
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){
          $('#modal-sleep').modal('hide');
        });
      }else{
        _error("Por favor verifique los datos ingresados.").then(function(){
          $('#modal-sleep').modal('hide');
          $('#modal-create').modal('show')
        });
      }

      
      clearErrors()

      if (error.response.data.radius_node){
        $("#div_radius_node").addClass('has-error')
        $('#error_radius_node').text(error.response.data.radius_node.error ? error.response.data.radius_node.error:error.response.data.radius_node[0])
      }
      if (error.response.data.comment){
        $("#div_comment").addClass('has-error')
        $('#error_comment').text(error.response.data.comment.error ? error.response.data.comment.error:error.response.data.comment[0])
      }
      if (error.response.data.apartments){
        $("#div_apartments").addClass('has-error')
        $('#error_apartments').text(error.response.data.apartments.error ? error.response.data.apartments.error:error.response.data.apartments[0])
      }
      if (error.response.data.towers){
        $("#div_towers").addClass('has-error')
        $('#error_towers').text(error.response.data.towers.error ? error.response.data.towers.error:error.response.data.towers[0])
      }
      if (error.response.data.alias){
        $("#div_alias").addClass('has-error')
        $('#error_alias').text(error.response.data.alias.error ? error.response.data.alias.error:error.response.data.alias[0])
      }

      if (error.response.data.status){
        $("#div_status").addClass('has-error')
        $('#error_status').text(error.response.data.status.error ? error.response.data.status.error:error.response.data.status[0])
      }

      if (error.response.data.parent){
        $("#div_parent_node").addClass('has-error')
        $('#error_parent_node').text(error.response.data.parent.error ? error.response.data.parent.error:error.response.data.parent[0])
      }

      if (error.response.data.default_connection_node){
        $("#div_default_connection_node").addClass('has-error')
        $('#error_default_connection_node').text(error.response.data.default_connection_node.error ? error.response.data.default_connection_node.error:error.response.data.default_connection_node[0])
      }

      if (error.response.data.ip){
        $("#div_ip").addClass('has-error')
        $('#error_ip').text(error.response.data.ip.error ? error.response.data.ip.error:error.response.data.ip[0])
      }

      if (error.response.data.ip_range){
        $("#div_ip_range").addClass('has-error')
        $('#error_ip_range').text(error.response.data.ip_range.error ? error.response.data.ip_range.error:error.response.data.ip_range[0])
      }

      if (error.response.data.user){
        $("#div_user").addClass('has-error')
        $('#error_user').text(error.response.data.user.error ? error.response.data.user.error:error.response.data.user[0])
      }

      if (error.response.data.password){
        $("#div_password").addClass('has-error')
        $('#error_password').text(error.response.data.password.error ? error.response.data.password.error:error.response.data.password[0])
      }

      if (error.response.data.plans){
        $("#div_plans").addClass('has-error')
        $('#error_plans').text(error.response.data.plans.error ? error.response.data.plans.error:error.response.data.plans[0])
      }
      
      if (error.response.data.address_pulso){
        $("#div_address_pulso").addClass('has-error')
        $('#error_address_pulso').text(error.response.data.address_pulso.error ? error.response.data.address_pulso.error:error.response.data.address_pulso[0])
      }

    }

    if (error.response.status == 500)
    {

      _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){
        $('#modal-sleep').modal('hide');
      });
      
    }     
     
    //handleErrorAxios(error);
  });
  
};


clearErrors = () => {
  $('#error_radius_node').text('')
  $('#error_alias').text('')
  $('#error_status').text('')
  $('#error_parent_node').text('')
  $('#error_default_connection_node').text('')
  $('#error_ip').text('')
  $('#error_ip_range').text('')
  $('#error_user').text('')
  $('#error_password').text('')
  $('#error_plans').text('')
  $('#error_address_pulso').text('')
  $('#error_comment').text('')
  $('#error_apartments').text('')
  $('#error_towers').text('')
  
  $('#div_radius_node').removeClass('has-error')
  $('#div_alias').removeClass('has-error')
  $('#div_status').removeClass('has-error')
  $('#div_parent_node').removeClass('has-error')
  $('#div_default_connection_node').removeClass('has-error')
  $('#div_ip').removeClass('has-error')
  $('#div_ip_range').removeClass('has-error')
  $('#div_user').removeClass('has-error')
  $('#div_password').removeClass('has-error')
  $('#div_plans').removeClass('has-error')
  $('#div_address_pulso').removeClass('has-error')
  $('#div_comment').removeClass('has-error')
  $('#div_apartments').removeClass('has-error')
  $('#div_towers').removeClass('has-error')
}


delete_utp = (id) => {
  var url = Django.MIKROTIK_URL + '/api/v1/utp/' + id + '/';
  delete_object(null, 'mikrotik', url, null, function(){
    oTable.api().ajax.reload(null, false);
  });
}


taskUpdateUTP = (utp_uuid) => {
  asynchronousTask({
    task:'update_utp',
    olt:utp_uuid
  }, function(){
    updateDatatable("#table_utp");
  });
}

$(document).on('keydown', function(event) {  
  if(event.which == 118) { // F7

    var url = DjangoURL('api_update_uptime_utps');
    axios.post(url, conf)
    .then(function(res){
      if (res.status == 201)
      {
        oTable.api().ajax.reload(null, false);
      }
      if (res.status == 401)
      {
        handleUnauthorizedAxios();
      }
    })//end then
    .catch(function(error) {
        handleErrorAxios(error);
    });


  }
});

$(window).keydown(function(e){
  //console.log(e)
});

keydownTaskUTP = (e) => {
  if (e.shiftKey && e.keyCode === 70){
    console.log(e)
  }
}

ayuda = () => {
  alert('Pagina de ayuda de utp')
}


var language = {

  noResults: function() {

    return "No hay resultado";        
  },
  searching: function() {

    return "Buscando..";
  },
  inputTooShort: function() {
    return 'Al menos 2 letras';
  }
}


getSelect2Object = (parent, ajax, name='name') => (
  {
    dropdownParent: parent,
    ajax: {
      
      headers: {
          "Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s",
      },
      processResults: function (data) {
        // Transforms the top-level key of the response object from 'items' to 'results'
        
        if(data.results)
          regions = data.results.map((region) => ({id: region.id, text: region[name]}))
        else
          regions = data.map((region) => ({id: region.id, text: region[name]}))
        
          return {
          results: regions
        };
      },
      ...ajax
    },
    width: 'resolve',
    language: language,
    minimumInputLength: 2
  }
)
initSelect2 = () => {
  $('select').css('width','100%')
  $('#radius_node').select2({dropdownParent: $('.radius_node_parent'), language});
  $('#status').select2({dropdownParent:$('.status_parent'), language});
  $('#plans').select2({dropdownParent:$('.plans_parent'), language});

  
  $('#parent_node').select2(
    getSelect2Object(
      $('.parent_node_parent'),
      {
        url: function() {
          return DjangoURL('api_node_list')
        },
        data: function (params) {
          return {search: params.term }
        }
      },
      'alias'
    )
  );

  $('#default_connection_node').select2(
    getSelect2Object(
      $('.default_connection_node_parent'),
      {
        url: function() {
          return DjangoURL('api_node_list')
        },
        data: function (params) {
          return {search: params.term }
        }
      },
      'alias'
    )
  );

  axios.get(Django.PULSO_URL + '/api/v1/region/', {
    headers: {
      "Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s",
    }
  })
  .then(function(res){
    if (res.status == 200)
    {
      $("#region").append(`<option value=""></option>`);
      console.log(res.data.results)
      for (var i = 0; i < res.data.results.length; i++) {
        // $("#nodes_olt").append(`<option value="${res.data[i].id}">${res.data[i].alias}</option>`);
        $("#region").append(`<option value="${res.data.results[i].id}">${res.data.results[i].name} </option>`);
      }

      $('#region').select2({dropdownParent:$('.region_parent'), language});
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });

 
  // $('#region').select2(
  //   getSelect2Object(
  //     $('.region_parent'),
  //     {
  //       url: function() {
  //         return Django.PULSO_URL + '/api/v1/region/'
  //       },
  //       data: function (params) {
  //         return {region: params.term }
  //       }
  //     }
    
  //   )
  // );

  $('#commune').select2(
    getSelect2Object(
      $('.commune_parent'),
      {
        url: function() {
          return Django.PULSO_URL + `/api/v1/search-commune/${$('#region').val()}/`
        },
        data: function (params) {
          return { commune: params.term }
        }
      }
    
    )
  );

  $('#street').select2(
    getSelect2Object(
      $('.street_parent'),
      {
        url: function() {
          return Django.PULSO_URL + `/api/v1/search-street/${$('#commune').val()}/`
        },
        data: function (params) {
          return { street: params.term }
        }
      }
    
    )
  );

  $('#house_number').select2(
    getSelect2Object(
      $('.street_parent'),
      {
        url: function() {
          return Django.PULSO_URL + `/api/v1/search-street-location/${$('#street').val()}/`
        },
        data: function (params) {
          return { streetlocation: params.term }
        },
        processResults: function (data) {
          // Transforms the top-level key of the response object from 'items' to 'results'
          locations = data.map((location) => ({id: location.id, text: location.street_no}))
          return {
            results: locations
          };
        }
      },
      'street_no'
      
    
    )
  );

  // $('#tower').select2(
  //   getSelect2Object(
  //     $('.tower_parent'),
  //     {
  //       url: function() {
  //         return Django.PULSO_URL + `/api/v1/search-location/${$('#region').val()}/${$('#commune').val()}/${$('#street').val()}/`
  //       },
  //       data: function (params) {
  //         return { tower: params.term, number: $('#house_number option:selected').html().split('|')[0] }
  //       },
  //       processResults: function (data) {
  //         // Transforms the top-level key of the response object from 'items' to 'results'
  //         locations = data.map((location) => ({id: location.id, text: location.location}))
  //         return {
  //           results: locations
  //         };
  //       }
  //     }
    
  //   )
  // );

  $('#floor').select2(


    {
      dropdownParent: $('.floor_parent'),
      ajax: {
        url: function() {
          return Django.PULSO_URL + `/api/v1/search-complete-location/${$(house_number).val()}/`
        },
        headers: {
            "Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s",
        },
        data: function (params) {
          return { 
            floor_num: params.term, 
            // tower: $('#tower option:selected').html(), 
            // number: $('#house_number option:selected').html().split('|')[0] 
          }
        },
        processResults: function (data) {
          // Transforms the top-level key of the response object from 'items' to 'results'
          // locations = data.map((location) => ({id: location.id+'|'+location.edification_type, text: location.number}))
          locations = data.map((location) => ({id: location.id, text: location.floor_num}))
          return {
            results: locations
          };
        }
      },
      width: 'resolve',
      language: language,
      minimumInputLength: 2
    }


  );
  
  $('#commune').prop("disabled", true);
  $('#house_number').prop("disabled", true);
  $('#street').prop("disabled", true);
  // $('#tower').prop("disabled", true);
  $('#floor').prop("disabled", true);
  
  $('#region').on('change', (e) => {
    // alert(1)
    $('#commune').prop("disabled", false);
    $('#house_number').prop("disabled", true);
    $('#street').prop("disabled", true);
    // $('#tower').prop("disabled", true);
    $('#floor').prop("disabled", true);
    
    $('#commune').empty();
    $('#house_number').empty()
    $('#street').empty()
    // $('#tower').empty()
    $('#floor').empty()

  })

  $('#commune').on('change', (e) => {
    $('#street').prop("disabled", false);
    $('#house_number').prop("disabled", true);
    // $('#tower').prop("disabled", true);
    $('#floor').prop("disabled", true);

    $('#house_number').empty()
    $('#street').empty()
    // $('#tower').empty()
    $('#floor').empty()

  })  
  
  $('#street').on('change', (e) => {
    $('#house_number').prop("disabled", false);
    // $('#tower').prop("disabled", true);
    $('#floor').prop("disabled", true);
    $('#house_number').empty()
    // $('#tower').empty()
    $('#floor').empty()
  })  

  $('#house_number').on('change', (e) => {
    if($('#house_number').val().split('|')[1] !== 'Casa')
    { 
      // $('#tower').prop("disabled", false);
      $('#floor').prop("disabled", false);
      // $('#tower').empty()
      $('#floor').empty()
    }
  })  
  
  // $('#tower').on('change', (e) => {
  //   $('#floor').prop("disabled", false);
  //   $('#floor').empty()
  // })  

}
