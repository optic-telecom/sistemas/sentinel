var header_common = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};

var PanelBoxTemplateCPEUTP = templater`
<div class="panel panel-inverse" id="container-main">
  <div class="panel-heading">
    <div class="panel-heading-btn">
      <a class="btn btn-xs btn-icon btn-success" onClick="closePanelAction()">
        <i class="fa fa-angle-double-right"></i>
      </a>
    </div>
    <h4 class="panel-title">Propiedades</h4>
  </div>
  <div>

  <div class="tab-content">

    <img src="${ 'img_model' }" style="width:40px; heigh:50px;" ></img>
    <i class="fas fa-circle" style="color: ${ 'color_status' };"></i>
    <i style="float: right;" class="fas fa-power-off"></i>

    <div class="tabbable-line">
    <!-- begin nav-pills-panel -->
    <ul class="nav nav-pills-panel">
      <li class="nav-items">
        <a href="#general" data-toggle="tab" class="nav-link-panel active">
          <span>General</span>
        </a>
      </li>
    </ul>
    </div>

    <hr>

    <span class="br">Número de Servicio: ${ 'service_number_html' }</span>
    <span class="br">descripción:<i>${ 'description' }</i></span>
    <span class="br">IP:<i>${ 'ip' }</i></span>
    <span class="br">MAC:<i>${ 'mac' }</i></span>
    <span class="br">Tiempo ONLINE:
      <i data-placement="right" data-toggle="tooltip" title='${ 'get_uptime_date' }' data-html="true">
        <i>${ 'get_uptime_delta' }</i>
      </i>
    </span>
    ${ 'generalInfoTemplateCPEUTP' }

  </div>
</div>
</div>`;

var generalInfoTemplateCPEUTP = templater`
<span class="br">Marca:<i>${ 'brand' }</i></span>
<span class="br">Modelo:<i>${ 'model' }</i></span>
<p></p>
<hr style="background: #4488de;">

<span class="br">Nodo UTP: <i>${ 'nodo_utp' }</i> </span>
<span class="br">Firmware: <i>${ 'firmware' }</i></span>
<span class="br">Serial Number: <i>${ 'serial' }</i></span>
<span class="br">Velocidad Bajada en Queue: <i>${ 'down_speed' }</i></span>
<span class="br">Velocidad Subida en Queue: <i>${ 'up_speed' }</i></span>

`;


var pingTemplate = templater`
<form class="form-inline" data-parsley-validate="true" id="ping-form">
<div class="row">
<div class="col-4">
<input id="ip_to_ping" required="true" data-parsley-required="true" type="text" class="form-control"></input>
</div>
<div class="col-6">
<button class="btn btn-warning float-right" type="submit">Hacer ping</button>
</div>
</div>
<div id="response_ping"></div>
</form>`;


function panel_utpcpe_content(el, id) {

  var url = DjangoURL('api_utpcpe_detail',id);
  axios.get(url, header_common)
  .then(function(res){
    if (res.status == 200)
    {
      HDD.set("panel_type",'cpeutp');
      service_num = res.data.service_number;
      if (service_num){

        if (res.data.service_number_verified)
        {
          //var html_service_verified = `<i class="fas fa-check"
          //ondblclick="serviceDetail(${service_num})" style="color: ${Django.color_success}"></i>`;
          var html_service_verified = `<i class="fas fa-check" style="color: ${Django.color_success}"></i>`;
        }else{
          var html_service_verified = `<i class="fas fa-check" 
          onClick="serviceVerifiedUTP(this,'${res.data.uuid}')" style="color: orange"></i>`;          
        }
        
        var service_number_html = `
        <i>${service_num}
        ${html_service_verified}</i>`;
      }else{
        var service_number_html = '<i style="color:red">Sin número agregado</i>';
      }
      
      var color_status = res.data.status ? Django.color_success : Django.color_error;
      var generalinfocontent = {
        nodo_utp : res.data.nodo_utp,
        firmware : res.data.firmware,
      };
      if (res.data.model){
        generalinfocontent.brand = res.data.model.brand;
        generalinfocontent.model = res.data.model.model;
      }
      if (res.data.download_speed) {
        generalinfocontent.down_speed = res.data.download_speed;
      }
      if (res.data.upload_speed) {
        generalinfocontent.up_speed = res.data.up_speed;
      }
      var content = extend(res.data,{
          mac:res.data.mac,
          generalInfoTemplateCPEUTP:generalInfoTemplateCPEUTP(generalinfocontent),
          color_status: color_status,
          img_model:Django.STATIC_URL + 'img/devices/mikrotik_logo.png',
          service_number_html:service_number_html,
          pingTemplate:pingTemplate({})
      });        

      $(el).html(PanelBoxTemplateCPEUTP(content));
      $('#ping-form').parsley();

    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });

}



///** events **///
function closePanelAction() {
  $('.theme-panel').removeClass('active');
  HDD.set('panel_page','0');
}


function serviceVerifiedUTP(self, id){
  var url = DjangoURL('api_utpcpe_detail', id)
  axios.patch(url, {service_number_verified: true}, header_common)
  .then(function(res){
    if (res.status == 200)
    {
      $(self).css('color',Django.color_success);
      _exito('Número de servicio verificado.');
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

function serviceDetail(service){
  var el = document.createElement("div");



  var url = DjangoURL('api_matrix_service_number_detail', service) + '?modal_info=true'
  axios.get(url, {service_number_verified: true}, header_common)
  .then(function(res){
    if (res.status == 200)
    {
      d = res.data
      el.innerHTML = `${d.name}<br>rut: ${d.rut}<br>
      <a href="https://optic.matrix2.cl/contracts/${service}">Ver en matrix</a>`;
      swal("Detalle del cliente", {
        content: el,
      });
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });



}


function ping() {
  $("#response_ping").html('<span class="spinner"></span>');
  var url = DjangoURL('api_ssh_onu_ping', HDD.get('panel_open'))
  axios.put(url, {ip: $("#ip_to_ping").val()}, header_common)
  .then(function(res){
    if (res.status == 200)
    {
      $("#response_ping").html(res.data.message);
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}


Parsley.on('form:submit', function() {
  if(this.element.id == "ping-form") ping();
  return false; // Don't submit form for this demo
});