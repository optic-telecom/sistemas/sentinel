userInSession();
App.setPageTitle('Sentinel - Provisionar UTP paso 1');
var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};

$("#menu_utp").addClass('active');
$("#menu_utp_provisionar_utp").addClass('active')

var MacMask;

$(window).on('load',function(){
  getSelects()
  $('#modal-provisioning_utp').modal('show');
  });

$(document).ready(function (){

  MacMask = new IMask(
    document.getElementById('utp_prov_mac'), {
      mask: '##{:}##{:}##{:}##{:}##{:}##',
      lazy: false,
      definitions: {
        '#': /[0-9,A-F,a-f]/
      }
  });
  
  getSelects()
  $('#modal-provisioning_utp').modal('show');

});  

function getSelects() {
  var url = DjangoURL('api_utp');  
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        $("#utp_prov_node").append(`<option value="${res.data[i].id}">${res.data[i].alias}</option>`);
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}


function provisionUTPperform() {

  var mac_value = MacMask.el.value;

  var selected_typecpe = $("#utp_prov_cpetype option:selected").val();
  if (selected_typecpe == "airrouter"){
    _info("Oops! Por ahora esta funcionalidad para airRouter no está disponible");
    return false
  }

  if (mac_value == "__:__:__:__:__:__" || mac_value.includes("_")){
    _error("Debe proveer una dirección MAC válida para eth1");
    return false;
  } 

  var check_valuesrestart = $("#utp_check_restartvalues").is(':checked') == true ? true : false;
  if (!check_valuesrestart){
    _error("Debe confirmar que el técnico ha reiniciado el equipo a los valores de fábrica");
    return false
  }

  var utp_check_deviceconected = $("#utp_check_deviceconected").is(':checked') == true ? true : false;
  if (!utp_check_deviceconected){
    _error("Debe confirmar que el equipo está conectado en el puerto eth2");
    return false
  }

  swal({
        title: "Comprobado",
        text: 'Los datos son correctos.',
        icon: "success",
        buttons: false
  }).then(function() {
    $('#modal-provisioning_utp').modal('hide');
  });
};