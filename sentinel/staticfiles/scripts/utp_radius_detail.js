userInSession();
App.setPageTitle('Sentinel - Detalle de UTP');
var utp_uuid = getUrlLastParameter();
var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};

$("#menu_utp").addClass('active');
$("#menu_radius_list").addClass('active');


//* events *//
$(document).ready(function (){

  url_profiles = Django.datatable_utp_speeds.replace(':val:',utp_uuid);
  url_logs = Django.datatable_logs_utp.replace(':val:',utp_uuid);
 
  table_profile_utp = $('#table_profile_utp').dataTable({
    "aaSorting":[[0,"desc"]],
      "language": {
      "sLengthMenu":     "Mostrar _MENU_ Profile",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando Profile del _START_ al _END_ de un total de _TOTAL_ Profile",
      "sInfoEmpty":      "Mostrando Profile del 0 al 0 de un total de 0 Profile",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ Profile)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },

      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
    "dom": '<"top">rt<"bottom"iflp><"clear">',
    "processing": true,
    "serverSide": true,
    "searchDelay": 400,
    "ajax": {
        "url": url_profiles,
        "type": "POST",
        "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
    },
    "columnDefs": [
      {
        orderable: true,
        searchable: true,
        targets: [0,1,2,3,4,5,6,7]
      },
      {
        orderable: false,
        searchable: false,
        targets: [8]
      }
    ],    
  });

  table_logs = $('#table_logs').dataTable({
      "aaSorting":[[0,"desc"]],
      "language": {
      "sLengthMenu":     "Mostrar _MENU_ LOG",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando LOG del _START_ al _END_ de un total de _TOTAL_ LOG",
      "sInfoEmpty":      "Mostrando LOG del 0 al 0 de un total de 0 LOG",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ LOG)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending":  ":Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ":Activar para ordenar la columna de manera descendente"
        }
      },
      "processing": true,
      "serverSide": true,
      "searchDelay": 400,
      "ajax": {
          "url": url_logs,
          "type": "POST",
          "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
          "data": function (d) {
            d.filter_type_log = $("#filter_type_log").val();
            d.filter_type_action = $("#filter_type_action").val();
        },
      },
  });

  table_profile_utp.on( 'order.dt', function (e, settings) {
     var order = table_profile_utp.api().order();
     $(".table_profile_utp > thead > tr > th").css('color','black')
     $( ".table_profile_utp > thead > tr > th:eq( "+order[0][0]+" )" )
     .css( "color", "#2271b3" );
  });

  getDetailsUTP();
  setTimeout(function() {getEntitysLogs()}, 150);
  
});/// end ready


$('[data-olt="true"]').on('blur',function(e){
  
  if (e.target.id == 'utp_description'){
    var data = {
      description: $("#utp_description").text()
    }
  }
  if (e.target.id == 'utp_alias'){
    var data = {
      alias: $("#utp_alias").text()
    }
  }
  var url = DjangoURL('api_utp_detail', utp_uuid);
  axios.patch(url, data, conf)
  .then(function(response){
    if (response.status == 200)
    {
      $.gritter.add({
        title: 'UTP actualizada',
        time: 650,
      });
    }
  })//end then
  .catch(function(error) {
    handleErrorAxios(error);
  });
})

function newSpeed() {

  if ($("#name").val() == '' ||
      $("#limitation_name").val() == '' ||
      $("#download_speed").val() == '' ||
      $("#upload_speed").val() == ''){

      swal({
      title: "Error",
        text: 'Verifique los datos ingresados',
        icon: "error",
        buttons: false
      })
      return false;
  }
  var url = DjangoURL('api_utp_radius');
  var radiusid=0;
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        if (utp_uuid == res.data[i].uuid){
          console.log("found")
          radiusid = res.data[i].id;
          break;
        }
      }
      $("#loader_modal").html('<span class="spinner"></span>');
      var url = DjangoURL('api_utp_profile');
      var data = { 
        "name": $("#name").val(),
        "limitation_name": $("#limitation_name").val(),
        "download_speed": $("#download_speed").val(),
        "upload_speed": $("#upload_speed").val(),
        "description": $("#description").val(),
        //"provisioning_available": $("#provisioning_available").val(),
        "radius": radiusid
      }
      axios.post(url, data, conf)
      .then(function(res){
        if (res.status == 201)
        {
          $("#loader_modal").html('');
          $('#modal-create').modal('toggle');
          swal({
          title: "Creada",
            text: 'Velocidad creada exitosamente.',
            icon: "success",
            buttons: false
          }).then(function() {
            updateDatatable();
          });
        }
      
      })//end then
      .catch(function(error) {
        $("#loader_modal").html('');
        handleErrorAxios(error);
      });
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
};

function getEntitysLogs(){
  $("#filter_type_log").empty();
  $("#filter_type_log").append('<option>Tipo</option>');
  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };
  var url = DjangoURL('api_entity_list');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        $("#filter_type_log").append(`<option value="${res.data[i].entity}">${res.data[i].entity}</option>`);
      }
      get_actions_logs();
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

get_actions_logs = () => {
  $("#filter_type_action").empty();
  $("#filter_type_action").append('<option>Aciones</option>');
  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };
  var url = DjangoURL('api_actions_log_list');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.actions.length; i++) {
        var o = res.data.actions
        $("#filter_type_action").append(`<option value="${o[i].id}">${o[i].name}</option>`);
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });  
}



function getDetailsUTP() {
  var url = DjangoURL('api_utp_radius_detail', utp_uuid)
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      $("#utp_id").text(res.data.utp_id);
      $("#utp_alias").text(res.data.alias);
      $(".utp_name").text(res.data.alias);
      $("#utp_ip").text(res.data.ip);
      $("#utp_description").text(res.data.description);
      if(res.data.comment){
        $("#utp_comment").text(res.data.comment);
      } else{
        $("#utp_comment").text("");
      }
      $("#utp_brand").text(res.data.model.brand);
      $("#utp_model").text(res.data.model.model);
      // $("#utp_firmware").text(res.data.firmware);       
      // $("#utp_architecture").text(res.data.architecture);       
      // $("#utp_serial").text(res.data.serial);       
      //$("#utp_uptime").text(res.data.get_uptime_delta.concat(" (").concat(res.data.get_uptime_date)
      //                              .concat(")"));   
      $("#utp_uptime").text(res.data.get_uptime_delta);
      

      //// status UTP
      if (res.data.status)
      {
        $("#utp_estatus").css('color', Django.color_success).text('Online');
      }
      else
      {
        $("#utp_estatus").css('color', Django.color_error).text('Offline');
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}


function deleteSpeed(id,name) {
  $("#modal-delete").modal('toggle');
  $("#namedelete").text(name);
  $("#id_olt_speed").val(id);
}

function deleteSpeedPerform() {
  var id = $("#id_olt_speed").val();
  var url = DjangoURL('api_utp_profile_detail',id);
  // delete_object(id,url,null, function(){
  //   table_speeds.api().ajax.reload(null, false);
  // });  
  var pa = $("#check_delete").is(':checked') == true ? true : false;
  $("#modal-delete").modal('toggle');
  $('#modal-sleep').modal('show');
  if (pa) {
    url = url.concat("?all");
  }

  axios.delete(url, conf)
  .then(function(res){
    if (res.status == 200 || res.status == 204)
    {
      $("#loader_speed").html('');
      $('#modal-sleep').modal('hide');
      swal({
      title: "Borrado",
        text: 'Velocidad UTP eliminada correctamente',
        icon: "success",
        buttons: false
      }).then(()=>{
        updateDatatable('#table_profile_utp');
      });
    } 
  
  })//end then
  .catch(function(error) {
    $('#modal-sleep').modal('hide');
    $("#loader_speed").html('');
    handleErrorAxios(error);
  });
}

function updateProfilelist(){
  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };
  $('#modal-sleep').modal('show');
  var url = DjangoURL('api_update_list_radiusprofile',utp_uuid);
  var data = {"utp_uuid" : utp_uuid};
  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 200)
    {
      $("#loader_modal_speed").html('');
      //$('#modal-create-speed').modal('toggle');
      $('#modal-sleep').modal('hide');
      $.gritter.add({
        title: 'Actualizado',
        text: 'La lista de Profiles fue actualizada correctamente',
        time: 1800,
      });      
      updateDatatable("#table_profile_utp");
    }
  })//end then
  .catch(function(error) {
      $('#modal-sleep').modal('hide');
      handleErrorAxios(error);
  });
}

function speedUtpSystem(id, selected, name){
  $("#modal-speedutp").modal('toggle');
  var url = DjangoURL('api_utp_system_profile');
  $("#name_in_olt_speed").text('');

  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    { 
      $("#speed_system").empty();
      $("#name_in_olt_speed").text(name);
      $("#speed_system").append(`<option>Seleccione Profile</option>`);
      for (var i = 0; i < res.data.length; i++) {
        if (selected){
          $("#speed_system").append(`<option ${res.data[i].id == selected ? "selected":""} value="${res.data[i].id}">${res.data[i].name} </option>`);
        }
        else{
          $("#speed_system").append(`<option value="${res.data[i].id}">${res.data[i].name}</option>`);
        }
      }
      $("#id_olt_speed").val(id);
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });  
}

function speedUtpSystemAsociar() {

  if($("#speed_system option:selected").val() == 'Seleccione Velocidad')
  {
    msg = '<small class="text-danger">Debe selecciona una Velocidad.</small>';
    $("#error_olt_speed").html(msg);
    return false;
  }

  $("#modal-speedutp").modal('toggle');
  var url = DjangoURL('api_utp_profile_detail',$("#id_olt_speed").val());
  var data = {
    system_profile:$("#speed_system option:selected").val()
  }

  axios.patch(url, data, conf)
  .then(function(res){
    if (res.status == 200)
    {
      $("#loader_speed").html('');
      $('#modal-speed').modal('toggle');
      swal({
      title: "Asociada",
        text: 'Velocidad asociada correctamente',
        icon: "success",
        buttons: false
      }).then(()=>{
        updateDatatable('#table_profile_utp');
      });
    }
  
  })//end then
  .catch(function(error) {
    $("#loader_speed").html('');
    handleErrorAxios(error);
  });

}


function speedUtpProvisioning(id, name){
  $("#modal-provisioning").modal('toggle');
  var url = DjangoURL('api_utp_profile_detail',id);
  $("#name_in_olt_speed").text('');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    { 
      $("#speed_system").empty();
      $("#nameprov").text(res.data.name);
      if (res.data.provisioning_available == true) {
        //document.getElementById("provisionamiento_check").checked = true;  
        $("#provisionamiento_check")[0].checked = true;
      } else {
        //document.getElementById("provisionamiento_check").checked = false;
        $("#provisionamiento_check")[0].checked = false;
      }
      $("#id_olt_speed").val(id);
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });  
}

function speedUtpProvisioningChange() {
  var pa = $("#provisionamiento_check").is(':checked') == true ? true : false;

  $("#modal-provisioning").modal('toggle');
  var url = DjangoURL('api_utp_profile_detail',$("#id_olt_speed").val());
  var data = {
    provisioning_available:pa
  }

  axios.patch(url, data, conf)
  .then(function(res){
    if (res.status == 200)
    {
      $("#loader_speed").html('');
      $('#modal-provisioning').modal('toggle');
      swal({
      title: "Actualizado",
        text: 'Profile actualizado correctamente',
        icon: "success",
        buttons: false
      }).then(()=>{
        updateDatatable('#table_profile_utp');
      });
    }
  
  })//end then
  .catch(function(error) {
    $("#loader_speed").html('');
    handleErrorAxios(error);
  });

}