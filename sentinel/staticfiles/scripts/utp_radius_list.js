userInSession();
App.setPageTitle('Sentinel - Lista de RADIUS');

//$("#menu_utp > a ").css('cssText', "color: #FFF !important;");
$("#menu_utp").addClass('active');
$("#menu_radius_list").addClass('active');
var oTable;
var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};


$(document).ready(function (){

  getNodesUTP();

  url_dt = Django.datatable_utp_radius_list;
  oTable = $('#table_utp_radius').dataTable({
      "aaSorting":[[0,"desc"]],
      "language": {
      "sLengthMenu":     "Mostrar _MENU_ RADIUS",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando RADIUS del _START_ al _END_ de un total de _TOTAL_ RADIUS",
      "sInfoEmpty":      "Mostrando RADIUS del 0 al 0 de un total de 0 RADIUS",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ RADIUS)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "sProcessing": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },

      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
      "processing": true,
      "serverSide": true,
      "searchDelay": 400,
      "ajax": {
          "url": url_dt,
          "type": "POST",
          "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
      },
      "columnDefs": [
      {
        orderable: true,
        searchable: true,
        targets: [0,1,2,3,4,5]
      },
      {
        orderable: false,
        searchable: false,
        targets: [6]
      }
      ], 
  });

  oTable.on( 'order.dt', function (e, settings) {

     var order = oTable.api().order();
     $("#table_utp_radius > thead > tr > th").css('color','black')
     $( "#table_utp_radius > thead > tr > th:eq( "+order[0][0]+" )" )
     .css( "color", "#2271b3" );

  });

  var invalidClassName = 'invalid';
  var inputs = document.querySelectorAll('input','textarea');
  inputs.forEach(function (input) {
    // Each time the user types or submits, this will
    // check validity, and set a custom message if invalid.
    function checkValidity () {
      const message = input.validity.valid
        ? null
        : getCustomMessage(input.type, input.validity)
      input.setCustomValidity("")
      if (message!=null){
        input.classList.add(invalidClassName)
      }else{
        input.classList.remove(invalidClassName)
      }
      
    }
    input.addEventListener('input', checkValidity)
    input.addEventListener('invalid', checkValidity)

  })

  // Remove error messages when modal is closed
  $('#modal-create').on('hidden.bs.modal', function () {
    inputs.forEach(function (input) {
      input.classList.remove(invalidClassName)
    })
  })

});  

getNodesUTP = () => {  
  var url = DjangoURL('api_node_list');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        $("#nodes_utp").append(`<option value="${res.data[i].id}">${res.data[i].name}</option>`);
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}

Parsley.on('form:init', function() {
  /// get nodes
  /// getNodesUTP();
});

/**
 * Convert select to array with values
 */    
function serealizeSelects (select)
{
    var array = [];
    select.each(function(){ array.push($(this).val()) });
    return array;
}


function newRadiusUTP() {

  if ($("#ip").val() == '' ||
    $("#alias").val() == '' ||
    $("#user").val() == '' ||
    $("#password").val() == ''){
    swal({
      title: "Error",
        text: "Por favor verifique los datos ingresados",
        icon: "error",
        buttons: false
      });
    var inputs = document.querySelectorAll('input');
    inputs.forEach(function (input) {
      if (input.type!="search"){
        input.classList.add('invalid');
        input.setCustomValidity("Campo requerido");
      }
    });
    return false;
  } 
  $('#modal-create').modal('hide');
  $('#modal-create-sleep').modal('show');
  var url = Django.MIKROTIK_URL + '/api/v1/radius/'
  var data = { 
    "ip": $("#ip").val(),
    "alias": $("#alias").val(),
    //"description": $("#description").val(),
    "description": "NaN",
    "user": $("#user").val(),
    "password": $("#password").val(),    
    // "nodes_utp": serealizeSelects($('#nodes_utp'))
  }

  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 201)
    {
      $('#modal-create-sleep').modal('hide');
      swal({
      title: "Creado",
        text: 'RADIUS Creada exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        var inputs = document.querySelectorAll('input');
        inputs.forEach(function (input) {
          if (input.type!="search"){
            input.classList.remove('invalid');
            input.setCustomValidity("");
          }
        });
        updateDatatable();
      });
    }
  
  })//end then
  .catch(function(error) {
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }else{
        _error("Por favor verifique los datos ingresados.").then(function(){
          $('#modal-create-sleep').modal('hide');
        });
      }
    }

    if (error.response.status == 500)
    {

      _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){
        $('#modal-create-sleep').modal('hide');
      });
      
    }     
     
    //handleErrorAxios(error);
  });
  
};


delete_utpradius = (id) => {
  var url = Django.MIKROTIK_URL + '/api/v1/radius/' + id + '/';
  delete_object(null, 'radius', url, null, function(){
    oTable.api().ajax.reload(null, false);
  });
}


taskUpdateUTP = (utp_uuid) => {
  asynchronousTask({
    task:'update_utp',
    utp:utp_uuid
  }, function(){
    updateDatatable("#table_utp");
  });
}

$(document).on('keydown', function(event) {  
  if(event.which == 118) { // F7

    var url = DjangoURL('api_update_uptime_utps');
    axios.post(url, conf)
    .then(function(res){
      if (res.status == 201)
      {
        oTable.api().ajax.reload(null, false);
      }
      if (res.status == 401)
      {
        handleUnauthorizedAxios();
      }
    })//end then
    .catch(function(error) {
        handleErrorAxios(error);
    });


  }
});

$(window).keydown(function(e){
  //console.log(e)
});

keydownTaskUTP = (e) => {
  if (e.shiftKey && e.keyCode === 70){
    console.log(e)
  }
}

ayuda = () => {
  alert('Pagina de ayuda de radius')
}