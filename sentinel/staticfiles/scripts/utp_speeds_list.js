userInSession();
App.setPageTitle('Sentinel - Lista de velocidades');
var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};

$("#menu_configuraciones").addClass('active');
$("#menu_velocidades").addClass('active');
$("#menu_utp_speeds").addClass('active');

const customMessages = {
    valueMissing:    'Campo obligatorio',       // `required` attr
  }

function getCustomMessage (type, validity) {
    if (validity.typeMismatch) {
      return customMessages[`${type}Mismatch`]
    } else {
      for (const invalidKey in customMessages) {
        if (validity[invalidKey]) {
          return customMessages[invalidKey]
        }
      }
    }
  }

$(document).ready(function (){
  url_dt = Django.datatable_utp_system_speeds;
  oTable = $('#table_speeds').dataTable({
        "aaSorting":[[0,"desc"]],
        "language": {
        "sLengthMenu":     "Mostrar _MENU_ UTP System Profile",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando UTP System Profile del _START_ al _END_ de un total de _TOTAL_ UTP System Profile",
        "sInfoEmpty":      "Mostrando UTP System Profile del 0 al 0 de un total de 0 UTP System Profile",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ UTP System Profile)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "sProcessing": "Cargando...",
        "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
        },

        "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      },
      "processing": true,
      "serverSide": true,
      "ajax": {
          "url": url_dt,
          "type": "POST",
          "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
      },
      "columnDefs": [
      {
        orderable: true,
        searchable: true,
        targets: [0,1,2,3,4,5,6]
      },
      {
        orderable: false,
        searchable: false,
        targets: [7]
      }
      ], 
   });
  var invalidClassName = 'invalid';
  var inputs = document.querySelectorAll('input','textarea');
  inputs.forEach(function (input) {
    // Each time the user types or submits, this will
    // check validity, and set a custom message if invalid.
    function checkValidity () {
      const message = input.validity.valid
        ? null
        : getCustomMessage(input.type, input.validity)
      input.setCustomValidity("")
      if (message!=null){
        input.classList.add(invalidClassName)
      }else{
        input.classList.remove(invalidClassName)
      }
      
    }
    input.addEventListener('input', checkValidity)
    input.addEventListener('invalid', checkValidity)

  })

  // Remove error messages when modal is closed
  $('#modal-create').on('hidden.bs.modal', function () {
    inputs.forEach(function (input) {
      input.classList.remove(invalidClassName)
    })
  })

});  

function newSpeed() {
  var url = DjangoURL('api_utp_system_profile');
  var iserror = false;

  if ($("#name").val() == '' ||
    $("#limitation_name").val() == '' ||
    $("#download_speed").val() == '' ||
    $("#upload_speed").val() == ''){
    swal({
      title: "Error",
        text: "Por favor verifique los datos ingresados",
        icon: "error",
        buttons: false
      });
    var inputs = document.querySelectorAll('input');
    inputs.forEach(function (input) {
      if (input.id != "provisionamiento_check" && input.id != "id_olt_speed"
          && input.id!="search_top_header"){
        input.classList.add('invalid');
        var msg = getCustomMessage(input.type, input.validity);
        input.setCustomValidity(msg);
      }
    });
    return false;
  } 

  $("#loader_modal").html('<span class="spinner"></span>');
  var data = { 
    "name": $("#name").val(),
    "limitation_name": $("#limitation_name").val(),
    "download_speed": $("#download_speed").val(),
    "upload_speed": $("#upload_speed").val(),
    "description": description,
    // "provisioning_available": $("#provisioning_available").val(),
  }
  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 201)
    {
      $("#loader_modal").html('');
      $('#modal-create').modal('toggle');
      swal({
      title: "Creada",
        text: 'Velocidad creada exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        // Unmark required fields
        var inputs = document.querySelectorAll('input');
        inputs.forEach(function (input) {
          if (input.id != "provisionamiento_check" && input.id != "id_olt_speed"
              && input.id!="search_top_header"){
            input.classList.remove('invalid');
            input.setCustomValidity("");
          }
        });
        updateDatatable();
      });
    }
  
  })//end then
  .catch(function(error) {
    $("#loader_modal").html('');
    if (error.response.status == 400)
    {
      console.log(error.response.data)
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){
        });
      }else{
        _error("Por favor verifique los datos ingresados.").then(function(){});
      }
    }

    if (error.response.status == 500)
    {

      _error("Ocurrio un error en el servidor, contacte al administrador.").then(function(){});
      
    }     
     
    //handleErrorAxios(error);
  });
};

function speedUtpProvisioning(id, name){
  $("#modal-provisioning").modal('toggle');
  var url = DjangoURL('api_utp_system_profile_detail',id);
  $("#name_in_olt_speed").text('');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    { 
      $("#speed_system").empty();
      $("#nameprov").text(res.data.name);
      if (res.data.provisioning_available == true) {
        //document.getElementById("provisionamiento_check").checked = true;  
        $("#provisionamiento_check")[0].checked = true;
      } else {
        //document.getElementById("provisionamiento_check").checked = false;
        $("#provisionamiento_check")[0].checked = false;
      }
      $("#id_olt_speed").val(id);
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });  
}

function speedUtpProvisioningChange() {
  var pa = $("#provisionamiento_check").is(':checked') == true ? true : false;

  $("#modal-provisioning").modal('toggle');
  var url = DjangoURL('api_utp_system_profile_detail',$("#id_olt_speed").val());
  var data = {
    provisioning_available:pa
  }

  axios.patch(url, data, conf)
  .then(function(res){
    if (res.status == 200)
    {
      $("#loader_speed").html('');
      $('#modal-provisioning').modal('toggle');
      swal({
      title: "Actualizado",
        text: 'Profile actualizado correctamente',
        icon: "success",
        buttons: false
      }).then(()=>{
        updateDatatable('#table_speeds');
      });
    }
  
  })//end then
  .catch(function(error) {
    $("#loader_speed").html('');
    handleErrorAxios(error);
  });

}


deleteSpeed = (id) => {
  var message = '¿Desea eliminar este elemento?';
  var url = DjangoURL('api_utp_system_profile_detail',id);

  swal({
      title: "Eliminar",
      text: message,
      icon: "warning",
      buttons: [
        'No',
        'Si'
      ],
      dangerMode: true,
    }).then(function(isConfirm) {
      if (isConfirm) {
        axios.delete(url, conf)
        .then(function(response){
          if (response.status == 204)
          {
            _exito('System Profile borrado exitosamente.');
            updateDatatable('#table_speeds');
          }
        })
        .catch(function(error) {
            if (error.response.data.error){
              _error(error.response.data.error).then(function() {});
            }          
        });
      }
    })  
}