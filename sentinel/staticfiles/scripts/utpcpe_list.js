userInSession();
App.setPageTitle('Sentinel - Lista de CPEs de UTP');

var olt_uuid = getParameterUrl('filter');
var conf = {
  headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
};

$(document).ready(function (){

  if (olt_uuid)
  {
    url_dt = Django.datatable_cpes_utp.replace(':val:',utp_uuid);
    $("#menu_utp_cpe").addClass('active'); 
    $("#menu_utp").addClass('active');
  }
  else
  {
    url_dt = Django.datatable_cpes_utp_list;
    $("#menu_cpeutp_list").addClass('active');
    $("#menu_cpeutp").addClass('active');
    $("#menu_cpes").addClass('active');    
  }

  oTable = $('#table_cpe_utp').dataTable({
    "aaSorting":[[0,"desc"]],
      "language": {
      "sLengthMenu":     "Mostrar _MENU_ CPEUTP",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando CPEUTP del _START_ al _END_ de un total de _TOTAL_ CPEUTP",
      "sInfoEmpty":      "Mostrando CPEUTP del 0 al 0 de un total de 0 CPEUTP",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ CPEUTP)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "sProcessing": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },

      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
    "dom": '<"top">rt<"bottom"iflp><"clear">',
    "processing": true,
    "serverSide": true,
    "searchDelay": 400,
    "ajax": {
        "url": url_dt,
        "type": "POST",
        "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
        data: function (d) {
            d.filter_status = $("#filter_status").val();
            d.filter_model = $("#filter_model").val();
            d.last_seen_filter = $("#last_seen_filter").val();
        },
    },
    "columnDefs": [
      {
        orderable: true,
        searchable: true,
        targets: [0,1,2,3,4,5,6,7,9]
      },
      {
        orderable: true,
        searchable: true,
        targets: [8]
      }
    ],    
  });

  oTable.on( 'order.dt', function (e, settings) {

     var order = oTable.api().order();
     $(".table > thead > tr > th").css('color','black')
     $( ".table > thead > tr > th:eq( "+order[0][0]+" )" )
     .css( "color", "#2271b3" );

  });

  // getModelsONU();
  // getOLTs();

  $('#filter_fsp').select2({
  });
  $("#select2-filter_fsp-container").css('width','220px');
  

  $('#filter_status').change(function(){
    oTable.api().ajax.reload(null, false);
  })

  $('#filter_model').change(function(){
    oTable.api().ajax.reload(null, false);
  })

  $('#filter_olt').change(function(){
    var selectedOlt = $(this).children("option:selected").val();
    getFSP(selectedOlt);
    oTable.api().ajax.reload(null, false);
  })

  $('#filter_fsp').change(function(){
    oTable.api().ajax.reload(null, false);
  })
  

});  


function getModelsONU() {
  $("#filter_model").empty();
  $("#filter_model").append('<option>Modelo</option>');
  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };
  var url = DjangoURL('api_model_device_list');
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        $("#filter_model").append(`<option value="${res.data[i].id}">${res.data[i].model}</option>`);
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}


function getOLTs() {
  $("#filter_olt").empty();
  $("#filter_olt").append('<option value="0">Seleccione OLT &nbsp;&nbsp;&nbsp;&nbsp;</option>');
  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };
  var url =  DjangoURL('api_utp_list') + '?fields=alias,uuid';
  axios.get(url, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        $("#filter_olt").append(`<option value="${res.data[i].uuid}">${res.data[i].alias}</option>`);
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });
}


function editDescription(button1) {
  $('#modal-edit-description').modal('hide');
  // Obtenemos los datos del cpe a modificar
  let newDescription = document.querySelector('#edit-description-area').value 
  let currentID = document.querySelector('#edit-id').innerText
  console.log(newDescription)
  console.log(currentID)
  let url = DjangoURL('api_utpcpe') + currentID  + '/modify_description/'
  console.log(url)
  let data = {
    "id": currentID,
    "description": newDescription
  }
  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 200)
    {      
      swal({
      title: "Actualizado",
        text: 'Descripción del CPE actualizada correctamente',
        icon: "success",
        buttons: false
      }).then(function() {
          updateDatatable("#table_cpes_utp");
        });
     }
     
  })
  .catch(function(error) {
    console.log(error.response);
    if (error.response.status == 400)
    {
      if (error.response.data.error){
        _error(""+ error.response.data.error).then(function(){
          $('#modal-sleep').modal('hide');
        });
      }else{
        _error("Por favor verifique los datos ingresados.").then(function(){
          $('#modal-sleep').modal('hide');
        });
      }
    }
})
}

function activateMatrixService(matrix_id){

  $('modal-sleep').modal('show')

  let matrixUrl = "https://optic.matrix2.cl/api/v1/services/" + matrix_id + '/' 
  console.log(matrix_id)

  let data = {
    'status': 1
  }

  axios.patch(matrixUrl, data, {
          headers: {'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'}
    }).then(function(res){
    if (res.status === 200){
      $('modal-sleep').modal('hide')
      swal({
      title: "Activado",
        text: 'Servicio activado en Matrix correctamente',
        icon: "success",
        buttons: false
      })
    }
  }).catch(function(error) {
      handleErrorAxios(error);
  })
}

function corteDeServicio(service_number){

  $('modal-sleep').modal('show')

  let corteUrl = '/api/v1/cutting_replacement/'
  let data = {
    'service_number': service_number,
    'type': 'corte'
  }
  axios.post(corteUrl, data, {
          headers: {'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'}
    }).then(function(res){
    if (res.status === 200){
      $('modal-sleep').modal('hide')
      swal({
      title: "Cortado",
        text: 'El Servicio será cortado dentro de unos minutos',
        icon: "success",
        buttons: false
      })
    }
  }).catch(function(error) {
      handleErrorAxios(error);
  })
}

function reposicionDeServicio(service_number){

  $('modal-sleep').modal('show')

  let corteUrl = '/api/v1/cutting_replacement/'
  let data = {
    'service_number': service_number,
    'type': 'reposicion'
  }
  axios.post(corteUrl, data, {
          headers: {'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'}
    }).then(function(res){
    if (res.status === 200){
      $('modal-sleep').modal('hide')
      swal({
      title: "Conectado",
        text: 'El Servicio será conectado dentro de unos minutos',
        icon: "success",
        buttons: false
      })
    }
  }).catch(function(error) {
      handleErrorAxios(error);
  })
}


function setPrimaryEquipment(service_number, device_ip){

  $('#modal-sleep').modal('show')

 let primaryEquipmentUrl = DjangoURL('set_primary_equipment', service_number)
 let data = {
    'device_ip': device_ip,
    'device_type': 'UTP'
 }
 axios.post(primaryEquipmentUrl, data, conf)
 .then(function(res){
  if (res.status === 200){
      $('#modal-sleep').modal('hide')
      swal({
      title: "Asociado",
        text: 'El Equipo seleccionado fue asociado como primario correctamente',
        icon: "success",
        buttons: false
      })
    }
 }).catch(function(error) {
      handleErrorAxios(error);
  })
}