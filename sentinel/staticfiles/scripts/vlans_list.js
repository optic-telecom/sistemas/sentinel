userInSession();
App.setPageTitle('Sentinel - Lista de VLANs sistema');

$("#menu_configuraciones").addClass('active');
$("#menu_vlans").addClass('active');

$(document).ready(function (){
  getSelectsVlans();
  url_dt = Django.datatable_vlans;
  oTable = $('#table_vlans').dataTable({
        "aaSorting":[[0,"desc"]],
        "language": {
        "sLengthMenu":     "Mostrar _MENU_ Vlan",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando Vlan del _START_ al _END_ de un total de _TOTAL_ Vlan",
        "sInfoEmpty":      "Mostrando Vlan del 0 al 0 de un total de 0 Vlan",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ Vlan)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "sProcessing": "Cargando...",
        "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
        },

        "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      },
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": url_dt,
            "type": "POST",
            "headers": { 'Authorization':  HDD.get(Django.name_jwt) },
        }
   });
});  


function getSelectsVlans() {
  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };

  var url1 = DjangoURL('api_operator_list');
  axios.get(url1, conf)
  .then(function(res){
    if (res.status == 200)
    {
      $("#operator").append(`<option>Seleccione operador</option>`);
      for (var i = 0; i < res.data.length; i++) {
        $("#operator").append(`<option value="${res.data[i].id}">${res.data[i].name}</option>`);
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });


  var url2 = DjangoURL('api_type_vlan_list');
  axios.get(url2, conf)
  .then(function(res){
    if (res.status == 200)
    {
      for (var i = 0; i < res.data.length; i++) {
        $("#vlan_type").append(`<option value="${res.data[i].id}">${res.data[i].name}</option>`);
      }
    }
    if (res.status == 401)
    {
      handleUnauthorizedAxios();
    }
  })//end then
  .catch(function(error) {
      handleErrorAxios(error);
  });

  


}



function newVLAN() {
  $("#loader_modal").html('<span class="spinner"></span>');
  var conf = {
    headers: { Authorization: 'jwt '+ HDD.get(Django.name_jwt)}
  };
  var url = DjangoURL('api_system_vlan');
  var data = { 
    "vlan_id": $("#vlan_id").val(),
    "vlan_type": $("#vlan_type option:selected").val(),
    "description": $("#description").val(),
    "operator": $("#operator option:selected").val()
  }

  axios.post(url, data, conf)
  .then(function(res){
    if (res.status == 201)
    {
      $("#loader_modal").html('');
      $('#modal-create').modal('toggle');
      swal({
      title: "Creada",
        text: 'VLAn creada exitosamente.',
        icon: "success",
        buttons: false
      }).then(function() {
        updateDatatable();
      });
    }
  
  })//end then
  .catch(function(error) {
    $("#loader_modal").html('');
    handleErrorAxios(error);
  });
};