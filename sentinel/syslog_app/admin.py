from django.contrib import admin
from .models import SysLog
from .forms import SyslogForm
# Register your models here.


@admin.register(SysLog)
class SysLogAdmin(admin.ModelAdmin):
    form = SyslogForm
    list_display = ('created','user_ip','username','log_type','message','origin','identificador')
    readonly_fields = ('created',)
    list_filter = ['username','log_type','origin']

    fieldsets = (
        ('Origen', {
            'fields': (
                'user_ip',
                'origin',
                'identificador'
            ),
        }),
        ('Detalles del syslog',{
            'classes':('collapse',),
            'fields':(
                'created',
                'username',
                'log_type',
                'message'

            )
        }),

    )

    