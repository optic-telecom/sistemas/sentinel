from django.apps import AppConfig


class SyslogAppConfig(AppConfig):
    name = 'syslog_app'
    verbose_name = "Syslog"
