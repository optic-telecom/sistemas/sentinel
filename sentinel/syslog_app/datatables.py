import datetime 
from dateutil.relativedelta import relativedelta
from django.utils import timezone
from django.db.models import Q#, Value, Sum, F
#from django.db.models.functions import Concat
#from django.db.models.fields import IntegerField

from common.utils import timedelta_format
from common.mixins import DataTable, ButtonDataTable

from .models import SysLog
from devices.models import OLT

class SysLogDatatable(DataTable):
    model = SysLog
    # id_current = 0
    columns = ['created','hora','user_ip','username','log_type','origin','identificador','message','botones']
    order_columns = ['created','hora','user_ip','username','log_type','origin','identificador','message']
    max_display_length = 2000
    
    def display_hora(self, obj):
        return obj.created.strftime("%H:%M:%S")
    
    def display_created(self, obj):
        return obj.created.strftime("%d-%m-%Y")
    

    def filtros(self,qs):
        filter_created_sys = self._querydict.get('advance_daterange', None)

        filter_ip_sys = self._querydict.get('filter_ip_sys', None)
        filter_user_sys = self._querydict.get('filter_user_sys', None)
        filter_type_syslog_sys = self._querydict.get('filter_type_syslog_sys', None)
        filter_origin_syslog_sys = self._querydict.get('filter_origin_syslog_sys', None)
        filter_message_sys = self._querydict.get('filter_message_sys', None)
        
        if filter_ip_sys:
            qs =  qs.filter(user_ip=filter_ip_sys)
        elif filter_user_sys:
            qs =  qs.filter(username=filter_user_sys)
        elif filter_type_syslog_sys:
            qs =  qs.filter(log_type=filter_type_syslog_sys)
        elif filter_origin_syslog_sys:
            qs =  qs.filter(origin=filter_origin_syslog_sys)
        elif filter_message_sys:
            qs =  qs.filter(message=filter_message_sys)
        elif filter_created_sys:
            startDate = filter_created_sys.split(',')[0] + ' 00:00:00'
            endDate = filter_created_sys.split(',')[1] + ' 23:59:59'
            
            qs =  qs.filter(created__range=[startDate,endDate])
            
        return qs
        

    def get_initial_queryset(self):
        
        qs = self.model.objects.all()
        try:
            """
            Aquí aplicamos el filtro por UUID dentro de las OLT'S
            se pudo haber aplicado un .get() pero necesitamos una lista 
            por esa razón el .filter()
            """
            olt = OLT.objects.filter(uuid=self.uuid)
            """
            Aqui filtramos solo OLT's o sea origin=2 y buscamos los que 
            tengan identificadores dentro de la lista de olt
            """
            qs = qs.filter(status_field=True,origin=2,identificador__in=olt)
            return self.filtros(qs)
        except:
            return qs.filter(status_field=True)     
    
    def btn_delete(self, row):
        t = """<a type="button" class="btn btn-danger btn-xs" onclick="{fn}">
        <img style="width:18px;" src="/static/img/papelera.png"></a>"""
        return t.format(fn="delete_log('%s')" % str(row.id))

    def render_column(self, row, column):
        if column == 'botones':
            btn = ButtonDataTable()                
            btn.list_btn.append(self.btn_delete(row))
            return btn()
        if column == 'origin':
            ORIGEN_LIST = ((0,'NO ENCONTRADO'),
               (1,'ONU'),
               (2,'OLT'),
               (3,'MIKROTIK'),
               (4,'CPE-UTP'),
               (5,'RADIUS'),)
            return ORIGEN_LIST[row.origin][1]
        elif column == 'identificador':
            return row.identificador if row.identificador else 'Desconocido'
        else:
           return super().render_column(row, column)

    def filter_queryset(self, qs):
        qs = self.filtros(qs)            
        #Mejorar filtro
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = qs.filter(
                Q(created__icontains=search) |
                Q(user_ip__icontains=search) |
                Q(username__icontains=search) |
                Q(log_type__icontains=search) |
                Q(message__icontains=search) 
            )

        #qs = self.get_initial_queryset()
        return qs
