from django import forms 
from .models import SysLog

class SyslogForm(forms.ModelForm):
    class Meta:
        model = SysLog
        fields = [
            'user_ip',
            'username',
            'log_type',
            'message',
            'origin',
            'identificador'
        ]