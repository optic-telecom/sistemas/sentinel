
from common.command import BaseCommand

from utp.models import Mikrotik,CpeUtp,Radius,AddressList
from devices.models import IPTable,OLT,ONU
from syslog_app.models import ORIGEN_LIST, SysLog

from datetime import datetime


import socketserver

LOG_FILE = 'sentinel.log'
HOST, PORT = "0.0.0.0", 2514

model_origin_list = {
    'NO ENCONTRADO':0,
    'ONU':1,
    'OLT':2,
    'Mikrotik':3,
    'CpeUtp':4,
    'Radius':5
    
}


class SyslogUDPHandler(socketserver.BaseRequestHandler):

    def search_address_list(self,ip):
        return AddressList.objects.filter(address__ip=ip,status_field=True).exists()
    
    def search_iptable(self,ip):
        """
        Búsqueda en IPTable
        """
        return IPTable.objects.filter(ip=ip).exists()
    
    def add_iptable(self,obj):
        """
        Solo para salvar en iptables
        """
        iptable = IPTable(
                    ip=obj.ip,
                    host="",
                    menu="",
                    title="",
                )
        iptable.save()
        
    def add_syslog(self,origin,obj,username,log_type,msj,iptable_instance=None):
        if iptable_instance:
            instancia_iptable = iptable_instance
        
        syslog = SysLog(user_ip=instancia_iptable,
                        username=username,#POR DEFINIR USUARIO
                        log_type=log_type,
                        message=msj,#POR DEFINIR MENSAJE
                        origin=origin,
                        identificador=obj.pk
                        )
        syslog.save()
               
    def get_syslog(self,ip,username,log_type,msj=''):
        """
        Cuando llega se debe salvar el syslog, preguntando si 
        esa ip está en AddressList y luego en IPTable,
        existe una variable centinela que cuida que no se duplique el
        registro, incializa en false y cambia a true una vez se guarda un syslog
        evitando
        """
        centinela = False
        if self.search_address_list(ip):
            origin = 3
            # filtro = {'node__ip__ip':ip}
            filtro = {'address__ip':ip}
            obj = AddressList.objects.filter(**filtro).last()

            iptable_instance = IPTable.objects.get(ip=ip)
            SysLog.objects.create(user_ip=iptable_instance,
                        username=username,
                        log_type=log_type,
                        message=msj,
                        origin=origin,
                        identificador=obj.node.pk
                        )
            print('Address List')

            centinela = True   

        else:
            modelos = [Mikrotik,CpeUtp,OLT,ONU,Radius]
            filtro = {'ip__ip':ip}        

            listado = list()
            for modelo in modelos:
                origin = model_origin_list[modelo.__name__]
                obj = getattr(modelo,'objects').filter(**filtro)

                if obj.exists():
                    obj = obj.last()

                    listado.append({'modelo':modelo.__name__,
                                'obj':obj,
                                'created':obj.created,
                                'pk':obj.pk})

                    iptable_instance = IPTable.objects.get(ip=ip)
                    identificador = obj.pk

                    
            if listado:
                fechas = list(map(lambda x: datetime.timestamp(x['created']),listado))
                ultima_fecha = max(fechas)
                objeto = list(filter(lambda x: datetime.timestamp(x['created'])==ultima_fecha,listado))[0]
                
                if objeto['modelo'] == 'Mikrotik':
                    identificador = objeto['obj'].node.pk 
                else:
                    identificador = obj['pk']

                print(modelo.__name__)

                SysLog.objects.create(
                    user_ip=iptable_instance,
                    username=username,
                    log_type=log_type,
                    message=msj,
                    origin=model_origin_list[objeto['modelo']],
                    identificador=identificador
                    )
                centinela = True

        if not centinela:

            if not self.search_iptable(ip):
                iptable_instance = IPTable.objects.create(ip=ip)
            else:
                iptable_instance = IPTable.objects.get(ip=ip)

            origin = ORIGEN_LIST[0][0]
            identificador = 0

            SysLog.objects.create(
                user_ip=iptable_instance,
                username=username,
                log_type=log_type,
                message=msj,
                origin=origin,
                identificador=identificador
            )



    def handle(self):
        data = bytes.decode(self.request[0].strip())
        socket = self.request[1]
        print( "%s : " % self.client_address[0], str(data))
        ip = self.client_address[0]
        try:
            msj = str(data.split(',')[2])
            # msj = str(data.split(','))
            username = str(data.split(',')[0])
            log_type = str(data.split(',')[1])


        except Exception as e:
            print(str(e))
            username=ORIGEN_LIST[0][0]
            log_type='INFO'
            msj=''

        self.get_syslog(ip,username,log_type,msj)
  
    
  

class Command(BaseCommand):

    help = 'Para Guardar en la base de datos los syslogs generados por los distintos dispositivos'
    
    def handle(self, *args, **options):
        try:
            server = socketserver.UDPServer((HOST,PORT), SyslogUDPHandler)
            server.serve_forever(poll_interval=0.5)

        except (IOError, SystemExit):
            raise
        except KeyboardInterrupt:
            print ("Crtl+C Pressed. Shutting down.")
