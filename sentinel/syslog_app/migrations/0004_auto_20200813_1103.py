# Generated by Django 2.1.3 on 2020-08-13 11:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('syslog_app', '0003_auto_20200428_1836'),
    ]

    operations = [
        migrations.AlterField(
            model_name='historicalsyslog',
            name='id_data',
            field=models.UUIDField(blank=True, db_index=True, editable='True', null=True),
        ),
        migrations.AlterField(
            model_name='syslog',
            name='id_data',
            field=models.UUIDField(blank=True, db_index=True, editable='True', null=True),
        ),
    ]
