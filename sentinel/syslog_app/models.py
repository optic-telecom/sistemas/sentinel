from django.db import models
from common.models import BaseModel
# Create your models here.

ORIGEN_LIST = ((0,'NO ENCONTRADO'),
               (1,'ONU'),
               (2,'OLT'),
               (3,'MIKROTIK'),
               (4,'CPE-UTP'),
               (5,'RADIUS'),)


class SysLog(BaseModel):
    """
    Modelo que almacena los Logs de cada dispositivo
    manejado por sentinel
    """
    user_ip = models.ForeignKey('devices.IPTable',on_delete=models.PROTECT,verbose_name="IP")
    username = models.CharField(max_length=255,verbose_name="Usuario o dirección MAC",null=True, blank=True)
    log_type = models.CharField(max_length=255,verbose_name="Tipo de Log",null=True, blank=True)
    message = models.TextField(verbose_name="Mensaje",blank=True,null=True)
    
    origin = models.IntegerField(
                              choices=ORIGEN_LIST,
                              default=ORIGEN_LIST[0][0],
                              verbose_name="origen",
                              blank=True,null=True)
    identificador = models.IntegerField(blank=True,null=True,verbose_name="pk de origen")
    

    def __str__(self):
        return f'{self.user_ip}'
    
    class Meta:
        ordering = ["-created"]

