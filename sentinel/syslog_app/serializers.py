from rest_framework import serializers as sz

class SysLogSelect2Sz(sz.Serializer):
    id = sz.CharField(max_length=30)
    text = sz.CharField(max_length=255)