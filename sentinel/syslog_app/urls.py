from rest_framework.routers import DefaultRouter
from .views import *
from django.urls import path
# from .logfile.syslog_search import SearchSysLog

router = DefaultRouter()
# router.register(r'')

urlpatterns = [

    path('api/v1/s2-syslog/<selector>/<dispositivo>/<uuid>', 
        Select2ViewSet.as_view({'get':'get_select2'}), 
        name="select2"),

    path('api/v1/s2-syslog/created',SelectSyslog.as_view(selector='created'),name="select_created"),
        
    # path('api/v1/syslog-search', SearchSysLog.as_view(),name="search")
]