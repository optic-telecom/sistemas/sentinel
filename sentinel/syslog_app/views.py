from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet, ViewSet
from rest_framework.response import Response
from rest_framework.views import APIView
# Create your views here.
from .serializers import SysLogSelect2Sz
from .models import SysLog

from devices.models import OLT
from utp.models import Mikrotik

class Select2ViewSet(ViewSet):

    queryset = SysLog.objects.order_by('-id')
    serializer_class = SysLogSelect2Sz


    def get_select2(self,request,*args,**kwargs):


        ORIGEN_LIST = ((0,'NO ENCONTRADO'),
               (1,'ONU'),
               (2,'OLT'),
               (3,'MIKROTIK'),
               (4,'CPE-UTP'),
               (5,'RADIUS'),)
        
        no_repeat = list()
        lista = list()
        selector = kwargs['selector']
        dispositivo = kwargs['dispositivo']
        uuid = kwargs['uuid']

        if dispositivo == 'olt':
            olt = OLT.objects.get(uuid=uuid)
            filtro = {'status_field':True,'origin':2,'identificador':olt.pk}
        elif dispositivo == 'mikrotik':
            mikrotik = Mikrotik.objects.get(uuid=uuid)
            filtro = {'status_field':True,'origin':3,'identificador':mikrotik.pk}

        for qs in self.queryset.filter(**filtro):
            text = getattr(qs,selector)
            if selector == 'origin':
                text = ORIGEN_LIST[qs.origin][1]
            if not text in no_repeat:
                no_repeat.append(text)
                if selector == 'created':
                    pk = qs.created.strftime("%d-%m-%Y")
                elif selector == 'origin':
                    pk = qs.origin 
                else:
                    pk = text
                lista.append({
                    'id':pk,
                    'text':text
                })

        serializer = self.serializer_class(lista,many=True)

        return Response(serializer.data)




class SelectSyslog(APIView):
    """
    {} Esta api retorna un objeto para ser utilizado por los
    selectores del datatable de la sección syslog en los detalles 
    de las OLT
    """
    selector = None
    tipe_syslog = None
    
    def get(self,request,*args,**kwargs):
        
        syslog = SysLog.objects.all()
        
        search = list()
        try:
            for x in syslog:
                search.append(getattr(x,self.selector))
            
            listado = set(search)
            
            ORIGEN_LIST = ((0,'NO ENCONTRADO'),
               (1,'ONU'),
               (2,'OLT'),
               (3,'MIKROTIK'),
               (4,'CPE-UTP'),
               (5,'RADIUS'),)
            
            
            if self.selector == 'origin':
                output = [{'id':x,'text':ORIGEN_LIST[x][1]} for x in listado]
            elif self.selector != 'user_ip' and self.selector != 'created':
                output = [{'id':x,'text':x.lower()} for x in listado]
            else:
                if self.selector == 'created':
                    output = [{'id':x.strftime("%d-%m-%Y"),'text':x.strftime("%d-%m-%Y")} for x in listado]
                else:
                    output = [{'id':x,'text':x} for x in listado]
            
            serializer = SysLogSelect2Sz(output,many=True).data
            status = 200
        except Exception as e:
            serializer = list()
            status = 404
        

        return Response(serializer,status=status)


class LoopForModels(APIView):
    """
    
    """
    MODEL_LIST = ['ONU','Mikrotik','Radius']