from django.contrib import admin
from tv.models import StatusVoucher, StatusSTB
from .tv_setupbox import SetupBoxAdmin
from .tv_voucher import VoucherAdmin


admin.site.register(StatusSTB)
admin.site.register(StatusVoucher)