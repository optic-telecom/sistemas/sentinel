from django.contrib import admin
from tv.models import SetupBox

@admin.register(SetupBox)
class SetupBoxAdmin(admin.ModelAdmin):
    list_display = ('id','voucher','operator','model','mac','service','status',
                    'associated_service','associated_voucher')
    list_filter = ('status','operator')
    search_fields = ('service','voucher')