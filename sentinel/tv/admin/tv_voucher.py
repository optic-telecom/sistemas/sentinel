from django.contrib import admin
from tv.models import Voucher
from tv.forms import VoucherForm


@admin.register(Voucher)
class VoucherAdmin(admin.ModelAdmin):
    list_display = ('code','supplier_status','operator')
    list_filter = ('supplier_status','operator')
    search_fields = ('code',)
    form = VoucherForm
