from django.apps import AppConfig


class TvConfig(AppConfig):
    name = 'tv'
    verbose_name = 'Banda Ancha Tv'
