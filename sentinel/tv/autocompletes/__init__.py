from dal import autocomplete
from django.contrib.auth import get_user_model
from rest_framework_jwt.settings import api_settings
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
User = get_user_model()

def validate(token):
    try:
        decode = jwt_decode_handler(token)
        user = User.objects.get(username=decode['username'])
        return True
    except Exception as e:
        print (e)
        return False

    return False

class AutocompleteSelect2(autocomplete.Select2QuerySetView):
    """ Filtrado Select2 Génerico recibe 2 atributos obligatorios 
        model el cual indica el modelo sobre el cual se va realizar 
        la selección y filter_by que indica el nombre del campo
        incluido también el lookup_field que se va usar y por último 
        un second_filter_by que es opcional y recibe un diccionario 
        ej.

        model=Voucher
        filter_by = "code__istarstwith"
    """

    model = None
    filter_by = None
    second_filter_by = {}
    high_obj = True

    def get_queryset(self):
        token = self.request.META['HTTP_AUTHORIZATION'].split(' ')[1]
        if validate(token):

            if self.high_obj:
                qs = self.model.objects.none()
            else:
                qs = self.model.objects.all()
                
            id = self.request.GET.get('id',None)
            
            if id:
                qs = self.model.objects.filter(id__in=[id])

            if self.filter_by and self.q:
                filtro = {self.filter_by:self.q}
                qs = self.model.objects.filter(**filtro)
            if self.second_filter_by:
                qs = qs.filter(**self.second_filter_by)
        else:
            qs = []
        
        return qs

class ListadoAutocomplete(autocomplete.Select2ListView):
    listado = list()

    def get_list(self):
        return self.listado