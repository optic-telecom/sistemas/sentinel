import datetime 
from dateutil.relativedelta import relativedelta
from django.utils import timezone
from django.db.models import Q
from common.utils import timedelta_format
from common.mixins import DataTable, ButtonDataTable

from tv.models import SetupBox


class SetupBoxDatatable(DataTable):
    model = SetupBox
    columns = ['id','model','mac','status','operator',
                'service','associated_service','voucher',
                'associated_voucher','botones']
    order_columns = ['id','model','mac','status','operator',
                'service','associated_service','voucher',
                'associated_voucher','botones']
    max_display_length = 10000
    

    def filter_queryset(self,qs):
        
        search = self.request.POST.get(u'search[value]', None)

        if search:
            qs = qs.filter(Q(model__model__icontains=search)|
                            Q(operator__name__istartswith=search)|
                            Q(mac__mac__istartswith=search)|
                            Q(operator__name__istartswith=search)|
                            Q(voucher__code__istartswith=search))

        return qs
    
    def get_initial_queryset(self):
        qs = SetupBox.objects.filter(status_field=True)

        if 'voucher' in self.kwargs:
            qs = qs.filter(voucher__id = self.kwargs['voucher'])

        return self.filter_queryset(qs)

    def btn_detail(self, row):
        t = """<a type="button" class="btn btn-default btn-xs" onclick="{fn}">
        <i style="width:18px;" class="fa fa-eye"></i></a>"""
        json = dict(
            model = row.model.model,
            mac = row.mac.mac,
            status = row.status.name,
            operator = row.operator.name,
            service = row.service.number if row.service else None,
            associated_service = row.associated_service.strftime('%d-%m-%Y') if row.associated_service else '-',
            voucher = row.voucher.code if row.voucher else None,
            associated_voucher = row.associated_voucher.strftime('%d-%m-%Y') if row.associated_voucher else '-'
        )
        return t.format(fn="detailSetupBox(%s)" % str(json))

    def btn_edit(self, row):
        t = """<a type="button" class="btn btn-warning btn-xs" onclick="{fn}">
        <i style="width:18px;" class="fa fa-edit"></i></a>"""
        json = dict(
            id = row.id,
            model = [{'id':row.model.id,'text':row.model.model}] if row.model else [],
            mac = row.mac.mac,
            status = [{'id':row.status.id,'text':row.status.name}] if row.status else [],
            operator = [{'id':row.operator.id,'text':row.operator.name}] if row.operator else [],
            service = [{'id':row.service.id,'text':row.service.number}] if row.service else [],
            voucher = [{'id':row.voucher.id,'text':row.voucher.code}] if row.voucher else [],
        )
        return t.format(fn="loadModalEdit(%s)" % str(json))

    def btn_delete(self, row):
        t = """<a type="button" class="btn btn-danger btn-xs" onclick="{fn}">
        <img style="width:18px;" src="/static/img/papelera.png"></a>"""
        return t.format(fn="deleteSetupbox('%s')" % str(row.id))


    def render_column(self, row, column):
        if column == 'botones':
            btn = ButtonDataTable()                
            btn.list_btn.append(self.btn_detail(row))
            btn.list_btn.append(self.btn_edit(row))
            btn.list_btn.append(self.btn_delete(row))
            return btn()
        elif column == 'mac':
            json = dict(
                id = row.id,
                mac = row.mac.id,
                operator = row.operator.id,
                model = row.model.id,
                service = row.service.id
            )
            return f'<a data-id="{json}" data-panel="panel_tv_content">{row.mac}</a>'
            # return f'<a data-click="panel-lateral" data-id="{json}" data-panel="panel_tv_content">{row.mac}</a>' 
        elif column in ['associated_service','associated_voucher']:
            columna = getattr(row,column)
            if columna:
                return getattr(row,column).strftime("%d-%m-%Y")
            else:
                return '-'
        elif column == 'service':
            if row.service:
                return super().render_column(row,column)
            else:
                return 'Ningún servicio asociado'
        else:
            return super().render_column(row,column)