import datetime 
from dateutil.relativedelta import relativedelta
from django.utils import timezone

from common.utils import timedelta_format
from common.mixins import DataTable, ButtonDataTable

from tv.models import Voucher
from django.db.models import Q


class VoucherDatatable(DataTable):
    columns = ['id','code','stb_assoc','supplier_status','operator','created','botones']
    order_columns = ['id','code','stb_assoc','supplier_status','operator','created','botones']
    max_display_length = 5000
    mac = None
    id = None
    html_panel = f'<tr><td><a data-click="panel-lateral" data-id="%s" data-panel="panel_tv_content">%s</a></td></tr>'

    def filter_queryset(self,qs):
        
        search = self.request.POST.get(u'search[value]', None)

        if search:
            qs = qs.filter(Q(code__icontains=search)|
                            Q(operator__name__istartswith=search))

        return qs
    
    def get_initial_queryset(self):
        qs = Voucher.objects.filter(status_field=True)
        return self.filter_queryset(qs)

    def btn_detail(self, row):
        t = """<a type="button" class="btn btn-default btn-xs" onclick="{fn}">
        <i style="width:18px;" class="fa fa-eye"></i></a>"""

        json = dict(
            id = row.id,
            code = row.code,
            stb_assoc = row.stb_assoc,
            status = row.supplier_status.name,
            operator = row.operator.name,
            created = row.created.strftime('%d-%m-%Y')
        )

        return t.format(fn="detailVoucher(%s)" % str(json))

    def btn_edit(self, row):
        t = """<a type="button" class="btn btn-warning btn-xs" onclick="{fn}">
        <i style="width:18px;" class="fa fa-edit"></i></a>"""

        json = dict(
            id = row.id,
            code = row.code,
            supplier_status = [{'id':row.supplier_status.pk,'text':row.supplier_status.name}],
            operator = [{'id':row.operator.pk,'text':row.operator.name}],
        )
        return t.format(fn="loadModalEdit(%s)" % str(json))

    def btn_delete(self, row):
        stb_assoc = len(row.stb_assoc)
        t = """<a type="button" class="btn btn-danger btn-xs" onclick="{fn}">
        <img style="width:18px;" src="/static/img/papelera.png"></a>"""
        fn = f"deleteVoucher('{row.id}','{row.code}',{stb_assoc})"
        return t.format(fn=fn)


    def render_column(self, row, column):
        if column == 'botones':
            btn = ButtonDataTable()                
            btn.list_btn.append(self.btn_detail(row))
            btn.list_btn.append(self.btn_edit(row))
            btn.list_btn.append(self.btn_delete(row))
            return btn()
        if column == 'code':
            return f'<a href="#/tv/vouchers-detail/{row.id}" title="Detalle del voucher {row.code}" style="text-decoration: none;color: #0060B6;">{row.code}</a>'
        if column == 'stb_assoc':
            text = '<ul>'
            for mac in row.stb_assoc:
                # text += f'<li><a data-click="panel-lateral" data-id="{mac["id"]}" data-panel="panel_tv_content">{mac["mac"]}</a></li>' 
                text += f'<li><a data-id="{mac["id"]}" data-panel="panel_tv_content">{mac["mac"]}</a></li>' 
            text += '</ul>'
            return text
        elif column == 'created':
            return row.created.strftime("%d-%m-%Y")

        else:
           return super().render_column(row, column)
