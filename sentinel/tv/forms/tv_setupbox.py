from django import forms 
from tv.models import SetupBox
from dal import autocomplete


class SetupBoxForm(forms.ModelForm):
    class Meta:
        model = SetupBox
        fields = [
                    'model','mac','operator',
                    'service','associated_service',
                    'voucher','associated_voucher'
                ]

