from django import forms 
from tv.models import Voucher
from dal import autocomplete


class VoucherForm(forms.ModelForm):
    class Meta:
        model = Voucher 
        fields = ['code','supplier_status','operator']

        widgets = dict(
            supplier_status = autocomplete.ModelSelect2(url='/api/v1/status-voucher-ac/',
                                                attrs={'class':'form-control',
                                                        'style':"100%",
                                                        'data-placeholder':"Status"}),
            operator = autocomplete.ModelSelect2(url='/api/v1/operator-ac/',
                                                attrs={'class':'form-control',
                                                        'style':"100%",
                                                        'data-placeholder':"Operador"})
        )
    