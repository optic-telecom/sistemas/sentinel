from django.db import models
from common.models import BaseModel
from tv.utils import SelectOrPrefetchManager

class SetupBox(BaseModel):
    
    model = models.ForeignKey('devices.ModelDevice',on_delete=models.CASCADE,verbose_name="Modelo",blank=True,null=True)
    mac = models.OneToOneField('devices.MACTable',unique=True,on_delete=models.PROTECT,verbose_name="Mac",blank=True,null=True)
    status = models.ForeignKey('StatusSTB',on_delete=models.CASCADE,verbose_name="Status del SetupBox",blank=True,null=True)
    operator = models.ForeignKey('devices.Operator',on_delete=models.PROTECT,verbose_name="Operador",blank=True,null=True)
    # network_status = models.BooleanField(default=True,verbose_name="Status de la red")
    service = models.ForeignKey('customers.Service',on_delete=models.PROTECT,verbose_name="Servicio nro.",blank=True,null=True)
    associated_service = models.DateTimeField(blank=True,null=True,verbose_name="Asociado al servicio desde")
    voucher = models.ForeignKey('Voucher',verbose_name="Voucher",on_delete=models.PROTECT,blank=True,null=True)
    associated_voucher = models.DateTimeField(blank=True,null=True,verbose_name="Asociado al voucher desde")

    # Manager Personalizado
    objects = SelectOrPrefetchManager(
        select_related=(
            "model", "mac", "operator", "service", "voucher"
            ),
        )

    def __str__(self):
        return f'{self.mac}'

    class Meta:
        ordering = ['-id']
        verbose_name = 'Setup Box'
        verbose_name_plural = 'Setup Boxes'