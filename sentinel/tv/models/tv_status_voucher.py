from django.db import models
from common.models import BaseModel
# Create your models here.

class StatusVoucher(BaseModel):
    name = models.CharField(max_length=50,verbose_name="Status")
    description = models.TextField(blank=True,null=True)

    def __str__(self):
        return f'{self.name}'
