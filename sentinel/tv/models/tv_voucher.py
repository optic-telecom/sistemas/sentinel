from django.db import models
from common.models import BaseModel
from tv.utils import SelectOrPrefetchManager
# Create your models here.

class Voucher(BaseModel):
    code = models.CharField(max_length=255,unique=True,verbose_name="miplay_id",help_text="Id suministrado por el proveedor")
    supplier_status = models.ForeignKey('StatusVoucher',on_delete=models.CASCADE,verbose_name="Status del Proveedor",blank=True,null=True)
    operator = models.ForeignKey('devices.Operator',on_delete=models.PROTECT,verbose_name="Operador",blank=True,null=True)

    objects = SelectOrPrefetchManager(
        select_related = (
            "supplier_status",
            "operator"
        )
    )

    def __str__(self):
        return f'{self.code}'

    @property
    def stb_assoc(self):
        macs = self.setupbox_set.all()
        listado = list()
        for stb in macs:
            listado.append({'id':stb.mac.pk,'mac':stb.mac.mac}) 
        return listado

    class Meta:
        ordering = ['-id']
        verbose_name = 'Voucher'
        verbose_name_plural = "Voucher's"
