from rest_framework import serializers 
from tv.models import SetupBox
from django.utils import timezone
from devices.models import MACTable

class SetupBoxSerializer(serializers.ModelSerializer):
    mac = serializers.CharField()

    class Meta:
        model = SetupBox
        fields = ['id','model','operator',
                'service','mac',
                'status',
                'voucher',
                'associated_service','associated_voucher',]
        
    def validate_service(self,service):
        error_service = "Este nro de servicio ya se encuentra ocupado por uno o mas dispositivos"
        self.generic_validation('service',service,error_service,**{'service':service})
        return service

    def validate_voucher(self,voucher):
        error_voucher = "Este código ya se encuentra ocupado por uno o mas dispositivos"
        self.generic_validation('voucher',voucher,error_voucher,**{'voucher':voucher})
        return voucher

    def generic_validation(self,field,attr,error,**kwargs):
        stb = SetupBox.objects.all()
        if self.instance and attr != getattr(self.instance,field):
            # al momento de editar
            stb_exists = stb.filter(**kwargs)
            if stb_exists.exists() and stb_exists.count() == 2:
                raise serializers.ValidationError(error)
        
        elif not self.instance:
            # al momento de crear
            if stb.filter(**kwargs).count() == 2:
                raise serializers.ValidationError(error)

    def validate_mac(self,mac):
        obj,create = MACTable.objects.get_or_create(mac=mac)
        stb = SetupBox.objects.filter(mac=obj)

        if self.instance and mac != self.instance.mac:
            # AL MOMENTO DE EDITAR
            if stb.exclude(id=self.instance.id).exists():
                raise serializers.ValidationError('Dispositivo registrado previamente en otro equipo')
        else:
            # AL MOMENTO DE CREAR
            if stb.exists():
                raise serializers.ValidationError('Dispositivo registrado previamente en otro equipo')
        return obj

    def create(self,validated_data):
        hoy = timezone.now()
        servicio = validated_data.get('service',None)
        voucher = validated_data.get('voucher',None)

        stb = SetupBox(**validated_data)

        if servicio:
            stb.associated_service = hoy
        
        if voucher:
            stb.associated_voucher = hoy

        stb.save()
        return stb

    def update(self,instance,validated_data):
        hoy = timezone.now()
        servicio = validated_data.pop('service')
        voucher = validated_data.pop('voucher')
        
        for key,value in validated_data.items():
            setattr(instance,key,value)

        if instance.service == None and servicio:
            instance.service = servicio
            instance.associated_service = hoy
        elif servicio:
            instance.service = servicio
        
        if instance.voucher == None and voucher:
            instance.voucher = voucher
            instance.associated_voucher = hoy
        elif voucher:
            instance.voucher = voucher

        instance.save()
        return instance
