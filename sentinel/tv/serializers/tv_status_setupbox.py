from rest_framework import serializers 
from tv.models import StatusSTB


class StatusSTBSerializers(serializers.ModelSerializer):
    class Meta:
        model = StatusSTB
        fields = ('id','name','description')

