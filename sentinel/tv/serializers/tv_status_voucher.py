from rest_framework import serializers 
from tv.models import StatusVoucher

class StatusVoucherSerializers(serializers.ModelSerializer):
    class Meta:
        model = StatusVoucher
        fields = ('id','name','description')
