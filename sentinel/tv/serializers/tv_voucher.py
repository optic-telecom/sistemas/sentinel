from rest_framework import serializers 
from tv.models import Voucher


class VoucherSerializer(serializers.ModelSerializer):

    class Meta:
        model = Voucher
        fields = ['id','code','operator','supplier_status','stb_assoc']
        lookup_field = 'code'

    def validate_code(self,code):
        
        if self.instance and self.instance.setupbox_set.count() > 2:
            raise serializers.ValidationError('Este voucher ya completó el número de Setupbox asociados')
        return code
