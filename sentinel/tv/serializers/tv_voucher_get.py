from rest_framework import serializers


class VoucherGetSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    code = serializers.CharField(read_only=True)
    operator = serializers.CharField(read_only=True)
    supplier_status = serializers.CharField(read_only=True)
    stb_assoc = serializers.JSONField(read_only=True)
