from django.urls import path, include

app_name = "tv"

urlpatterns = [
    path("", include("tv.urls.tv_views")),
    path("datatables/", include("tv.urls.tv_datatables")),
    path("autocompletes/", include("tv.urls.tv_autocompletes")),
    
]