from rest_framework.routers import DefaultRouter
from tv.viewsets import VoucherViewSet,SetupBoxViewSet,StatusSTBViewSet,StatusVoucherViewSet

router = DefaultRouter()
router.register(r'vouchers',VoucherViewSet)
router.register(r'setupboxs',SetupBoxViewSet)
router.register(r'status-voucher',StatusVoucherViewSet,'status-voucher')
router.register(r'status-stb',StatusSTBViewSet,'status-stb')