from django.urls import path
from tv.models import *
from customers.models import Service
from devices.models import Operator,ModelDevice,MACTable
from tv.autocompletes import AutocompleteSelect2

app_name = "autocompletes"

urlpatterns = [
    path('voucher/',(AutocompleteSelect2
                        .as_view(model=Voucher,
                                filter_by="code__istartswith")),
                        name="voucher-ac"),
    path('status-voucher/',(AutocompleteSelect2
                        .as_view(model=StatusVoucher,
                                filter_by="name__istartswith",high_obj=False)),
                        name="status-voucher-ac"),
    path('setupbox/',(AutocompleteSelect2
                        .as_view(model=SetupBox,
                                filter_by="voucher__code__istartswith")),
                        name="setupbox-ac"),
    path('status-stb/',(AutocompleteSelect2
                        .as_view(model=StatusSTB,
                                filter_by="name__istartswith",high_obj=False)),
                        name="status-stb-ac"),
    path('operator/',(AutocompleteSelect2
                        .as_view(model=Operator,
                                filter_by="name__istartswith")),
                        name="operator-ac"),
    path('model/',(AutocompleteSelect2
                        .as_view(model=ModelDevice,
                                filter_by="model__istartswith",
                                second_filter_by={'kind__exact':6})),
                        name="model-ac"),
    path('service/',(AutocompleteSelect2
                        .as_view(model=Service,
                                filter_by="number__istartswith")),
                        name="service-ac"),
    path('mac/',(AutocompleteSelect2
                        .as_view(model=MACTable,
                                filter_by="mac__istartswith")),
                        name="mac-ac"),
]