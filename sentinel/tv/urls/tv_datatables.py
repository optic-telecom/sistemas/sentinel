from django.urls import path
from tv.datatables import (VoucherDatatable, SetupBoxDatatable)

app_name = "datatables"

urlpatterns = [
    path('voucher',
        VoucherDatatable.as_view(),
        name="dt-vouchers"),#Listado general
    path('voucher-detail/<slug:voucher>',
        VoucherDatatable.as_view(),
        name="dt-vouchers-detail"),
    path('setupbox',
        SetupBoxDatatable.as_view(),
        name="dt-setupbox"),#Listado general
    path('setupbox-detail/<slug:voucher>',
        SetupBoxDatatable.as_view(),
        name="dt-setupbox-detail"),#detalle del setupbox segun voucher
]