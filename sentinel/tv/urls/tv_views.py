from django.urls import path
from tv.views import BandaAnchaTVView

app_name = "views"

urlpatterns = [
    path(
        'vouchers',
        BandaAnchaTVView.as_view(
            template_name='tv_voucher/list.html'
        ),
        name="list-vouchers"),
    path(
        'vouchers-detail/<slug:pk>',
        BandaAnchaTVView.as_view(
            template_name="tv_voucher/detail_index.html"
        ),
        name="detail-vouchers"),
    path(
        'setupbox',
        BandaAnchaTVView.as_view(
            template_name="tv_setupbox/list.html"
        ),
        name="list-setupbox"),
]