from django.db import models


class SelectOrPrefetchManager(models.Manager):
    """
    Manager standar para tratar relaciones foreign_key y many_to_many
    uso:
        en la clase models donde se emplee se debe hacer invocar al
        objects e indicarle que su manager sera SelectOrPrefetchManager
        y pasarle los argumentos select_related o prefetch_related

    ej:
        objects = SelectOrPrefetchManager(
            select_related=("foreign_key_fields_list",),
            prefetch_related=("m2m_fields_list",),
        )
    """
    def __init__(self, *args, **kwargs):
        self._select_related = kwargs.pop('select_related', None)
        self._prefetch_related = kwargs.pop('prefetch_related', None)
        super(SelectOrPrefetchManager, self).__init__(*args, **kwargs)

    def get_queryset(self, *args, **kwargs):
        qs = super(SelectOrPrefetchManager, self).get_queryset(*args, **kwargs)

        if self._select_related:
            qs = qs.select_related(*self._select_related)
        if self._prefetch_related:
            qs = qs.prefetch_related(*self._prefetch_related)

        return qs