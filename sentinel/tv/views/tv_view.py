from django.views.generic import TemplateView
from users.utils import JWTMixin
        

class BandaAnchaTVView(JWTMixin, TemplateView):
    pass