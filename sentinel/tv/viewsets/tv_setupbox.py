from tv.models import SetupBox
from tv.serializers import (SetupBoxSerializer)
from rest_framework.viewsets import ModelViewSet


class SetupBoxViewSet(ModelViewSet):
    queryset = SetupBox.objects.all()
    serializer_class = SetupBoxSerializer