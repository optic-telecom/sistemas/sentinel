from django.shortcuts import render
from tv.models import StatusSTB
from tv.serializers import (StatusSTBSerializers)
from rest_framework.viewsets import ModelViewSet


class StatusSTBViewSet(ModelViewSet):
    queryset = StatusSTB.objects.all()
    serializer_class = StatusSTBSerializers