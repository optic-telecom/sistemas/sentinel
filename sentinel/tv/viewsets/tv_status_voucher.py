from tv.models import StatusVoucher
from tv.serializers import (StatusVoucherSerializers)
from rest_framework.viewsets import ModelViewSet


class StatusVoucherViewSet(ModelViewSet):
    queryset = StatusVoucher.objects.all()
    serializer_class = StatusVoucherSerializers