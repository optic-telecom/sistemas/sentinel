from tv.models import Voucher
from tv.serializers import VoucherSerializer
from rest_framework.viewsets import ModelViewSet


class VoucherViewSet(ModelViewSet):
    queryset = Voucher.objects.all()
    serializer_class = VoucherSerializer