from django.contrib import admin, messages
from common.admin import BaseAdmin
from .models import Mikrotik, CpeUtp, ProfileUTP,\
    Radius, SystemProfile, RadiusConnection, MikrotikConnection, \
    Gear, PlanOrderKind, NotFoundEquipment, Lease, NotFoundMatrixEquipment, Availability, \
    BuildingContact, AddressList, ServiceStatusLog

from .forms import ServiceStatusLogForm

from dynamic_preferences.registries import global_preferences_registry
#global_preferences = global_preferences_registry.manager()



@admin.register(Mikrotik)
class MikrotikAdmin(BaseAdmin):
    list_display = ('ip','alias','model', 'description',
    	 			'cpeutps','cpeutp_active','uptime')
    search_fields = ('ip__ip','alias')



@admin.register(CpeUtp)
class CpeUtpAdmin(BaseAdmin):
    list_display = ('ip','alias','service', 'model', 'description','uptime', 'last_seen')
    list_filter = ('nodo_utp','model')
    search_fields = ['ip__ip','alias', 'service__number__exact']


@admin.register(Lease)
class LeaseAdmin(BaseAdmin):
    list_display = ('node', 'created', 'address', 'active_address', 'mac_address', \
                    'active_mac_address', 'client_id', 'active_client_id', 'server', \
                    'hostname', 'last_seen', 'status')
    list_filter = ('node','created') 


@admin.register(NotFoundMatrixEquipment)
class NotFoundMatrixEquipmentAdmin(BaseAdmin):
    list_display = ('id', 'alias', 'serial', 'description', 'is_cpe','node', 'service', 
                    'mac', 'ip', 'primary', 'ssid', 'password', 'ssid_5g')
    list_filter = ('node',) 



@admin.register(SystemProfile)
class SystemProfileAdmin(BaseAdmin):
    list_display = ('name','download_speed','upload_speed','provisioning_available',
                    'limitation_name','description')

@admin.register(ProfileUTP)
class ProfileUTPdmin(BaseAdmin):
    list_display = ('name','download_speed','upload_speed','provisioning_available',
                    'limitation_name','description')


@admin.register(Radius)
class RadiusAdmin(BaseAdmin):
    list_display = ('ip','alias','model', 'description','uptime','status')


@admin.register(RadiusConnection)
class RadiusConnectionAdmin(BaseAdmin):
    list_display = ('pk','username','password', 'radius',)


@admin.register(MikrotikConnection)
class MikrotikConnectionAdmin(BaseAdmin):
    list_display = ('pk','username','password', 'mikrotik',)



@admin.register(Gear)
class GearAdmin(BaseAdmin):
    list_display = ('pk', 'ip', 'description', 'mac', 'node')
    

@admin.register(NotFoundEquipment)
class NotFoundEquipmentAdmin(BaseAdmin):
    list_display = ('pk', 'ip', 'mac', 'host')

    
# @admin.register(PlanOrderKind)
# class PlanOrderKindAdmin(BaseAdmin):
#     list_display = ('pk', 'name')
#     date_hierarchy = 'mikrotik_set__created'




@admin.register(Availability)
class AvailabilityAdmin(BaseAdmin):
    list_display = ('node', 'address')
    list_filter = ('node',) 
    date_hierarchy = 'node__created'


@admin.register(BuildingContact)
class BuildingContactAdmin(BaseAdmin):
    list_display = ('kind', 'name', 'last_name', 'phone', 'email', 'node')




@admin.register(AddressList)
class AddressListAdmin(BaseAdmin):
    list_display = ('node', 'network', 'address', 'mask_address', 'interface')


@admin.register(ServiceStatusLog)
class ServiceStatusLogAdmin(BaseAdmin):
    form = ServiceStatusLogForm
    change_form_template = 'admin/status_node_change.html'
    list_display = ('id','service','status','created_at')