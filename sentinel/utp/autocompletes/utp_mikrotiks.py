from dal import autocomplete
from utp.models import Mikrotik


class MikrotikAutocomplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        qs = Mikrotik.objects.none()

        if self.q:
            qs = Mikrotik.objects.filter(
                alias__icontains=self.q
            )

        qs = qs.only("id", "alias")
        return qs

    def get_result_label(self, item):
        return item.alias

    def get_selected_result_label(self, item):
        return item.alias
