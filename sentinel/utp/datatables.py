import datetime
from dateutil.relativedelta import relativedelta
from django.utils import timezone
from django.db.models import Q  #, Value, Sum, F
#from django.db.models.functions import Concat
#from django.db.models.fields import IntegerField

from common.utils import timedelta_format
from common.mixins import DataTable, ButtonDataTable
#from dashboard.models import TaskModel
from utp.models import (Mikrotik, CpeUtp, SystemProfile, ProfileUTP, Radius,
                        Gear, Lease, Neighbor, MikrotikList, ServiceStatusLog)
from customers.models import Plan

from .models import ChangeStatusNode

from dynamic_preferences.registries import global_preferences_registry
#global_preferences = global_preferences_registry.manager()


class DatatableUTP(DataTable):
    model = Mikrotik
    id_current = 0
    columns = [
        'contador', 'id', 'alias', 'ip', 'cpeutps', 'uptime', 'model.model',
        'description', 'radius', 'botones'
    ]
    order_columns = [
        'id', 'alias', 'ip', 'uptime', 'model.model', 'description', 'radius'
    ]
    max_display_length = 1000

    def display_id(self, obj):
        return f'<a href="#/devices/utp/{obj.uuid}/" data-toggle="ajax">{obj.id}</a>'

    def display_alias(self, obj):
        return f'<a href="#/devices/utp/{obj.uuid}/" data-toggle="ajax">{obj.alias}</a>'

    def btn_delete(self, row):
        t = """<a type="button" class="btn btn-danger" onclick="{fn}">
        <img style="width:18px;" src="/static/img/papelera.png"></a>"""
        return t.format(fn="delete_utp('%s')" % str(row.uuid))

    def render_column(self, row, column):
        if column == 'botones':
            btn = ButtonDataTable()
            btn.list_btn.append(self.btn_delete(row))
            self.btn_update_utp(btn, row)
            return btn()
        elif column == 'contador':
            self.id_current += 1
            return '<p>%s</p>' % (self.id_current)
        elif column == 'cpeutps':
            return '%s/%d' % (row.cpeutp_active, row.cpeutps)
        else:
            return super().render_column(row, column)

    def display_uptime(self, obj):
        display = '<i class="fas fa-circle" style="color: green;"></i> '
        if obj.uptime:
            u = timezone.now() - obj.uptime
            display += timedelta_format(u)
        elif obj.get_uptime():
            u = timezone.now() - obj.get_uptime()
            display += timedelta_format(u)
        return display

        diff = relativedelta(obj.time_uptime, timezone.now())
        diff = '%sd, %sh, %sm, %ss' % (diff.days, diff.hours, diff.minutes,
                                       diff.seconds)
        diff = diff.replace('-', '')
        return '<i class="fas fa-circle" style="color: red;"></i> conexión pérdida hace ' + str(
            diff)

    def display_radius(self, obj):
        print(obj.radius)
        try:
            return obj.radius.alias
        except:
            return ""

    def btn_update_utp(self, btn, row):
        btn.click("taskUpdateUTP('%s')" % str(row.uuid), 'info', 'play',
                  'Actualizar TODA la informacion de Nodo y Leases')

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = self.model.objects.filter(
                Q(alias__icontains=search) | Q(ip__ip__icontains=search)
                | Q(description__icontains=search)
                | Q(model__model__icontains=search))

        filter_type = self._querydict.get('filter_type', None)
        if filter_type:
            qs = qs.filter(type=filter_type)

        return qs

    def get_initial_queryset(self):
        try:
            return self.model.objects.actives()
        except:
            return self.model.objects.all()


class DatatableCPEUTP(DataTable):
    model = CpeUtp
    id_current = 0
    columns = [
        'contador', 'id', 'service', 'status', 'network_status','uptime', 'model.brand',
        'model.model', 'description', 'mac', 'ip', 'firmware', 'last_seen',
        'edit_button', 'activate_service_button', 'corte', 'reposicion', 'primary_equipment', 
        'nodo_utp'
    ]
    order_columns = [
        'id', 'service', 'status', 'uptime', 'model.brand', 'model.model',
        'description', 'mac', 'ip', 'firmware', 'last_seen', 'nodo_utp'
    ]

    max_display_length = 1000
    html_panel = '<a data-click="panel-lateral" data-id="%s" data-panel="panel_utpcpe_content">%s</a>'
    edit_button = '<a class="btn btn-sm btn-success" href="#modal-edit-description" data-toggle="modal" type="button" id="%s" onclick="modalDescription(this)">Editar descripción</a>'
    tempuuid = ""

    def render_column(self, row, column):
        if column == 'contador':
            self.id_current += 1
            return self.html_panel % (row.uuid, self.id_current)
        elif column == 'edit_button':
            return self.edit_button % (row.id)
        elif column == 'activate_service_button':
            if row.service is not None:
                return f'<button class="btn btn-success" \
                onclick="activateMatrixService({row.service.matrix_id}) \
                ">Activar Servicio {row.service.number} en Matrix</button>'
            else:
                return '<button class="btn btn-fail">\
                No cuenta con Número de Servicio</button>'
        elif column == 'corte':
            if row.service is not None:
                return f'<button class="btn btn-danger" \
                    onclick="corteDeServicio({row.service.number}) \
                    ">Cortar Servicio {row.service.number}</button>'
            else:
                return '<button class="btn btn-fail">\
                No cuenta con Número de Servicio</button>'
        elif column == 'reposicion':
            if row.service is not None:
                return f'<button class="btn btn-success" \
                    onclick="reposicionDeServicio({row.service.number}) \
                    ">Conectar Servicio {row.service.number}</button>'
            else:
                return '<button class="btn btn-fail">\
                No cuenta con Número de Servicio</button>'

        elif column == 'network_status':
            if row.service is not None:
                if row.service.seen_connected is True:
                    return '<button class="btn btn-success">En línea</button>'
                else:
                    return '<button class="btn btn-danger">Cortado</button>'
            else:
                return '<button class="btn btn-fail">\
                No cuenta con Número de Servicio</button>'

        elif column == 'primary_equipment':
            if row.service is not None:
                if row.primary:
                    # Si ya es primario lo indicamos
                    return '<button class="btn btn-info">Ya es el equipo primario</button>'
                else:
                    if row.ip is not None:
                        return f'<button class="btn btn-primary"\
                                 onclick="setPrimaryEquipment({row.service.number}, \'{row.ip.ip}\')">Asociar como equipo primario <br>\
                                 del servicio #{row.service.number}</button>'
                    else:
                        '<button class="btn btn-fail"> El equipo no cuenta con Dirección IP</button>'
            else:
                return '<button class="btn btn-fail">\
                No cuenta con Número de Servicio</button>'

        else:
            return super().render_column(row, column)

    def display_id(self, obj):
        self.tempuuid = obj.uuid
        return self.html_panel % (obj.uuid, obj.id)

    def display_nodo_utp(self, obj):
        return f'<a href="#/devices/utp/{obj.nodo_utp.uuid}/" data-toggle="ajax">{obj.nodo_utp.alias}</a>'

    def display_service_number(self, obj):
        if obj.service == None:
            return ""
        else:
            return self.html_panel % (obj.uuid, obj.service.number)

    def display_firmware(self, obj):
        if obj.firmware == None:
            return ""
        else:
            return self.html_panel % (obj.uuid, obj.firmware)

    def display_last_seen(self, obj):
        return str(obj.last_seen)

    def get_initial_queryset(self):

        try:
            #Corregido error que se traía todo los cpe: utp -> nodo_utp
            qs = self.model.objects.filter(status_field=True,
                                           nodo_utp__uuid=self.uuid)
        except:
            qs = self.model.objects.filter(status_field=True)

        filter_status = self._querydict.get('filter_status', None)
        filter_model = self._querydict.get('filter_model', None)
        filter_utp = self._querydict.get('filter_utp', None)
        last_seen_filter = self._querydict.get('last_seen_filter', None)

        if last_seen_filter and last_seen_filter == 'Mayor a un mes':
            qs = qs.filter(last_seen__regex=r'[4-9]\d\s\w*|\d{3,}\s\w*')

        if last_seen_filter and last_seen_filter == 'Menor a un mes':
            qs = qs.exclude(last_seen__regex=r'[4-9]\d\s\w*|\d{3,}\s\w*')

        if filter_status and filter_status == 'ONLINE':
            qs = qs.filter(status=True)

        if filter_status and filter_status == 'OFFLINE':
            qs = qs.filter(status=False)

        if filter_model and filter_model != 'Modelo':
            qs = qs.filter(model__pk=filter_model)

        if filter_utp and filter_utp != '0':
            try:
                #Corregido error que se traía todo los cpe: utp -> nodo_utp
                qs = qs.filter(nodo_utp__uuid=filter_utp)
            except Exception as e:
                pass

        return qs

    def display_uptime(self, obj):
        try:
            if obj.status:
                u = timezone.now() - obj.uptime
                return self.html_panel % (obj.uuid, timedelta_format(u))
            return None
        except Exception as e:
            return None

    def display_RX(self, obj):
        click_template = 'data-click="panel-lateral" data-id="{id}" data-panel="panel_utpcpe_content"'
        bar_template = """<div {click} class="progress" style="height: 10px; width:51%;">
        <div onclick="openPanel('{id}');panel_utpcpe_content('#panel_right_content','{id}');" class="progress-bar {color}" style="width: {percent}%;"></div>
        </div><div {click} style="float: right; position:relative; top: -13px;">
        &nbsp;{signal}</div>"""
        signal = float(obj.RX) if obj.RX else ''
        percent = 0
        color_bar = ''

        if signal != '':
            if (signal > -10):
                html = """<div style="color:red;" data-click="panel-lateral" data-id="{}" 
                data-panel="panel_utpcpe_content">Sobrecarga {}</div>"""
                return html.format(obj.uuid, str(signal) + ' dBm')
            elif signal == -10:
                color_bar = 'bg-lime'
                percent = 100
            elif (signal < -10) & (signal >= -18.9):
                percent = 75
                color_bar = 'bg-green-transparent-8'
            elif (signal < -18.9) & (signal >= -22):
                color_bar = 'bg-green-transparent-8 '
                percent = 55
            elif (signal < -22) & (signal >= -24.9):
                color_bar = 'bg-red-transparent-8 '
                percent = 35
            else:
                color_bar = 'bg-red'
                percent = 5

        return bar_template.format(click=click_template.format(id=obj.uuid),
                                   id=obj.uuid,
                                   color=color_bar,
                                   percent=percent,
                                   signal=str(signal) + ' dBm')

    def display_brand(self, obj):
        try:
            #return obj.brand
            bd = obj.brand
            return self.html_panel % (self.tempuuid, bd)
        except:
            return ""

    def display_model(self, obj):
        try:
            #return obj.model
            md = obj.model
            return self.html_panel % (self.tempuuid, md)
        except:
            return ""

    def display_description(self, obj):
        if "(Duplicado)" in obj.description:
            if obj.status:
                return self.html_panel % (obj.uuid, obj.description.split(
                    ',')[0]) + '<p style="color: green;">{}</p>'.format(
                        self.html_panel %
                        (obj.uuid, obj.description.split(',')[1]))
            else:
                return self.html_panel % (obj.uuid, obj.description.split(
                    ',')[0]) + '<p style="color: red;">{}</p>'.format(
                        self.html_panel %
                        (obj.uuid, obj.description.split(',')[1]))
        else:
            return self.html_panel % (obj.uuid, obj.description)

    def display_ip(self, obj):
        if obj.active_ip is not None:
            if obj.active_ip.ip is not None:
                if obj.ip.ip == obj.active_ip.ip:
                    return '<p style="color: green;">{}</p>'.format(
                        self.html_panel % (obj.uuid, str(obj.active_ip)))
                else:
                    return '<p style="color: red;">{}</p>'.format(
                        self.html_panel % (obj.uuid, str(obj.active_ip)))
        return '<p style="color: red;">{}</p>'.format(self.html_panel %
                                                      (obj.uuid, obj.ip))

    def display_service_number(self, obj):
        return self.html_panel % (obj.uuid, obj.service.number)

    def display_mac(self, obj):
        if obj.mac_eth1:
            if obj.mac_eth1.mac != "" and obj.mac_eth1.mac != None:
                if obj.mac == obj.mac_eth1:
                    return '<p style="color: green;">{}</p>'.format(
                        self.html_panel % (obj.uuid, obj.mac_eth1))
                else:
                    return '<p style="color: green;">{}</p>'.format(
                        self.html_panel % (obj.uuid, obj.mac_eth1))
        return '<p style="color: green;">{}</p>'.format(self.html_panel %
                                                        (obj.uuid, obj.mac))

    def display_status(self, obj):
        if obj.status:
            string = '<i class="fas fa-circle" style="color: green;"></i>'
            return self.html_panel % (obj.uuid, string)
        string = '<i class="fas fa-circle" style="color: red;"></i>'
        return self.html_panel % (obj.uuid, string)

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search and search.replace(' ', '') != '':
            qs = self.get_initial_queryset()
            qs = qs.filter(
                Q(ip__ip__icontains=search.strip())
                | Q(description__icontains=search.strip())
                | Q(model__model__icontains=search.strip())
                | Q(mac__mac__icontains=search.strip())
                | Q(service__number__exact=search.strip())
                )

        filter_status = self._querydict.get('filter_status', None)
        filter_model = self._querydict.get('filter_model', None)

        if filter_status and filter_status == 'ONLINE':
            qs = qs.filter(status=True)

        if filter_status and filter_status == 'OFFLINE':
            qs = qs.filter(status=False)

        if filter_status and filter_status == 'Sin Número de Servicio':
            qs = qs.filter(service__isnull=True)

        if filter_status and filter_status == 'Duplicados':
            qs = qs.filter(description__icontains="(Duplicado)")

        return qs


class DatatableSystemProfile(DataTable):
    model = SystemProfile
    id_current = 0
    columns = [
        'id', 'name', 'limitation_name', 'download_speed', 'upload_speed',
        'provisioning_available', 'description', 'botones'
    ]
    order_columns = [
        'id', 'name', 'limitation_name', 'download_speed', 'upload_speed',
        'provisioning_available', 'description'
    ]
    max_display_length = 1000

    def render_column(self, row, column):
        if column == 'botones':
            btn = ButtonDataTable()
            m = '<i data-placement="right" data-toggle="tooltip" title="Editar Disponible Provisión" data-html="true"><a type="button" class="btn btn-light" onclick="{fn}"><i class="fas fa-exchange-alt"></i></a></i>'
            #<img style="width:18px;" src="/static/img/papelera.png">
            if row.name:
                name = row.name
            else:
                name = ""

            btn.list_btn.append(
                m.format(fn="speedUtpProvisioning(%d,'%s')" % (row.id, name)))
            btn.click("deleteSpeed(%d,'%s')" % (row.id, str(name)), 'danger',
                      'recycle')

            return btn()
        else:
            return super().render_column(row, column)

    def display_id(self, obj):
        self.id_current += 1
        return self.id_current

    def display_provisioning_available(self, obj):
        if obj.provisioning_available:
            return '<p style="color: green;">Si</p>'
        return '<p style="color: red;">No</p>'

    def display_upload_speed(self, obj):
        if obj.upload_speed:
            return "%d Mbps" % obj.upload_speed
        return ''

    def display_download_speed(self, obj):
        if obj.download_speed:
            return "%d Mbps" % obj.download_speed
        return ''

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = self.model.objects.filter(
                Q(name__icontains=search) | Q(cir__icontains=search))
        return qs


class DatatableProfile(DataTable):
    model = ProfileUTP
    id_current = 0
    columns = [
        'id', 'name', 'limitation_name', 'download_speed', 'upload_speed',
        'provisioning_available', 'description', 'system_profile', 'botones'
    ]
    order_columns = [
        'id', 'name', 'limitation_name', 'download_speed', 'upload_speed',
        'provisioning_available', 'description', 'system_profile'
    ]
    max_display_length = 1000

    def render_column(self, row, column):
        if column == 'botones':
            btn = ButtonDataTable()
            t = '<i data-placement="right" data-toggle="tooltip" title="Cambiar System Profile" data-html="true"><a type="button" class="btn btn-info" onclick="{fn}"><i class="fas fa-exchange-alt"></i></a></i>'
            m = '<i data-placement="right" data-toggle="tooltip" title="Editar Disponible Provisión" data-html="true"><a type="button" class="btn btn-light" onclick="{fn}"><i class="fas fa-exchange-alt"></i></a></i>'
            #<img style="width:18px;" src="/static/img/papelera.png">
            speeds_system = row.system_profile.id if row.system_profile else 0
            if row.name:
                name = row.name
            else:
                name = ""

            btn.list_btn.append(
                m.format(fn="speedUtpProvisioning(%d,'%s')" % (row.id, name)))
            btn.list_btn.append(
                t.format(fn="speedUtpSystem(%d,%d,'%s')" %
                         (row.id, speeds_system, name)))
            btn.click("deleteSpeed(%d,'%s')" % (row.id, str(name)), 'danger',
                      'recycle')

            return btn()
        else:
            return super().render_column(row, column)

    def display_id(self, obj):
        self.id_current += 1
        return self.id_current

    def display_system_profile(self, obj):
        if obj.system_profile:
            return obj.system_profile.name
        else:
            return ""

    def display_provisioning_available(self, obj):
        if obj.provisioning_available:
            return '<p style="color: green;">Si</p>'
        return '<p style="color: red;">No</p>'

    def display_upload_speed(self, obj):
        if obj.upload_speed:
            return "%d Mbps" % obj.upload_speed
        return ''

    def display_download_speed(self, obj):
        if obj.download_speed:
            return "%d Mbps" % obj.download_speed
        return ''

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = self.model.objects.filter(
                Q(name__icontains=search) | Q(cir__icontains=search))
        return qs


class DatatablePlan(DataTable):
    model = Plan
    columns = [
        'id',
        'operator',
        'name',
        'type_plan',
        'download_speed',
        'upload_speed',
        # 'speeds_system_download', #'profile_name.download_speed','profile_name.upload_speed',
        'aggregation_rate',
        'profile_name',
        'matrix_plan',
        'botones',
    ]
    order_columns = columns
    max_display_length = 1000

    def display_profile_name(self, obj):
        if obj.profile_name:
            return obj.profile_name.name
        return ''

    def display_aggregation_rate(self, obj):
        if obj.aggregation_rate:
            return "1:%d" % obj.aggregation_rate
        return ''

    def display_upload_speed(self, obj):
        if obj.upload_speed:
            return "%d Mbps" % obj.upload_speed
        elif obj.profile_name:
            return "%d Mbps" % obj.profile_name.upload_speed
        return ''

    def display_download_speed(self, obj):
        if obj.download_speed:
            return "%d Mbps" % obj.download_speed
        elif obj.profile_name:
            return "%d Mbps" % obj.profile_name.download_speed
        return ''

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = self.model.objects.filter(
                Q(name__icontains=search)
                | Q(type_plan__name__icontains=search))
        return qs

    def render_column(self, row, column):
        if column == 'botones':
            btn = ButtonDataTable()
            btn.click("editPlan('%s')" % str(row.id), 'info', 'edit')
            return btn()
            # return ""
        else:
            return super().render_column(row, column)


class DatatableRadius(DataTable):
    model = Radius
    id_current = 0
    columns = [
        'id', 'alias', 'ip', 'uptime', 'model.model', 'description', 'botones'
    ]  #]
    order_columns = [
        'id', 'alias', 'ip', 'uptime', 'model.model', 'description'
    ]
    max_display_length = 1000

    def display_id(self, obj):
        self.id_current += 1
        return f'<a href="#/devices/utp/radius/{obj.uuid}/" data-toggle="ajax">{self.id_current}</a>'
        #return self.id_current

    def display_alias(self, obj):
        return f'<a href="#/devices/utp/radius/{obj.uuid}/" data-toggle="ajax">{obj.alias}</a>'
        #return obj.alias

    def display_model(self, obj):
        try:
            return obj.model
        except:
            return ""

    def btn_delete(self, row):
        t = """<a type="button" class="btn btn-danger" onclick="{fn}">
        <img style="width:18px;" src="/static/img/papelera.png"></a>"""
        return t.format(fn="delete_utpradius('%s')" % str(row.uuid))

    def render_column(self, row, column):
        if column == 'botones':
            btn = ButtonDataTable()
            btn.list_btn.append(self.btn_delete(row))
            return btn()
        else:
            return super().render_column(row, column)

    def display_uptime(self, obj):
        display = '<i class="fas fa-circle" style="color: green;"></i> '
        if obj.uptime:
            u = timezone.now() - obj.uptime
            display += timedelta_format(u)
        elif obj.get_uptime():
            u = timezone.now() - obj.get_uptime()
            display += timedelta_format(u)
        return display

        diff = relativedelta(obj.time_uptime, timezone.now())
        diff = '%sd, %sh, %sm, %ss' % (diff.days, diff.hours, diff.minutes,
                                       diff.seconds)
        diff = diff.replace('-', '')
        return '<i class="fas fa-circle" style="color: red;"></i> conexión pérdida hace ' + str(
            diff)

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = self.model.objects.filter(
                Q(alias__icontains=search) | Q(ip__ip__icontains=search)
                | Q(description__icontains=search)  #|
                #Q(model__model__icontains=search)
            )
        return qs

    def get_initial_queryset(self):
        try:
            return self.model.objects.actives()
        except:
            return self.model.objects.all()


class DatatableGear(DataTable):
    model = Gear
    id_current = 0
    columns = [
        'contador', 'id', 'ip', 'description', 'model.model', 'mac', 'vlan',
        'node.alias', 'latest_backup_date_cache'
    ]
    order_columns = [
        'id',
        'ip',
        'description',
        'model.model',
        'mac',
        'vlan',
        'node.alias',
        'latest_backup_date_cache',
    ]
    max_display_length = 1000

    def display_id(self, obj):
        return f'<a href="#/devices/gear/{obj.id}/" data-toggle="ajax">{obj.id}</a>'

    def display_ip(self, obj):
        return f'<a href="#/devices/gear/{obj.id}/" data-toggle="ajax">{obj.ip}</a>'

    def display_mac(self, obj):
        return f'{obj.mac}'

    def display_vlan(self, obj):
        return f'{obj.vlan}'

    def display_description(self, obj):
        return f'{obj.description}'

    def display_latest_backup_date_cache(self, obj):
        return f'{obj.latest_backup_date_cache}'

    def btn_delete(self, row):
        t = """<a type="button" class="btn btn-danger" onclick="{fn}">
        <img style="width:18px;" src="/static/img/papelera.png"></a>"""
        return t.format(fn="delete_utp('%s')" % str(row.uuid))

    def render_column(self, row, column):
        if column == 'botones':
            btn = ButtonDataTable()
            btn.list_btn.append(self.btn_delete(row))
            return btn()
        elif column == 'contador':
            self.id_current += 1
            return '<p>%s</p>' % (self.id_current)
        else:
            return super().render_column(row, column)

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = self.model.objects.filter(
                Q(node__alias__icontains=search) | Q(ip__ip__icontains=search)
                | Q(description__icontains=search) | Q(vlan__icontains=search)
                | Q(mac__mac__icontains=search)
                | Q(latest_backup_date_cache__icontains=search)
                | Q(model__model__icontains=search))

        qs = self.filters(qs)
        return qs

    def get_initial_queryset(self):
        qs = self.model.objects.all()
        qs = self.filters(qs)
        return qs

    def filters(self, qs):
        filter_model = self._querydict.get('filter_model', None)
        filter_vlan = self._querydict.get('filter_vlan', None)
        filter_node = self._querydict.get('filter_node', None)
        filter_brand = self._querydict.get('filter_brand', None)

        if filter_brand:
            qs = qs.filter(model__brand=filter_brand)

        if filter_model:
            qs = qs.filter(model__model=filter_model)

        if filter_vlan:
            qs = qs.filter(vlan=filter_vlan)

        if filter_node:
            qs = qs.filter(node__id=filter_node)

        return qs


class DatatableLease(DataTable):
    model = Lease
    id_current = 0
    columns = [
        'last_updated', 'cpe', 'onu', 'not_found', 'address', 
        'active_address', 'mac_address', 'active_mac_address', 
        'client_id', 'active_client_id', 'server', 'hostname', 
        'last_seen', 'status'
    ]
    order_columns = [
        'cpe', 'onu', 'not_found', 'address', 'active_address', 
        'mac_address', 'active_mac_address', 'client_id', 'active_client_id', 
        'server', 'hostname', 'last_seen', 'status'
    ]
    max_display_length = 1000

    def render_column(self, row, column):
        if column == 'last_updated':
            return row.node.last_time_updated_leases_op
        else:
            return super().render_column(row, column)

    def display_is_cpe(self, obj):
        return 'CPE' if obj.is_cpe else 'ONU'

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        qs = self.get_initial_queryset()
        if search:
            qs = qs.filter(
                Q(node__alias__icontains=search)
                | Q(cpe__ip__ip__icontains=search)
                | Q(onu__ip__ip__icontains=search)
                | Q(not_found__ip__ip__icontains=search)
                | Q(address__ip__icontains=search)
                | Q(active_address__ip__icontains=search)
                | Q(mac_address__mac__icontains=search)
                | Q(active_mac_address__mac__icontains=search)
                | Q(client_id__icontains=search)
                | Q(active_client_id__icontains=search)
                | Q(hostname__icontains=search))

            qs = self.filters(qs)
        return qs

    def get_initial_queryset(self):
        try:
            #Corregido error que se traía todo los cpe: utp -> nodo_utp
            qs = self.model.objects.filter(node__uuid=self.uuid)
        except:
            qs = self.model.objects.all()

        return qs

    def filters(self, qs):
        filter_date_ini = self._querydict.get('filter_date_ini', None)
        filter_date_end = self._querydict.get('filter_date_end', None)

        if filter_date_ini and filter_date_end:
            qs = qs.filter(created__range=(filter_date_ini, filter_date_end))
        else:
            if qs.count() > 0:
                qs = qs.filter(created=qs.last().created)

        return qs


class DatatableNeighbor(DataTable):
    model = Neighbor
    id_current = 0
    columns = [
        'interface', 'address', 'mac_address', 'identity', 'board_name',
        'uptime', 'created'
    ]  #]
    order_columns = [
        'interface', 'address', 'mac_address', 'identity', 'board_name',
        'uptime', 'created'
    ]
    max_display_length = 1000

    # def display_id(self, obj):
    #     self.id_current += 1
    #     return f'<a href="#/devices/gear/{obj.id}/" data-toggle="ajax">{obj.id}</a>'

    # def display_ip(self, obj):
    #     return f'<a href="#/devices/gear/{obj.id}/" data-toggle="ajax">{obj.ip}</a>'

    # def display_is_cpe(self, obj):
    #     return 'CPE' if obj.is_cpe else 'ONU'

    def display_created(self, obj):
        return obj.created.strftime("%d/%m/%Y %H:%M:%S")

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = self.model.objects.filter(
                Q(interface__name__icontains=search)
                | Q(address__ip__icontains=search)
                | Q(mac_address__mac__icontains=search)
                | Q(identity__icontains=search) | Q(uptime__icontains=search))

        qs = self.filters(qs)
        return qs

    def get_initial_queryset(self):
        try:
            #Corregido error que se traía todo los cpe: utp -> nodo_utp
            qs = self.model.objects.filter(node__uuid=self.uuid)
        except:
            qs = self.model.objects.all()

        qs = self.filters(qs)
        return qs

    def filters(self, qs):
        filter_date_ini = self._querydict.get('filter_date_ini', None)
        filter_date_end = self._querydict.get('filter_date_end', None)

        if filter_date_ini and filter_date_end:
            qs = qs.filter(created__range=(filter_date_ini, filter_date_end))
        else:
            if qs.count() > 0:
                qs = qs.filter(created=qs.last().created)

        return qs


# =================================================================== #
# ================== DATATABLE SYSLOG MIKROTIK ====================== #
# =================================================================== #

from syslog_app.models import SysLog


class SysLogMikrotikDatatable(DataTable):
    # model = SysLog
    columns = [
        'created', 'hora', 'user_ip', 'username', 'log_type', 'origin',
        'identificador', 'message', 'botones'
    ]
    order_columns = [
        'created', 'hora', 'user_ip', 'username', 'log_type', 'origin',
        'identificador', 'message'
    ]
    max_display_length = 2000

    def display_hora(self, obj):
        return obj.created.strftime("%H:%M:%S")

    def display_created(self, obj):
        return obj.created.strftime("%d-%m-%Y")

    def filtros(self, qs):
        filter_created_sys = self._querydict.get('advance_daterange', None)

        filter_ip_sys = self._querydict.get('filter_ip_sys', None)
        filter_user_sys = self._querydict.get('filter_user_sys', None)
        filter_type_syslog_sys = self._querydict.get('filter_type_syslog_sys',
                                                     None)
        filter_origin_syslog_sys = self._querydict.get(
            'filter_origin_syslog_sys', None)
        filter_identificador_syslog_sys = self._querydict.get(
            'filter_identificador_syslog_sys', None)
        filter_message_sys = self._querydict.get('filter_message_sys', None)

        if filter_user_sys:
            qs = qs.filter(username__istartswith=filter_user_sys)
        if filter_type_syslog_sys:
            qs = qs.filter(log_type__istartswith=filter_type_syslog_sys)
        if filter_message_sys:
            qs = qs.filter(message__istartswith=filter_message_sys)
        if filter_ip_sys:
            qs = qs.filter(user_ip__ip__istartswith=filter_ip_sys)
        if filter_origin_syslog_sys:
            qs = qs.filter(origin__exact=filter_origin_syslog_sys)
        if filter_identificador_syslog_sys:
            qs = qs.filter(
                identificador__exact=filter_identificador_syslog_sys)
        if filter_created_sys:
            startDate = filter_created_sys.split(',')[0] + ' 00:00:00'
            endDate = filter_created_sys.split(',')[1] + ' 23:59:59'

            qs = qs.filter(created__range=[startDate, endDate])

        return qs

    def get_initial_queryset(self):

        qs = SysLog.objects.all()
        try:
            """
            Aquí aplicamos el filtro por UUID dentro de las MIKROTIK
            """
            mikrotik = Mikrotik.objects.get(uuid=self.uuid)
            qs = qs.filter(status_field=True,
                           origin=3,
                           identificador=mikrotik.pk)
            return self.filtros(qs)
        except:
            return qs.filter(status_field=True)

    def btn_delete(self, row):
        t = """<a type="button" class="btn btn-danger btn-xs" onclick="{fn}">
        <img style="width:18px;" src="/static/img/papelera.png"></a>"""
        return t.format(fn="delete_log('%s')" % str(row.id))

    def render_column(self, row, column):
        if column == 'botones':
            btn = ButtonDataTable()
            btn.list_btn.append(self.btn_delete(row))
            return btn()
        if column == 'origin':
            ORIGEN_LIST = (
                (0, 'NO ENCONTRADO'),
                (1, 'ONU'),
                (2, 'OLT'),
                (3, 'MIKROTIK'),
                (4, 'CPE-UTP'),
                (5, 'RADIUS'),
            )
            return ORIGEN_LIST[row.origin][1]
        elif column == 'identificador':
            return row.identificador if row.identificador else 'Desconocido'
        else:
            return super().render_column(row, column)

    def filter_queryset(self, qs):
        # qs = self.filtros(qs)
        #Mejorar filtro
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = qs.filter(
                Q(username__istartswith=search)
                | Q(log_type__istartswith=search)
                | Q(message__istartswith=search)
                | Q(user_ip__ip__istartswith=search))

        #qs = self.get_initial_queryset()
        return qs


class ServiceStatusLogTable(DataTable):
    columns = ['node', 'status', 'apariciones', 'fecha']
    order_columns = ['node', 'status', 'apariciones', 'fecha']
    max_display_length = 5000

    def filter_queryset(self, qs):
        # qs = qs.distinct('fecha').order_by('fecha')

        filter_fecha = self._querydict.get('filter_fecha', None)
        filter_node = self._querydict.get('filter_node', None)
        filter_status = self._querydict.get('filter_status', None)

        if filter_fecha:
            initiald = filter_fecha.split(',')[0]  #+ ' 00:00:00'
            finald = filter_fecha.split(',')[1]  #+ ' 23:59:59'
            qs = qs.filter(fecha__gte=initiald, fecha__lte=finald)

        if filter_node:
            n = MikrotikList.objects.get(pk=filter_node)
            qs = qs.filter(node__istartswith=n.alias)

        if filter_status:
            qs = qs.filter(status__istartswith=filter_status)

        return qs

    def get_initial_queryset(self):

        qs = ChangeStatusNode.objects.all()

        return qs

    # def render_column()
