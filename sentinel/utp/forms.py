from django import forms 
from .models import ServiceStatusLog
from dal import autocomplete

class ServiceStatusLogForm(forms.ModelForm):
    class Meta:
        model = ServiceStatusLog
        fields = ('id','status','seller','node','technician','service','created_at')

        widgets = {
            'service':autocomplete.ModelSelect2(url='/api/v1/service-ac/',
                                                attrs={'class':'form-control',
                                                        'style':"100%",
                                                        'data-placeholder':"Nro de Servicio"}),
            'node':autocomplete.ModelSelect2(url='/api/v1/node-ac/',
                                                attrs={'class':'form-control',
                                                        'style':"100%",
                                                        'data-placeholder':"Nodo"}),
        }

    class Media:
        css = {
            'all':[
            # '/static/autocomplete_light/select2.css',
            # '/static/vendor/select2/dist/css/select2.css',
            # '/static/admin/css/autocomplete.css',
            # '/static/autocomplete_light/select2.css'
            ]
        }

        js = [#'/static/admin/js/vendor/jquery/jquery.js',
            # '/static/autocomplete_light/jquery.init.js',
            # '/static/vendor/select2/dist/js/select2.full.js',
            # '/static/vendor/select2/dist/js/i18n/es.js',
            # '/static/autocomplete_light/autocomplete.init.js',
            # '/static/autocomplete_light/forward.js',
            # '/static/autocomplete_light/select2.js',
            # '/static/autocomplete_light/jquery.post-setup.js',
            # '/static/autocomplete_light/jquery.init.js',
            # '/static/autocomplete_light/select2.js',
            # '/static/autocomplete_light/autocomplete.init.js'
            'plugins/jquery/jquery-3.3.1.min.js',
            'plugins/jquery/jquery-migrate-1.1.0.min.js',
            'plugins/jquery-ui/jquery-ui.min.js'
            
            ]
