from django.apps import apps
from utp.mikrotik import get_address_list
from common.command import BaseCommand

class Command(BaseCommand):
    help = "LLenar la tabla de address list de un nodo"
 
    
    def add_arguments(self, parser):
        parser.add_argument('ip', type=str, help='Ip del nodo',)
        parser.add_argument('username', type=str, help='Username del nodo',)
        parser.add_argument('password', type=str, help='Password del nodo',)
        
    def handle(self, *args, **options):
        ip = options['ip']
        password = options['password']
        username = options['username']

        import ipaddress
        try:
            ipaddress.ip_address(ip)
        except ValueError as e:
            print(e)
            return

        get_address_list(ip, username, password)
        
        self.stdout.write('Ip address list del nodo {} se ha cargado'.format(ip))
