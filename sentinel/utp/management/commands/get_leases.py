from multifiberpy.command import BaseCommand
from utp.models import Mikrotik
from utp.serializers import main_updatecpeutp


class Command(BaseCommand):
    help = 'Obtiene informacion de los Leases de un mikrotik y la almacena en la base de datos'

    def getdata(self, mikrotik, **options):
        print("ENTRANDO AL GET DATA")
        return main_updatecpeutp(mikrotik.uuid)

    def add_arguments(self, parser):
        parser.add_argument('-m', '--mikrotikpk', type=str, help='Define mikrotik a obtener',)

    def handle(self, *args, **options):
        if 'mikrotikpk' in options:
            mikrotik = Mikrotik.objects.get(pk=options['mikrotikpk'])
            self.getdata(mikrotik,**options)
        else:

            all_mikrotik = Mikrotik.objects.all()
            for node in all_mikrotik:
                try:
                    self.getdata(node,**options)                
                except Exception as e:
                    pass
