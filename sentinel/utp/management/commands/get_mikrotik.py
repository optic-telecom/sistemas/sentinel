import uuid
from datetime import timedelta
from django.utils import timezone
from multifiberpy.command import BaseCommand
from devices.models import ModelDevice
from utp.models import Mikrotik, MikrotikConnection
from utp.mikrotik import get_mikrotik_info


class Command(BaseCommand):
    help = 'Obtiene informacion de los Mikrotik y la almacena en la base de datos'

    def getdata(self, mikrotik, **options):
        username = None
        password = None

        for conn in MikrotikConnection.objects.filter(
                mikrotik__id=mikrotik.id):
            print(conn.username, conn.password)
            if conn.username and conn.password:
                username = conn.username
                password = conn.password
                break

        info = get_mikrotik_info(str(mikrotik.ip), username, password)
        if (not isinstance(info, dict)) or info == {}:
            if info == {}:
                msg = 'Falló la conexión vía API. Por favor revise que el username soporte API, \
                        y que el IP Services permita la conexión desde las IP 190.113.247.215 y 190.113.247.219.'

            else:
                msg = info

            raise Exception('{}'.format(msg))
        model_name = info['model']
        try:
            model = ModelDevice.objects.get(model=model_name)
        except ModelDevice.DoesNotExist:
            model = ModelDevice.objects.create(model=model_name,
                                               kind=3,
                                               brand='Mikrotik')
        except Exception as e:
            model = None

        # If week
        if "w" in info['uptime']:
            weeks, rest = info['uptime'].split('w')
            weeks = int(weeks)
        else:
            weeks = 0
            rest = info['uptime']
        # If day
        if "d" in rest:
            days, rest = rest.split('d')
            days = int(days)
        else:
            days = 0
        # If hour
        if "h" in rest:
            hours, rest = rest.split('h')
            hours = int(hours)
        else:
            hours = 0
        # If minute
        if "m" in rest:
            minutes, rest = rest.split('m')
            minutes = int(minutes)
        else:
            minutes = 0
        # If seconds
        if "s" in rest:
            secs, rest = rest.split('s')
            secs = int(secs)
        else:
            secs = 0
        # Get uptime with retrieved values
        value_uptime = timezone.now() - timedelta(
            weeks=weeks, days=days, hours=hours, minutes=minutes, seconds=secs)

        mikrotik.status = True
        mikrotik.firmware = info['firmware']
        mikrotik.uptime = value_uptime
        mikrotik.time_uptime = timezone.now()
        if not mikrotik.id_data:
            mikrotik.id_data = uuid.uuid4()

        mikrotik.description = info['description']
        mikrotik.name = ""
        mikrotik.architecture = info['architecture']
        if model:
            mikrotik.model = model
        try:
            mikrotik.serial = info['serial']
        except KeyError as e:
            print(e)
            pass

        mikrotik.cpu = str(info['cpu'])
        mikrotik.ram = str(info['ram'])
        mikrotik.storage = str(info['hdd'])
        if info['temperature'] != '':
            mikrotik.temperature = info['temperature']
        if info['voltage'] != '':
            mikrotik.voltage = round(float(info['voltage']), 0)
        if info['temperature_cpu'] != '':
            mikrotik.temperature_cpu = info['temperature_cpu']
        if info['power_consumption'] != '':
            mikrotik.power_consumption = round(
                float(info['power_consumption']), 0)

        mikrotik.save()

    def add_arguments(self, parser):
        parser.add_argument(
            '-mikrotik',
            '--mikrotikpk',
            type=str,
            help='Define la mikrotik a obtener',
        )

    def handle(self, *args, **options):
        if options['mikrotikpk']:
            mikrotik = Mikrotik.objects.get(pk=options['mikrotikpk'])
            try:
                self.getdata(mikrotik,**options)
            except Exception as e:
                raise e
        else:

            all_mikrotik = Mikrotik.objects.all()
            for mikrotik in reversed(all_mikrotik):
                try:
                    self.getdata(mikrotik, **options)
                except Exception as e:
                    pass