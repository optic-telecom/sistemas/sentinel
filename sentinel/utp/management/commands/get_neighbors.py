from multifiberpy.command import BaseCommand
from utp.models import Mikrotik, MikrotikConnection
from utp.mikrotik import get_neighbors


class Command(BaseCommand):
    help = 'Obtiene informacion de los Neighbors de un mikrotik y la almacena en la base de datos'

    def getdata(self, mikrotik, **options):
        conn = MikrotikConnection.objects.filter(mikrotik=mikrotik).last()
        try:
            get_neighbors(mikrotik.ip.ip, conn.username, conn.password)
        except Exception as e:
            pass
            

    def add_arguments(self, parser):
        parser.add_argument('-m', '--mikrotikpk', type=str, help='Define mikrotik a obtener',)

    def handle(self, *args, **options):
        if options['mikrotikpk']:
            try:
                mikrotik = Mikrotik.objects.get(pk=options['mikrotikpk'])
            except Mikrotik.DoesNotExist:
                pass
            except Mikrotik.MultipleObjectsReturned:
                pass
            else:
                self.getdata(mikrotik)
        else:

            all_mikrotik = Mikrotik.objects.all()
            for node in all_mikrotik:
                self.getdata(node,**options)