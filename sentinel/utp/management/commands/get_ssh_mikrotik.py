import time
import os

from django.utils import timezone

from common.command import BaseCommand
from utp.models import *
from utp.ssh import UTP_SSH


class Command(BaseCommand):
    help = 'prueba conexion con mik'
                        
    def handle(self, *args, **options):
    
        mik = Mikrotik.objects.last()
        connectiondata = MikrotikConnection.objects.get(mikrotik=mik)
        ssh = UTP_SSH(**{
                            'host':mik.ip,
                            'username':connectiondata.username,
                            'password':connectiondata.password,
                            'is_in_cron':True,
                            'mikrotik':mik
                            })
        ssh.shell()
        #print(ssh.get_health())
        out = ssh.get_health()