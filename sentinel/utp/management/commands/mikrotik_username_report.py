import csv
import pandas as pd
import os
from utp.models import Mikrotik, MikrotikConnection
from . import Command as GetMikrotik
from multifiberpy.command import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        # crea una lista vacía para almacenar todos los datos resultantes
        final_list = []

        # crea el csv para guardar los archivos de manera más cómoda
        with open('mikrotik_user_error.csv', 'w', newline='') as f:
            writer = csv.writer(f)

        # lee el csv y lo transforma a formato xlsx
        read_file = pd.read_csv('mikrotik_user_error.csv')
        read_file.to_excel('mikrotik_user_error.xlsx')

        # Elimina el archivo .csv
        os.remove('mikrotik_user_error.csv')
