import csv
import pandas as pd
import os
from utp.models import Lease
from devices.models import OLT, ONU
from multifiberpy.command import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        # crea una lista vacía para almacenar todos los datos resultantes
        final_list = []

        # Busca los Leases con la condición not_found
        not_found = Lease.objects.filter(not_found__isnull=False).distinct('mac_address')

        #  Busca las macs de las ONUS y de las OLT
        onus = ONU.objects.all()
        onus_mac = [onu.mac.mac for onu in onus if onu.mac != None]

        olts = OLT.objects.all()
        olts_mac = [olt.mac.mac for olt in olts if olt.mac != None]

        # Une las dos listas
        final_macs = onus_mac + olts_mac

        # Hace la comparación para revisar si existe la mac
        for lease in not_found:
            if lease.mac_address.mac not in final_macs:
                final_list.append(lease)

        print(len(not_found))
        print("===")
        print(len(final_macs))
        print("===")
        print(len(final_list))

        # crea el csv para guardar los archivos de manera más cómoda
        with open('leases.csv', 'w', newline='') as f:
            writer = csv.writer(f)
            writer.writerow(['LEASE_ID', 'MAC', 'ALIAS DE NODO', 'IP'])
            for lease in final_list:
                writer.writerow([f'{lease.id}', f'{lease.mac_address.mac}', 
                                 f'{lease.node.alias}', f'{lease.address.ip}'])

        # lee el csv y lo transforma a formato xlsx
        read_file = pd.read_csv('leases.csv')
        read_file.to_excel('leases.xlsx')

        # Elimina el archivo .csv
        os.remove('leases.csv')
