from multifiberpy.command import BaseCommand
from utp.models import Mikrotik

class Command(BaseCommand):

    def handle(self, *args, **options):
        # Lista proveniente del reporte actualizado de los nodos que NO funcionan
        orange_list = [69,189,160,219,96,250,162,163,170,130,249,248,159,120,51,186,227,118,241,115,93,238,92,87,31,174,166,10,223,225,247,128,236,244,222,97,99,101,125,193,242,127,78,207,204,18,119,102,52,129,177,229,95,176,100,132,80,21,71,8,12,79,234,211,212,213,182,206,218,151,135,187,173,42,169,167,150,37,198,110,112,109,108,113,82,161,81,199,152,168,252,72,209,164,158,141,196,145,137,138,140,143,91,35,171,181,]

        qs = Mikrotik.objects.filter(id__in=orange_list)
        print(qs.count())

        for q in qs:
            print(q.ip)
            q.delete()
        
