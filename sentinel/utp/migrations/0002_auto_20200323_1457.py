# Generated by Django 2.1.3 on 2020-03-23 14:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0003_auto_20200323_1457'),
        ('utp', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cpeutp',
            name='active_mac',
        ),
        migrations.RemoveField(
            model_name='historicalcpeutp',
            name='active_mac',
        ),
        migrations.RemoveField(
            model_name='historicalmikrotik',
            name='ip_range',
        ),
        migrations.AddField(
            model_name='cpeutp',
            name='mac_eth1',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='cpe_eth1', to='devices.MACTable'),
        ),
        migrations.AddField(
            model_name='historicalcpeutp',
            name='mac_eth1',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='devices.MACTable'),
        ),
        migrations.AlterField(
            model_name='cpeutp',
            name='mac',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='devices.MACTable'),
        ),
        migrations.AlterField(
            model_name='gear',
            name='mac',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='devices.MACTable'),
        ),
        migrations.AlterField(
            model_name='historicalcpeutp',
            name='mac',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='devices.MACTable'),
        ),
        migrations.AlterField(
            model_name='historicalgear',
            name='mac',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='devices.MACTable'),
        ),
        migrations.AlterField(
            model_name='historicalmikrotik',
            name='mac_eth1',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='devices.MACTable'),
        ),
        migrations.AlterField(
            model_name='historicalnotfoundequipment',
            name='mac',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='devices.MACTable'),
        ),
        migrations.RemoveField(
            model_name='mikrotik',
            name='ip_range',
        ),
        migrations.AddField(
            model_name='mikrotik',
            name='ip_range',
            field=models.ManyToManyField(to='devices.IPRange'),
        ),
        migrations.AlterField(
            model_name='mikrotik',
            name='mac_eth1',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='devices.MACTable'),
        ),
        migrations.AlterField(
            model_name='notfoundequipment',
            name='mac',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='devices.MACTable'),
        ),
    ]
