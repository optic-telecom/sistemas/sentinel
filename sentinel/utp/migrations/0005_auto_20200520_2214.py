# Generated by Django 2.1.3 on 2020-05-20 22:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('utp', '0004_auto_20200428_1836'),
    ]

    operations = [
        migrations.CreateModel(
            name='DurationAntenna',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('activity_id', models.PositiveIntegerField(verbose_name='activity')),
                ('duration', models.PositiveSmallIntegerField(choices=[(0, '30 minutos'), (1, '1 hora'), (2, '2 horas'), (4, '4 horas'), (24, '1 día'), (72, '3 días'), (96, '4 días'), (120, '5 días'), (144, '6 días'), (168, '7 días')], verbose_name='duration')),
            ],
        ),
        migrations.CreateModel(
            name='DurationMikrotik',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('activity_id', models.PositiveIntegerField(verbose_name='activity')),
                ('duration', models.PositiveSmallIntegerField(choices=[(0, '30 minutos'), (1, '1 hora'), (2, '2 horas'), (4, '4 horas'), (24, '1 día'), (72, '3 días'), (96, '4 días'), (120, '5 días'), (144, '6 días'), (168, '7 días')], verbose_name='duration')),
            ],
        ),
        migrations.AlterField(
            model_name='antenna',
            name='comment',
            field=models.TextField(blank=True, max_length=300, null=True),
        ),
        migrations.AlterField(
            model_name='historicalantenna',
            name='comment',
            field=models.TextField(blank=True, max_length=300, null=True),
        ),
        migrations.AlterField(
            model_name='historicalmikrotik',
            name='comment',
            field=models.TextField(blank=True, max_length=300, null=True),
        ),
        migrations.AlterField(
            model_name='historicalmikrotik',
            name='status',
            field=models.PositiveIntegerField(choices=[(1, 'Cableado'), (2, 'Por retirar'), (3, 'En proceso'), (4, 'Sin clasificar'), (5, 'Descartado')], default=1, null=True, verbose_name='status'),
        ),
        migrations.AlterField(
            model_name='mikrotik',
            name='comment',
            field=models.TextField(blank=True, max_length=300, null=True),
        ),
        migrations.AlterField(
            model_name='mikrotik',
            name='status',
            field=models.PositiveIntegerField(choices=[(1, 'Cableado'), (2, 'Por retirar'), (3, 'En proceso'), (4, 'Sin clasificar'), (5, 'Descartado')], default=1, null=True, verbose_name='status'),
        ),
        migrations.AddField(
            model_name='durationmikrotik',
            name='node',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='utp.Mikrotik', verbose_name='node'),
        ),
        migrations.AddField(
            model_name='durationantenna',
            name='node',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='utp.Mikrotik', verbose_name='node'),
        ),
    ]
