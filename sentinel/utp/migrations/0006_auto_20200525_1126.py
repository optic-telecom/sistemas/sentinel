# Generated by Django 2.1.3 on 2020-05-25 11:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('utp', '0005_auto_20200520_2214'),
    ]

    operations = [
        migrations.AddField(
            model_name='cpeutp',
            name='password',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='cpeutp',
            name='ssid',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='cpeutp',
            name='ssid_5g',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='historicalcpeutp',
            name='password',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='historicalcpeutp',
            name='ssid',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='historicalcpeutp',
            name='ssid_5g',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='historicalmikrotik',
            name='type',
            field=models.PositiveIntegerField(choices=[(1, 'Nodo'), (2, 'Antena')], default=1, verbose_name='type'),
        ),
        migrations.AddField(
            model_name='mikrotik',
            name='type',
            field=models.PositiveIntegerField(choices=[(1, 'Nodo'), (2, 'Antena')], default=1, verbose_name='type'),
        ),
        migrations.AlterField(
            model_name='antenna',
            name='status',
            field=models.PositiveIntegerField(choices=[(1, 'Cableado'), (2, 'Por retirar'), (3, 'En proceso'), (4, 'Sin clasificar'), (5, 'Descartado')], default=1, null=True, verbose_name='status'),
        ),
        migrations.AlterField(
            model_name='historicalantenna',
            name='status',
            field=models.PositiveIntegerField(choices=[(1, 'Cableado'), (2, 'Por retirar'), (3, 'En proceso'), (4, 'Sin clasificar'), (5, 'Descartado')], default=1, null=True, verbose_name='status'),
        ),
    ]
