#!/usr/bin/python3
#import os
import sys
from os.path import join
try:
    import posix
except Exception as e:
    pass
import time
import logging
import binascii
import socket
import select
import hashlib
from datetime import datetime
from django.conf import settings
import routeros_api

from devices.models import IPTable, MACTable

#from dynamic_preferences.registries import global_preferences_registry

#global_preferences = global_preferences_registry.manager()


def get_logger(name, is_in_cron=True):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)  # WARNING ERROR

    # now = "{0.year}-{0.month}-{0.day}-{0.hour}".format(datetime.now())

    # if is_in_cron:
    #     ruta = join(settings.BASE_DIR_LOG,'mik_%s_cron.log' % now)
    # else:
    #     ruta = join(settings.BASE_DIR_LOG,'mik_%s_web.log' % now)
    ruta = join(settings.BASE_DIR_LOG, 'mik_web.log')
    file_log = open(str(ruta), "a+")
    format_log = "%(levelname)-.3s %(asctime)s.%(msecs)03d %(name)s %(message)s"
    #os.chmod(ruta, 777)
    handler1 = logging.StreamHandler(file_log)
    handler1.setFormatter(logging.Formatter(format_log, "%Y%m%d-%H:%M:%S"))

    logger.addHandler(handler1)
    return logger


logger = get_logger('Mikrotik', False)


class ApiRos:
    "Routeros api"

    def __init__(self, sk):
        self.sk = sk
        self.currenttag = 0

    def login(self, username, pwd):
        md = hashlib.md5()
        md.update(b'\x00')
        md.update(pwd.encode('UTF-8'))
        respuesta = self.talk(
            ["/login", "=name=" + username, "=password=" + pwd])

        if respuesta[0][1] != {}:
            for repl, attrs in self.talk(["/login"]):
                chal = binascii.unhexlify((attrs['=ret']).encode('UTF-8'))
            md = hashlib.md5()
            md.update(b'\x00')
            md.update(pwd.encode('UTF-8'))
            md.update(chal)
            return self.talk([
                "/login", "=name=" + username,
                "=response=00" + binascii.hexlify(md.digest()).decode('UTF-8')
            ])
        return []

    def talk(self, words):
        if self.writeSentence(words) == 0: return
        r = []
        while 1:
            i = self.readSentence()
            if len(i) == 0: continue
            reply = i[0]
            attrs = {}
            for w in i[1:]:
                j = w.find('=', 1)
                if (j == -1):
                    attrs[w] = ''
                else:
                    attrs[w[:j]] = w[j + 1:]
            r.append((reply, attrs))
            if reply == '!done': return r

    def writeSentence(self, words):
        ret = 0
        for w in words:
            self.writeWord(w)
            ret += 1
        self.writeWord('')
        return ret

    def readSentence(self):
        r = []
        while 1:
            w = self.readWord()
            if w == '':
                return r
            r.append(w)

    def writeWord(self, w):
        logger.info("Execute:%s" % w)
        #print(("<<< " + w))
        self.writeLen(len(w))
        self.writeStr(w)

    def readWord(self):
        ret = self.readStr(self.readLen())
        logger.info("Received:%s" % ret)
        #print((">>> " + ret))
        return ret

    def writeLen(self, l):
        if l < 0x80:
            self.writeStr(chr(l))
        elif l < 0x4000:
            l |= 0x8000
            self.writeStr(chr((l >> 8) & 0xFF))
            self.writeStr(chr(l & 0xFF))
        elif l < 0x200000:
            l |= 0xC00000
            self.writeStr(chr((l >> 16) & 0xFF))
            self.writeStr(chr((l >> 8) & 0xFF))
            self.writeStr(chr(l & 0xFF))
        elif l < 0x10000000:
            l |= 0xE0000000
            self.writeStr(chr((l >> 24) & 0xFF))
            self.writeStr(chr((l >> 16) & 0xFF))
            self.writeStr(chr((l >> 8) & 0xFF))
            self.writeStr(chr(l & 0xFF))
        else:
            self.writeStr(chr(0xF0))
            self.writeStr(chr((l >> 24) & 0xFF))
            self.writeStr(chr((l >> 16) & 0xFF))
            self.writeStr(chr((l >> 8) & 0xFF))
            self.writeStr(chr(l & 0xFF))

    def readLen(self):
        c = ord(self.readStr(1))
        if (c & 0x80) == 0x00:
            pass
        elif (c & 0xC0) == 0x80:
            c &= ~0xC0
            c <<= 8
            c += ord(self.readStr(1))
        elif (c & 0xE0) == 0xC0:
            c &= ~0xE0
            c <<= 8
            c += ord(self.readStr(1))
            c <<= 8
            c += ord(self.readStr(1))
        elif (c & 0xF0) == 0xE0:
            c &= ~0xF0
            c <<= 8
            c += ord(self.readStr(1))
            c <<= 8
            c += ord(self.readStr(1))
            c <<= 8
            c += ord(self.readStr(1))
        elif (c & 0xF8) == 0xF0:
            c = ord(self.readStr(1))
            c <<= 8
            c += ord(self.readStr(1))
            c <<= 8
            c += ord(self.readStr(1))
            c <<= 8
            c += ord(self.readStr(1))
        return c

    def writeStr(self, str):
        n = 0
        while n < len(str):
            r = self.sk.send(bytes(str[n:], 'UTF-8'))
            if r == 0:
                logger.error("RuntimeErr: Connection closed by remote end")
                raise RuntimeError("connection closed by remote end")
            n += r

    def readStr(self, length):
        ret = ''
        while len(ret) < length:
            s = self.sk.recv(length - len(ret))
            if s.decode('UTF-8', 'replace') == '':
                logger.error("RuntimeErr: Connection closed by remote end")
                raise RuntimeError("connection closed by remote end")
            ret += s.decode('UTF-8', 'replace')
        return ret


def drop_client(contract):
    if not contract['mikrotik_ip']:
        logger.error("Tried to DROP contract #%s without a node",
                     contract['number'])
        return False, 'error contract {} sin nodo'.format(contract['number'])

    mikrotik_ip = contract['mikrotik_ip']
    user = contract['mikrotik_username']
    password = contract['mikrotik_password']
    #primary = contract['primary_equipment']
    primary_ip = contract['primary_equipment_ip']
    slug = contract['slug']

    #if primary:
    s = None
    for res in socket.getaddrinfo(mikrotik_ip, "8728", socket.AF_UNSPEC,
                                  socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res
        try:
            s = socket.socket(af, socktype, proto)
            s.settimeout(1)
        except socket.error as msg:
            s = None
            continue
        try:
            s.connect(sa)
        except socket.error as msg:
            s.close()
            s = None
            continue
        break
    if s is None:
        # print('could not open socket')
        # logger.error("Can't open socket to Node %s to DROP contract #%s (Mikrotik: %s)", contract.node.code, contract.number, mikrotik_ip)
        logger.error(
            "Can't open socket to Node %s to DROP contract #%s (Mikrotik: %s)",
            contract['mikrotik_ip'], contract['number'], mikrotik_ip)
        return False, 'No se pudo abrir conexión con el nodo.'
        # sys.exit(1) # WTH

    try:
        apiros = ApiRos(s)
        apiros.login(user, password)

        apiros.talk([
            "/ip/firewall/address-list/add", "=list=CORTES CLIENTES",
            "=address={}".format(primary_ip),
            "=comment=servicio #{}".format(slug)
        ])
        # logger.info("DROPPED %s on Node %s for contract #%s", primary_ip, contract.node.code, contract.number)
        logger.info("DROPPED %s on Node %s for contract #%s", primary_ip,
                    contract['mikrotik_ip'], contract['number'])
        return True, 'ok'
    except Exception as e:
        print(e)
        return False, 'Ocurrió un error inesperado. {}'.format(e)
    # else:
    #     logger.error("Tried to DROP contract #%s without a primary")


def activate_client(contract):
    if not contract['mikrotik_ip']:
        logger.error("Tried to ACTIVATE contract #%s without a node",
                     contract.number)
        return

    mikrotik_ip = contract['mikrotik_ip']
    user = contract['mikrotik_username']
    password = contract['mikrotik_password']
    # eliminar
    # primary = contract['primary_equipment']
    primary_ip = contract['primary_equipment_ip']
    s = None
    for res in socket.getaddrinfo(mikrotik_ip, "8728", socket.AF_UNSPEC,
                                  socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res
        try:
            s = socket.socket(af, socktype, proto)
            s.settimeout(1)
        except socket.error as msg:
            s = None
            continue
        try:
            s.connect(sa)
        except socket.error as msg:
            s.close()
            s = None
            continue
        break
    if s is None:
        # print('could not open socket')
        # logger.error("Can't open socket to Node %s to ACTIVATE contract #%s (Mikrotik: %s)", contract.node.code, contract.number, mikrotik_ip)
        logger.error(
            "Can't open socket to Node %s to ACTIVATE contract #%s (Mikrotik: %s)",
            mikrotik_ip, contract['number'], mikrotik_ip)
        return False, 'No se pudo abrir conexión con el nodo.'

        # sys.exit(1) # why this was here, I can't comprehend

    try:
        apiros = ApiRos(s)
        apiros.login(user, password)

        response = apiros.talk([
            "/ip/firewall/address-list/print", "?list=CORTES CLIENTES",
            "?address={}".format(primary_ip), "=.proplist=.id"
        ])
    except Exception as e:
        print(e)
        return False, 'Ocurrió un error inesperado. {}'.format(e)

    for reply, data in response:
        if reply == '!re':
            try:
                client_id = data['=.id']
                apiros.talk([
                    "/ip/firewall/address-list/remove",
                    "=.id={}".format(client_id)
                ])
                logger.info("ACTIVATED %s on Node %s for contract #%s",
                            primary_ip, mikrotik_ip, contract['number'])
            except KeyError:
                return False, 'error.'
                pass

    return True, 'ok'
    # else:
    #     logger.error("Tried to ACTIVATE contract #%s without a primary",
    #                  contract['number'])


def get_client_status(mikrotik_ip, user, password, client_ip):
    # return False
    s = None
    for res in socket.getaddrinfo(mikrotik_ip, "8728", socket.AF_UNSPEC,
                                  socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res
        try:
            s = socket.socket(af, socktype, proto)
            s.settimeout(1)
        except socket.error as msg:
            s = None
            continue
        try:
            s.connect(sa)
        except socket.error as msg:
            s.close()
            s = None
            continue
        break
    if s is None:
        # print('could not open socket')
        sys.exit(1)

    apiros = ApiRos(s)
    apiros.login(user, password)

    response = apiros.talk([
        "/ip/firewall/address-list/print",
        "?list=CORTES CLIENTES",
    ])

    print(response)

    is_there = False
    for reply, data in response:
        if reply == '!re':
            try:
                if client_ip == data['=address']:
                    is_there = True
            except KeyError:
                pass
    return is_there


def get_dropped(mikrotik_ip, user, password):
    s = None
    for res in socket.getaddrinfo(mikrotik_ip, "8728", socket.AF_UNSPEC,
                                  socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res
        try:
            s = socket.socket(af, socktype, proto)
        except (socket.error, msg):
            s = None
            continue
        try:
            s.connect(sa)
        except (socket.error, msg):
            s.close()
            s = None
            continue
        break
    if s is None:
        # print('could not open socket')
        sys.exit(1)

    s.settimeout(1)
    apiros = ApiRos(s)
    apiros.login(user, password)

    response = apiros.talk([
        "/ip/firewall/address-list/print",
        "?list=CORTES CLIENTES",
    ])

    raw_results = []

    for reply, data in response:
        if reply == '!re':
            try:
                raw_results.append(data['=comment'])
            except KeyError:
                pass

    results = []
    for item in raw_results:
        try:
            contract_number = item.split('#')[1].split(' ')[0]
            results.append(int(contract_number))
        except:
            pass
    return results


def obtain_first_numbers(string):
    arr = ""
    for i in range(len(string)):
        try:
            new = int(string[i])
            arr = arr + string[i]
        except:
            break
    if arr == "":
        return None
    else:
        return int(arr)


def is_mikrotik_get_service(client_id, active_host_name):
    """
        return a tuple with two variables, first if is cpe and second is the service number
    """

    print("++++++++++++++++++++++++++++++++++++++++++++++++")
    print(client_id, active_host_name)

    if client_id[0] == '1' and client_id[1] == ':':
        is_cpe = True
        service_number = obtain_first_numbers(active_host_name)
    else:
        is_cpe = False
        service_number = obtain_first_numbers(client_id)

    if active_host_name is None:
        service_number = None

    return is_cpe, service_number


def get_mikrotik_info(mikrotik_ip, user, password):
    print('4444444444444444444444444444444444')
    #sys.argv[1]
    s = None
    #print ('list_interfaces')
    for res in socket.getaddrinfo(mikrotik_ip, "8728", socket.AF_UNSPEC,
                                  socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res
        try:
            s = socket.socket(af, socktype, proto)
        except socket.error as msg:
            s = None
            continue
        try:
            s.connect(sa)
        except socket.error as msg:
            s.close()
            s = None
            continue
        break
    if s is None:

        print('66666666666666666666666666666666666666666')
        logger.error(
            'get_mikrotik_info Err: Could not open socket for connection')
        return 'No se pudo abrir el socket para realizar la conección'

    results = []

    print('55555555555555555555555555555555555555')
    # try:
    #print ('time')
    s.settimeout(1)
    apiros = ApiRos(s)
    resp_login = apiros.login(user, password)
    for reply, data in resp_login:
        if reply == '!trap':
            logger.error(
                "get_mikrotik_info Err: Credentials given are not valid. Login unsucessful"
            )
            return "Las credenciales dadas no son válidas"

    print('77777777777777777777777777777777777777777777777777777777777')
    response = apiros.talk([
        "/system/routerboard/print",
        #"=.proplist=routerboard,model",
        #response = apiros.talk(["/system/identity/export",
        #                        "=file=temp.txt"
    ])
    #print(response)
    #response = apiros.talk(["/file/print",
    #"=from=temp.rsc",
    #                        "=detail"
    #"=.proplist=contents,.id",
    #                        ])
    #print(response)
    #response = apiros.talk(["/file/remove"])
    #print(response)

    uptime = apiros.talk([
        "/system/resource/print",
        #"=.proplist=uptime",
    ])
    #print(uptime)
    name = apiros.talk([
        "/system/identity/print",
        #"=.proplist=name",
    ])

    health = apiros.talk(["/system/health/print"])
    #print(health)
    for reply, data in response:
        if reply == '!re':
            try:
                #firmwarestr = data['=firmware-type']+" v"+data['=current-firmware']
                results.append(
                    dict(
                        routerboard=data['=routerboard'],
                        serial=data['=serial-number'],
                        #firmware=firmwarestr),
                    ))
                logger.info("get_mikrotik_info: Append serial and routerboard")
            except KeyError:
                logger.error(
                    "get_mikrotik_info KeyErr: Could not append serial and routerboard"
                )
                print("pass in response")
    if results == []:
        results = [{}]
    index = 0
    for reply, data in uptime:
        if reply == '!re':
            try:
                firmwarestr = "RouterOS v" + data['=version']
                ram = (float(data['=total-memory']) - float(
                    data['=free-memory'])) / float(data['=total-memory'])
                ram = round(ram * 100, 1)
                hdd = (float(data['=total-hdd-space']) - float(
                    data['=free-hdd-space'])) / float(data['=total-hdd-space'])
                hdd = round(hdd * 100, 1)
                results[index]['uptime'] = data['=uptime']
                results[index]['brand'] = data['=platform']
                results[index]['model'] = data['=board-name']
                results[index]['architecture'] = data['=architecture-name']
                results[index]['cpu'] = int(data['=cpu-load'])
                results[index]['ram'] = ram
                results[index]['hdd'] = hdd
                results[index]['firmware'] = firmwarestr
                logger.info("get_mikrotik_info: Append health info")
            except:
                logger.info(
                    "get_mikrotik_info Err: Could not append health info")
                print("pass in uptime")
    if results == []:
        results = [{}]
    index = 0
    for reply, data in name:
        if reply == '!re':
            try:
                results[index]['description'] = data['=name']
                logger.info("get_mikrotik_info: Append description")
            except:
                logger.error(
                    "get_mikrotik_info Err: Could not append description")
                print("pass")
    if results == []:
        results = [{}]
    index = 0
    for reply, data in health:
        if reply == '!re':
            try:
                if '=power-consumption' in data.keys():
                    results[index]['power_consumption'] = data[
                        '=power-consumption']
                else:
                    results[index]['power_consumption'] = ""
                if '=cpu-temperature' in data.keys():
                    results[index]['temperature_cpu'] = data[
                        '=cpu-temperature']
                else:
                    results[index]['temperature_cpu'] = ""
                if '=temperature' in data.keys():
                    results[index]['temperature'] = data['=temperature']
                else:
                    results[index]['temperature'] = ""
                if '=voltage' in data.keys():
                    results[index]['voltage'] = data['=voltage']
                else:
                    results[index]['voltage'] = ""
                logger.info(
                    "get_mikrotik_info: Append power_consumption,cpu-temp, temperature, voltage"
                )
            except:
                logger.error(
                    "get_mikrotik_info Err: Could not append power_consumption,cpu-temp, temperature, or voltage"
                )
                print("pass")
    if results == []:
        results = [{}]
    index = 0
    leases = apiros.talk(["/ip/dhcp-server/lease/print"])
    neighbors = apiros.talk(["/ip/neighbor/print"])
    resultsleases = []
    import pprint
    for reply, data in leases:
        if reply == '!re':
            print('')
            pprint.pprint(data)
            print('')
            try:
                if data["=status"] == "waiting":
                    statusbool = False
                else:
                    statusbool = True
                if '=active-mac-address' in data.keys():
                    active_mac = data["=active-mac-address"]
                else:
                    active_mac = ""
                if '=active-address' in data.keys():
                    active_address = data["=active-address"]
                else:
                    active_address = ""

                # var = obtain_first_numbers(data['=host-name'])

                is_cpe, service_number = is_mikrotik_get_service(
                    data['=client-id'], data.get('=host-name', ''))

                resultsleases.append(
                    dict(
                        description=data['=host-name'],
                        active_mac=active_mac,
                        mac=data['=mac-address'],
                        status=statusbool,
                        active_ip=active_address,
                        ip=data['=address'],
                        service_number=service_number,
                        server=data['=server'],
                        # active_hostname=data.get('=active-host-name', None),
                        active_client_id=data.get('=active-client-id', None),
                        client_id=data['=client-id'],
                        last_seen=data.get('=last-seen', None),
                        status_lease=data.get('=status', None),
                        is_cpe=True,
                    ))
                logger.info("get_mikrotik_info: Append lease, hostname: " +
                            data['=host-name'])
            except KeyError:
                logger.error(
                    "get_mikrotik_info KeyErr: Could not append lease with data="
                    + str(data))
                print("pass in leases")
    if results == []:
        results = [{}]
    index = 0
    for reply, data in neighbors:
        if reply == '!re':
            try:
                ip = data['=address']
                found = False
                for i in range(len(resultsleases)):
                    if resultsleases[i]['ip'] == ip:
                        index = i
                        found = True
                        break
                if found:
                    resultsleases[index]['brand'] = data['=platform']
                    resultsleases[index]['firmware'] = data['=version']
                    if '=board' in data.keys():
                        resultsleases[index]['model'] = data['=board']
                    else:
                        resultsleases[index]['model'] = ''
                    if '=uptime' in data.keys():
                        resultsleases[index]['uptime'] = data['=uptime']
                    else:
                        resultsleases[index]['uptime'] = None
                    logger.info(
                        "get_mikrotik_info: Append additional info to lease with ip="
                        + ip)
                else:
                    logger.info(
                        "get_mikrotik_info: Could not find additional info for lease with ip="
                        + ip)
            except:
                logger.error(
                    "get_mikrotik_info Err: Could not append additional info to lease with data="
                    + str(data))
                print("pass in neighbors")
    speeds = apiros.talk(["/queue/simple/print"])
    if results == []:
        results = [{}]
    index = 0
    for reply, data in speeds:
        if reply == '!re':
            try:
                ip = data['=target']
                found = False
                for i in range(len(resultsleases)):
                    if resultsleases[i]['ip'] == ip:
                        index = i
                        found = True
                        break
                if found:
                    temp = data['=limit-at'].split("/")
                    us = temp[0]
                    ds = temp[1]
                    resultsleases[index]['download_speed'] = ds
                    resultsleases[index]['upload_speed'] = us
                    logger.info(
                        "get_mikrotik_info: Append download/upload speeds to lease with ip="
                        + ip)
                else:
                    logger.info(
                        "get_mikrotik_info: Could not find download/upload speeds for lease with ip="
                        + ip)
            except:
                logger.error(
                    "get_mikrotik_info Err: Could not append download/upload speeds to lease with data="
                    + str(data))
                print("pass in neighbors")
    results[0]['leases'] = resultsleases
    # except:
    #     print('33333333333333333333333333333333333')
    #     logger.error("get_mikrotik_info UnhandledErr: Unhandled exception happened")
    #     pass
    if len(results) == 0:
        return {}
    else:
        #print(results[0])
        return results[0]


def find_service_number(description):
    import re
    """ Encuentra los numeros de servicios en las descripciones del mikrotik """

    try:
        coincidences = re.findall('^([0-9]+)', description)
    except Exception as e:
        print(e)
        coincidences = None

    print(coincidences)
    if len(coincidences) > 0:
        coincidences = coincidences[0]
    return coincidences


def get_mikrotik_leases(mikrotik_ip, user, password):
    # sys.argv[1]
    s = None
    # print ('list_interfaces')
    print('1111111111111111111111111111111111111111111111111111')
    print('len de check')
    print(len(socket.getaddrinfo(mikrotik_ip, "8728", socket.AF_UNSPEC,socket.SOCK_STREAM)))
    print('========')
    for res in socket.getaddrinfo(mikrotik_ip, "8728", socket.AF_UNSPEC,
                                  socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res
        try:
            s = socket.socket(af, socktype, proto)
        except socket.error as msg:
            s = None
            continue
        try:
            s.connect(sa)
        except socket.error as msg:
            s.close()
            s = None
            continue
        break
    if s is None:

        print('333333333333333333333333333333333333333333333333333333333333')
        logger.error(
            'get_mikrotik_leases Err: Could not open socket for connection')
        return 'No se pudo abrir el socket para realizar la conección'

    print('222222222222222222222222222222222222222222222222222222')
    resultsleases = []
    try:
        s.settimeout(1)
        apiros = ApiRos(s)
        print('algo1')
        resp_login = apiros.login(user, password)
        print('algo2')
        for reply, data in resp_login:
            print('bucle')
            if reply == '!trap':
                logger.error(
                    "get_mikrotik_leases Err: Credentials given are not valid. Login unsucessful"
                )
                return "Las credenciales dadas no son válidas"
        print("algo3")
        print(
            '44444444444444444444444444444444444444444444444444444444444444444444444444444444'
        )
        leases = apiros.talk(["/ip/dhcp-server/lease/print"])
        print(leases)
        print(len(leases))
        neighbors = apiros.talk(["/ip/neighbor/print"])
        resultsleases = []
        for reply, data in leases:
            if reply == '!re':
                try:
                    if data["=status"] == "waiting":
                        statusbool = False
                    else:
                        statusbool = True

                    if '=active-mac-address' in data.keys():
                        active_mac = data["=active-mac-address"]
                    else:
                        active_mac = ""

                    if '=active-address' in data.keys():
                        active_address = data["=active-address"]
                    else:
                        active_address = ""

                    # var = obtain_first_numbers(data['=client-id'])
                    print('ESTA ES LA DATA EN Mikrotik', data)
                    try:
                        print('ESTE ES EL HOSTNAME', data.get('=host-name'))
                    except Exception as e:
                        print('ERROR CON EL HOSTNAME')
                    service_num = find_service_number(
                        data.get('=host-name', ''))
                    is_cpe, service_number = is_mikrotik_get_service(
                        data['=client-id'], service_num)
                    if data['=host-name']:
                        host_name = data['=host-name']
                    else:
                        host_name = ''
                    resultsleases.append(
                        dict(
                            description=host_name,
                            active_mac=active_mac,
                            mac=data['=mac-address'],
                            status=statusbool,
                            active_ip=active_address,
                            ip=data['=address'],
                            server=data['=server'],
                            # active_hostname=data.get('=active-host-name',None),
                            service_number=service_number,
                            active_client_id=data.get('=active-client-id',
                                                      None),
                            client_id=data['=client-id'],
                            last_seen=data.get('=last-seen', None),
                            status_lease=data.get('=status', None),
                            is_cpe=is_cpe,
                        ))
                    logger.info(
                        "get_mikrotik_leases: Append lease, hostname: " +
                        data['=host-name'])
                except KeyError as e:
                    logger.error(
                        "get_mikrotik_leases KeyErr: Could not append lease with data="
                        + str(data))
                    print("pass in leases")
                    print(f'ERROR CON {e}')
        print("ESTE ES EL RESULT LEASES")
        print(resultsleases)
        print(len(resultsleases))
        index = 0
        for reply, data in neighbors:
            if reply == '!re':
                try:
                    ip = data['=address']
                    found = False
                    for i in range(len(resultsleases)):
                        if resultsleases[i]['ip'] == ip:
                            index = i
                            found = True
                            break
                    if found:
                        resultsleases[index]['brand'] = data['=platform']
                        resultsleases[index]['firmware'] = data['=version']
                        if '=board' in data.keys():
                            resultsleases[index]['model'] = data['=board']
                        else:
                            resultsleases[index]['model'] = ''
                        if '=uptime' in data.keys():
                            resultsleases[index]['uptime'] = data['=uptime']
                        else:
                            resultsleases[index]['uptime'] = None
                        logger.info("get_mikrotik_leases: Append lease, ip: " +
                                    ip)
                except:
                    logger.error(
                        "get_mikrotik_leases Err: Could not append lease additional info with data="
                        + str(data))
                    print("pass in neighbors")
        speeds = apiros.talk(["/queue/simple/print"])
        index = 0
        for reply, data in speeds:
            if reply == '!re':
                try:
                    ip = data['=target']
                    found = False
                    for i in range(len(resultsleases)):
                        if resultsleases[i]['ip'] == ip:
                            index = i
                            found = True
                            break
                    if found:
                        temp = data['=limit-at'].split("/")
                        us = round(int(temp[0]) / 1000000, 0)
                        ds = round(int(temp[1]) / 1000000, 0)
                        resultsleases[index]['download_speed'] = ds
                        resultsleases[index]['upload_speed'] = us
                        logger.info(
                            "get_mikrotik_info: Append download/upload speeds to lease with ip="
                            + ip)
                    else:
                        logger.info(
                            "get_mikrotik_info: Could not find download/upload speeds for lease with ip="
                            + ip)
                except:
                    logger.error(
                        "get_mikrotik_info Err: Could not append download/upload speeds to lease with data="
                        + str(data))
                    print("pass in neighbors")
    except:
        logger.error(
            "get_mikrotik_leases UnhandledErr: Unhandled exception happened")
        pass
    return resultsleases


def get_radius_profile(mikrotik_ip, user, password):
    #sys.argv[1]
    s = None
    #print ('list_interfaces')
    for res in socket.getaddrinfo(mikrotik_ip, "8728", socket.AF_UNSPEC,
                                  socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res
        try:
            s = socket.socket(af, socktype, proto)
        except socket.error as msg:
            s = None
            continue
        try:
            s.connect(sa)
        except socket.error as msg:
            s.close()
            s = None
            continue
        break
    if s is None:
        logger.error(
            'get_radius_profile Err: Could not open socket for connection')
        return 'No se pudo abrir el socket para realizar la conección'

    results = []
    try:
        #print ('time')
        s.settimeout(1)
        apiros = ApiRos(s)
        resp_login = apiros.login(user, password)
        for reply, data in resp_login:
            if reply == '!trap':
                logger.error(
                    "get_radius_profile Err: Credentials given are not valid. Login unsucessful"
                )
                return "Las credenciales dadas no son válidas"
        response = apiros.talk([
            "/tool/user-manager/profile/profile-limitation/print",
            #"=.proplist=routerboard,model",
        ])
        limitations = apiros.talk(
            ["/tool/user-manager/profile/limitation/print"])
        for reply, data in response:
            if reply == '!re':
                try:
                    results.append(
                        dict(name=data['=profile'],
                             limitation_name=data['=limitation']))
                    logger.info(
                        "get_radius_profile: Append profile link with data=" +
                        str(data))
                except KeyError:
                    logger.error(
                        "get_radius_profile KeyErr: Could not ppend profile link with data="
                        + str(data))
                    print("pass in response")
        if results != []:
            for reply, data in limitations:
                if reply == '!re':
                    try:
                        limitation = data['=name']
                        found = False
                        for i in range(len(results)):
                            speed = results[i]
                            if speed['limitation_name'] == limitation:
                                found = True
                                index = i
                                break
                        if found:
                            results[index]['upload_speed'] = data[
                                '=rate-limit-rx']
                            results[index]['download_speed'] = data[
                                '=rate-limit-tx']
                            logger.info(
                                "get_radius_profile: Append profile speeds with data="
                                + str(data))
                    except KeyError:
                        logger.info(
                            "get_radius_profile: Could not append profile speeds with data="
                            + str(data))
                        print("pass in response")
    except:
        logger.error(
            "get_mikrotik_leases UnhandledErr: Unhandled exception happened")
        pass
    print(results)
    if len(results) == 0:
        return []
    else:
        #print(results[0])
        return results


def create_radius_profile(mikrotik_ip, user, password, data):
    #sys.argv[1]
    s = None
    #print ('list_interfaces')
    for res in socket.getaddrinfo(mikrotik_ip, "8728", socket.AF_UNSPEC,
                                  socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res
        try:
            s = socket.socket(af, socktype, proto)
        except socket.error as msg:
            s = None
            continue
        try:
            s.connect(sa)
        except socket.error as msg:
            s.close()
            s = None
            continue
        break
    if s is None:
        logger.error(
            'create_radius_profile Err: Could not open socket for connection')
        return 'No se pudo abrir el socket para realizar la conección'

    results = []
    try:
        #print ('time')
        s.settimeout(1)
        apiros = ApiRos(s)
        resp_login = apiros.login(user, password)
        for reply, data in resp_login:
            if reply == '!trap':
                logger.error(
                    "create_radius_profile Err: Credentials given are not valid. Login unsucessful"
                )
                return "Las credenciales dadas no son válidas"
        profile = data['profile']
        limitation = data['limitation']
        download_speed = data['download_speed']
        upload_speed = data['upload_speed']
        search_profile = apiros.talk([
            "/tool/user-manager/profile/print",
        ])
        found_profile = False
        for reply, data in search_profile:
            if reply == '!re':
                if data['=name'] == profile:
                    found_profile = True
                    logger.info(
                        "create_radius_profile: Found profile with given data")
        if not found_profile:
            logger.info(
                "create_radius_profile: Could not find profile with given data, creating one"
            )
            logger.info("create_radius_profile: Creating profile with name=" +
                        profile)
            create_profile = apiros.talk([
                "/tool/user-manager/profile/add",
                "=name=" + profile,
                "=owner=admin",
                "=starts-at=now",
            ])
            for reply, data in create_profile:
                if reply == '!trap':
                    msg = data['=message']
                    logger.error(
                        "create_radius_profile Err: Creating profile resulted in !trap from device. Msg:"
                        + msg)
                    return msg
            logger.info("create_radius_profile: Profile created, name=" +
                        profile)
        search_limitation = apiros.talk([
            "/tool/user-manager/profile/limitation/add",
        ])
        found_limitation = False
        for reply, data in search_limitation:
            if reply == '!re':
                if data['=name'] == limitation:
                    found_limitation = True
                    logger.info(
                        "create_radius_profile: Found limitation with given data: name="
                        + limitation)
        if not found_limitation:
            logger.info(
                "create_radius_profile: Could not find limitation with given data, creating one"
            )
            data = {
                "name": limitation,
                "download_speed": str(download_speed) + "M",
                "upload_speed": str(upload_speed) + "M",
            }
            logger.info(
                "create_radius_profile: Creating limitation with data=" +
                str(data))
            create_limitation = apiros.talk([
                "/tool/user-manager/profile/limitation/add",
                "=name=" + limitation,
                "=owner=admin",
                "=rate-limit-rx=" + str(download_speed) + "M",
                "=rate-limit-tx=" + str(upload_speed) + "M",
            ])
            for reply, data in create_limitation:
                if reply == '!trap':
                    msg = data['=message']
                    logger.error(
                        "create_radius_profile Err: Creating limitation resulted in !trap from device. Msg:"
                        + msg)
                    return msg
            logger.info("create_radius_profile: Limitation created, data=" +
                        str(data))
        logger.info(
            "create_radius_profile: Creating profile/limitation link with profile="
            + profile + " limitation=" + limitation)
        link = apiros.talk([
            "/tool/user-manager/profile/profile-limitation/add",
            "=profile=" + profile,
            "=limitation=" + limitation,
        ])
        for reply, data in link:
            if reply == '!trap':
                msg = data['=message']
                logger.error(
                    "create_radius_profile Err: Creating profile/limitation link resulted in !trap from device. Msg:"
                    + msg)
                return msg
        logger.info(
            "create_radius_profile: Created profile/limitation link with profile="
            + profile + " limitation=" + limitation)
    except:
        logger.error(
            "create_radius_profile UnhandledErr: Unhandled exception happened")
        return ""
    print("success")
    results.append({})
    return results[0]


def delete_radius_profile_witheverything(mikrotik_ip, user, password, data):
    #sys.argv[1]
    s = None
    #print ('delete_interface')
    for res in socket.getaddrinfo(mikrotik_ip, "8728", socket.AF_UNSPEC,
                                  socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res
        try:
            s = socket.socket(af, socktype, proto)
        except (socket.error, msg):
            s = None
            continue
        try:
            s.connect(sa)
        except (socket.error, msg):
            s.close()
            s = None
            continue
        break
    if s is None:
        logger.error(
            'delete_radius_profile_witheverything Err: Could not open socket for connection'
        )
        return 'No se pudo abrir el socket para realizar la conección'
    results = []
    try:
        s.settimeout(1)
        apiros = ApiRos(s)
        resp_login = apiros.login(user, password)
        for reply, data in resp_login:
            if reply == '!trap':
                logger.error(
                    "delete_radius_profile_witheverything Err: Credentials given are not valid. Login unsucessful"
                )
                return "Las credenciales dadas no son válidas"

        profile = data['profile']
        limitation = data['limitation']

        # Get and delete link
        logger.info(
            "delete_radius_profile_witheverything: Getting profile/limitation link number where profile="
            + profile + " limitation=" + limitation)
        search_link = apiros.talk([
            "/tool/user-manager/profile/profile-limitation/print",
        ])
        index = 0
        linkid = None
        for reply, data in search_link:
            if reply == '!trap':
                msg = data['=message']
                logger.error(
                    "delete_radius_profile_witheverything Err: Looking up profile/limitation link number resulted in !trap from device. Msg:"
                    + msg)
                return msg
            if reply == '!re':
                if data['=profile'] == profile and data[
                        '=limitation'] == limitation:
                    linkid = index
                    logger.info(
                        "delete_radius_profile_witheverything: Got profile/limitation link number="
                        + str(linkid))
                    break
            index += 1
        if linkid == None:
            logger.error(
                "delete_radius_profile_witheverything Err: Could not find profile/limitation link where profile="
                + profile + " limitation=" + limitation)
            return "No se encontró profile-limitation link en el RADIUS. Por favor actualice la lista de profile"
        logger.info(
            "delete_radius_profile_witheverything: Deleting profile/limitation link where number="
            + str(linkid))
        delete_link = apiros.talk([
            "/tool/user-manager/profile/profile-limitation/remove",
            "=numbers=" + str(linkid)
        ])
        keepgoing = False
        for reply, data in delete_link:
            if reply == '!done':
                keepgoing = True
                logger.info(
                    "delete_radius_profile_witheverything: Deleted profile/limitation link where number="
                    + str(linkid))
            if reply == '!trap':
                msg = data['=message']
                logger.error(
                    "delete_radius_profile_witheverything Err: Deleting profile/limitation link where number="
                    + str(linkid) + " resulted in !trap from device. Msg:" +
                    msg)
        if not keepgoing:
            logger.error(
                "delete_radius_profile_witheverything Err: Could not delete profile/limitation link where number="
                + str(linkid))
            return "Ocurrió un error inesperado, no se pudo eliminar el profile/limitation link"

        # Get and delete profile
        logger.info(
            "delete_radius_profile_witheverything: Getting profile number where name="
            + profile)
        search_profile = apiros.talk([
            "/tool/user-manager/profile/print",
        ])
        index = 0
        profileid = None
        for reply, data in search_profile:
            if reply == '!trap':
                msg = data['=message']
                logger.error(
                    "delete_radius_profile_witheverything Err: Looking up profile number resulted in !trap from device. Msg:"
                    + msg)
                return msg
            if reply == '!re':
                if data['=name'] == profile:
                    profileid = index
                    logger.info(
                        "delete_radius_profile_witheverything: Got profile number="
                        + str(profileid))
                    break
            index += 1
        if profileid == None:
            logger.error(
                "delete_radius_profile_witheverything Err: Could not find profile where name="
                + profile)
            return "No se encontró el profile en el RADIUS. Por favor actualice la lista de profile"
        logger.info(
            "delete_radius_profile_witheverything: Deleting profile where number="
            + str(profileid))
        delete_profile = apiros.talk([
            "/tool/user-manager/profile/remove", "=numbers=" + str(profileid)
        ])
        keepgoingProfile = False
        for reply, data in delete_profile:
            if reply == '!done':
                keepgoingProfile = True
                logger.info(
                    "delete_radius_profile_witheverything: Deleted profile where number="
                    + str(profileid))
            if reply == '!trap':
                msg = data['=message']
                logger.error(
                    "delete_radius_profile_witheverything Err: Deleting profile where number="
                    + str(profileid) + " resulted in !trap from device. Msg:" +
                    msg)
        if not keepgoingProfile:
            logger.error(
                "delete_radius_profile_witheverything Err: Could not delete profile where number="
                + str(profileid))
            return "Ocurrió un error inesperado. Se eliminó el profile/limitation link pero no se pudo eliminar el profile ni la limitation en el dispositivo"

        # Get and delete limitation
        logger.info(
            "delete_radius_profile_witheverything: Getting limitation number where name="
            + limitation)
        search_limitation = apiros.talk([
            "/tool/user-manager/profile/limitation/print",
        ])
        index = 0
        limitationid = None
        for reply, data in search_limitation:
            if reply == '!trap':
                msg = data['=message']
                logger.error(
                    "delete_radius_profile_witheverything Err: Looking up limitation number resulted in !trap from device. Msg:"
                    + msg)
                return msg
            if reply == '!re':
                if data['=name'] == limitation:
                    limitationid = index
                    logger.info(
                        "delete_radius_profile_witheverything: Got limitation number="
                        + str(limitationid))
                    break
            index += 1
        if limitationid == None:
            logger.error(
                "delete_radius_profile_witheverything Err: Could not find limitation where name="
                + limitation)
            return "No se encontró el profile en el RADIUS. Por favor actualice la lista de profile"
        logger.info(
            "delete_radius_profile_witheverything: Deleting limitation where number="
            + str(limitationid))
        delete_limitation = apiros.talk([
            "/tool/user-manager/profile/limitation/remove",
            "=numbers=" + str(limitationid)
        ])
        keepgoingLimitation = False
        for reply, data in delete_limitation:
            if reply == '!done':
                keepgoingLimitation = True
                logger.info(
                    "delete_radius_profile_witheverything: Deleted limitation where number="
                    + str(limitationid))
            if reply == '!trap':
                msg = data['=message']
                logger.error(
                    "delete_radius_profile_witheverything Err: Deleting limitation where number="
                    + str(limitationid) +
                    " resulted in !trap from device. Msg:" + msg)
        if not keepgoingLimitation:
            logger.error(
                "delete_radius_profile_witheverything Err: Could not delete limitation where number="
                + str(limitationid))
            return "Ocurrió un error inesperado. Se ha eliminado el profile/limitation link, más no se pudo eliminar la limitation en el dispositivo"
    except:
        logger.error(
            "delete_radius_profile_witheverything UnhandledErr: Unhandled exception happened"
        )
        return ""
    print("success")
    results.append({})
    return results[0]


def delete_radius_profile_onlylink(mikrotik_ip, user, password, data):
    #sys.argv[1]
    s = None
    #print ('delete_interface')
    for res in socket.getaddrinfo(mikrotik_ip, "8728", socket.AF_UNSPEC,
                                  socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res
        try:
            s = socket.socket(af, socktype, proto)
        except (socket.error, msg):
            s = None
            continue
        try:
            s.connect(sa)
        except (socket.error, msg):
            s.close()
            s = None
            continue
        break
    if s is None:
        logger.error(
            'delete_radius_profile_onlylink Err: Could not open socket for connection'
        )
        return 'No se pudo abrir el socket para realizar la conección'
    results = []
    try:
        s.settimeout(1)
        apiros = ApiRos(s)
        resp_login = apiros.login(user, password)
        for reply, data in resp_login:
            if reply == '!trap':
                logger.error(
                    "delete_radius_profile_onlylink Err: Credentials given are not valid. Login unsucessful"
                )
                return "Las credenciales dadas no son válidas"

        profile = data['profile']
        limitation = data['limitation']

        # Get and delete link
        logger.info(
            "delete_radius_profile_onlylink: Getting profile/limitation link number where profile="
            + profile + " limitation=" + limitation)
        search_link = apiros.talk([
            "/tool/user-manager/profile/profile-limitation/print",
        ])
        index = 0
        linkid = None
        for reply, data in search_link:
            if reply == '!trap':
                msg = data['=message']
                logger.error(
                    "delete_radius_profile_onlylink Err: Looking up profile/limitation link number resulted in !trap from device. Msg:"
                    + msg)
                return msg
            if reply == '!re':
                if data['=profile'] == profile and data[
                        '=limitation'] == limitation:
                    linkid = index
                    logger.info(
                        "delete_radius_profile_onlylink: Got profile/limitation link number="
                        + str(linkid))
                    break
            index += 1
        if linkid == None:
            logger.error(
                "delete_radius_profile_onlylink Err: Could not find profile/limitation link where profile="
                + profile + " limitation=" + limitation)
            return "No se encontró profile-limitation link en el RADIUS. Por favor actualice la lista de profile"
        logger.info(
            "delete_radius_profile_onlylink: Deleting profile/limitation link where number="
            + str(linkid))
        delete_link = apiros.talk([
            "/tool/user-manager/profile/profile-limitation/remove",
            "=numbers=" + str(linkid)
        ])
        keepgoing = False
        for reply, data in delete_link:
            if reply == '!done':
                keepgoing = True
                logger.info(
                    "delete_radius_profile_onlylink: Deleted profile/limitation link where number="
                    + str(linkid))
            if reply == '!trap':
                msg = data['=message']
                logger.error(
                    "delete_radius_profile_onlylink Err: Deleting profile/limitation link where number="
                    + str(linkid) + " resulted in !trap from device. Msg:" +
                    msg)
        if not keepgoing:
            logger.error(
                "delete_radius_profile_onlylink Err: Could not delete profile/limitation link where number="
                + str(linkid))
            return "Ocurrió un error inesperado, no se pudo eliminar el profile/limitation link"
    except:
        logger.error(
            "delete_radius_profile_onlylink UnhandledErr: Unhandled exception happened"
        )
        print("Something failed in creating a profile in the device")
        return ""
    print("success")
    results.append({})
    return results[0]


def convert_uptime(uptime_str):
    date, time = uptime_str.split(" ")
    month, day, year = date.split("/")
    if "apr" in month:
        month = "04"
    return year + "-" + month + "-" + day + " " + time


def list_interfaces(mikrotik_ip, user, password):
    #sys.argv[1]
    s = None
    #print ('list_interfaces')
    for res in socket.getaddrinfo(mikrotik_ip, "8728", socket.AF_UNSPEC,
                                  socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res
        try:
            s = socket.socket(af, socktype, proto)
        except (socket.error, msg):
            s = None
            continue
        try:
            s.connect(sa)
        except (socket.error, msg):
            s.close()
            s = None
            continue
        break
    if s is None:
        # print('could not open socket')
        sys.exit(1)

    results = []
    try:
        #print ('time')
        s.settimeout(1)
        apiros = ApiRos(s)
        apiros.login(user, password)

        response = apiros.talk([
            "/interface/print",
            "=.proplist=.id,name,type,mac-address,last-link-up-time,status",
        ])

        for reply, data in response:
            if reply == '!re':
                try:
                    newuptime = convert_uptime(data['=last-link-up-time'])
                    results.append(
                        dict(name=data['=name'],
                             mac=data['=mac-address'],
                             type=data['=type'],
                             uptime=newuptime,
                             id=data['=.id'],
                             status=True))
                except KeyError:
                    newuptime = None
                    results.append(
                        dict(name=data['=name'],
                             mac=data['=mac-address'],
                             type=data['=type'],
                             uptime=newuptime,
                             id=data['=.id'],
                             status=False))
        #print (results)
    except:
        print("Connection timed out")
    return results


def create_interface(mikrotik_ip, user, password, data):
    #sys.argv[1]
    s = None
    #print ('create_interface')
    for res in socket.getaddrinfo(mikrotik_ip, "8728", socket.AF_UNSPEC,
                                  socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res
        try:
            s = socket.socket(af, socktype, proto)
        except (socket.error, msg):
            s = None
            continue
        try:
            s.connect(sa)
        except (socket.error, msg):
            s.close()
            s = None
            continue
        break
    if s is None:
        # print('could not open socket')
        sys.exit(1)

    results = []
    #print ('time')
    s.settimeout(1)
    apiros = ApiRos(s)
    apiros.login(user, password)

    id_given = data['id']
    alias = data['alias']
    #ip = data['ip']
    #model = data['model']
    description = data['description']
    #cpeutp_total = data['cpeutp_total']
    #cpeutp_active = data['cpeutp_active']
    #uptime = data['uptime']

    response = apiros.talk([
        "/interface/vlan/add",
        "=vlan-id=" + str(id_given),
        "=name=" + alias,
        "=interface=ether1",
        "=comment=" + description,
    ])
    return response


def delete_interface(mikrotik_ip, user, password, data):
    #sys.argv[1]
    s = None
    #print ('delete_interface')
    for res in socket.getaddrinfo(mikrotik_ip, "8728", socket.AF_UNSPEC,
                                  socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res
        try:
            s = socket.socket(af, socktype, proto)
        except (socket.error, msg):
            s = None
            continue
        try:
            s.connect(sa)
        except (socket.error, msg):
            s.close()
            s = None
            continue
        break
    if s is None:
        # print('could not open socket')
        sys.exit(1)
    response = []
    try:
        #print ('time')
        s.settimeout(1)
        apiros = ApiRos(s)
        apiros.login(user, password)

        alias = "\"" + data['alias'] + "\""

        reply, _ = apiros.talk([
            "/interface/vlan/print",
            "=from=" + alias,
            "=.proplist=.id",
        ])
        id_given = reply[1]['=.id']
        response = apiros.talk([
            "/interface/vlan/remove",
            "=.id=" + id_given,
        ])
    except:
        pass
    return response


def update_interface(mikrotik_ip, user, password, data, newdata):
    #sys.argv[1]
    s = None
    print('update_interface')
    for res in socket.getaddrinfo(mikrotik_ip, "8728", socket.AF_UNSPEC,
                                  socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res
        try:
            s = socket.socket(af, socktype, proto)
        except (socket.error, msg):
            s = None
            continue
        try:
            s.connect(sa)
        except (socket.error, msg):
            s.close()
            s = None
            continue
        break
    if s is None:
        # print('could not open socket')
        sys.exit(1)

    results = []
    #print ('time')
    s.settimeout(1)
    apiros = ApiRos(s)
    apiros.login(user, password)

    alias = "\"" + data['alias'] + "\""

    reply, _ = apiros.talk([
        "/interface/vlan/print",
        "=from=" + alias,
        "=.proplist=.id",
    ])
    id_given = reply[1]['=.id']
    list_command = [
        "/interface/vlan/set",
        "=.id=" + id_given,
    ]

    if newdata['alias']:
        alias = newdata['alias']
        list_command.append("=name=" + alias)
    if newdata['id']:
        vlan_id = newdata['id']
        list_command.append("=vlan-id=" + str(vlan_id))
    if newdata['description']:
        description = newdata['description']
        list_command.append("=comment=" + description)

    response = apiros.talk(list_command)
    return response


def get_leases(mikrotik_ip, user, password):
    s = None
    for res in socket.getaddrinfo(mikrotik_ip, "8728", socket.AF_UNSPEC,
                                  socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res
        try:
            s = socket.socket(af, socktype, proto)
        except (socket.error, msg):
            s = None
            continue
        try:
            s.connect(sa)
        except (socket.error, msg):
            s.close()
            s = None
            continue
        break
    if s is None:
        # print('could not open socket')
        sys.exit(1)

    s.settimeout(1)
    apiros = ApiRos(s)
    try:
        apiros.login(user, password)
    except:
        notify_error_mikrotik(mikrotik_ip, user)
        logger.error("Can't login with user %s to Drop node %s", user,
                     mikrotik_ip)
        return

    response = apiros.talk([
        "/ip/dhcp-server/lease/print",
        "=.proplist=.id,address,mac-address,status,host-name",
    ])

    results = []

    for reply, data in response:
        if reply == '!re':
            try:
                results.append(
                    dict(
                        address=data['=address'],
                        mac_address=data['=mac-address'],
                        status=data['=status'],
                        host_name=data.get('=host-name', ''),
                    ))
            except KeyError:
                pass
    return results


def get_routeros_api(mikrotik_ip, user, password):
    connection = routeros_api.RouterOsApiPool(mikrotik_ip,
                                              username=user,
                                              password=password,
                                              plaintext_login=True)
    api = connection.get_api()
    return api


def _get_client_status(mikrotik_ip, user, password, client_ip):
    try:
        api = get_routeros_api(mikrotik_ip, user, password)
        # addresses = api.get_resource('/ip/address').get()
        addresses = api.get_resource('/ip/firewall/address-list/').get()

        addresses_f = list(
            filter(lambda x: x['list'] == 'CORTES CLIENTES', addresses))

        cutting = list(filter(lambda x: x['address'] == client_ip,
                              addresses_f))

        if len(cutting) > 0:
            status = False
        elif len(cutting) == 0:
            status = True
    except Exception as e:
        # print(e)
        status = str(e)

    return status


def get_address_list(mikrotik_ip, user, password):
    from utp.models import Mikrotik, AddressList

    try:
        api = get_routeros_api(mikrotik_ip, user, password)
        addresses = api.get_resource('/ip/address').get()
    except Exception as e:
        print(e)
        return

    node = Mikrotik.objects.get(ip__ip=mikrotik_ip)

    for addr in addresses:
        network, created = IPTable.objects.get_or_create(ip=addr['network'])
        interface = addr['interface']
        address_mask = addr['address'].split('/')
        mask = int(address_mask[1])
        address, created = IPTable.objects.get_or_create(ip=address_mask[0])

        AddressList.objects.get_or_create(node=node,
                                          network=network,
                                          address=address,
                                          mask_address=mask,
                                          interface=interface)


def get_neighbors(mikrotik_ip, user, password):
    """
        throw Exception when can`t connect to mikrotik
    """

    from utp.models import Interface, Neighbor, Mikrotik

    api = get_routeros_api(mikrotik_ip, user, password)
    neighbors = api.get_resource('/ip/neighbor').get()

    import pprint
    # pprint.pprint(neighbors)
    node = Mikrotik.objects.get(ip__ip=mikrotik_ip)

    date = datetime.now()

    for neighbor in neighbors:
        pprint.pprint(neighbor)
        if neighbor.get('address', None):
            ip, created = IPTable.objects.get_or_create(
                ip=neighbor.get('address', None))
        else:
            ip = None

        if neighbor.get('mac-address', None):
            formatted_mac = MACTable.objects.format_mac(
                neighbor.get('mac-address', None))
            mac, created = MACTable.objects.get_or_create(mac=formatted_mac)
        else:
            mac = None

        interface, created = Interface.objects.get_or_create(name=neighbor.get(
            'interface-name', neighbor.get('interface')),
                                                             node=node)

        Neighbor.objects.get_or_create(created=date,
                                       interface=interface,
                                       address=ip,
                                       mac_address=mac,
                                       identity=neighbor.get('identity'),
                                       board_name=neighbor.get('board'),
                                       uptime=neighbor.get('uptime'),
                                       node=node)


def cutting_replacement(service_number, type_cr='reposicion'):
    from utp.models import (MikrotikConnection, CpeUtp)
    """ 
    Función que aplica corte ó reposición se acuerdo a un número de
    servicio y a un param denominado type_cr que indica cual de las
    acciones ejecutar.
    """

    if (service_number):
        print(service_number)

        try:
            # cpes = CpeUtp.objects.filter(service__number=service_number, primary=True)
            cpes = CpeUtp.objects.filter(service__number=service_number)
        except CpeUtp.DoesNotExist as e:
            print(str(e))

        for cpe in cpes:
            mikrotik_connections = MikrotikConnection.objects.filter(
                mikrotik=cpe.nodo_utp)
            contract = {}
            contract['mikrotik_ip'] = cpe.nodo_utp.ip.ip
            contract['primary_equipment'] = cpe
            contract['primary_equipment_ip'] = cpe.ip.ip
            contract['slug'] = service_number
            contract['number'] = service_number

            for mc in mikrotik_connections:
                contract['mikrotik_username'] = mc.username
                contract['mikrotik_password'] = mc.password

                if (type_cr == 'corte'):
                    ok, detail = drop_client(contract)
                elif (type_cr == 'reposicion'):
                    ok, detail = activate_client(contract)

                if ok:

                    conector = 'el' if type_cr == 'corte' else 'la'
                    print(
                        f'Se ha aplicado { conector } {type_cr} al servicio nro. {service_number}'
                    )

    else:
        print("Número de servicio no definido")
