import requests
import uuid
from datetime import timedelta
from django.db import models
from django.conf import settings
from django.core.validators import MaxValueValidator
from django.utils import timezone
from common.utils import timedelta_format
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from common.models import BaseModel, Node
from address.models import Address
from .cl_territory import COMMUNE_CHOICES
from .mikrotik import get_dropped, get_leases
from simple_history.models import HistoricalRecords

from devices.models import Device, ModelDevice, IPTable
from customers.models import Plan, Service


from django.template.defaultfilters import truncatechars

from django.contrib.contenttypes.fields import GenericRelation


class Group(BaseModel):
    name = models.CharField(_('name'), max_length=20, unique=True)

class Radius(Device):
    alias = models.CharField(max_length=80, db_index=True)
    # ip = models.GenericIPAddressField(_('ip'), protocol='IPv4', db_index=True)
    ip = models.ForeignKey(IPTable, on_delete=models.CASCADE, db_index=True)

    mac_eth1 = models.CharField(max_length=17, db_index=True, null=True, blank=True)
    status = models.BooleanField(default=False)
    model = models.ForeignKey(ModelDevice,
                              on_delete=models.PROTECT)
    description = models.TextField()
    uptime = models.DateTimeField(null=True, blank=True)
    time_uptime = models.DateTimeField(null=True, blank=True)
    def __str__(self):
        if self.ip:
            return f"{self.alias} {self.ip.ip}"
        return self.alias

    def get_uptime(self):
        return self.uptime

    def set_status_offline(self):
        self.status = False
        self.save()

    def set_status_online(self):
        self.status = True
        self.save()

    @property
    def get_uptime_delta(self):
        try:
            if self.status:
                u = timezone.now() - self.uptime
                return timedelta_format(u)
            return None
        except Exception as e:
            print (e)
            return None
            
    def save(self, *args, **kwargs):
        super(Radius, self).save(*args, **kwargs)

    class Meta:
        ordering = ["alias"]
        unique_together = ("alias", "ip")


class RadiusConnection(BaseModel):
    username = models.CharField(max_length=25)
    password = models.CharField(max_length=30)
    radius = models.ForeignKey(Radius, blank=True, on_delete=models.CASCADE)
    connections = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return self.radius.alias

    def release_connection(self):
        if self.connections > 0:
            self.connections -= 1
            self.save()

    def connection_usage(self):
        self.connections += 1
        self.save()

    def get_ip(self):
        return self.radius.ip

    class Meta:
        ordering = ["username"]



class Mikrotik(Device):
    WIRED = 1
    PENDING = 2
    IN_PROGRESS = 3
    NO_LABEL = 4
    DISCARDED = 5

    STATUS_CHOICES = ((WIRED, _('Cableado')),
                      (PENDING, _('Por retirar')),
                      (IN_PROGRESS, _('En proceso')),
                      (NO_LABEL, _('Sin clasificar')),
                      (DISCARDED, _('Descartado')))

    STATUS_CLASSES = {WIRED: 'success',
                      PENDING: 'danger',
                      IN_PROGRESS: 'warning'}

    MIKROTIK_C = 1
    ANTENNA_C = 2
    TYPE_CHOICES = ((MIKROTIK_C, _('Nodo')),
                      (ANTENNA_C, _('Antena')))

    name = models.CharField(max_length=80,blank=True)
    alias = models.CharField(max_length=80, db_index=True)
    # ip = models.GenericIPAddressField(_('ip'), protocol='IPv4', db_index=True)
    ip = models.ForeignKey(IPTable, on_delete=models.CASCADE, db_index=True)
    # mac_eth1 = models.CharField(max_length=17, db_index=True, null=True, blank=True)
    mac_eth1 = models.ForeignKey('devices.MACTable', on_delete=models.CASCADE, null=True, blank=True, 
                                    db_index=True)
    # status = models.BooleanField(default=False)
    model = models.ForeignKey(ModelDevice,
                              on_delete=models.PROTECT)
    architecture = models.CharField(max_length=80,blank=True)
    description = models.TextField()
    firmware = models.CharField(max_length=100, null=True, blank=True)
    serial = models.CharField(max_length=80,blank=True)
    #cpeutps = models.PositiveSmallIntegerField(default=0)
    devices = models.PositiveSmallIntegerField(default=0)
    #cpeutp_active = models.PositiveSmallIntegerField(default=0)
    ram = models.CharField(max_length=10, null=True, blank=True)
    cpu = models.CharField(max_length=10, null=True, blank=True)
    storage = models.CharField(max_length=10, null=True, blank=True)
    temperature_cpu = models.CharField(max_length=10, null=True, blank=True)
    temperature = models.CharField(max_length=10, null=True, blank=True)
    voltage = models.PositiveSmallIntegerField(default=0)
    power_consumption = models.PositiveSmallIntegerField(default=0)
    uptime = models.DateTimeField(null=True, blank=True)
    time_uptime = models.DateTimeField(null=True, blank=True)
    comment = models.TextField(max_length=300,null=True, blank=True)
    last_time_updated_leases = models.DateTimeField(null=True, blank=True)
    
    # campos de infra matrix
    expected_power = models.CharField(_('expected power'), max_length=255, default='', blank=True)
    
    status = models.PositiveIntegerField(_('status'), default=WIRED, null=True, choices=STATUS_CHOICES)
    
    address_pulso = models.ForeignKey(Address, on_delete=models.CASCADE)
    # address = models.CharField(_('address'), max_length=255)
    # street = models.CharField(_('street'), max_length=75, blank=True)
    # house_number = models.CharField(_('house number'), max_length=75, blank=True)
    towers = models.PositiveSmallIntegerField(default=0)
    # commune = models.CharField(_('commune'), max_length=30)
    # location = models.CharField(_('location'), max_length=60, choices=COMMUNE_CHOICES)
    
    
    parent = models.ForeignKey('self', verbose_name=_('parent'), null=True, blank=True, on_delete=models.CASCADE)
    default_connection_node = models.ForeignKey('self', verbose_name=_('default connection node'), null=True, blank=True, on_delete=models.CASCADE, related_name='+')

    apartments = models.PositiveIntegerField(default=0)
    phone = models.CharField(_('phone'), max_length=16, default='', blank=True)
    # notes = models.TextField(_('notes'), blank=True, null=True)
     
    # ip_range = models.GenericIPAddressField(_('ip range'), protocol='IPv4', unique=True, blank=True, null=True)
    # ip_range = models.OneToOneField(IPTable, on_delete=models.CASCADE, unique=True, related_name='mikrotik_ranges', null=True, blank=True)
    ip_range = models.ManyToManyField('devices.IPRange', blank=True)
    
    #no_address_binding = models.BooleanField('no asociar direcciones', default=False)
    plans = models.ManyToManyField(Plan)
    group = models.ForeignKey(Group, verbose_name=_('group'), null=True, blank=True, on_delete=models.CASCADE)
    matrix_seller = models.PositiveSmallIntegerField(null=True, blank=True) 
    plan_order_kinds = models.ManyToManyField('PlanOrderKind', verbose_name='tipo OS habilitadas', blank=True)
    
    # con este Campo se puede diferenciar de una antenna y un nodo 
    type = models.PositiveIntegerField(_('type'), default=MIKROTIK_C, choices=TYPE_CHOICES)

    pics = GenericRelation('documents.Pic')
    documents = GenericRelation('documents.Document')

    radius = models.ForeignKey(Radius, blank=True, null=True, on_delete=models.SET_NULL)
    logic_node = models.ForeignKey(Node, on_delete=models.PROTECT, null=True,  blank=True)
    
    def __str__(self):
        if self.ip:
            return f"{self.alias} {self.ip.ip}"
        return self.alias
        
    @property
    def last_time_updated_leases_op(self):
        try:
            update_time = self.last_time_updated_leases.strftime("%d/%m/%Y %H:%M:%S")
        except:
            update_time = 'No disponible'
        return update_time

    @property
    def cpeutps(self):
        cpes = CpeUtp.objects.filter(nodo_utp__pk=self.pk).count()
        return cpes

    @property
    def cpeutp_active(self):
        cpe_active = CpeUtp.objects.filter(nodo_utp__pk=self.pk, status=True).count()
        return cpe_active

    @property
    def address(self):
        return f'{self.address_pulso.street} {self.address_pulso.number}'
    
    @property
    def house_number(self):
        return f'{self.address_pulso.number}'

    @property
    def street(self):
        return f'{self.address_pulso.street}'

    @property
    def commune(self):
        return f'{self.address_pulso.commune}'

    @property
    def street_id(self):
        return f'{self.address_pulso.street_id}'

    @property
    def commune_id(self):
        return f'{self.address_pulso.commune_id}'

    @property
    def location(self):
        return f'{self.address_pulso.commune}'
        
    @property
    def floor(self):
        return f'{self.address_pulso.floor}'
    @property
    def tower(self):
        return f'{self.address_pulso.tower}'


    @property
    def region(self):
        return f'{self.address_pulso.region}'
    @property
    def region_id(self):
        return f'{self.address_pulso.region_id}'

    def get_uptime(self):
        return self.uptime

    # def set_status_offline(self):
    #     self.status = False
    #     self.save()

    # def set_status_online(self):
    #     self.status = True
    #     self.save()

    @property
    def get_uptime_delta(self):
        try:
            if self.status == 1:
                try:
                    u = timezone.now() - self.uptime
                except Exception as e:
                    u = timezone.now() - (timezone.now() - timedelta(days=1))
                return timedelta_format(u)
            return None
        except Exception as e:
            print (e)
            return None

    @property
    def get_uptime_date(self):
        return self.uptime.strftime('Desde %d/%m/%Y a las %H:%M')

    @property
    def get_radius_alias(self):
        if self.radius:
            return self.radius.alias
        else:
            return None
    

    # Properties from matrix
    @property
    def available_ips(self):
        try:
            ip_range = self.ip_range
            valid = set(['.'.join(ip_range.split('.')[:-1]+[str(n)]) for n in range(2,255)])
            used = set(Gear.objects.filter(ip__ip__startswith='.'.join(ip_range.split('.')[:-1])).values_list('ip', flat=True))
            return sorted(list(valid - used), key=lambda x: int(x.split('.')[-1]))
        except:
            return []

    def mapGears(self, gear):
        return [
            gear.id,
            gear.model.model,
            gear.description,
            gear.ip.ip,
            gear.mac.mac,
            gear.vlan,
        ]
    @property
    def get_gears(self):
        return map(self.mapGears, list(self.gear_set.all()))

    def refresh_network_equipment(self):
        from devices.models import MACTable

        NotFoundEquipment.objects.all().delete()
        # reset current node's mismatch status
        
        # cambio
        self.service_set.update(network_mismatch=False, node_mismatch_id=None)
        # Service.objects.filter(cpeutp_set__nodo_utp=self).update(network_mismatch=False, node_mismatch_id=None)

        leases = self.leases()
        for ll in  leases:
            hn = ll['host_name']
            addr = ll['address']
            mac = ll['mac_address']

            number = hn.strip().split('-')[0]
            try:
                # cambio
                cc = Service.objects.get(number=int(number))
                # cc = CpeUtp.objects.filter(service_number = int(number))

                # mark service as network mismatch if found in other node
                if self.pk != cc.node.pk:
                    # cambio
                    Service.objects.filter(pk=cc.pk).update(network_mismatch=True, node_mismatch_id=self.id)
                    print("Node mismatch with cpe node")

                # change ip of primary if is set
                primary = cc
                mact, created = MACTable.objects.get_or_create(mac=mac)
                    
                if primary:
                    if mact == primary.mac:
                        primary.ip = addr
                        primary.save()
                    else:
                        # save old primary as 0.0.0.0 for history
                        primary.ip = '0.0.0.0'
                        primary.primary = False
                        primary.save()

                        # create new one
                        # with new ip and mac and set as primary
                        #  ¿ CPE duplicado ?
                        # NetworkEquipment.objects.create(brand='AUTO',
                        #                                 model='AUTO',
                        #                                 ip=addr,
                        #                                 mac=mac,
                        #                                 primary=True,
                        #                                 service=cc)
                        CpeUtp.objects.create(ip = addr, 
                                                mac=mact, 
                                                service = cc,
                                                primary=True,
                                                serial='??????????')
                else:
                    # cc.networkequipment_set.update(primary=False)
                    # NetworkEquipment.objects.create(brand='AUTO',
                    #                                 model='AUTO',
                    #                                 ip=addr,
                    #                                 mac=mac,
                    #                                 primary=True,
                    #                                 service=cc)
                    CpeUtp.objects.create(ip = addr, 
                                            mac=mact, 
                                            service = cc, 
                                            primary=True,
                                            serial='??????????')

            except (ValueError, CpeUtp.DoesNotExist) as e:
                NotFoundEquipment.objects.create(ip=addr,
                                                 mac=mact,
                                                 host=hn)

        
        dropped = self.dropped_clients()

        if (dropped != None):
            self.service_set.filter(number__in=dropped).update(seen_connected=False)
            self.service_set.exclude(number__in=dropped).update(seen_connected=True)
        

    def leases(self):
        try:
            leases = get_leases(self.mikrotik_ip,
                                self.mikrotik_username,
                                self.mikrotik_password)
            return sorted(leases, key=itemgetter('host_name'))
        except:
            return []

    @property
    def get_leases(self):
        return self.leases()

    def dropped_clients(self):
        try:
            dropped = get_dropped(self.mikrotik_ip,
                                  self.mikrotik_username,
                                  self.mikrotik_password)
            return dropped
        except:
            return []

    @property
    def get_status_class(self):
        return self.STATUS_CLASSES.get(self.status, 'default')

    @property
    def truncated_code(self):
        return truncatechars(self.alias, 30)

    @property
    def composite_address(self):
        return "{} {}".format(self.street, self.house_number)

    @property
    def best_address(self):
        return self.composite_address.strip() or self.address

    @property
    def truncated_address(self):
        return truncatechars(self.best_address, 30)
    
    # def __str__(self):
    #     return "{} ({})".format(self.truncated_address, self.truncated_code)

    def save(self, *args, **kwargs):
        super(Mikrotik, self).save(*args, **kwargs)

    class Meta:
        ordering = ["alias"]
        unique_together = ("alias", "ip")


class MikrotikConnection(BaseModel):
    username = models.CharField(max_length=25)
    password = models.CharField(max_length=30)
    mikrotik = models.ForeignKey(Mikrotik, blank=True, on_delete=models.CASCADE)
    connections = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return self.mikrotik.alias

    def release_connection(self):
        if self.connections > 0:
            self.connections -= 1
            self.save()

    def connection_usage(self):
        self.connections += 1
        self.save()

    def get_ip(self):
        return self.mikrotik.ip

    class Meta:
        ordering = ["username"]




class Antenna(Device):
    WIRED = 1
    PENDING = 2
    IN_PROGRESS = 3
    NO_LABEL = 4
    DISCARDED = 5

    STATUS_CHOICES = ((WIRED, _('Cableado')),
                      (PENDING, _('Por retirar')),
                      (IN_PROGRESS, _('En proceso')),
                      (NO_LABEL, _('Sin clasificar')),
                      (DISCARDED, _('Descartado')))

    STATUS_CLASSES = {WIRED: 'success',
                      PENDING: 'danger',
                      IN_PROGRESS: 'warning'}


    name = models.CharField(max_length=80,blank=True)
    alias = models.CharField(max_length=80, db_index=True)
    # ip = models.GenericIPAddressField(_('ip'), protocol='IPv4', db_index=True)
    ip = models.ForeignKey(IPTable, on_delete=models.CASCADE, db_index=True)
    # mac_eth1 = models.CharField(max_length=17, db_index=True, null=True, blank=True)
    mac_eth1 = models.ForeignKey('devices.MACTable', on_delete=models.CASCADE, null=True, blank=True, 
                                    db_index=True)
    # status = models.BooleanField(default=False)
    model = models.ForeignKey(ModelDevice,
                              on_delete=models.PROTECT)
    architecture = models.CharField(max_length=80,blank=True)
    description = models.TextField()
    firmware = models.CharField(max_length=100, null=True, blank=True)
    serial = models.CharField(max_length=80,blank=True)
    cpeutps = models.PositiveSmallIntegerField(default=0)
    devices = models.PositiveSmallIntegerField(default=0)
    cpeutp_active = models.PositiveSmallIntegerField(default=0)
    ram = models.CharField(max_length=10, null=True, blank=True)
    cpu = models.CharField(max_length=10, null=True, blank=True)
    storage = models.CharField(max_length=10, null=True, blank=True)
    temperature_cpu = models.CharField(max_length=10, null=True, blank=True)
    temperature = models.CharField(max_length=10, null=True, blank=True)
    voltage = models.PositiveSmallIntegerField(default=0)
    power_consumption = models.PositiveSmallIntegerField(default=0)
    uptime = models.DateTimeField(null=True, blank=True)
    time_uptime = models.DateTimeField(null=True, blank=True)
    comment = models.TextField(max_length=300,null=True, blank=True)
    
    # campos de infra matrix
    expected_power = models.CharField(_('expected power'), max_length=255, default='', blank=True)
    
    status = models.PositiveIntegerField(_('status'), default=WIRED, null=True, choices=STATUS_CHOICES)
    
    address_pulso = models.ForeignKey(Address, on_delete=models.CASCADE)
    # address = models.CharField(_('address'), max_length=255)
    # street = models.CharField(_('street'), max_length=75, blank=True)
    # house_number = models.CharField(_('house number'), max_length=75, blank=True)
    towers = models.PositiveSmallIntegerField(default=0)
    # commune = models.CharField(_('commune'), max_length=30)
    # location = models.CharField(_('location'), max_length=60, choices=COMMUNE_CHOICES)
    
    
    parent = models.ForeignKey('self', verbose_name=_('parent'), null=True, blank=True, on_delete=models.CASCADE)
    default_connection_node = models.ForeignKey('self', verbose_name=_('default connection node'), null=True, blank=True, on_delete=models.CASCADE, related_name='+')

    apartments = models.PositiveIntegerField(default=0)
    phone = models.CharField(_('phone'), max_length=16, default='', blank=True)
    # notes = models.TextField(_('notes'), blank=True, null=True)
     
    # ip_range = models.GenericIPAddressField(_('ip range'), protocol='IPv4', unique=True, blank=True, null=True)
    # ip_range = models.OneToOneField(IPTable, on_delete=models.CASCADE, unique=True, related_name='mikrotik_ranges', null=True, blank=True)
    ip_range = models.ManyToManyField('devices.IPRange')
    
    no_address_binding = models.BooleanField('no asociar direcciones', default=False)
    plans = models.ManyToManyField(Plan)
    group = models.ForeignKey(Group, verbose_name=_('group'), null=True, blank=True, on_delete=models.CASCADE)
    matrix_seller = models.PositiveSmallIntegerField(null=True, blank=True) 
    plan_order_kinds = models.ManyToManyField('PlanOrderKind', verbose_name='tipo OS habilitadas', blank=True)
    
    pics = GenericRelation('documents.Pic')
    documents = GenericRelation('documents.Document')

    radius = models.ForeignKey(Radius, null=True, on_delete=models.SET_NULL)
    logic_node = models.ForeignKey(Node, on_delete=models.PROTECT, null=True,  blank=True)

    
    def __str__(self):
        if self.ip:
            return f"{self.alias} {self.ip.ip}"
        return self.alias

    @property
    def address(self):
        return f'{self.address_pulso.street} {self.address_pulso.number}'
    

    @property
    def house_number(self):
        return f'{self.address_pulso.number}'

        
        
    @property
    def floor(self):
        return f'{self.address_pulso.floor}'
    @property
    def tower(self):
        return f'{self.address_pulso.tower}'

    @property
    def street(street):
        return f'{self.address_pulso.street}'

    @property
    def commune(self):
        return f'{self.address_pulso.commune}'
    
    
    @property
    def region(self):
        return f'{self.address_pulso.region}'

    @property
    def location(self):
        return f'{self.address_pulso.commune}'
        
    @property
    def street_id(self):
        return f'{self.address_pulso.street_id}'

    @property
    def commune_id(self):
        return f'{self.address_pulso.commune_id}'

    def get_uptime(self):
        return self.uptime

    # def set_status_offline(self):
    #     self.status = False
    #     self.save()

    # def set_status_online(self):
    #     self.status = True
    #     self.save()

    @property
    def get_uptime_delta(self):
        try:
            if self.status == 1:
                u = timezone.now() - self.uptime
                return timedelta_format(u)
            return None
        except Exception as e:
            print (e)
            return None

    @property
    def get_uptime_date(self):
        return self.uptime.strftime('Desde %d/%m/%Y a las %H:%M')

    @property
    def get_radius_alias(self):
        if self.radius:
            return self.radius.alias
        else:
            return None
    

    
    # def mapGears(self, gear):
    #     return [
    #         gear.id,
    #         gear.model.model,
    #         gear.description,
    #         gear.ip.ip,
    #         gear.mac.mac,
    #         gear.vlan,
    #     ]
    # @property
    # def get_gears(self):
    #     return map(self.mapGears, list(self.gear_set.all()))

    # Properties from matrix
    @property
    def available_ips(self):
        try:
            ip_range = self.ip_range
            valid = set(['.'.join(ip_range.split('.')[:-1]+[str(n)]) for n in range(2,255)])
            used = set(Gear.objects.filter(ip__ip__startswith='.'.join(ip_range.split('.')[:-1])).values_list('ip', flat=True))
            return sorted(list(valid - used), key=lambda x: int(x.split('.')[-1]))
        except:
            return []

    def refresh_network_equipment(self):
        from devices.models import MACTable

        NotFoundEquipment.objects.all().delete()
        # reset current node's mismatch status
        
        # cambio
        self.service_set.update(network_mismatch=False, node_mismatch_id=None)
        # Service.objects.filter(cpeutp_set__nodo_utp=self).update(network_mismatch=False, node_mismatch_id=None)

        leases = self.leases()
        for ll in  leases:
            hn = ll['host_name']
            addr = ll['address']
            mac = ll['mac_address']

            number = hn.strip().split('-')[0]
            try:
                # cambio
                cc = Service.objects.get(number=int(number))
                # cc = CpeUtp.objects.filter(service_number = int(number))

                # mark service as network mismatch if found in other node
                if self.pk != cc.node.pk:
                    # cambio
                    Service.objects.filter(pk=cc.pk).update(network_mismatch=True, node_mismatch_id=self.id)
                    print("Node mismatch with cpe node")

                # change ip of primary if is set
                primary = cc
                mact, created = MACTable.objects.get_or_create(mac=mac)
                    
                if primary:
                    if mact == primary.mac:
                        primary.ip = addr
                        primary.save()
                    else:
                        # save old primary as 0.0.0.0 for history
                        primary.ip = '0.0.0.0'
                        primary.primary = False
                        primary.save()

                        # create new one
                        # with new ip and mac and set as primary
                        #  ¿ CPE duplicado ?
                        # NetworkEquipment.objects.create(brand='AUTO',
                        #                                 model='AUTO',
                        #                                 ip=addr,
                        #                                 mac=mac,
                        #                                 primary=True,
                        #                                 service=cc)
                        CpeUtp.objects.create(ip = addr, 
                                                mac=mact, 
                                                service = cc,
                                                primary=True,
                                                serial='??????????')
                else:
                    # cc.networkequipment_set.update(primary=False)
                    # NetworkEquipment.objects.create(brand='AUTO',
                    #                                 model='AUTO',
                    #                                 ip=addr,
                    #                                 mac=mac,
                    #                                 primary=True,
                    #                                 service=cc)
                    CpeUtp.objects.create(ip = addr, 
                                            mac=mact, 
                                            service = cc, 
                                            primary=True,
                                            serial='??????????')

            except (ValueError, CpeUtp.DoesNotExist) as e:
                NotFoundEquipment.objects.create(ip=addr,
                                                 mac=mact,
                                                 host=hn)

        
        dropped = self.dropped_clients()

        if (dropped != None):
            self.service_set.filter(number__in=dropped).update(seen_connected=False)
            self.service_set.exclude(number__in=dropped).update(seen_connected=True)
        

    def leases(self):
        try:
            leases = get_leases(self.mikrotik_ip,
                                self.mikrotik_username,
                                self.mikrotik_password)
            return sorted(leases, key=itemgetter('host_name'))
        except:
            return []

    def dropped_clients(self):
        try:
            dropped = get_dropped(self.mikrotik_ip,
                                  self.mikrotik_username,
                                  self.mikrotik_password)
            return dropped
        except:
            return []

    @property
    def get_status_class(self):
        return self.STATUS_CLASSES.get(self.status, 'default')

    @property
    def truncated_code(self):
        return truncatechars(self.name, 30)

    @property
    def composite_address(self):
        return "{} {}".format(self.street, self.house_number)

    @property
    def best_address(self):
        return self.composite_address.strip() or self.address

    @property
    def truncated_address(self):
        return truncatechars(self.best_address, 30)
    
    def __str__(self):
        return "{} ({})".format(self.truncated_address, self.truncated_code)


    def save(self, *args, **kwargs):
        super(Antenna, self).save(*args, **kwargs)

    class Meta:
        ordering = ["alias"]
        unique_together = ("alias", "ip")




class AntennaConnection(BaseModel):
    username = models.CharField(max_length=25)
    password = models.CharField(max_length=30)
    antenna = models.ForeignKey(Antenna, blank=True, on_delete=models.CASCADE)
    connections = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return self.antenna.alias

    def release_connection(self):
        if self.connections > 0:
            self.connections -= 1
            self.save()

    def connection_usage(self):
        self.connections += 1
        self.save()

    def get_ip(self):
        return self.antenna.ip

    class Meta:
        ordering = ["username"]



class Gear(BaseModel):
    description = models.TextField(_('Description'), blank=True)
    # ip = models.GenericIPAddressField(_('ip'), protocol='IPv4', unique=True)
    ip = models.OneToOneField(IPTable, on_delete=models.CASCADE, unique=True, db_index=True)
    # mac = models.CharField(_('mac'), max_length=17)
    mac = models.ForeignKey('devices.MACTable', on_delete=models.CASCADE)

    vlan = models.CharField(_('Management Vlan'), max_length=255)
    node = models.ForeignKey(Mikrotik, verbose_name=_('node'), on_delete=models.CASCADE)
    firmware_version = models.CharField(_('Firmware Version'), max_length=255)
    notes = models.TextField(_('Internal Notes'), blank=True)
    latest_backup_date_cache = models.DateTimeField(_('Last Backup'), blank=True, null=True)
    model = models.ForeignKey(ModelDevice,
                              on_delete=models.PROTECT)

    pics = GenericRelation('documents.Pic')
    documents = GenericRelation('documents.Document')
    

    def get_absolute_url(self):
        return reverse('gear_api-detail', kwargs={'version': 'v1', 'pk': self.pk})
    
    
    @property
    def latest_backup_date(self):
        return self.documents.order_by('-created_at').values_list('created_at', flat=True).first()

    @property
    def days_since_last_backup(self):
        lbd = self.latest_backup_date
        if lbd:
            return (timezone.now() - lbd).days

    @property
    def days_since_last_backup_class(self):
        dslb = self.days_since_last_backup
        if dslb is not None:
            if 0 <= dslb <= 20:
                return 'success'
            elif 20 < dslb <= 29:
                return 'warning'
        return 'danger'

    @property
    def modified_at(self):
        return self.history.first().history_date

    @property
    def truncated_description(self):
        return truncatechars(self.description, 30)

    class Meta:
        verbose_name = _('gear')
        verbose_name_plural = _('gears')



class NetworkEquipmentQuerySet(models.QuerySet):
    def for_real(self):
        return self.exclude(ip='0.0.0.0').exclude(ip="1.1.1.1")

class CpeUtp(Device):

    alias = models.CharField(max_length=80, null=True, blank=True)
    serial = models.CharField(max_length=30, db_index=True)
    nodo_utp = models.ForeignKey(Mikrotik, on_delete=models.CASCADE)
    description = models.TextField()
    model = models.ForeignKey(ModelDevice, null=True, blank=True,
                              on_delete=models.PROTECT)
    # service_number = models.PositiveSmallIntegerField(null=True, blank=True)
    service = models.ForeignKey(Service, null= True, blank=True, on_delete=models.CASCADE)
    service_number_verified = models.BooleanField(default=False)
    # Quitada la posibilidad de que status sea NULL en este campo para evitar error en el serializer
    status = models.BooleanField(default=False, blank=True)
    uptime = models.DateTimeField(null=True, blank=True)
    last_seen = models.DurationField(null=True, blank=True)
    # mac = models.CharField(max_length=17, db_index=True, null=True, blank=True)
    # active_mac = models.CharField(max_length=17, db_index=True, null=True, blank=True)
    mac = models.ForeignKey('devices.MACTable', null=True, blank=True,
                            on_delete=models.PROTECT)
    mac_eth1 = models.ForeignKey('devices.MACTable', null=True, blank=True,
                            on_delete=models.PROTECT, related_name="cpe_eth1")

    # ip = models.GenericIPAddressField(protocol="IPv4", db_index=True, null=True, blank=True)
    # active_ip = models.GenericIPAddressField(protocol="IPv4", db_index=True, null=True, blank=True)
    ip = models.ForeignKey(IPTable, null=True, blank=True,
                              on_delete=models.CASCADE, db_index=True)
    active_ip = models.ForeignKey(IPTable, null=True, blank=True, related_name='cpe_actives',
                              on_delete=models.CASCADE, db_index=True)

    firmware = models.CharField(max_length=100, null=True, blank=True)
    download_speed = models.PositiveSmallIntegerField(validators=[MaxValueValidator(1000)], null=True, blank=True)
    upload_speed = models.PositiveSmallIntegerField(validators=[MaxValueValidator(1000)], null=True, blank=True)    

    
    
    #fields from matrix
    primary = models.BooleanField(default=False)
    objects = NetworkEquipmentQuerySet.as_manager()

    #ssid
    ssid = models.CharField(max_length=100, null=True, blank=True)
    ssid_5g = models.CharField(max_length=100, null=True, blank=True)
    password = models.CharField(max_length=100, null=True, blank=True)
    
    def delete_this(self):
        self.delete()

    def __str__(self):
        if self.alias:
            return self.alias
        return self.serial

    def set_status_offline(self):
        self.status = False
        self.save()

    @property
    def get_uptime_delta(self):
        try:
            if self.status:
                u = timezone.now() - self.uptime
                return timedelta_format(u)
            return None
        except Exception as e:
            print (e)
            return None

    @property
    def get_uptime_date(self):
        return self.uptime.strftime('Desde %d/%m/%Y a las %H:%M')
  

    def get_mac(self):
        return self.mac.mac

    @property
    def last_history_ip(self):
        first, *hx = self.history.order_by('history_date')
        history = [first]        
        for hh in hx:
            if history[-1].ip != hh.ip:
                history.append(hh)
        last = history.pop()
        print('=================')
        print(last)
        print('=================')
        print(type(last))
        print('=================')
        #print(dir(last))
        return last

    class Meta:
        ordering = ["serial"]
        unique_together = (("alias", "nodo_utp"),)



class SystemProfile(BaseModel):
    name = models.CharField(max_length=50, unique=True)
    limitation_name = models.CharField(max_length=50)
    download_speed = models.PositiveSmallIntegerField(validators=[MaxValueValidator(1000)], null=True, blank=True)
    upload_speed = models.PositiveSmallIntegerField(validators=[MaxValueValidator(1000)], null=True, blank=True)    
    provisioning_available = models.BooleanField(default=False)
    description = models.TextField(blank=True,default="")

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-name"]


class ProfileUTP(BaseModel):
    name = models.CharField(max_length=50)
    limitation_name = models.CharField(max_length=50)
    download_speed = models.PositiveSmallIntegerField(validators=[MaxValueValidator(1000)], null=True, blank=True)
    upload_speed = models.PositiveSmallIntegerField(validators=[MaxValueValidator(1000)], null=True, blank=True)    
    provisioning_available = models.BooleanField(default=False)
    description = models.TextField(blank=True,default="")
    radius = models.ForeignKey(Radius, on_delete=models.CASCADE)
    system_profile = models.ForeignKey(SystemProfile, on_delete = models.SET_NULL,
                                     null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-name"]






class PlanOrderKind(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class NotFoundMatrixEquipment(BaseModel):
    alias = models.CharField(max_length=80, db_index=True, null=True, blank=True)
    serial = models.CharField(max_length=30, db_index=True, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    model = models.ForeignKey(ModelDevice, null=True, blank=True,
                              on_delete=models.PROTECT) 
    service = models.ForeignKey(Service, null= True, blank=True, on_delete=models.CASCADE)
    is_cpe = models.BooleanField(default=False)
    mac = models.ForeignKey('devices.MACTable', null=True, blank=True,
                                on_delete=models.CASCADE)
    ip = models.ForeignKey(IPTable, null=True, blank=True,
                              on_delete=models.CASCADE, db_index=True)
    node = models.ForeignKey(Mikrotik, verbose_name=_('node'), on_delete=models.CASCADE, null=True, blank=True)
    primary = primary = models.BooleanField(default=False)
    ssid = models.CharField(max_length=100, null=True, blank=True)
    password = models.CharField(max_length=100, null=True, blank=True)
    ssid_5g = models.CharField(max_length=100, null=True, blank=True)

    service = models.ForeignKey(Service, null= True, blank=True, on_delete=models.CASCADE)
    download_speed = models.PositiveSmallIntegerField(validators=[MaxValueValidator(1000)], null=True, blank=True)
    upload_speed = models.PositiveSmallIntegerField(validators=[MaxValueValidator(1000)], null=True, blank=True)    


    class Meta:
        verbose_name = _('not found matrix equipment')
        verbose_name_plural = _('not found matrix equipments')



class NotFoundEquipment(BaseModel):
    # ip = models.GenericIPAddressField(_('ip'), protocol='IPv4')
    
    ip = models.ForeignKey(IPTable, on_delete=models.CASCADE, db_index=True)

    # mac = models.CharField(_('mac'), max_length=17)
    mac = models.ForeignKey('devices.MACTable', on_delete=models.CASCADE, db_index=True)

    host = models.CharField(_('host'), max_length=255)

    class Meta:
        verbose_name = _('not found equipment')
        verbose_name_plural = _('not found equipments')

class Availability(models.Model):
    node = models.ForeignKey(Mikrotik, on_delete=models.CASCADE)
    address = models.ForeignKey(Address, on_delete=models.CASCADE)
    
    def __str__(self):
        return "{} {}, {}, {}".format(self.address.street, self.address.number, self.address.commune, self.address.region)


class AddressList(BaseModel):
    node = models.ForeignKey(Mikrotik, on_delete=models.CASCADE, db_index=True)
    network = models.ForeignKey(IPTable, on_delete=models.CASCADE, db_index=True, related_name='addr_list_net')
    address = models.ForeignKey(IPTable, on_delete=models.CASCADE, db_index=True)
    mask_address = models.PositiveSmallIntegerField(validators=[MaxValueValidator(30)])
    interface = models.CharField(_('interface'), max_length=100 )


DURATION_CHOICES = ((0, '30 minutos'),
                    (1, '1 hora'),
                    (2, '2 horas'),
                    (4, '4 horas'),
                    (24, '1 día'),
                    (72, '3 días'),
                    (96, '4 días'),
                    (120, '5 días'),
                    (144, '6 días'),
                    (168, '7 días'),)

class DurationMikrotik(models.Model):
    
    activity_id = models.PositiveIntegerField(_('activity'))
    node = models.ForeignKey(Mikrotik, verbose_name=_('node'), on_delete=models.CASCADE)
    duration = models.PositiveSmallIntegerField(_('duration'), choices=DURATION_CHOICES)

    @property
    def activity(self):
        from .views_helper import get_activity 
        status_code, activity = get_activity(self.activity_id)
        
        if status_code == 200:
            return {
                'name': activity['name'],
                'description': activity['description'],
                'id': activity['id']
            }
        else:
            return {
                'name': '-',
                'description': '-',
                'id': 0
            }

    @property
    def get_durantion_display(self):
        return self.get_duration_display()

    
    def save(self, *args, **kwargs):
        if not self.pk:
            from .views_helper import get_activity 
            # This code only happens if the objects is
            # not in the database yet. Otherwise it would
            # have pk


            status_code, activity = get_activity(self.activity_id)
            if not status_code == 200:
                raise DurationMikrotik.DoesNotExist

        super(DurationMikrotik, self).save(*args, **kwargs)


class DurationAntenna(models.Model):
    
    activity_id = models.PositiveIntegerField(_('activity'))
    node = models.ForeignKey(Mikrotik, verbose_name=_('node'), on_delete=models.CASCADE)
    duration = models.PositiveSmallIntegerField(_('duration'), choices=DURATION_CHOICES)




class Lease(BaseModel):
    node = models.ForeignKey(Mikrotik, verbose_name=_('node'), on_delete=models.CASCADE)
    cpe  = models.ForeignKey('utp.CpeUtp', on_delete=models.CASCADE, null=True, blank=True)
    onu  = models.ForeignKey('devices.ONU', on_delete=models.CASCADE, null=True, blank=True)
    not_found  = models.ForeignKey('utp.NotFoundMatrixEquipment', on_delete=models.CASCADE, null=True, blank=True)

    address = models.ForeignKey(IPTable, null=True, blank=True,
                              on_delete=models.CASCADE, related_name='lease_addrress')
    active_address = models.ForeignKey(IPTable, null=True, blank=True,
                              on_delete=models.CASCADE, related_name='lease_active_addrress')
    mac_address = models.ForeignKey('devices.MACTable', null=True, blank=True,
                                on_delete=models.CASCADE, related_name='lease_mac_address')
    active_mac_address = models.ForeignKey('devices.MACTable', null=True, blank=True,
                                on_delete=models.CASCADE, related_name='lease_active_mac_address')
    client_id = models.CharField(null=True, blank=True, max_length=100)
    active_client_id = models.CharField(null=True, blank=True, max_length=100)
    server = models.CharField(_('server'), max_length=100, null=True, blank=True)
    
    hostname = models.CharField(_('hostname'), max_length=100, null=True, blank=True)
    last_seen = models.CharField(_('hostname'), max_length=100, null=True, blank=True)
    status = models.CharField(_('hostname'), max_length=100, null=True, blank=True)
    # active_hostname = models.CharField(_('active hostname'), max_length=100, null=True, blank=True)

    

class BuildingContact(BaseModel):
    CONSERJE = 1
    MAYORDOMO = 2
    ADMINISTRADOR = 3
    MIEMBRO_COMITE = 4
    OTRO = 5
    TYPE_CHOICES = ((CONSERJE, _('conserje')),
                    (MAYORDOMO, _('mayordomo')),
                    (ADMINISTRADOR, _('administrador')),
                    (MIEMBRO_COMITE, _('miembro del comité')),
                    (OTRO, _('otro')))

    kind = models.PositiveSmallIntegerField(_('kind'), default=CONSERJE, choices=TYPE_CHOICES)
    name = models.CharField(_('name'), max_length=255, blank=True)
    last_name = models.CharField(_('name'), max_length=255, blank=True, null=True)
    phone = models.CharField(_('phone'), max_length=75, blank=True)
    email = models.EmailField(blank=True)
    node = models.ForeignKey(Mikrotik, verbose_name=_('node'), on_delete=models.CASCADE)



class Interface(BaseModel):
    node = models.ForeignKey(Mikrotik, verbose_name=_('node'), on_delete=models.CASCADE)
    name = models.CharField(_('name'), max_length=255)
    type = models.CharField(_('type'), max_length=255, blank=True, null=True)
    actual_mtu = models.CharField(_('actual_mtu'), max_length=255, blank=True, null=True)
    tx = models.CharField(_('tx'), max_length=255, blank=True, null=True)
    rx = models.CharField(_('rx'), max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name
    
    

class Neighbor(models.Model):
    created = models.DateTimeField(verbose_name=_('created_at'), db_index=True)
    interface = models.ForeignKey(Interface, verbose_name=_('interface'), on_delete=models.CASCADE)
    address = models.ForeignKey(IPTable, null=True, blank=True,
                            on_delete=models.CASCADE, related_name='neighbor_addrress')
    
    mac_address = models.ForeignKey('devices.MACTable', null=True, blank=True,
                            on_delete=models.CASCADE, related_name='neighbor_mac_address')
    
    identity = models.CharField(_('identity'), max_length=255, blank=True, null=True)
    board_name = models.CharField(_('board_name'), max_length=255, blank=True, null=True)
    uptime = models.CharField(_('uptime'), max_length=255, blank=True, null=True)
    node = models.ForeignKey(Mikrotik, verbose_name=_('node'), on_delete=models.CASCADE)

# =================== MODELO DE STATUS LOGS ========================= #

class ServiceStatusLog(BaseModel):
    created_at = models.DateTimeField(default=timezone.now,editable=True)
    status = models.PositiveSmallIntegerField()
    seller = models.PositiveSmallIntegerField(blank=True, null=True)
    node = models.ForeignKey('Mikrotik', 
                            verbose_name=_('node'),
                            on_delete=models.PROTECT)
    technician = models.PositiveSmallIntegerField(blank=True, null=True)
    service = models.ForeignKey('customers.Service', 
                                verbose_name=('service'), 
                                null=True,
                                on_delete=models.PROTECT)


    def __str__(self):
        return f'{self.node}'



    

# ============== VISTA POSTGRES ================ #
class MikrotikList(models.Model):
    """ 
    Vista Postgres para extraer los nodos existentes en sentinel
    sin necesidad de conectarme a pulso para traer su ubicación
    """ 
    id = models.IntegerField(primary_key=True)
    alias = models.CharField(max_length=50)

    def __str__(self):
        return f'{self.alias}'

    class Meta:
        managed = False
        db_table = 'public\".\"mikrotik_list'


class ChangeStatusNode(models.Model):
    """ 

    """ 
    id = models.IntegerField(primary_key=True)
    node = models.CharField(max_length=50)
    status = models.CharField(max_length=50)
    apariciones = models.IntegerField()
    fecha = models.DateField()

    def __str__(self):
        return f'{self.node}'

    class Meta:
        managed = False
        db_table = 'public\".\"change_status_node'
        # db_table = 'public\".\"one_test'
        ordering = ['fecha']


