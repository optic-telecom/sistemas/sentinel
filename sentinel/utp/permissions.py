from rest_framework import permissions

TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'

class IsSystemInternalPermission(permissions.BasePermission):
    message = 'No tiene permiso'
    def has_permission(self, request, view):
        token = request.META.get('HTTP_AUTHORIZATION',None)
        if token:
            return token == TOKEN
        return False