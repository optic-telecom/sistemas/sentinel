import uuid
from datetime import timedelta, datetime
from django.conf import settings
from django.utils import timezone
from django.core.management import call_command
from django.core.exceptions import MultipleObjectsReturned
from django.db import IntegrityError
from rest_framework import serializers
from rest_framework.serializers import HyperlinkedModelSerializer
from rest_framework.validators import UniqueValidator
from rest_framework.utils import model_meta

from common.serializers import QueryFieldsMixin

from .models import Mikrotik, MikrotikConnection,CpeUtp, ProfileUTP, Group, Antenna, Availability,\
                    Radius, RadiusConnection, SystemProfile, Gear, PlanOrderKind, Antenna, \
                    AntennaConnection, DurationMikrotik, NotFoundMatrixEquipment, Lease, \
                    Interface, Neighbor
from utp.mikrotik import get_mikrotik_info, get_mikrotik_leases, create_radius_profile, get_radius_profile
from dashboard.utils import LOG

from address.models import Address

from .models import BuildingContact, ChangeStatusNode, ServiceStatusLog

from devices.serializers import ModelDeviceSerializer
from devices.models import ModelDevice, IPTable, MACTable, IPRange, ONU, OLT
from customers.models import Service, Plan
from utp.models import Mikrotik
from dashboard.models import TaskModel

__all__ = [
    'MikrotikSerializer', 'AntennaSerializer', 'CPEUTPSerializer',
    'ProfileUTPSerializer', 'RadiusSerializer', 'ProvisioningSerializer',
    'GearSerializer', 'GroupSerializer', 'PlanOrderKindSerializer'
    'MikrotikBaseSerializer', 'AvailabilitySerializer',
    'DurationMikrotikSerializer'
]

_l = LOG()

CALL_API = False


class GroupSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):
    id = serializers.IntegerField(required=False, min_value=1)

    class Meta:
        model = Group
        fields = ('id', 'name')


class ListNodeField(serializers.Field):
    def to_internal_value(self, obj):
        if isinstance(obj, list):
            return obj[0]
        else:
            msg = self.error_messages['invalid']
            raise ValidationError(msg)


class RadiusSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    id = serializers.IntegerField(required=False, min_value=1)
    model = ModelDeviceSerializer(read_only=True)
    user = serializers.CharField(write_only=True)
    password = serializers.CharField(write_only=True)
    ip = serializers.CharField()

    def validate_ip(self, ip):
        import ipaddress
        try:
            ipaddress.ip_address(ip)
        except ValueError as e:
            _err = {'error': 'La ip es invalida'}
            raise serializers.ValidationError(_err)

        ipt, created = IPTable.objects.get_or_create(ip=ip)

        return ipt

    def create(self, validated_data):

        user = self.context['request'].user
        username = validated_data.pop('user')
        password = validated_data.pop('password')
        ip = validated_data.get('ip').ip
        id_data = uuid.uuid4()

        try:
            info = get_mikrotik_info(ip, username, password)
            if (not isinstance(info, dict)) or info == {}:
                if info == {}:
                    msg = 'Falló la conexión vía API. Por favor revise que el username soporte API, \
                            y que el IP Services permita la conexión desde las IP 190.113.247.215 y 190.113.247.219.'

                else:
                    msg = info
                raise Exception('{}'.format(msg))
            model_name = info['model']
            try:
                model = ModelDevice.objects.get(model=model_name)
            except ModelDevice.DoesNotExist:
                model = ModelDevice.objects.create(model=model_name,
                                                   id_data=id_data,
                                                   brand=info['brand'])

                _l.MODEL.ADD(by_representation=user,
                             id_value=model.id,
                             description=f'Modelo creado por crear RADIUS',
                             entity_representation=repr(model))
            except Exception as e:
                print("Error getmodel serializer RADIUS")
                print(e)
                raise serializers.ValidationError({
                    "error": "No se puede obtener el modelo del dispositivo",
                    "detail": str(e)
                })

            validated_data['model'] = model
            # If week
            if "w" in info['uptime']:
                weeks, rest = info['uptime'].split('w')
                weeks = int(weeks)
            else:
                weeks = 0
                rest = info['uptime']
            # If day
            if "d" in rest:
                days, rest = rest.split('d')
                days = int(days)
            else:
                days = 0
            # If hour
            if "h" in rest:
                hours, rest = rest.split('h')
                hours = int(hours)
            else:
                hours = 0
            # If minute
            if "m" in rest:
                minutes, rest = rest.split('m')
                minutes = int(minutes)
            else:
                minutes = 0
            # If seconds
            if "s" in rest:
                secs, rest = rest.split('s')
                secs = int(secs)
            else:
                secs = 0
            # Get uptime with retrieved values
            value_uptime = timezone.now() - timedelta(weeks=weeks,
                                                      days=days,
                                                      hours=hours,
                                                      minutes=minutes,
                                                      seconds=secs)

            obj = Radius.objects.create(**validated_data)
            print("here")
            obj.status = True
            obj.uptime = value_uptime
            obj.time_uptime = timezone.now()

            # ipt, created = IPTable.objects.get_or_create(ip=ip)
            # obj.ip = ipt

            obj.description = info['description']
            obj.name = ""
            obj.save()

            _l.RADIUS.ADD(by_representation=user,
                          id_value=obj.uuid,
                          description=f'Nuevo RADIUS',
                          entity_representation=repr(obj))

            RadiusConnection.objects.create(radius=obj,
                                            username=username,
                                            password=password,
                                            id_data=id_data)
            """ task """
            TaskModel.objects.create(id_obj=obj.id,
                                     model_obj='radius',
                                     uuid=obj.uuid,
                                     name='task_new_radius',
                                     id_data=id_data)
            return obj

        except Exception as e:
            _l.RADIUS.ADD(by_representation=user,
                          id_value=ip,
                          id_field='ip',
                          description=f'fallo crear: {e}')

            raise serializers.ValidationError({
                "error": f'fallo crear: {e}',
                "detail": str(e)
            })

    class Meta:
        model = Radius
        fields = ('id', 'ip', 'alias', 'model', 'description', 'uptime', 'id',
                  'user', 'password', 'uuid', 'status', 'mac_eth1',
                  'get_uptime_delta')


class PlanOrderKindSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):
    id = serializers.IntegerField(required=False, min_value=1)

    class Meta:
        model = PlanOrderKind
        fields = ('id', 'name')


#from customers.serializers import PlanSerializer


class MikrotikSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    id = serializers.IntegerField(required=False, min_value=1)
    # plan_order_kinds = PlanOrderKindSerializer(required=False,  many=True)
    plan_order_kinds = serializers.PrimaryKeyRelatedField(
        required=False, queryset=PlanOrderKind.objects.all(), many=True)
    # plans = PlanSerializer(required=False, many=True)
    plans = serializers.PrimaryKeyRelatedField(required=False,
                                               queryset=Plan.objects.all(),
                                               many=True)

    # parent = serializers.ModelField(required=False, model_field=Mikrotik()._meta.get_field('id'))
    parent = serializers.PrimaryKeyRelatedField(
        required=False, allow_null=True, queryset=Mikrotik.objects.all())
    # default_connection_node = serializers.ModelField(required=False, model_field=Mikrotik()._meta.get_field('id'))
    default_connection_node = serializers.PrimaryKeyRelatedField(
        required=False, allow_null=True, queryset=Mikrotik.objects.all())
    ip_range = serializers.CharField(required=False, allow_blank=True)

    address_pulso = serializers.ModelField(
        required=True, model_field=Address()._meta.get_field('id'))
    # address_pulso = serializers.IntegerField(required=False, min_value=1)

    model = ModelDeviceSerializer(read_only=True)
    radius = serializers.ModelField(required=False,
                                    model_field=Radius()._meta.get_field('id'))
    user = serializers.CharField(write_only=True)
    password = serializers.CharField(write_only=True)
    ip = serializers.CharField()
    comment = serializers.CharField(allow_blank=True, required=False)
    mac_eth1 = serializers.CharField(required=False)
    uuid = serializers.CharField(read_only=True)

    def get_task_in_progress(self, obj):
        pass

    def validate(self, attrs):
        if self.context['view'].action == 'partial_update' or\
          self.context['view'].action == 'update':
            return super().validate(attrs)

        return super().validate(attrs)

    def validate_ip(self, ip):
        import ipaddress
        try:
            ipaddress.ip_address(ip)
        except ValueError as e:
            _err = {'error': 'La ip es invalida'}
            raise serializers.ValidationError(_err)

        ipt, create = IPTable.objects.get_or_create(ip=ip)

        return ipt

    def validate_mac_eth1(self, mac):
        if mac:
            try:
                mact, create = MACTable.objects.get_or_create(mac=mac)
                return mact

            except Exception as e:
                print('***\n', e, '\n*****')
                _err = {'error': str(e)}
                raise serializers.ValidationError(_err)
        else:
            return None

    def validate_ip_range(self, iprange):
        if iprange:
            try:

                ip_range = iprange.split('/')

                ipt, created = IPTable.objects.get_or_create(ip=ip_range[0])

                ipr, create = IPRange.objects.get_or_create(
                    ip=ipt, mask=int(ip_range[1]) if len(ip_range) > 1 else 0)
                return ipr

            except Exception as e:
                print('***\n', e, '\n*****')
                _err = {'error': 'El ip range es invalido'}
                raise serializers.ValidationError(_err)
        else:
            return None

    def validate_address_pulso(self, id_pulso):
        print(1111111)
        print(id_pulso)
        if id_pulso:
            try:
                addr, created = Address.objects.get_or_create(
                    id_pulso=id_pulso)
                return addr.id
            except Exception as e:
                print('***\n', e, '\n*****')
                _err = {'error': 'El id no existe en pulso.'}
                raise serializers.ValidationError(_err)
        else:
            return None

    # def validate_plans(self, plans):
    #     print('csacascsacascscsd')
    #     print(plans)
    #     return Plan.objects.filter(pk__in=plans)

    def create(self, validated_data):
        user = self.context['request'].user
        username = validated_data.pop('user')
        password = validated_data.pop('password')
        ip = validated_data.get('ip').ip
        id_data = uuid.uuid4()
        radiusid = validated_data.get('radius')

        print(username, password)
        try:

            if (CALL_API):
                try:
                    radius = Radius.objects.get(id=radiusid)
                except:
                    radius = None
                print(radius)
                info = get_mikrotik_info(ip, username, password)
                if (not isinstance(info, dict)) or info == {}:
                    if info == {}:
                        msg = 'Falló la conexión vía API. Por favor revise que el username soporte API, \
                                y que el IP Services permita la conexión desde las IP 190.113.247.215 y 190.113.247.219.'

                    else:
                        msg = info

                    print(
                        '***************\n***************\n***************\n\n'
                    )
                    raise Exception('{}'.format(msg))
                model_name = info['model']
                try:
                    print(model_name)
                    model = ModelDevice.objects.get(model=model_name)
                except ModelDevice.DoesNotExist:
                    model = ModelDevice.objects.create(model=model_name,
                                                       kind=3,
                                                       brand='Mikrotik')

                    _l.MODEL.ADD(
                        by_representation=user,
                        id_value=model.id,
                        description=f'Modelo creado por crear Mikrotik',
                        entity_representation=repr(model))
                except Exception as e:
                    print("Error getmodel serializer Mikrotik")
                    print(e)
                    raise serializers.ValidationError({
                        "error":
                        "No se puede obtener el modelo del dispositivo",
                        "detail": str(e)
                    })

                validated_data['model'] = model
                # If week
                if "w" in info['uptime']:
                    weeks, rest = info['uptime'].split('w')
                    weeks = int(weeks)
                else:
                    weeks = 0
                    rest = info['uptime']
                # If day
                if "d" in rest:
                    days, rest = rest.split('d')
                    days = int(days)
                else:
                    days = 0
                # If hour
                if "h" in rest:
                    hours, rest = rest.split('h')
                    hours = int(hours)
                else:
                    hours = 0
                # If minute
                if "m" in rest:
                    minutes, rest = rest.split('m')
                    minutes = int(minutes)
                else:
                    minutes = 0
                # If seconds
                if "s" in rest:
                    secs, rest = rest.split('s')
                    secs = int(secs)
                else:
                    secs = 0
                # Get uptime with retrieved values
                value_uptime = timezone.now() - timedelta(weeks=weeks,
                                                          days=days,
                                                          hours=hours,
                                                          minutes=minutes,
                                                          seconds=secs)
                print(model)
                print(22222222)
                # obj = Mikrotik.objects.create(**validated_data)
                obj = Mikrotik(
                    ip=validated_data.get('ip'),
                    alias=validated_data.get('alias'),
                    radius=radius,
                    serial="None",
                    address_pulso_id=validated_data.get('address_pulso'))
                obj.status = True
                obj.firmware = info['firmware']
                obj.uptime = value_uptime
                obj.time_uptime = timezone.now()
                obj.id_data = id_data
                obj.description = info['description']
                obj.name = ""
                obj.architecture = info['architecture']
                obj.model = model
                try:
                    obj.serial = info['serial']
                except KeyError as e:
                    print(e)
                    pass

                obj.cpu = str(info['cpu'])
                obj.ram = str(info['ram'])
                obj.storage = str(info['hdd'])
                if info['temperature'] != '':
                    obj.temperature = info['temperature']
                if info['voltage'] != '':
                    obj.voltage = round(float(info['voltage']), 0)
                if info['temperature_cpu'] != '':
                    obj.temperature_cpu = info['temperature_cpu']
                if info['power_consumption'] != '':
                    obj.power_consumption = round(
                        float(info['power_consumption']), 0)

                obj.parent = validated_data.get('parent', None)
                obj.default_connection_node = validated_data.get(
                    'default_connection_node', None)
                obj.comment = validated_data.get('comment')
                obj.apartments = validated_data.get('apartments')
                obj.towers = validated_data.get('towers')
                obj.type = validated_data.get('type', 1)

                obj.save()

                obj.plans.set(validated_data.get('plans'))

                _l.MIKROTIK.ADD(by_representation=user,
                                id_value=obj.uuid,
                                description=f'Nuevo Mikrotik',
                                entity_representation=repr(obj))

                MikrotikConnection.objects.create(mikrotik=obj,
                                                  username=username,
                                                  password=password,
                                                  id_data=id_data)
                """ task """
                TaskModel.objects.create(id_obj=obj.id,
                                         model_obj='mikrotik',
                                         uuid=obj.uuid,
                                         name='task_new_mikrotik',
                                         id_data=id_data)

                if 'leases' in info.keys():
                    listleases = info['leases']

                    # create a object Lease to save historic
                    created_lease = datetime.datetime.now()
                    for i in listleases:

                        print(i)
                        model_name = i.get('model', 'UNKNOWN')

                        if i['is_cpe']:
                            try:
                                model = ModelDevice.objects.filter(
                                    model=model_name, kind=4).first()
                            except ModelDevice.DoesNotExist:
                                model = ModelDevice.objects.create(
                                    model=model_name,
                                    kind=4,
                                    brand=i.get('brand', 'MIKROTIK'))
                                _l.MODEL.ADD(
                                    by_representation=user,
                                    id_value=model.id,
                                    description=
                                    f'Modelo creado por crear una CPE al crear un Mikrotikik',
                                    entity_representation=repr(model))
                        else:
                            try:
                                model = ModelDevice.objects.filter(
                                    model=model_name, kind=1).first()
                            except ModelDevice.DoesNotExist:
                                model = ModelDevice.objects.create(
                                    model=model_name,
                                    kind=1,
                                    brand=i.get('brand', 'UNKNOWN'))
                                _l.MODEL.ADD(
                                    by_representation=user,
                                    id_value=model.id,
                                    description=
                                    f'Modelo creado por crear una ONU al crear un Mikrotikik',
                                    entity_representation=repr(model))

                        ipt, created = IPTable.objects.get_or_create(
                            ip=i['ip'])
                        # ipt_active, created  = IPTable.objects.get_or_create(ip=i['active_ip'])
                        mact, created = MACTable.objects.get_or_create(
                            mac=i['mac'])
                        # mact_eth1, created = MACTable.objects.get_or_create(mac=i['active_mac'])

                        # se cra historico del lease
                        if i['active_ip'] == "":
                            ipt_active = None
                        else:
                            ipt_active, created = IPTable.objects.get_or_create(
                                ip=i['active_ip'])

                        if i['active_mac'] == "":
                            mact_eth1 = None
                        else:
                            mact_eth1, created = MACTable.objects.get_or_create(
                                mac=i['active_mac'])

                        try:
                            if i['uptime'] != None:
                                # If week
                                if "w" in i['uptime']:
                                    weeks, rest = i['uptime'].split('w')
                                    weeks = int(weeks)
                                else:
                                    weeks = 0
                                    rest = i['uptime']
                                # If day
                                if "d" in rest:
                                    days, rest = rest.split('d')
                                    days = int(days)
                                else:
                                    days = 0
                                # If hour
                                if "h" in rest:
                                    hours, rest = rest.split('h')
                                    hours = int(hours)
                                else:
                                    hours = 0
                                # If minute
                                if "m" in rest:
                                    minutes, rest = rest.split('m')
                                    minutes = int(minutes)
                                else:
                                    minutes = 0
                                # If seconds
                                if "s" in rest:
                                    secs, rest = rest.split('s')
                                    secs = int(secs)
                                else:
                                    secs = 0
                                # Get uptime with retrieved values
                                value_uptime = datetime.now() - timedelta(
                                    weeks=weeks,
                                    days=days,
                                    hours=hours,
                                    minutes=minutes,
                                    seconds=secs)
                                print(value_uptime)
                            else:
                                value_uptime = None

                        except:
                            value_uptime = None

                        if i['is_cpe'] and i['service_number']:
                            cpe = CpeUtp.objects.create(
                                status=i['status'],
                                description=i['description'],
                                mac=mact,
                                mac_eth1=mact_eth1,
                                nodo_utp=obj,
                                model=model,
                                ip=ipt,
                                active_ip=ipt_active,
                                firmware=i.get('firmware', None),
                                alias=i['description'])
                            if value_uptime:
                                print(value_uptime)
                                cpe.uptime = value_uptime
                                cpe.save()

                            # se cra historico del lease
                            lease_object = Lease.objects.create(
                                created=created_lease,
                                node=obj,
                                cpe=cpe,
                                address=ipt,
                                mac_address=mact,
                                client_id=i['client_id'],
                                server=i['server'],
                                active_address=ipt_active,
                                active_mac_address=mact_eth1,
                                active_client_id=i['active_client_id'],
                                hostname=i['description'],
                                last_seen=i['last_seen'],
                                status=i['status_lease']
                                # active_hostname = i['active_hostname']
                            )

                            cpe.service, created = Service.objects.get_or_create(
                                number=i['service_number'])
                            if 'download_speed' in i.keys():
                                cpe.download_speed = i['download_speed']
                            if 'upload_speed' in i.keys():
                                cpe.upload_speed = i['upload_speed']
                            cpe.save()
                            print(cpe)
                            _l.CPEUTP.ADD(
                                by_representation=user,
                                id_value=cpe.id,
                                description=
                                f'CPE creado al actualizar lista CPEUTPE de un Mikrotik',
                                entity_representation=repr(cpe))
                            # obj.cpeutps += 1
                            # if cpe.status == True:
                            #     obj.cpeutp_active += 1
                            #obj.save()
                        else:

                            try:
                                onu = ONU.objects.get(ip=ipt, mac=mact)

                                update_onu(onu, i, onu.olt, username)

                                lease_object = Lease.objects.create(
                                    created=created_lease,
                                    node=obj,
                                    onu=onu,
                                    address=ipt,
                                    mac_address=mact,
                                    client_id=i['client_id'],
                                    server=i['server'],
                                    active_address=ipt_active,
                                    active_mac_address=mact_eth1,
                                    active_client_id=i['active_client_id'],
                                    hostname=i['description'],
                                    last_seen=i['last_seen'],
                                    status=i['status_lease']
                                    # active_hostname = i['active_hostname']
                                )

                                if value_uptime:
                                    print(value_uptime)
                                    onu.uptime = value_uptime
                                    onu.save()

                                if i['service_number']:
                                    onu.service, created = Service.objects.get_or_create(
                                        number=i['service_number'])

                                if 'download_speed' in i.keys():
                                    onu.download_speed = i['download_speed']
                                if 'upload_speed' in i.keys():
                                    onu.upload_speed = i['upload_speed']
                                onu.save()
                                print(onu)
                                _l.ONU.ADD(
                                    by_representation=user,
                                    id_value=onu.id,
                                    description=
                                    f'ONU creado al actualizar lista ONU de un Mikrotik',
                                    entity_representation=repr(onu))

                                # onu.olt.onus += 1
                                # if onu.status == True:
                                #     onu.olt.onus_active += 1

                            except ONU.DoesNotExist:
                                not_found = NotFoundMatrixEquipment.objects.create(
                                    description=i['description'],
                                    mac=mact,
                                    node=obj,
                                    model=model,
                                    ip=ipt,
                                    is_cpe=i['is_cpe'],
                                    alias=i['description'],
                                )

                                lease_object = Lease.objects.create(
                                    created=created_lease,
                                    node=obj,
                                    not_found=not_found,
                                    address=ipt,
                                    mac_address=mact,
                                    client_id=i['client_id'],
                                    server=i['server'],
                                    active_address=ipt_active,
                                    active_mac_address=mact_eth1,
                                    active_client_id=i['active_client_id'],
                                    hostname=i['description'],
                                    last_seen=i['last_seen'],
                                    status=i['status_lease']
                                    # active_hostname = i['active_hostname']
                                )

                                if i['service_number']:
                                    not_found.service, created = Service.objects.get_or_create(
                                        number=i['service_number'])

                                if 'download_speed' in i.keys():
                                    not_found.download_speed = i[
                                        'download_speed']
                                if 'upload_speed' in i.keys():
                                    not_found.upload_speed = i['upload_speed']
                                not_found.save()
                                print(not_found)
                                # _l.ONU.ADD(by_representation=user, id_value=onu.id,
                                #     description=f'ONU creado al actualizar lista ONU de un Mikrotik', entity_representation=repr(onu))

                                # onu.olt.onus += 1
                                # if onu.status == True:
                                #     onu.olt.onus_active += 1

                                # onu.olt.save()

            else:

                model, created = ModelDevice.objects.get_or_create(
                    model='MIKROTIK', kind=3)

                # obj = Mikrotik.objects.create(**validated_data, radius=None)
                obj = Mikrotik.objects.create(
                    id=validated_data.get('id'),
                    group=validated_data.get('group'),
                    alias=validated_data.get('alias'),
                    status=validated_data.get('status'),
                    address_pulso_id=validated_data.get('address_pulso'),
                    parent=validated_data.get('parent', None),
                    default_connection_node=validated_data.get(
                        'default_connection_node', None),
                    # is_building = validated_data.get('is_building'),
                    towers=validated_data.get('towers'),
                    apartments=validated_data.get('apartments'),
                    phone=validated_data.get('phone', None)
                    if validated_data.get('phone', None) else '123434343',
                    # antennas_left = validated_data.get('antennas_left'),
                    ip=validated_data.get('ip'),
                    comment=validated_data.get('comment'),
                    #no_address_binding = validated_data.get('no_address_binding') if validated_data.get('no_address_binding', None) else False,
                    matrix_seller=validated_data.get('matrix_seller'),
                    description=validated_data.get('description'),
                    radius=None,
                    model=model,
                    type=validated_data.get('type', 1))

                # plan_order_kinds =PlanOrderKind.objects.filter(pk__in=validated_data.get('plan_order_kinds', []))
                # # print(validated_data.get('plans'))
                # plans = Plan.objects.filter(pk__in=validated_data.get('plans', []))

                plan_order_kinds = validated_data.get('plan_order_kinds', None)
                if plan_order_kinds:
                    obj.plan_order_kinds.set(plan_order_kinds)

                plans = validated_data.get('plans', None)
                if plans:
                    obj.plans.set(plans)

                ip_range = validated_data.get('ip_range')
                if (ip_range):
                    obj.ip_range.add(ip_range)

                if (username and password):
                    MikrotikConnection.objects.create(mikrotik=obj,
                                                      username=username,
                                                      password=password,
                                                      id_data=id_data)
            return obj

        except Exception as e:
            print(e)
            _l.MIKROTIK.ADD(by_representation=user,
                            id_value=ip,
                            id_field='ip',
                            description=f'fallo crear: {e}')

            raise serializers.ValidationError({
                "error": f'fallo crear: {e}',
                "detail": str(e)
            })

    def update(self, instance, validated_data):

        print(validated_data)
        if (('parent' in validated_data or 'default_connection_node' in validated_data) and \
            (not 'ip' in validated_data and not 'address_pulso' in validated_data and \
            not 'alias' in validated_data)):

            print(validated_data)

            parent = validated_data.pop('parent', None)
            default_connection_node = validated_data.pop(
                'default_connection_node', None)

            # instance.parent = Mikrotik.objects.filter(id=parent).first() if parent else None
            # instance.default_connection_node = Mikrotik.objects.filter(id=default_connection_node).first() if default_connection_node else None
            instance.parent = parent
            instance.default_connection_node = default_connection_node
        else:
            parent = validated_data.pop('parent', None)
            default_connection_node = validated_data.pop(
                'default_connection_node', None)
            instance.parent = parent
            instance.default_connection_node = default_connection_node
            ip = validated_data.pop('ip', None)
            ip_range = validated_data.pop('ip_range', None)
            address_pulso = validated_data.pop('address_pulso', None)
            plans = validated_data.pop('plans', None)
            radius = validated_data.pop('radius', None)
            status = validated_data.pop('status', None)
            comment = validated_data.pop('comment', None)
            towers = validated_data.pop('towers', None)
            apartments = validated_data.pop('apartments', None)
            alias = validated_data.pop('alias', None)
            username = validated_data.pop('user', None)
            password = validated_data.pop('password', None)

            instance.radius = Radius.objects.filter(
                id=radius).first() if radius else None

            instance.ip = ip

            if ip_range:
                instance.ip_range.add(ip_range)
            if address_pulso:
                instance.address_pulso = Address.objects.filter(
                    id=address_pulso).first()

            instance.status = status
            instance.alias = alias
            instance.comment = comment
            instance.apartments = apartments
            instance.towers = towers

            mc, created = MikrotikConnection.objects.get_or_create(
                mikrotik=instance, username=username, password=password)
            instance.plans.clear()
            instance.plans.set(plans)

        instance.save()
        return instance

    def to_representation(self, instance):
        import pprint
        rep = super(MikrotikSerializer, self).to_representation(instance)
        rep['parent_alias'] = instance.parent.alias if instance.parent else ''
        rep['ip_range'] = str(
            instance.ip_range.last()) if instance.ip_range.last() else ''
        rep['status_display'] = instance.get_status_display()

        mikrotikConnection = MikrotikConnection.objects.filter(
            mikrotik=instance).first()
        rep['username'] = mikrotikConnection.username if mikrotikConnection else ''
        rep['password'] = mikrotikConnection.password if mikrotikConnection else ''

        rep['parent'] = instance.parent.id if instance.parent else None
        rep['address_pulso'] = instance.address_pulso.id if instance.address_pulso else None
        rep['id_pulso'] = instance.address_pulso.id_pulso if instance.address_pulso else None
        rep['default_connection_node'] = instance.default_connection_node.id if instance.default_connection_node else None
        rep['default_connection_node_alias'] = instance.default_connection_node.alias if instance.default_connection_node else None
        rep['radius'] = instance.radius.id if instance.radius else None

        return rep

    class Meta:
        model = Mikrotik
        fields = (
            'uuid',
            'id',
            'plan_order_kinds',
            'plans',
            'ip_range',
            'ip',
            'alias',
            'model',
            'description',
            #'cpeutps',
            #'cpeutp_active',
            'uptime',
            'id',
            'user',
            'password',
            'uuid',
            'name',
            'status',
            'firmware',
            'architecture',
            'cpu',
            'ram',
            'serial',
            'mac_eth1',
            'storage',
            'get_uptime_delta',
            'get_uptime_date',
            'temperature',
            'temperature_cpu',
            'voltage',
            'power_consumption',
            'comment',
            'radius',
            'get_radius_alias',
            'group',
            'status',
            'address_pulso',
            'parent',
            'default_connection_node',
            'expected_power',
            #   'is_building', 'antennas_left',
            'towers',
            'apartments',
            'phone',
            'devices',  #'no_address_binding',
            'commune',
            'region',
            'street',
            'floor',
            'tower',
            'house_number',
            'commune_id',
            'region_id',
            'street_id',
            #   'get_leases',
            'type')


class CPEUTPSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    id = serializers.IntegerField(required=False, min_value=1)
    # nodo_utp = serializers.CharField(read_only=True, source='nodo_utp.alias')
    # model = ModelDeviceSerializer()
    # Comentada la siguiente línea por error "NoneType has no attribute 'model'"
    #model = serializers.CharField(source='model.model')

    ip = serializers.CharField()
    mac = serializers.CharField()

    def validate_ip(self, ip):
        import ipaddress
        try:
            ipaddress.ip_address(ip)
        except ValueError as e:
            _err = {'error': 'La ip es invalida'}
            raise serializers.ValidationError(_err)

        ipt, create = IPTable.objects.get_or_create(ip=ip)

        return ipt

    def validate_mac(self, mac):
        if mac:
            try:
                mact, create = MACTable.objects.get_or_create(mac=mac)
                return mact

            except Exception as e:
                print('***\n', e, '\n*****')
                _err = {'error': str(e)}
                raise serializers.ValidationError(_err)
        else:
            return None

    class Meta:
        model = CpeUtp
        fields = ('id', 'serial', 'model', 'description', 'service',
                  'nodo_utp', 'uuid', 'alias', 'status', 'uptime', 'mac', 'ip',
                  'firmware', 'nodo_utp', 'mac_eth1', 'active_ip',
                  'get_uptime_delta', 'get_uptime_date', 'primary', 'ssid',
                  'password', 'ssid_5g')


def extract_elem_from_list(givenlist, index):
    """
    Extracts an element from a list given its index in said list.
    This method is used in main_updatecpeutp.
    """
    if index == 0:
        return givenlist[1:]
    elif index == len(givenlist) - 1:
        return givenlist[:len(givenlist) - 1]
    else:
        return givenlist[:index] + givenlist[index + 1:]


def update_cpeutp(cpeutp, i, obj, user):
    """
    This method supports the main for updating cpe.
    Simply grabs a dict and updates de writable fields of a CpeUtp instance.
    """
    print("===================")
    print(i)
    print("===================")
    print(i['service_number'])
    print("===================")
    print(cpeutp.service)
    print("===================")
    boolean = True
    # if i['service_number']!=cpeutp.service_number:
    if cpeutp.service is not None:
        if i['service_number'] is not None and i['service_number'] != cpeutp.service.number:
            boolean = False
            # cpeutp.service_number=i['service_number']
            cpeutp.service, created = Service.objects.get_or_create(number=i['service_number'])
        if i['service_number'] is None:
            cpeutp.service = None
    else:
        if i['service_number'] is None:
            cpeutp.service = None
        else:
            boolean = False
            cpeutp.service, created = Service.objects.get_or_create(number=i['service_number'])
    if i['status'] != cpeutp.status:
        boolean = False
        cpeutp.status = i['status']
        if cpeutp.status == True:
            #obj.cpeutp_active += 1
            #obj.save()
            _l.MIKROTIK.ADD(
                by_representation=user,
                id_value=obj.id,
                description=
                f'Cambiado status de un CPE al actualizar lista CPEUTP del nodo',
                entity_representation=repr(obj))
        else:
            if obj.cpeutp_active != 0:
                #obj.cpeutp_active -= 1
                #obj.save()
                _l.MIKROTIK.ADD(
                    by_representation=user,
                    id_value=obj.id,
                    description=
                    f'Cambiado status de un CPE al actualizar lista CPEUTP del nodo',
                    entity_representation=repr(obj))
    if i['description']:
        # Para actualizar la descripción y ubicar los cpes duplicados necesitamos
        # Verificar cuántos equipos asociados tiene el número de servicio actual 
        boolean = False
        if cpeutp.service is not None:
            if cpeutp.service.cpeutp_set.all().count() > 1:
                # Si se cumple significa que el servicio actual tiene más de un
                # equipo asociado así que están duplicados
                cpeutp.description = i['description'] + ", (Duplicado)"
            else:
                cpeutp.description = i['description']
        else:
            cpeutp.description = i['description']

    mact, created = MACTable.objects.get_or_create(mac=i['mac'])
    if mact != cpeutp.mac:
        boolean = False
        cpeutp.mac = mact
    mact, created = MACTable.objects.get_or_create(mac=i['mac'])
    if mact != cpeutp.mac_eth1:
        boolean = False
        cpeutp.mac_eth1 = mact
    if "model" in i.keys() and i['model'] != "":
        boolean = False
        model_name = i['model']
        try:
            model = ModelDevice.objects.get(model=model_name, brand=i['brand'], kind=4)
        except Exception as e:
            print(e)
            # Si falla la obtención del modelo entonces no existe
            # así que lo creamos
            model = ModelDevice.objects.create(model=model_name,
                                               brand=i['brand'],
                                               kind=4)
            _l.MODEL.ADD(
                by_representation=user,
                id_value=model.id,
                description=
                f'Modelo creado por crear una CPE al actualizar la lista CPEUTP de un Mikrotikik',
                entity_representation=repr(model))
        if cpeutp.model != model:
            boolean = False
            cpeutp.model = model
    else:
        if cpeutp.model:
            boolean = False
    # Chequeamos el IP
    if i['ip'] != "":
        if cpeutp.ip is not None:
            print('cpe ip is not none')
            if i['ip'] != cpeutp.ip.ip:
                boolean = False
                cpeutp.ip, created = IPTable.objects.get_or_create(ip=i['ip'])
                print('se creo IP', created)
        else:
            boolean = False
            if i['ip'] != "":
                cpeutp.ip, created = IPTable.objects.get_or_create(ip=i['ip'])
                print('se creo IP', created)
    else:
        cpeutp.ip = None
    # Chequeamos el active IP
    if i['active_ip'] != "":
        if cpeutp.active_ip is not None:
            print('active ip is not none')
            if i['active_ip'] != cpeutp.active_ip:
                boolean = False
                cpeutp.active_ip, created = IPTable.objects.get_or_create(ip=i['active_ip'])
                print('se creo IP2', created)
        else:
            print('cpe active ip is none')
            boolean = False
            if i['active_ip'] != "":
                cpeutp.active_ip, created = IPTable.objects.get_or_create(ip=i['active_ip'])
                print('se creo IP2', created)
    else:
        print('aca')
        cpeutp.active_ip = None

    if "firmware" in i.keys() and i['firmware'] != cpeutp.firmware:
        boolean = False
        cpeutp.firmware = i['firmware']
    if "uptime" in i.keys() and i['uptime'] is not None:
        # If week
        try:
            if "w" in i['uptime']:
                weeks, rest = i['uptime'].split('w')
                weeks = int(weeks)
            else:
                weeks = 0
                rest = i['uptime']
            # If day
            if "d" in rest:
                days, rest = rest.split('d')
                days = int(days)
            else:
                days = 0
            # If hour
            if "h" in rest:
                hours, rest = rest.split('h')
                hours = int(hours)
            else:
                hours = 0
            # If minute
            if "m" in rest:
                minutes, rest = rest.split('m')
                minutes = int(minutes)
            else:
                minutes = 0
            # If seconds
            if "s" in rest:
                secs, rest = rest.split('s')
                secs = int(secs)
            else:
                secs = 0
            # Get uptime with retrieved values
            value_uptime = timezone.now() - timedelta(
                weeks=weeks, days=days, hours=hours, minutes=minutes, seconds=secs)
            if value_uptime != cpeutp.uptime:
                boolean = False
                cpeutp.uptime = value_uptime
        except Exception as e:
            print(e)
            cpeutp.uptime = None
    if 'last_seen' in i.keys():
        try:
            if "w" in i['last_seen']:
                weeks, rest = i['last_seen'].split('w')
                weeks = int(weeks)
            else:
                weeks = 0
                rest = i['last_seen']
            # If day
            if "d" in rest:
                days, rest = rest.split('d')
                days = int(days)
            else:
                days = 0
            # If hour
            if "h" in rest:
                hours, rest = rest.split('h')
                hours = int(hours)
            else:
                hours = 0
            # If minute
            if "m" in rest:
                minutes, rest = rest.split('m')
                minutes = int(minutes)
            else:
                minutes = 0
            # If seconds
            if "s" in rest:
                secs, rest = rest.split('s')
                secs = int(secs)
            else:
                secs = 0
            # Get uptime with retrieved values
            value_last_seen = timedelta(
                weeks=weeks, days=days, hours=hours, minutes=minutes, seconds=secs)
            if value_last_seen != cpeutp.last_seen:
                boolean = False
                cpeutp.last_seen = value_last_seen
                cpeutp.save()
        except Exception as e:
            print(e)
            cpeutp.last_seen = None
            cpeutp.save()
    if 'download_speed' in i.keys():
        cpeutp.download_speed = i['download_speed']
        boolean = False
    if 'upload_speed' in i.keys():
        cpeutp.upload_speed = i['upload_speed']
        boolean = False
    print("BOOLEAN ES", boolean)
    if not boolean:
        cpeutp.save()
        _l.CPEUTP.ADD(
            by_representation=user,
            id_value=cpeutp.id,
            description=
            f'CPE actualizado al actualizar lista CPEUTP de Mikrotik',
            entity_representation=repr(cpeutp))


def update_onu(onu, i, obj, user):
    """
    This method supports the main for updating cpe.
    Simply grabs a dict and updates de writable fields of a onu instance.
    """
    boolean = True
    if onu.service != None:
        if i['service_number'] != onu.service.number:
            if i['service_number'] is not None:
                boolean = False
                onu.service, created = Service.objects.get_or_create(number=i['service_number'])
    else:
        if i['service_number'] != None:
            boolean = False
            onu.service, created = Service.objects.get_or_create(number=i['service_number'])
    if i['status'] != onu.status:
        boolean = False
        onu.status = i['status']
        print('ESTATUS DEL ONU COMING')
        print(i['status'])
        print('ESTATUS DEL ONU GUARDADO')
        print(onu.status)
        if onu.status == True:
            # obj.onus_active += 1
            obj.save()
            _l.MIKROTIK.ADD(
                by_representation=user,
                id_value=obj.id,
                description=
                f'Cambiado status de un ONU al actualizar lista ONU del nodo',
                entity_representation=repr(obj))
        else:
            if obj.onus_active != 0:
                # obj.onus_active -= 1
                obj.save()
                _l.MIKROTIK.ADD(
                    by_representation=user,
                    id_value=obj.id,
                    description=
                    f'Cambiado status de un ONU al actualizar lista ONU del nodo',
                    entity_representation=repr(obj))
    if i['description'] != onu.description:
        boolean = False
        onu.description = i['description']

    mact, created = MACTable.objects.get_or_create(mac=i['mac'])
    if mact != onu.mac:
        boolean = False
        onu.mac = mact
    mact, created = MACTable.objects.get_or_create(mac=i['mac'])
    if "model" in i.keys():
        boolean = False
        model_name = i['model']
        try:
            model = ModelDevice.objects.filter(model=model_name,
                                               kind=1).first()
        except ModelDevice.DoesNotExist:
            model = ModelDevice.objects.create(model=model_name,
                                               brand=i['brand'],
                                               kind=1)
            _l.MODEL.ADD(
                by_representation=user,
                id_value=model.id,
                description=
                f'Modelo creado por crear una ONU al actualizar la lista OnU de un Mikrotikik',
                entity_representation=repr(model))
        if onu.model != model:
            boolean = False
            onu.model = model
    else:
        if onu.model:
            boolean = False
            onu.model = None
    if onu.ip is not None:
        if i['ip'] != onu.ip.ip:
            boolean = False
            onu.ip, created = IPTable.objects.get_or_create(ip=i['ip'])
    else:
        boolean = False
        onu.ip, created = IPTable.objects.get_or_create(ip=i['ip'])
    if "uptime" in i.keys() and i['uptime'] != None:
        # If week
        if "w" in i['uptime']:
            weeks, rest = i['uptime'].split('w')
            weeks = int(weeks)
        else:
            weeks = 0
            rest = i['uptime']
        # If day
        if "d" in rest:
            days, rest = rest.split('d')
            days = int(days)
        else:
            days = 0
        # If hour
        if "h" in rest:
            hours, rest = rest.split('h')
            hours = int(hours)
        else:
            hours = 0
        # If minute
        if "m" in rest:
            minutes, rest = rest.split('m')
            minutes = int(minutes)
        else:
            minutes = 0
        # If seconds
        if "s" in rest:
            secs, rest = rest.split('s')
            secs = int(secs)
        else:
            secs = 0
        # Get uptime with retrieved values
        value_uptime = timezone.now() - timedelta(
            weeks=weeks, days=days, hours=hours, minutes=minutes, seconds=secs)
        if value_uptime != onu.uptime:
            boolean = False
            onu.uptime = value_uptime
    if 'download_speed' in i.keys():
        onu.download_speed = i['download_speed']
        boolean = False
    if 'upload_speed' in i.keys():
        onu.upload_speed = i['upload_speed']
        boolean = False
    if not boolean:
        onu.deleted_sentinel = False
        onu.save()
        _l.ONU.ADD(
            by_representation=user,
            id_value=onu.id,
            description=f'CPE actualizado al actualizar lista ONU de Mikrotik',
            entity_representation=repr(onu))


import datetime

from .models import Lease
from devices.models import ONU


def main_updatecpeutp(utp_uuid):
    """
    This is a method used in order to update the cpelist of a
    given utp node
    """
    # Get utp node
    elem = Mikrotik.objects.get(uuid=utp_uuid)
    
    print(elem)
    print(elem.alias)
    # Get connections details for this device
    connection_data = MikrotikConnection.objects.filter(mikrotik=elem)[0]
    user = connection_data.username
    _l.MIKROTIK.ADD(
        by_representation=user,
        id_value=elem.uuid,
        description=f'Petición para actualizar lista CPEUTP de Mikrotik',
        entity_representation=repr(elem))
    obj = elem

    # create a object Lease to save historic
    created_lease = datetime.datetime.now()

    # try:
    # Get device leases
    leases = get_mikrotik_leases(str(elem.ip), user, connection_data.password)
    if isinstance(leases, str):
        raise Exception("Error: " + leases)
    # Get cpes currently associated to utp node
    saved_cpeutp = list(CpeUtp.objects.filter(nodo_utp=elem))
    saved_onu = list(ONU.objects.filter(olt__node=elem))
    # Compare the two lists
    temp = []
    print(len(leases), len(saved_cpeutp), len(saved_onu))
    while len(leases) != 0 and len(saved_cpeutp) != 0:
        current = leases.pop()
        temp.append(current)
        print("This is current element from leases")
        print(current)

        is_cpe = False
        for i in range(len(saved_cpeutp)):
            mact, create = MACTable.objects.get_or_create(mac=current['mac'])
            print('1111111111111111111111111111111111111', current['ip'],
                  current['ip'] == saved_cpeutp[i].ip.ip,
                  saved_cpeutp[i].ip.ip, mact, saved_cpeutp[i].mac,
                  mact == saved_cpeutp[i].mac)
            print('ESTE ES EL SAVED ACTUAL')
            if current['ip'] == saved_cpeutp[i].ip.ip and mact == saved_cpeutp[
                    i].mac:
                print("found match in saved cpeutp,updating")
                print(saved_cpeutp[i].id)

                update_cpeutp(saved_cpeutp[i], current, elem, user)
                is_cpe = True

                # se cra historico del lease
                if current['active_ip'] == "":
                    active_address = None
                else:
                    active_address, created = IPTable.objects.get_or_create(
                        ip=current['active_ip'])

                if current['active_mac'] == "":
                    active_mac_address = None
                else:
                    active_mac_address, created = MACTable.objects.get_or_create(
                        mac=current['active_mac'])

                lease_object, created = Lease.objects.get_or_create(
                    #created=created_lease,
                    node=elem,
                    cpe=saved_cpeutp[i],
                    address=saved_cpeutp[i].ip,
                    mac_address=mact,
                    client_id=current['client_id'],
                    server=current['server'],
                    active_address=active_address,
                    active_mac_address=active_mac_address,
                    active_client_id=current['active_client_id'],
                    hostname=current['description'],
                )

                lease_object.last_seen = current['last_seen']
                lease_object.status = current['status_lease']
                lease_object.save()
                
                saved_cpeutp.pop(i)
                print("This is the new cpe list")
                print(saved_cpeutp)
                temp.pop()

                break

        if not is_cpe:
            for i in range(len(saved_onu)):
                # if current['ip'] == saved_onu[i].ip.ip and mact == saved_onu[
                #         i].mac:
                if mact == saved_onu[i].mac:
                    print("found match in saved onu,updating")
                    update_onu(saved_onu[i], current, saved_onu[i].olt, user)

                    # se cra historico del lease
                    if current['active_ip'] == "":
                        active_address = None
                    else:
                        active_address, created = IPTable.objects.get_or_create(
                            ip=current['active_ip'])

                    if current['active_mac'] == "":
                        active_mac_address = None
                    else:
                        active_mac_address, created = MACTable.objects.get_or_create(
                            mac=current['active_mac'])

                    # se cra historico del lease
                    lease_object, created = Lease.objects.get_or_create(
                        #created=created_lease,
                        node=elem,
                        onu=saved_onu[i],
                        address=saved_onu[i].ip,
                        mac_address=mact,
                        client_id=current['client_id'],
                        server=current['server'],
                        active_address=active_address,
                        active_mac_address=active_mac_address,
                        active_client_id=current['active_client_id'],
                        hostname=current['description'],
                    )
                    print('se creo', created)

                    lease_object.last_seen = current['last_seen']
                    lease_object.status = current['status_lease']
                    lease_object.save()     

                    saved_onu = extract_elem_from_list(saved_onu, i)
                    print("This is the new onu list")
                    print(saved_onu)
                    temp.pop()

                    break
    print("ESTA ES LA LISTA DE CURRENTS")
    print(temp)
    print("ESTA ES LA LISTA DE CURRENTS")
    if len(leases) == 0:
        leases = temp
    else:
        leases = temp + leases
    print("ESTOS SON LOS LEASES")
    print(leases)
    print('len de LEASES', len(leases))
    print('LEN DE CPES GUARDADOS', len(saved_cpeutp))
    print('CPES GUARDADOS SON', [i.mac.mac for i in saved_cpeutp])
    print('LEN DE ONUS GUARDADOS', len(saved_onu))

    # There is at least one cpe that needs to be added
    if len(leases) != 0 and (len(saved_cpeutp) == 0 or len(saved_onu) == 0):
        print("Need to add new cpe(s) to list")

        for i in leases:

            mact, created = MACTable.objects.get_or_create(mac=i['mac'])
            mact_eth1, created = MACTable.objects.get_or_create(
                mac=i['active_mac'])
            if i['ip'] != "":
                print('el ip es none')
                ipt, created = IPTable.objects.get_or_create(ip=i['ip'])
            else:
                ipt = None
            if i['active_ip'] != "":
                print('active el ip es none')
                ipt_active, created = IPTable.objects.get_or_create(
                    ip=i['active_ip'])
            else:
                ipt_active = None

            print("CREADO LO NECESARIO DEL LEASE")
            print("======================")
            print(i)

            model_name = i.get('model', 'UNKNOWN')

            if i['is_cpe']:
                print("DENTRO DEL IF I IS CPE")
                try:
                    model = ModelDevice.objects.filter(model=model_name,
                                                       kind=4).first()
                    print("COMPLETÓ EL PRIMER TRY")
                except ModelDevice.DoesNotExist:
                    print("EXCEPT DEL PRIMER TRY")
                    model = ModelDevice.objects.create(model=model_name,
                                                       kind=4,
                                                       brand=i.get(
                                                           'brand',
                                                           'MIKROTIK'))
                    _l.MODEL.ADD(
                        by_representation=user,
                        id_value=model.id,
                        description=
                        f'Modelo creado por crear una CPE al crear un Mikrotikik',
                        entity_representation=repr(model))
            else:
                print("ELSE DELIF IS CPE")
                try:
                    model = ModelDevice.objects.filter(model=model_name,
                                                       kind=1).first()
                    print("COMPLETÓ EL SEGUNDO TRY")
                except ModelDevice.DoesNotExist:
                    print("EXCEPT DEL SEGUNDO TRY")
                    model = ModelDevice.objects.create(model=model_name,
                                                       kind=1,
                                                       brand=i.get(
                                                           'brand', 'UNKNOWN'))
                    _l.MODEL.ADD(
                        by_representation=user,
                        id_value=model.id,
                        description=
                        f'Modelo creado por crear una ONU al crear un Mikrotikik',
                        entity_representation=repr(model))

            if i['active_ip'] == "":
                print("DENTRO DEL IF ACTIVE IP")
                ipt_active = None
            else:
                print("DENTRO DEL ELSE ACTIVE IP")
                ipt_active, created = IPTable.objects.get_or_create(
                    ip=i['active_ip'])

            if i['active_mac'] == "":
                print("DENTRO DEL ACTIVE MAC")
                mact_eth1 = None
            else:
                print("DENTRO DEL ELSE ACTIVE MAC")
                mact_eth1, created = MACTable.objects.get_or_create(
                    mac=i['active_mac'])

            try:
                if i['uptime'] is not None:
                    print("DENTRO DEL IF UPTIME")
                    # If week
                    if "w" in i['uptime']:
                        weeks, rest = i['uptime'].split('w')
                        weeks = int(weeks)
                    else:
                        weeks = 0
                        rest = i['uptime']
                    # If day
                    if "d" in rest:
                        days, rest = rest.split('d')
                        days = int(days)
                    else:
                        days = 0
                    # If hour
                    if "h" in rest:
                        hours, rest = rest.split('h')
                        hours = int(hours)
                    else:
                        hours = 0
                    # If minute
                    if "m" in rest:
                        minutes, rest = rest.split('m')
                        minutes = int(minutes)
                    else:
                        minutes = 0
                    # If seconds
                    if "s" in rest:
                        secs, rest = rest.split('s')
                        secs = int(secs)
                    else:
                        secs = 0
                    # Get uptime with retrieved values

                    value_uptime = datetime.datetime.now() - timedelta(
                        weeks=weeks,
                        days=days,
                        hours=hours,
                        minutes=minutes,
                        seconds=secs)
                    print(value_uptime)
            except Exception as e:
                print(e)
                print("DENTRO DEL EXCEPT VALUE UPTIME")
                value_uptime = None

            if i['is_cpe'] and i['service_number']:
                print("DENTRO DEL IS CPE AND IS SERVICE NUMBER")
                try:
                    cpe = CpeUtp.objects.create(
                        status=i['status'],
                        description=i['description'],
                        mac=mact,
                        mac_eth1=mact_eth1,
                        nodo_utp=obj,
                        model=model,
                        ip=ipt,
                        active_ip=ipt_active,
                        firmware=i.get('firmware', None),
                        alias=i['description'])
                except IntegrityError as e:
                    print('duplicadoooo')
                    print(e)
                    # Este error ocurre cuando un Lease está duplicado y
                    # tiene el mismo alias, así que lo cambiamos para crearlo
                    cpe = CpeUtp.objects.create(
                        status=i['status'],
                        description=i['description'],
                        mac=mact,
                        mac_eth1=mact_eth1,
                        nodo_utp=obj,
                        model=model,
                        ip=ipt,
                        active_ip=ipt_active,
                        firmware=i.get('firmware', None),
                        alias=i['description'] + 'DUPLICADO')
                    cpe.save()

                if value_uptime:
                    print(value_uptime)
                    cpe.uptime = value_uptime
                    cpe.save()

                # se cra historico del lease
                print("ANTES DE CREAR EL LEASE")
                lease_object, created = Lease.objects.get_or_create(
                    #created=created_lease,
                    node=elem,
                    cpe=cpe,
                    address=ipt,
                    mac_address=mact,
                    client_id=i['client_id'],
                    server=i['server'],
                    active_address=ipt_active,
                    active_mac_address=mact_eth1,
                    active_client_id=i['active_client_id'],
                    hostname=i['description'],
                    last_seen=i['last_seen'],
                    status=i['status_lease']
                    # active_hostname = i['active_hostname']
                )
                lease_object.last_seen = i['last_seen']
                lease_object.status = i['last_seen']
                lease_object.save()
                
                print("LUEGO DE CREAR EL LEASE")

                print("============")
                print("CREANDO O BUSCANDO EL SERVICIO DEL CPE")
                cpe.service, created = Service.objects.get_or_create(
                    number=i['service_number'])
                if 'download_speed' in i.keys():
                    cpe.download_speed = i['download_speed']
                if 'upload_speed' in i.keys():
                    cpe.upload_speed = i['upload_speed']
                cpe.save()
                print(cpe)
                _l.CPEUTP.ADD(
                    by_representation=user,
                    id_value=cpe.id,
                    description=
                    f'CPE creado al actualizar lista CPEUTPE de un Mikrotik',
                    entity_representation=repr(cpe))
                print('ESTE ES EL OBJCPEUTPS')
                print(obj.cpeutps)
            elif i['is_cpe'] and i['service_number'] is None:
                try:
                    cpe = CpeUtp.objects.create(
                        status=i['status'],
                        description=i['description'],
                        mac=mact,
                        mac_eth1=mact_eth1,
                        nodo_utp=obj,
                        model=model,
                        ip=ipt,
                        active_ip=ipt_active,
                        firmware=i.get('firmware', None),
                        alias=i['description'])
                except IntegrityError:
                    # Este error ocurre cuando un Lease está duplicado
                    # así que chequeamos el estatus del actual
                    # Este error ocurre cuando un Lease está duplicado y
                    # tiene el mismo alias, así que lo cambiamos para crearlo
                    cpe = CpeUtp.objects.create(
                        status=i['status'],
                        description=i['description'],
                        mac=mact,
                        mac_eth1=mact_eth1,
                        nodo_utp=obj,
                        model=model,
                        ip=ipt,
                        active_ip=ipt_active,
                        firmware=i.get('firmware', None),
                        alias=i['description'] + 'DUPLICADO')
                    cpe.save()

                if value_uptime:
                    print(value_uptime)
                    cpe.uptime = value_uptime
                    cpe.save()

                # se cra historico del lease
                print("ANTES DE CREAR EL LEASE")
                lease_object, created = Lease.objects.get_or_create(
                    #created=created_lease,
                    node=elem,
                    cpe=cpe,
                    address=ipt,
                    mac_address=mact,
                    client_id=i['client_id'],
                    server=i['server'],
                    active_address=ipt_active,
                    active_mac_address=mact_eth1,
                    active_client_id=i['active_client_id'],
                    hostname=i['description'],
                )

                lease_object.last_seen = i['last_seen']
                lease_object.status = i['status_lease']
                lease_object.save()
                print("LUEGO DE CREAR EL LEASE")

                print("============")
                cpe.service = None
                if 'download_speed' in i.keys():
                    cpe.download_speed = i['download_speed']
                if 'upload_speed' in i.keys():
                    cpe.upload_speed = i['upload_speed']
                cpe.save()
                print(cpe)
                _l.CPEUTP.ADD(
                    by_representation=user,
                    id_value=cpe.id,
                    description=
                    f'CPE creado al actualizar lista CPEUTPE de un Mikrotik',
                    entity_representation=repr(cpe))
            else:
                print("DENTRO DEL ELSE DEL IS CPE AND SERVICE NUMBER")
                try:
                    print("DENTRO DEL TRY")
                    print(ipt)
                    print(mact)
                    try:
                        if ipt is not None:
                            onu = ONU.objects.get(mac=mact, ip=ipt)
                        else:
                            onu = ONU.objects.get(mac=mact)
                    except MultipleObjectsReturned:
                        print('VARIOS OBJETOS, BUSCANDO EN LA DESCRIPCION')
                        # Si hay varias ONUs con la misma MAC entonces buscamos
                        # la que contenga el nodo actual en su descripción
                        onu = ONU.objects.filter(
                            mac=mact,
                            description__icontains=elem.alias
                        ).first()
                        
                    print("ONU ENCONTRADA")

                    print("ANTES DEL UPDATE DE LA ONU")
                    update_onu(onu, i, onu.olt, user)
                    print("LUEGO DEL UPDATE DE LA ONU")
                    print("==========")
                    print("ANTES DE CREAR EL LEASE")
                    lease_object, created = Lease.objects.get_or_create(
                        #created=created_lease,
                        node=elem,
                        onu=onu,
                        address=ipt,
                        mac_address=mact,
                        client_id=i['client_id'],
                        server=i['server'],
                        active_address=ipt_active,
                        active_mac_address=mact_eth1,
                        active_client_id=i['active_client_id'],
                        hostname=i['description'],
                    )
                    lease_object.last_seen = i['last_seen']
                    lease_object.status = i['status_lease']
                    lease_object.save()
                    print("LUEGO DE CREAR EL LEASE")

                    if value_uptime:
                        print("DENTRO DEL IF VALUE UPTIME ")
                        print(value_uptime)
                        onu.uptime = value_uptime
                        onu.save()
                        print("ONU GUARDADA")

                    if i['service_number']:
                        print("DENTRO DEL IF I SERVICE NUMBER")
                        onu.service, created = Service.objects.get_or_create(
                            number=i['service_number'])

                    if 'download_speed' in i.keys():
                        onu.download_speed = i['download_speed']
                    if 'upload_speed' in i.keys():
                        onu.upload_speed = i['upload_speed']
                    onu.save()
                    print(onu)
                    _l.ONU.ADD(
                        by_representation=user,
                        id_value=onu.id,
                        description=
                        f'ONU creado al actualizar lista ONU de un Mikrotik',
                        entity_representation=repr(onu))

                except ONU.DoesNotExist:
                    print("DENTRO DEL ONU DOES NOT EXIST")
                    not_found, created = NotFoundMatrixEquipment.objects.get_or_create(
                        description=i['description'],
                        mac=mact,
                        node=obj,
                        model=model,
                        ip=ipt,
                        is_cpe=i['is_cpe'],
                        alias=i['description'],
                    )
                    print("NOT FOUND CREADO")

                    # se cra historico del lease
                    print("ANTES DE CREAR EL LEASE")
                    lease_object, created = Lease.objects.get_or_create(
                        #created=created_lease,
                        node=elem,
                        not_found=not_found,
                        address=ipt,
                        mac_address=mact,
                        client_id=i['client_id'],
                        server=i['server'],
                        active_address=ipt_active,
                        active_mac_address=mact_eth1,
                        active_client_id=i['active_client_id'],
                        hostname=i['description'],
                    )
                    lease_object.last_seen = i['last_seen']
                    lease_object.status = i['status_lease']
                    lease_object.save()
                    print("LUEGO DE CREAR EL LEASE")
                    if i['service_number']:
                        print("DENTRO DEL NOT FOUND SERICE NUMBER")
                        not_found.service, created = Service.objects.get_or_create(
                            number=i['service_number'])

                    if 'download_speed' in i.keys():
                        not_found.download_speed = i['download_speed']
                    if 'upload_speed' in i.keys():
                        not_found.upload_speed = i['upload_speed']
                    not_found.save()
                    print(not_found)

            print(1)

    elif len(leases) == 0 and (len(saved_cpeutp) != 0 or len(saved_onu) != 0
                               ):  # There is a cpe that needs to be deleted
        print("We need to delete cpeutp(s)")
        for i in saved_cpeutp:
            print("Deleting this cpe element")
            print(i)
            print(i.ip)
            i.delete()
            _l.CPEUTP.ADD(
                by_representation=user,
                id_value=i.id,
                description=
                f'CPE borrado al actualizar lista CPEUTP de Mikrotik',
                entity_representation=repr(i))
            _l.MIKROTIK.ADD(
                by_representation=user,
                id_value=obj.id,
                description=
                f'Eliminado CPE al actualizar lista CPEUTP del nodo',
                entity_representation=repr(obj))

        for i in saved_onu:
            print("Deleting this onu element")
            print(i)
            print(i.ip)
            i.deleted_sentinel = True
            _l.ONU.ADD(
                by_representation=user,
                id_value=i.id,
                description=f'ONU borrado al actualizar lista ONU de Mikrotik',
                entity_representation=repr(i))
            i.olt.save()
            _l.OLT.ADD(by_representation=user,
                       id_value=i.olt.id,
                       description=
                       f'Eliminado ONU al actualizar lista de ONU del nodo',
                       entity_representation=repr(i.olt))

    else:  # All other have already been updated, do nothing
        print("All have been updated")
        pass
    _l.MIKROTIK.ADD(by_representation=user,
                    id_value=elem.uuid,
                    description=f'Actualizada lista CPEUTP y ONU de Mikrotik',
                    entity_representation=repr(elem))

    elem.last_time_updated_leases = datetime.datetime.now()
    elem.save()


class ProfileUTPSerializer(QueryFieldsMixin, serializers.ModelSerializer):
    def create(self, validated_data):
        user = self.context['request'].user
        profile_name = validated_data.pop('name')
        limitation_name = validated_data.pop('limitation_name')
        download_speed = validated_data.pop('download_speed')
        upload_speed = validated_data.pop('upload_speed')
        radius = validated_data.pop('radius')

        try:
            radius_conn = RadiusConnection.objects.get(radius__id=radius.id)
            data = {
                "profile": profile_name,
                "limitation": limitation_name,
                "download_speed": download_speed,
                "upload_speed": upload_speed
            }
            info = create_radius_profile(radius_conn.radius.ip,
                                         radius_conn.username,
                                         radius_conn.password, data)
            if not isinstance(info, dict):
                msg = 'Falló la conexión vía API. Por favor revise que el username soporte API, \
                        y que el IP Services permita la conexión desde las IP 190.113.247.215 y \
                        190.113.247.219.'

                if info != "":
                    msg = msg + "\n" + info
                raise Exception('{}'.format(msg))

            obj = ProfileUTP.objects.create(name=profile_name,
                                            limitation_name=limitation_name,
                                            download_speed=download_speed,
                                            upload_speed=upload_speed,
                                            radius=radius)
            obj.save()

            _l.PROFILEUTP.ADD(by_representation=user,
                              id_value=obj.id,
                              description=f'Nuevo Profile',
                              entity_representation=repr(obj))

            return obj

        except Exception as e:
            _l.PROFILEUTP.ADD(by_representation=user,
                              id_value=radius,
                              id_field='id',
                              description=f'fallo crear profile: {e}')

            raise serializers.ValidationError({
                "error": f'fallo crear profile: {e}',
                "detail": str(e)
            })

    class Meta:
        model = ProfileUTP
        fields = ('id', 'name', 'download_speed', 'upload_speed',
                  'provisioning_available', 'limitation_name', 'description',
                  'radius', 'system_profile')


class SystemProfileSerializer(QueryFieldsMixin, serializers.ModelSerializer):
    class Meta:
        model = SystemProfile
        fields = ('id', 'name', 'download_speed', 'upload_speed',
                  'provisioning_available', 'limitation_name', 'description')


def update_profileutp(profile, i):
    """
    This method supports the main for updating cpe.
    Simply grabs a dict and updates de writable fields of a ProfileUTP instance.
    """
    boolean = True
    if i['name'] != profile.name:
        boolean = False
        profile.name = i['name']
    if i['limitation_name'] != profile.limitation_name:
        boolean = False
        profile.limitation_name = i['limitation_name']
    if "download_speed" in i.keys() and round(
            int(i['download_speed']) / 1000000, 0) != profile.download_speed:
        boolean = False
        profile.download_speed = round(int(i['download_speed']) / 1000000, 0)
    if "upload_speed" in i.keys() and round(
            int(i['upload_speed']) / 1000000, 0) != profile.upload_speed:
        boolean = False
        profile.upload_speed = round(int(i['upload_speed']) / 1000000, 0)
    if not boolean:
        profile.save()
        _l.PROFILEUTP.ADD(
            by_representation=user,
            id_value=profile.id,
            description=
            f'Actualizado profile al actualizar lista Profile de RADIUS',
            entity_representation=repr(profile))


def main_updateprofileutp(id_given):
    """
    This is a method used in order to update the profile list of a
    given RADIUS with the information in the device
    """
    # Get RADIUS
    elem = Radius.objects.get(uuid=id_given)
    # Get connections details for this device
    connection_data = RadiusConnection.objects.get(radius=elem)
    user = connection_data.username
    _l.RADIUS.ADD(
        by_representation=user,
        id_value=elem.id,
        description=f'Petición para actualizar lista Profile de RADIUS',
        entity_representation=repr(elem))
    try:
        # Get device profiles
        profiles = get_radius_profile(elem.ip, connection_data.username,
                                      connection_data.password)
        if not isinstance(profiles, list):
            raise Exception("Error: " + profiles)
        # Get cpes currently associated to utp node
        saved_profile = list(ProfileUTP.objects.filter(radius=elem))
        # Compare the two lists
        temp = []
        while len(profiles) != 0 and len(saved_profile) != 0:
            current = profiles.pop()
            temp.append(current)
            print("This is current element from profile list in device")
            print(current)
            for i in range(len(saved_profile)):
                if (current['name'] == saved_profile[i].name
                        and current['limitation_name']
                        == saved_profile[i].limitation_name):
                    print("found match in saved profile,updating")
                    update_profileutp(saved_profile[i], current)
                    saved_profile = extract_elem_from_list(saved_profile, i)
                    print("This is the new list")
                    print(saved_profile)
                    temp.pop()
                    break
        if len(profiles) == 0:
            profiles = temp
        print(profiles)
        if len(profiles) != 0 and len(
                saved_profile
        ) == 0:  # There is at least one that needs to be added
            print("Need to add new profile(s) to list")
            radius = elem
            for i in profiles:
                print(i)
                try:
                    if "download_speed" in i.keys():
                        ds = round(int(i['download_speed']) / 1000000, 0)
                    else:
                        ds = None
                    if "upload_speed" in i.keys():
                        up = round(int(i['upload_speed']) / 1000000, 0)
                    else:
                        up = None
                    obj = ProfileUTP.objects.create(
                        name=i['name'],
                        limitation_name=i['limitation_name'],
                        download_speed=ds,
                        upload_speed=up,
                        radius=radius)
                    obj.save()

                    _l.PROFILEUTP.ADD(by_representation=user,
                                      id_value=obj.id,
                                      description=f'Nuevo Profile',
                                      entity_representation=repr(obj))

                except Exception as e:
                    _l.PROFILEUTP.ADD(by_representation=user,
                                      id_value=id_given,
                                      id_field='uuid',
                                      description=f'fallo crear profile: {e}')

                    raise serializers.ValidationError({
                        "error": f'fallo crear profile: {e}',
                        "detail": str(e)
                    })
        elif len(profiles) == 0 and len(
                saved_profile) != 0:  # There is a cpe that needs to be deleted
            print("We need to delete profile(s)")
            for i in saved_profile:
                print("Deleting this element")
                print(i)
                print(i.name)
                i.delete()
                _l.PROFILEUTP.ADD(
                    by_representation=user,
                    id_value=i.id,
                    description=
                    f'Borrado profile al actualizar lista Profile de RADIUS',
                    entity_representation=repr(i))

        else:  # All other have already been updated, do nothing
            print("All have been updated")
            pass
        _l.RADIUS.ADD(by_representation=user,
                      id_value=elem.uuid,
                      description=f'Actualizada lista Profile de RADIUS',
                      entity_representation=repr(elem))

    except Exception as e:
        _l.RADIUS.ADD(by_representation=user,
                      id_value=id_given,
                      id_field='uuid',
                      description=f'fallo actualizar lista profile: {e}')

        raise serializers.ValidationError({
            "error": f'fallo actualizar lista profile: {e}',
            "detail": str(e)
        })


class ProvisioningSerializer(QueryFieldsMixin, serializers.Serializer):

    mik = serializers.CharField()

    def create(self, validated_data):

        return validated_data


class AvailabilitySerializer(QueryFieldsMixin, serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, min_value=1)
    # address = serializers.IntegerField(required=False, min_value=1)
    address = serializers.ModelField(
        required=True, model_field=Address()._meta.get_field('id'))

    # address = serializers.PrimaryKeyRelatedField(required=False, queryset=Address.objects.all())

    def validate_address(self, id_pulso):
        if id_pulso:
            try:
                addr, created = Address.objects.get_or_create(
                    id_pulso=id_pulso)
                return addr
            except Exception as e:
                _err = {'error': 'El id no existe en pulso.'}
                raise serializers.ValidationError(_err)
        else:
            return None

    def to_representation(self, instance):
        import pprint
        rep = super(AvailabilitySerializer, self).to_representation(instance)
        rep['address'] = instance.address.complete_location

        return rep

    class Meta:
        model = Availability
        fields = ('id', 'node', 'address')

        # create para crear  establecer una direccion


class GearSerializer(QueryFieldsMixin, serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, min_value=1)
    ip = serializers.CharField()
    mac = serializers.CharField()

    # node = serializers.ModelField(model_field=Mikrotik()._meta.get_field('id'))
    # model = serializers.ModelField( model_field=ModelDevice()._meta.get_field('id'))

    def validate_ip(self, ip):
        import ipaddress
        try:
            ipaddress.ip_address(ip)
        except ValueError as e:
            _err = {'error': 'La ip es invalida'}
            raise serializers.ValidationError(_err)

        ipt, create = IPTable.objects.get_or_create(ip=ip)

        return ipt

    def validate_mac(self, mac):
        if mac:
            try:
                mact, create = MACTable.objects.get_or_create(mac=mac)
                return mact

            except Exception as e:
                print('***\n', e, '\n*****')
                _err = {'error': str(e)}
                raise serializers.ValidationError(_err)
        else:
            return None

    def to_representation(self, instance):
        import pprint
        rep = super(GearSerializer, self).to_representation(instance)
        rep['node_alias'] = instance.node.alias
        rep['model_name'] = instance.model.model

        # lista las acciones realizadas en el historico por usuario y la fecha
        action_log = []
        for activity in instance.action_log():
            action_log.append([{
                'activity': f"""
                    <span class="text-primary">{ activity['user'] }</span> 
                    { activity['action'] } 
                    { '<strong>'+", ".join(map(str, activity['changes']))+'</strong>.' if 'changes' in activity else 'este servicio.' }
                """,
                'dt': str(activity['dt'])
            }])

        rep['action_log'] = action_log
        return rep

    class Meta:
        model = Gear
        fields = ('id', 'description', 'ip', 'vlan', 'node',
                  'firmware_version', 'notes', 'latest_backup_date_cache',
                  'mac', 'model', 'latest_backup_date',
                  'days_since_last_backup', 'days_since_last_backup_class',
                  'modified_at')


class DurationMikrotikSerializer(QueryFieldsMixin,
                                 serializers.ModelSerializer):
    class Meta:
        model = DurationMikrotik
        fields = [
            'id', 'duration', 'get_durantion_display', 'activity',
            'activity_id', 'node'
        ]


class BuildingContactSerializer(QueryFieldsMixin, serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, min_value=1)

    class Meta:
        model = BuildingContact
        fields = ('id', 'kind', 'name', 'phone', 'email', 'node')


class ChangeStatusNodeSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    node = serializers.CharField()
    status = serializers.CharField()
    nro_servicio = serializers.CharField()
    apariciones = serializers.IntegerField()
    fecha = serializers.CharField()


class ServiceField(serializers.RelatedField):
    def to_representation(self, value):
        service = value.number
        return service

    def to_internal_value(self, data):
        return data


class ServiceStatusLogSerializer(serializers.ModelSerializer):
    service = ServiceField(queryset=Service.objects.all(), many=False)

    class Meta:
        model = ServiceStatusLog
        fields = ('id', 'created_at', 'status', 'seller', 'node', 'technician',
                  'service')

    def create(self, validated_data):
        service = validated_data.pop('service')
        try:
            if Service.objects.filter(number=service).exists:
                servicio = Service.objects.get(number=service)
        except Service.DoesNotExist:
            print('No existe el servicio, pero se estará creando de inmediato')
            servicio = Service.objects.create(number=service)
            print(
                f'Se ha creado satisfactoriamente el servicio el con nro. {service}'
            )

        validated_data['service'] = servicio

        if not ServiceStatusLog.objects.filter(**validated_data).exists():
            ssl = ServiceStatusLog.objects.create(**validated_data)
            ssl.save()
            return ssl
        else:
            raise serializers.ValidationError("Este registro ya existe")


class ReportSerializer(serializers.Serializer):
    node = serializers.CharField()
    status = serializers.CharField()
    apariciones = serializers.IntegerField()


class InterfaceSerializer(QueryFieldsMixin, serializers.ModelSerializer):
    class Meta:
        model = Interface

        fields = (
            'node',
            'name',
            'type',
            'actual_mtu',
            'tx',
            'rx',
        )


class NeighborSerializer(QueryFieldsMixin, serializers.ModelSerializer):
    class Meta:
        model = Neighbor
        fields = (
            'interface',
            'address',
            'mac_address',
            'identity',
            'board_name',
            'uptime',
        )
