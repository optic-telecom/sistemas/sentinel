import re, time
import logging
import socket
import paramiko
from os import getenv
from os.path import join
from datetime import datetime
from django.conf import settings
from dynamic_preferences.registries import global_preferences_registry
from dashboard.utils import LOG



def get_logger(name, is_in_cron=True):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG) # WARNING ERROR

    # now = "{0.year}-{0.month}-{0.day}-{0.hour}".format(datetime.now()) 

    # if is_in_cron:
    #     ruta = join(settings.BASE_DIR_LOG,'mik_%s_cron.log' % now)
    # else:
    #     ruta = join(settings.BASE_DIR_LOG,'mik_%s_web.log' % now)
    ruta = join(settings.BASE_DIR_LOG,'mik_ssh_web.log')
    #file_log = open(str(ruta), "a+")
    format_log = "%(levelname)-.3s %(asctime)s.%(msecs)03d %(name)s %(message)s"
    #os.chmod(ruta, 777)
    handler1 = logging.StreamHandler(open(str(ruta), "a+"))
    handler1.setFormatter(logging.Formatter(format_log, "%Y%m%d-%H:%M:%S"))

    logger.addHandler(handler1)
    return logger



class ManagerSSH:

    def __init__(self, host, username, password, port=22, key=None):
        self.host = host
        self.username = username
        self.password = password
        self.port = port
        self.key = key
        self.get_logger()
        self.ssh = None
        self.name_logger = None

    def get_logger(self):
        # if hasattr(self,'name_logger') and self.name_logger is not None:
        #     self.logger = get_logger(self.name_logger)
        # else:
        #     self.logger = get_logger('ManagerSSH')
        self.logger = get_logger("MikroSSH")
        print ('MI get_logger es',self.logger)
        
    def __repr__(self):
        """
        Returns a string representation of this object, for debugging.
        """
        return "<ManagerSSH from sentinel-mikrotik.utp.ssh>"

    def connect(self):
        self.ssh = paramiko.SSHClient()
        try:

            self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

            if self.key:
                self.ssh.connect(self.host, username=self.username,
                                 password=self.password,
                                 port=self.port, key_filename=self.key)
            else:
                print("connecting")
                self.ssh.connect(self.host, username=self.username,
                                 password=self.password, port=self.port)                
            print("connected")
            self.ssh._transport.get_banner()
            self.ssh._transport.set_log_channel('SSHSentinel')
            return True

        except paramiko.ssh_exception.NoValidConnectionsError as e:
            self.logger.error('Connection Failed')
            self.logger.error(e)
            return False

        except socket.gaierror as e:
            self.logger.error('Socket not being to resolve the address (%s)' % self.host)
            self.logger.error(e)
            return False

        except paramiko.SSHException as e:
            self.logger.error('Error connecting to host %s' % self.host)
            self.logger.error('Host is not a valid SSH server - Username is not valid or incorrect password')
            self.logger.error(e)
            return False

        except TimeoutError:
            self.logger.error('The %s host didn\'t respond for TimeoutError' % self.host)
            return False

        except Exception as err:
            print("except")
            self.logger.error('Exception %s' % str(err))
            return False


    def close(self):
        if self.ssh:
            self.ssh.close()

    def __del__(self):
        print("close")
        #if self.ssh:
            #self.ssh.close()


class DeviceSSHMixin(object):
    global_preferences = global_preferences_registry.manager()
    chan = None
    connected = False
    decode_type = 'UTF-8'
    raw = None
    buff = 1024 * 2
    _l = None
    try:
        time_sleep = global_preferences.get('times__sleep_high')
    except Exception as e:
        time_sleep = 5
    
    err_connection_msg = 'Usted no tiene conexion ssh.'

    def __init__(self, host, username, password, port=22, key=None):
        super().__init__(host, username, password, port, key)
        if self.connect():
            self.connected = True

    def get_sleep(self):
        time.sleep(2)

    def shell(self, mikrotik=None, by_representation=None):
        assert self.connected, (self.err_connection_msg)
        self.chan = self.ssh.invoke_shell()
        self._in = self.chan.makefile('wb', -1)
        self._out = self.chan.makefile('rb', -1)
        self._err = self.chan.makefile_stderr('rb', -1)

        # time.sleep(global_preferences['times__sleep_very_low'])
        #self.command(' \n\n\n')
        #self.command('system health print\n')
        # time.sleep(global_preferences['times__sleep_very_low'])
        # self.command('config\n')
        # self.get_sleep()

        if mikrotik:
            self.mikrotik = mikrotik
        if by_representation:
            self.by_representation = by_representation

    def simple_shell(self):
        global_preferences = global_preferences_registry.manager()
        assert self.connected, (self.err_connection_msg)
        self.chan = self.ssh.invoke_shell()
        self._in = self.chan.makefile('wb', -1)
        self._out = self.chan.makefile('rb', -1)
        self._err = self.chan.makefile_stderr('rb', -1)
        time.sleep(global_preferences['times__sleep_very_low'])

    def command(self, cmd, buff=None, split=True, pause=None, flush=True):
        assert self.connected, (self.err_connection_msg)
        #self.logger.info("command: %s " % cmd)
        self.log_class.info("s: %s " % cmd)

        if hasattr(self,'_l') and hasattr(self,'mikrotik'):
            try:
                by_representation = self.by_representation
            except AttributeError:
                by_representation = 'system'

            self._l.MIKROTIK.SHELL(by_representation=by_representation, id_value=self.mikrotik.uuid,
            description=cmd, entity_representation=repr(self.mikrotik))

        if flush:
            self._in.flush()
            self._out.flush() 
        buff = buff if buff else self.buff
        self.chan.send(cmd)
        
        if pause:
            time.sleep(pause)
        else:
            self.get_sleep()

        resp = self.chan.recv(buff)
        self.raw = resp
        output = resp.decode(self.decode_type)

        if hasattr(self,'_l') and hasattr(self,'mikrotik'):
            try:
                by_representation = self.by_representation
            except AttributeError:
                by_representation = 'system'
            self._l.MIKROTIK.SHELL(by_representation=by_representation, id_value=self.mikrotik.uuid,
            description=output, entity_representation=repr(self.mikrotik))

        if split:
            o = output.replace("\x1b[37D", '')\
                .replace('\x1b[1DB48C09B','').split()
        else:
            o = output.replace("\x1b[37D", '')\
                .replace('\x1b[1DB48C09B','')
                
        self.log_class.info("r: %s " % o)
        self.last_command = cmd
        return o

    def cmd(self, cmd, **options):
        assert self.connected, (self.err_connection_msg)
        define_action = options.pop('define_action', None)

        if options.get('log' , True):
            #self.logger.info("cmd: %s " % cmd)
            self.log_class.info("s: %s " % cmd)
            if hasattr(self,'_l') and hasattr(self,'mikrotik'):
                try:
                    by_representation = self.by_representation
                except AttributeError:
                    by_representation = 'system'

                if define_action:
                    met = getattr(self._l.MIKROTIK, define_action)
                    met(by_representation=by_representation, id_value=self.mikrotik.uuid,
                    description=cmd, entity_representation=repr(self.mikrotik))
                else:
                    self._l.MIKROTIK.SHELL(by_representation=by_representation, id_value=self.mikrotik.uuid,
                    description=cmd, entity_representation=repr(self.mikrotik))

        if options.get('flush', True):
            self._in.flush()
            self._out.flush() 
        buff = buff if options.get('buff', None) else self.buff
        self.chan.send(cmd)
        
        if options.get('pause', None):
            time.sleep(options.get('pause',None))
        else:
            self.get_sleep()

        resp = self.chan.recv(buff)
        self.raw = resp
        output = resp.decode(self.decode_type)
        o = output.replace("\x1b[37D", '')\
                .replace('\x1b[1DB48C09B','')

        if options.get('log' , True):
            self.log_class.info("r: %s " % o)
            if hasattr(self,'_l') and hasattr(self,'mikrotik'):
                try:
                    by_representation = self.by_representation
                except AttributeError:
                    by_representation = 'system'

                if define_action:
                    met = getattr(self._l.MIKROTIK, define_action)
                    met(by_representation=by_representation, id_value=self.mikrotik.uuid,
                    description=o, entity_representation=repr(self.mikrotik))
                else:
                    self._l.MIKROTIK.SHELL(by_representation=by_representation, id_value=self.mikrotik.uuid,
                    description=o, entity_representation=repr(self.mikrotik))

        if options.get('split', None):
            o = o.split()

          
        self.last_command = cmd
        #print ('pre return')
        return o


    def execute(self, cmd):
        assert self.connected, (self.err_connection_msg)
        self.logger.info("execute: %s " % cmd)
        self._in.flush()
        self._out.flush()
        self.chan.send(cmd)
        

    def parse_integer(self, inputs, position, return_first=True):
        print ("inputs %s " % str(inputs))
        try:
            coincidences = re.findall('\d+', inputs[position])
            if return_first:
                return coincidences[0]
            return coincidences
        except IndexError as e:
            return 0


    def parse_join(self, inputs, start, end, separator=" "):
        return separator.join(inputs[start:end])

    def parse_lines(self, data):
        text = str(data)
        lines = text.split("\\r\\n")
        return_lines = []
        replaces = []
        a = '<0x1b>'
        replaces.append(a)        
        for line in lines:

            for rep in replaces:
                line = line.replace(rep,'')

            return_lines.append(str(line))

        return return_lines

        
class UTP_SSH(DeviceSSHMixin, ManagerSSH):

    output_binary = None
    set_interface = False

    def __init__(self, *args, **kwargs):
        is_in_cron = kwargs.pop('is_in_cron', False)
        self._l = kwargs.pop('_l', LOG())
        if kwargs.get('mikrotik',False):
            self.mikrotik = kwargs.pop('mikrotik')

        super().__init__(*args, **kwargs)
        self.get_logger_class(is_in_cron)

    def get_logger_class(self, is_in_cron):
        self.log_class = get_logger('UTP_SSH', is_in_cron)
        print('self.log_class',self.log_class)
        
    def get_health(self):
        #self.command('system health print', None, False)
        stdin,stdout,stderr = self.ssh.exec_command("system health print")
        out = stdout.readlines()
        response = {}
        for i in out:
            i = i.strip().replace("\r","").replace("\n","")
            if ":" in i:
                temp = i.split(":")
                if len(temp)==2:
                    response[temp[0].strip()] = temp[1].strip()
        return response

    def get_mac_telnet(self,mac):
        stdin,stdout,stderr = self.ssh.exec_command("tool mac-telnet "+mac)
        out = stdout.readlines()
        # print(stdin.read())
        # print(stderr.read())
        return out
