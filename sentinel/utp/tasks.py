import time
import json
from datetime import timedelta
import multiprocessing
from threading import Thread

from django.utils import timezone
from django.db.models import Q

from celery import task, shared_task
from config.celery import app
from celery.schedules import crontab
from utp.models import Mikrotik
from dashboard.models import TaskModel
from dashboard.utils import LOG
from .mikrotik import drop_client, activate_client


@task(bind=True)
def generate_csv(self):
    import time
    time.sleep(50)


@task(bind=True, default_retry_delay=120, max_retries=10)
def task_cutting_replacement(self, contract, _type):

    if (_type == 'corte'):
        drop_client(contract)
    elif (_type == 'reposicion'):
        activate_client(contract)
    else:
        return False

    return True


@task(bind=True, default_retry_delay=120, max_retries=10)
def task_get_leases(self, uuid):
    mikrotik = Mikrotik.objects.get(uuid=uuid)
    from utp.management.commands.get_leases import Command as GetLeases
    command = GetLeases()
    command.getdata(mikrotik)


@task(bind=True, default_retry_delay=120, max_retries=10)
def task_get_all_leases(self):
    mikrotiks = Mikrotik.objects.all()
    from utp.management.commands.get_leases import Command as GetLeases
    command = GetLeases()
    command.handle()


@task(bind=True, default_retry_delay=12, max_retries=1)
def task_new_mikrotik(self, uuid, name_task="task_new_mikrotik"):
    try:

        mikrotik = Mikrotik.objects.get(uuid=uuid)
        print(
            '|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||'
        )
        from utp.management.commands.get_mikrotik import Command as GetMikrotik
        command = GetMikrotik()
        command.getdata(mikrotik)

        from utp.management.commands.get_leases import Command as GetLeases
        command = GetLeases()
        command.getdata(mikrotik)

        try:
            task_model = TaskModel.objects.get(id_obj=mikrotik.id,
                                               model_obj='mikrotik',
                                               name=name_task,
                                               status_field=True,
                                               status='PENDING')
            task_model.date_done = timezone.now()
            task_model.result = True
            task_model.status = 'SUCCESS'
            task_model.save()
        except TaskModel.DoesNotExist:
            task_model = None

        try:
            task_model = TaskModel.objects.get(id_obj=mikrotik.id,
                                               model_obj='mikrotik',
                                               name='task_update_mikrotik',
                                               status_field=True,
                                               status='PENDING')
            task_model.date_done = timezone.now()
            task_model.result = True
            task_model.status = 'SUCCESS'
            task_model.save()
        except TaskModel.DoesNotExist:
            task_model = None

    except Exception as e:
        raise self.retry(exc=e)


@task
def get_neighbors():
    from utp.management.commands.get_neighbors import Command as GetNeighbor
    command = GetNeighbor()

    nodes = Mikrotik.objects.all()
    execute_in_thread(nodes, command.getdata)


THREAD_NUM = multiprocessing.cpu_count()


def execute_in_thread(_list, _func, **options):
    ''' Split a list into equal parts depending on the CPUs quantity and run a function in
        a thread for each list.

        Parameters:
        _list (list): listo to be splited
        _func (function): function to be executed
        **options (dict): dictionary with paratemters to pass to _func

        Returns: 
        None
    '''
    def call_function(split_list):
        for item in split_list:
            _func(item, **options)

    lng = int(len(_list) / THREAD_NUM)

    try:
        threads = []

        for i in range(THREAD_NUM):
            end = len(_list) - 1 if i == THREAD_NUM - 1 else int((i * lng) +
                                                                 lng) - 1
            start = int(i * lng)
            t = Thread(target=call_function, args=(_list[start:end], ))
            t.start()
            threads.append(t)

        for thread in threads:
            thread.join()
    except AssertionError:
        pass