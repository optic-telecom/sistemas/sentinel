import pytest
import requests
from datetime import datetime


@pytest.mark.django_db
class TestUTPApis():
    def test_create_group(self, url, client, headers, fail):

        pass_payload = {
            'id': '1',
            'name': 'test'
        }

        fail_payload = {
            'id': 'aaa'
        }

        request = client.post("{}/group/".format(url),
                              data=pass_payload if not fail else fail_payload,
                              headers=headers, verify=False)

        assert request.status_code == 201

    def test_create_planorderkind(self, url, client, headers, fail):
        pass_payload = {
            'id': '1',
            'name': 'test'
        }

        fail_payload = {
            'id': 'aaa'
        }

        request = client.post("{}/planorderkind/".format(url),
                              data=pass_payload if not fail else fail_payload,
                              headers=headers, verify=False)

        assert request.status_code == 201

    def test_create_node(self, url, client, headers, fail,
                         pulso_url, test_planorderkind, test_plan,
                         test_group, test_IPRange):

        pulso_payload = {
            'commune': 'algo',
            'street': 'algo'
        }

        pulso_request = requests.get(pulso_url, pulso_payload)
        dire_id = pulso_request.json()['results'][0]['id']

        pass_payload = {
                "id": '1',
                "group": '',
                "alias": 'test',
                "status": '1',
                "address_pulso": dire_id,
                "towers": '1',
                "apartments": '1',
                "phone": '111',
                "ip": '1.1.1.1',
                "ip_range": test_IPRange,
                "comment": 'test',
                "no_address_binding": True,
                'plans': test_plan.id,
                'plan_order_kinds': test_planorderkind.id,
                'matrix_seller': '1',
                'user': 'test',
                'password': 'test',
                'description': 'None',
                'type': '1'
            }

        fail_payload = {
                "id": '1',
                "group": test_group,
                "alias": 'test',
                "status": '1',
                "address_pulso": dire_id,
                "towers": '1',
                "apartments": '1',
                "phone": '111',
                "ip": 'ip',
                "ip_range": '1',
                "comment": 'test',
                "no_address_binding": True,
                'plans': test_plan,
                'plan_order_kinds': test_planorderkind,
                'matrix_seller': '1',
                'user': 'test',
                'password': 'test',
                'description': 'None',
                'type': type
            }

        request = client.post("{}/utp/".format(url),
                              data=pass_payload if not fail else fail_payload,
                              headers=headers, verify=False)
        print(request.json())
        assert request.status_code == 201

    def test_create_gear(self, test_node, headers, url, client, fail,
                         test_IPTable, test_MACTable, test_model):

        pass_payload = {
                "id": '1',
                'description': 'test',
                'ip': '1.1.1.1',
                'mac': test_MACTable.id,
                'vlan': 'None',
                'node': test_node.id,
                'firmware_version': 'None',
                'notes': 'test',
                'latest_backup_date_cache': datetime.today(),
                'model': test_model.id
            }

        fail_payload = {
                "id": 'aaaaaaa',
                'description': 'test',
                'ip': '1.1.1.1',
                'mac': test_MACTable.id,
                'vlan': 'None',
                'node': test_node.id,
                'firmware_version': 'None',
                'notes': 'test',
                'latest_backup_date_cache': datetime.today(),
                'model': test_model.id
            }

        request = client.post("{}/gear/".format(url),
                              data=pass_payload if not fail else fail_payload,
                              headers=headers, verify=False)
        print(request.json())
        assert request.status_code == 201

    def test_create_cpeutp(self, test_node, headers, url, client, 
                           fail, test_service, test_model, test_MACTable,
                           test_IPTable):
        pass_payload = {
                    'alias': 'test',
                    'id': '1',
                    'serial': 'test',
                    'nodo_utp': test_node.id,
                    'description': 'test',
                    'model': test_model.id,
                    'service': test_service.id,
                    'service_number_verified': False,
                    'mac': test_MACTable.id,
                    'ip': '1.1.1.1',
                    'primary': True,
                    'ssid': 'test',
                    'password': 'test',
                    'ssid_5g': 'test'
                }

        fail_payload = {
                    'alias': 'test',
                    'id': 'aaaa',
                    'serial': 'test',
                    'nodo_utp': test_node.id,
                    'description': 'test',
                    'model': test_model.id,
                    'service': test_service.id,
                    'service_number_verified': False,
                    'mac': test_MACTable.id,
                    'ip': '1.1.1.1',
                    'primary': True,
                    'ssid': 'test',
                    'password': 'test',
                    'ssid_5g': 'test'
                }

        request = client.post("{}/cpeutp/".format(url),
                              data=pass_payload if not fail else fail_payload,
                              headers=headers, verify=False)
        print(request.json())
        assert request.status_code == 201

    def test_create_building_contacts(self, test_node, headers, url, client,
                                      fail):
        pass_payload = {
                'id': '1',
                'kind': '1',
                'name': 'test',
                'phone': '111',
                'email': 'test@test.com',
                'node': test_node.id,
            }

        fail_payload = {
                'id': 'aaa',
                'kind': 'test',
                'name': 'test',
                'phone': '111',
                'email': 'test@test.com',
                'node': test_node.id,
            }

        request = client.post("{}/building_contact/".format(url),
                              data=pass_payload if not fail else fail_payload,
                              headers=headers, verify=False)
        print(request.json())
        assert request.status_code == 201
