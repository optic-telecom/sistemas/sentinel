from rest_framework import routers
from .views import *
from .views import ChangeStatusNodeView,ServiceStatusLogView,GeneralServiceStatusLogReport
from .models import MikrotikList
from django.urls import re_path, path
from tv.autocompletes import AutocompleteSelect2,ListadoAutocomplete
# from .views import cutting_replacement, ServiceList, ServiceRetrieve, \
#                     UTPViewSet, CpeUtpViewSet, OperatorViewSet, ProfileViewSet, \
#                     SystemProfileViewSet, TypePLANViewSet, PlanViewSet, RadiusViewSet

from utp.autocompletes import MikrotikAutocomplete

router = routers.DefaultRouter()

router.register(r'utp', UTPViewSet, 'utp_api')
router.register(r'cpeutp', CpeUtpViewSet, 'cpeutp_api')
router.register(r'profileutp', ProfileViewSet, 'profile_utp_api')
router.register(r'system_profileutp', SystemProfileViewSet, 'system_profile_utp_api')
router.register(r'radius', RadiusViewSet, 'radiusutp_api')
router.register(r'group', GroupViewSet, 'group_api')
router.register(r'planorderkind', PlanOrderKindViewSet, 'planorderkind_api')
router.register(r'gear', GearViewSet, 'gear_api')
router.register(r'availability', AvailabilityViewSet, 'availability_api')
# router.register(r'antenna', AntennaViewSet, 'antenna_api')
router.register(r'durations', DurationMikrotikViewSet, 'duration_api')

router.register('building_contact', BuildingContactViewSet, 'building_contact_api')

router.register(r'csn', ChangeStatusNodeView, 'csn_api')
router.register(r'ssl', ServiceStatusLogView, 'ssl_api')
router.register(r'general-report-service', GeneralServiceStatusLogReport, 'grs_api')


urlpatterns = [
    re_path(r'^cutting_replacement', cutting_replacement),
    re_path(r'^activities/', get_activities),
    re_path(r'^update_list_cpe/(?P<uuid>[-\w]+)/$',updateCpeUtp),

    # ============= AUTOCOMPLETE'S =============== #
    re_path(r'^node-ac/',(AutocompleteSelect2
                            .as_view(model=MikrotikList,
                                    filter_by="alias__istartswith")),
                        name="node-ac"),  

    re_path(r'^status-matrix-ac/',ListadoAutocomplete.as_view(
        listado = ['activo','por retirar','retirado', 'no instalado','canje',
                'instalación rechazada','venta en verde','moroso','por re-agendar',
                'cambio de titular','descartado','baja temporal',
                'perdido, no se pudo recuperar']
    ),name="status-matrix-ac"),

    path("autocomplete-mikrotik/", MikrotikAutocomplete.as_view(), name="ac-mikrotiks"),
]