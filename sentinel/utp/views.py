#from django.contrib.auth import get_user_model
import requests
import routeros_api
import numpy as np
import pandas as pd
from django_filters.rest_framework import DjangoFilterBackend
from django.views.decorators.csrf import csrf_exempt
from dynamic_preferences.registries import global_preferences_registry

from rest_framework import status
from rest_framework import viewsets, mixins
from rest_framework.decorators import action, api_view, permission_classes, parser_classes
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated, AllowAny
from .permissions import IsSystemInternalPermission
from rest_framework.generics import get_object_or_404, ListAPIView, RetrieveAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
# from rest_framework.decorators import list_route
from rest_framework.renderers import JSONRenderer

from .serializers import MikrotikSerializer, CPEUTPSerializer, \
                         ProfileUTPSerializer, RadiusSerializer, \
                            main_updatecpeutp, main_updateprofileutp, \
                            SystemProfileSerializer, GroupSerializer, PlanOrderKindSerializer, \
                            GearSerializer, AvailabilitySerializer, DurationMikrotikSerializer

from .models import Mikrotik, MikrotikConnection,ModelDevice, CpeUtp, Group, Gear, Availability,\
                    ProfileUTP, Radius, RadiusConnection, SystemProfile, PlanOrderKind, Antenna, DurationMikrotik, \
                    MikrotikList,ServiceStatusLog, ChangeStatusNode

from .mikrotik import get_mikrotik_leases, delete_radius_profile_witheverything,\
                      delete_radius_profile_onlylink, get_neighbors

from dashboard.utils import LOG

from .views_helper import *

from .permissions import IsSystemInternalPermission

from documents.serializers import PicSerializer, DocSerializer

from .serializers import BuildingContactSerializer, ChangeStatusNodeSerializer, ServiceStatusLogSerializer
from .models import BuildingContact, ChangeStatusNode, ServiceStatusLog

__all__ = [
    'UTPViewSet', 'CpeUtpViewSet', 'ProfileViewSet', 'RadiusViewSet',
    'updateCpeUtp', 'AvailabilityViewSet', 'updateProfile',
    'SystemProfileViewSet', 'cutting_replacement', 'GroupViewSet',
    'PlanOrderKindViewSet', 'GearViewSet', 'DurationMikrotikViewSet',
    'get_activities', 'BuildingContactViewSet'
]

_l = LOG()


class GroupViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Group
    retrieve:
        End point for Group
    update:
        End point for Group
    delete:
        End point for Group
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class PlanOrderKindViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Group
    retrieve:
        End point for Group
    update:
        End point for Group
    delete:
        End point for Group
    """
    queryset = PlanOrderKind.objects.all()
    serializer_class = PlanOrderKindSerializer


class UTPViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Mikrotik
    retrieve:
        End point for Mikrotik
    update:
        End point for Mikrotik
    delete:
        End point for Mikrotik
    """
    queryset = Mikrotik.objects.all()
    serializer_class = MikrotikSerializer
    lookup_field = 'uuid'
    lookup_url_kwarg = 'uuid'

    def get_queryset(self):
        for_alias = self.request.GET.get('alias', None)
        search = self.request.GET.get('search', None)

        if search:
            return Mikrotik.objects.filter(alias__icontains=search)

        if for_alias:
            return Mikrotik.objects.filter(alias__icontains=for_alias)
        return Mikrotik.objects.filter()

    @action(detail=True, methods=['post'])
    @parser_classes([MultiPartParser])
    def add_gallery(self, request, version=None, uuid=None):

        try:
            node = Mikrotik.objects.get(id=uuid)
        except ValueError:
            return build_response(
                {"detail": f"El node con uuid = {uuid} no fue encontrado"},
                status.HTTP_400_BAD_REQUEST)

        return add_gallery(node, request)

    @action(detail=True, methods=['get'])
    def list_gallery(self, request, version=None, uuid=None):
        try:
            node = Mikrotik.objects.get(uuid=uuid)
        except Mikrotik.DoesNotExist:
            response = Response(
                {
                    "detail":
                    f"El equipo interno con id = {uuid} no fue encontrado"
                },
                content_type="application/json",
                status=status.HTTP_400_BAD_REQUEST,
            )
            response.accepted_renderer = JSONRenderer()
            response.accepted_media_type = "application/json"
            response.renderer_context = {}
            return response

        picSerializer = PicSerializer(node.pics.all(),
                                      context={"request": request},
                                      many=True)
        return build_response(picSerializer.data, 200)

    @action(detail=True, methods=['get'])
    def list_document(self, request, version=None, uuid=None):
        try:
            node = Mikrotik.objects.get(uuid=uuid)
        except Mikrotik.DoesNotExist:
            response = Response(
                {
                    "detail":
                    f"El equipo interno con id = {uuid} no fue encontrado"
                },
                content_type="application/json",
                status=status.HTTP_400_BAD_REQUEST,
            )
            response.accepted_renderer = JSONRenderer()
            response.accepted_media_type = "application/json"
            response.renderer_context = {}
            return response

        docSerializer = DocSerializer(node.documents.all(),
                                      context={"request": request},
                                      many=True)
        return build_response(docSerializer.data, 200)

    @action(detail=True, methods=['get'])
    def get_gears(self, request, version=None, uuid=None):
        try:
            node = Mikrotik.objects.get(uuid=uuid)
        except Mikrotik.DoesNotExist:
            response = Response(
                {"detail": f"El nodo con id = {uuid} no fue encontrado"},
                content_type="application/json",
                status=status.HTTP_400_BAD_REQUEST,
            )
            response.accepted_renderer = JSONRenderer()
            response.accepted_media_type = "application/json"
            response.renderer_context = {}
            return response

        # gearSerializer = GearSerializer(node.get_gears, many=True)
        # print('**************')
        # print(gearSerializer.data)
        return build_response(node.get_gears, 200)

    @action(detail=True, methods=['get'])
    def services_report(self, request, version=None, uuid=None):
        global_preferences = global_preferences_registry.manager()
        try:
            node = Mikrotik.objects.get(uuid=uuid)
        except Mikrotik.DoesNotExist:
            response = Response(
                {"detail": f"El nodo con id = {uuid} no fue encontrado"},
                content_type="application/json",
                status=status.HTTP_400_BAD_REQUEST,
            )
            response.accepted_renderer = JSONRenderer()
            response.accepted_media_type = "application/json"
            response.renderer_context = {}
            return response

        try:

            services = []
            for cpe in node.cpeutp_set.all():
                print(cpe)
                print(cpe.service.number)
                headers = {
                    'Authorization':
                    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6InRva2VubWF0aWMifQ.i-OzPIBtAKrXyW_hmQDPPgRxZ8gXXLdroyNWzqWJz6s'
                }

                try:

                    r = requests.get(
                        f'{global_preferences["url_endpoint_erp"]}api/v1/services/?number={cpe.service.number}',
                        headers=headers)
                    print(r.text)
                except Exception as e:
                    print(e)
                    r = requests.get(
                        f'http://localhost:8001/api/v1/services/?number={cpe.service.number}',
                        headers=headers)

                print(r.text)
                service_object = r.json()['results'][0]
                print(service_object)
                services.append({
                    'number':
                    service_object['number'],
                    'composite_address':
                    service_object['composite_address'],
                    'status':
                    service_object['status'],
                })

            actives = len(
                list(filter(lambda service: service['status'] == 1, services)))
            pending = len(
                list(filter(lambda service: service['status'] == 2, services)))
            inactive = len(
                list(filter(lambda service: service['status'] == 3, services)))
            not_installed = len(
                list(filter(lambda service: service['status'] == 4, services)))
            free = len(
                list(filter(lambda service: service['status'] == 5, services)))
            install_rejected = len(
                list(filter(lambda service: service['status'] == 6, services)))
            off_plan_sale = len(
                list(filter(lambda service: service['status'] == 7, services)))
            moroso = len(
                list(filter(lambda service: service['status'] == 8, services)))
            to_reschedule = len(
                list(filter(lambda service: service['status'] == 9, services)))
            holder_change = len(
                list(filter(lambda service: service['status'] == 10,
                            services)))
            discared = len(
                list(filter(lambda service: service['status'] == 11,
                            services)))
            on_hold = len(
                list(filter(lambda service: service['status'] == 12,
                            services)))
            lost = len(
                list(filter(lambda service: service['status'] == 13,
                            services)))

            response = {
                'indicators': {
                    'actives': actives,
                    'pending': pending,
                    'inactive': inactive,
                    'not_installed': not_installed,
                    'free': free,
                    'install_rejected': install_rejected,
                    'off_plan_sale': off_plan_sale,
                    'moroso': moroso,
                    'to_reschedule': to_reschedule,
                    'holder_change': holder_change,
                    'discared': discared,
                    'on_hold': on_hold,
                    'lost': lost,
                },
                'services': services
            }

            return build_response(response, 200)

        except:
            return build_response(
                {'detail': 'Error al consultar api de Matrix'}, 400)

    @action(detail=True, methods=['post'])
    def get_neighbors(self, request, version=None, uuid=None):
        try:
            node = Mikrotik.objects.get(uuid=uuid)
        except Mikrotik.DoesNotExist:
            return build_response(
                {"detail": f"El nodo con id = {uuid} no fue encontrado"},
                status.HTTP_400_BAD_REQUEST)

        for conn in MikrotikConnection.objects.filter(mikrotik=node):
            try:
                get_neighbors(node.ip.ip, conn.username, conn.password)
                return build_response({"ok": True}, status.HTTP_200_OK)
            except Exception as e:
                print(e)
                return build_response(
                    {
                        "detail":
                        f"No fue posible conectarse con el nodo {node.alias}"
                    }, status.HTTP_422_UNPROCESSABLE_ENTITY)

    @action(detail=True, methods=['post'])
    @parser_classes([MultiPartParser])
    def add_document(self, request, version=None, uuid=None):
        try:
            node = Mikrotik.objects.get(id=uuid)
        except Mikrotik.DoesNotExist:
            response = Response(
                {"detail": f"El node con uuid = {uuid} no fue encontrado"},
                content_type="application/json",
                status=status.HTTP_400_BAD_REQUEST,
            )
            response.accepted_renderer = JSONRenderer()
            response.accepted_media_type = "application/json"
            response.renderer_context = {}
            return response

        return add_document(node, request)

    @action(detail=True, methods=['post'])
    def add_history(self, request, version=None, uuid=None):

        try:
            mikrotik = Mikrotik.objects.get(uuid=uuid)
        except Exception as e:
            return build_response(
                f"El servicio con uuid = {uuid} no fue encontrado",
                status.HTTP_400_BAD_REQUEST)

        return add_mikrotik_history(mikrotik, request.data)


class CpeUtpViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for CPEUTP
    retrieve:
        End point for CPEUTP
    update:
        End point for CPEUTP
    delete:
        End point for CPEUTP
    retrieve_by_service_number
        End point for retrieve CPEUTP by service number(uuid)
    """
    queryset = CpeUtp.objects.all()
    serializer_class = CPEUTPSerializer
    lookup_field = 'uuid'
    lookup_url_kwarg = 'uuid'
    
    @action(methods=['post'], detail=True)
    def modify_description(self, request, version=None, uuid=None):
        try:
            cpe = CpeUtp.objects.get(id=uuid)
        except ValueError:
            response = Response(
                {"detail": "ID inválido"},
                content_type="application/json",
                status=status.HTTP_400_BAD_REQUEST,
            )
            response.accepted_renderer = JSONRenderer()
            response.accepted_media_type = "application/json"
            response.renderer_context = {}
            return response
        
        description = request.data.get('description', None) 
        if description is None or description == "":
            response = Response(
                {"error": "La descripción no puede estar vacía"},
                content_type="application/json",
                status=status.HTTP_400_BAD_REQUEST,
            )
            response.accepted_renderer = JSONRenderer()
            response.accepted_media_type = "application/json"
            response.renderer_context = {}
            return response
        
        # Buscamos las credenciales del nodo UTP       
        connection_creds = MikrotikConnection.objects.get(mikrotik=cpe.nodo_utp)
        username = connection_creds.username
        password = connection_creds.password
        print(username)
        print(password)
        # Establecemos la conexión con el cpe
        try:
            
            connection = routeros_api.RouterOsApiPool(
                cpe.ip.ip, username=username, 
                password=password, plaintext_login=True
            )
            api = connection.get_api()
        except Exception as e:
            print(e)
            response = Response(
                {"error": "No se ha podido realizar la conexión"},
                content_type="application/json",
                status=status.HTTP_400_BAD_REQUEST,
            )
            response.accepted_renderer = JSONRenderer()
            response.accepted_media_type = "application/json"
            response.renderer_context = {}
            return response
        
        config_hostname = api.get_resource("/system/identity")     
        config_hostname.set(name=description)
        # Actualizamos
        api.get_binary_resource('/').call('ip/dhcp-client', {'release': 0})
        api.get_binary_resource('/').call('ip/dhcp-client', {'renew': 0})
        connection.disconnect()        
        
        response = Response({'success':'editado correctamente'}, status=200, content_type="application/json")
        response.accepted_renderer = JSONRenderer()
        response.accepted_media_type = "application/json"
        response.renderer_context = {}
         
        return response
       

    @action(detail=True)
    def retrieve_by_service_number(self, request, version=None, uuid=None):
        try:
            cpes = CpeUtp.objects.filter(service__number=uuid)
        except ValueError:
            response = Response(
                {"detail": "Numero de servicio inválido"},
                content_type="application/json",
                status=status.HTTP_400_BAD_REQUEST,
            )
            response.accepted_renderer = JSONRenderer()
            response.accepted_media_type = "application/json"
            response.renderer_context = {}
            return response

        serializer = self.get_serializer(cpes, many=True)

        return Response(serializer.data)

    @action(detail=True, methods=['post'])
    def add_history(self, request, version=None, uuid=None):

        try:
            cpe = CpeUtp.objects.get(uuid=uuid)
        except Exception as e:
            return build_response(
                f"El servicio con uuid = {uuid} no fue encontrado",
                status.HTTP_400_BAD_REQUEST)

        return add_cpe_history(cpe, request.data)


from django.core.exceptions import ValidationError


@csrf_exempt
def updateCpeUtp(request, version, uuid):

    if request.method == "POST" or request.method == "GET":

        # url = request.get_full_path()
        # utp_uuid = url.split("/").pop()
        # try:
        main_updatecpeutp(uuid)
        response = Response(
            {"detail": "ok"},
            content_type="application/json",
            status=status.HTTP_200_OK,
        )
        # except ValidationError as e:

        # response = Response({detail: str(e)},
        # content_type="application/json",
        # status=status.HTTP_400_BAD_REQUEST,
        # )
        # response.accepted_renderer = JSONRenderer()
        # response.accepted_media_type = "application/json"
        # response.renderer_context = {}
        # return response
        # except Exception as e:
        #     response = Response({**e.detail},
        #     content_type="application/json",
        #     status=status.HTTP_400_BAD_REQUEST,
        #     )

        response.accepted_renderer = JSONRenderer()
        response.accepted_media_type = "application/json"
        response.renderer_context = {}
        return response


@csrf_exempt
def updateProfile(request, version):

    if request.method == "POST" or request.method == "GET":
        url = request.get_full_path()
        radiusid = url.split("/").pop()
        main_updateprofileutp(radiusid)
        response = Response(
            {"detail": "ok"},
            content_type="application/json",
            status=status.HTTP_200_OK,
        )
        response.accepted_renderer = JSONRenderer()
        response.accepted_media_type = "application/json"
        response.renderer_context = {}
        return response
    else:
        response = Response(
            {"detail": "ok"},
            content_type="application/json",
            status=status.HTTP_200_OK,
        )
        response.accepted_renderer = JSONRenderer()
        response.accepted_media_type = "application/json"
        response.renderer_context = {}
        return response


class SystemProfileViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Profile
    retrieve:
        End point for Profile
    update:
        End point for Profile
    delete:
        End point for Profile
    """
    queryset = SystemProfile.objects.all()
    serializer_class = SystemProfileSerializer


from django.db.models import Q, Count


class GearViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Profile
    retrieve:
        End point for Profile
    update:
        End point for Profile
    delete:
        End point for Profile
    """
    queryset = Gear.objects.all()
    serializer_class = GearSerializer

    @action(detail=True, methods=['post'])
    @parser_classes([MultiPartParser])
    def add_gallery(self, request, version=None, pk=None):
        try:
            gear = Gear.objects.get(pk=pk)
        except Gear.DoesNotExist:
            response = Response(
                {
                    "detail":
                    f"El equipo interno con id = {pk} no fue encontrado"
                },
                content_type="application/json",
                status=status.HTTP_400_BAD_REQUEST,
            )
            response.accepted_renderer = JSONRenderer()
            response.accepted_media_type = "application/json"
            response.renderer_context = {}
            return response

        return add_gallery(gear, request)

    @action(detail=True, methods=['get'])
    def list_gallery(self, request, version=None, pk=None):
        try:
            gear = Gear.objects.get(pk=pk)
        except Gear.DoesNotExist:
            response = Response(
                {
                    "detail":
                    f"El equipo interno con id = {pk} no fue encontrado"
                },
                content_type="application/json",
                status=status.HTTP_400_BAD_REQUEST,
            )
            response.accepted_renderer = JSONRenderer()
            response.accepted_media_type = "application/json"
            response.renderer_context = {}
            return response

        picSerializer = PicSerializer(gear.pics.all(),
                                      context={"request": request},
                                      many=True)
        return build_response(picSerializer.data, 200)

    @action(detail=True, methods=['post'])
    @parser_classes([MultiPartParser])
    def add_document(self, request, version=None, pk=None):
        try:
            gear = Gear.objects.get(pk=pk)
        except Gear.DoesNotExist:
            response = Response(
                {"detail": f"El equipo con id = {pk} no fue encontrado"},
                content_type="application/json",
                status=status.HTTP_400_BAD_REQUEST,
            )
            response.accepted_renderer = JSONRenderer()
            response.accepted_media_type = "application/json"
            response.renderer_context = {}
            return response

        return add_document(gear, request)

    @action(detail=True, methods=['get'])
    def list_document(self, request, version=None, pk=None):
        try:
            gear = Gear.objects.get(pk=pk)
        except Gear.DoesNotExist:
            response = Response(
                {
                    "detail":
                    f"El equipo interno con id = {pk} no fue encontrado"
                },
                content_type="application/json",
                status=status.HTTP_400_BAD_REQUEST,
            )
            response.accepted_renderer = JSONRenderer()
            response.accepted_media_type = "application/json"
            response.renderer_context = {}
            return response

        docSerializer = DocSerializer(gear.documents.all(),
                                      context={"request": request},
                                      many=True)
        return build_response(docSerializer.data, 200)

    @action(detail=True, methods=['post'])
    def add_history(self, request, version=None, pk=None):

        try:
            gear = Gear.objects.get(pk=pk)
        except Exception as e:
            return build_response(f"El gear con id = {pk} no fue encontrado",
                                  status.HTTP_400_BAD_REQUEST)

        return add_gear_history(gear, request.data)

    @action(detail=False, methods=['get'])
    def brands(self, request, version=None):
        # import pprint
        l = list(Gear.objects.values('model__brand').exclude(model__brand='').order_by('model__brand')\
            .annotate(count=Count('model__brand')))
        # pprint.pprint(l)
        return build_response(l, 200)

    @action(detail=False, methods=['get'])
    def models(self, request, version=None):
        l = list( Gear.objects.values('model__model').exclude(model__brand='').\
            annotate(count=Count('model__model')).order_by('model__model'))
        return build_response(l, 200)

    @action(detail=False, methods=['get'])
    def vlans(self, request, version=None):
        l = list(
            Gear.objects.values('vlan').exclude(vlan='').annotate(
                count=Count('vlan')).order_by('vlan'))
        return build_response(l, 200)

    @action(detail=False, methods=['get'])
    def nodes(self, request, version=None):
        l = list(
            Gear.objects.values(
                'node__id', 'node__alias').order_by('node__alias').annotate(
                    count=Count('node__alias')))
        return build_response(l, 200)


class ProfileViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Profile
    retrieve:
        End point for Profile
    update:
        End point for Profile
    delete:
        End point for Profile
    """
    queryset = ProfileUTP.objects.all()
    serializer_class = ProfileUTPSerializer

    def destroy(self, request, *args, **kwargs):
        url = request.path.split("/")
        id_given = url.pop()
        deleteall = False
        if id_given == "":
            id_given = url.pop()
        if "?all" in id_given:
            deleteall = True
            id_given = int(url.pop())
        else:
            id_given = int(id_given)
        elem = ProfileUTP.objects.get(id=id_given)
        radius = Radius.objects.get(id=elem.radius.id)
        connection_data = RadiusConnection.objects.get(radius=radius)
        user = connection_data.username
        _l.PROFILEUTP.ADD(by_representation=user,
                          id_value=elem.id,
                          description=f'Petición para borrar profile',
                          entity_representation=repr(elem))
        data = {"profile": elem.name, "limitation": elem.limitation_name}
        try:
            if deleteall:
                _l.RADIUS.ADD(
                    by_representation=user,
                    id_value=radius.uuid,
                    description=
                    f'Petición para borrar profile link de RADIUS incluyendo profile y limitation',
                    entity_representation=repr(radius))
                try:
                    result = delete_radius_profile_witheverything(
                        radius.ip, user, connection_data.password, data)
                    if result != {}:
                        raise Exception("Error: " + str(result))
                except Exception as e:
                    _l.PROFILEUTP.ADD(by_representation=user,
                                      id_value=radius,
                                      id_field='id',
                                      description=f'fallo borrar profile: {e}')

                    raise serializers.ValidationError({
                        "error": f'fallo borrar profile: {e}',
                        "detail": str(e)
                    })
            else:
                _l.RADIUS.ADD(
                    by_representation=user,
                    id_value=radius.uuid,
                    description=
                    f'Petición para borrar profile link de RADIUS sin borrar profile y limitation',
                    entity_representation=repr(radius))
                try:
                    result = delete_radius_profile_onlylink(
                        radius.ip, user, connection_data.password, data)
                    if result != {}:
                        raise Exception("Error: " + str(result))
                except Exception as e:
                    _l.PROFILEUTP.ADD(by_representation=user,
                                      id_value=radius,
                                      id_field='id',
                                      description=f'fallo borrar profile: {e}')

                    raise serializers.ValidationError({
                        "error": f'fallo borrar profile: {e}',
                        "detail": str(e)
                    })
            return super(ProfileViewSet, self).destroy(request, *args,
                                                       **kwargs)
        except Exception as e:
            _l.PROFILEUTP.ADD(by_representation=user,
                              id_value=radius,
                              id_field='id',
                              description=f'fallo borrar profile: {e}')

            raise serializers.ValidationError({
                "error": f'fallo borrar profile: {e}',
                "detail": str(e)
            })


class RadiusViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Radius
    retrieve:
        End point for Radius
    update:
        End point for Radius
    delete:
        End point for Radius
    """
    queryset = Radius.objects.all()
    serializer_class = RadiusSerializer
    lookup_field = 'uuid'
    lookup_url_kwarg = 'uuid'

    def get_queryset(self):
        for_alias = self.request.GET.get('alias', None)
        if for_alias:
            return Radius.objects.filter(alias__icontains=for_alias)
        return Radius.objects.filter()


class AvailabilityViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Availability
    retrieve:
        End point for Availability
    update:
        End point for Availability
    delete:
        End point for Availability
    """
    queryset = Availability.objects.all()
    serializer_class = AvailabilitySerializer

    @action(detail=True, methods=['post'])
    def add_history(self, request, version=None, pk=None):

        try:
            availability = Availability.objects.get(pk=pk)
        except Exception as e:
            return build_response(
                f"El avalability con id = {pk} no fue encontrado",
                status.HTTP_400_BAD_REQUEST)

        return add_availability_history(availability, request.data)

    def get_queryset(self):
        queryset = Availability.objects.all()
        node = self.request.query_params.get('node', None)
        if node is not None:
            queryset = queryset.filter(node__uuid=node)
        return queryset


class DurationMikrotikViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Duration
    retrieve:
        End point for Duration
    update:
        End point for Duration
    delete:
        End point for Duration
    """
    queryset = DurationMikrotik.objects.all()
    serializer_class = DurationMikrotikSerializer

    def get_queryset(self):
        queryset = self.queryset
        node = self.request.query_params.get('node', None)
        if node is not None:
            queryset = queryset.filter(node__uuid=node)
        return queryset


class BuildingContactViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for BuildingContact
    retrieve:
        End point for BuildingContact
    update:
        End point for BuildingContact
    delete:
        End point for BuildingContact
    """
    queryset = BuildingContact.objects.all()
    serializer_class = BuildingContactSerializer

    @action(detail=True, methods=['post'])
    def add_history(self, request, version=None, pk=None):

        try:
            building_contact = BuildingContact.objects.get(pk=pk)
        except Exception as e:
            return build_response(
                f"El building_contact con id = {pk} no fue encontrado",
                status.HTTP_400_BAD_REQUEST)

        return add_building_contact_history(building_contact, request.data)


class ServiceStatusLogView(viewsets.ModelViewSet):
    queryset = ServiceStatusLog.objects.all()
    serializer_class = ServiceStatusLogSerializer
    # permission_classes = [IsSystemInternalPermission,IsAuthenticated]


class ChangeStatusNodeView(viewsets.ReadOnlyModelViewSet):
    serializer_class = ChangeStatusNodeSerializer

    def get_queryset(self):
        qs = ChangeStatusNode.objects.all()
        initiald = self.request.query_params.get('initiald', None)
        finald = self.request.query_params.get('finald', None)
        if initiald and finald:
            qs = qs.filter(fecha__gte=initiald, fecha__lte=finald)

        return qs


from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.mixins import CreateModelMixin
from .serializers import ReportSerializer


@method_decorator(csrf_exempt, name="dispatch")
class GeneralServiceStatusLogReport(CreateModelMixin,
                                    viewsets.ReadOnlyModelViewSet):
    serializer_class = ReportSerializer

    def create(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        quantity = ChangeStatusNode.objects.all().count()

        if quantity > 0:
            fecha = self.request.POST.get(u'filter_fecha', None)
            node = self.request.POST.get(u'filter_node', None)
            status = self.request.POST.get(u'filter_status', None)

            if fecha:
                fecha = fecha.split(',')
                initiald = fecha[0]
                finald = fecha[1]
            else:
                initiald = qs.first().fecha
                finald = qs.last().fecha

            qs = ChangeStatusNode.objects.filter(fecha__gte=initiald,
                                                 fecha__lte=finald)

            if status:
                qs = qs.filter(status=status)

            if node:
                qs = qs.filter(node=node)

            qs = qs.values()
            output = list()

            try:
                df = pd.DataFrame(qs)
                df = df.groupby(['node', 'status']).apariciones.agg([np.sum])
                df = df.sort_values(by=['node', 'sum'], ascending=False)

                for clave in self.get_claves(df):
                    output.append({
                        'node': clave[0],
                        'status': clave[1],
                        'apariciones': df['sum'][clave]
                    })

                output = ReportSerializer(output, many=True).data
            except Exception as e:
                print(str(e))
        else:
            output = ChangeStatusNode.objects.none()

        return Response(output)

    def get_queryset(self):
        return []

    def get_claves(self, df):
        """
        Genera las claves del diccionario de un dataframe
        """
        output = list()
        for key, value in df.to_dict().items():
            for k, v in value.items():
                output.append(k)

        return set(output)
