import requests
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import action, api_view, permission_classes
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.serializers import ValidationError
from .permissions import IsSystemInternalPermission, TOKEN
from dashboard.utils import LOG
from documents.models import Pic, Document
from devices.models import IPTable, MACTable
from address.models import Address
from customers.models import Service


def build_response(res, status):
    """
        contruye un Response object con el objecto a enviar y el status
    """

    response = Response(
        res,
        content_type="application/json",
        status=status,
    )
    response.accepted_renderer = JSONRenderer()
    response.accepted_media_type = "application/json"

    return response


def add_gallery(obj, request):

    try:
        try:
            if type(request.data['pic']) == str:
                raise Exception('Debe subir un archivo.')
        except Exception as e:
            raise Exception('Debe subir un archivo.')

        pic = Pic(caption=request.data['caption'], photo=request.data['pic'])

        name = pic.photo.name
        print(name)
        name_split = name.split('.')
        if not name_split[len(name_split) - 1] in ['jpeg', 'jpg', 'png']:
            raise Exception('Archivo no soportado.')

        if request.data['caption'] == "":
            raise Exception('Debe colocar pie de foto.')

        pic.content_object = obj
        pic.save()
    except Exception as e:
        return build_response({"detail": f"{str(e)}"},
                              status.HTTP_400_BAD_REQUEST)

    return Response({'ok': True}, status=status.HTTP_200_OK)


def add_document(obj, request):

    try:

        try:
            if type(request.data['file']) == str:
                raise Exception('Debe subir un archivo.')

        except Exception as e:
            raise Exception('Debe subir un archivo.')

        document = Document(
            description=request.data['description'],
            tag=request.data['tag'],
            file=request.data['file'],
            agent=request.user if request.user.is_authenticated else None)

        if request.data['description'] == "":
            raise Exception('Debe colocar una descripción.')

        document.content_object = obj
        document.save()
    except Exception as e:
        return build_response({"detail": f"{str(e)}"},
                              status.HTTP_400_BAD_REQUEST)

    return Response({'ok': True}, status=status.HTTP_200_OK)


def add_mikrotik_history(mikrotik, data):
    try:
        try:
            ip = IPTable.objects.get(ip=data.get('ip', None))
        except IPTable.DoesNotExist as e:
            ip = None
        finally:
            ip = None

        addr, created = Address.objects.get_or_create(
            id_pulso=data['address_pulso'])

        mikrotik.history.create(
            id=data['id'],
            created=data['created'],
            modified=data['created'],
            history_date=data['history_date'],
            history_type=data['history_type'],
            # history_id= data['history_id'],
            group_id=data.get('group_id', None),
            alias=data['alias'],
            status=data['status'],
            # address = data['address'],
            # street = data['street'],
            # house_number = data['house_number'],
            # commune = data['commune'],
            # location = data['location'],
            address_pulso=addr,
            parent_id=data.get('parent_id', None),
            default_connection_node_id=data.get('default_connection_node_id',
                                                None),
            # is_building = data['is_building'],
            towers=data['towers'],
            apartments=data['apartments'],
            phone=data['phone'],
            # antennas_left = data['antennas_left'],
            ip=ip,
            comment=data['comment'],
            #no_address_binding = data['no_address_binding'],
            matrix_seller=data.get('matrix_seller', None),
            description=data['description'],
        )
    except Exception as e:
        print(e)
        return build_response({'detail': f"{e}"}, status.HTTP_400_BAD_REQUEST)

    return Response({'ok': True}, status=status.HTTP_200_OK)


def add_cpe_history(cpe, data):
    try:
        try:
            ip = IPTable.objects.get(ip=data.get('ip', None))
        except IPTable.DoesNotExist as e:
            ip = None
        finally:
            ip = None

        try:
            mac = MACTable.objects.get(mac=data.get('mac', None))
        except MACTable.DoesNotExist as e:
            mac = None
        finally:
            mac = None

        cpe.history.create(
            id=data['id'],
            created=data['created'],
            modified=data['created'],
            history_date=data['history_date'],
            history_type=data['history_type'],
            # history_id= data['history_id'],
            alias=data.get('alias', None),
            serial=data.get('serial', None),
            nodo_utp_id=data.get('nodo_utp', None),
            description=data.get('description', None),
            model_id=data.get('model', None),
            service_id=data.get('service', None),
            service_number_verified=data.get('service_number_verified', None),
            mac=mac,
            ip=ip,
            primary=data.get('primary', None),
        )
    except Exception as e:
        print(e)
        return build_response({'detail': f"{e}"}, status.HTTP_400_BAD_REQUEST)

    return Response({'ok': True}, status=status.HTTP_200_OK)


def add_gear_history(gear, data):
    try:
        try:
            ip = IPTable.objects.get(ip=data.get('ip', None))
        except IPTable.DoesNotExist as e:
            ip = None
        finally:
            ip = None

        try:
            mac = MACTable.objects.get(mac=data.get('mac', None))
        except MACTable.DoesNotExist as e:
            mac = None
        finally:
            mac = None

        gear.history.create(
            id=data['id'],
            created=data['created'],
            modified=data['created'],
            history_date=data['history_date'],
            history_type=data['history_type'],
            # history_id= data['history_id'],
            description=data.get('description', 'None Description'),
            ip=ip,
            mac=mac,
            vlan=data.get('vlan', None),
            node_id=data.get('node', None),
            firmware_version=data.get('firmware_version', None),
            notes=data.get('notes', None),
            latest_backup_date_cache=data.get('latest_backup_date_cache',
                                              None),
            model_id=data.get('model', gear.node.id),
        )
    except Exception as e:
        print(e)
        return build_response({'detail': f"{e}"}, status.HTTP_400_BAD_REQUEST)

    return Response({'ok': True}, status=status.HTTP_200_OK)


def add_building_contact_history(building_contact, data):
    try:
        building_contact.history.create(
            id=data['id'],
            created=data['created'],
            modified=data['created'],
            history_date=data['history_date'],
            history_type=data['history_type'],
            # history_id= data['history_id'],
            kind=data.get('kind', None),
            name=data.get('name', None),
            phone=data.get('phone', None),
            email=data.get('email', None),
            node_id=data.get('node', building_contact.node.id))
    except Exception as e:
        print(e)
        return build_response({'detail': f"{e}"}, status.HTTP_400_BAD_REQUEST)

    return Response({'ok': True}, status=status.HTTP_200_OK)


def add_availability_history(availbility, data):

    try:
        addr = Address.objects.get_or_create(
            id_pulso=data.get('id_pulso', None))
    except Address.DoesNotExist as e:
        addr = None
    finally:
        addr = None

    try:
        availbility.history.create(
            id=data['id'],
            created=data['created'],
            modified=data['created'],
            history_date=data['history_date'],
            history_type=data['history_type'],
            # history_id= data['history_id'],
            address=addr,
            node_id=data.get('node', availbility.node.id))
    except Exception as e:
        print(e)
        return build_response({'detail': f"{e}"}, status.HTTP_400_BAD_REQUEST)

    return Response({'ok': True}, status=status.HTTP_200_OK)


@csrf_exempt
@api_view(['POST'])
@permission_classes([
    IsSystemInternalPermission,
])
# @permission_classes([IsAuthenticated])
def cutting_replacement(request, version=None):
    from .tasks import task_cutting_replacement
    from utp.models import MikrotikConnection

    service_number = request.data.get('service_number')
    type_cr = request.data.get('type')

    if (service_number):
        # Buscamos el número de Servicio
        service = Service.objects.get(number=service_number)

        current_device = None
        technology = ""

        # Evaluamos los sets que contengan un nodo primario
        if service.cpeutp_set.filter(primary=True).count() == 1:
            current_device = service.cpeutp_set.filter(primary=True).first()
            technology = "UTP"
        elif service.onu_set(manager='all_objects').filter(primary=True).count() == 1:
            current_device = service.onu_set.filter(primary=True).first()
            technology = "FIBRA"

        # Evaluamos el valor de current_device
        if current_device is None:
            return build_response(
                {
                    "detail":
                    "Número de servicio inválido o no es equipo primario"
                }, status.HTTP_400_BAD_REQUEST)
        # try:
        #     cpe = CpeUtp.objects.get(service__number=service_number,
        #                              primary=True)
        # except Exception:
        #     return build_response(
        #         {
        #             "detail":
        #             "Número de servicio inválido o no es equipo primario"
        #         }, status.HTTP_400_BAD_REQUEST)

        # Creamos el diccionario con la información
        message = ""
        current_mikrotik = current_device.nodo_utp if technology == "UTP" else current_device.olt.node
        mikrotik_connection = MikrotikConnection.objects.get(
            mikrotik=current_mikrotik)
        contract = {}
        contract['mikrotik_ip'] = current_mikrotik.ip.ip
        try:
            contract['primary_equipment_ip'] = current_device.ip.ip
        except Exception as e:
            print(e)
            return build_response(
                {
                    "detail":
                    "El equipo primario no tiene una IP para realizar el corte/reposicion"
                }, status.HTTP_400_BAD_REQUEST)
        contract['slug'] = service_number
        contract['number'] = service_number
        contract['mikrotik_username'] = mikrotik_connection.username
        contract['mikrotik_password'] = mikrotik_connection.password

        # Ejecutamos la tarea necesaria
        if (type_cr == 'corte'):
            task_cutting_replacement.delay(contract, 'corte')
            message = f'El servicio #{service_number} sera cortado en minutos'
        elif (type_cr == 'reposicion'):
            task_cutting_replacement.delay(contract, 'reposicion')
            message = f'El servicio #{service_number} sera restituido en minutos'
        else:
            return build_response({"detail": "tipo no definido"},
                                  status.HTTP_400_BAD_REQUEST)

        # Ejecutamos la función para actualizar el estado dos veces
        service.seen_connected_live
        service.seen_connected_live
        return build_response({"detail": message}, status.HTTP_200_OK)

    else:
        return build_response({"detail": "Número de servicio no definido"},
                              status.HTTP_400_BAD_REQUEST)


from dynamic_preferences.registries import global_preferences_registry


def get_activity(id):
    global_preferences = global_preferences_registry.manager()

    try:
        URL_MATRIX = global_preferences["url_endpoint_erp"]
    except:
        URL_MATRIX = 'localhost:8001/'
    headers = {'Authorization': TOKEN}
    r = requests.get(f'{URL_MATRIX}api/v1/activities/{id}/', headers=headers)
    print(r.text)
    status_code = r.status_code
    activity = r.json()
    return status_code, activity


@api_view(['GET'])
# @permission_classes([IsSystemInternalPermission,])
def get_activities(request, version=None):
    try:
        headers = {'Authorization': TOKEN}

        r = requests.get(f'{URL_MATRIX}api/v1/activities/', headers=headers)
        status_code = r.status_code
        if status_code == 200:
            activity = r.json()
            response = build_response(activity['results'], status_code)
        else:
            response = build_response(
                {
                    "detail":
                    "Error al solicitar API de Matrix. Error " + status_code
                }, status_code)

        return response
    except:
        return build_response(
            {"detail": "Error al solicitar API de Matrix. Error "}, 500)